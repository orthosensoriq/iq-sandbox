module orthosensor {


    class CaseInteroperativeDataController {
        public static $inject = ['$window', 'CaseDetailsFactory', 'LogFactory', 'PatientService', 'SensorService', 'CaseService', '$confirm'];
        //var $ctrl = this;
        public linkStationsIDs: any[];
        public caseEvent: {};
        public showImages: boolean;
        public sensorDataLines: any[];
        public patient: orthosensor.domains.Patient;
        public event: {};
        public clinicalData: any;
        public loading: boolean;
        constructor(public $window: ng.IWindowService,
            public CaseDetailsFactory: any, public LogFactory: any,
            public PatientService: orthosensor.services.PatientService,
            public SensorService,
            public CaseService: orthosensor.services.CaseService,
            public $confirm: any) {

        }
        ////////////////
        $onInit() {
            this.caseEvent = {};
            this.showImages = false;
            this.sensorDataLines = [];
            this.loading = false;
            this.patient = this.PatientService.getPatient();
            console.log(this.patient);
            //console.log(caseEvent);
            // this.event = this.CaseDetailsFactory.getEvent();
            //console.log(this.caseEvent);
            console.log(this.event);
            if (this.event.Id !== null) {
                this.clinicalData = this.CaseService.clinicalData;
                //if (this.event.Clinical_Data__r) {
                //    this.clinicalData = this.event.Clinical_Data__r[0];
                //}    
                if (this.event.OS_Device_Events__r) {
                    this.showImages = this.event.OS_Device_Events__r.length > 0;
                } else {
                    this.showImages = false;
                }    
                this.loadSensorDataLines(this.event.Id);
                // console.log(this.sensorDataLines);
            } else {
                console.log('Event not yet defined');
            }
        };
        $onChanges(changesObj) { };
        $onDestory() { };
        // get list of link stations for the hospital and then call the routines to retrive the data       
        getLinkStationData() {
            let hospitalId = this.patient.hospitalId;
            console.log(hospitalId);
            if (hospitalId !== null) {
                // var l = Ladda.create(document.querySelector('.retrieve-btn'));
                // l.start();
                this.loading = true;
                this.CaseDetailsFactory.LoadLinkStationsIDs(hospitalId)
                    .then((data) => {
                        console.log(data);
                        this.linkStationsIDs = data;
                        this.loadData();
                        this.loading = false;
                    }, function (error) {
                        console.log(error);
                    });
            }
        }
        // start retrieving linked images from sensors. CheckSoftwareVersion acutally retrieves data once
        // it determines which version of Verasense is being used. It then retrieves the images
        loadData() {
            if (this.linkStationsIDs.length > 0) {
                for (let i = 0; i < this.linkStationsIDs.length; i++) {
                    let procedureId = this.event.Id;
                    console.log(this.event.Id);
                    let serialNo = this.linkStationsIDs[i].Serial_Number__c;
                    console.log(serialNo);
                    let laterality = this.clinicalData.Laterality__c;
                    console.log(laterality);
                    if (serialNo !== null) {
                        this.CaseDetailsFactory.CheckSoftwareVersion(procedureId, serialNo, laterality)
                            .then((data) => {
                                console.log(data);
                                this.getImages(data);
                            }, function (error) {
                                console.log(error);
                            });
                    }
                }
            } else {
                this.$window.alert('No images available');
            }
        }
        // images are retrieved and then added as attachments in Force.com
        getImages(linkStationsID) {
            //console.log(linkStationsID);
            let deviceEvents = this.event.OS_Device_Events__r;
            console.log(deviceEvents);
            if (deviceEvents !== null) {
                if (deviceEvents.length > 0) {
                    for (let i = 0; i < deviceEvents.length; i++) {
                        //console.log(deviceEvents[i]);
                        this.CaseDetailsFactory.GetImageList(this.event.Id, deviceEvents[i].OS_Device__r.Id, linkStationsID)
                            .then((data)  => {
                                //console.log(data);
                                var result = JSON.parse(data.replace(/&quot;/g, '"'));
                                console.log(result);
                                if ((typeof result.data !== "undefined") && (result.status !== 'error')) {
                                    let imageURLArray = result.data.images;
                                    let imageURLArrayCaptures = [];
                                    let recId = this.event.Id;
                                    let ii = 0;
                                    for (let k = 0; k < imageURLArray.length; k++) {
                                        //console.log('imageURLArray[i].indexOf(45) = ' + imageURLArray[k].indexOf('45.'));
                                        //console.log('imageURLArray.length = ' + imageURLArray.length);
                                        if (imageURLArray[k].indexOf('2.') > -1 || imageURLArray[k].indexOf('10.') > -1 || imageURLArray[k].indexOf('45.') > -1 || imageURLArray[k].indexOf('90.') > -1) {
                                            imageURLArrayCaptures.push(imageURLArray[k]);
                                        }
                                    }
                                    for (let j = 0; j < imageURLArrayCaptures.length; j++) {
                                        console.log(imageURLArrayCaptures[j]);
                                        this.createImageAttachment(recId, imageURLArrayCaptures[j]);
                                        ii++;
                                    }
                                    console.log(imageURLArrayCaptures.length);
                                    console.log(ii);
                                    if (ii === imageURLArrayCaptures.length) {
                                        //console.log('about to match');
                                        this.matchSDwithAttachment(recId);
                                    }
                                } else {
                                    console.log(event);
                                }
                            }, function (error) {
                                console.log(error);
                            });
                    }
                } else {
                    console.log('Nothing to do hide the spinner');
                }
            } else {
                console.log('Nothing to do hide the spinner');
            }
            // Ladda.stopAll();
        }
        createImageAttachment(recordId, imageURL) {
            this.CaseDetailsFactory.SaveImageToRecord(recordId, imageURL)
                .then((data) => {
                    console.log(data);
                    //console.log('Image was saved');
                }, function (error) {
                    console.log('No Images Captured.');
                    console.log(error);
                });
        }
        matchSDwithAttachment(recordId) {
            console.log(recordId);
            this.CaseDetailsFactory.MatchSensorDataAndAttachment(recordId)
                .then((data) => {
                    console.log(data);
                    //console.log('Sensor Data Match Complete');
                    this.loadSensorDataLines(recordId);
                }, (error) => {
                    //console.log('No Images Captured.');
                    console.log(error);
                });
        }
        loadSensorDataLines(procedureId) {
            console.log('Procedure Id: ' + procedureId);
            this.CaseDetailsFactory.loadSensorDataLines(procedureId)
                .then((data) => {
                    console.log(data);
                    this.sensorDataLines = data;
                    if (this.sensorDataLines.length > 0) {
                        if (this.sensorDataLines[0].AttachmentID === "") {
                            //if attachment ids are not found
                            //console.log("Getting attachments again!");
                            console.log(this.sensorDataLines);
                        }
                    }
                    //console.log(this.sensorDataLines);
                    // Ladda.stopAll();
                }, (error) => {
                    console.log('No Images Captured.');
                    console.log(error);
                });
        }
        deleteSensorImage(line: any) {
            this.$confirm({ text: 'Are you sure you want to delete this image?', title: 'Delete Sensor Image' })
                .then(function () {
                    this.CaseDetailsFactory.DeleteSensorLine(line, this.event.Id)
                        .then((data) => {
                        console.log(data);
                        this.loadSensorDataLines(this.event.Id);
                    }, function (error) {
                        console.log(error);
                    });
                });
        }
    }

    class CaseInteroperativeData implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                event: '<',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = CaseInteroperativeDataController;
            this.templateUrl = 'app/caseDetails/caseInteroperativeData/caseInteroperativeData.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osCaseInteroperativeData', new CaseInteroperativeData());
}

