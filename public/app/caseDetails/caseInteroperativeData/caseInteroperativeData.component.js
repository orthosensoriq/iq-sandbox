var orthosensor;
(function (orthosensor) {
    var CaseInteroperativeDataController = (function () {
        function CaseInteroperativeDataController($window, CaseDetailsFactory, LogFactory, PatientService, SensorService, CaseService, $confirm) {
            this.$window = $window;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.LogFactory = LogFactory;
            this.PatientService = PatientService;
            this.SensorService = SensorService;
            this.CaseService = CaseService;
            this.$confirm = $confirm;
        }
        ////////////////
        CaseInteroperativeDataController.prototype.$onInit = function () {
            this.caseEvent = {};
            this.showImages = false;
            this.sensorDataLines = [];
            this.loading = false;
            this.patient = this.PatientService.getPatient();
            console.log(this.patient);
            //console.log(caseEvent);
            // this.event = this.CaseDetailsFactory.getEvent();
            //console.log(this.caseEvent);
            console.log(this.event);
            if (this.event.Id !== null) {
                this.clinicalData = this.CaseService.clinicalData;
                //if (this.event.Clinical_Data__r) {
                //    this.clinicalData = this.event.Clinical_Data__r[0];
                //}    
                if (this.event.OS_Device_Events__r) {
                    this.showImages = this.event.OS_Device_Events__r.length > 0;
                }
                else {
                    this.showImages = false;
                }
                this.loadSensorDataLines(this.event.Id);
                // console.log(this.sensorDataLines);
            }
            else {
                console.log('Event not yet defined');
            }
        };
        ;
        CaseInteroperativeDataController.prototype.$onChanges = function (changesObj) { };
        ;
        CaseInteroperativeDataController.prototype.$onDestory = function () { };
        ;
        // get list of link stations for the hospital and then call the routines to retrive the data       
        CaseInteroperativeDataController.prototype.getLinkStationData = function () {
            var _this = this;
            var hospitalId = this.patient.hospitalId;
            console.log(hospitalId);
            if (hospitalId !== null) {
                // var l = Ladda.create(document.querySelector('.retrieve-btn'));
                // l.start();
                this.loading = true;
                this.CaseDetailsFactory.LoadLinkStationsIDs(hospitalId)
                    .then(function (data) {
                    console.log(data);
                    _this.linkStationsIDs = data;
                    _this.loadData();
                    _this.loading = false;
                }, function (error) {
                    console.log(error);
                });
            }
        };
        // start retrieving linked images from sensors. CheckSoftwareVersion acutally retrieves data once
        // it determines which version of Verasense is being used. It then retrieves the images
        CaseInteroperativeDataController.prototype.loadData = function () {
            var _this = this;
            if (this.linkStationsIDs.length > 0) {
                for (var i = 0; i < this.linkStationsIDs.length; i++) {
                    var procedureId = this.event.Id;
                    console.log(this.event.Id);
                    var serialNo = this.linkStationsIDs[i].Serial_Number__c;
                    console.log(serialNo);
                    var laterality = this.clinicalData.Laterality__c;
                    console.log(laterality);
                    if (serialNo !== null) {
                        this.CaseDetailsFactory.CheckSoftwareVersion(procedureId, serialNo, laterality)
                            .then(function (data) {
                            console.log(data);
                            _this.getImages(data);
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            else {
                this.$window.alert('No images available');
            }
        };
        // images are retrieved and then added as attachments in Force.com
        CaseInteroperativeDataController.prototype.getImages = function (linkStationsID) {
            var _this = this;
            //console.log(linkStationsID);
            var deviceEvents = this.event.OS_Device_Events__r;
            console.log(deviceEvents);
            if (deviceEvents !== null) {
                if (deviceEvents.length > 0) {
                    for (var i = 0; i < deviceEvents.length; i++) {
                        //console.log(deviceEvents[i]);
                        this.CaseDetailsFactory.GetImageList(this.event.Id, deviceEvents[i].OS_Device__r.Id, linkStationsID)
                            .then(function (data) {
                            //console.log(data);
                            var result = JSON.parse(data.replace(/&quot;/g, '"'));
                            console.log(result);
                            if ((typeof result.data !== "undefined") && (result.status !== 'error')) {
                                var imageURLArray = result.data.images;
                                var imageURLArrayCaptures = [];
                                var recId = _this.event.Id;
                                var ii = 0;
                                for (var k = 0; k < imageURLArray.length; k++) {
                                    //console.log('imageURLArray[i].indexOf(45) = ' + imageURLArray[k].indexOf('45.'));
                                    //console.log('imageURLArray.length = ' + imageURLArray.length);
                                    if (imageURLArray[k].indexOf('2.') > -1 || imageURLArray[k].indexOf('10.') > -1 || imageURLArray[k].indexOf('45.') > -1 || imageURLArray[k].indexOf('90.') > -1) {
                                        imageURLArrayCaptures.push(imageURLArray[k]);
                                    }
                                }
                                for (var j = 0; j < imageURLArrayCaptures.length; j++) {
                                    console.log(imageURLArrayCaptures[j]);
                                    _this.createImageAttachment(recId, imageURLArrayCaptures[j]);
                                    ii++;
                                }
                                console.log(imageURLArrayCaptures.length);
                                console.log(ii);
                                if (ii === imageURLArrayCaptures.length) {
                                    //console.log('about to match');
                                    _this.matchSDwithAttachment(recId);
                                }
                            }
                            else {
                                console.log(event);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
                else {
                    console.log('Nothing to do hide the spinner');
                }
            }
            else {
                console.log('Nothing to do hide the spinner');
            }
            // Ladda.stopAll();
        };
        CaseInteroperativeDataController.prototype.createImageAttachment = function (recordId, imageURL) {
            this.CaseDetailsFactory.SaveImageToRecord(recordId, imageURL)
                .then(function (data) {
                console.log(data);
                //console.log('Image was saved');
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.matchSDwithAttachment = function (recordId) {
            var _this = this;
            console.log(recordId);
            this.CaseDetailsFactory.MatchSensorDataAndAttachment(recordId)
                .then(function (data) {
                console.log(data);
                //console.log('Sensor Data Match Complete');
                _this.loadSensorDataLines(recordId);
            }, function (error) {
                //console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.loadSensorDataLines = function (procedureId) {
            var _this = this;
            console.log('Procedure Id: ' + procedureId);
            this.CaseDetailsFactory.loadSensorDataLines(procedureId)
                .then(function (data) {
                console.log(data);
                _this.sensorDataLines = data;
                if (_this.sensorDataLines.length > 0) {
                    if (_this.sensorDataLines[0].AttachmentID === "") {
                        //if attachment ids are not found
                        //console.log("Getting attachments again!");
                        console.log(_this.sensorDataLines);
                    }
                }
                //console.log(this.sensorDataLines);
                // Ladda.stopAll();
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.deleteSensorImage = function (line) {
            this.$confirm({ text: 'Are you sure you want to delete this image?', title: 'Delete Sensor Image' })
                .then(function () {
                var _this = this;
                this.CaseDetailsFactory.DeleteSensorLine(line, this.event.Id)
                    .then(function (data) {
                    console.log(data);
                    _this.loadSensorDataLines(_this.event.Id);
                }, function (error) {
                    console.log(error);
                });
            });
        };
        return CaseInteroperativeDataController;
    }());
    CaseInteroperativeDataController.$inject = ['$window', 'CaseDetailsFactory', 'LogFactory', 'PatientService', 'SensorService', 'CaseService', '$confirm'];
    var CaseInteroperativeData = (function () {
        function CaseInteroperativeData() {
            this.bindings = {
                event: '<',
            };
            this.controller = CaseInteroperativeDataController;
            this.templateUrl = 'app/caseDetails/caseInteroperativeData/caseInteroperativeData.component.html';
        }
        return CaseInteroperativeData;
    }());
    angular
        .module('orthosensor')
        .component('osCaseInteroperativeData', new CaseInteroperativeData());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseInteroperativeData.component.js.map