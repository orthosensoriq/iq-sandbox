var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var CaseDetailsService = (function () {
            function CaseDetailsService() {
            }
            return CaseDetailsService;
        }());
        CaseDetailsService.inject = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
        services.CaseDetailsService = CaseDetailsService;
        angular
            .module('orthosensor.dashboard')
            .service('CaseDetailsService', CaseDetailsService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseDetails.service.js.map