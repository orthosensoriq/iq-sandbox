module orthosensor.services {

    export class CaseDetailsService {
        static inject: Array<string> = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
    
        constructor() { }
    }

    angular
        .module('orthosensor.dashboard')
        .service('CaseDetailsService', CaseDetailsService);
}