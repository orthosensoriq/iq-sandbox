var orthosensor;
(function (orthosensor) {
    var ProcedureInfoController = (function () {
        function ProcedureInfoController(PatientService, CaseDetailsFactory, CaseService, $timeout, $state, $uibModal) {
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.CaseService = CaseService;
            this.$timeout = $timeout;
            this.$state = $state;
            this.$uibModal = $uibModal;
        }
        ProcedureInfoController.prototype.$onInit = function () {
            //console.log(this.clinicalData);
            this.clinicalData = this.CaseService.clinicalData;
            console.log(this.clinicalData);
            //thisevent: '<',
            this.patientCase = this.CaseService.currentCase;
            this.procedure = this.CaseService.currentProcedure;
        };
        ProcedureInfoController.prototype.completeEvent = function (eventId) {
            var _this = this;
            this.CaseDetailsFactory.CompleteEvent(eventId)
                .then(function (data) {
                //console.log(data);
                _this.$state.go('patientDetails');
            }, function (error) { });
        };
        ;
        ProcedureInfoController.prototype.editProcedure = function (procedure, patientCase) {
            console.log(procedure);
            console.log(patientCase);
            // console.log(clinicalData);`
            this.$uibModal.open({
                component: 'osEditProcedure',
                // controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory, PatientListFactory, PatientService) {
                //     var patient = PatientService.getPatient();
                //     PatientListFactory.loadSurgeons(patient.practiceId).then(function (data) {
                //         //console.log(data);
                //         $scope.surgeons = data;
                //     }, function (error) {
                //         console.log(error);
                //         });
                //     let clinical = procedure.Clinical_Data__r[0];
                //     $scope.procedureData = {};
                //     $scope.procedureData.Id = procedure.Id;
                //     $scope.procedureData.caseId = patientCase.Id;
                //     $scope.procedureData.physician = procedure.Physician__c;
                //     $scope.procedureData.laterality = patientCase.Laterality__c;
                //     $scope.procedureData.procedureDateString = new Date(procedure.Appointment_Start__c);
                //     $scope.procedureData.patientConsentObtained = clinical.Patient_Consent_Obtained__c;
                //     $scope.procedureData.anesthesiaType = clinical.Anesthesia_Type__c;
                //     console.log($scope.procedureData.procedureDateString);
                //     $scope.cancel = function () {
                //         $uibModalInstance.dismiss('cancel');
                //     };
                //     $scope.open = { procDate: false };
                //     $scope.open = function ($event, whichDate) {
                //         $event.preventDefault();
                //         $event.stopPropagation();
                //         $scope.open[whichDate] = true;
                //     };
                //     $scope.checkinput = function () {
                //         console.log(months);
                //     };
                //     $scope.dateOptions = {
                //         formatYear: 'yy',
                //         startingDay: 1,
                //         showWeeks: false
                //     };
                //     $scope.updateProcedure = function () {
                //         $scope.case.Laterality__c = $scope.procedureData.laterality;
                //         CaseDetailsFactory.UpdateProcedure($scope.procedureData).then(function (data) {
                //             //console.log(data);
                //             $uibModalInstance.dismiss();
                //             $state.go($state.current, {}, { reload: true });
                //         }, function (error) {
                //             console.log(error);
                //         });
                //     };
                // },
                size: 'sm'
            });
        };
        ;
        return ProcedureInfoController;
    }());
    ProcedureInfoController.$inject = ['PatientService', 'CaseDetailsFactory', 'CaseService', '$timeout', '$state', '$uibModal'];
    var ProcedureInfo = (function () {
        function ProcedureInfo() {
            this.bindings = {};
            this.controller = ProcedureInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/caseDetails/procedureInfo/procedureInfo.component.html';
        }
        return ProcedureInfo;
    }());
    angular
        .module('orthosensor')
        .component('osProcedureInfo', new ProcedureInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=procedureInfo.component.js.map