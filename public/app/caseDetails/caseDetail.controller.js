(function () {
    angular
        .module('orthosensor')
        .controller('CaseDetailsController', CaseDetailsController);
    CaseDetailsController.$inject = ['$window', '$scope', '$rootScope', '$state', '$uibModal', '$confirm', 'CaseDetailsFactory', 'LogFactory', 'SurveyService', 'PatientService', 'SensorService', 'CaseService', 'moment', 'events', 'sensorRefs'];
    function CaseDetailsController($window, $scope, $rootScope, $state, $uibModal, $confirm, CaseDetailsFactory, LogFactory, SurveyService, PatientService, SensorService, CaseService, moment, events, sensorRefs) {
        var ctrl = this; //can we mix $scope with controller as?
        //variables
        //$scope.patient = $rootScope.patient;
        $scope.case = $rootScope.case;
        $scope.surveyGraphable = true;
        $scope.toggleValue = 'chart';
        $scope.sensorEntryType = 'barcode';
        $scope.implantEntryType = 'barcode';
        $scope.showGraph = false;
        //methods        
        $scope.goBack = goBack;
        $scope.switchEvent = switchEvent;
        $scope.surveyChange = surveyChange;
        $scope.canGraph = canGraph;
        $scope.editPatient = editPatient;
        $scope.togglePatientDetailsDrawer = togglePatientDetailsDrawer;
        $scope.toggleOutcomes = toggleOutcomes;
        // $scope.openSurveyLinkQRCode = openSurveyLinkQRCode;
        $scope.addSensor = addSensor;
        $scope.checkAndSaveImplantBarcode = checkAndSaveImplantBarcode;
        $scope.addImplant = addImplant;
        $scope.refreshEventsAndSurveys = refreshEventsAndSurveys;
        $scope.refreshEvents = refreshEvents;
        $scope.deleteSensor = deleteSensor;
        $scope.addNewCaseNote = addNewCaseNote;
        $scope.editNote = editNote;
        $scope.deleteNote = deleteNote;
        //activate should include all start up functions - for now it does not include all
        // and will be refactored over time 
        activate();
        function activate() {
            // retrieved from resolver
            this.events = events;
            $scope.events = events;
            // console.log($scope.events);
            $scope.patient = PatientService.patient;
            CaseService.setCurrentCase($scope.case);
            //console.log($scope.patient);
            $scope.isPatientAnonymous = PatientService.isAnonymous();
            LogFactory.logReadsObject($scope.case.Id, 'Case__c');
            loadCaseEvents();
            //retrieved from resolver
            $scope.sensorRefs = sensorRefs;
            loadProducts();
            //loadSensorRefs();
            // getCompletedSurveys();
        }
        function goBack() {
            $window.history.back();
        }
        function loadCaseEvents() {
            for (var i = 0; i < $scope.events.length; i++) {
                if ($scope.events[i].Event_Type_Name__c === 'Procedure') {
                    $scope.procedure = $scope.events[i];
                    CaseService.currentProcedure = $scope.procedure;
                    $scope.clinicalData = $scope.procedure.Clinical_Data__r[0];
                    CaseService.clinicalData = $scope.procedure.Clinical_Data__r[0];
                    console.log($scope.clinicalData);
                    if ($scope.events[i].Implant_Components__r) {
                        for (var j = 0; j < $scope.events[i].Implant_Components__r.length; j++) {
                            var expDate = $scope.events[i].Implant_Components__r[j].Expiration_Date__c;
                            // console.log(expDate);
                            expDate = moment(expDate).add(1, 'd');
                            // console.log(expDate);
                            $scope.events[i].Implant_Components__r[j].Expiration_Date__c = expDate;
                        }
                    }
                }
                if ($rootScope.event && $rootScope.event.Event_Type_Name__c === $scope.events[i].Event_Type_Name__c) {
                    $scope.event = $scope.events[i];
                    CaseDetailsFactory.setEvent($scope.event);
                }
            }
            //get survey events
            if ($scope.events.length > 0) {
                SurveyService.setEventTypes($scope.events);
            }
            //console.log($rootScope.event);
            if (!$rootScope.event) {
                $scope.event = $scope.procedure;
                // CaseDetailsFactory.setEvent($scope.event);
            }
            else {
                if (!$scope.event) {
                    //should not reach this
                    if ($rootScope.event) {
                        console.log($rootScope.event);
                        $scope.event = $rootScope.event;
                        // CaseDetailsFactory.setEvent($scope.event);
                    }
                    else {
                        // if there is no event passed in
                        if ($scope.events.length > 0) {
                            $scope.event = $scope.events[0];
                        }
                        else {
                            $state.go('patient-list');
                        }
                    }
                }
            }
            ctrl.event = $scope.event;
            CaseDetailsFactory.setEvent($scope.event);
            console.log($scope.event);
            setSurveyList($scope.event);
            LogFactory.logReadsObject($scope.event.Id, 'Event__c');
        }
        function refreshEvents() {
            return CaseDetailsFactory.LoadCaseEvents()
                .then(function (data) {
                console.log(data);
                $scope.events = data;
                loadCaseEvents();
            });
        }
        // $scope.escapeURL = function (urlString) {
        //     return urlString.replace(/&amp;/g, '&');
        // };
        function setSurveyList(event) {
            //console.log($scope.event.Id);
            SurveyService.retrieveSurveysByEventId(event.Id)
                .then(function (data) {
                $scope.surveys = data;
                console.log($scope.surveys);
                if (data.length > 0) {
                    SurveyService.setSurveys(data);
                    console.log(data);
                    if ($scope.surveys.length > 0) {
                        setSurveysAndScores();
                    }
                }
            });
        }
        // called at initialization - gets first survey
        function setSurveysAndScores() {
            //console.log($scope.surveys);
            if ($scope.surveys.length > 0) {
                var currentSurvey = SurveyService.getSurvey();
                if (currentSurvey !== undefined) {
                    var found = false;
                    for (var i = 0; i < $scope.surveys.length; i++) {
                        if ($scope.surveys[0].Name__c === currentSurvey.Name__c) {
                            //set it to the matching survey
                            $scope.currentSurvey = $scope.surveys[i];
                            found = true;
                        }
                    }
                    if (!found) {
                        // survey not found in current event so set it to the first survey
                        $scope.currentSurvey = $scope.surveys[0];
                    }
                }
                else {
                    //set it to the first survey
                    $scope.currentSurvey = $scope.surveys[0];
                }
                SurveyService.setSurvey($scope.currentSurvey);
                $scope.renderedSurvey = $scope.currentSurvey.Name__c;
                //canGraphSurvey = canGraph($scope.currentSurvey);
                loadSurveyScores($scope.case.Id, $scope.currentSurvey.Id);
            }
        }
        function loadSurveyScores(caseId, surveyId) {
            console.log(caseId + ' ' + surveyId);
            var survey = SurveyService.getSurveyById(surveyId)
                .then(function (data) {
                survey = data;
                SurveyService.setSurvey(survey);
                //if ($scope.chartCategories) {
                CaseDetailsFactory.loadSurveyScores(caseId, surveyId)
                    .then(function (scores) {
                    console.log(scores);
                    $scope.showGraph = (scores.length > 0);
                    console.log($scope.showGraph);
                    if ($scope.showGraph) {
                        $scope.chartCategories = SurveyService.getChartCategories();
                        $scope.chartData = SurveyService.getChartData(scores);
                        console.log($scope.chartData);
                        $scope.chartGridLines = c3.generate(SurveyService.setGraphParameters('#kss-chart', $scope.chartData));
                    }
                    //$scope.chartGridLines.select($scope.questions, [$scope.chartCategories.indexOf($scope.event.Event_Type_Name__c)], true);
                }, function (error) {
                });
                //}
            });
        }
        $rootScope.expDateformat = function (milliseconds) {
            if (!milliseconds) {
                return '--';
            }
            else {
                return moment.utc(milliseconds).format('MM/YYYY');
            }
        };
        function toggleOutcomes(viewID) {
            if ($scope.toggleValue === 'data') {
                $scope.toggleValue = 'chart';
            }
            else {
                $scope.toggleValue = 'data';
            }
        }
        ;
        function switchEvent(event) {
            console.log(event);
            $scope.event = event;
            $rootScope.event = event;
            CaseDetailsFactory.setEvent($scope.event);
            setSurveyList(event);
            // if ($scope.surveys.length > 0) {
            //     setSurveysAndScores();
            // }    
            if ($scope.chartGridLines) {
                $scope.chartGridLines.select($scope.questions, [$scope.chartCategories.indexOf($scope.event.Event_Type_Name__c)], true);
            }
            LogFactory.logReadsObject(event.Id, 'Event__c');
        }
        $scope.patientDetailsVisible = false;
        function togglePatientDetailsDrawer() {
            $scope.patientDetailsVisible = !$scope.patientDetailsVisible;
        }
        $scope.open = { expDate: false };
        $scope.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.open[whichDate] = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };
        $scope.expYears = [{ "key": "null", "value": "Exp. Year" }];
        var thisYear2 = Number(new Date().getFullYear());
        var yearRange2 = 11;
        for (var i2 = 1; i2 <= yearRange2; i2++) {
            // console.log('thisYear ' +  thisYear2);
            var element = {};
            element.key = thisYear2;
            element.value = thisYear2;
            $scope.expYears.push(element);
            thisYear2 = thisYear2 + 1;
        }
        $scope.year = 'null';
        $scope.syear = 'null';
        $scope.expMonths = [
            {
                "key": "null",
                "value": "Exp. Month"
            },
            {
                "key": 1,
                "value": "01 - Jan"
            },
            {
                "key": 2,
                "value": "02 - Feb"
            },
            {
                "key": 3,
                "value": "03 - Mar"
            },
            {
                "key": 4,
                "value": "04 - Apr"
            },
            {
                "key": 5,
                "value": "05 - May"
            },
            {
                "key": 6,
                "value": "06 - Jun"
            },
            {
                "key": 7,
                "value": "07 - Jul"
            },
            {
                "key": 8,
                "value": "08 - Aug"
            },
            {
                "key": 9,
                "value": "09 - Sep"
            },
            {
                "key": 10,
                "value": "10 - Oct"
            },
            {
                "key": 11,
                "value": "11 - Nov"
            },
            {
                "key": 12,
                "value": "12 - Dec"
            }
        ];
        $scope.month = 'null';
        $scope.smonth = 'null';
        function editPatient() {
            //PatientService.setPatient(patient);
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }
        function refreshEventsAndSurveys() {
            var l = Ladda.create(document.querySelector('.refresh-btn'));
            l.start();
            CaseDetailsFactory.LoadCaseEvents().then(function (data) {
                //console.log(data);
                $scope.events = data;
                loadCaseEvents();
                loadSurveyScores($scope.case.Id, $scope.surveys[0].Id);
                Ladda.stopAll();
            }, function (error) {
            });
            Ladda.stopAll();
        }
        ;
        //Implants
        function loadProducts() {
            CaseDetailsFactory.LoadProducts().then(function (data) {
                //console.log(data);
                $scope.products = data;
            }, function (error) {
            });
        }
        $scope.onSelectProduct = function ($item, $model, $label) {
            $scope.selectedProduct = $item;
        };
        function checkAndSaveImplantBarcode() {
            console.log($scope.implantEntryType);
            if ($scope.implantEntryType === 'barcode') {
                console.log($scope.firstbar);
                if ((angular.isDefined($scope.firstbar) && $scope.firstbar.indexOf('/') !== -1) || (angular.isDefined($scope.firstbar) && $scope.firstbar.length > 25)) {
                    $scope.secondbar = '';
                    $scope.month = '';
                    $scope.year = '';
                    $scope.addImplant();
                }
                else if (angular.isDefined($scope.firstbar) && $scope.firstbar.length > 0 &&
                    angular.isDefined($scope.secondbar) && $scope.secondbar.length > 0 &&
                    angular.isDefined($scope.month) && $scope.month != null &&
                    angular.isDefined($scope.year) && $scope.year != null) {
                    $scope.addImplant();
                }
                else {
                    Ladda.stopAll();
                }
            }
            else if ($scope.implantEntryType === 'manual') {
                addImplant();
            }
        }
        function addImplant() {
            console.log('called?');
            var l = Ladda.create(document.querySelector('.implant-btn'));
            l.start();
            if ($scope.implantEntryType === 'barcode') {
                CaseDetailsFactory.AddImplant($scope.firstbar, $scope.secondbar, $scope.month, $scope.year, $scope.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    $scope.firstbar = '';
                    $scope.secondbar = '';
                    $scope.month = 'null';
                    $scope.year = 'null';
                    angular.element(document.querySelector('#firstbar'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
            else if ($scope.implantEntryType === 'manual') {
                CaseDetailsFactory.AddImplantManually($scope.selectedProduct.Id, $scope.lotNumber, $scope.month, $scope.year, $scope.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    $scope.selectedProduct = '';
                    $scope.lotNumber = '';
                    $scope.month = 'null';
                    $scope.year = 'null';
                    angular.element(document.querySelector('#ProductNumber'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
        }
        $scope.isEnterKeyPressed = function (event) {
            if (event.keyCode === 13) {
                angular.element(document.querySelector('#secondbar'))[0].focus();
            }
        };
        $scope.deleteImplant = function (implant) {
            $confirm({ text: 'Are you sure you want to delete this implant (' + implant.Product_Catalog_Id__r.Name + ')?', title: 'Delete Implant' })
                .then(function () {
                CaseDetailsFactory.DeleteImplant(implant.Id).then(function (data) {
                    refreshEvents();
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        };
        $scope.isEnterKeyPressedSensor = function (event) {
            if (event.keyCode === 13) {
                $scope.addSensor();
            }
        };
        //Sensors
        function loadSensorRefs() {
            CaseDetailsFactory.loadSensorRefs().then(function (data) {
                //console.log(data);
                $scope.sensorRefs = data;
            }, function (error) {
            });
        }
        $scope.onSelectRef = function ($item, $model, $label) {
            $scope.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        };
        function addSensor() {
            if ($scope.sensorCode !== null) {
                var l = Ladda.create(document.querySelector('.sensor-btn'));
                console.log(l);
                l.start();
                if ($scope.sensorEntryType === 'barcode') {
                    SensorService.AddSensor($scope.sensorCode, $scope.procedure.Id)
                        .then(function (data) {
                        console.log('Data is retrieved');
                        console.log(data);
                        if (data === 'Successful') {
                            refreshEvents();
                            $scope.sensorRefs = sensorRefs;
                            $scope.sensorCode = '';
                            angular.element(document.querySelector('#sensorCode'))[0].focus();
                        }
                        else {
                            alert(data);
                        }
                        Ladda.stopAll();
                    }, function (error) {
                        Ladda.stopAll();
                    });
                }
                else {
                    if ($scope.sensorForm.$valid) {
                        SensorService.AddSensorManually($scope.selectedRef.Name, $scope.selectedRef.Manufacturer__c, $scope.serialNumber, $scope.slotNumber, $scope.smonth, $scope.syear, $scope.procedure.Id)
                            .then(function (data) {
                            console.log(data);
                            refreshEvents();
                            $scope.sensorRefs = sensorRefs;
                            $scope.sensorForm.submitted = false;
                            $scope.selectedRef = '';
                            $scope.serialNumber = '';
                            $scope.slotNumber = '';
                            $scope.smonth = 'null';
                            $scope.syear = 'null';
                            Ladda.stopAll();
                        }, function (error) {
                            Ladda.stopAll();
                        });
                    }
                    else {
                        Ladda.stopAll();
                        $scope.sensorForm.submitted = true;
                    }
                }
            }
        }
        function deleteSensor(device) {
            $confirm({ text: 'Are you sure you want to delete this sensor (' + device.OS_Device__r.Device_ID__c + ')?', title: 'Delete Sensor' })
                .then(function () {
                CaseDetailsFactory.DeleteSensor(device.OS_Device__r.Id, $scope.procedure.Id).then(function (data) {
                    refreshEvents();
                    //console.log(data);
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
        ;
        //case note methods        
        function addNewCaseNote(procedureId) {
            $uibModal.open({
                templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_caseAddNewNote',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory) {
                    $scope.procedureId = procedureId;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.addNote = function () {
                        CaseDetailsFactory.AddNote($scope.caseTitle, $scope.caseNote, $scope.procedureId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        ;
        function deleteNote(note) {
            $confirm({ text: 'Are you sure you want to delete this note?', title: 'Delete Note' })
                .then(function () {
                CaseDetailsFactory.DeleteNote(note.Id).then(function (data) {
                    //console.log(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
        ;
        function editNote(note) {
            $uibModal.open({
                templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_caseUpdateNote',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory) {
                    $scope.caseTitle = note.Title;
                    $scope.caseNote = note.Body;
                    $scope.noteId = note.Id;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.updateNote = function () {
                        CaseDetailsFactory.UpdateNote($scope.caseTitle, $scope.caseNote, $scope.noteId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        ;
        function surveyChange(surveyName) {
            //canGraphSurvey = canGraph($scope.renderedSurvey);
            var survey = SurveyService.getSurveyByName($scope.renderedSurvey)
                .then(function (data) {
                survey = data;
                SurveyService.setSurvey(survey);
                //$scope.chartCategories = SurveyService.getChartCategories();
                console.log(survey);
                // $scope.surveyGraphable = canGraph(survey);
                if (survey.id !== undefined) {
                    loadSurveyScores($scope.case.Id, survey.Id);
                }
                else {
                    $scope.showGraph = false;
                }
            });
        }
        function canGraph(survey) {
            //console.log(survey.Can_Graph__c);
            var graphable = survey.Can_Graph__c;
            if (!graphable) {
                $scope.toggleValue = 'data';
            }
            return graphable;
        }
    }
})();
//# sourceMappingURL=caseDetail.controller.js.map