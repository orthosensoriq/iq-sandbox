module orthosensor.domains {
   
    export class KneeBalanceLoads {
        public id?: string;
        public patientId?: string;
        public patientName?: string
        public procedureDate?: Date;
        public procedureDateString?: String;
        public practiceId?: string;
        public practiceName?: string;
        public surgeonId?: string;
        public surgeonName?: string;
        public medial10?: number;
        public medial45?: number;
        public medial90?: number;
        public lateral10?: number;
        public lateral45?: number;
        public lateral90?: number;
        constructor(
        ) { }
    }
}