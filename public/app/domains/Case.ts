module orthosensor.domains {
    // import { ICaseType } from './caseType';
    // import { IPhysician } from './physician';
    // import { Hospital } from './hospital';

    export interface ICase {
        id: string;
        caseType: orthosensor.domains.ICaseType;
        laterality: string;
        physician: orthosensor.domains.IPhysician;
        procedure_Date: Date;
        hospital: Hospital;
    }
    export class Case implements ICase {
        constructor(
            public id: string,
            public caseType: orthosensor.domains.ICaseType,
            public laterality: string,
            public physician: orthosensor.domains.IPhysician,
            public procedure_Date: Date,
            public hospital: Hospital
        ) {

        }
    }
}