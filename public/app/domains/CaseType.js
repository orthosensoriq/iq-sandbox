var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var CaseType = (function () {
            function CaseType(id, description) {
                this.id = id;
                this.description = description;
            }
            return CaseType;
        }());
        domains.CaseType = CaseType;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=CaseType.js.map