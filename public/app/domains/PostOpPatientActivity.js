var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PostOpPatientActivity = (function () {
            function PostOpPatientActivity(id, lastName, firstName, days, climbed, walked, last, rangeOfMotion, postOperativeStatus, ptEconomic) {
                this.id = id;
                this.lastName = lastName;
                this.firstName = firstName;
                this.days = days;
                this.climbed = climbed;
                this.walked = walked;
                this.last = last;
                this.rangeOfMotion = rangeOfMotion;
                this.postOperativeStatus = postOperativeStatus;
                this.ptEconomic = ptEconomic;
            }
            return PostOpPatientActivity;
        }());
        domains.PostOpPatientActivity = PostOpPatientActivity;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=PostOpPatientActivity.js.map