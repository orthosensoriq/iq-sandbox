module orthosensor.domains {
    export
        class PatientsOutOfCompliance {
        patientName: string;
        phaseName?: string;
        lastContactDate?: Date;
        issue?: string;
        constructor() { } 
    }
}