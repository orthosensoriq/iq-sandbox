module orthosensor.domains {
    export class ChartType {
        public data: ChartDataElement[];
        public options: ChartOptions;
        public type: string;
    }

    export class ChartOptions {
        public title: string;
        public legend: string;
        public displayExactValues: boolean;
        public fill: number;
        public animation: {};
        public tooltip: {};
        public is3D: boolean;
        public chartArea: {};
        public hAxis: {};
        constructor() {}
    }
}
