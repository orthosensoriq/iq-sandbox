module orthosensor.domains {
    export class ChartDataElement {
        public label: string;
        public value: any;
        constructor() { }
    }
}
