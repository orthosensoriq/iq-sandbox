var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Physician = (function () {
            function Physician(id, name) {
                this.id = id;
                this.name = name;
            }
            return Physician;
        }());
        domains.Physician = Physician;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Physician.js.map