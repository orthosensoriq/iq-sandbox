module orthosensor.domains {
    
    export class Surgeon {
        public accountParentId?: string;
        public accountParentName?: string;
        constructor(
            public id: string,
            public name: string,
            public parentId: string,
            public parentName: string,
        ) {

        }
    }
}

