module orthosensor.domains {

    export interface IAccount {
        id: string;
        name: string;
        recordTypeName: string;
        parentId: string;
        //  Profile.Name
    }
    export class Account implements IAccount {
        constructor(
            public id: string,
            public name: string,
            public recordTypeName: string,
            public parentId: string) {

        }
    }
}