module orthosensor.domains {

    export class PatientKoosScore {
        public id: string;
        public surgeonId: string;
        public practiceId: string;
        public surgeonName: string;
        public practiceName: string;
        public patientName: string;
        public patientId: string;
       
        public phase: string;
        public procedureDate: Date;
        public symptom: number;
        public pain: number;
        public aDL: number;
        public sportRec: number;
        public qOL: number;
        constructor(
        ) { }
    }
}