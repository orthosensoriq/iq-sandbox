var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { IContact } from './contact';
        var UserProfile;
        (function (UserProfile) {
            UserProfile[UserProfile["SystemAdmin"] = 0] = "SystemAdmin";
            UserProfile[UserProfile["HospitalAdmin"] = 1] = "HospitalAdmin";
            UserProfile[UserProfile["Surgeon"] = 2] = "Surgeon";
            UserProfile[UserProfile["PracticeAdmin"] = 3] = "PracticeAdmin";
        })(UserProfile = domains.UserProfile || (domains.UserProfile = {}));
        var User = (function () {
            function User() {
            }
            return User;
        }());
        domains.User = User;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=User.js.map