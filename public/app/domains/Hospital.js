var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Hospital = (function () {
            function Hospital(id, name, recordTypeId) {
                this.id = id;
                this.name = name;
                this.recordTypeId = recordTypeId;
            }
            return Hospital;
        }());
        domains.Hospital = Hospital;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Hospital.js.map