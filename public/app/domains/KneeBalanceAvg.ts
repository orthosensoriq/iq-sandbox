module orthosensor.domains {
   
    export class KneeBalanceAvg {
        public id: string;
        public surgeonId: string;
        public startDate: Date;
        public startDateString: String;
        public practiceId: string;
        public medial10: number;
        public medial45: number;
        public medial90: number;
        public lateral10: number;
        public lateral45: number;
        public lateral90: number;
        constructor(
        ) { }
    }
}