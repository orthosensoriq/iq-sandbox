var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Account = (function () {
            function Account(id, name, recordTypeName, parentId) {
                this.id = id;
                this.name = name;
                this.recordTypeName = recordTypeName;
                this.parentId = parentId;
            }
            return Account;
        }());
        domains.Account = Account;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Account.js.map