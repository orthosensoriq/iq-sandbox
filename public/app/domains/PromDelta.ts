module orthosensor.domains {

    export class PromDelta {
        public id: string;
        public surgeonId: string;
        public practiceId: string;
        public surgeonName: string;
        public practiceName: string;
        public patientName: string;
        public patientId: string;
        public section: string;
        public stage: string;
        public procedureDate: Date;
        public score: number;
        constructor(
        ) { }
    }
}