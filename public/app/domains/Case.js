var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { ICaseType } from './caseType';
        // import { IPhysician } from './physician';
        // import { Hospital } from './hospital';
        var Case = (function () {
            function Case(id, caseType, laterality, physician, procedure_Date, hospital) {
                this.id = id;
                this.caseType = caseType;
                this.laterality = laterality;
                this.physician = physician;
                this.procedure_Date = procedure_Date;
                this.hospital = hospital;
            }
            return Case;
        }());
        domains.Case = Case;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Case.js.map