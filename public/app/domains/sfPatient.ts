module orthosensor.domains {
    export class sfPatient {
        Id?: string;
        Anonymous__c?: Boolean;
        Hl7__c?: Boolean;
        HospitalId__c?: string;
        hospital?: any;
        Last_Name__c?: string;
        First_Name__c?: string;
        practice?: any;
        Anonymous_Label__c?: string;
        Medical_Record_Number__c?: string;
        Email__c?: string;
        Date_Of_Birth__c?: Date;
        Anonymous_Year_Of_Birth__c?: string;
        Gender__c?: string;
        Social_Security_Number__c?: string;
        Patient_Number__c?: string;
        Source_Record_Id__c?: string;
        Account_Number__c?: string;
        Race__c?: string;
        Language__c?: string;

        constructor(

        ) {

        }
    }

}