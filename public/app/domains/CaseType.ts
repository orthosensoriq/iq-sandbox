module orthosensor.domains {
    export interface ICaseType {
        id: string,
        description: string
    }
    export class CaseType implements ICaseType {
        constructor(
            public id: string,
            public description: string

        ) {

        }
    }
}    
