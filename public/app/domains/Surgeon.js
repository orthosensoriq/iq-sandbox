var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Surgeon = (function () {
            function Surgeon(id, name, parentId, parentName) {
                this.id = id;
                this.name = name;
                this.parentId = parentId;
                this.parentName = parentName;
            }
            return Surgeon;
        }());
        domains.Surgeon = Surgeon;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Surgeon.js.map