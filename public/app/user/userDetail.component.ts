
angular
    .module("orthosensor")
    .component("userDetail", {
        bindings: {
            "user": "<",
        },
        controller: "UserDetailCtrl",
        templateUrl: "/app/user/userDetail.html",
    });

