module orthosensor.domains {
    // import { IUser } from '../domains/user';

    export class UserListCtrl {
        public title: string = "UserList";
        public user: orthosensor.domains.IUser;
        public users: orthosensor.domains.IUser[];
        public people: orthosensor.domains.IUser[] = [
            {
                "id": '9087',
                "lastName": "Schwartz",
                "firstName": "Moses",
            },
            {
                "id": '9088',
                "lastName": "Mertz",
                "firstName": "Fred",
            },
            {
                "id": '9089',
                "lastName": "Smith",
                "firstName": "Missy",
            },
        ];
        constructor() {
            
            // let newUser = new orthosensor.domain.User("1002", "Johnson", "102938",
            //     new orthosensor.domain.Contact("8298", "Contact Name", "90912"));
            // let newUser2 = new orthosensor.domain.User("1003", "Honest Joe", "103238",
            //     new orthosensor.domain.Contact("8292", "Contact Name2", "903222"));

            // this.users = [newUser];
            // this.users.push(newUser2);
        }
        public editRecord(user: any[]): void {
            //populate the detail
            //console.log(user.l.lastName);
        }
    }
    angular
        .module("app")
        .controller("UserListCtrl", UserListCtrl);
}
