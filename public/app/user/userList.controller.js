var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { IUser } from '../domains/user';
        var UserListCtrl = (function () {
            function UserListCtrl() {
                // let newUser = new orthosensor.domain.User("1002", "Johnson", "102938",
                //     new orthosensor.domain.Contact("8298", "Contact Name", "90912"));
                // let newUser2 = new orthosensor.domain.User("1003", "Honest Joe", "103238",
                //     new orthosensor.domain.Contact("8292", "Contact Name2", "903222"));
                this.title = "UserList";
                this.people = [
                    {
                        "id": '9087',
                        "lastName": "Schwartz",
                        "firstName": "Moses",
                    },
                    {
                        "id": '9088',
                        "lastName": "Mertz",
                        "firstName": "Fred",
                    },
                    {
                        "id": '9089',
                        "lastName": "Smith",
                        "firstName": "Missy",
                    },
                ];
                // this.users = [newUser];
                // this.users.push(newUser2);
            }
            UserListCtrl.prototype.editRecord = function (user) {
                //populate the detail
                //console.log(user.l.lastName);
            };
            return UserListCtrl;
        }());
        domains.UserListCtrl = UserListCtrl;
        angular
            .module("app")
            .controller("UserListCtrl", UserListCtrl);
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=userList.controller.js.map