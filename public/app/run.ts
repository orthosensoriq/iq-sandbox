// import { UserService } from './services/UserService';
(function () {
    angular
        .module('orthosensor')
        .run(runBlock);
    runBlock.$inject = ['$state', '$rootScope', '$timeout', 'InitFactory', '$window', 'UserService', 'DashboardService', 'PatientListFactory', 'SFDataService', 'HospitalService'];
    function runBlock($state, $rootScope, $timeout, InitFactory, $window,
        UserService: orthosensor.services.UserService,
        DashboardService: orthosensor.services.DashboardService,
        PatientListFactory: any,
        SFDataService: orthosensor.services.SFDataService,
        HospitalService: orthosensor.services.HospitalService) {

        $rootScope.handleSessionTimeout = function (event) {
            if (!event.status && event.statusCode === 500) {
                $window.location = '/login';
            }
        };
        
        InitFactory.isUserAdmin()
            .then((data: any) => {
                // console.log(data);
                $rootScope.isUserAdmin = data;
                UserService.isAdmin = data;

            }, (error: any) => {
            });

        InitFactory.getCurrentUser()
            .then((data: any) => {
                // console.log(data);
                $rootScope.currentUser = data;
                let sfUser = data;
                let user = UserService.convertSFToObject(sfUser);
                UserService.user = user;
                console.log(user);
                console.log(UserService.user);
                // let startPage = 'dashboard';
                let startPage = 'patientList';
                let hospitalPracticeId: string = '';
                DashboardService.setSurveys();
                // change if we get user info!
                // $timeout(() => {
                switch (UserService.user.userProfile) {
                    case orthosensor.domains.UserProfile.Surgeon:
                        console.log('surgeon Account');
                        return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                            .then(() => {
                                console.log(HospitalService.surgeons);
                            })
                            .then(() => {
                                HospitalService.loadHospitalByPractice(UserService.user.accountId);
                                console.log(HospitalService.practices);
                            })
                            .then(() => {
                                startPage = 'dashboard';
                                $state.go(startPage);
                            });
                    // break;

                    case orthosensor.domains.UserProfile.HospitalAdmin:
                        hospitalPracticeId = user.accountId;
                        console.log('hospital Account');
                        HospitalService.setPractices(hospitalPracticeId);
                        return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                            .then(() => {
                                console.log(HospitalService.surgeons);
                            })
                            // .then(() => {
                            //     HospitalService.loadHospitalByPractice(UserService.user.accountId);
                            //     console.log(HospitalService.practices);
                            // })
                            .then(() => {
                                startPage = 'dashboard';
                                $state.go(startPage);
                            });
                    // break;

                    case orthosensor.domains.UserProfile.PracticeAdmin:
                        hospitalPracticeId = user.accountId;
                        // console.log('practice Account');
                        // DashboardService.loadHospitalPractices(hospitalPracticeId);
                        return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                            .then(() => {
                                console.log(HospitalService.surgeons);
                            })
                            .then(() => {
                                HospitalService.loadHospitalByPractice(UserService.user.accountId);
                                console.log(HospitalService.practices);
                            })
                            .then(() => {
                                startPage = 'patientList';
                                $state.go(startPage);
                            });
                    // case 'Customer Community User':
                    //     hospitalPracticeId = user.accountId;
                    //     // console.log('practice Account');
                    //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                    //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                    //         .then(() => {
                    //             console.log(HospitalService.surgeons);
                    //         })
                    //         .then(() => {
                    //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                    //             console.log(HospitalService.practices);
                    //         })
                    //         .then(() => {
                    //             startPage = 'patientList';
                    //             $state.go(startPage);
                    //         });
                        
                    // case 'Customer Community Plus User':
                    //     hospitalPracticeId = user.accountId;
                    //     // console.log('practice Account');
                    //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                    //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                    //         .then(() => {
                    //             console.log(HospitalService.surgeons);
                    //         })
                    //         .then(() => {
                    //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                    //             console.log(HospitalService.practices);
                    //         })
                    //         .then(() => {
                    //             startPage = 'patientList';
                    //             $state.go(startPage);
                    //         });
                    // break;

                    default:
                        // console.log(DashboardService.hospitals);
                        // DashboardService.loadPractices(hospitalPracticeId);

                        HospitalService.getHospitals()
                            .then((data: any) => {
                                // console.log(data);
                                SFDataService.loadHospitalPractices(HospitalService.hospitals[0].id)
                                    .then((data: any) => {
                                        // console.log(data);
                                        HospitalService.practices = HospitalService.convertSFPracticeToObject(data);
                                        HospitalService.loadAllSurgeons()
                                            .then(() => {
                                                console.log(HospitalService.surgeons);
                                                startPage = 'dashboard';
                                                $state.go(startPage);
                                            });
                                    }, (error) => {
                                        console.log(error);
                                    });

                            });
                    // }, 2000)
                    //     .then(() => {
                    //         // $state.go(startPage);
                    // });
                }
            });

        // TODO: move this to a service at some point!
        $rootScope.dateformat = dateformat;
        function dateformat(milliseconds: number) {
            console.log('- using date format in run js!!! -');
            if (!milliseconds) {
                console.log('Not milliseconds');
                return '--';
            } else {
                let dateValue = moment.utc(milliseconds).format('LL');
                console.log(dateValue);
                return dateValue;
            }
        }
    }
})();
