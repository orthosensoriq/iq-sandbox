var orthosensor;
(function (orthosensor) {
    var ShellCtrl = (function () {
        function ShellCtrl($rootScope, UserService) {
            this.$rootScope = $rootScope;
            this.UserService = UserService;
            this.title = 'Orthosensor IQ Playground';
            this.name = $rootScope.currentUser;
            this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            this.sitePrefix = $rootScope.sitePrefix;
            // console.log($rootScope.sitePrefix);
            this.logoutCall = $rootScope.sitePrefix + '/secur/logout.jsp';
        }
        ShellCtrl.prototype.$onInit = function () {
            if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                this.systemMode = 'production';
            }
            else {
                this.systemMode = this.$rootScope.systemMode;
            }
            this.messages = 2;
            this.user = this.UserService.user;
            console.log(this.user);
        };
        return ShellCtrl;
    }());
    ShellCtrl.$inject = ['$rootScope', 'UserService'];
    orthosensor.ShellCtrl = ShellCtrl;
    angular
        .module('orthosensor')
        .controller('ShellCtrl', ShellCtrl);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=shell.js.map