﻿((): void => { 
    'use strict';
    
    var controllerId = 'sidebar';

    function sidebar($route: any, config: any, routes: any) {
        var vm = this;
        var title: string;
        vm.isCurrent = isCurrent;

        activate();

        function activate() { getNavRoutes(); }
        
        function getNavRoutes() {
            vm.navRoutes = routes.filter((r:any) => {
                return r.config.settings && r.config.settings.nav;
            }).sort((r1: any, r2:any) => {
                return r1.config.settings.nav - r2.config.settings.nav;
            });
        }
        
        function isCurrent(route: any) {
            if (!route.config.title || !$route.current || !$route.current.title) {
                return '';
            }
            var menuName = route.config.title;
            return $route.current.title.substr(0, menuName.length) === menuName ? 'current' : '';
        }
    angular.module('app').controller(controllerId,
        ['$route', 'config', 'routes', sidebar]);

    };
})();
