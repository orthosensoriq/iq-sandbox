(function () {
    'use strict';
    // Usage: this displays hamburger style menu in the upper left header
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osTopMenu', {
        templateUrl: 'app/layout/topMenu.component.html',
        //templateUrl: 'templateUrl',
        controller: topMenuController,
        bindings: {
            title: '@'
        }
    });
    topMenuController.$inject = ['$rootScope'];
    function topMenuController($rootScope) {
        /*jshint validthis: true */
        var $ctrl = this;
        console.log($rootScope.systemMode);
        if ($rootScope.systemMode === undefined || $rootScope.systemMode === null) {
            $ctrl.systemMode = 'production';
        }
        else {
            $ctrl.systemMode = $rootScope.systemMode;
        }
        ////////////////
        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };
    }
})();
//# sourceMappingURL=topMenu.component.js.map