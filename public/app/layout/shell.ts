module orthosensor {

    export class ShellCtrl {
        public static $inject: Array<string> = ['$rootScope', 'UserService'];

        public title: string;
        public name: string;
        public logo: any;
        public sitePrefix: string;
        // console.log($rootScope.sitePrefix);
        public logoutCall: string;
        public systemMode: string;

        public messages: number;
        public user: orthosensor.domains.User;

        constructor(public $rootScope: ng.IRootScopeService, public UserService: orthosensor.services.UserService) {
            this.title = 'Orthosensor IQ Playground';
            this.name = $rootScope.currentUser;
            this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            this.sitePrefix = $rootScope.sitePrefix;
            // console.log($rootScope.sitePrefix);
            this.logoutCall = $rootScope.sitePrefix + '/secur/logout.jsp';
            
        }
        $onInit() {
            if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                this.systemMode = 'production';
            } else {
                this.systemMode = this.$rootScope.systemMode;
            }
            this.messages = 2;
            this.user = this.UserService.user;
            console.log(this.user);
        }
    }
    angular
        .module('orthosensor')
        .controller('ShellCtrl', ShellCtrl);
}
