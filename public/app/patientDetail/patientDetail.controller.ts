(function () {

    function PatientDetailsController($q, $rootScope, $state, $uibModal, PatientDetailsFactory, CaseDetailsFactory, LogFactory, SurveyService, PatientService, CaseService) {
        //vars
        var $ctrl = this;
        //$ctrl.patient = $rootScope.patient;
        $ctrl.patient = PatientService.getSFObject();
        $ctrl.surveyGraphable = true;
        $ctrl.toggleValue = 'chart';
        $ctrl.patientDetailsVisible = false;

        //methods
        $ctrl.surveyChange = surveyChange;
        $ctrl.canGraph = canGraph;
        $ctrl.addPatientCase = addPatientCase;
        //ctrl.editPatient = editPatient;
        $ctrl.completeEvent = completeEvent;
        $ctrl.caseDetails = caseDetails;
        $ctrl.caseCount = 0;
        $ctrl.showGraph = false;
        // $ctrl.togglePatientDetailsDrawer = togglePatientDetailsDrawer;

        //////////
        LogFactory.logReadsObject($ctrl.patient.Id, 'Patient__c');
        $q.all([
            PatientDetailsFactory.LoadCasesAndEvents()
                .then(function (data) {
                    console.log(data);
                    $ctrl.cases = data;
                    $ctrl.caseCount = $ctrl.cases.length;
                    console.log($ctrl.caseCount);
                    for (var i = 0; i < $ctrl.cases.length; i++) {
                        LogFactory.logReadsObject($ctrl.cases[i].Id, 'Case__c');

                    }
                    if ($ctrl.cases.length > 0) {
                        SurveyService.setEventTypes($ctrl.cases[0].Events__r);
                    }
                }, function (error) {
                })])
            .then(function () {
                if ($ctrl.cases.length > 0) {
                    SurveyService.loadSurveysFromCaseType($ctrl.cases[0].Id).then(function (data) {
                        $ctrl.surveys = data;
                        console.log(data);
                        if ($ctrl.surveys.length > 0) {
                            $ctrl.renderedSurvey = $ctrl.surveys[0].Name__c;
                            $ctrl.surveyGraphable = canGraph($ctrl.surveys[0]);
                            loadDataBySurvey($ctrl.surveys[0]);
                        }
                    }, function (error) {
                    });
                }
            });
        function loadSurveyScores(caseId, surveyId) {
            console.log('SurveyId: ' + surveyId);
            console.log('CaseId: ' + caseId);
            SurveyService.setCaseId(caseId);
            let survey = SurveyService.getSurveyById(surveyId)
                .then((data) => {
                    survey = data;
                    console.log(data);
                    SurveyService.setSurvey(survey);
                    CaseDetailsFactory.loadSurveyScores(caseId, surveyId)
                        .then((scores) => {
                            console.log(scores);
                            $ctrl.showGraph = (scores.length > 0);
                            if ($ctrl.showGraph) {
                                $ctrl.chartCategories = SurveyService.getChartCategories();
                                console.log($ctrl.chartCategories);
                                $ctrl.chartData = SurveyService.getChartData(scores);
                                console.log($ctrl.chartData);
                            
                                //$ctrl.chartCategories = SurveyService.getEventTypes(surveyId);
                                $ctrl.chartGridLines = c3.generate(SurveyService.setGraphParameters('#' + caseId, $ctrl.chartData));
                            }    
                        }, function (error) {
                            //      });
                        });
                });
        }

        /// this was not used - created to show completion dates.
        function completeEvent(event) {
            var found = false;
            var today = new Date();
            if ($ctrl.eventsToComplete) {
                if ($ctrl.eventsToComplete.length > 0) {
                    for (var i = 0; i < $ctrl.eventsToComplete.length; i++) {
                        let complete = $ctrl.eventsToComplete[i];
                        // console.log(complete);
                        if (event.Event_Type_Name__c === $ctrl.eventsToComplete[i].Event__r.Event_Type_Name__c) {
                            console.log('Events match: ' + event.Event_Type_Name__c)
                            console.log(today)
                            // if its before today
                            if (complete.End_Date__c <= today) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }
            return found;
        }

        function addPatientCase(patient) {
            $state.go('addPatientCase');
            // $uibModal.open({
            //     templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
            //     controller: 'AddPatientCase',
            //     controllerAs: '$ctrl',
            //     size: 'small'
            // });
        }

        function caseDetails(patientCase, event) {
            $rootScope.case = patientCase;
            $rootScope.event = event;
            $state.go('caseDetails');
        }

        $ctrl.toggleOutcomes = function (viewID) {
            if ($ctrl.toggleValue === 'data') {
                $ctrl.toggleValue = 'chart';
            }
            else {
                $ctrl.toggleValue = 'data';
            }
        };
        function surveyChange(surveyName) {
            //console.log(patientCase);
            console.log(surveyName);
            //console.log($ctrl.renderedSurvey);
            var survey = SurveyService.getSurveyByName(surveyName)
                .then(function (data) {
                    survey = data;
                    SurveyService.setSurvey(survey);
                    $ctrl.chartCategories = SurveyService.getChartCategories();
                    console.log(survey);
                    $ctrl.surveyGraphable = canGraph(survey);
                    //LoadSurveyScores(patientCase.Id, survey.Id);
                    loadDataBySurvey(survey);
                });
        }

        //determine if survey can be graphed
        function canGraph(survey) {
            var graphable = survey.Can_Graph__c;
            //console.log(survey.Can_Graph__c)
            if (!graphable) {
                $ctrl.toggleValue = 'data';
            }
            return graphable;
        }
        function loadDataBySurvey(survey) {
            console.log('Loading Survey Data');
            if ($ctrl.cases.length > 0) {
                console.log($ctrl.cases.length);
                for (var i = 0; i < $ctrl.cases.length; i++) {
                    loadSurveyScores($ctrl.cases[i].Id, survey.Id);
                }
            }
        }
    }

    PatientDetailsController.$inject = ['$q', '$rootScope', '$state', '$uibModal', 'PatientDetailsFactory', 'CaseDetailsFactory', 'LogFactory', 'SurveyService', 'PatientService', 'CaseService'];

    angular
        .module("orthosensor")
        .controller('PatientDetailsController', PatientDetailsController);

})();
