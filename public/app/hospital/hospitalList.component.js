var app;
(function (app) {
    angular
        .module("app")
        .component("hospitalList", {
        template: "\n            <section>\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n            <div class=\"panel-title\">{{vm.title}}</div>\n        </div>\n\n        <div class=\"panel-body\">\n            <table class=\"table table-responsive table-striped\">\n                <thead>\n                    <tr>\n                        <th>Name</th>\n                        <th>ID</th>\n                        \n                    </tr>\n                    <tbody>\n                       <tr ng-repeat=\"hospital in vm.hospitals\" \n                            ui-sref=\"admin.hospitalDetail({hospital})\">\n                            <td>{{hospital.name}}</td>\n                            <td>{{hospital.id}}</td>\n                        \n                        </tr>\n                    </tbody>\n                </thead>\n            </table>\n        </div>\n</section>",
        bindings: {},
        controller: "HospitalsController",
        controllerAs: 'vm'
    });
})(app || (app = {}));
//# sourceMappingURL=hospitalList.component.js.map 
//# sourceMappingURL=hospitalList.component.js.map