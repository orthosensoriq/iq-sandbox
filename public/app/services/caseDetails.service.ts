(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('CaseDetailsFactory', CaseDetailsFactory);
    CaseDetailsFactory.$inject = ['$q', '$rootScope', '$http'];
    function CaseDetailsFactory($q, $rootScope, $http) {
        var event = {};
        var service = {
            LoadCaseEvents: LoadCaseEvents,
            LoadSurveys: LoadSurveys,
            loadSurveyScores: loadSurveyScores,
            CompleteEvent: CompleteEvent,
            UpdateProcedure: UpdateProcedure,
            AddSensor: AddSensor,
            DeleteSensor: DeleteSensor,
            LoadProducts: LoadProducts,
            loadSensorRefs: loadSensorRefs,
            AddImplantManually: AddImplantManually,
            AddImplant: AddImplant,
            DeleteImplant: DeleteImplant,
            AddNote: AddNote,
            UpdateNote: UpdateNote,
            DeleteNote: DeleteNote,
            DeleteSensorLine: DeleteSensorLine,
            LoadLinkStationsIDs: LoadLinkStationsIDs,
            CheckSoftwareVersion: CheckSoftwareVersion,
            GetImageList: GetImageList,
            SaveImageToRecord: SaveImageToRecord,
            MatchSensorDataAndAttachment: MatchSensorDataAndAttachment,
            loadSensorDataLines: loadSensorDataLines,
            getEvent: getEvent,
            setEvent: setEvent
        };
        return service;
        ////////////////
        function LoadCaseEvents() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadCaseEvents($rootScope.case.Id, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadSurveys() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSurveys(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSurveyScores(caseId, surveyId) {
            var deferred = $q.defer();
            //var questions = ['KOOS Symptoms', 'KOOS Pain', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS QOL'];
            var questions = 'Symptoms, Pain, ADL, Sport/Rec, QOL';
            if (!surveyId) {
                surveyId = 'a6M7A0000000AFeUAM';
            }
            IQ_PatientActions.LoadSurveyScores(caseId, surveyId, function (result, event) {
                //console.log('Case ID: ' + caseId);
                //console.log('Survey ID: ' + surveyId);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function CompleteEvent(eventId) {
            var deferred = $q.defer();
            IQ_PatientActions.CompleteEvent(eventId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function UpdateProcedure(procedureData) {
            let deferred = $q.defer();
            let procedureDateString = moment.utc(procedureData.procedureDateString).format('MM/DD/YYYY');
            let patientConsentObtained = procedureData.patientConsentObtained;
            if (procedureData.patientConsentObtained === 1) {
                patientConsentObtained = true;
            }
            IQ_PatientActions.UpdateProcedure(procedureData.Id, procedureData.caseId, procedureData.physician, procedureData.laterality, procedureDateString, patientConsentObtained, procedureData.anesthesiaType, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddSensor(barcode, procedureId) {
            var deferred = $q.defer();
            IQ_CaseActions.AddSensor(barcode, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteSensor(deviceId, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteSensor(deviceId, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadProducts() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadProducts(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSensorRefs() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSensorRefs(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddImplantManually(productId, lotNumber, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddImplantManually(productId, lotNumber, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddImplant(firstbarcode, secondbarcode, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddImplant(firstbarcode, secondbarcode, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteImplant(implantId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteImplant(implantId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddNote(title, text, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddNote(title, text, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function UpdateNote(title, text, noteId) {
            var deferred = $q.defer();
            IQ_PatientActions.UpdateNote(title, text, noteId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteNote(noteId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteNote(noteId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteSensorLine(line, procedureID) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteSensorLine(line.SD_Type_MedialLoadID, line.SD_Type_LateralLoadID, line.SD_Type_RotationID, line.SD_Type_ActualFlexID, line.SD_Type_APID, line.SD_Type_HKAID, line.SD_Type_TibiaRotID, line.SD_Type_TibiaTiltID, procedureID, function (result, event) {
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadLinkStationsIDs(hospitalId) {
            ////
            var deferred = $q.defer();
            IQ_PatientActions.LoadLinkStationsIDs(hospitalId, function (result, event) {
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function CheckSoftwareVersion(procedureId, linkStationId, laterality) {
            var deferred = $q.defer();
            IQ_PatientActions.CheckSoftwareVersion(procedureId, linkStationId, laterality, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function GetImageList(procedureId, osDeviceId, linkStationId) {
            var deferred = $q.defer();
            IQ_PatientActions.GetImageList(procedureId, osDeviceId, linkStationId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function SaveImageToRecord(recordId, imageURL) {
            var deferred = $q.defer();
            IQ_PatientActions.SaveImageToRecord(recordId, imageURL, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function MatchSensorDataAndAttachment(recordId) {
            var deferred = $q.defer();
            IQ_PatientActions.MatchSensorDataAndAttachment(recordId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSensorDataLines(procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSensorDataLines(procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getEvent() {
            return event;
        }
        function setEvent(_event) {
            event = _event;
            //console.log(event);
        }
        // factory.LoadSurveysFromCaseType = function (caseId) {
        //     var deferred = $q.defer();
        //     IQ_SurveyRepository.LoadSurveysFromCaseType(
        //         caseId,
        //         function (result, event) {
        //             //console.log(result);
        //             $rootScope.$apply(function () {
        //                 if (event.status) {
        //                     deferred.resolve(result);
        //                 } else {
        //                     $rootScope.handleSessionTimeout(event);
        //                     deferred.reject(event);
        //                 }
        //             })
        //         }, { buffer: false, escape: true, timeout: 120000 });
        //     return deferred.promise;
        // }
    }
})();
