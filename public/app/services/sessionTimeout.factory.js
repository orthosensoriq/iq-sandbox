(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('sessionTimeoutHandler', sessionTimeoutHandler);
    sessionTimeoutHandler.$inject = ['$q', '$injector', '$window'];
    function sessionTimeoutHandler($q, $injector, $window) {
        var sessionHandler = {
            responseError: function (response) {
                // Session has expired
                if (response.status === 500 || response.status === 404 || response.status === 401) {
                    $state.go('dashboard');
                }
                return $q.reject(response);
            }
        };
        return sessionHandler;
    }
    //     var service = {
    //         sessionHandler: sessionHandler
    //     };
    //     function sessionHandler(response) {
    //         // Session has expired
    //         if (response.status == 500 || response.status == 404 || response.status == 401){
    //             $window.location = '/login';
    //         }
    //         return $q.reject(response);
    //     }
    // };
})();
//# sourceMappingURL=sessionTimeout.factory.js.map