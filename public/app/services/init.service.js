(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('InitFactory', InitFactory);
    InitFactory.$inject = ['$q', '$rootScope', '$http'];
    function InitFactory($q, $rootScope, $http) {
        var service = {
            getCurrentUser: getCurrentUser,
            isUserAdmin: isUserAdmin
        };
        return service;
        ////////////////
        function getCurrentUser() {
            var deferred = $q.defer();
            IQ_PatientActions.GetCurrentUser(function (result, event) {
                //console.log(result);
                //$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function isUserAdmin() {
            var deferred = $q.defer();
            IQ_PatientActions.IsUserAdmin(function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=init.service.js.map