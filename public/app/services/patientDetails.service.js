(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('PatientDetailsFactory', PatientDetailsFactory);
    PatientDetailsFactory.$inject = ['$q', '$rootScope', '$http', 'PatientService'];
    function PatientDetailsFactory($q, $rootScope, $http, PatientService) {
        var service = {
            LoadCasesAndEvents: LoadCasesAndEvents,
            LoadSurveysFromCaseType: LoadSurveysFromCaseType,
        };
        return service;
        ////////////////
        function LoadCasesAndEvents() {
            var deferred = $q.defer();
            var patient = PatientService.patient;
            // console.log(patient);
            IQ_PatientActions.LoadCasesAndEvents(patient.id, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadSurveysFromCaseType(caseId) {
            var deferred = $q.defer();
            IQ_SurveyRepository.LoadSurveysFromCaseType(caseId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=patientDetails.service.js.map