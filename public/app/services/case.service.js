var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var CaseService = (function () {
            function CaseService($q, $rootScope) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this._currentCase = {};
                this.caseEvent = {};
                this.physician = {};
            }
            Object.defineProperty(CaseService.prototype, "currentProcedure", {
                get: function () {
                    return this._currentProcedure;
                },
                set: function (_currentProcedure) {
                    this._currentProcedure = _currentProcedure;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CaseService.prototype, "currentCase", {
                get: function () {
                    return this._currentCase;
                },
                set: function (_currentCase) {
                    this._currentCase = _currentCase;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CaseService.prototype, "clinicalData", {
                get: function () {
                    return this._clinicalData;
                },
                set: function (_clinicalData) {
                    this._clinicalData = _clinicalData;
                },
                enumerable: true,
                configurable: true
            });
            CaseService.prototype.getCurrentCaseEvent = function () {
                return this.caseEvent;
            };
            CaseService.prototype.setCurrentCaseEvent = function (event) {
                this.caseEvent = event;
            };
            CaseService.prototype.getCurrentCase = function () {
                return this.currentCase;
            };
            CaseService.prototype.setCurrentCase = function (_currentCase) {
                this._currentCase = _currentCase;
            };
            CaseService.prototype.getProcedure = function () {
                return this.currentProcedure;
            };
            CaseService.prototype.setProcedure = function (_procedure) {
                this.currentProcedure = _procedure;
                this.clinicalData = _procedure.Clinical_Data__r[0];
                console.log(this._clinicalData);
            };
            CaseService.prototype.getSurveyCompletionDates = function (caseId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_CaseActions.GetSurveyCompletionDates(caseId, function (result, event) {
                    //console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            CaseService.prototype.getPhysician = function () {
                return this.physician;
            };
            CaseService.prototype.setPhysician = function (_physician) {
                this.physician = _physician;
            };
            return CaseService;
        }());
        CaseService.inject = ['$q', '$rootScope'];
        services.CaseService = CaseService;
        angular
            .module('orthosensor.services')
            .service('CaseService', CaseService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=case.service.js.map