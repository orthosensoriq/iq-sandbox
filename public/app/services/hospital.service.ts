module orthosensor.services {
    export class HospitalService {
        static inject: Array<string> = ['$q', '$rootScope', '$http', 'SFDataService'];
        private _hospitals: any;
        private _hospital: orthosensor.domains.Hospital;
        private _practices: any;
        private _filterHospitalId: string;
        private _filterPracticeId: string;
        private _filterHospitalName: string;
        private _filterPracticeName: string;
        private _currentFilterId: string;
        get hospitals(): any {
            return this._hospitals;
        }
        set hospitals(hospitals: any) {
            this._hospitals = hospitals;
        }

        get hospital(): orthosensor.domains.Hospital {
            return this._hospital;
        }
        set hospital(_hospital: orthosensor.domains.Hospital) {
            this._hospital = _hospital;
        }
        get practices(): any {
            return this._practices;
        }
        set practices(practices: any) {
            this._practices = practices;
        }
        get filterHospitalId() {
            return this._filterHospitalId;
        }
        set filterHospitalId(filterHospitalId: string) {
            this._filterHospitalId = filterHospitalId;
            // this._filterHospitalName = this.getHospitalName(filterHospitalId);
        }

        get filterPracticeId() {
            return this._filterPracticeId;
        }
        set filterPracticeId(filterPracticeId: string) {
            this._filterPracticeId = filterPracticeId;
            // this._filterPracticeName = this.getPracticeName(filterPracticeId);
        }
        get currentFilterId(): string {
            return this._currentFilterId;
        }
        set currentFilterId(currentFilterId: string) {
            this._currentFilterId = currentFilterId;
        }

        get filterHospitalName() {
            return this._filterHospitalName;
        }
        set filterHospitalName(name: string) {
            this._filterHospitalName = name;
        }
        get filterPracticeName() {
            return this._filterPracticeName;
        }

        set filterPracticeName(name: string) {
            this._filterPracticeName = name;
        }
        private _surgeons: orthosensor.domains.Surgeon[];
        get surgeons(): orthosensor.domains.Surgeon[] {
            console.log(this._surgeons);
            return this._surgeons;
        }
        set surgeons(_surgeons: orthosensor.domains.Surgeon[]) {
            console.log(this._surgeons);
            this._surgeons = _surgeons;
            console.log(this._surgeons);
        }

        constructor(public $q: any,
            public $rootScope: ng.IRootScopeService,
            public $http: ng.IHttpProvider,
            public SFDataService: orthosensor.services.SFDataService) { }

        getHospitals(): ng.IPromise<any> {

            return this.SFDataService.loadHospitals()
                .then((data) => {
                    this._hospitals = this.convertSFHospitalToObject(data);
                    console.log(this._hospitals);
                    return this._hospitals;
                }, (error) => {
                });
        }

        setHospitals(data) {
            this._hospitals = this.convertSFHospitalToObject(data);
            console.log(this._hospitals);
        }

        setPractices(hospitalId: string): void {
            return this.SFDataService.loadHospitalPractices(hospitalId)
                .then((data) => {
                    this._practices = this.convertSFPracticeToObject(data);
                    console.log(this._practices);
                    //return this._practices;
                }, (error) => {
                });
            // return [];
        }
        getPracticeName(id: string) {
            console.log('Searching for practice: ' + id);
            if (id !== undefined) {
                for (let i = 0; i < this._practices.length; i++) {
                    // console.log(this._practices[i].Id);
                    if (this._practices[i].Id === id) {
                        console.log(this._practices[i].Id + ', ' + id + ' found!');
                        return this._practices[i].Name;
                    }
                }
            }
            return '';
        }

        getHospitalName(id: string) {
            console.log('Searching for hospital: ' + id);
            if (id !== undefined) {
                for (let i = 0; i < this._hospitals.length; i++) {
                    if (this._hospitals[i].Id === id) {
                        console.log(this._hospitals[i].Id + ', ' + id + ' found!');
                        return this._hospitals[i].Name;
                    }
                }
            }
            return '';
        }

        public loadHospitalPracticeSurgeons(hospitalPracticeId: string): ng.IPromise<any> {
            // let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalPracticeId);
            // if (hospitalPracticeId !== null) {
            return this.SFDataService.loadHospitalPracticeSurgeons(hospitalPracticeId)
                .then((data: any) => {
                    this._surgeons = this.convertSFSurgeonToObject(data);
                    console.log(this._surgeons);
                }, (error: any) => {
                    console.log(error);
                });
            // return surgeonData;
            // }
        };
        public loadHospitalByPractice(hospitalPracticeId: string): ng.IPromise<any> {
            // let surgeonData: orthosensor.domains.Surgeon[];
            console.log(hospitalPracticeId);
            if (hospitalPracticeId !== null) {
                return this.SFDataService.getHospitalByPractice(hospitalPracticeId)
                    .then((data: any) => {
                        console.log(data);
                        this._hospital = new orthosensor.domains.Hospital(
                            data.ParentId,
                            data.Parent.Name,
                            'HospitalAccount'
                        );
                        console.log(this._hospital);
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        }
        public loadAllSurgeons(): ng.IPromise<any> {
            return this.SFDataService.loadAllSurgeons()
                .then((data: any) => {
                    // console.log(data);
                    this.surgeons = this.convertSFSurgeonToObject(data);
                }, (error: any) => {
                    console.log(error);
                });
            // return surgeonData;
        }
        public loadSurgeons(hospitalId: string): void {
            let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalId);
            if (hospitalId !== null) {
                this.PatientService.loadSurgeons(hospitalId)
                    .then((data: any) => {
                        let surg = data;
                        // console.log(surg);
                        for (let i = 0; i < surg.length; i++) {
                            let surgeonR = surg[i].Surgeon__r;
                            let surgeonD: orthosensor.domains.Surgeon;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        this.surgeons = surgeonData;
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        }

        public loadHospitalPractices(hospitalId: string): void {
            this.setPractices(hospitalId);
        }
        convertSFHospitalToObject(data: any[]): orthosensor.domains.Hospital[] {
            let hospitals: orthosensor.domains.Hospital[] = [];
            // console.log(data.length);
            for (let i = 0; i < data.length; i++) {
                let hospital: orthosensor.domains.Hospital = {
                    id: data[i].Id,
                    name: data[i].Name,
                };

                // console.log(practice);
                hospitals.push(hospital);
            }
            // console.log(hospitals);
            return hospitals;
        }
        public convertSFPracticeToObject(data: any[]): orthosensor.domains.Practice[] {
            let practices: orthosensor.domains.Practice[];
            practices = [];
            // console.log(data.length);
            for (let i = 0; i < data.length; i++) {
                let practice: orthosensor.domains.Practice = {
                    id: data[i].Id,
                    name: data[i].Name,
                    parentId: data[i].ParentId,
                    parentName: '' //data[i].Parent.Name
                };
                // console.log(practice);
                practices.push(practice);
            }
            // console.log(practices);
            return practices;
        }
        public convertSFSurgeonToObject(data: any[]): orthosensor.domains.Surgeon[] {
            console.log(data);
            let surgeons: orthosensor.domains.Surgeon[];
            surgeons = [];
            for (let i = 0; i < data.length; i++) {
                let surgeon: orthosensor.domains.Surgeon = {
                    id: data[i].Surgeon__r.AccountId,
                    name: data[i].Surgeon__r.Name,
                    parentId: data[i].Hospital__c,
                    parentName: '' //data[i].Parent.Name //do we need this?
                };

                surgeons.push(surgeon);
            }
            console.log(surgeons);
            return surgeons;
        }


    }

    angular
        .module('orthosensor.services')
        .service('HospitalService', HospitalService);
}
