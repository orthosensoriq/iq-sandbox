var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var HospitalService = (function () {
            function HospitalService($q, $rootScope, $http, SFDataService) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
                this.SFDataService = SFDataService;
            }
            Object.defineProperty(HospitalService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (hospitals) {
                    this._hospitals = hospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "hospital", {
                get: function () {
                    return this._hospital;
                },
                set: function (_hospital) {
                    this._hospital = _hospital;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "practices", {
                get: function () {
                    return this._practices;
                },
                set: function (practices) {
                    this._practices = practices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalId", {
                get: function () {
                    return this._filterHospitalId;
                },
                set: function (filterHospitalId) {
                    this._filterHospitalId = filterHospitalId;
                    // this._filterHospitalName = this.getHospitalName(filterHospitalId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeId", {
                get: function () {
                    return this._filterPracticeId;
                },
                set: function (filterPracticeId) {
                    this._filterPracticeId = filterPracticeId;
                    // this._filterPracticeName = this.getPracticeName(filterPracticeId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "currentFilterId", {
                get: function () {
                    return this._currentFilterId;
                },
                set: function (currentFilterId) {
                    this._currentFilterId = currentFilterId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalName", {
                get: function () {
                    return this._filterHospitalName;
                },
                set: function (name) {
                    this._filterHospitalName = name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeName", {
                get: function () {
                    return this._filterPracticeName;
                },
                set: function (name) {
                    this._filterPracticeName = name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "surgeons", {
                get: function () {
                    console.log(this._surgeons);
                    return this._surgeons;
                },
                set: function (_surgeons) {
                    console.log(this._surgeons);
                    this._surgeons = _surgeons;
                    console.log(this._surgeons);
                },
                enumerable: true,
                configurable: true
            });
            HospitalService.prototype.getHospitals = function () {
                var _this = this;
                return this.SFDataService.loadHospitals()
                    .then(function (data) {
                    _this._hospitals = _this.convertSFHospitalToObject(data);
                    console.log(_this._hospitals);
                    return _this._hospitals;
                }, function (error) {
                });
            };
            HospitalService.prototype.setHospitals = function (data) {
                this._hospitals = this.convertSFHospitalToObject(data);
                console.log(this._hospitals);
            };
            HospitalService.prototype.setPractices = function (hospitalId) {
                var _this = this;
                return this.SFDataService.loadHospitalPractices(hospitalId)
                    .then(function (data) {
                    _this._practices = _this.convertSFPracticeToObject(data);
                    console.log(_this._practices);
                    //return this._practices;
                }, function (error) {
                });
                // return [];
            };
            HospitalService.prototype.getPracticeName = function (id) {
                console.log('Searching for practice: ' + id);
                if (id !== undefined) {
                    for (var i = 0; i < this._practices.length; i++) {
                        // console.log(this._practices[i].Id);
                        if (this._practices[i].Id === id) {
                            console.log(this._practices[i].Id + ', ' + id + ' found!');
                            return this._practices[i].Name;
                        }
                    }
                }
                return '';
            };
            HospitalService.prototype.getHospitalName = function (id) {
                console.log('Searching for hospital: ' + id);
                if (id !== undefined) {
                    for (var i = 0; i < this._hospitals.length; i++) {
                        if (this._hospitals[i].Id === id) {
                            console.log(this._hospitals[i].Id + ', ' + id + ' found!');
                            return this._hospitals[i].Name;
                        }
                    }
                }
                return '';
            };
            HospitalService.prototype.loadHospitalPracticeSurgeons = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                // console.log(hospitalPracticeId);
                // if (hospitalPracticeId !== null) {
                return this.SFDataService.loadHospitalPracticeSurgeons(hospitalPracticeId)
                    .then(function (data) {
                    _this._surgeons = _this.convertSFSurgeonToObject(data);
                    console.log(_this._surgeons);
                }, function (error) {
                    console.log(error);
                });
                // return surgeonData;
                // }
            };
            ;
            HospitalService.prototype.loadHospitalByPractice = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                console.log(hospitalPracticeId);
                if (hospitalPracticeId !== null) {
                    return this.SFDataService.getHospitalByPractice(hospitalPracticeId)
                        .then(function (data) {
                        console.log(data);
                        _this._hospital = new orthosensor.domains.Hospital(data.ParentId, data.Parent.Name, 'HospitalAccount');
                        console.log(_this._hospital);
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            HospitalService.prototype.loadAllSurgeons = function () {
                var _this = this;
                return this.SFDataService.loadAllSurgeons()
                    .then(function (data) {
                    // console.log(data);
                    _this.surgeons = _this.convertSFSurgeonToObject(data);
                }, function (error) {
                    console.log(error);
                });
                // return surgeonData;
            };
            HospitalService.prototype.loadSurgeons = function (hospitalId) {
                var _this = this;
                var surgeonData;
                // console.log(hospitalId);
                if (hospitalId !== null) {
                    this.PatientService.loadSurgeons(hospitalId)
                        .then(function (data) {
                        var surg = data;
                        // console.log(surg);
                        for (var i = 0; i < surg.length; i++) {
                            var surgeonR = surg[i].Surgeon__r;
                            var surgeonD = void 0;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        _this.surgeons = surgeonData;
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            HospitalService.prototype.loadHospitalPractices = function (hospitalId) {
                this.setPractices(hospitalId);
            };
            HospitalService.prototype.convertSFHospitalToObject = function (data) {
                var hospitals = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var hospital = {
                        id: data[i].Id,
                        name: data[i].Name,
                    };
                    // console.log(practice);
                    hospitals.push(hospital);
                }
                // console.log(hospitals);
                return hospitals;
            };
            HospitalService.prototype.convertSFPracticeToObject = function (data) {
                var practices;
                practices = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var practice = {
                        id: data[i].Id,
                        name: data[i].Name,
                        parentId: data[i].ParentId,
                        parentName: '' //data[i].Parent.Name
                    };
                    // console.log(practice);
                    practices.push(practice);
                }
                // console.log(practices);
                return practices;
            };
            HospitalService.prototype.convertSFSurgeonToObject = function (data) {
                console.log(data);
                var surgeons;
                surgeons = [];
                for (var i = 0; i < data.length; i++) {
                    var surgeon = {
                        id: data[i].Surgeon__r.AccountId,
                        name: data[i].Surgeon__r.Name,
                        parentId: data[i].Hospital__c,
                        parentName: '' //data[i].Parent.Name //do we need this?
                    };
                    surgeons.push(surgeon);
                }
                console.log(surgeons);
                return surgeons;
            };
            return HospitalService;
        }());
        HospitalService.inject = ['$q', '$rootScope', '$http', 'SFDataService'];
        services.HospitalService = HospitalService;
        angular
            .module('orthosensor.services')
            .service('HospitalService', HospitalService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=hospital.service.js.map