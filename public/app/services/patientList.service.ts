module orthosensor.services {

    export class PatientListFactory {
        static inject: Array<string> = ['$q', '$rootScope', '$http'];
        public patient: any;
        private _hl7Patients: any[];
        get hl7Patients(): any[] {
            return this._hl7Patients;
        }
        set hl7Patients(hl7Patients: any[]) {
            this._hl7Patients = hl7Patients;
        }

        constructor(public $q: ng.IQService, public $rootScope: ng.IRootScopeService, public $http: ng.IHttpProvider) {
            this.patient = {};
        }

        // hospitalId can be a hospital or practice        
        getPatients(hospitalId: string) {
            let deferred = this.$q.defer();
            // console.log(hospitalId);
            if (!hospitalId) {
                // console.log('hospitalId is not defined at all!');
                hospitalId = null;
            }
            if (hospitalId === undefined) {
                // console.log('hospitalId is not defined!');
                hospitalId = null;
            }
            IQ_PatientActions.LoadPatientList(hospitalId, function (result: any, event: any) {
                //console.log(result);
                //this.$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        getHL7Patients(hospitalId: string) {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadHL7PatientList(hospitalId, function (result, event) {
                console.log(result);
                // this.$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        SearchPatientList(hospitalId, keyword) {
            let deferred = this.$q.defer();
            IQ_PatientActions.SearchPatientList(hospitalId, keyword,
                (result, event) => {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        SearchHL7PatientList(hospitalId: string, keyword: string): any[] {
            console.log(hospitalId + ', ' + keyword);
            console.log(this.hl7Patients);
            let matching: any[] = [];
            if (this.hl7Patients.length > 0) {
                for (let i = 0; i < this.hl7Patients.length; i++) {
                    if (this.hl7Patients[i].Patient_Last_Name__c.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) {
                        matching.push(this.hl7Patients[i]);
                    }
                } 
            } 
            return matching;
        }

        // function getFilterHospitalId() {
        //     return _filterHospitalId;
        // }
        // function setFilterHospitalId(filterHospitalId: string) {
        //     _filterHospitalId = filterHospitalId;
        // }

        // function getFilterPracticeId() {
        //     return _filterPracticeId;
        // }
        // function setFilterPracticeId(filterPracticeId: string) {
        //     _filterPracticeId = filterPracticeId;
        // }
        getCurrentPatient() {
            return this.patient;
        }
        setCurrentPatient(_patient) {
            this.patient = _patient;
        }

        createCase(patient, patientCase) {
            console.log(patient);
            console.log(patientCase);
            let deferred = this.$q.defer();
            let procedureDateString = moment.utc(patientCase.ProcedureDateString).format('MM/DD/YYYY');
            // console.log(procedureDateString);
            IQ_PatientActions.CreateCase(patient.id, patientCase.CaseTypeId, procedureDateString, patientCase.SurgeonID, patientCase.laterality, function (result, event) {
                //console.log(result);
                //this.$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadCaseTypes(hospitalId: string) {
            let deferred = this.$q.defer();
            console.log(hospitalId);
            IQ_PatientActions.GetCaseTypes(hospitalId, (result, event) => {
                //console.log(result);
                // this.$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadSurgeons(hospitalId: string) {
            let deferred = this.$q.defer();
            // console.log(hospitalId);
            // if (hospitalId !== null) {
            IQ_PatientActions.GetSurgeons(hospitalId, (result, event) => {
                // console.log(result);
                // this.$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }


        loadHospitalPracticeSurgeons(hospitalId) {
            let deferred = this.$q.defer();
            // console.log(hospitalId);
            // if (hospitalId !== null) {
            IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                // console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        //            else {
        //                return null;
        //            }
        //        }
        loadHospitals() {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadHospitals((result, event) => {
                //console.log(result);
                this.$rootScope.$apply(() => {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        loadPractices(hospitalId) {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadPractices((result, event) => {
                // console.log(result);
                this.$rootScope.$apply(() => {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getHospitalPractices(hospitalId) {
            let deferred = this.$q.defer();
            // console.log(hospitalId);
            IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                //console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getCaseActivityByPractice(practiceId, startDate, endDate) {
            // let IQ_DashboardRepository: any
            let deferred = this.$q.defer();
            // console.log(startDate);
            // console.log(endDate);
            IQ_DashboardRepository.GetProceduresPerMonthByPractice(practiceId, startDate, endDate, function (result, event) {
                // console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getCaseActivityBySurgeon(surgeonId, startDate, endDate) {
            // let IQ_DashboardRepository: any
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetProceduresPerMonthBySurgeon(surgeonId, startDate, endDate, function (result, event) {
                //console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getSurgeonPhaseCount(id, startDate, endDate) {
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetSurgeonPhaseCount(id, startDate, endDate, function (result, event) {
                // console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getPracticePhaseCount(id, startDate, endDate) {
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetPracticePhaseCount(id, startDate, endDate, function (result, event) {
                // console.log(result);
                this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getMonthlyPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear) {
            console.log(id, startMonth, startYear, endMonth, endYear);
            /// change this before deployment - set for test data
            // startMonth = 4;
            // startYear = 2016;
            // endMonth = 8;
            // endYear = 2016;

            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear,
                function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        } else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getMonthlySurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear) {
            console.log(startMonth, startYear, endMonth, endYear);
            /// change this before deployment - set for test data
            // startMonth = 4;
            // startYear = 2016;
            // endMonth = 8;
            // endYear = 2016;
            let deferred = this.$q.defer();
            IQ_DashboardRepository.GetSurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear,
                function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        } else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }

    angular
        .module('orthosensor.services')
        .service('PatientListFactory', PatientListFactory);
}
