module orthosensor.services {
    // intended for calls to salesforce data - need to migrate some 
    // other 
    export class SFDataService {
        static inject: Array<string> = ['$q', '$rootScope', '$http'];
        constructor(public $q: any, public $rootScope: ng.IRootScopeService, $http: ng.IHttpProvider) { }

        loadHospitals() {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadHospitals((result, event) => {
                //console.log(result);
                // this.$rootScope.$apply(() => {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadPractices() {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadPractices((result, event) => {
                // console.log(result);
                 this.$rootScope.$apply(() => {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
             }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadHospitalPractices(hospitalId) {
            let deferred = this.$q.defer();
            IQ_PatientActions.LoadHospitalPractices(hospitalId, (result, event) => {
            console.log(result);
                this.$rootScope.$apply(() => {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadHospitalPracticeSurgeons(hospitalId) {
            let deferred = this.$q.defer();
            console.log(hospitalId);
            // if (hospitalId !== null) {
            IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, (result, event) => {
                console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        loadAllSurgeons() {
            let deferred = this.$q.defer();

            // if (hospitalId !== null) {
            IQ_PatientActions.LoadAllSurgeons(function (result, event) {
                // console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }

        getHospitalByPractice(practiceId) {
            let deferred = this.$q.defer();
            // console.log(hospitalId);
            IQ_PatientActions.LoadHospitalByPractice(practiceId, function (result, event) {
                console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                } else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
    angular
        .module('orthosensor.services')
        .service('SFDataService', SFDataService);
}
