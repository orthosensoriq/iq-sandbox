(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('SensorService', SensorService);
    SensorService.$inject = ['$q', '$rootScope', '$http'];
    function SensorService($q, $rootScope, $http) {
        var service = {
            AddSensor: AddSensor,
            AddSensorManually: AddSensorManually
        };
        return service;
        ////////////////
        function AddSensor(barcode, procedureId) {
            var deferred = $q.defer();
            console.log(barcode);
            IQ_CaseActions.AddSensor(barcode, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply( () => {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddSensorManually(ref, man, sn, lot, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            //console.log('adding sensor manually');
            IQ_CaseActions.AddSensorManually(ref, man, sn, lot, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    //console.log('succeeding in adding sensor');
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    //console.log('failed to add sensor');
                    deferred.reject(event);
                }
            });
            //}, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=sensor.service.js.map