module orthosensor.services {
    // import orthosensor.domains.UserProfile from orthosensor.domains.orthosensor.domains.UserProfile;
    // import User from orthosensor.domains.User;
    export class UserService {
        public sfUser: {};

        public accountName: string;
        public accountId: string;
        public recordType: string;
        public userType: string;
        // public hasPractices: boolean;
        private _hasPractices: boolean;
        private _isAdmin: boolean;
        private _user: orthosensor.domains.User;

        get hasPractices(): boolean {
            return this._hasPractices;
        }
        set hasPractices(hasPractices: boolean) {
            this._hasPractices = hasPractices;
        }

        get isAdmin(): boolean {
            return this._isAdmin;
        }

        set isAdmin(isAdmin: boolean) {
            this._isAdmin = isAdmin;
        }

        constructor() { }

        public get user(): orthosensor.domains.User {
            return this._user;
        }
        // get current user from Force.com. this is done in the run.js file.
        // Change if we authenticate differently
        public set user(_user: orthosensor.domains.User) {
            this._user = _user;
        }

        public isSurgeon() {
            // console.log(this.user.Contact.Account.RecordType.Name);
            // return this.user.Contact.Account.RecordType.Name === 'Surgeon';
            return true;
        }
        public convertSFToObject(data: any): orthosensor.domains.User {
            console.log(data);
            let user = new orthosensor.domains.User();
            user.id = data.Id;
            user.name = data.Name;
            user.userType = data.Profile.Name;
            user.userProfile = this.getUserProfile(user.userType);
            if (user.userType !== 'System Administrator') {
                user.contactId = data.ContactId;
                user.accountName = data.Contact.Account.Name;
                user.accountId = data.Contact.Account.Id;
                user.recordType = data.Contact.Account.RecordType.Name;
                user.anonymousPatients = data.Contact.Account.Anonymous_Patients__c;
                if (data.Contact.Account.Parent) {
                    user.hospitalId = data.Contact.Account.ParentId;
                    user.hospitalName = data.Contact.Account.Parent.Name;
                }    
            }
            console.log(user);
            return user;
        }
        getUserProfile(userType: string): orthosensor.domains.UserProfile {
            switch (userType) {
                case 'System Administrator':             return orthosensor.domains.UserProfile.SystemAdmin;
                case 'Hospital Partner Community User':  return orthosensor.domains.UserProfile.HospitalAdmin;
                case 'Hospital Admin Community Login User':  return orthosensor.domains.UserProfile.HospitalAdmin;
                case 'Hospital Admin Community User':  return orthosensor.domains.UserProfile.HospitalAdmin;
                case 'Surgeon Partner Community User':   return orthosensor.domains.UserProfile.Surgeon;
                case 'Surgeon Community Login User':   return orthosensor.domains.UserProfile.Surgeon;
                case 'Surgeon Community User':   return orthosensor.domains.UserProfile.Surgeon;
                case 'Partner Community User': return orthosensor.domains.UserProfile.PracticeAdmin;
                case 'Practice Community Login User': return orthosensor.domains.UserProfile.PracticeAdmin;
                case 'Practice Community User': return orthosensor.domains.UserProfile.PracticeAdmin;
                case 'Customer Community User': return orthosensor.domains.UserProfile.PracticeAdmin;
                case 'Customer Community Plus User': return orthosensor.domains.UserProfile.PracticeAdmin;
                default: return orthosensor.domains.UserProfile.PracticeAdmin;    
                
            }
        }
    }

    UserService.$inject = [];
    angular
        .module('orthosensor.services')
        .service('UserService', UserService);
}
