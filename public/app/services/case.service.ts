module orthosensor.services {

    export class CaseService {
        static inject: Array<string> = ['$q', '$rootScope'];
        private _currentCase = {};
        public caseEvent = {};
        private _currentProcedure: {};
        get currentProcedure() {
            return this._currentProcedure;
        }
        set currentProcedure(_currentProcedure: any) {
            this._currentProcedure = _currentProcedure;
        }
        get currentCase() {
            return this._currentCase;
        }
        set currentCase(_currentCase) {
            this._currentCase = _currentCase;
        }

        public physician = {};
        private _clinicalData: {};
        get clinicalData() {
            return this._clinicalData;
        }
        set clinicalData(_clinicalData) {
            this._clinicalData = _clinicalData;
        }

        constructor(public $q: ng.IQService, public $rootScope: ng.IRootScopeService) {
        }

        getCurrentCaseEvent() {

            return this.caseEvent;
        }
        setCurrentCaseEvent(event) {
            this.caseEvent = event;
        }
        getCurrentCase() {
            return this.currentCase;
        }
        setCurrentCase(_currentCase) {
            this._currentCase = _currentCase;
        }
        getProcedure() {
            return this.currentProcedure;
        }
        setProcedure(_procedure) {
            this.currentProcedure = _procedure;
            this.clinicalData = _procedure.Clinical_Data__r[0];
            console.log(this._clinicalData);
        }
        getSurveyCompletionDates(caseId) {
            let deferred = this.$q.defer();

            IQ_CaseActions.GetSurveyCompletionDates(caseId, (result, event) => {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    this.$rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        getPhysician() {
            return this.physician;
        }
        setPhysician(_physician) {
            this.physician = _physician;
        }
    }

    angular
        .module('orthosensor.services')
        .service('CaseService', CaseService);
}
