var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        // intended for calls to salesforce data - need to migrate some 
        // other 
        var SFDataService = (function () {
            function SFDataService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
            }
            SFDataService.prototype.loadHospitals = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitals(function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(() => {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadPractices = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadPractices(function (result, event) {
                    // console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadHospitalPractices = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                    console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadHospitalPracticeSurgeons = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                    console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadAllSurgeons = function () {
                var deferred = this.$q.defer();
                // if (hospitalId !== null) {
                IQ_PatientActions.LoadAllSurgeons(function (result, event) {
                    // console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.getHospitalByPractice = function (practiceId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                IQ_PatientActions.LoadHospitalByPractice(practiceId, function (result, event) {
                    console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            return SFDataService;
        }());
        SFDataService.inject = ['$q', '$rootScope', '$http'];
        services.SFDataService = SFDataService;
        angular
            .module('orthosensor.services')
            .service('SFDataService', SFDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=SFDataService.js.map