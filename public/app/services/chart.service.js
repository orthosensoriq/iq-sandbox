var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var ChartService = (function () {
            function ChartService() {
                this.chart = new orthosensor.domains.ChartType();
                this.chartTitle = 'Test';
                //this.chart.options = this.getColumnChartOptions();
                this.chart.type = 'ColumnChart';
                this.chartHeight = 240;
            }
            ChartService.prototype.getChart = function () {
                return this.chart;
            };
            ChartService.prototype.getMultiBarChartOptions = function () {
                return {
                    chart: {
                        type: 'multiBarChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showValues: true,
                        valueFormat: function (d) {
                            return d3.format(',.2f')(d);
                        },
                        showLegend: true,
                        showControls: false,
                        reduceXTicks: false,
                        rotateLabels: 0,
                        transitionDuration: 400,
                        stacked: false,
                        tooltips: true,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            ticks: d3.time.months,
                            tickFormat: function (d) {
                                // console.log(d);
                                var format = d3.time.format('%b-%y');
                                // console.log(format);
                                var dateConvert = format(new Date(d));
                                // console.log(dateConvert);
                                return dateConvert;
                            },
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -20,
                            tickFormat: (d3.format(',.1f')),
                            margin: { top: 10, left: 20 },
                            showMaxMin: false,
                        },
                        showXAxis: true,
                        showYAxis: true,
                        legend: {
                            margin: {
                                top: 5,
                                right: 0,
                                bottom: 20,
                                left: 0
                            },
                            radioButtonMode: true,
                            rightAlign: true,
                        },
                    },
                    title: {
                        enable: false,
                        text: this.chartTitle,
                    },
                };
            };
            ChartService.prototype.getMultiBarHorizontalChartOptions = function () {
                return {
                    chart: {
                        type: 'multiBarHorizontalChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showValues: true,
                        valueFormat: function (d) {
                            return d3.format(',.2f')(d);
                        },
                        showLegend: false,
                        showControls: false,
                        reduceXTicks: false,
                        rotateLabels: 0,
                        transitionDuration: 400,
                        stacked: false,
                        tooltips: true,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            ticks: d3.time.months,
                            tickFormat: function (d) {
                                // console.log(d);
                                var format = d3.time.format("%b-%y");
                                // console.log(new Date(d));
                                var dateConvert = format(new Date(d));
                                // console.log(dateConvert);
                                return dateConvert;
                            },
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -20,
                            tickFormat: (d3.format(',.1f')),
                            margin: { top: 10, left: 20 },
                            showMaxMin: false,
                        },
                        showXAxis: true,
                        showYAxis: true,
                        legend: {
                            margin: {
                                top: 5,
                                right: 0,
                                bottom: 20,
                                left: 0
                            },
                            radioButtonMode: true,
                            rightAlign: true,
                        },
                    },
                    title: {
                        enable: false,
                        text: this.chartTitle,
                    },
                };
            };
            ChartService.prototype.getLineChartOptions = function () {
                var events = ["Pre-Op", "4 - 8 week", "6 month", "1 year", "2 year"];
                return {
                    chart: {
                        type: 'lineChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        useInteractiveGuideline: false,
                        interactive: false,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            tickFormat: function (d) {
                                return events[d];
                            }
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -10
                        },
                        yTickFormat: function (d) {
                            return d3.format('.02f')(d);
                        },
                    },
                    title: {
                        enable: false,
                        text: 'Title for Line Chart'
                    },
                    subtitle: {
                        enable: false,
                        text: '',
                        css: {
                            'text-align': 'center',
                            'margin': '10px 13px 0px 7px'
                        }
                    },
                    caption: {
                        enable: false,
                        css: {
                            'text-align': 'justify',
                            'margin': '10px 13px 0px 7px'
                        }
                    },
                    styles: {
                        css: {
                            'background-color': 'White'
                        }
                    }
                };
            };
            ChartService.prototype.getColumnChartOptions = function () {
                // var chart = {};
                var options;
                options = new orthosensor.domains.ChartOptions();
                options.title = 'Tewt';
                options.legend = 'none';
                options.displayExactValues = true;
                options.fill = 20;
                options.animation = {
                    'duration': 1000,
                    'easing': 'out',
                };
                options.tooltip = { 'isHtml': true };
                options.is3D = true;
                options.chartArea = { 'left': 180, 'top': 20, 'bottom': 40, 'height': '100%' };
                options.hAxis = { title: 'Patients', 'gridlines': { 'count': 6 } };
                console.log(options);
                return options;
            };
            ChartService.prototype.getChartOptions = function () {
                return {
                    'title': this.chartTitle,
                    'legend': 'none',
                    'displayExactValues': true,
                    'fill': 20,
                    'animation': {
                        'duration': 1000,
                        'easing': 'out',
                    },
                    'tooltip': { 'isHtml': true },
                    'is3D': true,
                    'chartArea': { 'left': 180, 'top': 20, 'bottom': 40, 'height': '100%' },
                    'hAxis': { 'title': 'Patients', 'gridlines': { 'count': 6 } },
                };
            };
            return ChartService;
        }());
        services.ChartService = ChartService;
        // ChartService.$inject = [];
        angular
            .module('orthosensor.services')
            .service('ChartService', ChartService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=chart.service.js.map