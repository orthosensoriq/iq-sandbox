var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientListFactory = (function () {
            function PatientListFactory($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
                this.patient = {};
            }
            Object.defineProperty(PatientListFactory.prototype, "hl7Patients", {
                get: function () {
                    return this._hl7Patients;
                },
                set: function (hl7Patients) {
                    this._hl7Patients = hl7Patients;
                },
                enumerable: true,
                configurable: true
            });
            // hospitalId can be a hospital or practice        
            PatientListFactory.prototype.getPatients = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                if (!hospitalId) {
                    // console.log('hospitalId is not defined at all!');
                    hospitalId = null;
                }
                if (hospitalId === undefined) {
                    // console.log('hospitalId is not defined!');
                    hospitalId = null;
                }
                IQ_PatientActions.LoadPatientList(hospitalId, function (result, event) {
                    //console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getHL7Patients = function (hospitalId) {
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHL7PatientList(hospitalId, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.SearchPatientList = function (hospitalId, keyword) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.SearchPatientList(hospitalId, keyword, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.SearchHL7PatientList = function (hospitalId, keyword) {
                console.log(hospitalId + ', ' + keyword);
                console.log(this.hl7Patients);
                var matching = [];
                if (this.hl7Patients.length > 0) {
                    for (var i = 0; i < this.hl7Patients.length; i++) {
                        if (this.hl7Patients[i].Patient_Last_Name__c.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) {
                            matching.push(this.hl7Patients[i]);
                        }
                    }
                }
                return matching;
            };
            // function getFilterHospitalId() {
            //     return _filterHospitalId;
            // }
            // function setFilterHospitalId(filterHospitalId: string) {
            //     _filterHospitalId = filterHospitalId;
            // }
            // function getFilterPracticeId() {
            //     return _filterPracticeId;
            // }
            // function setFilterPracticeId(filterPracticeId: string) {
            //     _filterPracticeId = filterPracticeId;
            // }
            PatientListFactory.prototype.getCurrentPatient = function () {
                return this.patient;
            };
            PatientListFactory.prototype.setCurrentPatient = function (_patient) {
                this.patient = _patient;
            };
            PatientListFactory.prototype.createCase = function (patient, patientCase) {
                console.log(patient);
                console.log(patientCase);
                var deferred = this.$q.defer();
                var procedureDateString = moment.utc(patientCase.ProcedureDateString).format('MM/DD/YYYY');
                // console.log(procedureDateString);
                IQ_PatientActions.CreateCase(patient.id, patientCase.CaseTypeId, procedureDateString, patientCase.SurgeonID, patientCase.laterality, function (result, event) {
                    //console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadCaseTypes = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log(hospitalId);
                IQ_PatientActions.GetCaseTypes(hospitalId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadSurgeons = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_PatientActions.GetSurgeons(hospitalId, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadHospitalPracticeSurgeons = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            //            else {
            //                return null;
            //            }
            //        }
            PatientListFactory.prototype.loadHospitals = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitals(function (result, event) {
                    //console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadPractices = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadPractices(function (result, event) {
                    // console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getHospitalPractices = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                    //console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getCaseActivityByPractice = function (practiceId, startDate, endDate) {
                // let IQ_DashboardRepository: any
                var deferred = this.$q.defer();
                // console.log(startDate);
                // console.log(endDate);
                IQ_DashboardRepository.GetProceduresPerMonthByPractice(practiceId, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getCaseActivityBySurgeon = function (surgeonId, startDate, endDate) {
                // let IQ_DashboardRepository: any
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetProceduresPerMonthBySurgeon(surgeonId, startDate, endDate, function (result, event) {
                    //console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getSurgeonPhaseCount = function (id, startDate, endDate) {
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetSurgeonPhaseCount(id, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getPracticePhaseCount = function (id, startDate, endDate) {
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPracticePhaseCount(id, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getMonthlyPracticeSensorAverages = function (id, startMonth, startYear, endMonth, endYear) {
                console.log(id, startMonth, startYear, endMonth, endYear);
                /// change this before deployment - set for test data
                // startMonth = 4;
                // startYear = 2016;
                // endMonth = 8;
                // endYear = 2016;
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getMonthlySurgeonSensorAverages = function (id, startMonth, startYear, endMonth, endYear) {
                console.log(startMonth, startYear, endMonth, endYear);
                /// change this before deployment - set for test data
                // startMonth = 4;
                // startYear = 2016;
                // endMonth = 8;
                // endYear = 2016;
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetSurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            return PatientListFactory;
        }());
        PatientListFactory.inject = ['$q', '$rootScope', '$http'];
        services.PatientListFactory = PatientListFactory;
        angular
            .module('orthosensor.services')
            .service('PatientListFactory', PatientListFactory);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientList.service.js.map