module orthosensor.dashboard {

    class PatientTestController {
        public static $inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        public title: string;
        public patientsUnderBundleTitle: string;
        public patients: any[];
        public patientsUnderBundleCount: number;
        public DashboardDataService: any;
        public PatientsUnderBundleService: any;
        public patientsScheduled: number;

        public logo: any;

        public PatientService: any;
        public $state: any;
        constructor(private DashboardService: any, DashboardDataService: any, PatientsUnderBundleService: any, PatientService: any, public UserService: orthosensor.services.UserService, $state: any) {
            this.DashboardDataService = DashboardDataService;
            this.PatientService = PatientService;
            this.PatientsUnderBundleService = PatientsUnderBundleService;
            this.$state = $state;
            this.patientsUnderBundleTitle = 'Patients Under Bundle';
            // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
        }
        public $onInit(): void {
            let practiceId: string = '0017A00000M803ZQAR';

            practiceId = this.UserService.user.accountId;
            //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            
        }
    }
    class PatientTest implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                practiceId: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = PatientTestController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/patientTest.component.html';
        }

    }
    angular
        .module('orthosensor.dashboard')
        .component('osPatientTest', new PatientTest());
}