module orthosensor.admin {
    // import { Practice } from '../domains/practice';
    // import Modal = 
    export class PracticeSummaryController {
        static $inject: Array<string> = ['$uibModal'];
        public title: string = 'Practices';
        public practices: orthosensor.domains.Practice[] = [
            {
                id: '989098',
                name: 'Practice A',
                parentId: '',
                parentName: '',
            },
            {
                id: '989097',
                name: 'Practice B',
                parentId: '',
                parentName: '',
            },
        ];
        constructor(private $uibModal: ng.ui.bootstrap.IModalService) {
        }

        public open(practice: orthosensor.domains.Practice) {
            let options: ng.ui.bootstrap.IModalSettings = {
                template: '<practice-detail-modal></practice-detail-modal>',
            };
            let modalInstance = this.$uibModal.open(options);

        }
    }
    angular
        .module('app')
        .controller('PracticeSummaryController', PracticeSummaryController);

}
