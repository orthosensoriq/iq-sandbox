
angular
    .module("app")
    .component("practiceSummary", {
        template: "app/components/hospital/practiceSummary.component.html",
        bindings: {

        },
        controller: "PracticeSummaryController",
        controllerAs: "vm",
    });
