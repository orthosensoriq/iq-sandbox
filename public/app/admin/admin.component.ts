var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('admin', {
        templateUrl: "app/admin/admin.component.html",
        controller: 'AdminController',
        controllerAs: '$ctrl'
    });
})(app || (app = {}));
