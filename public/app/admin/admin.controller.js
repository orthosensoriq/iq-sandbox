(function () {
    'use strict';
    var controllerId = 'AdminController';
    AdminController.$inject = ['$timeout', 'UserService'];
    function AdminController($timeout, UserService) {
        /* jshint validthis:true */
        var $ctrl = this;
        $ctrl.title = 'Admin Test';
        getUser = getUser;
        setUser = setUser;
        $ctrl.addSensor = addSensor;
        activate();
        function activate() {
            // [setUser()];
        }
        //         function login(force) {
        // force.login().then(function () {
        //                 force.query('select id, name from contact limit 50')
        //                 .then(
        //                     function (contacts) {
        //                         vm.contacts = contacts.records;
        //                         console.log(vm.contacts);
        //                     });
        //             });
        //         }
        function getUser() {
            // $ctrl.user = UserService.getUser();
        }
        function setUser() {
            UserService.setUser();
        }
        function addSensor() {
            // var l = Ladda.create(document.querySelector('.sensor-btn'));
            // console.log(l);
            $ctrl.loading = true;
            $timeout(function () {
                $ctrl.loading = false;
            }, 4000);
        }
    }
    angular
        .module('orthosensor.admin')
        .controller(controllerId, AdminController);
})();
//# sourceMappingURL=admin.controller.js.map