module orthosensor.dashboard {

    class NotificationListController  {

        public static $inject: any[] = ['UserService'];

        public title: string;
        public user: orthosensor.domains.User;
        
        private UserService: any;
        public notifications: any[];

        constructor( UserService: any) {
            
            // this.PatientService = PatientListFactory;
            this.title = 'Notifications';
            this.user = UserService.user;
           
            // console.log(this.user);
            
            // console.log(config);
            this.$onInit();
        }

        public $onInit(): void {
            this.notifications = [
                {
                    patientName: 'Jane Wales', id: 29347, message: 'Upcoming Appointment, I have some questions and concerns about my upcoming Total Knee Arthroplasty. Would you please contact me at your earliest convenience.' 
                },
                {
                    patientName: 'Robert Narnacle', id: 29532, message: 'Upcoming 4-8 Week Post-Op Follow-up, I wanted to follow up with you and see if you need me to complete any additional surveys or bring any additional documentation with me to my appointment. Also, as you requested I will send you photos of my wound to your review.'
                },
                {
                    patientName: 'Mildred Ginger', id: 21237, message: 'Upcoming Operation March 22, 2017, Dr. McCoy would you please resend the Pre-Operative Knee Injury and Osteoarthritis Outcome Score (KOOS) survey that you spoke about when we met previously as I have not received it yet.'
                },
            ];
        };

    }

    angular
        .module('orthosensor')
        .component('osNotifications', {
            controller: NotificationListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/notifications/notificationList.component.html',
            bindings: {
             
            }
        });
}


