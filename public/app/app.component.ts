module orthosensor {
    angular
        .module('orthosensor')
        .component('app', {
        controller: 'ShellCtrl',
        templateUrl: 'app/layout/shell.html'
    });
}
