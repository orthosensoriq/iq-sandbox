var orthosensor;
(function (orthosensor) {
    var reports;
    (function (reports) {
        var ReportsSidebarController = (function () {
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function ReportsSidebarController(config, UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'PROM score deltas';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            ReportsSidebarController.prototype.$onInit = function () {
            };
            ;
            return ReportsSidebarController;
        }());
        ReportsSidebarController.$inject = ['config', 'UserService'];
        angular
            .module('orthosensor')
            .component('osReportsSidebar', {
            controller: ReportsSidebarController,
            controllerAs: '$ctrl',
            templateUrl: 'app/reports/reports-sidebar.component.html',
            bindings: {}
        });
    })(reports = orthosensor.reports || (orthosensor.reports = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=reports-sidebar.component.js.map