var orthosensor;
(function (orthosensor) {
    var reports;
    (function (reports) {
        var ReportsController = (function () {
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function ReportsController(config, UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'PROM score deltas';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            ReportsController.prototype.$onInit = function () {
            };
            ;
            return ReportsController;
        }());
        ReportsController.$inject = ['config', 'UserService'];
        angular
            .module('orthosensor')
            .component('osReports', {
            controller: ReportsController,
            controllerAs: '$ctrl',
            templateUrl: 'app/reports/reports.component.html',
            bindings: {}
        });
    })(reports = orthosensor.reports || (orthosensor.reports = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=reports.component.js.map