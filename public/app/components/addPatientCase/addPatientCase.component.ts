module orthosensor {

    class AddPatientCaseController {
        public static $inject = ['$rootScope', '$state', 'UserService', 'PatientListFactory', 'PatientService', 'CaseService', 'HospitalService', '$timeout'];
        public title: string;
        public patient: orthosensor.domains.Patient;
        public surgeons: any[];
        private dateOptions: {};
        private user: orthosensor.domains.User;
        public loadingCase: any;
        public caseTypes: any[];
        public patientCase: {
            CaseTypeId: string;
            ProcedureDateString: Date;
            laterality: string;
            SurgeonID: string;
        };
        public multipleCaseTypes: boolean;
        public caseTypeDescription: string;
        constructor(
            public $rootScope: ng.IRootScopeService,
            public $state: ng.ui.IStateService,
            public UserService: orthosensor.services.UserService,
            public PatientListFactory: any,
            public PatientService: orthosensor.services.PatientService,
            public CaseService: any,
            public HospitalService: orthosensor.services.HospitalService,
            public $timeout: ng.ITimeoutService) {

            this.title = 'New Patient Case';
            //this.patient = patient;
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false
            };
            this.multipleCaseTypes = false;

        }

        $onInit() {
            this.UserService = this.UserService;
            this.user = this.UserService.user;
            this.patient = this.PatientService.patient;
            console.log(this.patient);
            let today = new Date();
            let today_utc = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), today.getUTCHours(), today.getUTCMinutes(), today.getUTCSeconds());
            console.log(today_utc);
            //this.patient.dateofBirthString = dob_utc;

            this.patientCase = {
                CaseTypeId: '',
                ProcedureDateString: today_utc,
                laterality: 'Left',
                SurgeonID: ''

            };

            console.log(this.patientCase.ProcedureDateString);

            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }

        }
        $onChanges(patient) {
            console.log(patient);
            console.log(this.patient);
            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }
        }

        loadCaseTypes() {
            console.log(this.user.accountId);
            this.PatientListFactory.loadCaseTypes(this.user.accountId)
                .then((data: any) => {
                    console.log(data);
                    this.caseTypes = data;
                    this.multipleCaseTypes = this.caseTypes.length > 1;
                    if (this.caseTypes.length === 1) {
                        this.caseTypeDescription = this.caseTypes[0].Case_Type__r.Description__c;
                        this.patientCase.CaseTypeId = this.caseTypes[0].Case_Type__c;
                    }
                }, (error: any) => {
                    console.log(error);
                });
        }

        loadSurgeons() {
            this.surgeons = this.HospitalService.surgeons;
        }

        // this.open = { procDate: false };

        open($event, whichDate) {
            //$event.preventDefault();
            //$event.stopPropagation();
            this.open[whichDate] = true;
        }

        cancel() {
            // this.$uibModalInstance.dismiss('cancel');
        }

        createCase(pageForm) {
            console.log(this.patient);
            console.log(this.patientCase);
            if (pageForm.$valid) {
                this.loadingCase = true;

                this.$timeout(() => {
                    if (this.patientCase.laterality === 'Both') {
                        this.patientCase.laterality = 'Left';
                        this.PatientListFactory.createCase(this.patient, this.patientCase)
                            .then((data: any) => {
                                this.patientCase.laterality = 'Right';
                                this.PatientListFactory.createCase(this.patient, this.patientCase)
                                    .then((data: any) => {
                                        this.$rootScope.case = data;
                                        this.CaseService.setCurrentCase(data);
                                        this.$state.go('caseDetails');
                                    }, (error: any) => {
                                        console.log(error);
                                    });

                            }, (error: any) => {
                                console.log(error);
                            });
                    } else {
                        this.PatientListFactory.createCase(this.patient, this.patientCase)
                            .then((data: any) => {
                                // console.log(data);              
                                // this.$uibModalInstance.close('OK');
                                this.$rootScope.case = data;
                                this.$state.go('caseDetails');
                            }, (error: any) => {
                                console.log(error);
                            });
                    }
                    // this.loadingCase = false;
                }, 1000);
            }  else {

                pageForm.submitted = true;
            }
            // this.loadingCase = false;
            // Ladda.stopAll();
        }
    }
    class AddPatientCase implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                patient: '<',
            };
            this.controller = AddPatientCaseController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatientCase/addPatientCase.component.html';
        }
    }

    angular
        .module('orthosensor')
        .component('osAddPatientCase', new AddPatientCase());
}