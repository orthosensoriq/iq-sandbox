var orthosensor;
(function (orthosensor) {
    var CaseSensorItemsController = (function () {
        function CaseSensorItemsController(CaseDetailsFactory, $confirm) {
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.$confirm = $confirm;
            this.onChanges = function (changesObj) { };
            this.onDestroy = function () { };
        }
        CaseSensorItemsController.prototype.$onInit = function () {
            this.procedure = this.event;
            console.log(this.event);
        };
        ;
        CaseSensorItemsController.prototype.deleteSensor = function (device) {
            var _this = this;
            this.$confirm({ text: 'Are you sure you want to delete this sensor (' + device.OS_Device__r.Device_ID__c + ')?', title: 'Delete Sensor' })
                .then(function () {
                _this.CaseDetailsFactory.DeleteSensor(device.OS_Device__r.Id, _this.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        };
        ;
        return CaseSensorItemsController;
    }());
    CaseSensorItemsController.$inject = ['CaseDetailsFactory', '$confirm'];
    var CaseSensorItems = (function () {
        function CaseSensorItems() {
            this.bindings = {
                // clinicalData: '<',
                event: '<',
            };
            this.controller = CaseSensorItemsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/caseSensorInfo/caseSensorItems.component.html';
        }
        return CaseSensorItems;
    }());
    angular
        .module('orthosensor')
        .component('osCaseSensorItems', new CaseSensorItems());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseSensorItems.component.js.map