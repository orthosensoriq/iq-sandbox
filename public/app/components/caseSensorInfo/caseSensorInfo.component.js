var orthosensor;
(function (orthosensor) {
    // 
    var CaseSensorInfoController = (function () {
        function CaseSensorInfoController(CaseDetailsFactory, SensorService) {
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.SensorService = SensorService;
            this.sensorEntryType = 'barcode';
            this.sensorEntryType = 'barcode';
        }
        ////////////////
        CaseSensorInfoController.prototype.$onInit = function () {
            console.log(this.event);
            this.sensorEntryType = 'barcode';
            this.loadSensorRefs();
        };
        // this.$onChanges = function (changesObj) { };
        // this.$onDestory = function () { };
        //Sensors
        CaseSensorInfoController.prototype.loadSensorRefs = function () {
            var _this = this;
            this.CaseDetailsFactory.loadSensorRefs()
                .then(function (data) {
                //console.log(data);
                _this.sensorRefs = data;
            }, function (error) {
            });
        };
        CaseSensorInfoController.prototype.onSelectRef = function ($item, $model, $label) {
            this.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        };
        CaseSensorInfoController.prototype.addSensor = function () {
            var l = Ladda.create(document.querySelector('.sensor-btn'));
            l.start();
            if (this.sensorEntryType === 'barcode') {
                this.SensorService.AddSensor(this.sensorCode, this.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    //loadCaseEvents();
                    this.sensorCode = '';
                    angular.element(document.querySelector('#sensorCode'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    Ladda.stopAll();
                });
            }
        };
        CaseSensorInfoController.prototype.isEnterKeyPressedSensor = function (event) {
            if (event.keyCode === 13) {
                this.addSensor();
            }
        };
        return CaseSensorInfoController;
    }());
    CaseSensorInfoController.$inject = ['CaseDetailsFactory', 'SensorService'];
    var CaseSensorInfo = (function () {
        function CaseSensorInfo() {
            this.bindings = {};
            this.controller = CaseSensorInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/caseSensorInfo/caseSensorInfo.component.html';
        }
        return CaseSensorInfo;
    }());
    angular
        .module('orthosensor')
        .component('osCaseSensorInfo', new CaseSensorInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseSensorInfo.component.js.map