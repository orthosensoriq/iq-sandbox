module orthosensor {

    // 
    class CaseSensorInfoController {
        public static $inject = ['CaseDetailsFactory', 'SensorService'];
        public sensorEntryType = 'barcode';
        public sensorRefs: any[];
        public event: any;
        public selectedRef: any;
        public sensorCode: string;
        public procedure: {
            Id: string;
        }
        constructor(public CaseDetailsFactory: any, public SensorService: any) {
            this.sensorEntryType = 'barcode';

        }
        ////////////////
        $onInit() {
            console.log(this.event);
            this.sensorEntryType = 'barcode';
            this.loadSensorRefs();
        }
        // this.$onChanges = function (changesObj) { };
        // this.$onDestory = function () { };
        //Sensors
        loadSensorRefs() {
            this.CaseDetailsFactory.loadSensorRefs()
                .then((data) => {
                    //console.log(data);
                    this.sensorRefs = data;
                }, function (error) {
                });
        }
        onSelectRef($item, $model, $label) {
            this.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        }

        addSensor() {
            var l = Ladda.create(document.querySelector('.sensor-btn'));
            l.start();
            if (this.sensorEntryType === 'barcode') {
                this.SensorService.AddSensor(this.sensorCode, this.procedure.Id)
                    .then(function (data) {
                        //console.log(data);
                        //loadCaseEvents();
                        this.sensorCode = '';
                        angular.element(document.querySelector('#sensorCode'))[0].focus();
                        Ladda.stopAll();
                    }, function (error) {
                        Ladda.stopAll();
                    });
            }
        }

        isEnterKeyPressedSensor(event) {
            if (event.keyCode === 13) {
                this.addSensor();
            }
        }
    }

    class CaseSensorInfo implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                // clinicalData: '<',
                // event: '<',
                // case: '<',
                // procedure: '<',
            };
            this.controller = CaseSensorInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/caseSensorInfo/caseSensorInfo.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osCaseSensorInfo', new CaseSensorInfo());
}

