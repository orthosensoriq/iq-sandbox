var orthosensor;
(function (orthosensor) {
    var EditProcedureController = (function () {
        function EditProcedureController(PatientService, CaseDetailsFactory, DashboardService, CaseService, $timeout, $state, HospitalService) {
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.DashboardService = DashboardService;
            this.CaseService = CaseService;
            this.$timeout = $timeout;
            this.$state = $state;
            this.HospitalService = HospitalService;
            this.popup = {
                opened: false
            };
            this.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(2014, 1, 1),
                startingDay: 1,
                showWeeks: false
            };
        }
        EditProcedureController.prototype.$onInit = function () {
            this.patient = this.PatientService.getPatient();
            this.surgeons = this.HospitalService.surgeons;
            var procedure = this.CaseService.getProcedure();
            var patientCase = this.CaseService.currentCase;
            console.log(procedure);
            var clinicalData = this.CaseService.clinicalData;
            this.procedureData = {
                Id: '',
                caseId: '',
                physician: '',
                laterality: '',
                procedureDateString: '',
                patientConsentObtained: false,
                anesthesiaType: ''
            };
            this.procedureData.Id = procedure.Id;
            this.procedureData.caseId = patientCase.Id;
            this.procedureData.physician = procedure.Physician__c;
            this.procedureData.laterality = patientCase.Laterality__c;
            var procDate = new Date(procedure.Appointment_Start__c);
            console.log(procDate);
            console.log(new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds()));
            this.procedureData.procedureDateString = new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds());
            this.procedureData.patientConsentObtained = clinicalData.Patient_Consent_Obtained__c;
            this.procedureData.anesthesiaType = clinicalData.Anesthesia_Type__c;
            console.log(this.procedureData.procedureDateString);
        };
        EditProcedureController.prototype.completeEvent = function (eventId) {
            this.CaseDetailsFactory.CompleteEvent(eventId)
                .then(function (data) {
                //console.log(data);
                this.$state.go('patientDetails');
            }, function (error) {
            });
        };
        ;
        EditProcedureController.prototype.cancel = function () {
            this.modalInstance.dismiss('cancel');
        };
        ;
        // this.open = { procDate: false };
        EditProcedureController.prototype.open = function () {
            this.popup.opened = true;
        };
        ;
        EditProcedureController.prototype.updateProcedure = function () {
            var _this = this;
            // this.case.Laterality__c = this.procedureData.laterality;
            this.CaseDetailsFactory.UpdateProcedure(this.procedureData)
                .then(function (data) {
                //console.log(data);
                _this.modalInstance.dismiss();
                _this.$state.go(_this.$state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        };
        ;
        return EditProcedureController;
    }());
    EditProcedureController.$inject = ['PatientService', 'CaseDetailsFactory', 'DashboardService', 'CaseService', '$timeout', '$state', 'HospitalService'];
    var EditProcedure = (function () {
        function EditProcedure() {
            this.bindings = {
                modalInstance: '<',
            };
            this.controller = EditProcedureController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/editProcedure/editProcedure.component.html';
        }
        return EditProcedure;
    }());
    angular
        .module('orthosensor')
        .component('osEditProcedure', new EditProcedure());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=editProcedure.component.js.map