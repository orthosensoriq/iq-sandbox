module orthosensor {

    class EditProcedureController {
        public static $inject = ['PatientService', 'CaseDetailsFactory', 'DashboardService', 'CaseService', '$timeout', '$state', 'HospitalService'];

        public procedureData: {
            Id: string,
            caseId: string,
            physician: string,
            laterality: string,
            procedureDateString: string,
            patientConsentObtained: boolean,
            anesthesiaType: string
        };
        public popup = {
            opened: false
        }
        public patient: {};
        public surgeons: orthosensor.domains.Surgeon[];
        public dateOptions: {};
        constructor(public PatientService: orthosensor.services.PatientService,
            public CaseDetailsFactory: any,
            public DashboardService: orthosensor.services.DashboardService,
            public CaseService: orthosensor.services.CaseService,
            public $timeout: ng.ITimeoutService,
            public $state: ng.ui.IStateService,
            public HospitalService: orthosensor.services.HospitalService) {
            this.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(2014, 1, 1),
                startingDay: 1,
                showWeeks: false
            };
        }

        $onInit() {
            this.patient = this.PatientService.getPatient();
            this.surgeons = this.HospitalService.surgeons;

            let procedure: {} = this.CaseService.getProcedure();
            let patientCase = this.CaseService.currentCase;
            console.log(procedure);
            let clinicalData = this.CaseService.clinicalData;
            this.procedureData = {
                Id: '',
                caseId: '',
                physician: '',
                laterality: '',
                procedureDateString: '',
                patientConsentObtained: false,
                anesthesiaType: ''
            };
            this.procedureData.Id = procedure.Id;
            this.procedureData.caseId = patientCase.Id;
            this.procedureData.physician = procedure.Physician__c;
            this.procedureData.laterality = patientCase.Laterality__c;
            let procDate: Date = new Date(procedure.Appointment_Start__c);
            console.log(procDate);
            console.log(new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds()));
            this.procedureData.procedureDateString = new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds());
            this.procedureData.patientConsentObtained = clinicalData.Patient_Consent_Obtained__c;
            this.procedureData.anesthesiaType = clinicalData.Anesthesia_Type__c;
            console.log(this.procedureData.procedureDateString);

        }

        completeEvent(eventId) {
            this.CaseDetailsFactory.CompleteEvent(eventId)
                .then(function (data) {
                    //console.log(data);
                    this.$state.go('patientDetails');
                }, function (error) {
                });
        };

        cancel() {
            this.modalInstance.dismiss('cancel');
        };
        // this.open = { procDate: false };
        open() {
            this.popup.opened = true;
        };
        updateProcedure() {
            // this.case.Laterality__c = this.procedureData.laterality;
            this.CaseDetailsFactory.UpdateProcedure(this.procedureData)
                .then((data) => {
                    //console.log(data);
                    this.modalInstance.dismiss();
                    this.$state.go(this.$state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
        };

    }

    class EditProcedure implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                modalInstance: '<',
                // resolve: '<',
            };
            this.controller = EditProcedureController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/editProcedure/editProcedure.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osEditProcedure', new EditProcedure());
}