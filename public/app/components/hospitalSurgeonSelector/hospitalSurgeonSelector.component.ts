(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osHospitalSurgeonSelector', {
        templateUrl: 'app/components/hospitalSurgeonSelector.component.html',
        controller: hospitalSurgeonSelectorController,
        bindings: {
            hospital: '=',
            surgeon: '='
        }
    });
    hospitalSurgeonSelectorController.$inject = ['$rootScope'];
    function hospitalSurgeonSelectorController($rootScope) {
        let $ctrl = this;
        ////////////////
        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };
    }
})();
