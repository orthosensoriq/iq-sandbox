(function () {
    'use strict';
    angular
        .module('orthosensor')
        .component('osSurveyList', {
        templateUrl: 'app/components/takeSurvey/takeSurvey.component.html',
        controller: SurveyListController,
        bindings: {
            event: '<'
        },
    });
    SurveyListController.$inject = ['$q', '$rootScope', 'CaseService', 'SurveyService', 'CaseDetailsFactory', 'PatientService', '$uibModal', 'logger'];
    function SurveyListController($q, $rootScope, CaseService, SurveyService, CaseDetailsFactory, PatientService, $uibModal, logger) {
        var $ctrl = this;
        $ctrl.surveys = [];
        $ctrl.completedSurveys = [];
        $ctrl.event = {};
        $ctrl.patient = {};
        $ctrl.openQRCode = openSurveyLinkQRCode;
        $ctrl.isSurveyComplete = isSurveyComplete;
        $ctrl.escapeURL = escapeURL;
        $ctrl.sendQRCode = sendQRCode;
        ////////////////
        $ctrl.$onInit = function () {
            $ctrl.case = CaseService.getCurrentCase();
            $ctrl.event = CaseDetailsFactory.getEvent();
            $ctrl.surveys = SurveyService.getSurveys();
            $ctrl.patient = PatientService.patient;
            // console.log($ctrl.patient);
            // console.log($ctrl.surveys);
            processSurveys();
            getCompletedSurveys();
        };
        $ctrl.$onChanges = function (changesObj) {
            if (changesObj.event) {
                $ctrl.case = CaseService.getCurrentCase();
                $ctrl.event = CaseDetailsFactory.getEvent();
                getCompletedSurveys();
                $ctrl.surveys = setSurveyList(changesObj.event.currentValue);
            }
        };
        $ctrl.$onDestroy = function () { };
        function getCompletedSurveys() {
            CaseService.getSurveyCompletionDates($ctrl.case.Id)
                .then(function (data) {
                // console.log(data);
                $ctrl.completedSurveys = data;
            });
        }
        function setSurveyAsComplete() {
            console.log($ctrl.event);
            if ($ctrl.completedSurveys) {
                if ($ctrl.completedSurveys.length > 0) {
                    for (var i = 0; i < $ctrl.surveys.length; i++) {
                        // let complete = $ctrl.eventsToComplete[i];
                        console.log($ctrl.completedSurveys);
                        for (var j = 0; j < $ctrl.completedSurveys; j++) {
                            if ($ctrl.surveys[i].Survey_Name__c === $ctrl.completedSurveys[j].Survey__r.Name__c && event.Event_Type_Name__c === $ctrl.completedSurveys[j].Event__r.Event_Type_Name__c) {
                                console.log('Events match: ' + event.Event_Type_Name__c);
                                //console.log(today)
                                // if its before today
                                //if (complete.End_Date__c <= today) {
                                return true;
                                //} else {
                                //    return false;
                                //}
                            }
                        }
                    }
                }
            }
            return false;
        }
        function setSurveyList(event) {
            // make sure we have the right event
            // $ctrl.event = CaseDetailsFactory.getEvent();
            console.log(event.Id);
            SurveyService.retrieveSurveysByEventId(event.Id)
                .then(function (data) {
                $ctrl.surveys = data;
                console.log($ctrl.surveys);
                SurveyService.setSurveys(data);
                processSurveys();
            });
        }
        function escapeURL(urlString) {
            return urlString.replace(/&amp;/g, '&');
        }
        function sendQRCode(target) {
            console.log(target);
            var physician = CaseService.getPhysician();
            console.log(physician);
            // console.log($ctrl.patient.firstName + ' ' + $ctrl.patient.lastName);
            // TO DO: get survey name
            // TO DO: get surgeon address and Practice
            var body = '<div><p>From: Dr. ' + physician.name + '</p><p>Subject: KOOS Knee Survey</p><p>Dear ' + $ctrl.patient.firstName + ' ' + $ctrl.patient.lastName + ', </p><p>Dr.' + physician.name + ' is sending you this KOOS Survey to determine how your knee is affecting your quality of life. </p><p>This information will help us keep track of how you feel about your knee and how well you are able to perform your usual activities.</p><p>Answer every question by selecting the appropriate answer, only one answer for each question. If you are unsure about how to answer a question, please give the best answer you can. </p><p>To start the survey, please click this link: <a href =' + target + '>KOOS Survey</a></p><p>Best Regards,</p><p style="font-weight:600;">Dr. ' + physician.name + '</p><p>Orthosensor Practice</p><address>123 Main Street<br>Dania Beach, FL 33004<br>954-999-9090</address></div>';
            var subject = 'Knee Survey';
            var to = 'OIQDemoPatient@orthosensor.com';
            // let to = 'richard.salit@orthosensor.com';
            var from = 'Dr. Matt Foley';
            var deferred = $q.defer();
            IQ_SurveyRepository.SendEmail(from, to, subject, body, function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                logger.logSuccess('Email sent!', '', '', true);
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function openSurveyLinkQRCode(target) {
            console.log(target);
            var modalInstance = $uibModal.open({
                //templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_surveyLinkQRCode',
                templateUrl: 'app/components/surveyLinkQRCode/surveyLinkQRCode.modal.html',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, PatientListFactory) {
                    var vm = this;
                    console.log(target);
                    vm.surveyURL = target;
                    vm.close = close;
                    function close() {
                        // console.log('return from modal');
                        // getCompletedSurveys();
                        // $ctrl.surveys = setSurveyList($ctrl.event);
                        $uibModalInstance.close('OK');
                    }
                },
                controllerAs: 'vm',
                size: 'sm'
            });
            modalInstance.result.then(function (selectedItem) {
                console.log('return from modal');
                // getCompletedSurveys();
                // $ctrl.surveys = setSurveyList($ctrl.event);
            }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
            });
        }
        function processSurveys() {
            if ($ctrl.surveys) {
                if ($ctrl.surveys.length) {
                    for (var i = 0; i < $ctrl.surveys.length; i++) {
                        isSurveyComplete($ctrl.surveys[i]);
                    }
                }
            }
            console.log($ctrl.surveys);
        }
        function isSurveyComplete(survey) {
            var completed = false;
            if ($ctrl.completedSurveys) {
                if ($ctrl.completedSurveys.length > 0) {
                    // console.log('Found completed surveys');
                    // console.log($ctrl.completedSurveys);
                    for (var j = 0; j < $ctrl.completedSurveys.length; j++) {
                        // console.log(survey.Name__c);
                        if ($ctrl.completedSurveys[j].Survey__r) {
                            // console.log($ctrl.completedSurveys[j].Survey__r.Name__c);
                        }
                        // console.log($ctrl.event.Event_Type_Name__c);
                        // console.log($ctrl.completedSurveys[j].Event__r.Event_Type_Name__c);
                        if ($ctrl.completedSurveys[j].Survey__r) {
                            if (survey.Name__c === $ctrl.completedSurveys[j].Survey__r.Name__c && $ctrl.event.Event_Type_Name__c === $ctrl.completedSurveys[j].Event__r.Event_Type_Name__c) {
                                // console.log($ctrl.completedSurveys[j]);
                                // console.log('Events match: ' + $ctrl.event.Event_Type_Name__c)
                                if ($ctrl.completedSurveys[j].End_Date__c) {
                                    survey.EndDate = $ctrl.completedSurveys[j].End_Date__c;
                                    survey.Completed = true;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            survey.Completed = false;
            return completed;
        }
    }
})();
//# sourceMappingURL=SurveyList.component.js.map