module orthosensor {

    class AddPatientController {
        public static $inject = ['$rootScope', '$state', '$timeout', 'UserService', 'PatientListFactory', 'PatientService', 'PatientDataService', 'HospitalService', 'PatientDetailsFactory', 'SFDataService', 'moment'];

        public title: string;
        public user: orthosensor.domains.User;
        public isUserAdmin: Boolean;
        public patient: orthosensor.domains.Patient;
        public hospitals: any[];
        public practices: any[];
        public dateOfBirthString: string;
        public dateOptions: {};
        public birthYears: any[];
        public maxBirthDate: Date;
        public PatientListFactory: any;
        // public PatientService: orthosensor.services.PatientService;
        public languages: string[];
        public races: string[];
        public mode: string;
        matchedPatient: { };


        constructor(public $rootScope: ng.IRootScopeService,
            public $state: ng.ui.IStateService,
            public $timeout: ng.ITimeoutService,
            public UserService: orthosensor.services.UserService,
            PatientListFactory: any, public PatientService: orthosensor.services.PatientService,
            public PatientDataService: orthosensor.services.PatientDataService,

            public HospitalService: orthosensor.services.HospitalService,
            public PatientDetailsFactory: any,
            public SFDataService: orthosensor.services.SFDataService,
            public moment: any) {

            this.title = 'New Patient';

            this.user = UserService.user;

            this.PatientListFactory = PatientListFactory;
        }
        $onInit() {
            this.user = this.UserService.user;

            console.log(this.user);
            this.isUserAdmin = this.UserService.isAdmin;
            this.hospitals = this.HospitalService.hospitals;
            this.practices = this.HospitalService.practices;
            console.log(this.practices);
            this.languages = this.PatientDataService.getLanguages();
            this.races = this.PatientDataService.getRaces();
            console.log(this.hospitals);
            console.log(this.practices);
            console.log(this.PatientService.patient);
            if (this.PatientService.patient === null) {
                this.createPatient();
                this.mode = 'Create';
            } else {
                this.patient = this.PatientService.patient;
                this.mode = this.patient.hl7 === true ? 'Create' : 'Edit';
                console.log(this.mode);

                let dob = new Date(this.patient.dateOfBirth);
                let dob_utc = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
                console.log(dob_utc);
                this.patient.dateOfBirthString = dob_utc;
                let maxDate = new Date();

                this.maxBirthDate = new Date(maxDate.getUTCFullYear(), maxDate.getUTCMonth(), maxDate.getUTCDate(), maxDate.getUTCHours(), maxDate.getUTCMinutes(), maxDate.getUTCSeconds());
                console.log(dob_utc);
                //this.patient.dateOfBirthString = new Date(this.patient.dateOfBirth);
                //this.patient.dateofBirthString = new Date();;
                console.log(this.patient);
                this.title = 'Edit Patient';
                // this.mode = 'Edit';
            }
            let today = new Date();
            let startYear = this.moment().subtract(65, 'year').year();
            let month = this.moment(today).month();
            let day = this.moment(today).day();
            console.log(startYear + ',' + month + ',' + day)
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false,
                initDate: new Date(startYear, month, day),
                maxDate: new Date(),
                minDate: new Date(1920, 1, 1)
            };
            // this.patient.dateofBirthString = new Date(1945, 8, 1);

            this.birthYears = [];
            for (var i = new Date().getFullYear(); i > 1920; i--) {
                this.birthYears.push(i);
            }
        }

        //this.open = { birthDate: false };
        open($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            this.open[whichDate] = true;

        }

        cancel() {
            this.$state.go('patientList');
        }

        resetPractice() {
            this.SFDataService.loadHospitalPractices(this.patient.hospital.id)
                .then((data: any) => {
                    console.log(data);
                    this.practices = this.HospitalService.convertSFPracticeToObject(data);
                }, (error) => {
                    console.log(error);
                });
            this.patient.practice = undefined;
        }
        checkAnonymous() {
            var accountId = this.patient.hospital;
            console.log(this.patient.hospital);
            console.log(this.patient.hl7);
            console.log(this.patient);
            if (this.patient.practice !== undefined && this.patient.practice.length > 0) {
                accountId = this.patient.practice;
            }
            this.PatientService.checkAnonymous(accountId)
                .then((data: Boolean) => {
                    //console.log(data);
                    this.patient.anonymous = data;
                }, function (error) {
                    console.log(error);
                });
        }

        createPatient() {
            this.patient = new orthosensor.domains.Patient();
            this.patient.anonymous = this.UserService.isAdmin ? this.user.anonymousPatients : false;
            this.patient.hl7 = false;
            this.patient.hospitalId = this.UserService.isAdmin ? null : this.user.accountId;
            this.patient.hospital = '';     //For Internal Users only - required
            this.patient.practice = '';     //For Internal Users only - optional
            this.patient.practiceId = this.UserService.isAdmin ? null : this.user.accountId;
            this.patient.lastName = '';
            this.patient.firstName = '';
            this.patient.label = '';
            this.patient.medicalRecordNumber = '';
            this.patient.email = '';
            this.patient.dateOfBirthString = '';
            this.patient.birthYear = null;
            this.patient.gender = '';
            this.patient.socialSecurityNumber = '';
            this.patient.patientNumber = '';
            this.patient.accountNumber = '';
            this.patient.race = '';
            this.patient.language = '';
        }
        createNewPatient(pageForm: any) {
            if (pageForm.$valid) {

                this.loadingPatient = true;
                this.$timeout(function () {
                    this.PatientDataService.createPatient(this.patient)
                        .then((data: any) => {
                            console.log(data);
                            this.PatientService.patient = data;

                            this.$state.go('patientDetails');
                            this.loading = false;
                        }, function (error: any) {
                            console.log(error);
                        });


                }, 1000);
            }
            else {

                pageForm.submitted = true;
            }
            //this.loading = false;
        }

        savePatientOnly(pageForm: any) {
            this.savePatient(pageForm, 'patientList');
//            this.$state.go('patientList');
        }

        savePatientAndAddCase(pageForm: any) {
            this.savePatient(pageForm, 'addPatientCase');
            this.$state.go('addPatientCase');
        }
        savePatient(pageForm: any, nextForm: string) {
            console.log(this.mode);
            if (pageForm.$valid) {
                this.loadingPatient = true;
                if (!this.isUserAdmin) {
                    this.patient.hospitalId = this.user.accountId;
                } else {
                    console.log(this.patient.practice);
                    this.patient.hospitalId = this.patient.practice;
                }

                console.log(this.patient);
                this.PatientService.patient = this.patient;
                if (this.mode === 'Create') {
                    this.PatientDataService.checkDuplicate(this.patient)
                        .then((data) => {
                            //console.log(data); 
                            if (data != null) {
                                this.matchedPatient = data;
                            } else {
                                this.PatientDataService.createPatient(this.patient)
                                    .then((newPatient) => {
                                        this.PatientService.patient.id = newPatient.Id;
                                        this.$state.go(nextForm);
                                    }, function (error) {
                                        console.log(error);
                                    })
                            }
                        }, (error) => {
                            console.log(error);
                        });

                } else {
                    // let sfPatient = this.PatientService.getSFObject();
                    console.log(this.patient);
                    this.PatientDataService.updatePatient(this.patient)
                        .then((data: any) => {
                            console.log(data);
                            // this.PatientService.patient = data;
                            // $rootScope.patient = data;
                            console.log(data);
                            //PatientService.setPatient(data);
                            this.$state.go(nextForm);
                        }, (error: any) => {
                            console.log(error);
                        });
                }
            } else {
                pageForm.submitted = true;
            }
        }

    }

    class AddPatient implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {

            };
            this.controller = AddPatientController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatient/addPatient.component.html';
        }

    }
    angular
        .module('orthosensor')
        .component('osAddPatient', new AddPatient());

}
