(function () {
    'use strict';
    angular
        .module('orthosensor')
        .component('osAddCaseNote', {
        templateUrl: 'app/components/caseNotes/addCaseNote.component.html',
        controller: AddCaseNoteController,
        bindings: {
            procedure: '<'
        },
    });
    AddCaseNoteController.inject = ['$state', '$uibModalInstance', 'CaseDetailsFactory', 'CaseService'];
    function AddCaseNoteController($state, $uibModalInstance, CaseDetailsFactory, CaseService) {
        var $ctrl = this;
        $ctrl.addNote = addNote;
        $ctrl.procedure = CaseService.getProcedure();
        console.log($ctrl.procedure);
        $ctrl.procedureId = $ctrl.procedure.Id;
        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function addNote() {
            console.log($ctrl.caseTitle);
            console.log($ctrl.caseNote);
            console.log($ctrl.procedureId);
            CaseDetailsFactory.AddNote($ctrl.caseTitle, $ctrl.caseNote, $ctrl.procedureId)
                .then(function (data) {
                //console.log(data);
                $uibModalInstance.close('OK');
                // $state.go($state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        }
        activate();
        ////////////////
        function activate() { }
    }
})();
//# sourceMappingURL=addCaseNote.component.js.map