(function () {
    'use strict';

    angular
        .module('orthosensor')
        .controller('EditCaseNoteController', EditCaseNoteController);

    EditCaseNoteController.inject = ['$state', '$uibModalInstance', 'CaseDetailsFactory'];

    function EditCaseNoteController($state, $uibModalInstance, CaseDetailsFactory) {
        var ctrl = this;
        ctrl.caseTitle = note.Title;
        ctrl.caseNote = note.Body;
        ctrl.noteId = note.Id;
        ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        ctrl.updateNote = function () {
            CaseDetailsFactory.UpdateNote(ctrl.caseTitle, ctrl.caseNote, ctrl.noteId).then(function (data) {
                //console.log(data);
                $uibModalInstance.dismiss();
                $state.go($state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        };
    
    activate();

    ////////////////

    function activate() { }
}
})();