var orthosensor;
(function (orthosensor) {
    angular.module('orthosensor', [
        'app',
        'app.core',
        'app.common',
        'angular-confirm',
        'orthosensor.services',
        'orthosensor.data',
        'orthosensor.dashboard',
        'orthosensor.admin',
    ]);
    angular.module('app.common', []);
    angular.module('orthosensor.services', [
        'app.core'
    ]);
    angular.module('orthosensor.dashboard', [
        'app.core',
        'orthosensor.services'
    ]);
    angular.module('orthosensor.data', []);
    angular.module('orthosensor.admin', [
        'app.core',
        'orthosensor.data',
        'orthosensor.services'
    ]);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=orthosensor.module.js.map