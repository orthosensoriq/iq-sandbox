var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientTestController = (function () {
            function PatientTestController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state) {
                this.DashboardService = DashboardService;
                this.UserService = UserService;
                this.DashboardDataService = DashboardDataService;
                this.PatientService = PatientService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.$state = $state;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            PatientTestController.prototype.$onInit = function () {
                var practiceId = '0017A00000M803ZQAR';
                practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            };
            return PatientTestController;
        }());
        PatientTestController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        var PatientTest = (function () {
            function PatientTest() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = PatientTestController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/patientTest.component.html';
            }
            return PatientTest;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientTest', new PatientTest());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientTest.component.js.map