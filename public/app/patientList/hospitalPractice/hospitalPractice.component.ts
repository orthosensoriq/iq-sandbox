module orthosensor {
    class HospitalPracticeController {
        public static $inject = ['PatientListFactory', 'PatientService',
            'UserService',
            'HospitalService', '$uibModal'];
        public currentHospitalId: string;
        public currentPracticeId: string;
        public currentHospitalName: string;
        public currentPracticeName: string;
        constructor(public PatientListFactory: any,
            public PatientService: orthosensor.services.PatientService,
            public CaseDetailsFactory: any,
            public UserService: orthosensor.services.UserService,
            public HospitalService: orthosensor.services.HospitalService,
            public $uibModal: ng.ui.bootstrap.IModalService)
        { }
        openHospitalFilter() {
            let options: ng.ui.bootstrap.IModalSettings = {
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            };
            this.$uibModal.open(options).result
                .then(() => {
                    // getPatients();
                    // loadSurgeons();
                    this.currentHospitalId = this.PatientListFactory.getFilterHospitalId();
                    this.currentPracticeId = this.PatientListFactory.getFilterPracticeId();
                    this.currentHospitalName = this.HospitalService.getHospitalName(this.currentHospitalId);
                    this.currentPracticeName = this.HospitalService.getPracticeName(this.currentPracticeId);
                    console.log(this.PatientService.currentFilterId);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        }
        getPracticeName(id: string) {
            console.log('Searching for practice: ' + id);
            for (let i = 0; i < this.HospitalService.practices.length; i++) {
                console.log(this.HospitalService.practices[i].Id);
                if (this.HospitalService.practices[i].Id === id) {
                    console.log(this.HospitalService.practices[i].Id + ', ' + id);
                    return this.HospitalService.practices[i].Name;
                }
            }
            return '';
        }
        getHospitalName(id: string) {
            console.log('Searching for hospital: ' + id);
            for (let i = 0; i < this.HospitalService.hospitals.length; i++) {
                if (this.HospitalService.hospitals[i].Id === id) {
                    return this.HospitalService.hospitals[i].Name;
                }
            }
            return '';
        }

    }
    class HospitalPractice implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                hospital: '=',
                practice: '='
            };
            this.controller = HospitalPracticeController;
            this.templateUrl = 'app/patientList/hospitalPractice/hospitalPractice.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osHospitalPractice', new HospitalPractice());
}