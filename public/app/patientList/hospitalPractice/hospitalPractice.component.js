var orthosensor;
(function (orthosensor) {
    var HospitalPracticeController = (function () {
        function HospitalPracticeController(PatientListFactory, PatientService, CaseDetailsFactory, UserService, HospitalService, $uibModal) {
            this.PatientListFactory = PatientListFactory;
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.UserService = UserService;
            this.HospitalService = HospitalService;
            this.$uibModal = $uibModal;
        }
        HospitalPracticeController.prototype.openHospitalFilter = function () {
            var _this = this;
            var options = {
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            };
            this.$uibModal.open(options).result
                .then(function () {
                // getPatients();
                // loadSurgeons();
                _this.currentHospitalId = _this.PatientListFactory.getFilterHospitalId();
                _this.currentPracticeId = _this.PatientListFactory.getFilterPracticeId();
                _this.currentHospitalName = _this.HospitalService.getHospitalName(_this.currentHospitalId);
                _this.currentPracticeName = _this.HospitalService.getPracticeName(_this.currentPracticeId);
                console.log(_this.PatientService.currentFilterId);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
        HospitalPracticeController.prototype.getPracticeName = function (id) {
            console.log('Searching for practice: ' + id);
            for (var i = 0; i < this.HospitalService.practices.length; i++) {
                console.log(this.HospitalService.practices[i].Id);
                if (this.HospitalService.practices[i].Id === id) {
                    console.log(this.HospitalService.practices[i].Id + ', ' + id);
                    return this.HospitalService.practices[i].Name;
                }
            }
            return '';
        };
        HospitalPracticeController.prototype.getHospitalName = function (id) {
            console.log('Searching for hospital: ' + id);
            for (var i = 0; i < this.HospitalService.hospitals.length; i++) {
                if (this.HospitalService.hospitals[i].Id === id) {
                    return this.HospitalService.hospitals[i].Name;
                }
            }
            return '';
        };
        return HospitalPracticeController;
    }());
    HospitalPracticeController.$inject = ['PatientListFactory', 'PatientService',
        'UserService',
        'HospitalService', '$uibModal'];
    var HospitalPractice = (function () {
        function HospitalPractice() {
            this.bindings = {
                hospital: '=',
                practice: '='
            };
            this.controller = HospitalPracticeController;
            this.templateUrl = 'app/patientList/hospitalPractice/hospitalPractice.component.html';
        }
        return HospitalPractice;
    }());
    angular
        .module('orthosensor')
        .component('osHospitalPractice', new HospitalPractice());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=hospitalPractice.component.js.map