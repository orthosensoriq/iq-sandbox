var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('patientListHeader', {
        template: "\n            <div class=\"dropdown\">\n                <button id=\"main-navigation\" \n                    class=\"btn btn-os navigation-action\" type=\"button\" data-toggle=\"dropdown\" \n                    aria-haspopup=\"true\" aria-expanded=\"false\">\n                    <i class=\"glyphicon glyphicon-menu-hamburger\"></i>\n                </button>\n                <ul class=\"dropdown-menu\" aria-labelledby=\"dLabel\">\n                    <li><a><i class=\"glyphicon glyphicon-list\"></i> Patients</a></li>\n                    <li><a href=\"#\" ui-sref=\"dashboard\">\n                    <i class=\"glyphicon glyphicon-stats\"></i> Dashboard</a>\n                    </li>\n                </ul>\n            </div>\n\n            <h1>Patients</h1>",
        controller: 'PatientListController'
    });
})(app || (app = {}));
//# sourceMappingURL=patientListHeader.component.js.map 
//# sourceMappingURL=patientListHeader.component.js.map