(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osPatientListFilter', {
        // template:'htmlTemplate',
        templateUrl: 'app/patientList/patientListFilter.component.html',
        controller: PatientListFilterController,
        bindings: {
            Binding: '=',
        },
    });
    PatientListFilterController.inject = ['$rootScope'];
    function PatientListFilterController($rootScope) {
        var ctrl = this;
        ctrl.currentHospital = 'Orthosensor Hospital';
        ctrl.logo = $rootScope.globalStaticResourcePath + '/images/OrthoLogiQLogo_darkblue.png';
        ////////////////
        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
    }
})();
//# sourceMappingURL=patientListFilter.component.js.map