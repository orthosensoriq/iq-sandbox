(function () {
    'use strict';
    function PatientSummaryController($ctrl, $rootScope, $state, $filter, $uibModal, PatientListFactory, PatientService, CaseDetailsFactory, UserService) {
        //variables
        $ctrl.patientSearchValue = '';
        $ctrl.title = 'Patients';
        $ctrl.activeTab = 'existing-patients';
        $ctrl.surgeonFilters = [];
        $ctrl.sourceFilter = 'Existing';
        $ctrl.filterOpen = false;
        $ctrl.filterDrillDown = false;
        $ctrl.procedureDateFilter = false;
        //methods
        $ctrl.addNewPatient = addNewPatient;
        $ctrl.editPatient = editPatient;
        $ctrl.searchPatients = searchPatients;
        $ctrl.openHospitalFilter = openHospitalFilter;
        $ctrl.currentHospital = currentHospital;
        $ctrl.procedureDateFilterValue = null;
        $ctrl.canChangeHospitalPracticeView = false;

        activate();

        //implementations
        if ($rootScope.filterHospitalId === undefined) {
            $rootScope.filterHospitalId = null;
        }

        //add more start up to this as we refactor        
        function activate() {
            console.log('initializing controller!');
            getUserRights();
            getUserDefaultHospital();
            if ($rootScope.userDefaultHospital) {
                console.log($rootScope.userDefaultHospital);
                $rootScope.filterHospitalId = $rootScope.userDefaultHospital.Id;
            }
            loadHospitals();
            loadPractices();
            loadSurgeons();

            getPatients();
            getHL7Patients();
            if (currentHospital() !== 'All Hospitals') {
                getHospitalPractices();
            }
            getHospitalPractices();

        }

        function getUserRights() {
            $ctrl.isUserAdmin = $rootScope.isUserAdmin;
            // if user is not an admin, see if there are practices for their hospital
            if (!$ctrl.isUserAdmin) {
                $ctrl.canChangeHospitalPracticeView = userHasPractices();

            } else {
                $ctrl.canChangeHospitalPracticeView = true;
            }
        }

        function getUserDefaultHospital() {
            if ($rootScope.currentUser.Contact != null) {
                $rootScope.userDefaultHospital = $rootScope.currentUser.Contact.Account;
                console.log($rootScope.userDefaultHospital);
                return $rootScope.userDefaultHospital;
            } else {
                //  no default for admin!!
            }
        }

        function getPatients() {
            console.log($rootScope.filterHospitalId);
            // if ($rootScope.filterHospitalId) {
            PatientListFactory.getPatients($rootScope.filterHospitalId)
                .then(function (data) {
                    $ctrl.patients = data;
                    $ctrl.filteredPatients = data;
                    console.log($ctrl.filteredPatients);
                }, function (error) {
                });
            // }
        }

        function getHL7Patients() {
            if ($rootScope.filterHospitalId) {
                PatientListFactory.getHL7Patients($rootScope.filterHospitalId).then(function (data) {
                    //console.log(data);
                    $ctrl.hl7patients = data;
                }, function (error) {
                });
            }
        }
        function loadSurgeons() {
            // get surgeons at start up        
            console.log($rootScope.filterHospitalId);
            if ($rootScope.filterHospitalId) {
                PatientListFactory.loadSurgeons($rootScope.filterHospitalId)
                    .then(function (data) {
                        //console.log(data);
                        $ctrl.surgeons = data;
                    }, function (error) {
                        console.log(error);
                    });
            }
        }

        function loadHospitals() {
            PatientListFactory.loadHospitals()
                .then(function (data) {
                    console.log(data);
                    $rootScope.hospitals = data;
                }, function (error) {
                    console.log(error);
                });
        }

        function loadPractices() {
            PatientListFactory.loadPractices()
                .then(function (data) {
                    console.log(data);
                    $rootScope.practices = data;
                }, function (error) {
                    console.log(error);
                });
        }

        function getHospitalPractices() {
            if ($rootScope.filterHospitalId) {
                PatientListFactory.getHospitalPractices($rootScope.filterHospitalId)
                    .then(function (data) {
                        console.log(data);
                        $rootScope.hospitalPractices = data;
                        // userHasPractices();
                        if (!$ctrl.isUserAdmin) {
                            $ctrl.canChangeHospitalPracticeView = userHasPractices();

                        } else {
                            $ctrl.canChangeHospitalPracticeView = true;
                        }
                    }, function (error) {
                        console.log(error);
                    });
            }
        }

        function userHasPractices() {
            if ($rootScope.hospitalPractices) {
                console.log('should be true if practices exist' + ($rootScope.hospitalPractices.length > 0));
                return ($rootScope.hospitalPractices.length > 0);
            } else {
                return false;
            }
        }

        function openHospitalFilter() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            });

            modalInstance.result
                .then(function () {
                    getPatients();
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        }

        function currentHospital() {
            var hospitalName = '';
            // console.log($rootScope.currentUser);
            if ($rootScope.currentUser.Contact != null) {
                hospitalName = getHospitalNameForContact();
            } else {
                hospitalName = getHospitalNameForNonContact();
            }
            return hospitalName;
        }

        function getHospitalNameForContact() {
            var hospitalName = '';
            var contact = $rootScope.currentUser.Contact;
            if ('ParentId' in contact.Account) {
                if (contact.Account.Parent !== null && contact.Account.Parent.Name !== null) {
                    hospitalName = contact.Account.Parent.Name + ' - ' + contact.Account.Name;
                } else {
                    hospitalName = contact.Account.Name;
                }
            } else {
                hospitalName = $rootScope.currentUser.Contact.Account.Name;
            }
            return hospitalName;
        }

        function getHospitalNameForNonContact() {
            var hospitalName = '';
            if ($rootScope.filterHospitalId == null) {
                hospitalName = 'All Hospitals';
            } else {
                if ($rootScope.filterPracticeId !== undefined) {
                    for (var i = 0; i < $rootScope.practices.length; i++) {
                        if ($rootScope.practices[i].Id === $rootScope.filterPracticeId) {
                            hospitalName = $rootScope.practices[i].Parent.Name + ' - ' + $rootScope.practices[i].Name;
                        }
                    }
                } else {
                    for (var i = 0; i < $rootScope.hospitals.length; i++) {
                        if ($rootScope.hospitals[i].Id === $rootScope.filterHospitalId) {
                            hospitalName = $rootScope.hospitals[i].Name;
                        }
                    }
                }
            }
            return hospitalName;
        }

        $ctrl.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };

        $ctrl.filterClicked = function (filterType, filter, evt) {
            var $element = $(evt.currentTarget);
            if (filterType === 'surgeon') {
                var index = $ctrl.surgeonFilters.indexOf(filter);
                if (index > -1) {
                    $ctrl.surgeonFilters.splice(index, 1);
                }
                else {
                    $ctrl.surgeonFilters.push(filter);
                }
                filterPatients();
            }
            else if (filterType === 'source') {
                if ($ctrl.sourceFilter !== filter) {
                    $ctrl.sourceFilter = filter;
                    filterPatients();
                }
            }
            else if (filterType === 'date') {
                evt.preventDefault();
                evt.stopPropagation();
                $ctrl.procedureDateFilter = true;
            }
        };

        $ctrl.$watch('procedureDateFilterValue', function (newValue, oldValue) {
            filterPatients();
        });

        $ctrl.toggleFilter = function () {
            if ($ctrl.filterOpen) {
                $ctrl.filterOpen = false;
                $ctrl.filterDrillDown = false;
            }
            else {
                $ctrl.filterOpen = true;
            }
        };
        $ctrl.closeFilter = function () {
            $ctrl.filterOpen = false;
            $ctrl.filterDrillDown = false;
        };
        $ctrl.drillDownFilter = function () {
            $ctrl.filterDrillDown = true;
        };
        $ctrl.drillUpFilter = function () {
            $ctrl.filterDrillDown = false;
        };
        $ctrl.removeSurgeonFilter = function (filter) {
            var index = $ctrl.surgeonFilters.indexOf(filter);
            if (index > -1) {
                $ctrl.surgeonFilters.splice(index, 1);
            }
            filterPatients();
        };
        $ctrl.removeDateFilter = function () {
            $ctrl.procedureDateFilterValue = undefined;
            $ctrl.procedureDateFilter = false;
            filterPatients();
        };
        function filterPatients() {
            //console.log('Calling filter function');
            var source = [];
            if ($ctrl.sourceFilter === 'Existing') {
                source = $ctrl.patients;
            }
            else if ($ctrl.sourceFilter === 'Scheduled') {
                source = $ctrl.hl7patients;
            }
            if ($ctrl.surgeonFilters.length > 0) {
                $ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    for (var i = 0; i < $ctrl.surgeonFilters.length; i++) {
                        if (p.Cases__r) {
                            for (var c = 0; c < p.Cases__r.length; c++) {
                                if (p.Cases__r[c].Physician__c === $ctrl.surgeonFilters[i].Surgeon__r.AccountId) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                });
                source = $ctrl.filteredPatients;
            }
            else {
                $ctrl.filteredPatients = source;
            }
            if ($ctrl.procedureDateFilterValue) {
                $ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    if (p.Cases__r) {
                        var filterDateValue = moment($ctrl.procedureDateFilterValue);
                        var filterDate = new Date(filterDateValue);
                        filterDate = new Date(filterDate.getFullYear(), filterDate.getMonth(), filterDate.getDate());
                        var filterTime = filterDate.getTime();
                        for (var c = 0; c < p.Cases__r.length; c++) {
                            var procedureDateVale = p.Cases__r[c].Procedure_Date__c;
                            var procedureDate = new Date(procedureDateVale);
                            procedureDate = new Date(procedureDate.getFullYear(), procedureDate.getMonth(), procedureDate.getDate());
                            //procedureDate.setMinutes(procedureDate.getTimezoneOffset());
                            var procedureTime = procedureDate.getTime();
                            //console.log(procedureTime);
                            if (filterTime === procedureTime) {
                                return true;
                            }
                        }
                    }
                    return false;
                });
                source = $ctrl.filteredPatients;
                console.log(source);
            }
            else {
                $ctrl.filteredPatients = source;
            }
        }
        function addNewPatient() {
            $uibModal.open({
                templateUrl: 'app/components/addPatient/addPatient.modal.html',
                controller: 'AddPatient',
                size: 'large'
            });
        }
        $ctrl.addPatientCase = function (patient) {
            PatientService.setPatient(patient);
            console.log(patient);
            $uibModal.open({
                //template: '<add-patient-case patient=patient></add-patient-case>',
                templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
                controller: 'AddPatientCase',
                size: 'small'
            });
        };
        function editPatient(patient) {
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }
        $ctrl.patientDetails = function (patient) {
            PatientService.setPatient(patient);
            $state.go('patientDetails');
        };
        $ctrl.caseDetails = function (patient, patientCase) {
            PatientService.setPatient(patient);
            console.log(patient);
            //$rootScope.patient = patient;
            $rootScope.case = patientCase;
            //$rootScope.event = null;
            CaseDetailsFactory.LoadCaseEvents()
                .then(function (data) {
                    console.log(data);
                    var events = data;
                    for (var i = 0; i < events.length; i++) {
                        if (events[i].Event_Type_Name__c === 'Procedure') {
                            CaseDetailsFactory.setEvent(events[i]);
                        }
                    }
                    $state.go('caseDetails');
                });
        };
        // $ctrl.patientSearchValue = '';
        // $ctrl.activeTab = 'existing-patients';
        function searchPatients() {
            if ($ctrl.patientSearchValue.length > 2) {
                var hospitalId = $rootScope.filterHospitalId;
                PatientListFactory.SearchPatientList(hospitalId, $ctrl.patientSearchValue).then(function (data) {
                    //console.log(data);
                    $ctrl.searchPatientResults = data;
                }, function (error) {
                });
                PatientListFactory.SearchHL7PatientList(hospitalId, $ctrl.patientSearchValue).then(function (data) {
                    //console.log(data);
                    $ctrl.searchHL7PatientResults = data;
                }, function (error) {
                });
            }
        }
        $ctrl.searchPatientSelected = function (result) {
            //$rootScope.patient = result;
            PatientService.setPatient(result);
            $state.go('patientDetails');
        };
        $ctrl.resetSearch = function () {
            $ctrl.patientSearchValue = '';
        };
        $ctrl.searchHL7PatientSelected = function (result) {
            $uibModal.open({
                templateUrl: 'app/addPatient/addPatient.modal.html',
                controller: function ($ctrl, $rootScope, $state, $uibModalInstance, PatientListFactory) {
                    $ctrl.open = { birthDate: false };
                    $ctrl.open = function ($event, whichDate) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $ctrl.open[whichDate] = true;
                    };
                    $ctrl.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 1,
                        showWeeks: false
                    };
                    $ctrl.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $ctrl.patient = {};
                    $ctrl.patient.hl7 = true;
                    $ctrl.patient.hospital = result.SFDC_Account__c; //For Internal Users only - read-only
                    $ctrl.patient.practice = ''; //For Internal Users only - will not render
                    $ctrl.patient.HospitalId = result.SFDC_Account__c;
                    $ctrl.patient.FirstName = result.Patient_First_Name__c;
                    $ctrl.patient.LastName = result.Patient_Last_Name__c;
                    $ctrl.patient.Gender = result.Patient_Gender__c;
                    //$ctrl.patient.DateofBirthString = result.Patient_Date_Of_Birth__c;
                    $ctrl.patient.dateOfBirthString = moment.utc(result.Patient_Date_Of_Birth__c).format('MM/DD/YYYY');
                    $ctrl.patient.Race = angular.isDefined(result.Patient_Race__c) ? result.Patient_Race__c : '';
                    $ctrl.patient.Language = angular.isDefined(result.Patient_Language__c) ? result.Patient_Language__c : '';
                    $ctrl.patient.PatientNumber = '';
                    $ctrl.patient.MedicalRecordNumber = angular.isDefined(result.Medical_Record_Number__c) ? result.Medical_Record_Number__c : '';
                    $ctrl.patient.AccountNumber = angular.isDefined(result.Account_Number__c) ? result.Account_Number__c : '';
                    $ctrl.patient.SocialSecurityNumber = angular.isDefined(result.Patient_SSN__c) ? result.Patient_SSN__c : '';
                    console.log('$ctrl.patient.hospital = ' + $ctrl.patient.hospital);
                    console.log('$ctrl.patient.HospitalId = ' + $ctrl.patient.HospitalId);
                    console.log('$ctrl.patient.FirstName = ' + $ctrl.patient.FirstName);
                    $ctrl.createNewPatient = function () {
                        if ($ctrl.pageForm.$valid) {
                            PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                                //console.log(data);               
                                $uibModalInstance.dismiss();
                                PatientService.setPatient(data);
                                //$rootScope.patient = data;
                                $state.go('patientDetails');
                            }, function (error) {
                                console.log(error);
                            });
                        }
                        else {
                            Ladda.stopAll();
                            $ctrl.pageForm.submitted = true;
                        }
                    };
                    $ctrl.savePatient = function () {
                        if ($ctrl.pageForm.$valid) {
                            PatientListFactory.checkDuplicate($ctrl.patient).then(function (data) {
                                //console.log(data); 
                                if (data != null) {
                                    $ctrl.matchedPatient = data;
                                    Ladda.stopAll();
                                }
                                else {
                                    PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                                        //console.log(data);               
                                        $uibModalInstance.dismiss();
                                        PatientService.setPatient(data);
                                        //$rootScope.patient = data;
                                        $state.go('patientDetails');
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }
                            }, function (error) {
                                console.log(error);
                            });
                        }
                        else {
                            Ladda.stopAll();
                            $ctrl.pageForm.submitted = true;
                        }
                    };
                    $ctrl.createNewCase = function (matchdPatient) {
                        $uibModalInstance.dismiss();
                        $rootScope.addPatientCase(matchdPatient);
                    };
                },
                size: 'large'
            });
        };
    }

    PatientSummaryController.$inject = ['$ctrl', '$rootScope', '$state', '$filter', '$uibModal', 'PatientListFactory', 'PatientService', 'CaseDetailsFactory', 'UserService'];

    angular
        .module('orthosensor')
        .controller('PatientSummaryController', PatientSummaryController);

})();
