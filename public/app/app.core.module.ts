module orthosensor {
    angular.module('app.core', [
        // Angular modules 
        'ngAnimate',
        'ngSanitize',
        'ngTouch',
        'ngMessages',
        'ui.router',
 
        // Custom modules
        'common',
        'common.services',

        // 3rd Party Modules       
        // 'googlechart',
        'monospaced.qrcode',
        'nvd3',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'angular-ladda',
        'angularMoment',
        // 'toastr',
    ]);
}