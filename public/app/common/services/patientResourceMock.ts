var mockResource = angular
    .module("patientResourceMock",
    ["ngMockE2E"]);

mockResource.run(mockRun);

mockRun.$inject = ["$httpBackend"];
function mockRun($httpBackend: ng.IHttpBackendService): void {
    var patients: orthosensor.domains.IPatient[] = [];
    var patient: orthosensor.domains.IPatient;

    patient = new orthosensor.domains.Patient("1", "Johnny", "Q", "Rotten", new Date(2009, 2, 19),
        "Male", "English", "RottenJohnny01", "0909", "OS-9099", "", "OrthoTest");
    patients.push(patient);

    var patientUrl = "/api/patientlist";

    $httpBackend.whenGET(patientUrl).respond(patients);

    var editingRegex = new RegExp(patientUrl + "/[0-9][0-9]*", '');
    $httpBackend.whenGET(editingRegex).respond(function (method, url, data) {
        var patient = { "id": 0 };
        var parameters = url.split('/');
        var length = parameters.length;
        var id = parameters[length - 1];

        // if (id > 0) {
        //     for (var i = 0; i < products.length; i++) {
        //         if (products[i].productId == id) {
        //             product = products[i];
        //             break;
        //         }
        //     }
        // }
        return [200, patient, {}];
    });

    // Catch all for testing purposes
    $httpBackend.whenGET(/api/).respond(function (method, url, data) {
        return [200, patients, {}];
    });

    // Pass through any requests for application files
    $httpBackend.whenGET(/app/).passThrough();
}
