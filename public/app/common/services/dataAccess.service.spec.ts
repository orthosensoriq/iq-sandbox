
describe("Data Access Service Tests", function () {
    beforeEach(module('orthosensor.services'));
    var $service;
    beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
    }));
    describe('Check for some fake data', function () {
        it('Check Data', function () {
            var $scope = {};
            var controller = $controller('HospitalsController', { $scope: $scope });
            var hospitals = controller.getHospitals();
            expect(hospitals.length > 0).toBe(true);
        });
    });
});
