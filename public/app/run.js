// import { UserService } from './services/UserService';
(function () {
    angular
        .module('orthosensor')
        .run(runBlock);
    runBlock.$inject = ['$state', '$rootScope', '$timeout', 'InitFactory', '$window', 'UserService', 'DashboardService', 'PatientListFactory', 'SFDataService', 'HospitalService'];
    function runBlock($state, $rootScope, $timeout, InitFactory, $window, UserService, DashboardService, PatientListFactory, SFDataService, HospitalService) {
        $rootScope.handleSessionTimeout = function (event) {
            if (!event.status && event.statusCode === 500) {
                $window.location = '/login';
            }
        };
        InitFactory.isUserAdmin()
            .then(function (data) {
            // console.log(data);
            $rootScope.isUserAdmin = data;
            UserService.isAdmin = data;
        }, function (error) {
        });
        InitFactory.getCurrentUser()
            .then(function (data) {
            // console.log(data);
            $rootScope.currentUser = data;
            var sfUser = data;
            var user = UserService.convertSFToObject(sfUser);
            UserService.user = user;
            console.log(user);
            console.log(UserService.user);
            // let startPage = 'dashboard';
            var startPage = 'patientList';
            var hospitalPracticeId = '';
            DashboardService.setSurveys();
            // change if we get user info!
            // $timeout(() => {
            switch (UserService.user.userProfile) {
                case 2 /* Surgeon */:
                    console.log('surgeon Account');
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        HospitalService.loadHospitalByPractice(UserService.user.accountId);
                        console.log(HospitalService.practices);
                    })
                        .then(function () {
                        startPage = 'dashboard';
                        $state.go(startPage);
                    });
                // break;
                case 1 /* HospitalAdmin */:
                    hospitalPracticeId = user.accountId;
                    console.log('hospital Account');
                    HospitalService.setPractices(hospitalPracticeId);
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        startPage = 'dashboard';
                        $state.go(startPage);
                    });
                // break;
                case 3 /* PracticeAdmin */:
                    hospitalPracticeId = user.accountId;
                    // console.log('practice Account');
                    // DashboardService.loadHospitalPractices(hospitalPracticeId);
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        HospitalService.loadHospitalByPractice(UserService.user.accountId);
                        console.log(HospitalService.practices);
                    })
                        .then(function () {
                        startPage = 'patientList';
                        $state.go(startPage);
                    });
                // case 'Customer Community User':
                //     hospitalPracticeId = user.accountId;
                //     // console.log('practice Account');
                //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                //         .then(() => {
                //             console.log(HospitalService.surgeons);
                //         })
                //         .then(() => {
                //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                //             console.log(HospitalService.practices);
                //         })
                //         .then(() => {
                //             startPage = 'patientList';
                //             $state.go(startPage);
                //         });
                // case 'Customer Community Plus User':
                //     hospitalPracticeId = user.accountId;
                //     // console.log('practice Account');
                //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                //         .then(() => {
                //             console.log(HospitalService.surgeons);
                //         })
                //         .then(() => {
                //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                //             console.log(HospitalService.practices);
                //         })
                //         .then(() => {
                //             startPage = 'patientList';
                //             $state.go(startPage);
                //         });
                // break;
                default:
                    // console.log(DashboardService.hospitals);
                    // DashboardService.loadPractices(hospitalPracticeId);
                    HospitalService.getHospitals()
                        .then(function (data) {
                        // console.log(data);
                        SFDataService.loadHospitalPractices(HospitalService.hospitals[0].id)
                            .then(function (data) {
                            // console.log(data);
                            HospitalService.practices = HospitalService.convertSFPracticeToObject(data);
                            HospitalService.loadAllSurgeons()
                                .then(function () {
                                console.log(HospitalService.surgeons);
                                startPage = 'dashboard';
                                $state.go(startPage);
                            });
                        }, function (error) {
                            console.log(error);
                        });
                    });
            }
        });
        // TODO: move this to a service at some point!
        $rootScope.dateformat = dateformat;
        function dateformat(milliseconds) {
            console.log('- using date format in run js!!! -');
            if (!milliseconds) {
                console.log('Not milliseconds');
                return '--';
            }
            else {
                var dateValue = moment.utc(milliseconds).format('LL');
                console.log(dateValue);
                return dateValue;
            }
        }
    }
})();
//# sourceMappingURL=run.js.map