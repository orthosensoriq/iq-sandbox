(function () {
    'use strict';
    angular
        .module('orthosensor')
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', routeConfigurator]);
    function routeConfigurator($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        //$urlRouterProvider.otherwise({ redirectTo: '/' });
        // Now set up the states
        var dashboardUrl = 'app/layout/dashboard.js';
        $httpProvider.interceptors.push('sessionTimeoutHandler');
        $urlRouterProvider.otherwise('patientList');
        $stateProvider
            .state('home', {
            url: '#',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': { template: '<os-dashboard></os-dashboard>' }
            }
        })
            .state('dashboard', {
            url: '/dashboard',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': { template: '<os-dashboard></os-dashboard>' }
            },
        })
            .state('admin', {
            url: '/admin',
            views: {
                'header': { template: '<os-top-menu title="Admin"></os-top-menu>' },
                'content': {
                    template: '<admin></admin>'
                }
            }
        })
            .state('admin.doctors', {
            url: '/doctors',
            template: '<doctor-list></doctor-list>'
        })
            .state('admin.hospitals', {
            url: '/admin/hospitals',
            template: "<hospital-list></hospital-list>"
        })
            .state('admin.hospitalDetail', {
            url: '/hospitalDetail/:id',
            template: "<hospital-detail></hospital-detail>"
        })
            .state('admin.people', {
            url: '/admin/people',
            template: '<h1>People!</h1>'
        })
            .state('admin.users', {
            url: '/admin/users',
            template: '<user-list></user-list>'
        })
            .state('reports', {
            url: '/reports',
            views: {
                'header': { template: '<os-top-menu title="Reports"></os-top-menu>' },
                'content': {
                    template: '<os-reports></os-reports>'
                }
            }
        })
            .state('reports.promDeltaChart', {
            url: "/reports/promDeltaChart",
            template: '<div class = "row"><os-mean-change-in-prom-score></os-mean-change-in-prom-score><os-patients-koos-score-list></os-patients-koos-score-list></div>',
        })
            .state('reports.phaseCountChart', {
            url: "/reports/phaseCountChart",
            template: '<os-patient-phase-count chartType="bar"></os-patient-phase-count>',
        })
            .state('reports.kneeBalanceChart', {
            url: "/reports/kneeBalanceChart",
            template: '<os-knee-balance-data chartType2="line"></os-knee-balance-data><os-knee-balance-patient-cards></os-knee-balance-patient-cards>',
        })
            .state('reports.caseActivityChart', {
            url: "/reports/caseActivityChart",
            template: '<os-case-activity-chart></os-case-activity-chart>',
        })
            .state('test', {
            url: '/test',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': {
                    template: '<os-dashboard-test></os-dashboard-test>'
                }
            }
        })
            .state('patientTest', {
            url: '/patientTest',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': {
                    template: '<os-patient-test></os-patient-test>'
                }
            }
        })
            .state('patientList', {
            url: '/patient-list',
            views: {
                'header': {
                    templateUrl: 'app/patientList/patientListHeader.html',
                    controller: 'PatientListController',
                    controllerAs: '$ctrl'
                },
                'content': {
                    templateUrl: 'app/patientList/patientListContent.html',
                    // template: '<os-patient-list><os-patient-list>'
                    controller: 'PatientListController',
                    controllerAs: '$ctrl'
                }
            }
        })
            .state('addPatient', {
            url: '/addPatient',
            views: {
                'header': {
                    template: '<os-top-menu title="Add Patient"></os-top-menu>',
                },
                'content': {
                    template: '<os-add-patient></os-add-patient>',
                }
            }
        })
            .state('addPatientCase', {
            url: '/addPatientCase',
            views: {
                'header': {
                    template: '<os-top-menu title="Add Patient Case"></os-top-menu>',
                },
                'content': {
                    template: '<os-add-patient-case></os-add-patient-case>',
                }
            }
        })
            .state('patientDetails', {
            url: '/patient-details',
            views: {
                'header': {
                    templateUrl: 'app/patientDetail/patientDetailHeader.html',
                    controller: 'PatientDetailsController',
                    controllerAs: '$ctrl'
                },
                'content': {
                    templateUrl: 'app/patientDetail/patientDetailContent.html',
                    controller: 'PatientDetailsController',
                    controllerAs: '$ctrl'
                }
            }
        })
            .state('caseDetails', {
            url: '/case-details',
            views: {
                'header': {
                    templateUrl: 'app/caseDetails/caseDetailHeader.html',
                    controller: 'CaseDetailsController'
                },
                'content': {
                    templateUrl: 'app/caseDetails/caseDetailContent.html',
                    controller: 'CaseDetailsController'
                }
            },
            resolve: {
                events: function (CaseDetailsFactory) {
                    //return dashboardService.getHospitals();
                    return CaseDetailsFactory.LoadCaseEvents()
                        .then(function (data) {
                        //var hospitals = data;
                        console.log(data);
                        return data;
                    }, function (error) {
                        console.log(error);
                    });
                },
                sensorRefs: function (CaseDetailsFactory) {
                    return CaseDetailsFactory.loadSensorRefs().then(function (data) {
                        //console.log(data);
                        return data;
                    }, function (error) {
                    });
                }
            }
        })
            .state('patientActivityChart', {
            url: "/patientActivityChart",
            views: {
                'header': { template: '<top-nav title="Patient Activity Chart"> </top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><patient-activity></patient-activity></div></div></div>'
                }
            }
        })
            .state('practiceActivityChart', {
            url: "/practiceActivityChart",
            views: {
                'header': { template: '<top-nav title="Practice Activity Chart"></top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><practice-activity></practice-activity></div></div></div>'
                }
            }
        })
            .state('promActivityChart', {
            url: "/promActivityChart",
            views: {
                'header': { template: '<top-nav title="PROM Activity Chart"> </top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><prom-activity></prom-activity></div></div></div>'
                }
            }
        })
            .state('notifications', {
            url: "/notifications",
            views: {
                'header': { template: '<os-top-menu title="Notifications"></os-top-menu>' },
                'content': {
                    template: '<os-notifications></os-notifications>'
                }
            }
        });
        $locationProvider.html5Mode(true);
    }
})();
//# sourceMappingURL=config.route.js.map