var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsOutOfComplianceController = (function (_super) {
            __extends(PatientsOutOfComplianceController, _super);
            ///// /* @ngInject */
            function PatientsOutOfComplianceController(DashboardService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.HospitalService = HospitalService;
                _this.PatientService = PatientListFactory;
                _this.title = 'Patients out of compliance';
                _this.hospitalPracticeLabel = 'Practices';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                return _this;
            }
            PatientsOutOfComplianceController.prototype.$onInit = function () {
                // super.$onInit();
                this.surgeons = this.DashboardService.surgeons;
                // console.log(this.surgeons);
                this.patientsOutOfCompliance = [];
                var patient1 = new orthosensor.domains.PatientsOutOfCompliance();
                patient1.patientName = 'Fredericka Winfield';
                patient1.phaseName = 'Procedure';
                patient1.issue = 'Incomplete';
                patient1.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient1);
                var patient2 = new orthosensor.domains.PatientsOutOfCompliance();
                patient2.patientName = 'Jane Hart';
                patient2.phaseName = '4 - 8 week';
                patient2.issue = 'Survey Missing';
                patient2.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient2);
                var patient3 = new orthosensor.domains.PatientsOutOfCompliance();
                patient3.patientName = 'James Johnson';
                patient3.phaseName = 'Pre-Op';
                patient3.issue = 'Survey Missing';
                patient3.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient3);
                var patient4 = new orthosensor.domains.PatientsOutOfCompliance();
                patient4.patientName = 'Midred Ginger';
                patient4.phaseName = 'Pre-Op';
                patient4.issue = 'Survey Missing';
                patient4.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient4);
                // console.log(this.patientsOutOfCompliance);
            };
            ;
            PatientsOutOfComplianceController.prototype.isHospital = function () {
                return true;
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientsOutOfComplianceController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('practice Account');
                        this.setHospitalPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientsOutOfComplianceController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.setAdmin = function () {
                // console.log(this.hospitals);
                // console.log(this.practices);
                // console.log(this.surgeons);
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getSurgeonData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.getSurgeonData = function () {
                return this.getSurgeonCompliance(this.surgeon.id);
            };
            PatientsOutOfComplianceController.prototype.getPracticeData = function () {
                return this.getPracticeCompliance(this.practice.id);
            };
            PatientsOutOfComplianceController.prototype.validate = function () {
                return true;
            };
            PatientsOutOfComplianceController.prototype.getSurgeonCompliance = function (surgeonId) {
                var surgeon = this.surgeon.id;
                return [];
            };
            PatientsOutOfComplianceController.prototype.getPracticeCompliance = function (practiceId) {
                var practice = this.practice.id;
                return [];
            };
            // convert to object - dates come in last day of the prior month
            PatientsOutOfComplianceController.prototype.convertSFComplianceToObject = function (data) {
                return [];
            };
            PatientsOutOfComplianceController.prototype.createPatientComplianceRecord = function (id, name, toDate, sDate, surgeonId, practiceId, count) {
                var patient = new orthosensor.domains.PatientsOutOfCompliance();
                // phaseCount.id = id;
                // phaseCount.phase = name;
                // phaseCount.startDate = toDate;
                // phaseCount.startDateString = sDate;
                // phaseCount.surgeonId = surgeonId;
                // phaseCount.practiceId = practiceId;
                // phaseCount.count = count;
                // console.log(phaseCount);
                return patient;
            };
            return PatientsOutOfComplianceController;
        }(dashboard.DashboardBaseController));
        PatientsOutOfComplianceController.$inject = ['DashboardService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        var PatientsOutOfCompliance = (function () {
            function PatientsOutOfCompliance() {
                this.bindings = {
                    chartType: '@'
                };
                this.controller = PatientsOutOfComplianceController;
                this.templateUrl = 'app/dashboard/patientsOutOfCompliance/patientsOutOfCompliance.component.html';
            }
            return PatientsOutOfCompliance;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientsOutOfCompliance', new PatientsOutOfCompliance());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsOutOfCompliance.component.js.map