var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardController = (function () {
            function DashboardController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state, $rootScope) {
                this.DashboardService = DashboardService;
                this.DashboardDataService = DashboardDataService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.PatientService = PatientService;
                this.UserService = UserService;
                this.$state = $state;
                this.$rootScope = $rootScope;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            DashboardController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = '0017A00000M803ZQAR';
                this.user = this.UserService.user;
                this.isAdmin = this.UserService.isAdmin;
                // console.log(this.isAdmin);
                this.standardComponentSize = 'col-sm-6 col-md-4 col-xs-12 dashboardComponentHeight';
                this.standardDashboardWidgetSize = 'col-sm-4 col-xs-12';
                if (!this.isAdmin) {
                    practiceId = this.UserService.user.accountId;
                    if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                        this.systemMode = 'production';
                    }
                    else {
                        this.systemMode = this.$rootScope.systemMode;
                    }
                    console.log(this.$rootScope);
                    this.DashboardDataService.getScheduledPatients(practiceId, 7)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsScheduled = data.length;
                        }
                        else {
                            _this.patientsScheduled = 0;
                        }
                        // console.log(this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                    this.DashboardDataService.getPatientsUnderBundle(practiceId)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsUnderBundleCount = data.length;
                        }
                        else {
                            _this.patientsUnderBundleCount = 0;
                        }
                        // console.log(this.patientsUnderBundleCount);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            return DashboardController;
        }());
        DashboardController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state', '$rootScope'];
        var Dashboard = (function () {
            function Dashboard() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = DashboardController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/dashboard.component.html';
            }
            return Dashboard;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osDashboard', new Dashboard());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.component.js.map