# Dashboard Tasks/priorities
1.     Patient Activity Graphic
        Number of patients in Pre-op (PROM)
        Number of patients booked/scheduled (This week)
        Number of Patient in post-acute f                        
        Number of Patients in Long term follow up (>90 days)
        Number of Inactive Patients.
        - Add time value (use end of the time period for calculations) 
           
        Questions / Assertions
        - Pre-op - booked/scheduled in future or not booked/scheduled
        - Booked/scheduled - within specified time period - could also 
        be counted also in pre-op 
        - how do we measure inactive

            - remove booked scheduled
            - remove inactive
            - add Surgeon - Patrick Meere, Martin Roche
            - create Similar graph of booked/schedule and Inactive

2.     PROM Activity Graphic
            Pre Op
            Average Pre-op scores under the surgeon.
            Practice average pre-op score.
            Hospital / Global aggregate pre-op score.
            Post Op
            Average Post-op scores under the surgeon.
            Practice average post-op score.
            Hospital / Global aggregate post-op score.
           - drop down for month year and all history - 
           - separate graphs for post op - 4 - 8 weeks weeks and 6 months)

           - drop down for practice and surgeon

3.     Prom Outcome graph
            Populating bar graph, 6-month PROM scores – satisfaction distribution.
             - delete - 

4.     Patients PROM Compliance
 
    Patient compliance rate by interval (pre, post 4wk, post 6 months, Post 1yr[MG1] )
    -       Should show the graph which lists how many completed the PROMS for the each 
    scheduled interval.

    Drill down of the patients who have not completed PROMS under the surgeon. (future)
    - drop down for practice and surgeon


*** Remove patient Pain Activity

5.     Risk Graphs

        Average risk score[MG2]  under the surgeon.
            Practice average risk score.                
            Hospital / Global aggregate risk score.
 
            LACE Score [MG3] that predicts readmission rates within 30 days.
            Patients with the LACE score.
            Patients above certain LACE score.
            Patients with their risk score.
            Patients above certain risk score.
             - focus on LACE score
 
 Preop Risk distrubtion
    - 3 bars: 2 weeks prior and 1 day before: high risk, medium and low risk
    - drop down for practice and surgeon

Pie chart average Preop scores for hopital , average post op scores 2 weeks afer 
    - 20 
6.     Further Data points. (per surgeon, per practice, per hospital)

 
1.     Mean prom score
    A report on mean score of patients pre operatively as well as 
    the post operatively.
 
2.     Mean change in prom score:
    A report a mean change in prom scores between pre-op to 
    post-op two weeks to post-op 3 months to post-op six months 
    to post-op one year  
 
3.     Post-surgery PROM score threshold:
    Proportion of patients per hospital or per provider who 
    achieve a postoperative PROM score consistent with some pre-specified threshold. (will be defined by the surgeon based on the data)[MG4] 
 
4.     Mean change in PROM score threshold:
    Proportion of patients per hospital or per provider who achieve 
    an improvement in PROM score consistent with some pre-specified 
    threshold (will be defined by the surgeon based on the data.[MG5] 
 
    Prom score improvement is measured between pre-op and two weeks after surgery.
    Prom score improvement is measured between pre-op and three months after surgery.  
 