module orthosensor.dashboard {

    interface ChartController {
        title: string;
        hospitals: orthosensor.domains.Hospital[];
        hospitalPracticeLabel: string;
        surgeonLabel: string;
        surgeons: orthosensor.domains.Surgeon[];
        chart: orthosensor.domains.ChartType;
        //   isHospital(): boolean;
        //   $onInit(): void;

    }
}
