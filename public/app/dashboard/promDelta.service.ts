namespace orthosensor.dashboard {
    'use strict';

    export interface IPromDeltaService {
    }
    export class PromDeltaService implements IPromDeltaService {
        static inject: Array<string> = ['DashboardService', 'DashboardDataService', 'UserService', 'moment'];
        protected DashboardDataService: any;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public startDate: any;
        public endDate: any;
        public startMonth: number;
        public startYear: number;
        public monthDiff: number;
        public isUserAdmin: boolean;
        public promDeltas: orthosensor.domains.PromDelta[];
        public data: any[];
        public eventTypes: string[];

        constructor(public DashboardService: any, DashboardDataService: any, UserService: any, moment: any) {
            this.DashboardDataService = DashboardDataService;

            this.isUserAdmin = UserService.isUserAdmin;
            // these values are being hard coded for KOOS and current event types
            this.eventTypes = ['Pre-Op', '4-8 Week Follow-up', '6 Month Follow-up', '12 Month Follow-up', '2 Year Follow-up'];
        }

        getPracticePromDeltas(): orthosensor.domains.PromDelta[] {
            let practice: string = this.practice.id;
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            // console.log(this.practice);
            this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                .then((data: any) => {
                    let result = data;
                    // console.log(result);
                    if (result !== null) {
                        this.promDeltas = this.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        this.getData();
                    }
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return this.promDeltas;
        }

        //// stopped here - creating service to manage data collection

        getData(): any[] {

            //console.log(this.promDeltas);
            //this.data = this.chartData;
            return this.data;
        }

        convertDataToObjects(data: any): orthosensor.domains.PromDelta[] {
            let promDeltas: orthosensor.domains.PromDelta[] = [];
            // console.log(data);
            for (let k = 0; k <= this.eventTypes.length; k++) {
                for (let i = 0; i <= data.length; i++) {
                    // make sure a record exists!
                    if (data[i] !== undefined) {
                        if (data[i].Stage__c === this.eventTypes[k]) {
                            let delta: orthosensor.domains.PromDelta = this.convertSFToObject(data[i]);
                            promDeltas.push(delta);
                        }
                    }    
                }
            }
            return promDeltas;
        }


        // stopped here 2/3/17 - need to add another method for aggregate objects and one for detail
        
        // convert to object - dates come in last day of the prior month
        convertSFToObject(data: any): orthosensor.domains.PromDelta {
            // console.log(data);
            let promDelta: orthosensor.domains.PromDelta = new orthosensor.domains.PromDelta();
            promDelta.id = data.Id;
            promDelta.practiceId = data.PracticeID__c;
            promDelta.surgeonId = data.SurgeonID__c;
            promDelta.practiceName = data.PracticeName__c;
            promDelta.surgeonName = data.SurgeonName__c;
            promDelta.section = data.Section__c;
            promDelta.stage = data.Stage__c;
            promDelta.score = data.Score__c;
            promDelta.patientName = data.PatientName__c;
            promDelta.patientIde = data.PatientID__c;
            promDelta.procedureDate = moment(data.ProcedureDate__c);
            // console.log(promDelta);

            return promDelta;
        }
    }

    angular
        .module('orthosensor.dashboard')
        .service('PromDeltaService', PromDeltaService);
}
