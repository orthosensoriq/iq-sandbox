module orthosensor {
    class PatientKoosCardController {
        public patients: any[];
        public patient: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };
         constructor() { }

         $onInit() { }
        
  }

     class PatientKoosCard implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                
            };
            this.controller = PatientKoosCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/patientKoosCard.component.html';
        }

    }
    angular
        .module('orthosensor')
        .component('osPatientKoosCard', new PatientKoosCard());
}