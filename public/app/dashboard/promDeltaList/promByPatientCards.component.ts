module orthosensor {
    class PromByPatientCardsController {
        public patients: any[];
        public patient: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };
         constructor() { }

         $onInit() { }
        
  }

     class PromByPatientCards implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                
            };
            this.controller = PromByPatientCardsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/promByPatientCards.component.html';
        }

    }
    angular
        .module('orthosensor')
        .component('osPromByPatientCards', new PromByPatientCards());
}