var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsKoosScoreListController = (function (_super) {
            __extends(PatientsKoosScoreListController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function PatientsKoosScoreListController(DashboardService, PatientListFactory, UserService, moment, config, DashboardDataService, PromDeltaService, HospitalService, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.DashboardDataService = DashboardDataService;
                _this.PromDeltaService = PromDeltaService;
                _this.HospitalService = HospitalService;
                _this.$rootScope = $rootScope;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'Patient KOOS scores';
                _this.hospitals = [];
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                // this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                // this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                _this.$onInit();
                return _this;
            }
            PatientsKoosScoreListController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // this.surgeons = this.DashboardService.surgeons;
                // console.log(this.surgeons);
                // this.proms = this.getProms();
                // this.chartData = [];
                this.surveys = this.DashboardService.surveys;
                this.patientsScores = [
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Richard Robert',
                        patientId: '7348',
                        phase: 'Pre-op',
                        procedureDate: new Date(2016, 12, 2),
                        symptom: 15,
                        pain: 14,
                        aDL: 10,
                        sportRec: 16,
                        qOL: 15
                    },
                    {
                        id: '8901',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: 'Pre-op',
                        procedureDate: new Date(2017, 1, 15),
                        symptom: 12,
                        pain: 13,
                        aDL: 11,
                        sportRec: 17,
                        qOL: 18
                    },
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: '4-8 week Follow-up',
                        procedureDate: new Date(2017, 3, 4),
                        symptom: 18,
                        pain: 16,
                        aDL: 14,
                        sportRec: 20,
                        qOL: 20
                    },
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: '6 month Follow-up',
                        procedureDate: new Date(2017, 6, 4),
                        symptom: 25,
                        pain: 22,
                        aDL: 24,
                        sportRec: 22,
                        qOL: 21
                    },
                    {
                        id: '8903',
                        surgeonId: '4563',
                        practiceId: '9878',
                        surgeonName: 'Marcus Welby',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Marc Abt',
                        patientId: '9876',
                        phase: 'Pre-Op',
                        procedureDate: new Date(2017, 3, 1),
                        symptom: 11,
                        pain: 12,
                        aDL: 10,
                        sportRec: 8,
                        qOL: 10
                    },
                ];
                //this.setDateParams();
                this.initData();
            };
            ;
            PatientsKoosScoreListController.prototype.initChartParams = function () {
                // this.initChartLookups();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientsKoosScoreListController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientsKoosScoreListController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonPromDeltas();
                            break;
                        }
                    }
                }
            };
            PatientsKoosScoreListController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticePromDeltas();
                    }
                }
            };
            PatientsKoosScoreListController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticePromDeltas(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            PatientsKoosScoreListController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        console.log('trying to change the chart');
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PatientsKoosScoreListController.prototype.getSurgeonData = function () {
                return this.getSurgeonPromDeltas();
            };
            PatientsKoosScoreListController.prototype.getPracticeData = function () {
                return this.getPracticePromDeltas();
            };
            PatientsKoosScoreListController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            PatientsKoosScoreListController.prototype.getData = function () {
                console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                this.data = this.chartData;
                return this.chartData;
            };
            PatientsKoosScoreListController.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                // console.log('Getting knee balance data');
                // loop through months
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                        console.log(_this.promDeltas);
                        //this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            PatientsKoosScoreListController.prototype.getSurgeonPromDeltas = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    var surgeon = this.surgeon.id;
                    this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                            console.log(_this.promDeltas);
                            _this.getData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            return PatientsKoosScoreListController;
        }(dashboard.DashboardBaseController));
        PatientsKoosScoreListController.$inject = ['DashboardService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService', 'HospitalService', '$rootScope'];
        var PatientsKoosScoreList = (function () {
            function PatientsKoosScoreList() {
                this.bindings = {};
                this.controller = PatientsKoosScoreListController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/promDeltaList/patientsKoosScoresList.component.html';
            }
            return PatientsKoosScoreList;
        }());
        angular
            .module('orthosensor')
            .component('osPatientsKoosScoreList', new PatientsKoosScoreList());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsKoosScoresList.component.js.map