var orthosensor;
(function (orthosensor) {
    var PromByPatientCardsController = (function () {
        function PromByPatientCardsController() {
        }
        PromByPatientCardsController.prototype.$onInit = function () { };
        return PromByPatientCardsController;
    }());
    var PromByPatientCards = (function () {
        function PromByPatientCards() {
            this.bindings = {};
            this.controller = PromByPatientCardsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/promByPatientCards.component.html';
        }
        return PromByPatientCards;
    }());
    angular
        .module('orthosensor')
        .component('osPromByPatientCards', new PromByPatientCards());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=promByPatientCards.component.js.map