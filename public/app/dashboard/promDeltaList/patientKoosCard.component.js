var orthosensor;
(function (orthosensor) {
    var PatientKoosCardController = (function () {
        function PatientKoosCardController() {
        }
        PatientKoosCardController.prototype.$onInit = function () { };
        return PatientKoosCardController;
    }());
    var PatientKoosCard = (function () {
        function PatientKoosCard() {
            this.bindings = {};
            this.controller = PatientKoosCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/patientKoosCard.component.html';
        }
        return PatientKoosCard;
    }());
    angular
        .module('orthosensor')
        .component('osPatientKoosCard', new PatientKoosCard());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientKoosCard.component.js.map