module orthosensor.dashboard {

    class PatientsKoosScoreListController extends DashboardBaseController {

        public static $inject = ['DashboardService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService', 'HospitalService', '$rootScope'];

        public title: string;
        public user: orthosensor.domains.User;
        
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public promDeltaChart: orthosensor.domains.ChartType;
        // public surgeons: orthosensor.domains.Surgeon[];
        public chart: orthosensor.domains.ChartType;
        // public surgeon: orthosensor.domains.Surgeon;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        public startDate: any;
        public endDate: any;
        public startMonth: number;
        public startYear: number;
        public monthDiff: number;
        public chartData: any[];
        public patientsScores: orthosensor.domains.PatientKoosScore[];
        public surgeonSelectClass: string;
        public practiceSelectClass: string;
        public surveys: any[];

        public allData: orthosensor.domains.KneeBalanceAvg[];
        public chartType: string;
        // private PatientService: any;

        public dateFormat: string = 'MM/DD/YYYY';

        ///// /* @ngInject */
        // tslint:disable-next-line:max-line-length
        constructor(public DashboardService: any,
            PatientListFactory: any,
            public UserService: any, moment: any, config: any,
            public DashboardDataService: orthosensor.services.DashboardDataService,
            public PromDeltaService: any,
            public HospitalService: orthosensor.services.HospitalService,
            public $rootScope: ng.IRootScopeService) {
            super(UserService, HospitalService, $rootScope);
            // this.PatientService = PatientListFactory;
            this.title = 'Patient KOOS scores';
            
            this.hospitals = [];
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            // this.surgeons = [];
            this.chartHeight = DashboardService.chartHeight;
            // this.user = UserService.user;

            // console.log(this.user);

            // console.log(config);
            this.$onInit();
        }

        public $onInit(): void {
            super.$onInit();

            // this.surgeons = this.DashboardService.surgeons;
            // console.log(this.surgeons);
            // this.proms = this.getProms();
            // this.chartData = [];
            this.surveys = this.DashboardService.surveys;
            this.patientsScores = [
                {
                    id: '8900',
                    surgeonId: '4562',
                    practiceId: '9877',
                    surgeonName: 'Samantha Jones',
                    practiceName: 'Orthosensor Orthopedic',
                    patientName: 'Richard Robert',
                    patientId: '7348',
                    phase: 'Pre-op',
                    procedureDate: new Date(2016, 12, 2),
                    symptom: 15,
                    pain: 14,
                    aDL: 10,
                    sportRec: 16,
                    qOL: 15
                },
                                {
                    id: '8901',
                    surgeonId: '4562',
                    practiceId: '9877',
                    surgeonName: 'Samantha Jones',
                    practiceName: 'Orthosensor Orthopedic',
                    patientName: 'Mildred Ginger',
                    patientId: '9871',
                    phase: 'Pre-op',
                    procedureDate: new Date(2017, 1, 15),
                    symptom: 12,
                    pain: 13,
                    aDL: 11,
                    sportRec: 17,
                    qOL: 18
                },
                  {
                    id: '8900',
                    surgeonId: '4562',
                    practiceId: '9877',
                    surgeonName: 'Samantha Jones',
                    practiceName: 'Orthosensor Orthopedic',
                    patientName: 'Mildred Ginger',
                    patientId: '9871',
                    phase: '4-8 week Follow-up',
                    procedureDate: new Date(2017, 3, 4),
                    symptom: 18,
                    pain: 16,
                    aDL: 14,
                    sportRec: 20,
                    qOL: 20
                                },
                {
                    id: '8900',
                    surgeonId: '4562',
                    practiceId: '9877',
                    surgeonName: 'Samantha Jones',
                    practiceName: 'Orthosensor Orthopedic',
                    patientName: 'Mildred Ginger',
                    patientId: '9871',
                    phase: '6 month Follow-up',
                    procedureDate: new Date(2017, 6, 4),
                    symptom: 25,
                    pain: 22,
                    aDL: 24,
                    sportRec: 22,
                    qOL: 21
                  },
                {
                    id: '8903',
                    surgeonId: '4563',
                    practiceId: '9878',
                    surgeonName: 'Marcus Welby',
                    practiceName: 'Orthosensor Orthopedic',
                    patientName: 'Marc Abt',
                    patientId: '9876',
                    phase: 'Pre-Op',
                    procedureDate: new Date(2017, 3, 1),
                    symptom: 11,
                    pain: 12,
                    aDL: 10,
                    sportRec: 8,
                    qOL: 10
                },
                

            ];
            //this.setDateParams();

            this.initData();

        };

        public initChartParams(): void {
            // this.initChartLookups();

        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {

            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonPromDeltas();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            // console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticePromDeltas();

                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            // this.data = this.getPracticeKneeBalanceAvg();
                            this.getPracticePromDeltas(); // for testing!!
                        }
                    }
                }
            }
        }



        // called from page
        public getChart(surgeon: any): any[] {
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    return this.getSurgeonData();

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    console.log('trying to change the chart');
                    return this.getPracticeData();

                default:
                    return this.getSurgeonData();
            }
        }

        public getSurgeonData(): any[] {
            return this.getSurgeonPromDeltas();
        }

        public getPracticeData(): any[] {
            return this.getPracticePromDeltas();
        }

        public getMonthLabel(date: Date): string {
            let dateLabel: string = this.formatDate(date);
            return dateLabel;
        }

        public getData(): any[] {

            console.log(this.promDeltas);
            this.chartData = [];
            // let chartData: any[] = [];

            this.data = this.chartData;
            return this.chartData;
        }

        getPracticePromDeltas(): orthosensor.domains.PromDelta[] {
            let practice: string = this.practice.id;

            // console.log('Getting knee balance data');
            // loop through months
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.practice);
            this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                .then((data: any) => {
                    let result = data;
                    console.log(result);
                    if (result !== null) {
                        this.promDeltas = this.PromDeltaService.convertDataToObjects(result);
                        console.log(this.promDeltas);
                        //this.getData();
                    }
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return this.promDeltas;
        }

        getSurgeonPromDeltas(): orthosensor.domains.PromDelta[] {
            // let surgeon: string = this.surgeon.id;
            console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            console.log(this.surgeon);
            if (this.surgeon !== undefined) {
                let surgeon = this.surgeon.id;
                this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                    .then((data: any) => {
                        let result = data;
                        console.log(result);
                        if (result !== null) {
                            this.promDeltas = this.PromDeltaService.convertDataToObjects(result);
                            console.log(this.promDeltas);
                            this.getData();
                        }
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
            return this.promDeltas;

        }
    }
    class PatientsKoosScoreList implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {

            };
            this.controller = PatientsKoosScoreListController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/patientsKoosScoresList.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osPatientsKoosScoreList', new PatientsKoosScoreList());

}

