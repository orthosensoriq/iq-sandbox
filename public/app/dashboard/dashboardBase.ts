module orthosensor.dashboard {
   
    export abstract class DashboardBaseController {

        public static $inject = ['UserService', 'HospitalService', '$rootScope'];
        protected PatientService: any;
        public user: orthosensor.domains.User;
        public isUserAdmin: boolean;
        public hospitals: orthosensor.domains.Hospital[];
        public dateFormat: string = 'MM/DD/YYYY';
        public hospital: orthosensor.domains.Hospital;
        public practice: orthosensor.domains.Practice;
        public practices: orthosensor.domains.Practice[];
        public surgeons: orthosensor.domains.Surgeon[];
        public isDashboard: boolean;
        public systemMode: string;
        public showPracticeList: boolean;
        public showSurgeonList: boolean;
    
        constructor(public UserService: orthosensor.services.UserService,
            public HospitalService: orthosensor.services.HospitalService,
            public $rootScope: ng.IRootScopeService) {
            this.isDashboard = false;
            
        }
        $onInit(): void {
            this.user = this.UserService.user;    
            console.log(this.user);
            this.isUserAdmin = this.UserService.isAdmin;
            // console.log(this.$rootScope);
            console.log(this.$rootScope.systemMode);
            if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                this.systemMode = 'production';
            } else {
                this.systemMode = this.$rootScope.systemMode;
            }
            console.log(this.systemMode);
            this.showPracticeList = this.isUserAdmin || this.user.userProfile === orthosensor.domains.UserProfile.HospitalAdmin; 
            this.showSurgeonList = this.isUserAdmin || this.user.userProfile === orthosensor.domains.UserProfile.Surgeon || this.user.userProfile === orthosensor.domains.UserProfile.PracticeAdmin;
            console.log(this.showPracticeList);
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    console.log(this.HospitalService);
                    
                    this.hospital = this.HospitalService.hospital;
                    this.practice = new orthosensor.domains.Practice;
                    this.practice.id = this.user.accountId;
                    this.practice.name = this.user.accountName;
                    break;

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('hospital Account');
                    this.hospital = this.HospitalService.hospital;
                    this.practices = this.HospitalService.practices;
                    break;

                case orthosensor.domains.UserProfile.PracticeAdmin:
                    // console.log('hospital Account');
                    this.hospital = this.HospitalService.hospital;
                    this.practice = new orthosensor.domains.Practice;
                    this.practice.id = this.user.accountId;
                    this.practice.name = this.user.accountName;
                    break;

                // case 'Customer Community User':
                //     // console.log('hospital Account');
                //     this.hospital = this.HospitalService.hospital;
                //     this.practice = new orthosensor.domains.Practice;
                //     this.practice.id = this.user.accountId;
                //     this.practice.name = this.user.accountName;
                //     break;
                // case 'Customer Community Plus User':
                //     // console.log('hospital Account');
                //     this.hospital = this.HospitalService.hospital;
                //     this.practice = new orthosensor.domains.Practice;
                //     this.practice.id = this.user.accountId;
                //     this.practice.name = this.user.accountName;
                //     break;
                    
                default:
                    // console.log('Admin Account');
                    if (this.isUserAdmin) {
                        this.hospitals = this.HospitalService.hospitals;
                        this.practices = this.HospitalService.practices;
                        console.log(this.practices);
                    }
            }
            this.surgeons = this.HospitalService.surgeons;
            // console.log(this.surgeons);
         }
    }
}
