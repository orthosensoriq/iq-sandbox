var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardBaseController = (function () {
            function DashboardBaseController(UserService, HospitalService, $rootScope) {
                this.UserService = UserService;
                this.HospitalService = HospitalService;
                this.$rootScope = $rootScope;
                this.dateFormat = 'MM/DD/YYYY';
                this.isDashboard = false;
            }
            DashboardBaseController.prototype.$onInit = function () {
                this.user = this.UserService.user;
                console.log(this.user);
                this.isUserAdmin = this.UserService.isAdmin;
                // console.log(this.$rootScope);
                console.log(this.$rootScope.systemMode);
                if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                    this.systemMode = 'production';
                }
                else {
                    this.systemMode = this.$rootScope.systemMode;
                }
                console.log(this.systemMode);
                this.showPracticeList = this.isUserAdmin || this.user.userProfile === 1 /* HospitalAdmin */;
                this.showSurgeonList = this.isUserAdmin || this.user.userProfile === 2 /* Surgeon */ || this.user.userProfile === 3 /* PracticeAdmin */;
                console.log(this.showPracticeList);
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        console.log(this.HospitalService);
                        this.hospital = this.HospitalService.hospital;
                        this.practice = new orthosensor.domains.Practice;
                        this.practice.id = this.user.accountId;
                        this.practice.name = this.user.accountName;
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.hospital = this.HospitalService.hospital;
                        this.practices = this.HospitalService.practices;
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('hospital Account');
                        this.hospital = this.HospitalService.hospital;
                        this.practice = new orthosensor.domains.Practice;
                        this.practice.id = this.user.accountId;
                        this.practice.name = this.user.accountName;
                        break;
                    // case 'Customer Community User':
                    //     // console.log('hospital Account');
                    //     this.hospital = this.HospitalService.hospital;
                    //     this.practice = new orthosensor.domains.Practice;
                    //     this.practice.id = this.user.accountId;
                    //     this.practice.name = this.user.accountName;
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('hospital Account');
                    //     this.hospital = this.HospitalService.hospital;
                    //     this.practice = new orthosensor.domains.Practice;
                    //     this.practice.id = this.user.accountId;
                    //     this.practice.name = this.user.accountName;
                    //     break;
                    default:
                        // console.log('Admin Account');
                        if (this.isUserAdmin) {
                            this.hospitals = this.HospitalService.hospitals;
                            this.practices = this.HospitalService.practices;
                            console.log(this.practices);
                        }
                }
                this.surgeons = this.HospitalService.surgeons;
                // console.log(this.surgeons);
            };
            return DashboardBaseController;
        }());
        DashboardBaseController.$inject = ['UserService', 'HospitalService', '$rootScope'];
        dashboard.DashboardBaseController = DashboardBaseController;
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboardBase.js.map