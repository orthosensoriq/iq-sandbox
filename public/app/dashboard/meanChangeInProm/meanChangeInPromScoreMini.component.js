var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var MeanChangeInPromScoreMiniController = (function (_super) {
            __extends(MeanChangeInPromScoreMiniController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function MeanChangeInPromScoreMiniController(DashboardService, ChartService, PatientListFactory, UserService, moment, config, DashboardDataService, PromDeltaService) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, moment, config) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                // this.PatientService = PatientListFactory;
                _this.title = 'PROM score deltas';
                _this.DashboardDataService = DashboardDataService;
                _this.hospitals = [];
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                _this.user = UserService.user;
                _this.promDeltaService = PromDeltaService;
                return _this;
                // this.$onInit();
            }
            MeanChangeInPromScoreMiniController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // console.log(this.surgeons);
                this.surveys = this.DashboardService.surveys;
                this.promDeltas = [];
                this.setDateParams();
                this.initChartParams();
                // console.log(this.surveys);
                // for now - only KOOS!
                for (var i = 0; i <= this.surveys.length; i++) {
                    // console.log(i);
                    if (this.surveys[i]) {
                        if (this.surveys[i].name === 'KOOS') {
                            this.prom = this.surveys[i];
                            break;
                        }
                    }
                }
                this.initData();
            };
            MeanChangeInPromScoreMiniController.prototype.getProms = function () {
                return [];
            };
            MeanChangeInPromScoreMiniController.prototype.initChartParams = function () {
                this.initChartLookups();
                this.promDeltaChart = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.promDeltaChart = service.chart;
                this.options = this.ChartService.getLineChartOptions();
                this.options.showLegend = false;
                this.ChartService.chartTitle = 'Prom Delta';
                this.ChartService.yAxisLabel = 'Score';
                this.ChartService.xAxisLabel = 'Events';
                this.setSelectSizes();
                // these values are being hard coded for KOOS and current event types
                this.eventTypes = this.promDeltaService.eventTypes;
                this.responseTypes = ['KOOS Symptoms', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS Pain', 'KOOS QOL'];
                this.lineColors = ['#ff7f0e', 'green', 'blue', '#787878', 'red'];
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and returning?)
            MeanChangeInPromScoreMiniController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            MeanChangeInPromScoreMiniController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i <= this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            MeanChangeInPromScoreMiniController.prototype.setSelectSizes = function () {
                this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
                // console.log(this.surgeonSelectClass);
                this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.hospitalSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.promSelectClass = 'col-md-2 col-sm-3 col-xs-4';
                this.dateRangeSelectClass = 'col-md-1 col-sm-2 col-xs-3';
            };
            MeanChangeInPromScoreMiniController.prototype.getSurgeonChart = function () {
                this.getSurgeonData();
            };
            MeanChangeInPromScoreMiniController.prototype.createChartObject = function (key, color) {
                var getChartValuesForType = this.getChartValues(key);
                var chartObj = {
                    'key': [key],
                    'color': color,
                    'values': this.getChartValues(key)
                };
                // console.log(chartObj);
                return chartObj;
            };
            MeanChangeInPromScoreMiniController.prototype.getChartValues = function (key) {
                var values = [];
                // console.log(this.promDeltas);
                if (this.promDeltas.length > 0) {
                    for (var i = 0; i <= this.promDeltas.length; i++) {
                        // console.log(this.promDeltas[i]);
                        for (var j = 0; j <= this.eventTypes.length; j++) {
                            if (this.promDeltas[i] !== undefined) {
                                if (this.promDeltas[i].stage === this.eventTypes[j]) {
                                    if (this.promDeltas[i].section === key) {
                                        var point = { x: j, y: this.promDeltas[i].score };
                                        // console.log(point);
                                        values.push(point);
                                        // console.log(values);
                                    }
                                }
                            }
                            else {
                                // console.log(this.promDeltas[i]);
                            }
                        }
                    }
                    return values;
                }
                // console.log(values);
                return values;
            };
            MeanChangeInPromScoreMiniController.prototype.getChartData = function () {
                // console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                for (var i = 0; i <= this.responseTypes.length; i++) {
                    // console.log('Response Type' + i);
                    this.chartData.push(this.createChartObject(this.responseTypes[i], this.lineColors[i]));
                }
                console.log(this.chartData);
                return this.chartData;
            };
            MeanChangeInPromScoreMiniController.prototype.getPracticeData = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.DashboardDataService.getAggregatePromDeltasByPractice(this.practice.id, startMonth, 0, 0, 0)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        _this.getChartData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            MeanChangeInPromScoreMiniController.prototype.getSurgeonData = function () {
                var _this = this;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.surgeon);
                if (this.surgeon !== null) {
                    // let surgeon = this.surgeon.id;
                    this.DashboardDataService.getAggregatePromDeltasBySurgeon(this.surgeon.id, 0, 0, 0, 0)
                        .then(function (data) {
                        var result = data;
                        // console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                            // console.log(this.promDeltas);
                            _this.getChartData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            MeanChangeInPromScoreMiniController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            MeanChangeInPromScoreMiniController.prototype.validate = function () {
                return true;
            };
            return MeanChangeInPromScoreMiniController;
        }(dashboard.DashboardChartBaseController));
        MeanChangeInPromScoreMiniController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService'];
        angular
            .module('orthosensor.dashboard')
            .component('osMeanChangeInPromScoreMini', {
            controller: MeanChangeInPromScoreMiniController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/meanChangeInProm/meanChangeInPromScoreMini.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=meanChangeInPromScoreMini.component.js.map