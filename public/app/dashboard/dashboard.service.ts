module orthosensor.services {

    export class DashboardService {
        static inject: Array<string> = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
        public chartHeight: number;
        private _hospitals: orthosensor.domains.Hospital[];
        private _hospital: orthosensor.domains.Hospital;
        private PatientService: any;
        private _surgeons: orthosensor.domains.Surgeon[];
        private _practices: orthosensor.domains.Practice[];
        private _surveys: Array<orthosensor.domains.Survey>;
       
        // private dashboardService: any;

        get practices(): orthosensor.domains.Practice[] {
            return this._practices;
        }

        set practices(newPractices: orthosensor.domains.Practice[]) {
            this._practices = newPractices;
        }

        get surgeons(): orthosensor.domains.Surgeon[] {
            return this._surgeons;
        }

        set surgeons(newSurgeons: orthosensor.domains.Surgeon[]) {
            this._surgeons = newSurgeons;
        }

        get hospitals(): orthosensor.domains.Hospital[] {
            return this._hospitals;
        }
        set hospitals(newHospitals: orthosensor.domains.Hospital[]) {
            this._hospitals = newHospitals;
        }
        get hospital(): orthosensor.domains.Hospital {
            return this._hospital;
        }
        set hospital(newHospital: orthosensor.domains.Hospital) {
            this._hospital = newHospital;
        }
        get surveys(): Array<orthosensor.domains.Survey> {
            return this._surveys;
        }
        set surveys(newSurvey: Array<orthosensor.domains.Survey>) {
            this._surveys = newSurvey;
        }

        /* @ngInject */
        constructor(PatientListFactory: any,
            public SurveyService: orthosensor.services.SurveyService,
            public $q: ng.IQService,
            public HospitalService: orthosensor.services.HospitalService,
            public SFDataService: orthosensor.services.SFDataService) {
            this.PatientService = PatientListFactory;
            // this.hospitals = this.getHospitals();
            this.chartHeight = 280;
            this.surveys = new Array<orthosensor.domains.Survey>();
            // this.dashboardService = IQ_DashboardRepository;
        }

        public getHospitals(): orthosensor.domains.Hospital[] {
            // console.log(this.hospitals);
            if (this.hospitals !== undefined && this.hospitals.length > 0) {
                return this.hospitals;
            } else {
                this.hospitals = this.HospitalService.hospitals;
            }
        }

        public loadPractices(): void {
            this.SFDataService.loadPractices()
                .then((data: any[]) => {
                    // console.log(data);
                    this._practices = this.HospitalService.convertSFPracticeToObject(data);
                    // console.log(this.practices);
                }, (error: any) => {
                    console.log(error);
                });
        }

        public loadHospitalPractices(hospitalId: string): void {
            this.HospitalService.setPractices(hospitalId);
            this.practices = this.HospitalService.practices;
        }

        public getSurgeons(hospitalId: string): orthosensor.domains.Surgeon[] {
            return this.surgeons;
        }

        public setSurgeons(hospitalId: string): void {
            let surgeonData: orthosensor.domains.Surgeon[];
            // console.log(hospitalId);
            if (hospitalId !== null) {
                this.PatientService.loadSurgeons(hospitalId)
                    .then((data: any) => {
                        let surg = data;
                        // console.log(surg);
                        for (let i = 0; i < surg.length; i++) {
                            let surgeonR = surg[i].Surgeon__r;
                            let surgeonD: orthosensor.domains.Surgeon;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        this.surgeons = surgeonData;
                    }, (error: any) => {
                        console.log(error);
                    });
                // return surgeonData;
            }
        }      
        
        public getCaseActivityByPractice(practiceId: string, startDate: string, endDate: string): any[] {
            this.PatientService.getCaseActivityByPractice(practiceId, startDate, endDate)
                .then((data: any) => {
                    let results = data;
                    // console.log(results);
                    return results;
                    // this.surgeons = surgeonData;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return [];
        }

        public setSurveys(): void {
            this.surveys = [];
            this.SurveyService.loadSurveys()
                .then((data: any) => {
                    for (let i = 0; i <= data.length; i++) {
                        if (data[i] !== undefined) {
                            let survey: orthosensor.domains.Survey = new orthosensor.domains.Survey();
                            survey.id = data[i].Id;
                            survey.name = data[i].Name__c;
                            // console.log(survey);
                            this.surveys.push(survey);
                        }    
                    }
                }, (error: any) => {
                    console.log(error);
                    // return error;
                });
            
        }

        public formatDate(data: any): string {
            // console.log(data);
            let dateLabel: Date;
            dateLabel = moment(data).add(1, 'd').toDate();
            let month: number = moment(dateLabel).month() + 1;
            let smonth: string; 
            if (month < 10) {
                smonth = '0' + String(month);
            } else {
                smonth = String(month);
            }
            let formattedDate = moment(dateLabel).year() + '-' + smonth + '-02';
            console.log(formattedDate);
            return formattedDate;
        }        
        // public convertSFPracticeToObject(data: any[]): orthosensor.domains.Practice[] {
        //     let practices: orthosensor.domains.Practice[];
        //     practices = [];
        //     // console.log(data.length);
        //     for (let i = 0; i < data.length; i++) {
        //         let practice: orthosensor.domains.Practice = {
        //             id: data[i].Id,
        //             name: data[i].Name,
        //             parentId: data[i].ParentId,
        //             parentName: '' //data[i].Parent.Name
        //         };
        //         // console.log(practice);
        //         practices.push(practice);
        //     }
        //     // console.log(practices);
        //     return practices;
        // }

        


    }

    // DashboardService.$inject = ['PatientListFactory', 'SurveyService', '$q'];

    angular
        .module('orthosensor.dashboard')
        .service('DashboardService', DashboardService);
}
