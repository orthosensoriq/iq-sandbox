describe('component: osDashboard', function () {
    var $componentController;

    beforeEach(module('orthosensor'));
    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should expose set a a `hero` object', function () {
        // Here we are passing actual bindings to the component
        // var bindings = {hero: {name: 'Wolverine'}};
        var ctrl = $componentController('DashboardController', null, bindings);

        expect(ctrl.noOfItems).toBeDefined();
        expect(ctrl.noOfItems).toBe(3);
    });
  });