module orthosensor.dashboard {

    class PatientPhaseCountController extends DashboardChartBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', '$filter', 'config', '$rootScope'];

        public title: string;
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public patientPhaseCount: orthosensor.domains.ChartType;
        public chart: orthosensor.domains.ChartType;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        public startDate: any;
        public endDate: any;
        public allData: orthosensor.domains.PhaseCount[];
        // public chartType: string;
        public PatientService: any;
        public phases: Phase[];
        public chartType: string;

        ///// /* @ngInject */
        constructor(public DashboardService: any,
            public ChartService: any,
            PatientListFactory: any,
            UserService: any,
            HospitalService: orthosensor.services.HospitalService,
            moment: any, public $filter: any, config: any,
            public $rootScope: ng.IRootScopeService) {
            super(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope);
            this.PatientService = PatientListFactory;
        }

        public $onInit(): void {
            super.$onInit();
            this.title = 'Patient Phase Count';
            this.phases = this.getPhases();
            this.hospitalPracticeLabel = 'Practices';
            this.surgeonLabel = 'Surgeon';
            this.monthsToChart = "6";
            this.chartHeight = this.DashboardService.chartHeight;

            console.log(this.surgeons);
            this.setDateParams();
            console.log(this.chartType);
            this.initChartParams();
        };

        getPhases(): any[] {
            return [
                new Phase('PreOp', 'Pre Op', '"#b1b9bd"'),
                new Phase('PostOp', 'Post Op', '#109bd6'),
                new Phase('LongTerm', 'Long Term', '#cf0a2c'),
            ];
        }
        public setDateParams() {
            let today: Date = moment();
            let monthsInPast: number;
            let monthsInFuture: number;
            if ((Number(this.monthsToChart) % 2) !== 0) {
                monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                monthsInFuture = Math.ceil((Number(this.monthsToChart) / 2));
            } else {
                monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                monthsInFuture = Math.floor((Number(this.monthsToChart) / 2));
            }
            // console.log(today);
            let endDate: Date = moment(today).add(monthsInFuture, 'months');
            endDate = moment(endDate).endOf('month');
            // console.log(endDate);
            let sEndDate: string = moment(endDate).format(this.dateFormat);
            // console.log(sEndDate);
            let startMonth: Date = moment(today).subtract((monthsInPast - 1), 'months');
            let startDate: Date = moment(startMonth).startOf('month');

            this.startDate = moment(startDate).format(this.dateFormat);
            this.endDate = moment(endDate).format(this.dateFormat);
            // console.log(this.startDate);
        }

        public isHospital(): boolean {
            return true;
        }
        public initChartParams(): void {
            this.patientPhaseCount = new orthosensor.domains.ChartType();
            let service = this.ChartService;
            this.patientPhaseCount = service.chart;

            // if (this.chartType === 'line') {
            //     this.options = this.ChartService.getLineChartOptions();
            // } else {
            this.options = this.ChartService.getMultiBarChartOptions();

            this.ChartService.chartTitle = 'Patient Phase Chart';
            this.ChartService.xAxisLabel = 'Months';
            this.ChartService.xAxisLabel = 'Procedures';

            // console.log(this.options);
            this.initData();

        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    this.setSurgeonPartner();
                    break;

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                case orthosensor.domains.UserProfile.PracticeAdmin:
                    // console.log('practice Account');
                    this.setSurgeonPartner();
                    break;
                // case 'Customer Community User':
                //     // console.log('practice Account');
                //     this.setSurgeonPartner();
                //     break;
                // case 'Customer Community Plus User':
                //     // console.log('practice Account');
                //     this.setSurgeonPartner();
                //     break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonData();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticeData();
                }
            }
        }

        public setAdmin() {
            console.log(this.hospitals);
            // console.log(this.practices);
            // console.log(this.surgeons);
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            if (this.surgeons !== undefined) {
                                for (let i = 0; i < this.surgeons.length; i++) {
                                    if (this.surgeons[i].parentId === this.practice.id) {
                                        this.surgeon = this.surgeons[i];
                                        this.data = this.getSurgeonData();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // called from page
        public getChart(surgeon: any): any[] {
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    return this.getSurgeonData();

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    return this.getPracticeData();

                default:
                    return this.getSurgeonData();
            }
        }

        public getSurgeonData(): any[] {
            this.setDateParams();
            return this.getSurgeonPhaseCount(this.surgeon.id, this.startDate, this.endDate);
        }

        public getPracticeData(): any[] {
            this.setDateParams();
            return this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
        }

        public setChartData(allData: orthosensor.domains.PhaseCount[]): any[] {
            let data: any[] = [];
            this.allData = allData;
            // console.log(this.allData);
            let preOpData: any[] = this.getPreOpData();
            // console.log(preOpData);
            let preOpChartData = {
                "key": ["Pre Op"],
                "color": "#b1b9bd",
                "values": preOpData
            }
            data.push(preOpChartData);

            let postOpData: any[] = this.getPostOpData();
            // console.log(postOpData);
            let postOpChartData = {
                "key": ["Post Op"],
                "color": "#109bd6",
                "values": postOpData
            };
            data.push(postOpChartData);

            let longTermData: any[] = this.getLongTermData();
            // console.log(longTermData);
            let longTermChartData = {
                "key": ['Long Term'],
                "color": "#cf0a2c",
                "values": longTermData
            };
            data.push(longTermChartData);
            // console.log(data);
            this.data = data;
            return data;
        }

        private validate(): boolean {
            return true;
        }

        public getPreOpData(): any[] {
            let data: orthosensor.domains.PhaseCount[] = this.getPhaseData('PreOp');
            console.log(data);
            return this.convertPhaseData(data);
        }

        public getPostOpData(): any[] {
            let data: orthosensor.domains.PhaseCount[] = this.getPhaseData('PostOp');
            console.log(data);
            return this.convertPhaseData(data);
        }

        public getLongTermData(): any[] {
            let data: orthosensor.domains.PhaseCount[] = this.getPhaseData('Long Term');
            console.log(data);
            return this.convertPhaseData(data);
        }

        public getPhaseData(phaseName: string): orthosensor.domains.PhaseCount[] {
            let phases: orthosensor.domains.PhaseCount[] = [];
            console.log(this.allData);
            for (let i = 0; i < this.allData.length; i++) {
                if (this.allData[i].phase === phaseName) {
                    // console.log(this.allData[i]);
                    let phase: orthosensor.domains.PhaseCount = this.allData[i];
                    phases.push(phase);
                }
            }
            console.log(phases);
            return phases;
        }

        private convertPhaseData(filteredData: orthosensor.domains.PhaseCount[]): any[] {
            this.setDateParams();
            let items: orthosensor.domains.ChartDataElement[] = [];
            let item: orthosensor.domains.ChartDataElement;
            item = new orthosensor.domains.ChartDataElement();

            // items.push(item);
            let chartData: any[] = [];

            console.log(filteredData);
            if (filteredData) {
                for (let i = 0; i < filteredData.length; i++) {
                    let dateLabel: string = this.formatDate(filteredData[i].startDate);
                    item = { label: dateLabel, value: filteredData[i].count };
                    items.push(item);
                    chartData.push([dateLabel, filteredData[i].count]);
                }
            }
            // console.log(items);

            return items;
        }

        private getSurgeonPhaseCount(surgeonId: string, startDate: string, endDate: string): orthosensor.domains.PhaseCount[] {
            let surgeon: string = this.surgeon.id;

            this.PatientService.getSurgeonPhaseCount(surgeon, this.startDate, this.endDate)
                .then((data: any) => {
                    let results = data;
                    // console.log(results);
                    let phaseCount: orthosensor.domains.PhaseCount[];
                    phaseCount = this.convertSFPhaseCountsToObject(results);
                    console.log(phaseCount);
                    if (phaseCount.length > 0) {
                        phaseCount = this.createBlankMonths(phaseCount);
                        //phaseCount = this.$filter('orderBy')(phaseCount, 'startDate');
                        let sortedPhases: orthosensor.domains.PhaseCount[];
                        sortedPhases = phaseCount.slice(0);
                        sortedPhases = phaseCount.sort((n1, n2) => {
                            if (n1.startDate > n2.startDate) {
                                return 1;
                            }

                            if (n1.startDate < n2.startDate) {
                                return -1;
                            }

                            return 0;
                        });

                        // console.log(phaseCount);
                        let chartData = this.setChartData(phaseCount);
                        return chartData;
                    } else {
                        return [];
                    }
                    // return results;
                    //this.surgeons = surgeonData;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return [];

        }

        private aggregatePracticeData(data: orthosensor.domains.PhaseCount[]): orthosensor.domains.PhaseCount[] {
            let temp: orthosensor.domains.PhaseCount[] = [];
            // data = this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                let found = false;
                for (let j = 0; j < temp.length; j++) {
                    if (data[i].startDate === temp[j].startDate) {
                        found = true;
                        temp[j].count = data[i].count + temp[i].count;
                    }
                }
                if (!found) {
                    temp.push(data[i]);
                }
            }
            return temp;
        }

        private getPracticePhaseCount(practiceId: string, startDate: string, endDate: string): orthosensor.domains.PhaseCount[] {
            let practice: string = this.practice.id;
            this.PatientService.getPracticePhaseCount(practice, this.startDate, this.endDate)
                .then((data: any) => {
                    let results = data;
                    // console.log('Practice Phase count');
                    // console.log(results);
                    let phaseCount: orthosensor.domains.PhaseCount[];
                    phaseCount = this.convertSFPhaseCountsToObject(results);
                    // console.log(phaseCount);
                    phaseCount = this.aggregatePracticeData(phaseCount);
                    // console.log(phaseCount);
                    let chartData = this.setChartData(phaseCount);
                    return chartData;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return [];

        }

        // convert to object - dates come in last day of the prior month
        private convertSFPhaseCountsToObject(data: any[]): orthosensor.domains.PhaseCount[] {
            let phaseCounts: orthosensor.domains.PhaseCount[] = [];
            console.log(data);
            for (let i = 0; i < data.length; i++) {
                let phaseCount: orthosensor.domains.PhaseCount = new orthosensor.domains.PhaseCount();
                phaseCount.id = data[i].Id;
                phaseCount.phase = data[i].Name;
                phaseCount.startDate = moment(data[i].Month_Date__c).add(1, 'd');
                phaseCount.startDateString = moment(phaseCount.startDate).format(this.dateFormat);
                phaseCount.surgeonId = data[i].Surgeon_ID__c;
                phaseCount.practiceId = data[i].Parent_ID__c;
                phaseCount.count = data[i].Count__c;

                phaseCounts.push(phaseCount);
            }
            // }    
            // console.log(phaseCounts);
            return phaseCounts;
        }

        // assumes we are doing one surgeon at a time        
        private createBlankMonths(phaseCounts: orthosensor.domains.PhaseCount[]): orthosensor.domains.PhaseCount[] {
            let phaseCounts2: orthosensor.domains.PhaseCount[] = phaseCounts;
            for (let i = 0; i < Number(this.monthsToChart); i++) {
                let found = false;
                let dateToCompare: Date = moment(this.startDate).add(i, 'months');
                // console.log(dateToCompare);
                console.log(this.phases);
                for (let j = 0; j < this.phases.length; j++) {
                    for (let k = 0; k < phaseCounts.length; k++) {
                        // console.log(phaseCounts[i]);
                        if (moment(dateToCompare).format(this.dateFormat) === phaseCounts[k].startDateString && this.phases[j].name === phaseCounts[k].phase) {
                            found = true;
                            // console.log(found = true);
                        }
                    }
                    if (!found) {
                        let phaseCount: orthosensor.domains.PhaseCount = this.createPhaseCountRecord(phaseCounts[0].id,
                            this.phases[j].name,
                            moment(dateToCompare).toDate(),
                            moment(dateToCompare).format(this.dateFormat),
                            phaseCounts[0].surgeonId,
                            phaseCounts[0].practiceId,
                            0);
                        // console.log(phaseCount);
                        phaseCounts2.push(phaseCount);
                        // console.log(phaseCounts2);
                    }
                }
            }

            return phaseCounts2;
        }
        private createPhaseCountRecord(id: string, name: string, toDate: Date, sDate: string, surgeonId: string, practiceId: string, count: number): orthosensor.domains.PhaseCount {
            let phaseCount: orthosensor.domains.PhaseCount = new orthosensor.domains.PhaseCount();
            phaseCount.id = id;
            phaseCount.phase = name;
            phaseCount.startDate = toDate;
            phaseCount.startDateString = sDate;
            phaseCount.surgeonId = surgeonId;
            phaseCount.practiceId = practiceId;
            phaseCount.count = count;
            // console.log(phaseCount);
            return phaseCount;
        }

    }

    class PatientPhaseCountChart implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                chartType: '@'
            };
            this.controller = PatientPhaseCountController;
            this.templateUrl = 'app/dashboard/patientPhaseCount/patientPhaseCount.component.html';
        }
    }
    angular
        .module('orthosensor.dashboard')
        .component('osPatientPhaseCount', new PatientPhaseCountChart());

    class Phase {
        constructor(
            public name: string,
            public displayName: string,
            public color: string,
        ) {
        }
    }
}    
