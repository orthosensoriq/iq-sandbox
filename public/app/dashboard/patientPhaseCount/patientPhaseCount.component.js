var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientPhaseCountController = (function (_super) {
            __extends(PatientPhaseCountController, _super);
            ///// /* @ngInject */
            function PatientPhaseCountController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, $filter, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.$filter = $filter;
                _this.$rootScope = $rootScope;
                _this.PatientService = PatientListFactory;
                return _this;
            }
            PatientPhaseCountController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.title = 'Patient Phase Count';
                this.phases = this.getPhases();
                this.hospitalPracticeLabel = 'Practices';
                this.surgeonLabel = 'Surgeon';
                this.monthsToChart = "6";
                this.chartHeight = this.DashboardService.chartHeight;
                console.log(this.surgeons);
                this.setDateParams();
                console.log(this.chartType);
                this.initChartParams();
            };
            ;
            PatientPhaseCountController.prototype.getPhases = function () {
                return [
                    new Phase('PreOp', 'Pre Op', '"#b1b9bd"'),
                    new Phase('PostOp', 'Post Op', '#109bd6'),
                    new Phase('LongTerm', 'Long Term', '#cf0a2c'),
                ];
            };
            PatientPhaseCountController.prototype.setDateParams = function () {
                var today = moment();
                var monthsInPast;
                var monthsInFuture;
                if ((Number(this.monthsToChart) % 2) !== 0) {
                    monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                    monthsInFuture = Math.ceil((Number(this.monthsToChart) / 2));
                }
                else {
                    monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                    monthsInFuture = Math.floor((Number(this.monthsToChart) / 2));
                }
                // console.log(today);
                var endDate = moment(today).add(monthsInFuture, 'months');
                endDate = moment(endDate).endOf('month');
                // console.log(endDate);
                var sEndDate = moment(endDate).format(this.dateFormat);
                // console.log(sEndDate);
                var startMonth = moment(today).subtract((monthsInPast - 1), 'months');
                var startDate = moment(startMonth).startOf('month');
                this.startDate = moment(startDate).format(this.dateFormat);
                this.endDate = moment(endDate).format(this.dateFormat);
                // console.log(this.startDate);
            };
            PatientPhaseCountController.prototype.isHospital = function () {
                return true;
            };
            PatientPhaseCountController.prototype.initChartParams = function () {
                this.patientPhaseCount = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.patientPhaseCount = service.chart;
                // if (this.chartType === 'line') {
                //     this.options = this.ChartService.getLineChartOptions();
                // } else {
                this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Patient Phase Chart';
                this.ChartService.xAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Procedures';
                // console.log(this.options);
                this.initData();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientPhaseCountController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('practice Account');
                        this.setSurgeonPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('practice Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('practice Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientPhaseCountController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            PatientPhaseCountController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            PatientPhaseCountController.prototype.setAdmin = function () {
                console.log(this.hospitals);
                // console.log(this.practices);
                // console.log(this.surgeons);
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getSurgeonData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            // called from page
            PatientPhaseCountController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PatientPhaseCountController.prototype.getSurgeonData = function () {
                this.setDateParams();
                return this.getSurgeonPhaseCount(this.surgeon.id, this.startDate, this.endDate);
            };
            PatientPhaseCountController.prototype.getPracticeData = function () {
                this.setDateParams();
                return this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
            };
            PatientPhaseCountController.prototype.setChartData = function (allData) {
                var data = [];
                this.allData = allData;
                // console.log(this.allData);
                var preOpData = this.getPreOpData();
                // console.log(preOpData);
                var preOpChartData = {
                    "key": ["Pre Op"],
                    "color": "#b1b9bd",
                    "values": preOpData
                };
                data.push(preOpChartData);
                var postOpData = this.getPostOpData();
                // console.log(postOpData);
                var postOpChartData = {
                    "key": ["Post Op"],
                    "color": "#109bd6",
                    "values": postOpData
                };
                data.push(postOpChartData);
                var longTermData = this.getLongTermData();
                // console.log(longTermData);
                var longTermChartData = {
                    "key": ['Long Term'],
                    "color": "#cf0a2c",
                    "values": longTermData
                };
                data.push(longTermChartData);
                // console.log(data);
                this.data = data;
                return data;
            };
            PatientPhaseCountController.prototype.validate = function () {
                return true;
            };
            PatientPhaseCountController.prototype.getPreOpData = function () {
                var data = this.getPhaseData('PreOp');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getPostOpData = function () {
                var data = this.getPhaseData('PostOp');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getLongTermData = function () {
                var data = this.getPhaseData('Long Term');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getPhaseData = function (phaseName) {
                var phases = [];
                console.log(this.allData);
                for (var i = 0; i < this.allData.length; i++) {
                    if (this.allData[i].phase === phaseName) {
                        // console.log(this.allData[i]);
                        var phase = this.allData[i];
                        phases.push(phase);
                    }
                }
                console.log(phases);
                return phases;
            };
            PatientPhaseCountController.prototype.convertPhaseData = function (filteredData) {
                this.setDateParams();
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                console.log(filteredData);
                if (filteredData) {
                    for (var i = 0; i < filteredData.length; i++) {
                        var dateLabel = this.formatDate(filteredData[i].startDate);
                        item = { label: dateLabel, value: filteredData[i].count };
                        items.push(item);
                        chartData.push([dateLabel, filteredData[i].count]);
                    }
                }
                // console.log(items);
                return items;
            };
            PatientPhaseCountController.prototype.getSurgeonPhaseCount = function (surgeonId, startDate, endDate) {
                var _this = this;
                var surgeon = this.surgeon.id;
                this.PatientService.getSurgeonPhaseCount(surgeon, this.startDate, this.endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log(results);
                    var phaseCount;
                    phaseCount = _this.convertSFPhaseCountsToObject(results);
                    console.log(phaseCount);
                    if (phaseCount.length > 0) {
                        phaseCount = _this.createBlankMonths(phaseCount);
                        //phaseCount = this.$filter('orderBy')(phaseCount, 'startDate');
                        var sortedPhases = void 0;
                        sortedPhases = phaseCount.slice(0);
                        sortedPhases = phaseCount.sort(function (n1, n2) {
                            if (n1.startDate > n2.startDate) {
                                return 1;
                            }
                            if (n1.startDate < n2.startDate) {
                                return -1;
                            }
                            return 0;
                        });
                        // console.log(phaseCount);
                        var chartData = _this.setChartData(phaseCount);
                        return chartData;
                    }
                    else {
                        return [];
                    }
                    // return results;
                    //this.surgeons = surgeonData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            PatientPhaseCountController.prototype.aggregatePracticeData = function (data) {
                var temp = [];
                // data = this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var found = false;
                    for (var j = 0; j < temp.length; j++) {
                        if (data[i].startDate === temp[j].startDate) {
                            found = true;
                            temp[j].count = data[i].count + temp[i].count;
                        }
                    }
                    if (!found) {
                        temp.push(data[i]);
                    }
                }
                return temp;
            };
            PatientPhaseCountController.prototype.getPracticePhaseCount = function (practiceId, startDate, endDate) {
                var _this = this;
                var practice = this.practice.id;
                this.PatientService.getPracticePhaseCount(practice, this.startDate, this.endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log('Practice Phase count');
                    // console.log(results);
                    var phaseCount;
                    phaseCount = _this.convertSFPhaseCountsToObject(results);
                    // console.log(phaseCount);
                    phaseCount = _this.aggregatePracticeData(phaseCount);
                    // console.log(phaseCount);
                    var chartData = _this.setChartData(phaseCount);
                    return chartData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            // convert to object - dates come in last day of the prior month
            PatientPhaseCountController.prototype.convertSFPhaseCountsToObject = function (data) {
                var phaseCounts = [];
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var phaseCount = new orthosensor.domains.PhaseCount();
                    phaseCount.id = data[i].Id;
                    phaseCount.phase = data[i].Name;
                    phaseCount.startDate = moment(data[i].Month_Date__c).add(1, 'd');
                    phaseCount.startDateString = moment(phaseCount.startDate).format(this.dateFormat);
                    phaseCount.surgeonId = data[i].Surgeon_ID__c;
                    phaseCount.practiceId = data[i].Parent_ID__c;
                    phaseCount.count = data[i].Count__c;
                    phaseCounts.push(phaseCount);
                }
                // }    
                // console.log(phaseCounts);
                return phaseCounts;
            };
            // assumes we are doing one surgeon at a time        
            PatientPhaseCountController.prototype.createBlankMonths = function (phaseCounts) {
                var phaseCounts2 = phaseCounts;
                for (var i = 0; i < Number(this.monthsToChart); i++) {
                    var found = false;
                    var dateToCompare = moment(this.startDate).add(i, 'months');
                    // console.log(dateToCompare);
                    console.log(this.phases);
                    for (var j = 0; j < this.phases.length; j++) {
                        for (var k = 0; k < phaseCounts.length; k++) {
                            // console.log(phaseCounts[i]);
                            if (moment(dateToCompare).format(this.dateFormat) === phaseCounts[k].startDateString && this.phases[j].name === phaseCounts[k].phase) {
                                found = true;
                                // console.log(found = true);
                            }
                        }
                        if (!found) {
                            var phaseCount = this.createPhaseCountRecord(phaseCounts[0].id, this.phases[j].name, moment(dateToCompare).toDate(), moment(dateToCompare).format(this.dateFormat), phaseCounts[0].surgeonId, phaseCounts[0].practiceId, 0);
                            // console.log(phaseCount);
                            phaseCounts2.push(phaseCount);
                            // console.log(phaseCounts2);
                        }
                    }
                }
                return phaseCounts2;
            };
            PatientPhaseCountController.prototype.createPhaseCountRecord = function (id, name, toDate, sDate, surgeonId, practiceId, count) {
                var phaseCount = new orthosensor.domains.PhaseCount();
                phaseCount.id = id;
                phaseCount.phase = name;
                phaseCount.startDate = toDate;
                phaseCount.startDateString = sDate;
                phaseCount.surgeonId = surgeonId;
                phaseCount.practiceId = practiceId;
                phaseCount.count = count;
                // console.log(phaseCount);
                return phaseCount;
            };
            return PatientPhaseCountController;
        }(dashboard.DashboardChartBaseController));
        PatientPhaseCountController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', '$filter', 'config', '$rootScope'];
        var PatientPhaseCountChart = (function () {
            function PatientPhaseCountChart() {
                this.bindings = {
                    chartType: '@'
                };
                this.controller = PatientPhaseCountController;
                this.templateUrl = 'app/dashboard/patientPhaseCount/patientPhaseCount.component.html';
            }
            return PatientPhaseCountChart;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientPhaseCount', new PatientPhaseCountChart());
        var Phase = (function () {
            function Phase(name, displayName, color) {
                this.name = name;
                this.displayName = displayName;
                this.color = color;
            }
            return Phase;
        }());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientPhaseCount.component.js.map