var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        'use strict';
        var PromDeltaService = (function () {
            function PromDeltaService(DashboardService, DashboardDataService, UserService, moment) {
                this.DashboardService = DashboardService;
                this.DashboardDataService = DashboardDataService;
                this.isUserAdmin = UserService.isUserAdmin;
                // these values are being hard coded for KOOS and current event types
                this.eventTypes = ['Pre-Op', '4-8 Week Follow-up', '6 Month Follow-up', '12 Month Follow-up', '2 Year Follow-up'];
            }
            PromDeltaService.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        _this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            //// stopped here - creating service to manage data collection
            PromDeltaService.prototype.getData = function () {
                //console.log(this.promDeltas);
                //this.data = this.chartData;
                return this.data;
            };
            PromDeltaService.prototype.convertDataToObjects = function (data) {
                var promDeltas = [];
                // console.log(data);
                for (var k = 0; k <= this.eventTypes.length; k++) {
                    for (var i = 0; i <= data.length; i++) {
                        // make sure a record exists!
                        if (data[i] !== undefined) {
                            if (data[i].Stage__c === this.eventTypes[k]) {
                                var delta = this.convertSFToObject(data[i]);
                                promDeltas.push(delta);
                            }
                        }
                    }
                }
                return promDeltas;
            };
            // stopped here 2/3/17 - need to add another method for aggregate objects and one for detail
            // convert to object - dates come in last day of the prior month
            PromDeltaService.prototype.convertSFToObject = function (data) {
                // console.log(data);
                var promDelta = new orthosensor.domains.PromDelta();
                promDelta.id = data.Id;
                promDelta.practiceId = data.PracticeID__c;
                promDelta.surgeonId = data.SurgeonID__c;
                promDelta.practiceName = data.PracticeName__c;
                promDelta.surgeonName = data.SurgeonName__c;
                promDelta.section = data.Section__c;
                promDelta.stage = data.Stage__c;
                promDelta.score = data.Score__c;
                promDelta.patientName = data.PatientName__c;
                promDelta.patientIde = data.PatientID__c;
                promDelta.procedureDate = moment(data.ProcedureDate__c);
                // console.log(promDelta);
                return promDelta;
            };
            return PromDeltaService;
        }());
        PromDeltaService.inject = ['DashboardService', 'DashboardDataService', 'UserService', 'moment'];
        dashboard.PromDeltaService = PromDeltaService;
        angular
            .module('orthosensor.dashboard')
            .service('PromDeltaService', PromDeltaService);
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=promDelta.service.js.map