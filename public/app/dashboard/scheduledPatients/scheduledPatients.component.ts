module orthosensor.dashboard {

    class ScheduledPatientsController extends DashboardBaseController {

        public static $inject = ['DashboardService', 'DashboardDataService', 'PatientListFactory', 'UserService', 'PatientService', '$state', '$timeout', 'HospitalService', '$rootScope'];
        public patients: any[];
        public title: string;
        public period: string;
        constructor(public DashboardService: orthosensor.services.DashboardService,
            public DashboardDataService: any,
            public PatientListFactory: any,
            public UserService: orthosensor.services.UserService,
            public PatientService: orthosensor.services.PatientService,
            public $state: ng.ui.IStateService,
            public $timeout: ng.ITimeoutService,
            public HospitalService: orthosensor.services.HospitalService,
            $rootScope: ng.IRootScopeService) {
            super(UserService, HospitalService, $rootScope);
            this.title = 'Scheduled Procedures';
        }

        public $onInit(): void {
            super.$onInit();
            let practiceId = this.UserService.user.accountId;
            //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            // let daysInFuture: number = 60;
            this.period = '30';
            if (!this.isUserAdmin) {
                this.DashboardDataService.getScheduledPatients(practiceId, Number(this.period))
                    .then((data: any) => {
                        this.patients = data;
                        // console.log(this.patients);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
        }

        public patientDetails(patient: any): void {
            // console.log(patient);
            this.PatientService.setPatientId(patient.patientId);
            this.$timeout(() => {
                this.$state.go('patientDetails');
            }, 1000);
        }
        public changePeriod(): void {
            let practiceId = this.UserService.user.accountId;
            // console.log(this.period);
            let daysInFuture: number = Number(this.period);
            this.DashboardDataService.getScheduledPatients(practiceId, daysInFuture)
                .then((data: any) => {
                    this.patients = data;
                    // console.log(this.patients);
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
        }
    }

    class ScheduledPatients implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                // textBinding: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = ScheduledPatientsController;
            this.templateUrl = 'app/dashboard/scheduledPatients/scheduledPatients.component.html';
        }


    }
    angular
        .module('orthosensor.dashboard')
        .component('osScheduledPatients', new ScheduledPatients());
}