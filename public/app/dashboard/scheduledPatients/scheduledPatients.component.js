var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var ScheduledPatientsController = (function (_super) {
            __extends(ScheduledPatientsController, _super);
            function ScheduledPatientsController(DashboardService, DashboardDataService, PatientListFactory, UserService, PatientService, $state, $timeout, HospitalService, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.DashboardDataService = DashboardDataService;
                _this.PatientListFactory = PatientListFactory;
                _this.UserService = UserService;
                _this.PatientService = PatientService;
                _this.$state = $state;
                _this.$timeout = $timeout;
                _this.HospitalService = HospitalService;
                _this.title = 'Scheduled Procedures';
                return _this;
            }
            ScheduledPatientsController.prototype.$onInit = function () {
                var _this = this;
                _super.prototype.$onInit.call(this);
                var practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                // let daysInFuture: number = 60;
                this.period = '30';
                if (!this.isUserAdmin) {
                    this.DashboardDataService.getScheduledPatients(practiceId, Number(this.period))
                        .then(function (data) {
                        _this.patients = data;
                        // console.log(this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            ScheduledPatientsController.prototype.patientDetails = function (patient) {
                var _this = this;
                // console.log(patient);
                this.PatientService.setPatientId(patient.patientId);
                this.$timeout(function () {
                    _this.$state.go('patientDetails');
                }, 1000);
            };
            ScheduledPatientsController.prototype.changePeriod = function () {
                var _this = this;
                var practiceId = this.UserService.user.accountId;
                // console.log(this.period);
                var daysInFuture = Number(this.period);
                this.DashboardDataService.getScheduledPatients(practiceId, daysInFuture)
                    .then(function (data) {
                    _this.patients = data;
                    // console.log(this.patients);
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return ScheduledPatientsController;
        }(dashboard.DashboardBaseController));
        ScheduledPatientsController.$inject = ['DashboardService', 'DashboardDataService', 'PatientListFactory', 'UserService', 'PatientService', '$state', '$timeout', 'HospitalService', '$rootScope'];
        var ScheduledPatients = (function () {
            function ScheduledPatients() {
                this.bindings = {};
                this.controller = ScheduledPatientsController;
                this.templateUrl = 'app/dashboard/scheduledPatients/scheduledPatients.component.html';
            }
            return ScheduledPatients;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osScheduledPatients', new ScheduledPatients());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=scheduledPatients.component.js.map