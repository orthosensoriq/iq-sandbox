var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var DashboardService = (function () {
            /* @ngInject */
            function DashboardService(PatientListFactory, SurveyService, $q, HospitalService, SFDataService) {
                this.SurveyService = SurveyService;
                this.$q = $q;
                this.HospitalService = HospitalService;
                this.SFDataService = SFDataService;
                this.PatientService = PatientListFactory;
                // this.hospitals = this.getHospitals();
                this.chartHeight = 280;
                this.surveys = new Array();
                // this.dashboardService = IQ_DashboardRepository;
            }
            Object.defineProperty(DashboardService.prototype, "practices", {
                // private dashboardService: any;
                get: function () {
                    return this._practices;
                },
                set: function (newPractices) {
                    this._practices = newPractices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surgeons", {
                get: function () {
                    return this._surgeons;
                },
                set: function (newSurgeons) {
                    this._surgeons = newSurgeons;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (newHospitals) {
                    this._hospitals = newHospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospital", {
                get: function () {
                    return this._hospital;
                },
                set: function (newHospital) {
                    this._hospital = newHospital;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surveys", {
                get: function () {
                    return this._surveys;
                },
                set: function (newSurvey) {
                    this._surveys = newSurvey;
                },
                enumerable: true,
                configurable: true
            });
            DashboardService.prototype.getHospitals = function () {
                // console.log(this.hospitals);
                if (this.hospitals !== undefined && this.hospitals.length > 0) {
                    return this.hospitals;
                }
                else {
                    this.hospitals = this.HospitalService.hospitals;
                }
            };
            DashboardService.prototype.loadPractices = function () {
                var _this = this;
                this.SFDataService.loadPractices()
                    .then(function (data) {
                    // console.log(data);
                    _this._practices = _this.HospitalService.convertSFPracticeToObject(data);
                    // console.log(this.practices);
                }, function (error) {
                    console.log(error);
                });
            };
            DashboardService.prototype.loadHospitalPractices = function (hospitalId) {
                this.HospitalService.setPractices(hospitalId);
                this.practices = this.HospitalService.practices;
            };
            DashboardService.prototype.getSurgeons = function (hospitalId) {
                return this.surgeons;
            };
            DashboardService.prototype.setSurgeons = function (hospitalId) {
                var _this = this;
                var surgeonData;
                // console.log(hospitalId);
                if (hospitalId !== null) {
                    this.PatientService.loadSurgeons(hospitalId)
                        .then(function (data) {
                        var surg = data;
                        // console.log(surg);
                        for (var i = 0; i < surg.length; i++) {
                            var surgeonR = surg[i].Surgeon__r;
                            var surgeonD = void 0;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        _this.surgeons = surgeonData;
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            DashboardService.prototype.getCaseActivityByPractice = function (practiceId, startDate, endDate) {
                this.PatientService.getCaseActivityByPractice(practiceId, startDate, endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log(results);
                    return results;
                    // this.surgeons = surgeonData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            DashboardService.prototype.setSurveys = function () {
                var _this = this;
                this.surveys = [];
                this.SurveyService.loadSurveys()
                    .then(function (data) {
                    for (var i = 0; i <= data.length; i++) {
                        if (data[i] !== undefined) {
                            var survey = new orthosensor.domains.Survey();
                            survey.id = data[i].Id;
                            survey.name = data[i].Name__c;
                            // console.log(survey);
                            _this.surveys.push(survey);
                        }
                    }
                }, function (error) {
                    console.log(error);
                    // return error;
                });
            };
            DashboardService.prototype.formatDate = function (data) {
                // console.log(data);
                var dateLabel;
                dateLabel = moment(data).add(1, 'd').toDate();
                var month = moment(dateLabel).month() + 1;
                var smonth;
                if (month < 10) {
                    smonth = '0' + String(month);
                }
                else {
                    smonth = String(month);
                }
                var formattedDate = moment(dateLabel).year() + '-' + smonth + '-02';
                console.log(formattedDate);
                return formattedDate;
            };
            return DashboardService;
        }());
        DashboardService.inject = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
        services.DashboardService = DashboardService;
        // DashboardService.$inject = ['PatientListFactory', 'SurveyService', '$q'];
        angular
            .module('orthosensor.dashboard')
            .service('DashboardService', DashboardService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.service.js.map