module orthosensor.dashboard {

    class CaseActivityChartController extends DashboardChartBaseController implements orthosensor.dashboard.DashboardBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'CaseActivityService', 'moment', 'config', '$rootScope'];

        public title: string;
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public caseActivityChart: orthosensor.domains.ChartType;
        public chart: orthosensor.domains.ChartType;

        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;

        public PatientService: any;

        // public isHospital(): boolean;
        // public $onInit(): void;

        ///// /* @ngInject */
        constructor(public DashboardService: orthosensor.services.DashboardService,
            public ChartService: any,
            PatientListFactory: any,
            public UserService: orthosensor.services.UserService,
            public HospitalService: orthosensor.services.HospitalService,
            public CaseActivityService: orthosensor.services.CaseActivityService,
            moment: any, config: any,
            public $rootScope: ng.IRootScopeService) {
            super(DashboardService, ChartService,
                PatientListFactory, UserService,
                HospitalService, moment, config, $rootScope);
            this.PatientService = PatientListFactory;
            this.title = 'Case Activity Chart';

            this.hospitalPracticeLabel = 'Practices';

            // this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.monthsToChart = '3';
            this.chartHeight = DashboardService.chartHeight;
        }

        public $onInit(): void {
            super.$onInit();
            console.log(this.user);
            // handle surgeon or practice staff
            console.log(this.surgeons);
            this.setDateParams();
            this.initChartParams();
            this.initData();
        };

        public initChartParams(): void {
            console.log('Initializing chart');
            // this.initChartLookups();
            this.caseActivityChart = new orthosensor.domains.ChartType();
            // let data: orthosensor.domains.ChartDataElement[];
            let service = this.ChartService;
            this.caseActivityChart = service.chart;
            // this.caseActivityChart.options.title = 'Case Activity Chart';
            this.options = this.ChartService.getMultiBarChartOptions();
            this.ChartService.chartTitle = 'Case Activity Chart';
            this.ChartService.xAxisLabel = 'Months';
            this.ChartService.xAxisLabel = 'Procedure Count';
            if (!this.isDashboard) {
                // this.ChartService.chartHeight = 480;
            }
        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                case orthosensor.domains.UserProfile.PracticeAdmin:
                    // console.log('hospital Account');
                    this.setSurgeonPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            console.log(this.surgeons);
            console.log(this.practice.id);
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getData();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getData();
                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            if (this.surgeons !== undefined) {
                                for (let i = 0; i < this.surgeons.length; i++) {
                                    if (this.surgeons[i].parentId === this.practice.id) {
                                        this.surgeon = this.surgeons[i];
                                        this.data = this.getData();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        getPracticeData(): any[] {
            console.log(this.practice);
            console.log(this.surgeons);
            if (this.practice) {
                this.surgeon = this.surgeons[0];
            }
            return [];
        }
        public getData(): any[] {
            let data: any[] = [];
            let averageData: any[] = this.getAverageData();
            console.log(averageData);
            let averageChartData: {};
            averageChartData = {
                "key": ["Average"],
                "color": "#b1b9bd",
                "values": averageData
            };
            data.push(averageChartData);
            if (this.user.userProfile !== orthosensor.domains.UserProfile.HospitalAdmin && this.user.userProfile !== orthosensor.domains.UserProfile.PracticeAdmin) {
                let surgeonChartData: {};
                let surgeonData: any[] = this.getSurgeonData();
                console.log(surgeonData);
                surgeonChartData = {
                    "key": [this.surgeon.name],
                    "color": "#109bd6",
                    "values": surgeonData
                };
                data.push(surgeonChartData);
            }
            console.log(data);
            this.data = data;
            return data;
        }

        public getAverages() {
            // console.log(this.practice);
            return this.getPracticeData();
        }

        public getChart() {
            this.setDateParams();
            this.getData();
        }

        getSurgeonData(): any[] {
            if (this.surgeon === null) {
                if (this.surgeons.length > 0) {
                    this.surgeon = this.surgeons[0];
                }
            }
            console.log(this.surgeon);

            let items: orthosensor.domains.ChartDataElement[] = [];
            let item: orthosensor.domains.ChartDataElement;
            item = new orthosensor.domains.ChartDataElement();

            // items.push(item);
            let chartData: any[] = [];

            this.PatientService.getCaseActivityBySurgeon(this.surgeon.id, this.startDate, this.endDate)
                .then((data: any) => {
                    // console.log(data);

                    for (let i = 0; i < data.length; i++) {
                        let dateLabel: string = this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return items;
        }

        private getAverageData(): any[] {
            console.log(this.practice);
            let items: orthosensor.domains.ChartDataElement[] = [];
            
            if (this.UserService.user.userProfile === orthosensor.domains.UserProfile.HospitalAdmin) {
                items = null;
            } else {
                items = this.CaseActivityService.getActivityByPractice(this.practice.id, this.startDate, this.endDate);
            }
            return items;
        }
    }
    class CaseActivityChart implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                // textBinding: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = CaseActivityChartController;
            this.templateUrl = 'app/dashboard/caseActivityChart/caseActivityChart.component.html';
        }
    }

    angular
        .module('orthosensor.dashboard')
        .component('osCaseActivityChart', new CaseActivityChart());
}
