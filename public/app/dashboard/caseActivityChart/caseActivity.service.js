var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        'use strict';
        var CaseActivityService = (function () {
            function CaseActivityService(DashboardDataService, UserService, PatientListFactory, DashboardService) {
                this.DashboardDataService = DashboardDataService;
                this.UserService = UserService;
                this.PatientListFactory = PatientListFactory;
                this.DashboardService = DashboardService;
                // this.PatientService = PatientListFactory;
            }
            CaseActivityService.prototype.getActivityByPractice = function (practiceId, startDate, endDate) {
                var _this = this;
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                console.log(practiceId, startDate, endDate);
                this.PatientListFactory.getCaseActivityByPractice(practiceId, startDate, endDate)
                    .then(function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.DashboardService.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                    }
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            return CaseActivityService;
        }());
        CaseActivityService.inject = ['DashboardDataService', 'UserService', 'PatientService', 'DashboardService'];
        services.CaseActivityService = CaseActivityService;
        angular
            .module('orthosensor.services')
            .service('CaseActivityService', CaseActivityService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseActivity.service.js.map