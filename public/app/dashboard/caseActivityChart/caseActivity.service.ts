module orthosensor.services {
    'use strict';

   
    export class CaseActivityService{
        static inject: Array<string> = ['DashboardDataService', 'UserService', 'PatientService', 'DashboardService'];
        
        constructor(public DashboardDataService: orthosensor.services.DashboardDataService,
            public UserService: orthosensor.services.UserService,
            public PatientListFactory: any,
            public DashboardService: orthosensor.services.DashboardService) {
            
            // this.PatientService = PatientListFactory;
        }
        getActivityByPractice(practiceId: string, startDate, endDate) {
            let items: orthosensor.domains.ChartDataElement[] = [];
            let item: orthosensor.domains.ChartDataElement;
            item = new orthosensor.domains.ChartDataElement();
            console.log(practiceId, startDate, endDate);
            this.PatientListFactory.getCaseActivityByPractice(practiceId, startDate, endDate)
                .then((data: any) => {
                    console.log(data);

                    for (let i = 0; i < data.length; i++) {
                        let dateLabel: string = this.DashboardService.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                    }
                    return items;
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return items;
        }        
    }
    angular
        .module('orthosensor.services')
        .service('CaseActivityService', CaseActivityService);
}
