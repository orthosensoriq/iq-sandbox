var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var CaseActivityChartController = (function (_super) {
            __extends(CaseActivityChartController, _super);
            // public isHospital(): boolean;
            // public $onInit(): void;
            ///// /* @ngInject */
            function CaseActivityChartController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, CaseActivityService, moment, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                _this.HospitalService = HospitalService;
                _this.CaseActivityService = CaseActivityService;
                _this.$rootScope = $rootScope;
                _this.PatientService = PatientListFactory;
                _this.title = 'Case Activity Chart';
                _this.hospitalPracticeLabel = 'Practices';
                // this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.monthsToChart = '3';
                _this.chartHeight = DashboardService.chartHeight;
                return _this;
            }
            CaseActivityChartController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                console.log(this.user);
                // handle surgeon or practice staff
                console.log(this.surgeons);
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            CaseActivityChartController.prototype.initChartParams = function () {
                console.log('Initializing chart');
                // this.initChartLookups();
                this.caseActivityChart = new orthosensor.domains.ChartType();
                // let data: orthosensor.domains.ChartDataElement[];
                var service = this.ChartService;
                this.caseActivityChart = service.chart;
                // this.caseActivityChart.options.title = 'Case Activity Chart';
                this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Case Activity Chart';
                this.ChartService.xAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Procedure Count';
                if (!this.isDashboard) {
                    // this.ChartService.chartHeight = 480;
                }
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            CaseActivityChartController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('hospital Account');
                        this.setSurgeonPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            CaseActivityChartController.prototype.setSurgeonPartner = function () {
                console.log(this.surgeons);
                console.log(this.practice.id);
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getData();
                            break;
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getData();
                    }
                }
            };
            CaseActivityChartController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.getPracticeData = function () {
                console.log(this.practice);
                console.log(this.surgeons);
                if (this.practice) {
                    this.surgeon = this.surgeons[0];
                }
                return [];
            };
            CaseActivityChartController.prototype.getData = function () {
                var data = [];
                var averageData = this.getAverageData();
                console.log(averageData);
                var averageChartData;
                averageChartData = {
                    "key": ["Average"],
                    "color": "#b1b9bd",
                    "values": averageData
                };
                data.push(averageChartData);
                if (this.user.userProfile !== 1 /* HospitalAdmin */ && this.user.userProfile !== 3 /* PracticeAdmin */) {
                    var surgeonChartData = void 0;
                    var surgeonData = this.getSurgeonData();
                    console.log(surgeonData);
                    surgeonChartData = {
                        "key": [this.surgeon.name],
                        "color": "#109bd6",
                        "values": surgeonData
                    };
                    data.push(surgeonChartData);
                }
                console.log(data);
                this.data = data;
                return data;
            };
            CaseActivityChartController.prototype.getAverages = function () {
                // console.log(this.practice);
                return this.getPracticeData();
            };
            CaseActivityChartController.prototype.getChart = function () {
                this.setDateParams();
                this.getData();
            };
            CaseActivityChartController.prototype.getSurgeonData = function () {
                var _this = this;
                if (this.surgeon === null) {
                    if (this.surgeons.length > 0) {
                        this.surgeon = this.surgeons[0];
                    }
                }
                console.log(this.surgeon);
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                this.PatientService.getCaseActivityBySurgeon(this.surgeon.id, this.startDate, this.endDate)
                    .then(function (data) {
                    // console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            CaseActivityChartController.prototype.getAverageData = function () {
                console.log(this.practice);
                var items = [];
                if (this.UserService.user.userProfile === 1 /* HospitalAdmin */) {
                    items = null;
                }
                else {
                    items = this.CaseActivityService.getActivityByPractice(this.practice.id, this.startDate, this.endDate);
                }
                return items;
            };
            return CaseActivityChartController;
        }(dashboard.DashboardChartBaseController));
        CaseActivityChartController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'CaseActivityService', 'moment', 'config', '$rootScope'];
        var CaseActivityChart = (function () {
            function CaseActivityChart() {
                this.bindings = {};
                this.controller = CaseActivityChartController;
                this.templateUrl = 'app/dashboard/caseActivityChart/caseActivityChart.component.html';
            }
            return CaseActivityChart;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osCaseActivityChart', new CaseActivityChart());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseActivityChart.component.js.map