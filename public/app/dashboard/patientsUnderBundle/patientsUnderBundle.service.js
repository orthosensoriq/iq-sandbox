var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        'use strict';
        var PatientsUnderBundleService = (function () {
            function PatientsUnderBundleService(DashboardDataService, UserService) {
                this.DashboardDataService = DashboardDataService;
            }
            PatientsUnderBundleService.prototype.getPatientsUnderBundle = function (practiceId) {
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then(function (data) {
                    // console.log(data);
                    return data;
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return PatientsUnderBundleService;
        }());
        PatientsUnderBundleService.inject = ['DashboardDataService', 'UserService'];
        services.PatientsUnderBundleService = PatientsUnderBundleService;
        angular
            .module('orthosensor.services')
            .service('PatientsUnderBundleService', PatientsUnderBundleService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsUnderBundle.service.js.map