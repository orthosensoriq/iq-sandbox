module orthosensor.dashboard {

    class PatientsUnderBundleController extends DashboardBaseController{
        public static $inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', 'HospitalService', '$state', '$timeout', '$rootScope'];
        public title: string;
        public patients: any[];
        public DashboardDataService: any;
        public PatientsUnderBundleService: any;
        
        public $state: any;

        constructor(public DashboardService: any, DashboardDataService: any, PatientsUnderBundleService: any, public PatientService: orthosensor.services.PatientService, public UserService: orthosensor.services.UserService, HospitalService: orthosensor.services.HospitalService, $state: ng.ui.IStateService, public $timeout: ng.ITimeoutService, $rootScope: ng.IRootScopeService) {
            super(UserService, HospitalService, $rootScope);
            this.DashboardDataService = DashboardDataService;
            
            this.PatientsUnderBundleService = PatientsUnderBundleService;
            this.$state = $state;
          this.title = 'Patients Under Bundle';
        }

        public $onInit(): void {
            super.$onInit();
            let practiceId = this.UserService.user.accountId;
            // console.log(practiceId);
            //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            
            if (!this.isUserAdmin) {
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then((data: any) => {
                        this.patients = data;
                        // console.log(this.patients);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
        }

        public patientDetails(patient: any): void {
            //console.log(patient);
            this.PatientService.setPatientId(patient.patientId);
            this.$timeout(() => {
                this.$state.go('patientDetails');
            }, 1000);    
        }
    }

    class PatientsUnderBundle implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                practiceId: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = PatientsUnderBundleController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/patientsUnderBundle/patientsUnderBundle.component.html';
        }

    }
    angular
        .module('orthosensor.dashboard')
        .component('osPatientsUnderBundle', new PatientsUnderBundle());
}