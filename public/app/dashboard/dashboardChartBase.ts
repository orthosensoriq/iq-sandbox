module orthosensor.dashboard {

    export abstract class DashboardChartBaseController extends DashboardBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];

        protected PatientService: any;
        
        public surgeon: orthosensor.domains.Surgeon;

        // public isUserAdmin: boolean;
        public monthsToChart: string;
        public monthDiff: number;
        public startDate: any;
        public endDate: any;
        public dateFormat: string = 'MM/DD/YYYY';
        public data: any[];
        public chartData: any[];
        public surgeonSelectClass: string;
        public practiceSelectClass: string;

        constructor(public DashboardService: orthosensor.services.DashboardService,
            ChartService: any, PatientListFactory: any,
            public UserService: orthosensor.services.UserService,
            public HospitalService: orthosensor.services.HospitalService,
            moment: any, config: any,
            public $rootScope: ng.IRootScopeService) {
            super(UserService, HospitalService, $rootScope) ;
            
            this.PatientService = PatientListFactory;
        }
        $onInit(): void {
            super.$onInit();
            
            this.monthsToChart = '3';
            console.log(this.surgeons);
            if (this.surgeons && this.surgeons.length) {
                //add logic to get surgeon that is logged in if its a surgeon account
                this.surgeon = this.surgeons[0];
            }

            this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
            // console.log(this.surgeonSelectClass);
            this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
        }

        setDateParams(): void {
            let today: Date = moment();
            // console.log(today);
            let endDate: Date = moment(today).add((Number(this.monthsToChart) + 1), 'months');
            //  console.log(endDate);
            let sEndDate: string = moment(endDate).format(this.dateFormat);
            // console.log(sEndDate);
            let startMonth: Date = moment(today).subtract((Number(this.monthsToChart) - 1), 'months');
            let startDate: Date = moment(startMonth).startOf('month');
            this.monthDiff = moment(endDate).diff(startDate, 'months');
            // console.log(this.monthDiff);
            this.startDate = moment(startDate).format(this.dateFormat);
            this.endDate = moment(endDate).format(this.dateFormat);
            // console.log(this.startDate);
        }

        abstract initChartParams(): void;
        abstract initData(): void;
        abstract getPracticeData(): any[];
        abstract getSurgeonData(): any[]
       
        getSurgeonPartnerSurgeonSelectClass(): string {
            if (this.user.userProfile === orthosensor.domains.UserProfile.Surgeon) {
                return 'col-md-4 col-sm-6 col-xs-10';
            } else {
                return 'col-md-2 col-sm-3 col-xs-6';
            }
        }

        getHospitalPartnerPracticeSelectClass(): string {
            if (this.user.userProfile === orthosensor.domains.UserProfile.HospitalAdmin) {
                return 'col-md-4 col-sm-6 col-xs-10';
            } else {
                return 'col-md-2 col-sm-3 col-xs-6';
            }
        }

        public setHospitalPartner() {
            // console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticeData();
                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            // this.data = this.getPracticeKneeBalanceAvg();
                            this.getPracticeData(); // for testing!!
                        }
                    }
                }
            }
        }
        protected formatDate(data: any): string {
            // console.log(data);
            return this.DashboardService.formatDate(data);
        }
        ascii(a: number) { return String.fromCharCode(a); }
    }
}
