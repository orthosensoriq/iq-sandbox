module orthosensor.services {

    export class KneeBalanceService {
        static inject: Array<string> = ['DashboardDataService'];
        private _startMonth: number;
        private _startYear: number;
        private _endMonth: number;
        private _endYear: number;
        
        private _practiceId: string;
        set practiceId(_practiceId: string) {
            this._practiceId = _practiceId;
        }
        private _kneePatients: orthosensor.domains.KneeBalanceLoads[];
        get kneePatients(): orthosensor.domains.KneeBalanceLoads[] {
            console.log(this._kneePatients);
            return this._kneePatients;
        }
        set startMonth(_startMonth: number) {
            this._startMonth = _startMonth;
        }
        set startYear(_startYear: number) {
            this._startYear = _startYear;
        }
        set endMonth(_endMonth: number) {
            this._endMonth = _endMonth;
        }
        set endYear(_endYear: number) {
            this._endYear = _endYear;
        }
       
        constructor(public DashboardDataService: orthosensor.services.DashboardDataService) {
            // this._patient = new orthosensor.domains.Patient();
       
        }
       
        setKneeBalanceByPatient(): Promise<any> {
            return this.DashboardDataService.GetPracticeSensorData(this._practiceId,
                this._startMonth, this._startYear, this._endMonth, this._endYear)
                .then((data: any) => {
                    let result: any[] = data;
                    console.log(result);
                    this.setToPatientKneeObjects(result);
         
                }, (error: any) => {
                    console.log(error);
                });
        }

        getSFObject(): orthosensor.domains.sfPatient {

            let sfPatient: orthosensor.domains.sfPatient = new orthosensor.domains.sfPatient();
            // sfPatient.Id = this._patient.id;
            // sfPatient.Anonymous__c = this._patient.anonymous;
            // sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
            // sfPatient.Hl7__c = this._patient.hl7;
            // sfPatient.HospitalId__c = this._patient.hospitalId;
            // sfPatient.hospital = this._patient.hospital;
            // sfPatient.Last_Name__c = this._patient.lastName;
            // sfPatient.First_Name__c = this._patient.firstName;
            // sfPatient.practice = this._patient.practice;
            // // sfPatient.Anonymous_Label__c = this._patient.label;
            // sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
            // sfPatient.Email__c = this._patient.email;
            // sfPatient.Date_Of_Birth__c = this._patient.dateOfBirth;
            // sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
            // sfPatient.Gender__c = this._patient.gender;
            // sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
            // sfPatient.Source_Record_Id__c = this._patient.patientNumber;
            // sfPatient.Account_Number__c = this._patient.accountNumber;
            // sfPatient.Race__c = this._patient.race;
            // sfPatient.Language__c = this._patient.language;

            return sfPatient;
        }
        setToPatientKneeObjects(result: any[]): orthosensor.domains.KneeBalanceLoads[] {
            this._kneePatients = [];
            let patientId = '';
            console.log(result);
            for (let i = 0; i < result.length; i++) {
                patientId = result[i].PatientID__c;
                let found: boolean = false;
                for (let j = 0; j < this._kneePatients.length; j++) {
                    if (patientId === this._kneePatients[j].patientId) {
                        found = true;
                        this.mapFields(j, result[i]);
                    }
                }
                if (!found) {
                    console.log('new patient');
                    this.mapFields(-1, result[i]);
                }

            }
            console.log(this._kneePatients);
            return this._kneePatients;
        }

        mapFields(i: number, result: any) {
            let record: orthosensor.domains.KneeBalanceLoads;
            if (i === -1) {
                record = new orthosensor.domains.KneeBalanceLoads();
            } else {
                record = this._kneePatients[i];
            }
            console.log(record);
            if (i === -1) {
                record.id = result.Id;
                record.patientId = result.PatientID__c;
                record.patientName = result.PatientName__c;
                record.practiceId = result.Practice_ID__c;
                record.surgeonId = result.Surgeon_ID__c;
                record.surgeonName = result.Surgeon_Name__c;
                record.procedureDate = result.ProcedureDate__c;
            }
            switch (result.ActualFlex__c) {
                case 10:
                    record.medial10 = result.MedialLoad__c;
                    record.lateral10 = result.LateralLoad__c;
                    break;
                case 45:
                    record.medial45 = result.MedialLoad__c;
                    record.lateral45 = result.LateralLoad__c;
                    break;
                case 90:
                    record.medial90 = result.MedialLoad__c;
                    record.lateral90 = result.LateralLoad__c;
                    break;
            }
            if (i === -1) {
                this._kneePatients.push(record);
            }
            console.log(record);
            console.log(this._kneePatients);
        }
    }

    angular
        .module('orthosensor.services')
        .service('KneeBalanceService', KneeBalanceService);

}
