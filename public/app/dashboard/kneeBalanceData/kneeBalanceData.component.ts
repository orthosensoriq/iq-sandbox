module orthosensor.dashboard {

    class KneeBalanceDataController extends DashboardChartBaseController {

        public static $inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        public title: string;
        public user: orthosensor.domains.User;
        public hospitalPracticeLabel: string;
        public surgeonLabel: string;
        public chartService: orthosensor.services.ChartService;
        public kneeBalanceDataChart: orthosensor.domains.ChartType;
        // public surgeons: orthosensor.domains.Surgeon[];
        public chart: orthosensor.domains.ChartType;
        // public surgeon: orthosensor.domains.Surgeon;
        public hospital: orthosensor.domains.Hospital;
        public practices: orthosensor.domains.Practice[];
        public practice: orthosensor.domains.Practice;
        public options: any;
        public data: any[];
        public monthsToChart: string;
        public chartHeight: number;
        public startMonth: number;
        public startYear: number;
        public chartData: any[];
        public kneeBalanceData: orthosensor.domains.KneeBalanceAvg[];
                
        public allData: orthosensor.domains.KneeBalanceAvg[];
        public chartType: string;
        // private PatientService: any;
        
        public dateFormat: string = 'MM/DD/YYYY';

        ///// /* @ngInject */
        // tslint:disable-next-line:max-line-length
        constructor(public DashboardService: any, public ChartService: any, PatientListFactory: any, UserService: any, HospitalService: orthosensor.services.HospitalService, moment: any, config: any, $rootScope: ng.IRootScopeService) {
            super(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope);
            // this.PatientService = PatientListFactory;
            this.title = 'Medial & Lateral Load Values (10' + this.ascii(176) + ', 45' + this.ascii(176) + ', 90' + this.ascii(176) + ')';

            this.hospitals = [];
            this.hospitalPracticeLabel = 'Practices';
            this.practices = [];
            this.practice = new orthosensor.domains.Practice();
            this.surgeonLabel = 'Surgeon';
            this.surgeons = [];
            this.chartHeight = DashboardService.chartHeight;
        }

        public $onInit(): void {
            super.$onInit();
            this.kneeBalanceData = [];
            this.setDateParams();
            this.initChartParams();
            this.initData();

        };
        
        public initChartParams(): void {
            this.initChartLookups();
            this.kneeBalanceDataChart = new orthosensor.domains.ChartType();
            let service = this.ChartService;
            this.kneeBalanceDataChart = service.chart;
            // this.options = this.ChartService.getMultiBarChartOptions();
            this.ChartService.chartTitle = 'Knee Balance Data';
            this.ChartService.yAxisLabel = 'Months';
            this.ChartService.xAxisLabel = 'Medial & Lateral Load Values';
            this.options = this.ChartService.getMultiBarChartOptions();

        }

        // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
        public initData() {

            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;

                case orthosensor.domains.UserProfile.PracticeAdmin:
                    // console.log('surgeon Account');
                    this.setSurgeonPartner();
                    break;
                // case 'Customer Community User':
                //     // console.log('surgeon Account');
                //     this.setSurgeonPartner();
                //     break;
                // case 'Customer Community Plus User':
                //     // console.log('surgeon Account');
                //     this.setSurgeonPartner();
                //     break;

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('hospital Account');
                    this.setHospitalPartner();
                    break;

                default:
                    // console.log('Admin Account');
                    this.setAdmin();
            }
        }

        public setSurgeonPartner() {
            if (this.surgeons !== undefined) {
                for (let i = 0; i < this.surgeons.length; i++) {
                    if (this.surgeons[i].parentId === this.practice.id) {
                        this.surgeon = this.surgeons[i];
                        this.data = this.getSurgeonKneeBalanceAvg();
                        break;
                    }
                }
            }
        }

        public setHospitalPartner() {
            // console.log(this.practices);
            if (this.practices !== undefined) {
                if (this.practices.length > 0) {
                    this.practice = this.practices[0];
                    this.data = this.getPracticeKneeBalanceAvg();

                }
            }
        }

        public setAdmin() {
            if (this.hospitals !== undefined) {
                if (this.hospitals.length > 0) {
                    this.hospital = this.hospitals[0];
                    if (this.practices !== undefined) {
                        if (this.practices.length > 0) {
                            this.practice = this.practices[0];
                            // this.data = this.getPracticeKneeBalanceAvg();
                            this.getSurgeonKneeBalanceAvg(); // for testing!!
                        }
                    }
                }
            }
        }


        // called from page
        public getChart(surgeon: any): any[] {
            switch (this.user.userProfile) {
                case orthosensor.domains.UserProfile.Surgeon:
                    return this.getSurgeonData();

                case orthosensor.domains.UserProfile.HospitalAdmin:
                    // console.log('trying to change the chart');    
                    return this.getPracticeData();

                default:
                    return this.getSurgeonData();
            }
        }

        public getSurgeonData(): any[] {
            return this.getSurgeonKneeBalanceAvg();
        }

        public getPracticeData(): any[] {
            return this.getPracticeKneeBalanceAvg();
        }


        public createChartObject(key: string, color: string): {} {
            let getChartValuesForType: any[] = this.getChartValues(key);
            let chartObj = {
                'key': [key],
                'color': color,
                'values': getChartValuesForType
            }
            // console.log(chartObj);
            return chartObj;
        }

        public getChartValues(key: string): any[] {
            let values: any[] = [];
            // console.log(this.kneeBalanceData);
            if (this.kneeBalanceData.length > 0) {
                for (let i = 0; i <= this.kneeBalanceData.length; i++) {
                    // console.log(this.kneeBalanceData[i]);
                    if (this.kneeBalanceData[i]) {
                        switch (key) {
                            case "10-L":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].lateral10
                                });
                                break;
                            case "10-M":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].medial10
                                });
                                break;
                            case "45-L":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].lateral45
                                });
                                break;
                            case "45-M":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].medial45
                                });
                                break;
                            case "90-L":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].lateral90
                                });
                                break;
                            case "90-M":
                                values.push({
                                    "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                    "value": this.kneeBalanceData[i].medial90
                                });
                                break;

                        }
                    }
                }
            }
            // console.log(values);
            return values;
        }

        public getMonthLabel(date: Date): string {
            let dateLabel: string = this.formatDate(date);
            return dateLabel;
        }

        public getChartData(): any[] {

            // console.log(this.kneeBalanceData);
            this.chartData = [];
            
            // create chart objects for each laterality _ ange to arrays in fure 
            let lateral10 = this.createChartObject("10-L", "#787878");
            this.chartData.push(lateral10);
            let medial10 = this.createChartObject("10-M", "#b1b9bd");
            this.chartData.push(medial10);
            let lateral45 = this.createChartObject("45-L", "#0000FF");
            this.chartData.push(lateral45);
            let medial45 = this.createChartObject("45-M", "#109bd6");
            this.chartData.push(medial45);
            let lateral90 = this.createChartObject("90-L", "#5F9EA0");
            this.chartData.push(lateral90);
            let medial90 = this.createChartObject("90-M", "#82e49e");
            this.chartData.push(medial90);

            this.data = this.chartData;
            return this.chartData;
        }

        getPracticeKneeBalanceAvg(): orthosensor.domains.KneeBalanceAvg[] {
            let practice: string = this.practice.id;
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            // console.log(this.practice);
            this.PatientService.getMonthlyPracticeSensorAverages(this.practice.id, startMonth, startYear, endMonth, endYear)
                .then((data: any) => {
                    let result = data;
                    // console.log(result);
                    if (result !== null) {
                        this.kneeBalanceData = this.convertDataToObjects(result);
                        // console.log(this.kneeBalanceData);
                        this.getChartData();
                    }
                }, (error: any) => {
                    console.log(error);
                    return error;
                });
            return this.kneeBalanceData
        }

        getSurgeonKneeBalanceAvg(): orthosensor.domains.KneeBalanceAvg[] {
            // let surgeon: string = this.surgeon.id;
            // console.log(this.monthDiff);
            let startMonth = moment(this.startDate).month() + 1;
            let startYear = moment(this.startDate).year();
            //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
            let endMonth = moment(this.endDate).month() + 1;
            let endYear = moment(this.endDate).year();
            // console.log(this.surgeon);
            if (this.surgeon !== undefined) {
                console.log(this.surgeon);
                let surgeon = this.surgeon.id;
                this.PatientService.getMonthlySurgeonSensorAverages(surgeon, startMonth, startYear, endMonth, endYear)
                    .then((data: any) => {
                        let result = data;
                        console.log(result);
                        if (result !== null) {
                            this.kneeBalanceData = this.convertDataToObjects(result);
                            // console.log(this.kneeBalanceData);
                            this.getChartData();
                        }
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
            return this.kneeBalanceData

        }

        private initChartLookups(): void {
            // get hospitals, surgeon 
            // get hospitals
        }

        private validate(): boolean {
            return true;
        }

        private convertDataToObjects(data: any): orthosensor.domains.KneeBalanceAvg[] {
            let kneeBalance: orthosensor.domains.KneeBalanceAvg[] = [];
            // console.log(data);

            for (let i = 0; i <= data.length; i++) {
                // make sure a record exists!
                if (data[i] !== undefined && data[i].Month) {
                    let knee: orthosensor.domains.KneeBalanceAvg = this.convertSFKneeBalanceToObject(data[i]);
                    kneeBalance.push(knee);
                }
            }
            return kneeBalance;
        }

        // convert to object - dates come in last day of the prior month
        private convertSFKneeBalanceToObject(data: any): orthosensor.domains.KneeBalanceAvg {
            let kneeBalance: orthosensor.domains.KneeBalanceAvg = new orthosensor.domains.KneeBalanceAvg();
            // console.log(data);

            kneeBalance.startDate = moment(data.Year + '-' + data.Month + '-' + '02').toDate();
            // kneeBalance.startDateString = moment(kneeBalance.startDate).format(this.dateFormat);
            if (data.SurgeonId) {
                kneeBalance.surgeonId = data.SurgeonId;
            }
            if (data.PracticeId) {
                kneeBalance.practiceId = data.PracticeId;
            }
            kneeBalance.medial10 = data.Medial10;
            kneeBalance.medial45 = data.Medial45;
            kneeBalance.medial90 = data.Medial90;
            kneeBalance.lateral10 = data.Lateral10;
            kneeBalance.lateral45 = data.Lateral45;
            kneeBalance.lateral90 = data.Lateral90;
            // console.log(kneeBalance);
            return kneeBalance;
        }

        
    }

    angular
        .module('orthosensor.dashboard')
        .component('osKneeBalanceData', {
            controller: KneeBalanceDataController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/kneeBalanceData/kneeBalanceData.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
}

class KneeBalanceData {
    constructor(public name: string,
        public displayName: string,
        public color: string,
    ) {
    }
}
