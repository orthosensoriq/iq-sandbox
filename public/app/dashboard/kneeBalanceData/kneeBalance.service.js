var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var KneeBalanceService = (function () {
            function KneeBalanceService(DashboardDataService) {
                // this._patient = new orthosensor.domains.Patient();
                this.DashboardDataService = DashboardDataService;
            }
            Object.defineProperty(KneeBalanceService.prototype, "practiceId", {
                set: function (_practiceId) {
                    this._practiceId = _practiceId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "kneePatients", {
                get: function () {
                    console.log(this._kneePatients);
                    return this._kneePatients;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "startMonth", {
                set: function (_startMonth) {
                    this._startMonth = _startMonth;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "startYear", {
                set: function (_startYear) {
                    this._startYear = _startYear;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "endMonth", {
                set: function (_endMonth) {
                    this._endMonth = _endMonth;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "endYear", {
                set: function (_endYear) {
                    this._endYear = _endYear;
                },
                enumerable: true,
                configurable: true
            });
            KneeBalanceService.prototype.setKneeBalanceByPatient = function () {
                var _this = this;
                return this.DashboardDataService.GetPracticeSensorData(this._practiceId, this._startMonth, this._startYear, this._endMonth, this._endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    _this.setToPatientKneeObjects(result);
                }, function (error) {
                    console.log(error);
                });
            };
            KneeBalanceService.prototype.getSFObject = function () {
                var sfPatient = new orthosensor.domains.sfPatient();
                // sfPatient.Id = this._patient.id;
                // sfPatient.Anonymous__c = this._patient.anonymous;
                // sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
                // sfPatient.Hl7__c = this._patient.hl7;
                // sfPatient.HospitalId__c = this._patient.hospitalId;
                // sfPatient.hospital = this._patient.hospital;
                // sfPatient.Last_Name__c = this._patient.lastName;
                // sfPatient.First_Name__c = this._patient.firstName;
                // sfPatient.practice = this._patient.practice;
                // // sfPatient.Anonymous_Label__c = this._patient.label;
                // sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
                // sfPatient.Email__c = this._patient.email;
                // sfPatient.Date_Of_Birth__c = this._patient.dateOfBirth;
                // sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
                // sfPatient.Gender__c = this._patient.gender;
                // sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
                // sfPatient.Source_Record_Id__c = this._patient.patientNumber;
                // sfPatient.Account_Number__c = this._patient.accountNumber;
                // sfPatient.Race__c = this._patient.race;
                // sfPatient.Language__c = this._patient.language;
                return sfPatient;
            };
            KneeBalanceService.prototype.setToPatientKneeObjects = function (result) {
                this._kneePatients = [];
                var patientId = '';
                console.log(result);
                for (var i = 0; i < result.length; i++) {
                    patientId = result[i].PatientID__c;
                    var found = false;
                    for (var j = 0; j < this._kneePatients.length; j++) {
                        if (patientId === this._kneePatients[j].patientId) {
                            found = true;
                            this.mapFields(j, result[i]);
                        }
                    }
                    if (!found) {
                        console.log('new patient');
                        this.mapFields(-1, result[i]);
                    }
                }
                console.log(this._kneePatients);
                return this._kneePatients;
            };
            KneeBalanceService.prototype.mapFields = function (i, result) {
                var record;
                if (i === -1) {
                    record = new orthosensor.domains.KneeBalanceLoads();
                }
                else {
                    record = this._kneePatients[i];
                }
                console.log(record);
                if (i === -1) {
                    record.id = result.Id;
                    record.patientId = result.PatientID__c;
                    record.patientName = result.PatientName__c;
                    record.practiceId = result.Practice_ID__c;
                    record.surgeonId = result.Surgeon_ID__c;
                    record.surgeonName = result.Surgeon_Name__c;
                    record.procedureDate = result.ProcedureDate__c;
                }
                switch (result.ActualFlex__c) {
                    case 10:
                        record.medial10 = result.MedialLoad__c;
                        record.lateral10 = result.LateralLoad__c;
                        break;
                    case 45:
                        record.medial45 = result.MedialLoad__c;
                        record.lateral45 = result.LateralLoad__c;
                        break;
                    case 90:
                        record.medial90 = result.MedialLoad__c;
                        record.lateral90 = result.LateralLoad__c;
                        break;
                }
                if (i === -1) {
                    this._kneePatients.push(record);
                }
                console.log(record);
                console.log(this._kneePatients);
            };
            return KneeBalanceService;
        }());
        KneeBalanceService.inject = ['DashboardDataService'];
        services.KneeBalanceService = KneeBalanceService;
        angular
            .module('orthosensor.services')
            .service('KneeBalanceService', KneeBalanceService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=kneeBalance.service.js.map