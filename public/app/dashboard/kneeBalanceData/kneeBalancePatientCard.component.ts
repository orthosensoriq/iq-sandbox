module orthosensor {

    class KneeBalancePatientCardController {
        public static $inject = ['PatientService', '$timeout', '$state'];
        // public patient: {
        //     id: string;
        //     name: string;

        //     kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        // };
        public tenDegree: string;
        public fortyFiveDegree: string;
        public ninetyDegree: string;
        constructor(public PatientService: orthosensor.services.PatientService, public $timeout: any, public $state: any) { }

        $onInit() {
            this.tenDegree = '10' + this.ascii(176);
            this.fortyFiveDegree = '45' + this.ascii(176);
            this.ninetyDegree = '90' + this.ascii(176);

        }
        ascii(a: number) { return String.fromCharCode(a); }

        public patientDetails(patient: any): void {
            console.log(patient);
            this.PatientService.setPatientId(patient.patientId);
            this.$timeout(() => {
                this.$state.go('patientDetails');
            }, 1000);
        }
    }

    class KneeBalancePatientCard implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                patient: '<'
            };
            this.controller = KneeBalancePatientCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/kneeBalanceData/kneeBalancePatientCard.component.html';
        }
    }
    angular
        .module('orthosensor')
        .component('osKneeBalancePatientCard', new KneeBalancePatientCard());
}