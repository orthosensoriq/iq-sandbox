var orthosensor;
(function (orthosensor) {
    var KneeBalancePatientCardController = (function () {
        function KneeBalancePatientCardController(PatientService, $timeout, $state) {
            this.PatientService = PatientService;
            this.$timeout = $timeout;
            this.$state = $state;
        }
        KneeBalancePatientCardController.prototype.$onInit = function () {
            this.tenDegree = '10' + this.ascii(176);
            this.fortyFiveDegree = '45' + this.ascii(176);
            this.ninetyDegree = '90' + this.ascii(176);
        };
        KneeBalancePatientCardController.prototype.ascii = function (a) { return String.fromCharCode(a); };
        KneeBalancePatientCardController.prototype.patientDetails = function (patient) {
            var _this = this;
            console.log(patient);
            this.PatientService.setPatientId(patient.patientId);
            this.$timeout(function () {
                _this.$state.go('patientDetails');
            }, 1000);
        };
        return KneeBalancePatientCardController;
    }());
    KneeBalancePatientCardController.$inject = ['PatientService', '$timeout', '$state'];
    var KneeBalancePatientCard = (function () {
        function KneeBalancePatientCard() {
            this.bindings = {
                patient: '<'
            };
            this.controller = KneeBalancePatientCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/kneeBalanceData/kneeBalancePatientCard.component.html';
        }
        return KneeBalancePatientCard;
    }());
    angular
        .module('orthosensor')
        .component('osKneeBalancePatientCard', new KneeBalancePatientCard());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=kneeBalancePatientCard.component.js.map