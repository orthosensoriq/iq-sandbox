module orthosensor {
    class KneeBalancePatientCardsController {
        public static $inject = ['DashboardDataService', 'KneeBalanceService', 'UserService'];
        public patients: orthosensor.domains.KneeBalanceLoads[];
        public patient: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };
        public patient1: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };

        public patient3: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };

        public patient4: {
            id: string;
            name: string;
            procedureDate: Date;
            kneeBalanceData: orthosensor.domains.KneeBalanceLoads
        };

        constructor(
            public DashboardDataService: orthosensor.services.DashboardDataService,
            public KneeBalanceService: orthosensor.services.KneeBalanceService,
            public UserService: orthosensor.services.UserService) { }

        $onInit() {
            this.patients = [];
            let practiceId = this.UserService.user.accountId;

            this.KneeBalanceService.practiceId = practiceId;
            this.KneeBalanceService.startMonth = 1;
            this.KneeBalanceService.startYear = 2017;
            this.KneeBalanceService.endMonth = 4;
            this.KneeBalanceService.endYear = 2017;
            this.KneeBalanceService.setKneeBalanceByPatient()
                .then(() => {
                    this.patients = this.KneeBalanceService.kneePatients;
                    console.log(this.patients);
                });


            // this.patient = {
            //     name: 'Mildred Ginger',
            //     id: '123456',
            //     procedureDate: new Date(2017, 2, 24),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient.name = 'Mark Tweedy';
            // this.patient.kneeBalanceData.medial10 = 18;
            // this.patient.kneeBalanceData.medial45 = 16;
            // this.patient.kneeBalanceData.medial90 = 26;
            // this.patient.kneeBalanceData.lateral10 = 12;
            // this.patient.kneeBalanceData.lateral45 = 14;
            // this.patient.kneeBalanceData.lateral90 = 19;
            // this.patients.push(this.patient);
            // this.patient1 = {
            //     name: 'Jane Hart',
            //     id: '12345236',
            //     procedureDate: new Date(2017, 2, 4),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient1.kneeBalanceData.medial10 = 20;
            // this.patient1.kneeBalanceData.medial45 = 16;
            // this.patient1.kneeBalanceData.medial90 = 26;
            // this.patient1.kneeBalanceData.lateral10 = 12;
            // this.patient1.kneeBalanceData.lateral45 = 14;
            // this.patient1.kneeBalanceData.lateral90 = 19;
            // this.patients.push(this.patient1);
            // this.patient3 = {
            //     name: 'Martin Half',
            //     id: '12345343',
            //     procedureDate: new Date(2017, 3, 4),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient3.kneeBalanceData.medial10 = 18;
            // this.patient3.kneeBalanceData.medial45 = 18;
            // this.patient3.kneeBalanceData.medial90 = 22;
            // this.patient3.kneeBalanceData.lateral10 = 12;
            // this.patient3.kneeBalanceData.lateral45 = 16;
            // this.patient3.kneeBalanceData.lateral90 = 16;
            // this.patients.push(this.patient3);

            // this.patient4 = {
            //     name: 'Robert Richard',
            //     id: '123987',
            //     procedureDate: new Date(2017, 2, 13),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient4.kneeBalanceData.medial10 = 4;
            // this.patient4.kneeBalanceData.medial45 = 12;
            // this.patient4.kneeBalanceData.medial90 = 20;
            // this.patient4.kneeBalanceData.lateral10 = 15;
            // this.patient4.kneeBalanceData.lateral45 = 16;
            // this.patient4.kneeBalanceData.lateral90 = 10;
            // this.patients.push(this.patient4);
        }


    }

    class KneeBalancePatientCards implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {

            };
            this.controller = KneeBalancePatientCardsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/kneeBalanceData/kneeBalancePatientCards.component.html';
        }

    }
    angular
        .module('orthosensor')
        .component('osKneeBalancePatientCards', new KneeBalancePatientCards());
}