var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var KneeBalanceDataController = (function (_super) {
            __extends(KneeBalanceDataController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function KneeBalanceDataController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'Medial & Lateral Load Values (10' + _this.ascii(176) + ', 45' + _this.ascii(176) + ', 90' + _this.ascii(176) + ')';
                _this.hospitals = [];
                _this.hospitalPracticeLabel = 'Practices';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                return _this;
            }
            KneeBalanceDataController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.kneeBalanceData = [];
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            KneeBalanceDataController.prototype.initChartParams = function () {
                this.initChartLookups();
                this.kneeBalanceDataChart = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.kneeBalanceDataChart = service.chart;
                // this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Knee Balance Data';
                this.ChartService.yAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Medial & Lateral Load Values';
                this.options = this.ChartService.getMultiBarChartOptions();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            KneeBalanceDataController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            KneeBalanceDataController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonKneeBalanceAvg();
                            break;
                        }
                    }
                }
            };
            KneeBalanceDataController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeKneeBalanceAvg();
                    }
                }
            };
            KneeBalanceDataController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getSurgeonKneeBalanceAvg(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            KneeBalanceDataController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        // console.log('trying to change the chart');    
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            KneeBalanceDataController.prototype.getSurgeonData = function () {
                return this.getSurgeonKneeBalanceAvg();
            };
            KneeBalanceDataController.prototype.getPracticeData = function () {
                return this.getPracticeKneeBalanceAvg();
            };
            KneeBalanceDataController.prototype.createChartObject = function (key, color) {
                var getChartValuesForType = this.getChartValues(key);
                var chartObj = {
                    'key': [key],
                    'color': color,
                    'values': getChartValuesForType
                };
                // console.log(chartObj);
                return chartObj;
            };
            KneeBalanceDataController.prototype.getChartValues = function (key) {
                var values = [];
                // console.log(this.kneeBalanceData);
                if (this.kneeBalanceData.length > 0) {
                    for (var i = 0; i <= this.kneeBalanceData.length; i++) {
                        // console.log(this.kneeBalanceData[i]);
                        if (this.kneeBalanceData[i]) {
                            switch (key) {
                                case "10-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral10
                                    });
                                    break;
                                case "10-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial10
                                    });
                                    break;
                                case "45-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral45
                                    });
                                    break;
                                case "45-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial45
                                    });
                                    break;
                                case "90-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral90
                                    });
                                    break;
                                case "90-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial90
                                    });
                                    break;
                            }
                        }
                    }
                }
                // console.log(values);
                return values;
            };
            KneeBalanceDataController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            KneeBalanceDataController.prototype.getChartData = function () {
                // console.log(this.kneeBalanceData);
                this.chartData = [];
                // create chart objects for each laterality _ ange to arrays in fure 
                var lateral10 = this.createChartObject("10-L", "#787878");
                this.chartData.push(lateral10);
                var medial10 = this.createChartObject("10-M", "#b1b9bd");
                this.chartData.push(medial10);
                var lateral45 = this.createChartObject("45-L", "#0000FF");
                this.chartData.push(lateral45);
                var medial45 = this.createChartObject("45-M", "#109bd6");
                this.chartData.push(medial45);
                var lateral90 = this.createChartObject("90-L", "#5F9EA0");
                this.chartData.push(lateral90);
                var medial90 = this.createChartObject("90-M", "#82e49e");
                this.chartData.push(medial90);
                this.data = this.chartData;
                return this.chartData;
            };
            KneeBalanceDataController.prototype.getPracticeKneeBalanceAvg = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.PatientService.getMonthlyPracticeSensorAverages(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.kneeBalanceData = _this.convertDataToObjects(result);
                        // console.log(this.kneeBalanceData);
                        _this.getChartData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.kneeBalanceData;
            };
            KneeBalanceDataController.prototype.getSurgeonKneeBalanceAvg = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                // console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    console.log(this.surgeon);
                    var surgeon = this.surgeon.id;
                    this.PatientService.getMonthlySurgeonSensorAverages(surgeon, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.kneeBalanceData = _this.convertDataToObjects(result);
                            // console.log(this.kneeBalanceData);
                            _this.getChartData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.kneeBalanceData;
            };
            KneeBalanceDataController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            KneeBalanceDataController.prototype.validate = function () {
                return true;
            };
            KneeBalanceDataController.prototype.convertDataToObjects = function (data) {
                var kneeBalance = [];
                // console.log(data);
                for (var i = 0; i <= data.length; i++) {
                    // make sure a record exists!
                    if (data[i] !== undefined && data[i].Month) {
                        var knee = this.convertSFKneeBalanceToObject(data[i]);
                        kneeBalance.push(knee);
                    }
                }
                return kneeBalance;
            };
            // convert to object - dates come in last day of the prior month
            KneeBalanceDataController.prototype.convertSFKneeBalanceToObject = function (data) {
                var kneeBalance = new orthosensor.domains.KneeBalanceAvg();
                // console.log(data);
                kneeBalance.startDate = moment(data.Year + '-' + data.Month + '-' + '02').toDate();
                // kneeBalance.startDateString = moment(kneeBalance.startDate).format(this.dateFormat);
                if (data.SurgeonId) {
                    kneeBalance.surgeonId = data.SurgeonId;
                }
                if (data.PracticeId) {
                    kneeBalance.practiceId = data.PracticeId;
                }
                kneeBalance.medial10 = data.Medial10;
                kneeBalance.medial45 = data.Medial45;
                kneeBalance.medial90 = data.Medial90;
                kneeBalance.lateral10 = data.Lateral10;
                kneeBalance.lateral45 = data.Lateral45;
                kneeBalance.lateral90 = data.Lateral90;
                // console.log(kneeBalance);
                return kneeBalance;
            };
            return KneeBalanceDataController;
        }(dashboard.DashboardChartBaseController));
        KneeBalanceDataController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        angular
            .module('orthosensor.dashboard')
            .component('osKneeBalanceData', {
            controller: KneeBalanceDataController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/kneeBalanceData/kneeBalanceData.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
var KneeBalanceData = (function () {
    function KneeBalanceData(name, displayName, color) {
        this.name = name;
        this.displayName = displayName;
        this.color = color;
    }
    return KneeBalanceData;
}());
//# sourceMappingURL=kneeBalanceData.component.js.map