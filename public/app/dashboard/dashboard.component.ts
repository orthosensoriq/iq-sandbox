module orthosensor.dashboard {

    class DashboardController {
        public static $inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state', '$rootScope'];
        public title: string;
        public patientsUnderBundleTitle: string;
        public patients: any[];
        public patientsUnderBundleCount: number;
        public patientsScheduled: number;
        public isAdmin: Boolean;
        public logo: any;
        public standardComponentSize: string;
        public standardDashboardWidgetSize: string;
        public user: orthosensor.domains.User;
        public systemMode: string;

        constructor(public DashboardService: any,
            public DashboardDataService: any,
            public PatientsUnderBundleService: any,
            public PatientService: any,
            public UserService: orthosensor.services.UserService,
            public $state: any,
            public $rootScope: ng.IRootScopeService) {
       
            this.patientsUnderBundleTitle = 'Patients Under Bundle';
           
            // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
        }
        public $onInit(): void {
            let practiceId: string = '0017A00000M803ZQAR';
            this.user = this.UserService.user;
            this.isAdmin = this.UserService.isAdmin;
            // console.log(this.isAdmin);
            this.standardComponentSize = 'col-sm-6 col-md-4 col-xs-12 dashboardComponentHeight';
            this.standardDashboardWidgetSize = 'col-sm-4 col-xs-12';
            if (!this.isAdmin) {
                practiceId = this.UserService.user.accountId;
                if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                    this.systemMode = 'production';
                } else {
                    this.systemMode = this.$rootScope.systemMode;
                }
                console.log(this.$rootScope);
                this.DashboardDataService.getScheduledPatients(practiceId, 7)
                    .then((data: any) => {
                        if (data !== null) {
                            this.patientsScheduled = data.length;
                        } else {
                            this.patientsScheduled = 0;
                        }
                        // console.log(this.patients);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then((data: any) => {
                        if (data !== null) {
                            this.patientsUnderBundleCount = data.length;
                        } else {
                            this.patientsUnderBundleCount = 0;
                        }
                        // console.log(this.patientsUnderBundleCount);
                    }, (error: any) => {
                        console.log(error);
                        return error;
                    });
            }
        }
    }
    class Dashboard implements ng.IComponentOptions {
        public bindings: any;
        public controller: any;
        public controllerAs: any;
        public templateUrl: string;

        constructor() {
            this.bindings = {
                practiceId: '@',
                // dataBinding: '<',
                // functionBinding: '&'
            };
            this.controller = DashboardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/dashboard.component.html';
        }

    }
    angular
        .module('orthosensor.dashboard')
        .component('osDashboard', new Dashboard());
}