# Orthosensor IQ Notes

## Sprint 32
* added email field to patients

## Bugs
* On Patient List in Scheduled Patient, if you click on a patient, it brings up a patient form labeled New Patient. 
    -   not a bug: since patients on this list have not had a procedure done and are only scheduled, 
        the system still shows them as new patients.
        * LAterality is not being stored in Sensor_Data object

## Fixes
* Update patient info on case detail form was not working

## Design Issues
* Buttons don't show arrow on text
* Buttons aren't easily distinguished - for example Procedure on patient-details has a 
procedure button and when you click it, 
the next page also has a Procedure button but its not a button.
* Filter function drop down on Paitent List doesn't go away after selection
* Functionality is different for patient list action items. Surgeon is a different type of selection * Patient Detail back button shoould have tooltip
* Patient Details when made visible icon doesn't change - should show up arrow
* Dates are full date instead of mm/mm/yyyyy - takes space 
* sort by date?
* should notes have date/time stamps?
* for implant and sensors, should we hide the row title until a record is entered?
* menu is not visible until you go back to the list page. Should we have a way to show the menu all the time?
* on Case details the label says the Event type but the buttons don't indicate which is current.


## Other
* Buttons need depth
* Should not be able to take a survey for dates in the future? 
* View Data / Chart button turns black - different from everything else
* Accidentally, added a survey for Bernard Aalburg - 7/29/16 - no data.
* Case wizard steps could be a component

## Technical issues / process
* Version control

    The Salesforce developer console is on salesforce so there is no local copy. To use Git we would 
    need a local client (Sublime Text, Eclipse / Force IDE , VS Code) so that code can be checked in,
    branched and tested.

* Angular versions

    The current version we are using is 1.49. Angular 1.x is currently
    at 1.59. Angular 1.5 introduced a new component feature. This
    is a simplified directive creation tool. A component/directive in angular makes it easier to create html-like declarations in the html markup 
    (e.g.<top-menu></top-menu>, <patient-list></patient-list>)
    increasing readability, modularization and code re-use.

    In addition, we should discuss migration to angular 2.0. 2.0 has been in development for some time and represents some performance improvements. Clearly, this will be 
    the focus for the angular development term in the near and long term.
    The larger our code base, the more time consuming it will be to migrate.

    Some concerns about Angular 2.0: 1) its different - angular 1 skills help greatly but
    there is definitely a learning curve, 2) while development can be accomplishedf in javascript, they recommend 
    using Typescript (as well as Dart or Babel). Typescript "transpiles" to javascriptbut does so in a 
    typed language enabling better development and compile time error checking aswell as providing a more structured 
    environment for larger projects.

* CSS and SASS

    SASS is commonly used as a tool to create CSS files. It provides some advantages 
    in that it allows variables and some other advantages to CSS creation
    especially if multiple CSS files are required.

* Task runners

    Tools like Gulp and Grunt have become fairly standard in javascript projects to 
    prepare code for deployment as well as perform tests, check code for compliance to best practice 
    and a variety of other functions. My preference is Gulp but we will need to have something 
    in place for security and performance.

* Package Manager

    The project uses Bower as a package manager for 3rd party libraries. Its OK 
    but NPM has become the preferred package manager in the last few years. 
    While there's little difference I did find that some of the 3rd party products we use 
    do not have a direct bower installation.

* Testing

    I don't see any unit tests on the angular code. We should create some standards and best practices for these 
    tests. 


## Tables and Objects
* Accounts
    * Practice Accounts
    * Hospital Accounts
* Users
* Case Types
* Product Catalog
* OS Device Ref
* Surgeon Hospital
* Patient
* ADT Interface (Hl7 patients)
* Event
* Event Type
* Implant Component
* OS_Device
* Notes
* Fam__Fixed Asset (Fam is the namespace, Fixed Asset is the table)
* Survey
* Sensor Data

## Classes
* ProcedureCreationController
* ProcedureSummaryController



## Questions
* Can a Practice Account belong to or be associated with a Hospital Account? 
* Why the blank space above the case in the patient list? 
* Why the gray background behind the pages?
* Why is the crrent user retrieved for each query request?
* Is there a way to link practices to hospitals

## Possible stories
* Refactor scripts and html into resource bundles in directory 
structure
    - template cache enables easier addressing in Salesforce and better performance
* Componentize pages for modularity and re-use
    - survey results (graphs and tables)
        - graph
        - data table
* Break up existing resource bundle to separate 3rd party tools from 
orthosensor code. We could also separate certain modules or folders into separate bundles.
for example, separate the dashboard module as a separate bundle so that the prototype 
doesn't conflict with the  
* Koos label and questions are hard coded in survey view. This could either be one or 
2 stories
* Internationalization - angular language internationalization is fairly easy to implement. 
Headings and field labels need to be soft coded and then translation json files need to be 
created for each language.
* user preferences for dashboard components. Eventually drag and drop but could do it fairly 
easily by just allowing users to select in a settings page which dashboard items to show. 

    
## Process
* use directory structure - (show my structure)
* use bitbucket repo
    - use notifications to sync when other developer(s) checks in
    - create branching for production and development

## Notes
* Should Cases have a required case type?
* Should an admin be able to assign different Surveys to an event Type? THe graph 
should only be able to display one survey type     
* Have incomplete items with a hollow check circle and the ones completed with a 
checkmark
* Koos is hard coded in survey view data should be soft coded
* is there a way to edit an existing case?
* System uses a logging technique to save variables between forms. It might be better 
to use angular services. (-- Need to see if there's another reason this is being done! --)
* Koos survey results are hard coded in Apex queries 
* patient info is stored in $rootscope (global) and should be stored in patient service

## Sprint 25 changes
* add field "Can Graph" as a checkbox field to Survey object
* added IQ_SurveyRepository as Apex class for additional survey functions

* Survey Response -> EventForm -> Event -> Patient

## Recommendations
* Break pages into a more modular approach using components or at least ng-include - allows easier 
re-use of functionality and testing 
* use controller as syntax - recommended and better for migration to newer angular versions
* use a directory structure to break about the modules - will be easier for mid-large project 
* break apart factory/service script into multiple scripts per function - easier to find code and tested
* use Typescript - better typing + angular 2 recommends it. Typescript is a superset of javascript
so initial migration is as simple as renaming files to a '.ts' extension. 
* 
# Anonymous Patients
* 3 fields om Patients: Anonymous, Anonymous_Label, Anonymous_Date_Of_Birth
* 1 field in Accounts: Anomous_Patients 

# Bar code
## Bar code for Sensor
### Bar Code composition 
* found in ProcedureSummaryController.cls - ParseSensorBoxBarcode()
* comma delimited string - 4 parts
* 0 - device ID
* 1 - first 3 characters - manufacturer - see below
* 1 - Style (after the hyphen)
* 2 - Lot Number
* 3 - Assembly Date (parsed by '-')
* 4 - Expiration Date (parsed by '-')
        `//Handles different date formats for the expiration date
                //YYYY-MM
                if(expDate.size()<3 && expDate[0].length() == 4) expirationdate = expDate[1] + '-01-' + expDate[0];
                //MM-YYYY
                else if(expDate.size()<3 && expDate[0].length() == 2) expirationdate = expDate[0] + '-01-' + expDate[1];
                //MM-DD-YYYY
                else if(expDate.size()==3 && expDate[2].length() == 4) expirationdate = expDate[0] + '-01-' + expDate[2];`
* YearAssmbled  = Assemble Date [2]

### Manufacturer bar code abbreviations
* `SYK = 'Stryker'`
* `BMT = 'Biomet'`
* `ZMR = 'Zimmer'` 
* `SNN = 'Smith and Nephew'` 

## Bar Code for Implants
* 
                
