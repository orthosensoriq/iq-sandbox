module.exports = function () {
    var client = './public/';
    var clientApp = client + 'app/';
    var temp = './tmp/';
    var resourceBundleRoot =  './resource-bundles/IQ_AppBundle.resource/';
    // var resourceBundleRoot = '../iq-uidev/resource-bundles/IQ_AppBundle.resource/';
    var config = {
        temp: temp,
        //all js
        alljs: [
            clientApp + '/**/*.js'
        ],
        build: './build/',
        client: client,
        clientApp: clientApp,
        css: client + 'css/',
        fonts: './bower_components/font-awesome/fonts/**/*.*',
        htmltemplates: clientApp + '**/*.html',
        images: './images/**/*.*',
        index: './index.html',
        js: [
            clientApp + '**/*.module.js',
            clientApp + '**/*.js',
            '!' + clientApp + '**/*.spec.js'
        ],
        sass: client + 'sass/**/*.scss',
        appScriptFile: 'scripts.js',
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'orthosensor',
                standAlone: false,
                root: 'app/'
            }
        },
        resourceBundleRoot: resourceBundleRoot,
        resourceBundle: {
            app: resourceBundleRoot + 'app',
            css: resourceBundleRoot + 'css',
            admin: resourceBundleRoot + 'admin',
            components: resourceBundleRoot + 'components',
            // thirdParty:  '../iq-uidev/resource-bundles/IQ_bundle_resources.resource'
            thirdParty: './resource-bundles/IQ_bundle_resources.resource'
        },
        thirdPartyResourceBundleRoot: '../iq-sandbox/resource-bundles/IQ_bundle_resources.resource',
        browserReloadDelay: 1000,
        bower: {
            json: require('./bower.json'),
            directory: './bower_components/',
            ignorePath: '../..'
        },
        optimized: {
            app: 'app.js',
            lib: 'lib.js'
        }
    };
    config.getWiredepDefaultOptions = function () {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };
    return config;
};
