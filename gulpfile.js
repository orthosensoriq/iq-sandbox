var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('./gulp.config')();
var del = require('del');
var uglify = require('gulp-uglify');
var ts = require("gulp-typescript");
var karma = require('karma');
var jshint = require('gulp-jshint');
var stripDebug = require('gulp-strip-debug');
var $ = require('gulp-load-plugins')({ lazy: true });
var tsProject = ts.createProject("tsconfig.json");
gulp.task('help', $.taskListing);
gulp.task('default', ['help']);
gulp.task('vet', function () {
    log('Analyzing source with JSHint and JSCS');
    return gulp
        .src(config.alljs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', { verbose: true }))
        .pipe($.jshint.reporter('fail'));
});
gulp.task('styles',  function () {
    log('Compiling Sass --> css');
    return gulp
        .src(config.sass)
        .pipe($.plumber())
        .pipe($.sass())
        .pipe($.autoprefixer({ browsers: ['last 2 version', '> 5%'] }))
        .pipe(gulp.dest(config.css));
});
gulp.task('fonts', ['clean-fonts'], function () {
    log('Building fonts');
    return gulp.src(config.fonts)
        .pipe(gulp.dest(config.build + 'fonts'));
});
gulp.task('compress', function () {
    var options = {
        preserveComments: 'license',
        mangle: true
    };
    return gulp.src('./app/**/*.js')
        .pipe($.uglify())
        .pipe(gulp.dest(config.build));
});
gulp.task('images', ['clean-images'], function () {
    log('Copying and Optimizing images');
    return gulp.
        src(config.images)
        .pipe($.imagemin({ optimizationlevel: 3 }))
        .pipe(gulp.dest(config.build + 'images'));
});
gulp.task('clean', function (done) {
    var delconfig = [].concat(config.build, config.temp);
    log('Cleaning: ' + $.util.colors.blue(delconfig));
    del(delconfig, done);
});
gulp.task('clean-fonts', function (done) {
    clean(config.build + 'fonts/**/*.*', done);
});
gulp.task('clean-images', function (done) {
    clean(config.build + 'images/**/*.*', done);
});
gulp.task('clean-styles', function (done) {
    clean(config.temp + '**/*.css', done);
    //clean(config.build + 'styles/*.css', done);
});
gulp.task('clean-code', function (done) {
    var files = [].concat(config.temp + '**/*.js', config.build + '**/*.html', config.build + 'js/**/*.js');
    clean(files, done);
});
gulp.task('sass-watcher', function () {
    gulp.watch([config.sass], ['styles']);
});
gulp.task("typescript", function () {
    return tsProject.src()
        .pipe(ts(tsProject))
        .js.pipe(gulp.dest('./'));
});
gulp.task('clean-resource-bundle', function () {
    return del([config.resourceBundle + '/**/*'], { force: true });
});
gulp.task("resource-bundle-js", ['clean-resource-bundle'], function() {
    log('Copying files to: ' + config.build + 'js/*.js');
    return gulp.src(config.build + 'js/*.js')
    .pipe(gulp.dest(config.resourceBundle.app));
});

gulp.task("resource-bundle-html", function () {
    return gulp.src('./public/app/**/*.html')
        .pipe(gulp.dest(config.resourceBundle.app));
});
gulp.task("resource-bundle-css", ['styles'], function () {
    return gulp.src(config.css + '/**/*.css')
        .pipe(gulp.dest(config.resourceBundle.css));
});
gulp.task("resource-bundle-fonts", function () {
    return gulp.src('./public/fonts/**/*.*')
        .pipe(gulp.dest(config.resourceBundleRoot + 'fonts'));
});
gulp.task("resource-bundle-images", function () {
    return gulp.src('./public/images/**/*.*')
        .pipe(gulp.dest(config.appResourceBundleRoot + 'images'));
});
gulp.task("resource-admin-bundle-js", function () {
    return gulp.src(['./public/app/admin/**/*.js'])
        .pipe(gulp.dest(config.resourceBundle.admin));
});
gulp.task("resource-components-bundle-js", function () {
    return gulp.src(['./public/app/components/**/*.js'])
        .pipe(gulp.dest(config.resourceBundle.components));
});

gulp.task("dev-resource-bundle", [
    'dev-optimize',
    'resource-bundle-js',
    'resource-bundle-css',
    'resource-bundle-fonts',
    'resource-bundle-images' //,
]);

gulp.task("prod-resource-bundle", [
    'prod-optimize',
    'resource-bundle-js',
    //'resource-bundle-html',
    'resource-bundle-css',
    'resource-bundle-fonts',
    'resource-bundle-images' //,
]);
// IQ_bundle_resources
gulp.task('thirdPartyClean-resource-bundle', function () {
    return del([config.thirdPartyResourceBundleRoot + '/**/*'], { force: true });
});
gulp.task("thirdPartyResource-bundle-bower", function () {
    return gulp.src(['./bower_components/**/*'])
        .pipe(gulp.dest(config.thirdPartyResourceBundleRoot + '/bower_components'));
});
gulp.task("thirdPartyResource-bundle-css", function () {
    return gulp.src('./public/css/**/*.css')
        .pipe(gulp.dest(config.thirdPartyResourceBundleRoot + '/css'));
});
gulp.task("thirdPartyResource-bundle-fonts", function () {
    return gulp.src('./public/fonts/**/*.*')
        .pipe(gulp.dest(config.thirdPartyResourceBundleRoot + '/fonts'));
});
gulp.task("thirdPartyResource-bundle-images", function () {
    return gulp.src('./public/images/**/*.*')
        .pipe(gulp.dest(config.thirdPartyResourceBundleRoot + '/images'));
});
gulp.task("thirdPartyResource-bundle", [
    'thirdPartyClean-resource-bundle',
    'thirdPartyResource-bundle-bower',
    'thirdPartyResource-bundle-css',
    'thirdPartyResource-bundle-fonts',
    'thirdPartyResource-bundle-images'
]);
gulp.task('templatecache', function () {
    log("Creating Angular template cache");
    return gulp
        .src(config.htmltemplates)
        .pipe($.minifyHtml({ empty: true }))
        .pipe($.angularTemplatecache(config.templateCache.file, config.templateCache.options))
        .pipe(gulp.dest(config.temp));
});
gulp.task('wiredep', function () {
    log("wire up the bower css, js and our app js into the html");
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    log('starting wiredep');
    log(config.index);
    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest('./'));
});
gulp.task('inject', ['styles','wiredep', 'templatecache'], function () {
    log("wire up the app css and call wiredep");
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.css)))
        .pipe(gulp.dest('./'));
});
gulp.task('scripts', function () {
    var options = {
        preserveComments: 'license',
        mangle: true
    };
    return gulp.src(config.js)
        .pipe($.concat(config.appScriptFile))
        .pipe(gulp.dest(config.build))
        .pipe($.notify({ message: 'Scripts task complete' }));
});
gulp.task('prod-optimize', ['styles','inject'], function () {
    log('Optimizing the javascript, css, html');
    //var assets = $.useref.assets({ searchPath: './' });
    var assets = $.useref();
    var templateCache = config.temp + config.templateCache.file;
    var cssFilter = $.filter('**/*.css', { restore: true });
    //var jsFilter = $.filter('**/*.js');
    var jsLibFilter = $.filter('**/' + config.optimized.lib);
    var jsAppFilter = $.filter('**/' + config.optimized.app);
    var options = {
        preserveComments: 'license',
        mangle: true,
        compress: {
            drop_console: true
        }
    };
    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, { read: false }), {
        starttag: '<!-- inject:templates:js -->'
    }))
        .pipe(assets)
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(assets)
        .pipe(jsLibFilter)
        .pipe($.uglify(options))
        .pipe(assets)
        .pipe(jsAppFilter)
        //.pipe($.ngAnnotate())
        //.pipe(stripDebug())
        .pipe($.uglify(options))
        .pipe(assets)
        .pipe(gulp.dest(config.build));
});
gulp.task('dev-optimize', ['styles','inject'], function () {
    log('Optimizing the javascript, css, html');
    //var assets = $.useref.assets({ searchPath: './' });
    var assets = $.useref();
    var templateCache = config.temp + config.templateCache.file;
    log(templateCache);
    var cssFilter = $.filter('**/*.css', { restore: true });
    //var jsFilter = $.filter('**/*.js');
    var jsLibFilter = $.filter('**/' + config.optimized.lib);
    var jsAppFilter = $.filter('**/' + config.optimized.app);
    var options = {
        preserveComments: 'license',
        mangle: true
    };
    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, { read: false }), {
        starttag: '<!-- inject:templates:js -->'
    }))
        .pipe(assets)
        .pipe(cssFilter)
        .pipe($.csso())
        .pipe(assets)
        .pipe(jsLibFilter)
        .pipe(assets)
        .pipe(jsAppFilter)
        .pipe($.ngAnnotate())
        .pipe(assets)
        .pipe(gulp.dest(config.build));
});

gulp.task('test', function (done) {
    var server = karma.server;
    server({
        configFile: 'karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('lint', function () {
    return gulp.src('./app/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
//////////////////////////
function errorLogger(error) {
    log('*** Start of Error ***');
    log(error);
    log('*** End of Error ***');
    this.emit('end');
}
function clean(path, done) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del(path, done);
}
function log(msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.yellow(msg[item]));
            }
        }
    }
    else {
        $.util.log($.util.colors.yellow(msg));
    }
}
