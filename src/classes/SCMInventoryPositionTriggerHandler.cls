/**
 * Copyright (c) 2014, Financial Force, inc
 * All rights reserved.
 *
 *  Orthosensor trigger handler for the inventory position
 *  This trigger handler supports automatically setting the ownership code on the inventory 
 *  position to match the ownership code assigned to the warehouse
 **/ 
 public with sharing class SCMInventoryPositionTriggerHandler {
 	
 	Map<ID,SCMC__Inventory_Location__c> locns = new Map<ID, SCMC__Inventory_Location__c>();
 	
 	public void onBeforeInsert(SCMC__Inventory_Position__c[] posns){
		Map<ID, SCMC__Receipt_Line__c> receipts = new Map<ID, SCMC__Receipt_Line__c>();
		for (SCMC__Inventory_Position__c posn : posns){
			locns.put(posn.SCMC__Bin__c, null);
			if (posn.SCMC__Receipt_Line__c != null) 
				receipts.put(posn.SCMC__Receipt_Line__c, null);
		}
		receipts = 
			new Map<ID, SCMC__Receipt_Line__c>([
				select 
					id, 
					SCMC__Receiver__c, 
					SCMC__Receiver__r.SCMC__Return_Material_Authorization__c, 
					SCMC__Receiver__r.SCMC__Return_Material_Authorization__r.Value__c
				from SCMC__Receipt_Line__c
				where id in :receipts.Keyset()]);
 		popLocns();
		for (SCMC__Inventory_Position__c posn : posns){
			SCMC__Inventory_Location__c locn = locns.get(posn.SCMC__Bin__c);
			if (locn != null &&
				locn.SCMC__Warehouse__c != null){
				posn.SCMC__Ownership_Code__c = locn.SCMC__Warehouse__r.Ownership_Code__c;
			}
			SCMC__Receipt_Line__c rline = receipts.get(posn.SCMC__Receipt_Line__c);
			if
			(
				rLine != null &&
				rLine.SCMC__Receiver__c != null &&
				rline.SCMC__Receiver__r.SCMC__Return_Material_Authorization__c != null && 
				rline.SCMC__Receiver__r.SCMC__Return_Material_Authorization__r.Value__c != null
			)
			{
				posn.SCMC__Current_Value__c = rline.SCMC__Receiver__r.SCMC__Return_Material_Authorization__r.Value__c;	
			}
		}
 	}
 	
 	public void onBeforeUpdate(SCMC__Inventory_Position__c[] oldPosns, SCMC__Inventory_Position__c[] newPosns){
		for(Integer i = 0; i < oldPosns.Size(); i++){
			if (oldPosns[i].SCMC__Bin__c != newPosns[i].SCMC__Bin__c){
				locns.put(newPosns[i].SCMC__Bin__c, null);
			}
			if(newPosns[i].SCMC__Shelf_Life_Expiration__c != null &&
			   oldPosns[i].SCMC__Shelf_Life_Expiration__c != newPosns[i].SCMC__Shelf_Life_Expiration__c){
				 newPosns[i].SCMC__Date_Placed_in_Inventory__c = newPosns[i].SCMC__Shelf_Life_Expiration__c;
			}							
		}
		popLocns();
		for(Integer i = 0; i < oldPosns.Size(); i++){
			if (oldPosns[i].SCMC__Bin__c != newPosns[i].SCMC__Bin__c){
				SCMC__Inventory_Location__c locn = locns.get(newPosns[i].SCMC__Bin__c);
				if (locn != null &&
					locn.SCMC__Warehouse__c != null){
					newPosns[i].SCMC__Ownership_Code__c = locn.SCMC__Warehouse__r.Ownership_Code__c;
				}
			}		
		}	
 	}
 	
 	private void popLocns() {
 		if (locns.Keyset().Size() > 0){
			locns = new Map<ID, SCMC__Inventory_Location__c>([Select Id
					,name
					,SCMC__Warehouse__c
					,SCMC__Warehouse__r.Ownership_Code__c
				from SCMC__Inventory_Location__c
				where id in :locns.Keyset()]);
		}
 	}
}