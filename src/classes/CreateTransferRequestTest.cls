@isTest(seeAllData=true)
public class CreateTransferRequestTest {
    
    @isTest
        public static void CreateTransReqTest() {
        
        User user1 = [select id, name from user where name = 'Mark Hottle'];
        SCMC__Item__c item1 = [select id, name from SCMC__Item__c where name like 'syk%' limit 1];
        
        system.runAs(user1){
            
            Rep_Stock_Request__c request = new Rep_Stock_Request__c();
            request.Status__c = '';
            request.Date_of_Request__c = system.today();
            request.Rep_Name__c = user1.id; 
            request.Shipping_Address__c = null; 
            request.Shipping_Method__c = 'Hand Deliver';
            request.Notes__c = 'Here are some notes';
            insert request;
            
            Rep_Stock_Request_Item__c item = new Rep_Stock_Request_Item__c();
            item.Rep_Stock_Request__c= request.id;
            item.Quantity__c = 2;
            item.Item_Master__c = item1.id;
            insert item;
            
            CreateTransferRequest.mkTransferRequest(request.id);
            
        }
    }
}