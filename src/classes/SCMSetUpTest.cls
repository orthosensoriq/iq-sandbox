public with sharing class SCMSetUpTest {
	
	private SCMC__Condition_Code__c cCode = this.createConditionCode('New', 'NE');
	private SCMC__Currency_Master__c curr = this.createTestCurrency();
	private static Integer suffix = 0;

	public Account createTestCustomerAccount(Boolean custActive, Boolean doInsert, Boolean onHold, id warehouseSpecific) {

		Account custSite = new Account();
		custSite.SCMC__Active__c = custActive;

		if(onHold){
			custSite.SCMC__Temporary_Hold__c = 'Finance';
		} else {
			custSite.SCMC__Temporary_Hold__c = null;
		}

		custSite.AnnualRevenue = null;
		custSite.SCMC__CAGE_Code__c = null;
		custSite.SCMC__Capability_Listing__c = null;
		custSite.SCMC__Certifications__c = null;
		custSite.SCMC__Credit_Card_Expiration_Date__c = null;
		custSite.SCMC__Credit_Card_Number__c = null;
		custSite.SCMC__Credit_Card_Type__c = null;
		custSite.SCMC__Credit_Card_Validation_Code__c = null;
		custSite.SCMC__Customer__c = true;
		custSite.SCMC__DUNS_Number__c = null;
		custSite.SCMC__Default_Payment_Type__c = null;
		custSite.SCMC__Corp_City__c = 'San Diego';
		custSite.SCMC__Corp_Country__c = 'USA';
		custSite.SCMC__Corp_State_Province__c = 'CA';
		custSite.SCMC__Corp_Line1__c = '247 Ocean Blvd.';
		custSite.SCMC__Corp_Line2__c = null;
		custSite.SCMC__Corp_PostalCode__c = '92130';
		custSite.Description = 'Test customer site';
		custSite.FAX = '555-1234';
		custSite.SCMC__FOB__c = null;
		custSite.SCMC__Freight__c = null;
		custSite.Industry = null;
		custSite.SCMC__Language__c = null;
		custSite.Name = 'Test Customer' + string.valueOf(suffix++);
		custSite.NumberOfEmployees = null;
		custSite.Phone = null;
		custSite.Ownership = null;
		custSite.ParentId = null;
		custSite.SCMC__Preferred_Communication__c = 'Fax';
		custSite.SCMC__Provide_Advanced_Ship_Notice__c = false;
		custSite.SIC = null;
		custSite.SCMC__Sales_Rep__c = null;
		custSite.SCMC__Sales_Responsibility__c = null;
		custSite.SCMC__Ship_Via__c = null;
		custSite.SCMC__Small_Business_Designations__c = null;
		custSite.SCMC__Tax_Exemption_Group__c = null;
		custSite.SCMC__Terms__c = null;
		custSite.TickerSymbol = null;
		custSite.Type = null;
		custSite.Website = null;
		custSite.SCMC__warehouse__c = warehouseSpecific;

		custSite.BillingStreet = '123 Main St\nSuite 101\nc/0George';
		custSite.BillingCity = 'San Francisco';
		custSite.BillingState = 'CA';
		custSite.BillingPostalCode = '12345';
		custSite.BillingCountry = 'US';

		custSite.ShippingStreet = '123 Main St\nSuite 101\nc/0George';
		custSite.ShippingCity = 'San Francisco';
		custSite.ShippingState = 'CA';
		custSite.ShippingPostalCode = '12345';
		custSite.ShippingCountry = 'US';

		custSite.SCMC__Ship_Via__c = 'UPS Ground';
		custSite.SCMC__Shipping_Account__c = '123456789';

		if (doInsert){
			insert custSite;
		}

		return custSite;
	}

	public SCMC__Sales_Order__c createTestSalesOrder(Account acc, boolean doInsert){
		SCMC__Sales_Order__c so = new SCMC__Sales_Order__c();
		ID salesRep = createSalesRep();
		so.SCMC__Sales_Order_Date__c = Date.today();
		so.SCMC__Primary_Sales_Rep__c = salesRep;
		so.SCMC__Customer_Account__c = acc.Id;
		so.RecordTypeId = SCMUtilities.getRecordType('Inventory', so);

		if (doInsert){
			insert so;
		}

		return so;
	}

	public SCMC__Sales_Order_Line_Item__c createSalesOrderLine(SCMC__Sales_Order__c so, SCMC__Item__c item, String status, boolean doInsert){
		SCMC__Sales_Order_Line_Item__c line = new SCMC__Sales_Order_Line_Item__c();
		line.RecordTypeId = SCMUtilities.getRecordType('Item', line);
		line.SCMC__Sales_Order__c = so.Id;
		line.SCMC__Quantity__c = 1;
		line.SCMC__Price__c = 25.00;
		line.SCMC__Item_Master__c = item.Id;
		line.SCMC__Customer_Commitment_Date__c = Date.today();
		line.SCMC__Status__c = status;
		line.SCMC__Condition_Code__c = cCode.Id;
		if (doInsert){
			insert line;
		}
		return line;
	}

	public SCMC__Unit_of_Measure__c createUnitofMeasure(String name, String ilsName) {
		SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
		try {
			uom = [select id
					from SCMC__Unit_of_Measure__c
					where name = :name];
		} catch(Exception ex) {
			uom.name =name;
			if (ilsName <> null) {
				uom.SCMC__ILS_Unit_of_Measure__c = ilsName;
				uom.SCMC__Valid_for_ILS__c = true;
			}
			insert uom;
		}
		return uom;
	}

	public SCMC__Item__c createTestItem(boolean doInsert) {
		SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
		SCMC__Item__c im = new SCMC__Item__c();
		im.name = 'Test' + suffix++;
		im.SCMC__Item_Description__c = 'Test part for unit tests';
		im.SCMC__Inspection_Required__c  = false;
		im.SCMC__Serial_Number_Control__c = false;
		im.SCMC__Lot_Number_Control__c = false;
		im.SCMC__Stocking_UOM__c = uom.id;
		im.RecordTypeId = SCMUtilities.getRecordType('Master', im);

		if (doInsert) {
			insert im;
		}

		return im;
	}

	public ID createSalesRep(){
		return UserInfo.getUserId();
	}

	public SCMC__Condition_Code__c createConditionCode(String code, String ilscode) {
		SCMC__Condition_Code__c cCode = new SCMC__Condition_Code__c();
		try {
			cCode = [select id
					   from SCMC__Condition_Code__c
					  where name = :code];
		} catch (Exception ex) {
			cCode.name = code;
			cCode.SCMC__description__c = 'Description for ' + code;
			cCode.SCMC__Disposal_Code__c = '1 -Excellent';
			cCode.SCMC__Supply_Code__c = 'A -Serviceable';
			insert cCode;
		}
		return cCode;
	}

	public SCMC__Currency_Master__c createTestCurrency() {
		return createTestCurrency('CAD', true);
	}

	public SCMC__Currency_Master__c createTestCurrency(String cname, Boolean corporate) {
		SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
		try {
			curr = [select id, name
					from SCMC__Currency_Master__c
					where name = :cname];
		} catch (Exception ex) {
			curr.name = cname;
			curr.SCMC__Active__c = true;
			curr.SCMC__Conversion_Rate__c = 1;
			curr.SCMC__Corporate_Currency__c = corporate;
			insert curr;
		}
		return curr;
	}

	public SCMC__Customer_Address__c createCustomerAddress(Account a) {
		SCMC__Customer_Address__c address = new SCMC__Customer_Address__c();
		address.SCMC__Active__c = true;
		//address.Address_Number__c
		address.SCMC__Mailing_City__c = 'San Diego';
		address.SCMC__Mailing_Country__c = 'USA';
		address.SCMC__Mailing_State_Province__c = 'CA';
		address.SCMC__Mailing_Street__c = '247 Ocean Blvd.';
		address.SCMC__Mailing_Zip_Postal_Code__c = '92130';
		address.SCMC__Customer_Site__c = a.Id;
		insert address;

		return address;
	}

	public SCMC__Item__c createTestItem() {
		SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
		SCMC__Item__c im = new SCMC__Item__c();
		im.name = 'Test' + suffix++;
		im.SCMC__Item_Description__c = 'Test part for unit tests';
		im.SCMC__Inspection_Required__c  = false;
		im.SCMC__Serial_Number_Control__c = false;
		im.SCMC__Lot_Number_Control__c = false;
		im.SCMC__Stocking_UOM__c = uom.id;
		im.RecordTypeId = SCMUtilities.getRecordType('Master', im);
		insert im;
		return im;
	}
	
	public SCMC__Warehouse__c setupWarehouse() {
		SCMC__Address__c address = createTestAddress();
		SCMC__ICP__c icp = createTestICP(address);
		return createTestWarehouse(icp, address);
	}

	public SCMC__Address__c createTestAddress() {
		SCMC__Address__c address = new SCMC__Address__c();
		address.name = 'Test Address';
		address.SCMC__City__c = 'A City';
		address.SCMC__Country__c = 'Country';
		address.SCMC__Line1__c = 'Address line 1';
		address.SCMC__PostalCode__c = 'Postcd';
		address.SCMC__State__c = 'State';
		insert address;
		return address;
	}

	public SCMC__ICP__c createTestICP(SCMC__Address__c address) {
		SCMC__ICP__c icp = new SCMC__ICP__c();
		icp.SCMC__Address__c = address.id;
		icp.name = 'Test ICP';
		icp.SCMC__Currency__c = curr.id;
		insert icp;
		return icp;
	}

	public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address){
		return createTestWarehouse(icp, address, true);
	}

	public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address, boolean doInsert){
		SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
		warehouse.SCMC__ICP__c = icp.id;
		warehouse.SCMC__Address__c = address.id;
		warehouse.name = 'Test Warehouse';
		if (doInsert) {
			insert warehouse;
		}
		return warehouse;
	}

	private static Integer fakeIdCount = 0;
	public static Id generateMockId(Schema.SObjectType sobjectType)
	{
		String keyPrefix = sobjectType.getDescribe().getKeyPrefix();
		fakeIdCount++;
		String fakeIdPrefix = '000000000000'.substring(0, 12 - fakeIdCount.format().length());
		return Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount);
	}
}