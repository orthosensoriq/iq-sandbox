/*****************************************************
This class is designed to write CRUD audit logging to 
the logging object for OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public without sharing class logUtils
{
	//running from @istest
	public static boolean isApexTest = false;
    
    public void logCRUD(String action, Map<Id,SObject> beforeData, Map<Id, SObject> afterData)
    {
        System.debug('In logCRUD');
        list<Audit_Log__c> logIt = new list<Audit_Log__c>();
        For(SObject curObject : beforeData.values())
        {
            string objAfterJSON = JSON.serialize(afterData.get(curObject.id));
            string objBeforeJSON = JSON.serialize(curObject);
            // system.debug('curObject: ' + objAfterJSON);
            Audit_Log__c newLogs = new Audit_Log__c();
            newLogs.Action__c = action;
            newLogs.Date__c = DateTime.now();
            newLogs.Parent_Record_Id__c = curObject.Id;
            newLogs.User__c = UserInfo.getUserId();
            newLogs.S_object_Type__c = getReadObjects(objAfterJSON);
            newLogs.Parent_Record_Name__c = String.valueOf(curObject.get('Name'));
            newLogs.Parent_Record_Before_Long_Text_JSON__c = objBeforeJSON;
			newLogs.Parent_Record_Long_Text_JSON__c = objAfterJSON; 
			newLogs.Update_Delta__c = getUpdateDeltas(objBeforeJSON, objAfterJSON);
                
            logIt.add(newLogs);
        }
        Insert logIt;
    }
    
    public void logCRUD(string action, Map<Id,SObject> logData)
    {
        list<Audit_Log__c> logIt = new list<Audit_Log__c>();
        For(SObject curObject : logData.values())
        {
	        string objJSON = JSON.serialize(curObject); 
            // system.debug('curObject: ' + objJSON);
            Audit_Log__c newLogs = new Audit_Log__c();
            newLogs.Action__c = action;
            newLogs.Date__c = DateTime.now();
            newLogs.Parent_Record_Id__c = curObject.Id;
            newLogs.User__c = UserInfo.getUserId();
            newLogs.S_object_Type__c = getReadObjects(objJSON);
            newLogs.Parent_Record_Name__c = String.valueOf(curObject.get('Name'));
			newLogs.Parent_Record_Long_Text_JSON__c = objJSON; 
			
            logIt.add(newLogs);
        }
        Insert logIt;
    }
    
    public void logCRUD(string action, List<SObject> logData)
    {
        list<Audit_Log__c> logIt = new list<Audit_Log__c>();
        For(SObject curObject : logData)
        {
	        string objJSON = JSON.serialize(curObject); 
            system.debug('curObject: ' + objJSON);
            Audit_Log__c newLogs = new Audit_Log__c();
            newLogs.Action__c = action;
            newLogs.Date__c = DateTime.now();
            newLogs.Parent_Record_Id__c = curObject.Id;
            newLogs.User__c = UserInfo.getUserId();
            newLogs.S_object_Type__c = getReadObjects(objJSON);
            newLogs.Parent_Record_Name__c = String.valueOf(curObject.get('Name'));
			newLogs.Parent_Record_Long_Text_JSON__c = objJSON; 
			
            logIt.add(newLogs);
        }
        Insert logIt;
    }
    
    public void logCRUD(string action, SObject curObject)
    {
        if(action=='View Report')
            ReportMetadata(curObject.Id,System.Userinfo.getSessionId());
        else{
        string objJSON = JSON.serialize(curObject); 
        system.debug('curObject: ' + objJSON);
        Audit_Log__c newLogs = new Audit_Log__c();
        newLogs.Action__c = action;
        newLogs.Date__c = DateTime.now();
        newLogs.Parent_Record_Id__c = curObject.Id;
        newLogs.User__c = UserInfo.getUserId();
        
        newLogs.S_object_Type__c = getReadObjects(objJSON);
        newLogs.Parent_Record_Name__c = String.valueOf(curObject.get('Name'));
        newLogs.Parent_Record_Long_Text_JSON__c = objJSON; 
        
        Insert newLogs;
    	}
    }
    
    public static void apimethodForTest(string testReportId){
        isApexTest = true;
        ReportMetaData(testReportId,System.Userinfo.getSessionId());
    }
    
    @future(callout=true)
    public static void ReportMetadata(string ReportId,string mysess)
    {
	    HttpRequest request = new HttpRequest();

        //populate header
        system.debug(System.userinfo.getSessionId());
        system.debug(mysess);
        system.debug(System.Url.getSalesforceBaseUrl().toExternalForm());
        //System.debug;
        final PageReference epUrl = new PageReference(System.Url.getSalesforceBaseUrl().toExternalForm() +'/services/data/v29.0/analytics/reports/'+ReportId+'/describe');
		//excludes Est_Actual_Contracts__c
        request.setEndpoint(epUrl.getUrl());
        request.setMethod('GET');
        request.setHeader('Authorization', 'OAuth ' + mysess);
		request.setTimeout(120000);
        Http http = new Http();
        String jsonbody = '';

		//if @istest, don't send httprequest
        if(!isApexTest){
        	HTTPResponse response = http.send(request);
            if (response.getstatus()=='OK'){            //check status to make sure it's not bad
            	jsonbody = response.getBody();
				Audit_Log__c newLogs = new Audit_Log__c();
        		newLogs.Action__c = 'View Report';
        		newLogs.Date__c = DateTime.now();
        		newLogs.Parent_Record_Id__c = ReportId;
        		newLogs.User__c = UserInfo.getUserId();
        		newLogs.S_object_Type__c = 'Report';
        		newLogs.Parent_Record_Name__c = '';
        		newLogs.Parent_Record_Long_Text_JSON__c = jsonbody; 
        
        		Insert newLogs;
            }else{
            	system.debug(response.getStatusCode());
            }
        }
        
    }
    
    Private string getReadObjects(string recordString)
    {
        set<string> whichObjects = new set<string>();
        string tempString = recordString;
        //look through the JSON and find all the attribute:type
        while(tempString.indexOf('"attributes":{"type":"') != -1)
        {
            tempString = tempstring.substringAfter('"attributes":{"type":"');
      		whichObjects.add(tempString.substring(0, tempString.indexOf('"')));        
        }
        tempString = '';
        for(string s: whichObjects)
		{
            tempString += (s + ', '); 
		}
        if(tempString != '')
            tempString = tempString.subString(0, tempString.length() - 2);
        
        return tempString;
    }
    
    Private string getUpdateDeltas(string beforeObj, string afterObj)
    {
        list<string> deltas = new list<string>();
        set<string> allFields = new set<string>();
        set<string> ignoreFields = new set<string>{'attributes', 'LastModifiedDate', 'LastModifiedById', 'CreatedById', 'CreatedDate', 'SystemModstamp'};

            // Deserialize it back into a key/value map
        Map<String,Object> beforeMap = (Map<String,Object>) JSON.deserializeUntyped(beforeObj);
        allFields.addAll(beforeMap.keySet());
        Map<String,Object> afterMap = (Map<String,Object>) JSON.deserializeUntyped(afterObj);
        allFields.addAll(afterMap.keySet());
        
        allFields.removeAll(ignoreFields);
        system.debug('getUpdateDeltas: allFields.size = ' + allFields.size());
        
        for(string fieldName : allFields)
        {
            if(beforeMap.get(fieldName) != null && afterMap.get(fieldName) != null)
            {
            	if(beforeMap.get(fieldName) != afterMap.get(fieldName))
                {
                    // system.debug('getUpdateDeltas: adding - ' + fieldName + ': ' + beforeMap.get(fieldName) + ' | ' + afterMap.get(fieldName));
                    deltas.add(fieldName + ': ' + beforeMap.get(fieldName) + ' | ' + afterMap.get(fieldName));
                }
            }
            else if(beforeMap.get(fieldName) != null && afterMap.get(fieldName) == null)
            {
                system.debug('getUpdateDeltas: adding - ' + fieldName + ': ' + beforeMap.get(fieldName) + ' | EMPTY');
                deltas.add(fieldName + ': ' + beforeMap.get(fieldName) + ' | EMPTY'); 
            }
            else if(beforeMap.get(fieldName) == null && afterMap.get(fieldName) != null)
            {
                // system.debug('getUpdateDeltas: adding - ' + fieldName + ': ' + 'EMPTY | ' + afterMap.get(fieldName));
             	deltas.add(fieldName + ': ' + 'EMPTY | ' + afterMap.get(fieldName));
            }
        }

        // system.debug('getUpdateDeltas: returning - ' + string.join(deltas, ', '));
        return string.join(deltas, ', ');
    }

}