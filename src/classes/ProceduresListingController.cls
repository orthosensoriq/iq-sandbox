public with sharing class ProceduresListingController{

	public String AccountID {get;set;}
	public String ProcedureID {get;set;}
	public String SurgeonID {get;set;}
	public List<Account> surgeonAccounts {get;set;}
	public List<Contact> surgeonContacts {get;set;}
	public List<Event__c> procedures {get;set;}
	public List<Clinical_Data__c> clinicalData {get;set;}
	public List<SelectOption> hospitalNames  {get;set;}
	public List<SelectOption> surgeonNames  {get;set;}
	public String surgeonName {get;set;}
	public String surgeonFilter {get;set;}
	public Account selectedHospital {get;set;}
	List<Patient__c> patients = new List<Patient__c>();
	Map<String, Account> accountNameMap = new Map<String, Account>();
	Event_Type__c eventType;
	public Opportunity tempOpportunity {get;set;}
	public String formattedProcDate {get;set;}
	public boolean changeToToday {get;set;}
    public integer procedurecount {get;set;}
	public integer rowcount { get; set; }
    osController basectrl;
    
	public ProceduresListingController(){
        //rowcount=0;
		AccountID = ApexPages.currentPage().getParameters().get('accid');
		changeToToday = false;
		hospitalNames = new List<SelectOption>();
		surgeonNames = new List<SelectOption>();
		List<Account> hospitals = [SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000];
		for(Account hospital:hospitals)
		{
			if(hospital.Name!=null) hospitalNames.add(new SelectOption(hospital.Name, hospital.Name));
			if(hospital.id == AccountID) selectedHospital = hospital;
			accountNameMap.put(hospital.Name, hospital);
		}
		if(selectedHospital==null && hospitals.size()>0) selectedHospital = hospitals[0];
		if(selectedHospital!=null) patients = [Select Id, First_Name__c, Last_Name__c, Gender__c, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id =: selectedHospital.id];
		eventType = [Select Id from Event_Type__c where primary__c = true]; //Name = 'Procedure'];
		
		tempOpportunity = new Opportunity();
		if(tempOpportunity.CloseDate==null) tempOpportunity.CloseDate = System.today();
		formattedProcDate = tempOpportunity.CloseDate.format();
		List<String> splitString = formattedProcDate.split('/',4);
		if(splitString[1].length()==1) splitString[1] = '0' + splitString[1];
		formattedProcDate = splitString[0] + '/' + splitString[1] + '/' + splitString[2];
		
		surgeonFilter = '';
		surgeonContacts = [Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:selectedHospital.Id)];
		surgeonAccounts = [Select Id, Name from Account Where personcontactID IN: surgeonContacts];
		surgeonNames.add(new SelectOption('All Surgeons','All Surgeons'));
		for(Account surgeon:surgeonAccounts)
		{
			if(surgeon.Name!=null) surgeonNames.add(new SelectOption(surgeon.Name,surgeon.Name));	
		}
		
			
	}
	
    public void LogConstructor()
    {
        GetProcedures();
    }

    public void GetProcedures(){
            map<integer, string> params = new map<integer, string>();
            params.put(0, eventType.Id);
         	baseCtrl = new osController();
            procedures = (list<Event__c>)baseCtrl.getSelectedData('ProcedureListings', params);

        /*procedures = [Select 
								Id, 
								Name, 
								Appointment_Start__c, 
								Description__c, 
								Case__c, 
								Case__r.Patient__c, 
								Case__r.Patient__r.Name, 
								Case__r.Patient__r.Last_Name__c, 
								Case__r.Patient__r.First_Name__c, 
								Case__r.Patient__r.Gender__c, 
								Case__r.Patient__r.Date_Of_Birth__c,
								Case__r.Patient__r.Medical_Record_Number__c,
								Case__r.Case_Type__r.Description__c,
								Physician__c,
								Physician__r.Name,
								Physician__r.Id,
								Location__c,
								Location__r.Name
							FROM Event__c WHERE Event_Type__c=: eventTypeID AND Location__c!=null LIMIT 999]; //WHERE Location__c =: selectedHospital.id 
			//procedurecount = procedures.size();
			//rowcount = procedures.size();
			*/
	}
    
    public void GetPatients(string pHospital)
    {
        map<integer, string> params = new map<integer, string>();
        params.put(0, pHospital);
        baseCtrl = new osController();
        patients = (list<Patient__c>)baseCtrl.getSelectedData('ProcedureListingPatients', params);
        
        //patients = [Select Id, First_Name__c, Last_Name__c, Gender__c, Hospital__r.Name, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id =: selectedHospital.id];

    }
	
	public PageReference goToProcedureSummary(){
		String accString = '';
		if(selectedHospital.Id!=null) accString = '&accid=' + selectedHospital.Id;
		String surgeonString = '';
		if(SurgeonID!=null && SurgeonID!='') surgeonString = '&surgId=' + SurgeonID;
		PageReference pageRef = new PageReference('/apex/ProcedureSummary?procid=' + ProcedureID + accString + surgeonString);
		return pageRef;
		
	}
	
	public void ChangeHospital(){
		
		if(!accountNameMap.isEmpty()) selectedHospital = accountNameMap.get(selectedHospital.Name);
		
		if(selectedHospital!=null) 
		{
			//patients = [Select Id, First_Name__c, Last_Name__c, Gender__c, Hospital__r.Name, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id =: selectedHospital.id];
			GetPatients(selectedHospital.Id);
			GetProcedures();
		}
		
	}
	
	public void ChangeDate(){
		
		if(tempOpportunity.CloseDate!=null)
		{
			formattedProcDate = tempOpportunity.CloseDate.format();
			List<String> splitString = formattedProcDate.split('/',4);
			if(splitString[1].length()==1) splitString[1] = '0' + splitString[1];
			formattedProcDate = splitString[0] + '/' + splitString[1] + '/' + splitString[2];
		}
		else{
			formattedProcDate = '';	
		}
		if(changeToToday)
		{
			formattedProcDate = System.now().format('MM/dd/yyyy');
			tempOpportunity.CloseDate = System.Today();
			changeToToday = false;
		}
		
		if(selectedHospital!=null) 
		{
			//patients = [Select Id, First_Name__c, Last_Name__c, Gender__c, Hospital__r.Name, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id =: selectedHospital.id];
			GetPatients(selectedHospital.Id);
			GetProcedures();
		}
		
	}
	
	public void ChangeSurgeon(){
			//rowcount = null;
			if(surgeonName!='All Surgeons') surgeonFilter = surgeonName;
			GetProcedures();
		
	}
	
	public PageReference PostBack()
    {		
		PageReference pageRef = new PageReference('/Apex/ProcedureListing?accid=' + selectedHospital.Id);
        return pageRef;
    }
	
	public PageReference GoToCaseSelector()
    {		
		if(!accountNameMap.isEmpty()) selectedHospital = accountNameMap.get(selectedHospital.Name);
		PageReference pageRef = new PageReference('/apex/CaseSelector?accid=' + selectedHospital.id);
        return pageRef;
    }
	


}