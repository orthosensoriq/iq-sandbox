/*****************************************************
This class is designed to provide the User Agreements 
    functionality for Orthosensor managed internal users

REVISIONS
------------------------------------------------------
01/08/2013 - support@enablepath.com - Created

*****************************************************/

public class UserAgreementsController 
{
    public string AttachmentID { get; set; }
    public string AttachmentVersion { get; set; }
    public boolean AgreedVal { get; set; }
    public string agreementMessage { get; set; }
    string whichCurrent = '';
    Map<string,string> URLParameters;
    User curUser;
    string returnTo = '';
	public String imageURL{get;set;}
   
    public UserAgreementsController()
    {
        imageURL='/servlet/servlet.FileDownload?file=';
        List< document > documentList=[select name from document where 
                                        Name='logo-orthosensor.png'];
       
        if(documentList.size()>0)
        {
          imageURL=imageURL+documentList[0].id;
        }
        
    //Need to:
    	//Get URLParameters
        //system.debug('Getting URLParameters...');
        URLParameters = ApexPages.currentPage().getParameters();
        //system.debug('URLParameters.size: ' + URLParameters.size());
        AgreedVal = false;
            
        returnTo = URLParameters.get('retURL');
        
		curUser = [SELECT  	Id,
                   			Accepted_EULA_Version__c,
                   			Accepted_HIPAA_Version__c
                   FROM    	User
                   WHERE   	Id =: System.UserInfo.getUserId()];
        //system.debug('Got curUser: ' + curUser.Id);
    	//Determine if EULA, HIPAA, or BOTH
        if(URLParameters.get('EULA') != null)
        {
    		//Set AttachmentID property
            AttachmentId = URLParameters.get('EULA');
            AttachmentVersion = URLParameters.get('EULAVer');
            agreementMessage = 'EULA_Agreement_Header';
            whichCurrent = 'EULA';
        }
        else if(URLParameters.get('HIPAA') != null)
        {
    		//Set AttachmentID property
            AttachmentId = URLParameters.get('HIPAA');
            AttachmentVersion = URLParameters.get('HIPAAVer');
            agreementMessage = 'HIPAA_Agreement_Header';
            whichCurrent = 'HIPAA';
        }
        //system.debug(whichCurrent + ' AttachmentId: ' + AttachmentId);
    }
    
    public PageReference SaveAndReturn()
    {
        PageReference pageRef = null;
    //On Save:
    	//Save 
        system.debug('AgreedVal: ' + AgreedVal + ', whichCurrent: ' + whichCurrent);
        
        if(whichCurrent == 'EULA' && AgreedVal)
        {
        	curUser.Accepted_EULA_Version__c = AttachmentId;
            Update curUser;
            
			saveToHistory();
            //system.debug('updating curUser (EULA)...');
            if(URLParameters.get('HIPAA') != null)
            {
            	AttachmentId = URLParameters.get('HIPAA');
            	AttachmentVersion = URLParameters.get('HIPAAVer');
	            agreementMessage = 'HIPAA_Agreement_Header';
                whichCurrent = 'HIPAA';
                AgreedVal = false;
                //System.debug('AttachmentId (HIPAA): ' + AttachmentId);
                return null;
            }
        }
        else if(whichCurrent == 'HIPAA' && AgreedVal)
        {
        	curUser.Accepted_HIPAA_Version__c = AttachmentId;
            Update curUser;
            
			saveToHistory();
            //system.debug('updating curUser (HIPAA)...');
        }
        //System.debug('returnTo: ' + returnTo);
        //if(returnTo != null)
	  	//	pageRef = new PageReference(returnTo);
        //else
            pageRef = Page.DashboardPatientMatrix;
            //pageRef = new PageReference('/home/home.jsp');
        
		return pageRef;
    }
    
    private void saveToHistory()
    {
        User_Acceptance_History__c accepted = new User_Acceptance_History__c();
        accepted.user__c = System.UserInfo.getUserId();
        accepted.Agreement_Type__c = whichCurrent;
        accepted.Acceptance_Version_Id__c = AttachmentId;
        accepted.Accepted_Version__c = AttachmentVersion;
        Insert accepted;
    }
}