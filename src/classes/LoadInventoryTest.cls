@isTest
private with sharing class LoadInventoryTest {

    static Testmethod void testLoad() {
    	List<LoadInventory__c>testLoad = new List<LoadInventory__c>();
    	LoadInventory__c ink = new LoadInventory__c();
    	ink.Warehouse__c = 'SCMTest Warehouse';
    	ink.UOM__c = 'SCMTestuom';
    	ink.Unit_Cost__c = 55.23;
    	ink.Quantity__c = 100;
    	ink.Location__c = 'SCMTestLocation';
    	ink.Item_Number__c = 'SCMTestItem';
    	ink.Reason__c = 'Initial Load';
    	ink.Condition__c = 'New';
    	ink.Ownership__c = 'Project';
    	testLoad.add(ink);
    	LoadInventory__c inkr = new LoadInventory__c(
    		Warehouse__c = 'SCMTest Warehouse'
    		,UOM__c = 'SCMTestuom'
    		,Unit_Cost__c = 55.23
    		,Quantity__c = -10
    		,Location__c = 'SCMTestLocation'
    		,Item_Number__c = 'SCMTestItem'
    		,Reason__c = 'Initial Load'
    		,Condition__c = 'New'
    		,Ownership__c = 'Project');
    	testLoad.add(inkr);
    	
     	LoadInventory__c ink2 = new LoadInventory__c();
    	ink2.Warehouse__c = 'SCMTest Warehouse';
    	ink2.UOM__c = 'xxx';
    	ink2.Condition__c = 'xxx';
    	ink2.Reason__c = 'xxx';
    	ink2.Unit_Cost__c = 55.23;
    	ink2.Quantity__c = 100;
    	//ink.Ownership__c = 'Apple';
    	ink2.Location__c = 'SCMTestLocation';
    	ink2.Item_Number__c = 'xxx';
    	testLoad.add(ink2);
    	LoadInventory__c ink3 = new LoadInventory__c();
    	ink3.Warehouse__c = 'SCMTest Warehouse';
    	ink3.UOM__c = 'SCMTestuom';
    	ink3.Unit_Cost__c = 55.23;
    	ink3.Quantity__c = -50;
    	//ink3.Lot_Number__c = 'Lot';
    	//ink3.Ownership__c = 'Apple';
    	ink3.Location__c = 'SCMTestLocation';
    	ink3.Item_Number__c = 'SCMTestItem';
    	testLoad.add(ink3);
    	insert testLoad;
    	//Create warehouse, location, Reason, UOM and item.
    	//create address, ICP and warehouse
    	SCMC__Address__c address = new SCMC__Address__c();
        address.name = 'Test Address';
        address.SCMC__City__c = 'A City';
        address.SCMC__Country__c = 'Country';
        address.SCMC__Line1__c = 'Address line 1';
        address.SCMC__PostalCode__c = 'Postcd';
        address.SCMC__State__c = 'State';
        insert address;
        SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c(
        		name = 'USD'
        		,SCMC__Active__c = TRUE
        		,SCMC__Corporate_Currency__c = TRUE);
        insert curr;
       	SCMC__ICP__c icp = new SCMC__ICP__c();
        icp.SCMC__Address__c = address.id;
        icp.name = 'SCMTest ICP';
        icp.SCMC__Currency__c = curr.id;
        icp.SCMC__ILS_User__c = 'xxxxxxx';
        icp.SCMC__ILS_Password__c = 'yyyyyy';
        insert icp;
		SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
        warehouse.SCMC__ICP__c = icp.id;
        warehouse.SCMC__Address__c = address.id;
        warehouse.name = 'SCMTest Warehouse';
        insert warehouse;
        SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c();
        location.name = 'SCMTestLocation';
        location.SCMC__Level1__c = 'SCMTestLocation';
        location.SCMC__Warehouse__c = warehouse.id;
        insert location;
        SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
        uom.name = 'SCMTestuom';
        insert uom;
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'SCMTestItem';
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
        insert im;
        SCMC__Reason_Code__c reason = new SCMC__Reason_Code__c(
        		name = 'Initial Load');
        insert reason;
        SCMC__Condition_Code__c ccode = new SCMC__Condition_Code__c(
        		name = 'New');
        insert ccode;
        
        SCMC__Ownership_Code__c owner = new SCMC__Ownership_Code__c(
        		name = 'Project');
        insert owner;
        
    	Test.startTest();
    	ApexPages.StandardSetController setController =
    		new ApexPages.StandardSetController(testLoad);
    	LoadInventoryExtension extension = new LoadInventoryExtension(setController);
    	extension.process();
    	extension.goback();
    	Test.stopTest();
    	List<LoadInventory__c> testInv = [Select id
    			, name
    			, Warehouse__c
    			, WarehouseRef__c
    			, Unit_of_Measure__c
    			, Unit_cost__c
    			, Stocking_UOM__c
    			, Quantity__c
    			, Ownership__c
    			, Ownership_Code__c
    			, Condition__c
    			, Condition_Code__c
    			, Location__c
    			, Loaded__c
    			, Lot_Number__c
    			, Item_Number__c
    			, Item_Master__c
				, Inventory_Location__c
				, Error__c
				, Error_Message__c
			 From LoadInventory__c
			 where Loaded__c = True];
		System.assert(testInv.Size() > 0);
    }

}