/*****************************************************
This class is designed to provide definitions for 
CRUD audit logging for OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public without sharing class osController
{
    public sObject curObject { get; set; }
    public string curUser { get; set; }
    logUtils osLogUtils = new logUtils();
    
    public osController(){}
    public osController(ApexPages.StandardController ctrl)
    {
    	curObject = (sObject)ctrl.getRecord();
        curUser = system.UserInfo.getUserId();
        //LogView();
    }
    
    public void LogPDFView()
    {
        osLogUtils.logCRUD('PDF Read', curObject);
    }

    public void LogPDFGenerated()
    {
        osLogUtils.logCRUD('PDF Generated', curObject);
    }
    
    public void LogCSVGenerated()
    {
        osLogUtils.logCRUD('CSV Generated', curObject);
    }

    public void LogView()
    {
        osLogUtils.logCRUD('Read', curObject);
    }
    
    public List<sObject> getSelectedData(string pWhich, map<integer, string> params)
    {
   		osSelector selectObj = new osSelector();
        
        //Set the select statement based on the passed in Params
        //then grab the returned data to Log and return
        selectObj.setSelectStatement(pWhich, params);
        List<sObject> returnData = selectObj.getSelectedData();
        
        //Log the 'READ'
        osLogUtils.logCRUD('Read', returnData);
        
        //Return
        return returnData;
    }
    
    public List<sObject> getSelectedData(sObject objInput)
    {
   		osSelector selectObj = new osSelector(objInput);
        
        //Set the select statement based on the passed in Params
        //then grab the returned data to Log and return
        string tempSelect = selectObj.SelectStatement;
        List<sObject> returnData = selectObj.getSelectedData();
        
        //Log the 'READ'
        osLogUtils.logCRUD('Read', returnData);
        
        //Return
        return returnData;
    }
    
    public List<sObject> getSelectedData(String inputId)
    {
   		osSelector selectObj = new osSelector(inputId);
        
        //Set the select statement based on the passed in Params
        //then grab the returned data to Log and return
        string tempSelect = selectObj.SelectStatement;
        List<sObject> returnData = selectObj.getSelectedData();
        
        //Log the 'READ'
        osLogUtils.logCRUD('Read', returnData);
        
        //Return
        return returnData;
    }

}