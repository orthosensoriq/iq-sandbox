public with sharing class PatientPopulationSearchController 
{
    public String searchAge {get;set;}
    public string searchUpperAge {get;set;}
	public String searchDOB {get;set;}
    public String searchProcDate {get;set;}
    public String searchUpperProcDate {get;set;}
	public String searchCaseType {get;set;}
    public double searchHeight {get;set;}
    public double searchWeight {get;set;}
    public double searchBMI {get;set;}
    public String AgeOperation {get;set;}
    public String ProcedureDateOperation {get;set;}
    public String searchParams {get;set;}
    public String[] selectedHospitals = new String[]{};
    public String[] selectedSurgeons = new String[]{};
    public String[] selectedRaces = new String[]{};
    public String[] selectedGenders = new String[]{};
    public String[] selectedCaseTypes = new String[]{};
    public String[] selectedLaterality = new String[]{};
    public String[] selectedPrimaryDiagnosis = new String[]{};
    public String[] selectedSecondaryDiagnosis = new String[]{};
    public String[] selectedManufacturers = new String[]{};
    public String[] exportData {get;set;}
    public Set<Id> eventIdsForExport = new Set<Id>();
	public boolean showExport {get;set;}
    public boolean showSearchResults {get;set;}
    public boolean showUpperAge {get;set;}
    public boolean showUpperProcDate {get;set;}
    public boolean isExportCSV {get;set;}
    public Id PatientID {get;set;}
	public List<PopWrapper> patientPopLines {get;set;}

	public PatientPopulationSearchController() {
		patientPopLines = new List<PopWrapper>();
        exportData = new List<string>();
        showSearchResults = false;
        isExportCSV = false;
        showUpperAge = false;
        AgeOperation = '';
        showUpperProcDate = false;
        ProcedureDateOperation = '';
		searchParams = '';
    }

    public void setUpperAgeVisibility()
    {
        if(AgeOperation == 'Between')
            showUpperAge = true;
       	else
            showUpperAge = false;
        
        system.debug('AgeOperation: ' + AgeOperation + ' | showUpperAge: ' + showUpperAge);
    }
    
    public void setUpperDateVisibility()
    {
        if(ProcedureDateOperation == 'Between')
            showUpperProcDate = true;
       	else
            showUpperProcDate = false;
        
        system.debug('ProcedureDateOperation: ' + ProcedureDateOperation + ' | showUpperProcDate: ' + showUpperProcDate);
    }
    
	public void getResults() {

        searchParams = '';
        patientPopLines.clear();
        eventIdsForExport.clear();

        if(selectedHospitals.size() == 0 && selectedSurgeons.size() == 0 && selectedCaseTypes.size() == 0 
            && searchHeight == 0.0 && searchWeight == 0.0 && searchBMI == 0.0 && searchAge == ''
            && selectedRaces.size() == 0 && selectedGenders.size() == 0 && selectedLaterality.size() == 0
            && selectedPrimaryDiagnosis.size() == 0 && selectedSecondaryDiagnosis.size() == 0
            && selectedManufacturers.size() == 0 && searchProcDate == ''
            )
        {
            showExport = false;
            showSearchResults = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter at Least one search criteria value');
            ApexPages.addMessage(myMsg);
        }
        else
        {
            showExport = true;
            showSearchResults = true;
            String searchQuery = '';
            if(isExportCSV)
                searchQuery = 'Select Id';
            else
            {
                searchQuery = 'SELECT count(id) eventCount, Case__r.Patient__c PatientId, Case__r.Patient__r.First_Name__c FirstName, Case__r.Patient__r.Last_Name__c LastName, Case__r.Patient__r.Date_Of_Birth__c DateOfBirth,';
                searchQuery = searchQuery + ' Location__r.Name HospitalName, Case__r.Patient__r.Gender__c Gender, Case__r.Patient__r.Race__c Race ';
            }    
            searchQuery = searchQuery + ' FROM Event__c';
            searchQuery = searchQuery + ' WHERE Case__r.Patient__r.isDeleted = False';

            if (selectedHospitals.size() > 0) {
                String hospitalIDs = '\'' + String.join(selectedHospitals,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Location__c IN ('+hospitalIDs+')';
                
                set<string> tempSet = new set<string>(selectedHospitals);
                searchParams +=  'Hospital in (';
                for(selectOption hosp : getHospitalItems())
                {
                    if(tempSet.contains(hosp.getValue()))
	                    searchParams += hosp.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
         	}

            if (selectedSurgeons.size() > 0) {
                String surgeonIDs = '\'' + String.join(selectedSurgeons,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Physician__c IN ('+surgeonIDs+')';
                //List<Event__c> surgPatients = Database.query(patientQuery);
                //Set<Id> patientIds = new Set<Id>();
                //for (Event__c sp : surgPatients) patientIds.add(sp.Patient_ID__c);
                //searchQuery = searchQuery + ' AND Id IN :patientIds';
                
                set<string> tempSet = new set<string>(selectedSurgeons);
                searchParams += 'Surgeon in (';
                for(selectOption surgeon : getSurgeonItems())
                {
                    if(tempSet.contains(surgeon.getValue()))
	                    searchParams += surgeon.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedCaseTypes.size() > 0) {
                String caseTypeIDs = '\'' + String.join(selectedCaseTypes,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Case__r.Case_Type__c IN ('+caseTypeIDs+')';
                //List<Case__c> casePatients = Database.query(patientQuery);
                //Set<Id> patientIds1 = new Set<Id>();
                //for (Case__c cp : casePatients) patientIds1.add(cp.Patient__c);
                //searchQuery = searchQuery + ' AND Id IN :patientIds1';
                
                set<string> tempSet = new set<string>(selectedCaseTypes);
                searchParams += 'Case Types in (';
                for(selectOption caseType : getCaseTypeItems())
                {
                    if(tempSet.contains(caseType.getValue()))
	                    searchParams += caseType.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedLaterality.size() > 0){
                String lateralityVals = '\'' + String.join(selectedLaterality,'\',\'') + '\'';
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE Laterality__c IN ('+lateralityVals+')';
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds2 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds2.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds2';

                set<string> tempSet = new set<string>(selectedLaterality);
                searchParams += 'Laterality in (';
                for(selectOption laterality : getLateralityItems())
                {
                    if(tempSet.contains(laterality.getValue()))
	                    searchParams += laterality.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (searchHeight > 0.0){
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE Height__c >= '+searchHeight;
                system.debug('Height eventQuery =====> '+eventQuery);
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds3 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds3.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds3';
                
                searchParams = searchParams + 'Height >= ' + string.valueOf(searchHeight) + ',';
            }

            if (searchWeight > 0.0){
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE Weight__c >= '+searchWeight;
                system.debug('Weight eventQuery =====> '+eventQuery);
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds4 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds4.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds4';

                searchParams = searchParams + 'Weight >= ' + string.valueOf(searchWeight) + ',';
            }

            if (searchBMI > 0.0){
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE BMI__c >= '+searchBMI;
                system.debug('BMI eventQuery =====> '+eventQuery);
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds5 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds5.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds5';

                searchParams = searchParams + 'BMI >= ' + string.valueOf(searchBMI) + ',';
            }

            if (searchAge != '' && searchAge != null) {
                Integer Agevalue = Integer.valueOf(searchAge);
                system.debug('Agevalue =====> '+Agevalue);
                if(AgeOperation == 'Between')
                {
                    Integer UpperAgevalue = Integer.valueOf(searchUpperAge);
	                system.debug('Agevalue =====> '+UpperAgevalue);
                    searchQuery = searchQuery + ' AND (Case__r.Patient__r.Calculated_Age__c >= :Agevalue and Case__r.Patient__r.Calculated_Age__c <= :UpperAgevalue)';
                    
	                searchParams = searchParams + 'Patient Age Between ' + searchAge + ' and ' + searchUpperAge + ',';
                }
				else
                {
                	searchQuery = searchQuery + ' AND Case__r.Patient__r.Calculated_Age__c ' + AgeOperation + ' :Agevalue';
	                searchParams = searchParams + 'Patient Age ' + AgeOperation + ' ' + searchAge + ',';
            	}
                    
            }

            if (searchProcDate != '' && searchProcDate != null) {
                //String eventQuery = 'SELECT Patient_ID__c FROM Event__c';
                Date ProcDatevalue = Date.Parse(searchProcDate);
                system.debug('ProcDatevalue =====> '+ProcDatevalue);
                if(ProcedureDateOperation == 'Between')
                {
                    Date UpperProcDatevalue = Date.Parse(searchUpperProcDate);
	                system.debug('UpperProcDatevalue =====> '+UpperProcDatevalue);
                    searchQuery = searchQuery + ' AND (Appointment_Start__c >= :ProcDatevalue and Appointment_Start__c <= :UpperProcDatevalue)';

	                searchParams = searchParams + 'Procedure Date Between ' + searchProcDate + ' and ' + searchUpperProcDate + ',';
                }
				else if(ProcedureDateOperation == '=')
                {
                    Date UpperProcDatevalue = Date.Parse(searchProcDate);
                    UpperProcDatevalue = UpperProcDatevalue.addDays(1);
	                system.debug('UpperProcDatevalue =====> '+UpperProcDatevalue);
                    searchQuery = searchQuery + ' AND (Appointment_Start__c >= :ProcDatevalue and Appointment_Start__c <= :UpperProcDatevalue)';
	                searchParams = searchParams + 'Procedure Date ' + ProcedureDateOperation + ' ' + searchProcDate + ',';
                }
				else

                {
                	searchQuery = searchQuery + ' AND Appointment_Start__c ' + ProcedureDateOperation + ' :ProcDatevalue';
	                searchParams = searchParams + 'Procedure Date ' + ProcedureDateOperation + ' ' + searchProcDate + ',';
            	}
                
                //system.debug('ProcDate eventQuery =====> '+eventQuery);
                //List<Event__c> events = Database.query(eventQuery);
                //Set<Id> patientIds9 = new Set<Id>();
                //for (Event__c e : events) patientIds9.add(e.Patient_ID__c);
                //searchQuery = searchQuery + ' AND Id IN :patientIds9';
            }

            if (selectedRaces.size() > 0) {
                String raceIDs = '\'' + String.join(selectedRaces,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Case__r.Patient__r.Race__c IN ('+raceIDs+')';

                set<string> tempSet = new set<string>(selectedRaces);
                searchParams += 'Race in (';
                for(selectOption race : getRaceItems())
                {
                    if(tempSet.contains(race.getValue()))
	                    searchParams += race.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedGenders.size() > 0) {
                String genderIDs = '\'' + String.join(selectedGenders,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Case__r.Patient__r.Gender__c IN ('+genderIDs+')';

                set<string> tempSet = new set<string>(selectedGenders);
                searchParams += 'Gender in (';
                for(selectOption gender : getGenderItems())
                {
                    if(tempSet.contains(gender.getValue()))
	                    searchParams += gender.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedManufacturers.size() > 0){
                String manufacturerVals = '\'' + String.join(selectedManufacturers,'\',\'') + '\'';
                String eventQuery = 'SELECT Event__c FROM Implant_Component__c WHERE Product_Catalog_Id__r.Manufacturer__c IN ('+manufacturerVals+')';
                system.debug(eventQuery);
                List<Implant_Component__c> manufacturerEvents = Database.query(eventQuery);
                Set<Id> EventIds8 = new Set<Id>();
                for (Implant_Component__c ic : manufacturerEvents) EventIds8.add(ic.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds8';

                set<string> tempSet = new set<string>(selectedManufacturers);
                searchParams += 'Manufacturer in (';
                for(selectOption manufacturer : getManufacturerItems())
                {
                    if(tempSet.contains(manufacturer.getValue()))
	                    searchParams += manufacturer.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedPrimaryDiagnosis.size() > 0){
                String primaryDiagVals = '\'' + String.join(selectedPrimaryDiagnosis,'\',\'') + '\'';
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE Primary_Diagnosis__c IN ('+primaryDiagVals+')';
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds6 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds6.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds6';

                set<string> tempSet = new set<string>(selectedPrimaryDiagnosis);
                searchParams += 'Primary Diag in (';
                for(selectOption diag : getPrimaryDiagItems())
                {
                    if(tempSet.contains(diag.getValue()))
	                    searchParams += diag.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedSecondaryDiagnosis.size() > 0){
                String secondaryDiagVals = '\'' + String.join(selectedSecondaryDiagnosis,'\',\'') + '\'';
                String eventQuery = 'SELECT Event__c FROM Clinical_Data__c WHERE Secondary_Diagnosis__c IN ('+secondaryDiagVals+')';
                List<Clinical_Data__c> clinicalEvents = Database.query(eventQuery);
                Set<Id> EventIds7 = new Set<Id>();
                for (Clinical_Data__c ep : clinicalEvents) EventIds7.add(ep.Event__c);
                searchQuery = searchQuery + ' AND Id IN :EventIds7';
                
                set<string> tempSet = new set<string>(selectedSecondaryDiagnosis);
                searchParams += + 'Secondary Diagnosis in (';
                for(selectOption diag : getSecondaryDiagItems())
                {
                    if(tempSet.contains(diag.getValue()))
	                    searchParams += diag.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if(!isExportCSV)
            {
                searchQuery = searchQuery + ' Group By Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c,';
                searchQuery = searchQuery + ' Location__r.Name, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Race__c ';
            }
            
            searchQuery = searchQuery + ' ORDER BY Case__r.Patient__r.Last_Name__c LIMIT 1000';
            system.debug('searchQuery =====> '+searchQuery);
            
            if(!isExportCSV)
            {
                List<AggregateResult> PopResult = Database.query(searchQuery);
                for(AggregateResult ar : PopResult)
                {
                    PopWrapper wrap = new PopWrapper(String.valueOf(ar.get('PatientId')),String.valueOf(ar.get('FirstName')),
                                                     String.valueOf(ar.get('LastName')),Date.valueOf(ar.get('DateOfBirth')),
                                                     String.valueOf(ar.get('HospitalName')),String.valueOf(ar.get('Gender')),
                                                     String.valueOf(ar.get('Race')));
                    patientPopLines.add(wrap);
                }
    			PopResult = null;	//Cleanup
                
                if(patientPopLines.size()==0 || patientPopLines == null)
                {
                    showExport = false;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results were found');
                    ApexPages.addMessage(myMsg);
                }
            }
            else
            {
                //NEED TO CREATE THE ID COLLECTION to use to pull the CSV data
                Map<Id, Event__c> eventResults = new Map<Id, Event__c>();
                eventResults.putAll((list<event__c>)Database.query(searchQuery));
                if(eventResults.size()==0 || eventResults == null)
                {
                    showExport = false;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results were found');
                    ApexPages.addMessage(myMsg);
                }
                else
                {
                	eventIdsForExport = eventResults.keySet();
                }
                eventResults = null;	//Cleanup
            }
            searchParams = searchParams.substringBeforeLast(',');
       }
    }

    public class PopWrapper
    {
        //Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c,';
        //Location__r.Name HospitalName, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Race__c
        public string PatientId {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public date DateOfBirth {get;set;}
        public string HospitalName {get;set;}
        public string Gender {get;set;}
        public string Race {get;set;}
        
        public PopWrapper(string patid, string fn, string ln, date dob, string hn, string g, string r)
        {
            PatientId = patid;
            FirstName = fn;
            LastName = ln;
            DateOfBirth = dob;
            HospitalName = hn;
            Gender = g;
            Race = r;
        }
    }
    
    public String[] getHospitals() {
        return selectedHospitals;
    }

    public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }

    public String[] getSurgeons() {
        return selectedSurgeons;
    }

    public void setSurgeons(String[] surgeons) {
        this.selectedSurgeons = surgeons;
    }

    public String[] getCaseTypes() {
        return selectedCaseTypes;
    }

    public void setCaseTypes(String[] caseTypes) {
        this.selectedCaseTypes = caseTypes;
    }

    public String[] getLaterality() {
        return selectedLaterality;
    }

    public void setLaterality(String[] laterality) {
        this.selectedLaterality = laterality;
    }

    public String[] getRaces() {
        return selectedRaces;
    }

    public void setRaces(String[] races) {
        this.selectedRaces = races;
    }

    public String[] getGenders() {
        return selectedGenders;
    }

    public void setGenders(String[] genders) {
        this.selectedGenders = genders;
    }

    //public String getAgeOperation() {
    //    return selectedAgeOperation;
    //}

    //public void setAgeOperation(String AgeOperation) {
    //    this.selectedAgeOperation = AgeOperation;
    //}

    public String[] getManufacturers() {
        return selectedManufacturers;
    }

    public void setManufacturers(String[] Manufacturers) {
        this.selectedManufacturers = Manufacturers;
    }

    public String[] getPrimaryDiagnosis() {
        return selectedPrimaryDiagnosis;
    }

    public void setPrimaryDiagnosis(String[] primaryDiagnosis) {
        this.selectedPrimaryDiagnosis = primaryDiagnosis;
    }

    public String[] getSecondaryDiagnosis() {
        return selectedSecondaryDiagnosis;
    }

    public void setSecondaryDiagnosis(String[] secondaryDiagnosis) {
        this.selectedSecondaryDiagnosis = secondaryDiagnosis;
    }

    public List<selectOption> getAgeOperationItems() {
        List<selectOption> options = new List<selectOption>();
		options.add(new SelectOption('=','Equal To'));
 	 	options.add(new SelectOption('<','Less Than'));
		options.add(new SelectOption('>','Greater Than'));
 	 	options.add(new SelectOption('Between','Between'));        
		return options;        
    }
    
    public List<selectOption> getProcedureDateOperationItems() {
        List<selectOption> options = new List<selectOption>();
		options.add(new SelectOption('=','Equal To'));
 	 	options.add(new SelectOption('<','Less Than'));
		options.add(new SelectOption('>','Greater Than'));
 	 	options.add(new SelectOption('Between','Between'));        
		return options;        
    }
    
    public List<selectOption> getHospitalItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Hospital Account' and ispartner = true ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }

    public List<selectOption> getSurgeonItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Surgeon Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.Id, a.Name));
        }
        return options;
    }

    public List<selectOption> getCaseTypeItems() {
        List<selectOption> options = new List<selectOption>();
        for (Case_Type__c ct : [SELECT Id, Description__c FROM Case_Type__c WHERE isDeleted = False]) { 
            options.add(new selectOption(ct.Id, ct.Description__c));
        }
        return options;
    }

    public List<selectOption> getLateralityItems() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult lateralityFieldDescription = Clinical_Data__c.Laterality__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : lateralityFieldDescription.getPicklistValues()) {
            options.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return options;
    }

    public List<selectOption> getRaceItems() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult raceFieldDescription = Patient__c.Race__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : raceFieldDescription.getPicklistValues()) {
            options.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return options;
    }

    public List<selectOption> getGenderItems() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult genderFieldDescription = Patient__c.Gender__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : genderFieldDescription.getPicklistValues()) {
            options.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return options;
    }

    public List<selectOption> getManufacturerItems() {
        List<selectOption> options = new List<selectOption>();
        Manufacturer__c[] manufacturers = [SELECT Id, Name FROM Manufacturer__c ORDER BY Name];
        for (Manufacturer__c m : manufacturers) {
            options.add(new selectOption(m.id, m.Name));
        }
        return options;
    }

    public List<selectOption> getPrimaryDiagItems() {
        List<selectOption> options = new List<selectOption>();
        AggregateResult[] primDiagAggr = [SELECT count(Id), Primary_Diagnosis__c, Primary_Diagnosis__r.Name__c diagName FROM Clinical_Data__c WHERE Primary_Diagnosis__c != null GROUP BY Primary_Diagnosis__c, Primary_Diagnosis__r.Name__c];
        for (AggregateResult pd : primDiagAggr) {
            String primDiagVal = String.valueOf(pd.get('diagName'));
            String primDiagId = String.valueOf(pd.get('Primary_Diagnosis__c'));
            options.add(new selectOption(primDiagId, primDiagVal));
        }
        return options;
    }

    public List<selectOption> getSecondaryDiagItems() {
        List<selectOption> options = new List<selectOption>();
        AggregateResult[] secDiagAggr = [SELECT count(Id), Secondary_Diagnosis__c, Secondary_Diagnosis__r.Name__c diagName FROM Clinical_Data__c WHERE Secondary_Diagnosis__c != null GROUP BY Secondary_Diagnosis__c, Secondary_Diagnosis__r.Name__c];
        for (AggregateResult sd : secDiagAggr) {
            String secDiagVal = String.valueOf(sd.get('diagName'));
            String secDiagId = String.valueOf(sd.get('Secondary_Diagnosis__c'));
            options.add(new selectOption(secDiagId, secDiagVal));
        }
        return options;
    }

    public PageReference exportToCSV() 
    {
        isExportCSV = true;
        getResults();
        if(eventIdsForExport != null)
        {
            boolean singleCaseType = (selectedCaseTypes.size() == 1 ? true : false);
            PatientPopulationExportController ppec = new PatientPopulationExportController();
            //Need to get all the data for the export
            exportData = ppec.BuildExportCSV(eventIdsForExport,singleCaseType);
            //system.debug('exportData.Size: ' + exportData.size());
            //if(exportData != null)
            //{
            //    for(string s : exportData)
            //        system.debug(s);
            //}
            eventIdsForExport.clear(); //Cleanup? 
            PageReference csvPage = Page.PatientPopulationSearchCSV;
            csvPage.setRedirect(false);
            isExportCSV = false;
            return csvPage;
        }
        else
        {
            isExportCSV = false;
            return Null;
        }
    }
    
    // ReDirect to the Patient View Page
    public PageReference goToPatientView(){
        PageReference pageRef = new PageReference('/apex/PatientView?id=' + PatientID);
        return pageRef;
    }
}