public class EventTriggerHandler extends BaseTriggerHandler
{
    final String CLASSNAME = '\n\n**** EventTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : the class constructor method
    //@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
    //@returns : nothing
    public EventTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
        String METHODNAME = CLASSNAME.replace('METHODNAME', 'EventTriggerHandler') + ' - class constructor';
        system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

        // trigger is executing
        TriggerIsExecuting = isExecuting;
        
        // set batch size
        BatchSize = pTriggerSize;
        
        // set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
        //Not Called for this Trigger
    }
    
    public override void OnAfterInsert(Map<Id, SObject> insertObject)
    {
        list<event_form__c> addForms = new list<event_form__c>();
        set<Id> eventTypeIds = new set<Id>();
        for(Event__c evt : (list<event__c>)insertobject.Values())
        {
            eventTypeIds.add(evt.event_type__c);  
        }
        list<event_type_survey__c> surveys = [select event_type__c, survey__c, survey__r.name from event_type_survey__c where survey__r.active__c = true and event_type__c in :eventTypeIds];
        /*
        Map<Id,List<event_type_survey__c>> eventIDTypeSurveyMap = new  Map<Id,List<event_type_survey__c>>();

        for(event_type_survey__c ets : surveys)
        {
           if(eventIDTypeSurveyMap.containsKey(ets.Event_Type__c))
            {
                eventIDTypeSurveyMap.get(ets.Event_Type__c).add(ets); 
            }
            else{
                eventIDTypeSurveyMap.put(ets.Event_Type__c,new List <event_type_survey__c> {ets});
            }
        }
        */
        for (Event__c procEvent: ((map<Id, Event__c>)insertObject).Values()) 
        {
            if(procEvent.Physician__c !=null)
            {
                // 3/30/17 - removed I don't think we are using these anymore! - RS
                // PatientSharingController.addPatientPhysicanSharingRules(procEvent.Id);
            }
            if(procEvent.Location__c !=null)
            {
                // 3/30/17 - removed I don't think we are using these anymore! - RS
                // PatientSharingController.addPatientHospSharingRules(procEvent.Id);
            }

            //Create Event Forms from existing Surveys for Event Type
            for(event_type_survey__c survey : surveys) //eventIDTypeSurveyMap.get(procEvent.Event_Type__c)
            {
                if(survey.Event_Type__c == procEvent.Event_Type__c)
                {
                    event_form__c newForm = new event_form__c();
                    newForm.Event__c = procEvent.Id;
                    newForm.Name = survey.survey__r.Name;
                    newForm.Survey__c = survey.survey__c;
                    addForms.add(newForm);
                }
            }
        }

        logTrigger('Create', insertObject, null, null); 
        
        insert addForms;
    }

    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
        //Not Called for this Trigger
    }

    public override void OnAfterUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
        List<Case__c> casesToBeUpdated = new List<Case__c>();
            
        for (Event__c procEvent:((map<Id, Event__c>)afterObject).Values()) 
        {
            if(procEvent.Physician__c !=null && procEvent.Physician__c <> ((Map<Id, event__c>)beforeObject).get(procEvent.Id).Physician__c)
            {
                PatientSharingController.addPatientPhysicanSharingRules(procEvent.Id);
                PatientSharingController.deletePatientSharingRules(((Map<Id, event__c>)beforeObject).get(procEvent.Id).Physician__c, procEvent.Patient__c);
                PatientSharingController.deleteCaseSharingRules(((Map<Id, event__c>)beforeObject).get(procEvent.Id).Physician__c, procEvent.Case__c);
            }
            if(procEvent.Location__c !=null && procEvent.Location__c <> ((Map<Id, event__c>)beforeObject).get(procEvent.Id).Location__c)
            {
                // PatientSharingController.addPatientHospSharingRules(procEvent.Id);
                PatientSharingController.deletePatientSharingRules(((Map<Id, event__c>)beforeObject).get(procEvent.Id).Location__c, procEvent.Patient__c);
                PatientSharingController.deleteCaseSharingRules(((Map<Id, event__c>)beforeObject).get(procEvent.Id).Location__c, procEvent.Case__c);
            }
            if(procEvent.Event_Type_Name__c == 'Procedure' && procEvent.Appointment_Start__c  <> ((Map<Id, event__c>)beforeObject).get(procEvent.Id).Appointment_Start__c)
            {
                Case__c parentCase = new Case__c(id=procEvent.Case__c);
                parentCase.Procedure_Date__c = date.newInstance(procEvent.Appointment_Start__c.year(),procEvent.Appointment_Start__c.month(),procEvent.Appointment_Start__c.day());
                casesToBeUpdated.add(parentCase);
            }
        }
        if(!casesToBeUpdated.isEmpty())
        	update casesToBeUpdated;
        
        logTrigger('Update', beforeObject, afterObject, null);
    }

    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
        //Not Called for this Trigger
    }
}