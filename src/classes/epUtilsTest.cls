/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class epUtilsTest{
	static testMethod void test_GetMapOfObjectPrefixesToObjectName(){
		map<String,String> testMap = epUtils.GetMapOfObjectPrefixesToObjectName();
		system.assert(testMap.Size() > 0);
	}
	
    static testMethod void test_GetRecordTypeIdByRecordTypeName()
    {
		list<RecordType> types = [Select
									  r.SobjectType
									, r.NamespacePrefix
									, r.Name
									, r.IsActive
									, r.Id
									, r.DeveloperName
									, r.Description
									, r.BusinessProcessId
								From
									RecordType r
								WHERE
									r.IsActive = true];
        
        RecordType rt;
        if(epUtils.ValidateList(types))
        {
        	rt = types[0];
        }

    	system.assert(rt.Id != null);
    	
    	String returnedId = epUtils.GetRecordTypeIdByRecordTypeName(rt.Name);
    	
    	system.assert(rt.Id == returnedId);
   	
    }

    static testMethod void test_GetRecordTypeIdBySobjectTypeAndRecordTypeName()
    {
		list<RecordType> types = [Select
									  r.SobjectType
									, r.NamespacePrefix
									, r.Name
									, r.IsActive
									, r.Id
									, r.DeveloperName
									, r.Description
									, r.BusinessProcessId
								From
									RecordType r
								WHERE
									r.IsActive = true];
        
        RecordType rt;
        if(epUtils.ValidateList(types))
        {
        	rt = types[0];
        }

    	system.assert(rt.Id != null);
    	
    	String returnedId = epUtils.GetRecordTypeIdBySobjectTypeAndRecordTypeName(rt.SobjectType, rt.Name);
    	
    	system.assert(rt.Id == returnedId);
   	
    }

    //static testMethod void test_ValidateSaveresultListPositive()
    //{
    //    user_login_settings__c uls = new user_login_settings__c();
    //    uls.Name = 'Default';
    //  	uls.HL7_API_Key__c = 'D6H8smtD4AvmbV2SPHdp';  
    //  	uls.HL7_Endpoint_String__c = 'http://api.notonlydev.com/api/index.php?';
    //    uls.Logins_Per_Month_Threshold__c = 3;
    //    uls.License_Count_Notification_Address__c = 'sswiger@enablepath.com';
    //    uls.Control_Center_Admin__c = userInfo.getUserId();
    //    insert uls;

    //    Contact cont = new Contact
    //    (
    //         MailingStreet        = 'Test Street'
    //        ,MailingCity          = 'Test City'
    //        ,Email                = 'unamefortesting@enablepath.com'
    //        ,FirstName            = 'Test_FirstName'
    //        ,LastName             = 'Test_Lastname'
    //        ,Phone                = '7045551212'
    //        ,OtherPhone           = '7045551212'
    //        ,MailingState         = 'FL'
    //        ,MailingPostalCode    = '12345'
    //    );

    //    list<Contact> Contacts = new list<Contact>();
    //    Contacts.add(cont);
    //    list<Database.Saveresult> Results = Database.insert(Contacts);
    //	system.assert(epUtils.ValidateSaveresultList(Results) == true);
   	
    //}

    //static testMethod void test_ValidateMapPositive()
    //{
    //		user_login_settings__c uls = new user_login_settings__c();
    //		uls.Name = 'Default';
    //  	uls.HL7_API_Key__c = 'D6H8smtD4AvmbV2SPHdp';  
    //  	uls.HL7_Endpoint_String__c = 'http://api.notonlydev.com/api/index.php?';
    //		uls.Logins_Per_Month_Threshold__c = 3;
    //		uls.License_Count_Notification_Address__c = 'sswiger@enablepath.com';
    //		uls.Control_Center_Admin__c = userInfo.getUserId();
    //		insert uls;
        
    //    Contact cont = new Contact
    //    (
    //         MailingStreet        = 'Test Street'
    //        ,MailingCity          = 'Test City'
    //        ,Email                = 'unamefortesting@enablepath.com'
    //        ,FirstName            = 'Test_FirstName'
    //        ,LastName             = 'Test_Lastname'
    //        ,Phone                = '7045551212'
    //        ,OtherPhone           = '7045551212'
    //        ,MailingState         = 'FL'
    //        ,MailingPostalCode    = '12345'
    //    );
    //    insert cont;
    //	map<Id, Contact> myMap = new map<Id, Contact>();
    //	myMap.put(cont.Id, cont);
    //	system.assert(epUtils.ValidateMap(myMap) == true);
   	
    //}
    
    static testMethod void test_ValidateMapNegativeNull()
    {
    	map<Id, Contact> myMap;
    	system.assert(epUtils.ValidateMap(myMap) == false);
    }
    
    static testMethod void test_ValidateMapNegativeEmpty()
    {
    	map<Id, Contact> myMap = new map<Id, Contact>();
    	system.assert(epUtils.ValidateMap(myMap) == false);
    }
    
    //static testMethod void test_ExistsInListNegative()
    //{
    //    Contact cont = new Contact
    //    (
    //         MailingStreet        = 'Test Street'
    //        ,MailingCity          = 'Test City'
    //        ,Email                = 'unamefortesting@enablepath.com'
    //        ,FirstName            = 'Test_FirstName'
    //        ,LastName             = 'Test_Lastname'
    //        ,Phone                = '7045551212'
    //        ,OtherPhone           = '7045551212'
    //        ,MailingState         = 'FL'
    //        ,MailingPostalCode    = '12345'
    //    );
    //    list<Contact> Contacts = new list<Contact>();
    //    system.assert(epUtils.ExistsInList(cont, Contacts) == false);
    //}

    //static testMethod void test_ExistsInListPositive()
    //{
    //    Contact cont = new Contact
    //    (
    //         MailingStreet        = 'Test Street'
    //        ,MailingCity          = 'Test City'
    //        ,Email                = 'unamefortesting@enablepath.com'
    //        ,FirstName            = 'Test_FirstName'
    //        ,LastName             = 'Test_Lastname'
    //        ,Phone                = '7045551212'
    //        ,OtherPhone           = '7045551212'
    //        ,MailingState         = 'FL'
    //        ,MailingPostalCode    = '12345'
    //    );
    //    list<Contact> Contacts = new list<Contact>();
    //    Contacts.add(cont);
    //    system.assert(epUtils.ExistsInList(cont, Contacts) == true);
    //}
    
    static testMethod void test_MultipleFunctions()
    {
        string pSeparatorChar = '|';
        list<account> testAccounts = new list<account>();
        Account testObject1 = new Account();
        testObject1.Name = 'Test Account 1';
		system.assert(epUtils.ExistsInList(testObject1, testAccounts) == false);
        testAccounts.add(testObject1);
        
        Account testObject2 = new Account();
        testObject2.Name = 'Test Account 2';
        testAccounts.add(testObject2);

        Account testObject3 = new Account();
        testObject3.Name = 'Test Account 3';
        testAccounts.add(testObject3);
        
        list<Database.Saveresult> Results = Database.insert(testAccounts);
    	system.assert(epUtils.ValidateSaveresultList(Results) == true);
        
    	string test1 = epUtils.BuildStringOfIDsForInClause(testAccounts, pSeparatorChar);
        
    	map<Id, account> myMap = new map<Id, account>();
        for(account a : testAccounts)
        {
        	myMap.put(a.id, a);    
        }
        
        string test2 = epUtils.BuildStringOfIDsForInClause(myMap.keyset(), pSeparatorChar);
        system.assert(epUtils.ExistsInList(testObject2, testAccounts) == true);
    	system.assert(epUtils.ValidateMap(myMap) == true);
        system.assert(epUtils.ValidateList(testAccounts) == true);
        string soqlString = epUtils.getSelect(testObject3.getsObjectType());
    }

    //static testMethod void test_ValidateSobjectListPositive()
    //{
    //    Contact cont = new Contact
    //    (
    //         MailingStreet        = 'Test Street'
    //        ,MailingCity          = 'Test City'
    //        ,Email                = 'unamefortesting@enablepath.com'
    //        ,FirstName            = 'Test_FirstName'
    //        ,LastName             = 'Test_Lastname'
    //        ,Phone                = '7045551212'
    //        ,OtherPhone           = '7045551212'
    //        ,MailingState         = 'FL'
    //        ,MailingPostalCode    = '12345'
    //    );
    //    list<Contact> Contacts = new list<Contact>();
    //    Contacts.add(cont);
    //    system.assert(epUtils.ValidateList(Contacts) == true);
    //}

    static testMethod void test_ValidateSobjectListNegative()
    {
        list<Contact> Contacts = new list<Contact>();
        system.assert(epUtils.ValidateList(Contacts) == false);
    }

    static testMethod void test_ValidateObjectList()
    {
        list<Integer> ints = new list<Integer>{1,2,3};
        system.assert(epUtils.ValidateList(ints));
    }

    static testMethod void test_StringGetValueOrDefault1()
    {
        String one = '';
        String defaultVal = 'DEFAULT';
        system.assert(epUtils.StringGetValueOrDefault(one, defaultVal) == 'DEFAULT');
    }

    static testMethod void test_StringGetValueOrDefault2()
    {
        String one = 'TEST';
        String defaultVal = 'DEFAULT';
        system.assert(epUtils.StringGetValueOrDefault(one, defaultVal) == 'TEST');
    }
    
    //static testMethod void test_DateConversions()
    //{
    //    Date testDate = date.newInstance(2014, 3, 25);
    //    integer jDate = epUtils.GetDateJulianForGregorian(testDate);
    //    Date gDate = epUtils.GetDateGregorianForJulian(jDate);
    //}
    
    static testMethod void test_sObjectNames()
    {
		List<String> testList = epUtils.getSObjectNames('account');
    }
}