public with sharing class SCMInvoiceControllerExtension {

    private SCMC.InvoiceController controller = null;
    public SCMC__Invoicing__c invExtended { get; private set; }

    public SCMInvoiceControllerExtension(SCMC.InvoiceController baseController){
        this.controller = baseController;
    }

	public string invIdExtend { 
		get;
		set {
			invIdExtend = value;
			Init(invIdExtend);
		}
	}

    public void Init(string Id){
        this.invExtended = [select Id
        						  ,SCMC__Sales_Order__r.SCMC__Customer_Purchase_Order__c
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__c
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.Name
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.ShippingStreet
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.ShippingCity
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.ShippingState
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.ShippingPostalCode
                                  ,SCMC__Sales_Order__r.SCMC__Customer_Account__r.ShippingCountry
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.Name
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street_Additional_Information__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_City__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_State_Province__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Zip_Postal_Code__c
                                  ,SCMC__Sales_Order__r.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Country__c
                                  ,SCMC__Shipping__r.SCMC__Carrier_Service__c
                                  ,SCMC__Payment_Terms_Name__c
                              from SCMC__Invoicing__c
                             where Id = :Id];

        
    }

}