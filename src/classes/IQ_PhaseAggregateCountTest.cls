@isTest(SeeAllData=true)
public class IQ_PhaseAggregateCountTest {

    @isTest static void SetDatesTest() {
        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        //agg.SetDates();
        System.debug(agg.monthsToAnalyze);
        // Date startDate = Date.today().toStartofMonth().AddMonths(-2); 
        System.Assert(agg.monthsToAnalyze > 1);
    }

    @isTest static void GetMappedSurgeonPracticesTest() {
        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        
        Map<String, Surgeon_Hospital__c> surgeons = agg.GetMappedSurgeonPractices();
        System.debug(surgeons);
        
        System.Assert(true);
    }

    @isTest static void GetMonthlyCasesTest() {
        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        //agg.SetDates();
        
        List<Event__c> events = agg.GetMonthlyCases(Date.newInstance(2017, 4, 1));
        System.debug(events);
        // TO DO: make this a test that will pass 
        // System.Assert(events.size() == 0);
    }

    @isTest static void GetMonthlyPhaseCountTest() {
       	Account surgeon = CreateSurgeonAccount();

        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        //agg.SetDates();
		//insert records
        IQ_Phase_Count__c phase = new IQ_Phase_Count__c();
        phase.Name = 'Preop';
        phase.Month__c = 11;
        phase.Year__c =  2016;
        phase.Surgeon_ID__c = surgeon.Id;
        phase.Count__c = 10;
        insert phase;

        List<IQ_Phase_Count__c> phaseCounts = agg.GetMonthlyPhaseCounts();
        System.debug(phaseCounts);
        System.Assert(phaseCounts.size() > 0);
    }

    @isTest static void CreateOrAddToListRecordsTest() {
    	
       	Account surgeon = CreateSurgeonAccount();
     	
     	Test.startTest();
 		IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
    	agg.monthlyCountList = new List<IQ_Phase_Count__c>();
    	
    	Event__c evnt = CreateEvent();
    	
    	agg.CreateOrAddToListRecords(evnt, 'PreOp', Date.newInstance(2016,5,1));
    	System.debug(agg.monthlyCountList);
    	System.assertEquals(agg.monthlyCountList.size(), 1);
    }

	@isTest static void CreateOrAddToListRecordsTest2() {
    	
       	Account surgeon = CreateSurgeonAccount();
     	
     	Test.startTest();
 		IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
    	agg.monthlyCountList = new List<IQ_Phase_Count__c>();
    	
    	Event__c evnt = CreateEvent();
    		
    	agg.CreateOrAddToListRecords(evnt, 'PreOp', Date.newInstance(2016,5,1));
    	
    	evnt.Appointment_Start__c = Date.today().addDays(1);	
    	agg.CreateOrAddToListRecords(evnt, 'PreOp', Date.newInstance(2016,5,1));
    	System.debug(agg.monthlyCountList);
    	System.assertEquals(agg.monthlyCountList.size(), 1);
    	System.assertEquals(agg.monthlyCountList[0].Count__c, 2);

    }

    @isTest static void PhaseCountAggregatorBySurgeonTest() {
    	Test.startTest();
 		IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
    	agg.PhaseCountAggregatorBySurgeon();
    	System.Assert(agg.startDate!= null);
    	System.Assert(agg.endDate!= null);
    	Test.stopTest();
    }

     @isTest static void PhaseCountAggregatorBySurgeonTest1() {
    	Test.startTest();
 		IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
    	agg.PhaseCountAggregatorBySurgeon();
    	System.Assert(agg.startDate!= null);
    	System.Assert(agg.endDate!= null);
    }

    @isTest static void ProcessEvents() {
    	Test.startTest();
 		IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
 		List<Event__c> events = new List<Event__c>();
 		events.add(CreateEvent());
 		agg.ProcessEvents(events);
 		System.Assert(true);
 		Test.stopTest();
    }

    public static Account CreateSurgeonAccount() {
    	Account surgeon = new Account();
        surgeon.FirstName = 'Surgeon';
        surgeon.LastName = 'Tester';
        surgeon.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        insert surgeon;
        return surgeon;
    }

    private static Event__c CreateEvent() {
		Account surgeon = CreateSurgeonAccount();
		List<Account> hospitals = IQ_PatientActions.LoadHospitals();
		Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = hospitals.get(0).Id);
        insert hospitalCaseType;            
        List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(hospitals.get(0).Id);
        List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(hospitals.get(0).Id);     
        Patient__c patient = IQ_PatientActions.CreateNewPatient(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'M', 'SSN','P1234','A1234','White','English', 'test@test.com');
        Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Left');
            
    	Event__c evnt = new Event__c();
    	
    	// evnt.Event_Type_Name__c = 'Procedure';
    	evnt.Appointment_Start__c = Date.today(); 
    	evnt.Physician__c = surgeon.Id;
    	evnt.Status__c = 'Complete'; 
		evnt.Case__c = patientCase.Id;
		insert evnt;
		return evnt;
    }
}