/*
    Copyright (c) 2013 LessSoftware.com, inc.
    All rights reserved.

	Create sales invoices for recurring billing

*/
global with sharing class SCMScheduledBilling implements  Schedulable, Database.Batchable<SObject>{

	global Integer limRecords{get; set;}

	/*
	 * start a batch process to create the sales invoices
	 */
	global void execute(SchedulableContext ctx)
	{
		// Start Batch Apex job to create sales invoice in FinancialForce
		Database.executeBatch(new SCMScheduledBilling(),50);
	}
	/*
	 *  create sales invoices from list and reset next date
	 */
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        //process all invoices that have been selected

        Date iDate = System.Today();
        String iday = '-';
        if (iDate.day() >= 10){
        	iday += iDate.day();
        } else {
        	iday += '0' + iDate.day();
        }
        String imonth = '-';
        if (iDate.Month() >= 10){
        	imonth += iDate.Month();
        }else {
        	imonth += '0' + iDate.Month();
        }
        String invdate = iDate.year() + imonth + iday;

        String query = 'Select id from SCMC__Service_Order__c ';
        query += 'where RecordType.DeveloperName = \'Activated\'';
        query += ' and SCMC__Next_Invoice_Date__c <= ' + invdate;
        if (limRecords != null){
        	query += ' limit ' + limRecords;
        }
        System.debug('query for billing ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext ctx, List<SObject> records) {
    	//invoke plugin to create invoice
 		SCMC.SCMPlugin plug = null;
 		boolean failure = false;
		ID[] updRecId = new ID[]{};
		Map<ID, SCMC__Service_Order__c> scs = new Map<ID, SCMC__Service_Order__c>();
		for (SObject record : records){
			scs.put(record.id, (SCMC__Service_Order__c)record);
		}
		System.Savepoint sp = Database.setSavepoint();
		try {

			SCMC__SCM_Plugins__c plugins = SCMC__SCM_Plugins__c.getInstance();
			if (plugins != null) {
				string recurInvoicePlugin = plugins.SCMC__Recur_Invoice_Plugin__c;
				system.debug('+++plugin name:' + recurInvoicePlugin);
				if (recurInvoicePlugin == null){
					recurInvoicePlugin = 'SCMRecureInvoice';
				}
				string namespace = '';
				string className = '';

				if (recurInvoicePlugin.indexOf('.') == -1) {
					className = recurInvoicePlugin;
				} else {
					string[] pluginClassSplit = recurInvoicePlugin.split('.', 2);
					namespace = pluginClassSplit[0];
					className = pluginClassSplit[1];
				}

				Type t = Type.forName(namespace, className);
				if (t == null) {
					SCMC.ErrorLog.log('SCMRecurInvoice', 'SCM Plugin class not found.', null, 'Plugin: ' + recurInvoicePlugin);
					throw new SCMException('SCM Plugin class for recurring billing not found');
				} else {
					plug = (SCMC.SCMPlugin)t.newInstance();
					//create the invocies
					updRecId.addAll(scs.Keyset());
					//create all the invoices
					plug.execute(updRecId);
				}
			}

		} catch (Exception ex) {
			failure = true;
			System.debug('SCM Plugin Failure ' + ex.getMessage());
			SCMC.ErrorLog.log('SCMScheduledBilling.execute', 'SCM Plugin failure', ex);
		}

		if (failure){
			if (plug == null || (plug !=null && plug.doRollback)) {
				Database.rollback(sp);
			}

			SCMC.ErrorLog.flush();
		} else {
			//perform group finish activity here
			//the following is performed in the plugin
				//Service Term Detail indicates period until next invoice
				//if rev recognition is involved, then number of periods is
				//defined in service detail

				//update remaining periods
		}

    }
    global void finish(Database.BatchableContext ctx){

    }
}