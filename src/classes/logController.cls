/*****************************************************
This class is designed to provide log viewing 
functionality as part of the audit logging for 
OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public class logController
{
    public list<Audit_Log__c> logs { get; set; }
    public string filterUser { get; set; }
    public string filterObjectType { get; set; }
    public DateTime filterStartDate { get; set; }
    public DateTime filterEndDate { get; set; }
    //public Audit_Log__c log { get; set; }
    
    public logController()
    {
        //initialize filter properties
    	filterUser = '';
        filterObjectType = '';
    }
    
    public list<Audit_Log__c> getAuditLogs()
    {
        string sqlStr = 'Select	Id, Date__c, S_Object_Type__c, User__c, User__r.Name, Parent_Record_Id__c, Parent_Record_Name__c';
        sqlStr += ' From Audit_Log__c';
        //Build Where if filters are selected
        if(filterUser != '' || filterObjectType != '' || filterStartDate != null || filterEndDate != null)
        {
            sqlStr += ' Where';
        	string whereStr = '';   
            if(filterUser != '')
                whereStr += ' User__c = \'' + filterUser + '\' and';
            if(filterObjectType != '')
                whereStr += ' S_Object_Type__c = \'' + filterObjectType + '\' and';
            if(filterStartDate != null)
                whereStr += ' Date__c >= ' + filterStartDate.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' and';
            if(filterEndDate != null)
                whereStr += ' Date__c <= ' + filterEndDate.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' and';
            sqlStr += whereStr.substring(0, whereStr.lastIndexOf(' and'));
        }
        sqlStr += ' Order by Date__c desc';
        system.debug(sqlstr);
        logs = Database.query(sqlStr);
        return logs;
    }
}