/*
    Copyright (c) 2013 LessSoftware.com, inc.
    All rights reserved.
    
	class to support recurring billing functions
*/
global with sharing class SCMBilling {
	
	//map service order id to SCM Invoice
	global Map<ID,SCMC__Invoicing__c>invoices {get; set;}
	//map service order id to SCM Invoice lines
	global Map<ID, List<SCMC__Invoice_Line_Item__c>>lines {get; set;}
	
	global ApexPages.StandardSetController controller {get; set;}
	private Static final Schema.DescribeSObjectResult describe = Schema.SObjectType.SCMC__Service_Order__c;
	private String keyPrefix;
	
	global SCMBilling() {
		
		invoices = new Map<ID, SCMC__Invoicing__c>();
		lines = new Map<ID, List<SCMC__Invoice_Line_Item__c>>();
		
	}
	global SCMBilling(ApexPages.StandardSetController controller){
		
		this.controller = controller;
		invoices = new Map<ID, SCMC__Invoicing__c>();
		lines = new Map<ID, List<SCMC__Invoice_Line_Item__c>>();
		keyPrefix = describe.getKeyPrefix();
		
	}
	/*
	 *  Update the service order to the current billing and the next billing date
	 */
	global void updateDates(SCMC__Service_Order__c order){
		
		//update last invoiced date to today
		//calculate the next invoice date based on term and today's date
		//update the remaining term
		
		order.SCMC__Last_Invoice_Date__c = calcInvoiceDate(order);
		order.SCMC__Billed_Through__c = order.SCMC__Last_Invoice_Date__c;
		Integer days = null;
		Integer months = null;
		if (order.SCMC__Service_Term_Record__r.SCMC__Number_Of_Days__c != null){
			days = order.SCMC__Service_Term_Record__r.SCMC__Number_Of_Days__c.intValue();
		}
		if (order.SCMC__Service_Term_Record__r.SCMC__Number_Of_Months__c != null){
			months = order.SCMC__Service_Term_Record__r.SCMC__Number_Of_Months__c.intValue();
		}
		if (days != null &&
			days != 0){
			order.SCMC__Next_Invoice_Date__c = order.SCMC__Next_Invoice_Date__c.addDays(days);
			order.SCMC__Billed_Through__c = order.SCMC__Billed_Through__c.addDays(days);
		} else if (months != null &&
			months != 0){
			order.SCMC__Next_Invoice_Date__c = order.SCMC__Next_Invoice_Date__c.addMonths(months);
			order.SCMC__Billed_Through__c = order.SCMC__Billed_Through__c.addMonths(months);
		} else {
			System.debug('There is no valid terms definition to update service order.');
			throw new SCMException('There is no valid terms definition to update service order.');
		}
		order.SCMC__Remain_Service_Term_Number__c -= 1;
		order.SCMC__Last_Invoice_Generation__c = System.today();
		order.SCMC__Billed_Through__c = order.SCMC__Billed_Through__c.addDays(-1);
		system.debug('+++++ final updateDates: ');
		return ;
	}
	private Date calcInvoiceDate(SCMC__Service_Order__c order){
		Date retDate = System.Today();
		System.debug('order to update ' + order);
		if (order.SCMC__Invoice_Date_Option__c == 'Start of next month'){
			
			retDate =
				retDate.toStartOfMonth().addMonths(1);
			System.debug('ret date next' + retDate);
			
		} else if (order.SCMC__Invoice_Date_Option__c == 'Start of current month'){
			
			retDate =retDate.toStartOfMonth();
			System.debug('ret date current ' + retDate);
			
		}else if (order.SCMC__Invoice_Date_Option__c == 'End of current month'){
			
			retDate = retDate.toStartOfMonth().addMonths(1).addDays(-1);
			System.debug('ret date current end ' + retDate);
		}
		return retDate;
	}
	/*
	 *	Create an SCM Invoice from a service order
	 
	 */
	global void createSCMInvoice(SCMC__Service_Order__c order){
		system.debug('+++++ inside createSCMInvoice: ');
		Date basisDate = calcInvoiceDate(order);
		Integer days = 0;
		if (order.SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__r.SCMC__Number_of_Days__c != null){
			days = order.SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__r.SCMC__Number_of_Days__c.intValue();
		}
		
		SCMC__Invoicing__c invoice = new SCMC__Invoicing__c (
			SCMC__Service_Contract__c = order.id
			//set this based on payment terms on account
			, SCMC__Invoice_Due_Date__c = basisDate.addDays(days)
			//set this based on setting in service order
			, SCMC__Invoice_Date__c = basisDate
			//, SCMC__Customer_Purchase_Order_Number__c = order.SCMC__Original_Customer_Purchase_Order__c
			,SCMC__Payment_Terms__c = order.SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__c
			,SCMC__Sales_Order__c = order.SCMC__Sales_Order__c
			);	
		invoices.put(order.id, invoice);
		System.debug('number of invoice lines ' + order.SCMC__Service_Order_Lines__r.Size());
		SCMC__Invoice_Line_Item__c tline = new SCMC__Invoice_Line_Item__c();
		ID rType = SCM_Utilities.getRecordType('Item', tline);
		for (SCMC__Service_Order_Line__c line : order.SCMC__Service_Order_Lines__r) {
			List<SCMC__Invoice_Line_Item__c> llist = lines.get(order.id);
			if (llist == null){
				llist = new List<SCMC__Invoice_Line_Item__c>();
				lines.put(order.id, llist);
			}
			SCMC__Invoice_Line_Item__c iline = new SCMC__Invoice_Line_Item__c (
				SCMC__Service_Contract_Line__c = line.id
				,SCMC__Item__c = line.SCMC__Item_Number__c
				,SCMC__Quantity__c = line.SCMC__Quantity__c
				,SCMC__Amount__c = line.SCMC__RC_Base__c 
				,SCMC__Sales_Order_Line_Item__c = line.SCMC__Sales_Order_Line_Item__c
				//,SCMC__Delivery_Method__c = line.SCMC__Delivery_Method__c
				,RecordTypeId = rType);
			llist.add(iline);
		
		}
		System.debug('number of lines ' + lines.Size());
	}
	/*
	 * Save the generated invoices
	 */
	global void save () {
		List<SCMC__Invoice_Line_Item__c>insLines = new List<SCMC__Invoice_Line_Item__c>();
		System.debug('+++++ invoices ' + invoices.Values().Size());
		if (invoices.Values().Size() > 0) {
			insert invoices.Values();
			for (ID scId : lines.Keyset()){
				SCMC__Invoicing__c invoice = invoices.get(scId);
				if (invoice != null){
					List<SCMC__Invoice_Line_Item__c> scLines = lines.get(scId);
					if (scLines != null){
						for (SCMC__Invoice_Line_Item__c scLine : scLines){
							scLine.SCMC__Invoicing__c = invoice.id;
						}
						insLines.addAll(scLines);
					}
				}
			}
			System.debug('+++++ insLines ' + insLines.Size());
			insert insLines;
		}
	}
	
	global PageReference goBack() {
		PageReference retPage = null;

		retPage = new PageReference('/' + keyPrefix + '/o');
		return retPage;
    }

	global PageReference runBillEngine() {
		SCMScheduledBilling sBilling = new SCMScheduledBilling();
		if (Test.isRunningTest()){
			sBilling.limRecords = 1;
		}
		Database.executeBatch(sBilling,50);
		return goBack();	
	}
	
}