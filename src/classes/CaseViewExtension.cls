public with sharing class CaseViewExtension {
     public final Case__c currCase;
     public List<Event__c> currEvents {get; set;}
     public boolean renderEditView {get; set;}
	public String procedureURL {get; set;}
     osController baseCtrl;
	public Map<Id,String> eventURLMap {get; set;}

     // The extension constructor initializes the private member
     // variable currCase by using the getRecord method from the standard
     // controller.
     public CaseViewExtension(ApexPages.StandardController stdController) {
          baseCtrl = new osController(stdController);
          this.currCase = (Case__c)stdController.getRecord();
          renderEditView = false;
          eventURLMap = new Map<Id,String>();
     }

	public void LogConstructor()
     {
          baseCtrl.LogView();
          getEvents();
     }

     public void getEvents () {
		map<integer, string> params = new map<integer, string>();
		params.put(0,currCase.Name);
          currEvents = baseCtrl.getSelectedData('CaseViewEvent', params);

		for(Event__c e : currEvents)
		{
			if(e.Event_Type__r.Name.equalsIgnoreCase('Procedure')) eventURLMap.put(e.id, '/apex/ProcedureSummary?procid=' + e.id);
			else eventURLMap.put(e.id, '/apex/EventView?id=' + e.id);
		}
     }

     public PageReference toggleEditView() {
          renderEditView = true;
          return null;
     }

     public void Save(){
          renderEditView = false;
          update currCase;
     }

     public PageReference cancelEdit() {
          renderEditView = false;
          return null;
     }
}