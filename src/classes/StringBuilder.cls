//@author : EnablePath
//@date : 12/18/2013
//@description : class to perform non-standard string operations
public with sharing class StringBuilder
{
    static final String CLASSNAME = 'StringBuilder.METHODNAME()';
    
    private String[] arr;
    private list<String> stringList = new list<String>();

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : private class constructor
    private void StringBuilder()
    {
        final string METHODNAME = CLASSNAME.replace('.METHODNAME()',' - constructor');
    }

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to convert collection of Strings to one (1) single String
    //@paramaters : none
    //@returns : a String representing ...
    public String AsString()
    {
        // cannot override ToString() method so method is "AsString" instead
        final string METHODNAME = CLASSNAME.replace('METHODNAME','AsString');
        
        String a = '';
        if(this.stringList.size() > 0)
        {
            for(String s : this.stringList)
            { a += s; }
        }
        return a;
    }

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : none
    //@returns : nothing
    public void Clear()
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','Clear');
        stringList.clear();
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to add a String to the collection
    //@paramaters : a String to add to the collection
    //@returns : nothing
    public void Append(String pStringToAdd)
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','Append');
        //system.debug(LoggingLevel.INFO, METHODNAME.replace('**** ', '**** Inside ') + ' :: String to add = ' + pStringToAdd +'\n\n');
        stringList.add(pStringToAdd);
    }

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to add a String to the object's collection in addition to the "new line" characters
    //@paramaters : a String to add to the collection
    //@returns : nothing
    public void AppendLine(String pStringToAdd)
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','AppendLine');
        Append(pStringToAdd + '\r\n');
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to instantiate a new CT_StringBuilder object
    //@paramaters : none
    //@returns : new instance of CT_StringBuilder class
    public static StringBuilder NewStringBuilder()
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','NewStringBuilder');

        StringBuilder sb = new StringBuilder();
        return sb;
    }
        
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to instantiate a new StringBuilder object AND populate its String collection with an initial value
    //@paramaters : a String to add to the collection
    //@returns : new instance of StringBuilder class with an initial value in its String collection
    public static StringBuilder NewWithFirstValue(String pValue)
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','NewWithFirstValue');
        StringBuilder sb = new StringBuilder();
        sb.Append(pValue);
        return sb;
    }
}