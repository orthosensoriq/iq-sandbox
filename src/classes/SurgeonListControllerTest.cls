@isTest
private class SurgeonListControllerTest {
	
	static testMethod void testSurgeonList() {

		//Construct controller
        SurgeonListController con = new SurgeonListController();

        //Execute code
        List<Account> surgeonList = con.getSurgeons();
        PageReference pageRef = con.goToSurgeonView();
	}
	
}