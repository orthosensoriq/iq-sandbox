@isTest
private class SCM_GLExportTest {
	
	private static SCM_SetupExportTest st = new SCM_SetupExportTest();

	@isTest static void testGLExport() {
		SCMC__Inventory_Transaction_Financial_Records__c[] itfrs = new SCMC__Inventory_Transaction_Financial_Records__c[]{};
        
        SCMC__Inventory_Transaction_Financial_Records__c fr = new SCMC__Inventory_Transaction_Financial_Records__c();
        fr.SCMC__GL_Account__c = 'Inventory';
        fr.SCMC__Quantity__c = 2;
        fr.SCMC__ICP_Value_Amount__c = 10;
        itfrs.add(fr);
        SCMC__Inventory_Transaction_Financial_Records__c fr2 = new SCMC__Inventory_Transaction_Financial_Records__c();
        fr2.SCMC__GL_Account__c = 'AP Liability';
        fr2.SCMC__Quantity__c = 2;
        fr2.SCMC__ICP_Value_Amount__c = -10;
        itfrs.add(fr2);
        
        insert itfrs;
        
        test.startTest();
        SCM_GLExport.ExportedGLTransaction[] transactions = SCM_GLExport.getGLTransactions();

		Id[] recIds = new Id[]{};
		for (SCM_GLExport.ExportedGLTransaction trans : transactions) {
			recIds.add(trans.transId);
			trans.status = 'Exported';
			trans.message = '';
		}

		SCM_GLExport.flagExported(transactions);

		SCMC__Inventory_Transaction_Financial_Records__c[] refreshedTransactions = [select Id, SCMC__Export_Status__c 
			from SCMC__Inventory_Transaction_Financial_Records__c 
			where Id in :recIds];
		for (SCMC__Inventory_Transaction_Financial_Records__c trans : refreshedTransactions) {
			system.assertEquals('Exported', trans.SCMC__Export_Status__c, 'unexpected export status');
		}
        
        test.stopTest();
	}
	
}