/*****************************************************
This class is designed to provide Reporting audit 
logging for OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public class ReportController //extends osController
{
    public List<Report> Reports { get; set; }
    public String filterReportFolder { get; set; }
    
    public void CreateReport()
    {
        
    }
    
    public void UpdateReport()
    {
        
    }

    public void ViewReport()
    {
        
    }
    
    public void DeleteReport()
    {
        
    }
}