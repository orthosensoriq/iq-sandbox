public without sharing class PatientSharingController {

	public static void addPatientPhysicanSharingRules(String eventId){
        //Get current sharing rules for patients
        List<Patient__Share> patientSharingRules = new List<Patient__Share>();
        List<Case__Share> caseSharingRules = new List<Case__Share>();

        //Get list of event ids, physician ids
        // List<Event__c> eventList = [SELECT Id, Physician__c, Case__r.Patient__c, Case__c FROM Event__c WHERE Id =:eventId];

        //set list of Phys ids
        Set<String> physSet = new Set<String>();
        // for (Event__c docs: eventList)
        // {
        // 	if(docs.Physician__c <> null)
        //     {
        //     	physSet.add(docs.Physician__c);
        //     }
        // }
        // //build out user list
        // list<User> userList = [Select Id, accountId from User where accountId IN :physSet and isActive = true];

        // //map list of all users & account ids

        // Map<Id,Id> usertoAccountMap = new Map<Id,Id>();
		// for(User u:userList){
		// 	usertoAccountMap.put(u.Id, u.AccountId);
		// }


        // for (Event__c assign: eventList)
        // {
        // 	if(assign.Physician__c <> null)
        //     {
        //     	for (User u: userList)
        // 		{
        // 			if(usertoAccountMap.containsKey(assign.Physician__c))
        // 			{
        // 				Patient__Share patientShare = new Patient__Share();
		//                 patientShare.AccessLevel = 'Edit';
		//                 patientShare.UserOrGroupId =  usertoAccountMap.get(assign.Physician__c);
		//                 patientShare.ParentId = assign.Case__r.Patient__c;
		//                 patientSharingRules.add(patientShare);

		//                 Case__Share caseShare = new Case__Share();
		//                 caseShare.AccessLevel = 'Edit';
		//                 caseShare.UserOrGroupId =  usertoAccountMap.get(assign.Physician__c);
		//                 caseShare.ParentId = assign.Case__c;
		//                 caseSharingRules.add(caseShare);
        // 			}
        // 		}

        //     }
        // }

        // insert patientSharingRules;
        // insert caseSharingRules;
	}

	public static void addPatientHospSharingRules(String eventId){
		//Get current sharing rules for patients
        List<Patient__Share> patientSharingRules = new List<Patient__Share>();
        List<Case__Share> caseSharingRules = new List<Case__Share>();

        //Get list of event ids & location/hospitals
        // List<Event__c> eventList = [SELECT Id, Case__r.Patient__c, Location__c, Case__c FROM Event__c WHERE Id =:eventId];

        //set list of location ids
        Set<String> locationSet = new Set<String>();
        // for (Event__c locs: eventList)
        // {
        // 	if(locs.Location__c <> null)
        //     {
        //     	locationSet.add(locs.Location__c);
        //     }
        // }
        //build out user list
        list<User> userList = new list<user>();

        // list<User> hospUsers = [Select Id, accountId, UserRoleId  from User where accountId IN :locationSet and isActive = true];
        Set<id> RoleIds = new Set<id>();
        // for (User u : hospUsers) RoleIds.add(u.UserRoleId);

        // //add manuf reps too
        // /*
        // list<User> repUsers = [Select Id, accountId, UserRoleId  from User where isActive = true AND ContactId IN
		// (Select Manufacturer_Rep__c from Manufacturer_Rep__c where Hospital__c IN :locationSet)];
		// */
		// userList.addAll(hospUsers);
		// //userList.addAll(repUsers);

        // // Get the group IDs associated with the 'UserRoleId' from the User records
        // map<Id,List<Group>> GroupIdsMap = new map<Id,List<Group>>();
        // for (Group gi : [SELECT Id,RelatedId,DeveloperName,Type FROM Group WHERE RelatedId IN :RoleIds AND Type = 'Role']){
        //        List<Group> groupList = GroupIdsMap.get(gi.RelatedId);
        //        if (groupList == null) groupList = new List<Group>();
        //        groupList.add(gi);
        //        GroupIdsMap.put(gi.RelatedId, groupList);
        // }


        // //map list of all users & account ids

        // Map<Id,Id> usertoAccountMap = new Map<Id,Id>();
		// for(User u:userList){
		// 	usertoAccountMap.put(u.Id,u.AccountId);
		// }


		// for (Event__c assign: eventList)
        // {
       	// 	if(assign.Location__c <> null)
        //     {
        //     	for (User u: userList)
        // 		{
        // 			//if(usertoAccountMap.containsKey(assign.Location__c))
        // 			if(u.accountid == assign.Location__c)
        // 			{
        //                  String GroupId = '';
        //                  if (GroupIdsMap.containsKey(u.UserRoleId)) {
        //                       List<Group> GroupIds = GroupIdsMap.get(u.UserRoleId);
        //                       for (Group gir : GroupIds) GroupId = gir.Id;
        //                  }

        // 				Patient__Share patientShare = new Patient__Share();
		//                 patientShare.AccessLevel = 'Edit';
		//                 patientShare.UserOrGroupId = GroupId;//usertoAccountMap.get(assign.Location__c);
		//                 patientShare.ParentId = assign.Case__r.Patient__c;
		//                 patientSharingRules.add(patientShare);

		//                 Case__Share caseShare = new Case__Share();
		//                 caseShare.AccessLevel = 'Edit';
		//                 caseShare.UserOrGroupId =  GroupId;//usertoAccountMap.get(assign.Location__c);
		//                 caseShare.ParentId = assign.Case__c;
		//                 caseSharingRules.add(caseShare);
        // 			}
        // 		}

        //     }
        // }
		// insert patientSharingRules;
		// insert caseSharingRules;
	}

    public static void deletePatientSharingRules(String acctId, String patientId){

        list<User> userList = [Select Id from User where accountId = :acctId and isActive = true];
        Set<String> userRemove = new Set<String>();

        for(user u: userList)
        {
        	userRemove.add(u.Id);
        }

        List<Patient__Share> patientSharingDelete = [SELECT Id FROM Patient__Share WHERE ParentId = :patientId AND UserOrGroupId in :userRemove AND RowCause = 'Manual'];
        delete patientSharingDelete;

    }

     public static void deleteCaseSharingRules(String acctId, String caseId){

        list<User> userList = [Select Id from User where accountId = :acctId and isActive = true];
        Set<String> userRemove = new Set<String>();

        for(user u: userList)
        {
        	userRemove.add(u.Id);
        }

        List<Case__Share> caseSharingDelete = [SELECT Id FROM Case__Share WHERE ParentId = :caseId AND UserOrGroupId in :userRemove AND RowCause = 'Manual'];
        delete caseSharingDelete;
    }

}