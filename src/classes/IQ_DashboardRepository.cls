global without sharing class IQ_DashboardRepository {

    private IQ_PatientActions ctrl;
    
    public IQ_DashboardRepository(IQ_PatientActions controllerParam) {
        ctrl = controllerParam;
    }
    
    @RemoteAction
    global static Integer GetPreopPatientActivity(){
        List<AggregateResult> preops =  [SELECT COUNT(Id) Total
                FROM Event__C
                WHERE Appointment_Start__c > 2016-10-08T01:02:03Z and Event_Type_Name__c = 'Pre-Op'];
        return Integer.valueOf(preops[0].get('Total'));
    }

    @RemoteAction
    global static List<CaseActivity> GetProceduresPerMonthBySurgeon(string surgeonId, string sDate, string edate ){
        Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
        Date endDate = parseDate(eDate);  //  Date.newInstance(2016, 12, 31);   
        System.debug(surgeonId);
        System.debug(startDate);
        System.debug(endDate);

        List<AggregateResult> results = [Select Event_Type__r.Name, 
        calendar_year(Appointment_start__c) Year, 
                calendar_month(Appointment_start__c) Month, 
                COUNT(id) Total
                from Event__c 
                where Physician__c = :surgeonId 
                and Event_Type__r.Name = 'Procedure'
                and Appointment_start__c >= :startDate 
                and Appointment_start__c <= :endDate
                GROUP By Event_Type__r.Name, calendar_year(Appointment_start__c), calendar_month(Appointment_start__c)];//2016-09-08T01:02:03Z];
        System.debug(results);
        List<Surgeon_Hospital__c> surgeons = GetSurgeons();
        List<CaseActivity> caseActs = new List<CaseActivity>();
        for (AggregateResult ar: results) {
            CaseActivity caseAct = new CaseActivity();
            caseAct.PhysicianId = surgeonId;
            // Date procedureDate = (Date)ar.get('Appointment_Start__c');
            caseAct.Month = (Integer)ar.get('Month');
            caseAct.Year = (Integer)ar.get('Year');
            // caseAct.Day = procedureDate.day();
            caseAct.PhysicianId = surgeonId;
            caseAct.PhysicianName = '';
            caseAct.Total = (Decimal)ar.get('Total');
            caseActs.add(caseAct);
        }
        System.debug(caseActs.size());
        System.debug(caseActs);
        return caseActs;
    }

    @RemoteAction
    global static List<CaseActivity> GetProceduresPerMonthByPractice(string practiceId, string sDate, string eDate ){
        Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
        Date endDate = parseDate(eDate);  //  Date.newInstance(2016, 12, 31);   
        System.debug(startDate);
        System.debug(endDate);
       
        // Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
  
        System.debug(endDate);
        List<AggregateResult> results = [Select Event_Type__r.Name, calendar_year(Appointment_start__c) Year, calendar_month(Appointment_start__c) Month, COUNT(id) Total 
                                        from Event__c 
                                        where case__r.Patient__r.Hospital__c = :practiceId 
                                        and
                                        Event_Type__r.Name = 'Procedure'
                                        and 
                                        Appointment_start__c >= :startDate 
                                        and 
                                        Appointment_start__c <= :endDate
                                        GROUP By Event_Type__r.Name, calendar_year(Appointment_start__c), calendar_month(Appointment_start__c)];//2016-09-08T01:02:03Z];
        System.debug(results);
        // List<Surgeon_Hospital__c> surgeons = GetSurgeons();
        List<CaseActivity> caseActs = new List<CaseActivity>();
        Integer monthDiff = startDate.monthsBetween(endDate) + 1; // CalcMonthDifference(startDate, endDate);
        System.debug(monthDiff);
        Integer year;

        Integer surgeonCount = GetSurgeonsForMonth(startDate, practiceId);
        for (Integer i = 0; i < monthDiff; i++) {
            Integer month = startDate.month() + i;
            if (month > 12) {
                month = (month - 12);
                year = startDate.year() + 1;
                // do we need to handle more than one year ahead?
            } else {
                year = startDate.year();
            }
            System.debug(month);

            CaseActivity caseAct = new CaseActivity();
            caseAct.PracticeId = practiceId;
            caseAct.Month = month;
            caseAct.Year = year;
            
            boolean found = false;
            
            for (AggregateResult ar: results) {
                if ((Integer)ar.get('Month') == month && (Integer)ar.get('Year') == year) {
                    caseAct.Total = (Decimal)ar.get('Total') / surgeonCount;
                    found = true;
                }
            }
            if (!found) {
                caseAct.Total = 0;
            }
            caseActs.add(caseAct);
        }
        // Integer surgeonCount = GetSurgeonsForMonth(startDate, practiceDate);

        System.debug(caseActs.size());
        System.debug(caseActs);
        return caseActs;
    }

@RemoteAction
    global static List<CaseActivity> GetProceduresPerMonthByHospital(string hospitalId, string sDate, string eDate ){
        Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
        Date endDate = parseDate(eDate);  //  Date.newInstance(2016, 12, 31);   
        System.debug(startDate);
        System.debug(endDate);
        List<AggregateResult> results = [Select Event_Type__r.Name, calendar_year(Appointment_start__c) Year, calendar_month(Appointment_start__c) Month, COUNT(id) Total 
                                        from Event__c 
                                        where case__r.Patient__r.Hospital__c = :hospitalId 
                                        and
                                        Event_Type__r.Name = 'Procedure'
                                        and 
                                        Appointment_start__c >= :startDate 
                                        and 
                                        Appointment_start__c <= :endDate
                                        GROUP By Event_Type__r.Name, calendar_year(Appointment_start__c), calendar_month(Appointment_start__c)];//2016-09-08T01:02:03Z];
        System.debug(results);
        // List<Surgeon_Hospital__c> surgeons = GetSurgeons();
        List<CaseActivity> caseActs = new List<CaseActivity>();
        Integer monthDiff = startDate.monthsBetween(endDate) + 1; // CalcMonthDifference(startDate, endDate);
        System.debug(monthDiff);
        Integer year;

        Integer surgeonCount = GetSurgeonsForMonth(startDate, hospitalId);
        for (Integer i = 0; i < monthDiff; i++) {
            Integer month = startDate.month() + i;
            if (month > 12) {
                month = (month - 12);
                year = startDate.year() + 1;
                // do we need to handle more than one year ahead?
            } else {
                year = startDate.year();
            }
            System.debug(month);

            CaseActivity caseAct = new CaseActivity();
            caseAct.HospitalId = hospitalId;
            caseAct.Month = month;
            caseAct.Year = year;
            
            boolean found = false;
            
            for (AggregateResult ar: results) {
                if ((Integer)ar.get('Month') == month && (Integer)ar.get('Year') == year) {
                    caseAct.Total = (Decimal)ar.get('Total') / surgeonCount;
                    found = true;
                }
            }
            if (!found) {
                caseAct.Total = 0;
            }
            caseActs.add(caseAct);
        }
        // Integer surgeonCount = GetSurgeonsForMonth(startDate, practiceDate);

        System.debug(caseActs.size());
        System.debug(caseActs);
        return caseActs;
    }


    global static Integer CalcMonthDifference(Date startDate, Date endDate) {
        Integer diff =  startDate.monthsBetween(endDate);
        System.debug(diff);
        return startDate.monthsBetween(endDate);
    }
    
    // Get the surgeons for the practice - at some point we could look at determining how many were there for the month. 
    // we need a start date and end date
    global static Integer GetSurgeonsForMonth(Date month, String practiceId) {
        List<Surgeon_Hospital__c> acct = [Select Id, Name 
                    from Surgeon_Hospital__c 
                    Where Hospital__r.IsPartner = true AND Surgeon__r.Account.RecordType.Name = 'Surgeon Account' and Hospital__r.Id =: practiceID];
        return acct.size();            
    }

    global static Surgeon_Hospital__c getSurgeonFromList(string SurgeonId, List<Surgeon_Hospital__c> surgeons) {

        for (Surgeon_Hospital__c surgeon: surgeons) {
            if (surgeon.Id == surgeonId) {
                return surgeon;
         
            }
        }
        return new Surgeon_Hospital__c();
    }
    
    @RemoteAction
    global static List<Surgeon_Hospital__c> LoadHospitalPracticeSurgeons(String hospitalId){
        
            return [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name 
                    from Surgeon_Hospital__c 
                    Where Hospital__r.Id =: hospitalId And Hospital__r.IsPartner = true AND Surgeon__r.Account.RecordType.Name = 'Surgeon Account' 
                    Order By Surgeon__r.Name];    
    }


    @RemoteAction
    global static Map<String, Surgeon_Hospital__c> GetMappedSurgeonPractices() {
        List<Surgeon_Hospital__c> practices = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name, Hospital__r.Id, Hospital__r.Name 
                    from Surgeon_Hospital__c 
                    Where Hospital__r.IsPartner = true AND Surgeon__r.Account.RecordType.Name = 'Surgeon Account' AND Hospital__r.RecordType.Name = 'Practice Account' 
                    Order By Surgeon__r.Name];
        Map<String, Surgeon_Hospital__c> maps = new Map<String, Surgeon_Hospital__c>();
        for(Surgeon_Hospital__c practice: practices) {
            maps.put(practice.Surgeon__r.Id, practice);
        }            
        return maps;
    }

   
    @RemoteAction
    global static List<Surgeon_Hospital__c> GetSurgeons() {
        //filter by user rights!!!!!
        List<Surgeon_Hospital__c> surgeons = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name, Hospital__r.id 
                                                from Surgeon_Hospital__c 
                                                Where  Hospital__r.IsPartner = true AND Surgeon__r.Account.RecordType.Name = 'Surgeon Account' 
                                                Order By Surgeon__r.Name];
        return surgeons;
    }
    
    @RemoteAction
    global static List<IQ_Phase_Count__c> GetSurgeonPhaseCount(string id, string sDate, string eDate) {
        Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
        Date endDate = parseDate(eDate);  //  Date.newIn
        List<IQ_Phase_Count__c> results =  [Select ID, Name, Surgeon_ID__c, Month_Date__c, Count__c
                                        from IQ_Phase_Count__c 
                                        where Surgeon_ID__c = :id 
                                        and
                                        Month_Date__c >=:startDate
                                        and 
                                        Month_Date__c <=:endDate
                                        order By Month_Date__c];
        return results;
    }

    @RemoteAction
    global static List<IQ_Phase_Count__c> GetPracticePhaseCount(string id, string sDate, string eDate) {
        Date startDate = parseDate(sDate); //Date.newInstance(2016, 9, 1);
        Date endDate = parseDate(eDate);  //  
        List<IQ_Phase_Count__c> results =  [Select ID, Name, Surgeon_ID__c, Month_Date__c, Count__c
                                        from IQ_Phase_Count__c 
                                        where Parent_ID__c = :id 
                                        and
                                        Month_Date__c >=:startDate
                                        and 
                                        Month_Date__c <=:endDate
                                        order By Month_Date__c];
        return results;
    }
    
     @RemoteAction
    public static List<KneeBalance> GetPracticeSensorAverages(string id, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<KneeBalance> kneeBalances = new List<KneeBalance>(); 
        Date startDate = Date.newInstance(startYear, startMonth, 2);
        Date endDate = Date.newInstance(endYear, endMonth, 2);

        for(integer i = 0; i <= startDate.monthsBetween(endDate); i++) {
            Date newDate = startDate.addMonths(i);
            KneeBalance knee = GetMonthlyPracticeSensorAverages(id, newDate.month(), newDate.year());
            kneeBalances.add(knee);
        }
        return kneeBalances;
    }

    @RemoteAction
    public static KneeBalance GetMonthlyPracticeSensorAverages(string id, Integer month, Integer year) { 
        List<AggregateResult> results =  [select ActualFlexString__c, AVG(MedialLoad__c) Medial, 
                                        AVG(LateralLoad__c) Lateral from IQ_MonthlySensorData__c 
                                        where Practice_ID__c = :id 
                                        and
                                        CALENDAR_MONTH(Measurement_Date__c) =:month
                                        and 
                                        CALENDAR_YEAR(Measurement_Date__c) =:year
                                        group by ActualFlexString__c];
        System.debug(results);
        if (results.size() > 0) {
            KneeBalance data = new KneeBalance();

            data.PracticeID = id;
            data.Month = month;
            data.Year = year;
            System.debug(string.valueOf(data.Month) + ', ' + string.valueOf(data.Year));
            return ConvertKneeBalanceAggregateToObject(results, data);
        } else {
            return new KneeBalance();
        }
    }
    
    @RemoteAction
    public static List<KneeBalance> GetSurgeonSensorAverages(string id, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<KneeBalance> kneeBalances = new List<KneeBalance>(); 
        Date startDate = Date.newInstance(startYear, startMonth, 2);
        Date endDate = Date.newInstance(endYear, endMonth, 2);

        for(integer i = 0; i<= startDate.monthsBetween(endDate); i++) {
            Date newDate = startDate.addMonths(i);
            KneeBalance knee = GetMonthlySurgeonSensorAverages(id, newDate.month(), newDate.year());
            kneeBalances.add(knee);
        }

        return kneeBalances;
    }

    @RemoteAction
    public static KneeBalance GetMonthlySurgeonSensorAverages(string id, Integer month, Integer year) {
         List<AggregateResult> results =  [select ActualFlexString__c, AVG(MedialLoad__c) Medial, 
                                        AVG(LateralLoad__c) Lateral from IQ_MonthlySensorData__c 
                                        where Surgeon_ID__c = :id 
                                        and
                                        CALENDAR_MONTH(Measurement_Date__c) =:month
                                        and 
                                        CALENDAR_YEAR(Measurement_Date__c) =:year
                                        group by ActualFlexString__c];

        System.debug(results);
        
        if (results.size() > 0) {
            KneeBalance data = new KneeBalance();
            data.SurgeonID = id;
            data.Month = month;
            data.Year = year;
            System.debug(string.valueOf(data.Month) + ', ' + string.valueOf(data.Year));
            return ConvertKneeBalanceAggregateToObject(results, data);
        } else {
            return new KneeBalance();
        }
    }

    @RemoteAction
    public static List<IQ_MonthlySensorData__c> GetPracticeSensorData(string practiceId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) { 
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        System.debug(startDate);
        Date endDate = Date.newInstance(endYear, endMonth, Date.daysInMonth(endyear, endMonth));
        System.debug(endDate);
        List<IQ_MonthlySensorData__c> results =  [Select ActualFlex__c,
                                        ActualFlexString__c, 
                                        MedialLoad__c, 
                                        LateralLoad__c,
                                        Device_ID__c,
                                        Event_ID__c,
                                        MeasurementDate__c,
                                        PatientID__c,
                                        PatientName__c,
                                        Practice_ID__c,
                                        Practice_Name__c,
                                        ProcedureDate__c,
                                        Rotation__c,
                                        Surgeon_ID__c,
                                        Surgeon_Name__c 
                                        From IQ_MonthlySensorData__c 
                                        Where Practice_ID__c = :practiceId
                                        and
                                        ProcedureDate__c >=:startDate
                                        and 
                                        ProcedureDate__c <=:endDate];
        return results;
    }

    @RemoteAction 
    public static List<AggregateResult> GetAggregatePromDeltasBySurgeon( string surgeonId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<IQ_Prom_Delta__c> deltas = new List<IQ_Prom_Delta__c>();
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        Date endDate = Date.newInstance(endYear, endMonth, date.daysInMonth(endYear, endMonth));
        // deltas = [Select EventFormID__c, PracticeID__c, PracticeName__c, ProcedureDate__c, Score__c, Section__c, Stage__c, SurgeonID__c, SurgeonName__c, SurveyID__c from IQ_Prom_Delta__c];
        List<AggregateResult> results = [Select AVG(Score__c) Score__c, Section__c, Stage__c
                                        From IQ_Prom_Delta__c
                                        Where SurgeonID__c = :surgeonId   
                                        and ProcedureDate__c >= :startDate
                                        and ProcedureDate__c <= :endDate                                     
                                        Group By Section__c, Stage__c];
        return results;
    }

    @RemoteAction 
    public static List<AggregateResult> GetAggregatePromDeltasByPractice( string practiceId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<IQ_Prom_Delta__c> deltas = new List<IQ_Prom_Delta__c>();
        // deltas = [Select EventFormID__c, PracticeID__c, PracticeName__c, ProcedureDate__c, Score__c, Section__c, Stage__c, SurgeonID__c, SurgeonName__c, SurveyID__c from IQ_Prom_Delta__c];
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        Date endDate = Date.newInstance(endYear, endMonth, date.daysInMonth(endYear, endMonth));
        System.debug(endDate);
        List<AggregateResult> results = [Select AVG(Score__c) Score__c, Section__c, Stage__c
                                        From IQ_Prom_Delta__c
                                        Where PracticeID__c = :practiceId                                        
                                        and ProcedureDate__c >= :startDate
                                        and ProcedureDate__c <= :endDate                  
                                        Group By Section__c, Stage__c];
        return results;
    }

    @RemoteAction 
    public static List<IQ_Prom_Delta__c> GetPromDeltasBySurgeon( string surgeonId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<IQ_Prom_Delta__c> deltas = new List<IQ_Prom_Delta__c>();
        // deltas = [Select EventFormID__c, PracticeID__c, PracticeName__c, ProcedureDate__c, Score__c, Section__c, Stage__c, SurgeonID__c, SurgeonName__c, SurveyID__c from IQ_Prom_Delta__c];
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        Date endDate = Date.newInstance(endYear, endMonth, date.daysInMonth(endYear, endMonth));
        List<IQ_Prom_Delta__c> results = [Select Score__c, Section__c, Stage__c, ProcedureDate__c, SurgeonID__c, SurgeonName__c, 
                                        PracticeID__c, PracticeName__c, PatientID__c, PatientName__c
                                        From IQ_Prom_Delta__c
                                        Where SurgeonID__c = :surgeonId
                                        and ProcedureDate__c >= :startDate
                                        and ProcedureDate__c <= :endDate];
        return results;
    }

    @RemoteAction 
    public static List<IQ_Prom_Delta__c> GetPromScoresBySurgeon( string surgeonId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<IQ_Prom_Delta__c> deltas = new List<IQ_Prom_Delta__c>();
        // deltas = [Select EventFormID__c, PracticeID__c, PracticeName__c, ProcedureDate__c, Score__c, Section__c, Stage__c, SurgeonID__c, SurgeonName__c, SurveyID__c from IQ_Prom_Delta__c];
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        Date endDate = Date.newInstance(endYear, endMonth, date.daysInMonth(endYear, endMonth));
        List<IQ_Prom_Delta__c> results = [Select Score__c, Section__c, Stage__c, ProcedureDate__c, SurgeonID__c, SurgeonName__c, 
                                        PracticeID__c, PracticeName__c, PatientID__c, PatientName__c
                                        From IQ_Prom_Delta__c
                                        Where SurgeonID__c = :surgeonId
                                        and ProcedureDate__c >= :startDate
                                        and ProcedureDate__c <= :endDate];
        return results;
    }

    @RemoteAction 
    public static List<IQ_Prom_Delta__c> GetPromDeltasByPractice( string practiceId, Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
        List<IQ_Prom_Delta__c> deltas = new List<IQ_Prom_Delta__c>();
        // deltas = [Select EventFormID__c, PracticeID__c, PracticeName__c, ProcedureDate__c, Score__c, Section__c, Stage__c, SurgeonID__c, SurgeonName__c, SurveyID__c from IQ_Prom_Delta__c];
        Date startDate = Date.newInstance(startYear, startMonth, 1);
        Date endDate = Date.newInstance(endYear, endMonth, date.daysInMonth(endYear, endMonth));
        List<IQ_Prom_Delta__c> results = [Select Score__c, Section__c, Stage__c, ProcedureDate__c, SurgeonID__c, SurgeonName__c, 
                                        PracticeID__c, PracticeName__c, PatientID__c, PatientName__c
                                        From IQ_Prom_Delta__c
                                        Where PracticeID__c = :practiceId
                                        and ProcedureDate__c >= :startDate
                                        and ProcedureDate__c <= :endDate];
        return results;
    }

    @RemoteAction
    public static List<Surgeon_Hospital__c> GetParents() {
        List<Surgeon_Hospital__c> accts = [Select Id, Name, Surgeon__r.Account.Id, Hospital__r.Id
                    from Surgeon_Hospital__c 
                    Where Hospital__r.RecordType.Name = 'Practice Account'];
        System.debug(accts.size());
        return accts;
    }
    
    @RemoteAction
    public static Surgeon_Hospital__c GetParent(List<Surgeon_Hospital__c> accts , String Id) {
        // System.debug(Id);
        Surgeon_Hospital__c acct = new Surgeon_Hospital__c();
        for (Surgeon_Hospital__c found: accts) {
            // System.debug(found.Surgeon__r.Account.Id);
            if (found.Surgeon__r.Account.Id == Id) {
                // System.debug('found');
                return found;
            }
        }
        return acct;
    } 

    @RemoteAction
    global static List<PatientCase> GetPatientsUnderBundle(String practiceId){  
        Date today = Date.today();
        Date startDate = today.addDays(-1);
        Date endDate = today.addDays(90);
        
        List<Event__c> result =  [Select id, Case__r.Case_Type__r.Description__c, Case__r.Laterality__c, Physician__c, Physician__r.Name, 
                            Case__r.Procedure_Date__c, Case__r.Patient__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.First_Name__c, 
                            Case__r.Patient__r.Date_of_Birth__c, Case__r.Patient__r.Anonymous_Year_Of_Birth__c,
                            Case__r.Patient__r.Anonymous__c, Case__r.Patient__r.Anonymous_label__c
                            from event__c 
                            where Location__c = :practiceId
                            and Case__r.Procedure_Date__c >=:startDate 
                            AND Case__r.Procedure_Date__c <=:endDate
                            and Event_Type__r.Name = 'Procedure'
                                        Limit 10000];   
        System.debug(result);  
        if (result.size() > 0) {
            List<PatientCase> patients = ConvertToPatientCase(result);
            System.debug(patients);
            return patients;   
        } else {
            return null;
        }
    }

    //// TO DO: add date range!!!!
    @RemoteAction
    global static List<PatientCase> GetScheduledPatients(String practiceId, Integer daysInFuture){  
        Date today = Date.today();
        Date startDate = today.addDays(0);
        Date endDate = today.addDays(daysInFuture);
        System.debug(startDate);
        List<event__c> result =  [Select id, Case__r.Case_Type__r.Description__c, Case__r.Laterality__c, Physician__c, Physician__r.Name, 
                            Case__r.Procedure_Date__c, Case__r.Patient__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.First_Name__c, 
                            Case__r.Patient__r.Date_of_Birth__c, Case__r.Patient__r.Anonymous_Year_Of_Birth__c,
                            Case__r.Patient__r.Anonymous__c, Case__r.Patient__r.Anonymous_label__c
                            from event__c 
                            where Location__c = :practiceId
                            and Case__r.Procedure_Date__c >=:startDate 
                            AND Case__r.Procedure_Date__c <=:endDate
                            and Event_Type__r.Name = 'Procedure'
                                        Limit 10000];     
        if (result.size() > 0) {
             List<PatientCase> patients = ConvertToPatientCase(result);
            
            return patients;   
        } else {
            return null;
        }
    }

    public static List<PatientCase> ConvertToPatientCase(List<event__c> event) {
        List<PatientCase> patients = new List<PatientCase>();
        for (Event__c item: event) {
                PatientCase patient = new PatientCase();
                patient.id = item.Case__r.Patient__c;
                patient.patientId = item.Case__r.Patient__c;
                patient.lastName = item.Case__r.Patient__r.Last_Name__c;
                patient.firstName = item.Case__r.Patient__r.First_Name__c;
                patient.procedureDate = item.Case__r.Procedure_Date__c;
                patient.dateOfBirth = item.Case__r.Patient__r.Date_of_Birth__c;
                patient.birthYear = item.Case__r.Patient__r.Anonymous_Year_Of_Birth__c != null ? item.Case__r.Patient__r.Anonymous_Year_Of_Birth__c : '';
                patient.surgeonId = item.Physician__c;
                patient.surgeonName = item.Physician__r.Name;
                patient.isAnonymous = item.Case__r.Patient__r.Anonymous__c;
                patient.anonymousLabel = item.Case__r.Patient__r.Anonymous_Label__c != null ? item.Case__r.Patient__r.Anonymous_Label__c : '';
                patient.laterality = item.Case__r.Laterality__c;
                patients.add(patient);
            }
            return patients;   
    }

    public static Date parseDate(String inDate) {
        Date    dateRes    = null;
        //  1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy   

        try {
            String candDate    = inDate.substring(0,Math.min(10,inDate.length()));// grab date portion only m[m]/d[d]/yyyy , ignore time
            dateRes    = Date.parse(candDate);
        }
        catch (Exception e) {}
 
        if (dateRes == null) {
        //  2 - Try yyyy-mm-dd         
        try {
            String candDate    = inDate.substring(0,10);           // grab date portion only, ignore time, if any
            dateRes            = Date.valueOf(candDate);
            }
            catch (Exception e) {}
        }
        return dateRes;
    }

    public static DateTime parseDateTime(String inDate) {
        DateTime    dateRes    = null;
        //  1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy   

        try {
            String candDate    = inDate.substring(0,Math.min(10,inDate.length()));// grab date portion only m[m]/d[d]/yyyy , ignore time
            dateRes    = DateTime.parse(candDate);
        }
        catch (Exception e) {}
 
        if (dateRes == null) {
        //  2 - Try yyyy-mm-dd         
        try {
            String candDate    = inDate.substring(0,10);           // grab date portion only, ignore time, if any
            dateRes            = DateTime.valueOf(candDate);
            }
            catch (Exception e) {}
        }
        System.debug(dateRes);
        return dateRes;
    }

    public static KneeBalance ConvertKneeBalanceAggregateToObject(List<AggregateResult> results, KneeBalance data) {
        // KneeBalance data = new KneeBalance();
        System.debug(results);
        for (AggregateResult result : results) {
            if (result.get('ActualFlexString__c') == '10') {
                data.Medial10 = (Decimal)result.get('Medial');
                data.Lateral10 = (Decimal)result.get('Lateral');
            } else {
                if (result.get('ActualFlexString__c') == '45') {
                    data.Medial45 = (Decimal)result.get('Medial');
                    data.Lateral45 = (Decimal)result.get('Lateral');
                } else {
                    if (result.get('ActualFlexString__c') == '90') {
                        data.Medial90 = (Decimal)result.get('Medial');
                        data.Lateral90 = (Decimal)result.get('Lateral');
                    }
                }
            }
        }
        //assume if there's data its for a given practice or surgeon
        System.debug(data);
        return data;
    }

    global class CaseActivity {
        Integer Month;
        Integer Day; 
        Integer Year;
        String HospitalId;
        String PracticeId;
        String PhysicianId;
        string PhysicianName;
        Decimal Total;
    }

    global class PhaseCount {
        string SurgeonId;

        Integer Month;
        Integer Year;
        Integer PreOpCount;
        Integer PostOpCcount;
        Integer LongTerm;

    }

    global class KneeBalance {
        string SurgeonId;
        string PracticeId;
        public Integer Month {get; set;}
        Integer Year;
        Decimal Medial10;
        Decimal Medial45;
        Decimal Medial90;
        Decimal Lateral10;
        Decimal Lateral45;
        Decimal Lateral90;
    }

    global class PatientCase {
        string id;
        string patientId;
        string lastName;
        string firstName;
        Boolean isAnonymous;
        string anonymousLabel;
        Date procedureDate;
        Date dateOfBirth;
        String birthYear;
        string surgeonId;
        string surgeonName;
        string practiceId;
        string practiceName;
        string laterality;
        
    }

}