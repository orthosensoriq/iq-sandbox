@isTest(SeeAllData=true)
public class RHX_TEST_SCM_Revenue_Line {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM SCM_Revenue_Line__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new SCM_Revenue_Line__c()
            );
        }
    	Database.upsert(sourceList);
    }
}