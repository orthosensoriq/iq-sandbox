public with sharing class SCMUtilities {

	public static Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();

	public static id getRecordType(String name, SObject userObject){
		ID retId = null;
		// Get the sObject describe result for the userObject
		Schema.DescribeSObjectResult r = userObject.getSObjectType().getDescribe();
		String objName = r.getName();
		List<RecordType> info = rTypes.get(objName);
		if (info == null){
			Map<ID, Schema.RecordTypeInfo> rInfo = r.getRecordTypeInfosByID();
			info = [select id
					,name
					,developerName
				from RecordType
				where id in :rInfo.keySet()];
			rTypes.put(objName, info);
		}
		for (RecordType rType : rTypes.get(objName)){
			if (rType.developerName == name){
				retId = rType.id;
				break;
			}
		}
		System.assertNotEquals(null, retId, name + ' record type not available for ' + objName);
		return retId;
		
	}

	public static string getCompanyLogo() {
		SCMC__Organization_Settings__c orgSetting = SCMC__Organization_Settings__c.getInstance();
		String statResourceURL = '';

		if(orgSetting != null && orgSetting.SCMC__Company_Logo_URL__c != null && orgSetting.SCMC__Company_Logo_URL__c != '') {
			statResourceURL = orgSetting.SCMC__Company_Logo_URL__c;
		}

		return statResourceURL;
	}

	public static SCMC__Org_Address__c getOrganization() {
		SCMC__Org_Address__c orgAddress = SCMC__Org_Address__c.getInstance();
		return orgAddress;
	}

	public static String getFormattedAddress(String name, String line1, String line2, String city
											,String state, String postalCode, String country, string Phone, string Fax, string Email, String delimiter) {
		String address = '';

		if (name <> null) { address = name; }

		if (line1 <> null) {
			if (address <> '') { address += delimiter; }
			address += line1;
		}

		if (line2 <> null) {
			if (address <> '') { address += delimiter; }
			address += line2;
		}

		if (city <> null){
			if (address <> ''){ address += delimiter; }
			address += city;
		}
		
		if(state <> null) {
			if(city <> null){ 
				address += ', ';
			} else if(address <> ''){
				 address += delimiter;
			}
			address += state;
		}
		
		if (postalCode <> null) {
			if(state <> null){
				address += ' ';
			} else if (address <> '') {
				address += delimiter;
			}
			address += postalCode;
		}

		if (country <> null) {
			if (address <> '') { address += delimiter; }
			address += country;
		}

		if (Phone <> null) {
			if (address <> '') { address += delimiter; }
			address += '<div class="spacer">&nbsp;</div> Phone: ' + Phone;
		}

		if (Fax <> null) {
			if (address <> '') { address += delimiter; }
			address += 'Fax:&nbsp;&nbsp;&nbsp;&nbsp; ' + Fax;
		}

		if (Email <> null) {
			if (address <> '') { address += delimiter; }
			address += 'Email:&nbsp;&nbsp;&nbsp;' + Email;
		}

		return  address;

	}
}