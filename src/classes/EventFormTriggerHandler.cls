public with sharing class EventFormTriggerHandler extends BaseTriggerHandler
{
	final String CLASSNAME = '\n\n**** EventFormTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
	//@date : 12/18/2013
	//@description : the class constructor method
	//@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
	//@returns : nothing
    public EventFormTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
    	String METHODNAME = CLASSNAME.replace('METHODNAME', 'EventFormTriggerHandler') + ' - class constructor';
    	// system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

		// trigger is executing
        TriggerIsExecuting = isExecuting;
		
		// set batch size
        BatchSize = pTriggerSize;
		
		// set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
    	//Not Called for this Trigger
    }
    
    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	//Not Called for this Trigger
    }
    
    public override void OnAfterUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
     	Super.OnAfterUpdate(beforeObject, afterObject);
        /* Commented Out By Bijan Sadeghi - Advised by Michael Gold on 3/16/16 to avoid completing events when they are created
		set<Id> eventFormIds = new set<Id>();
        set<Id> eventIds = new set<Id>();
        eventFormIds.addAll(afterobject.keyset());
        for(event_form__c eventform : (list<event_form__c>)afterObject.values())
        {
         	eventIds.add(eventform.event__c);   
        }
		list<event__c> events = [select id, status__c from event__c where Id in :eventIds];
        list<event_form__c> forms = [select id, event__c from event_form__c where event__c in :eventIds and mandatory__c = true and form_complete__c = false];
        
        for(event__c curEvent : events)
        {
            boolean wasFound = false;
			for(event_form__c curForm : forms)
            {
            	if(curEvent.Id == curForm.event__c)
                {
                    wasFound = true;
                    break;
                }
            }
            if(wasFound)
                curEvent.Status__c = 'Pending Completion';
            else
                curEvent.Status__c = 'Complete';
        }            
        
        update events;
		*/
    }

    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
    	//Not Called for this Trigger
    }
}