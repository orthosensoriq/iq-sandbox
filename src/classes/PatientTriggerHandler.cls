public class PatientTriggerHandler extends BaseTriggerHandler
{
	final String CLASSNAME = '\n\n**** PatientTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
	//@date : 12/18/2013
	//@description : the class constructor method
	//@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
	//@returns : nothing
    public PatientTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
    	String METHODNAME = CLASSNAME.replace('METHODNAME', 'PatientTriggerHandler') + ' - class constructor';
    	system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

		// trigger is executing
        TriggerIsExecuting = isExecuting;
		
		// set batch size
        BatchSize = pTriggerSize;
		
		// set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
		Metaphone m = new Metaphone();
        For (Patient__c a: (list<Patient__c>)insertObject)
        {
			a.Soundex_Metaphone_Value__c = m.getMetaphone(a.Last_Name__c);
			
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
            system.debug(guid);
            
            a.SurveyUUID__c = guid;   
        }
    }
    
    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
		Metaphone m = new Metaphone();
    	For (Patient__c a: (List<Patient__c>)afterObject.values())
        {
			a.Soundex_Metaphone_Value__c = m.getMetaphone(a.Last_Name__c);   
        }
    }

    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
    	//Not Called for this Trigger
    }
}