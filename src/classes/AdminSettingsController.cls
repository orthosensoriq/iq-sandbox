public with sharing class AdminSettingsController {
	
	public String AuditLogPrefix {get; set;}

	public AdminSettingsController() {
		Schema.DescribeSObjectResult r = Audit_Log__c.sObjectType.getDescribe();
		AuditLogPrefix = r.getKeyPrefix();
	}
}