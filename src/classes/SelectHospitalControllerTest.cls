@isTest(SeeAllData=true)
private class SelectHospitalControllerTest {
	static testMethod void myUnitTest() {
		
		Test.startTest();
		SelectHospitalController selectHospitalController = new SelectHospitalController();
		Account testAccount = [Select Id from Account Limit 1];
		selectHospitalController.AccountID = testAccount.Id;
		selectHospitalController.searchPhrase = 'abc';
		selectHospitalController.Search();
		selectHospitalController.goToProcedureListing();
		selectHospitalController.getRecentlyViewed();
		selectHospitalController.getAllHospitals();
		Test.stopTest();
		
	}
}