@isTest
private class ProceduresListingControllerTest {
	static testMethod void myUnitTest() {
		
		Test.startTest();
//SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000
		Account a = new Account();
		a.Name = 'TestAccount';
        a.RecordTypeId = [Select Id from RecordType where Name = 'Hospital Account' LIMIT 1].Id;
		insert a;
        
        Account s = new Account();
        s.FirstName = 'Test';
        s.LastName = 'Tester';
        s.RecordTypeId = [Select Id from RecordType where Name = 'Surgeon Account' Limit 1].id;
        insert s;
        
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test';
		c.Email = 'Tester@Tester.com.Test';
        c.accountId = a.id;
		insert c;
        
        //User testUser = new User();
        //testUser.FirstName = 'Testing';
        //testUser.LastName = 'Tester';
        //testUser.Username = 'tester@OrthoSensor.com.test';
        //testUser.Email = 'tester@OrthoSensor.com.test';
        //testUser.Alias = 'tester';
        //testUser.TimeZoneSidKey = 'America/New_York';
        //testUser.LocaleSidKey = 'en_US';
        //testUser.EmailEncodingKey = 'ISO-8859-1';
        //testUser.LanguageLocaleKey = 'en_US';
        //testUser.ProfileId = [Select id from Profile where name = 'Partner Community User' LIMIT 1].id;
        //testUser.ContactId = c.id;
        //insert testUser;
		
        surgeon_hospital__c surghosp = new surgeon_hospital__c();
        surghosp.Hospital__c = a.id;
        surghosp.Surgeon__c = c.id;
        insert surghosp;
        a.IsPartner = true;
        update a;
		
        
		list<Account> testAccts = [SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000];
        system.debug('Hospital Accounts: ' + testAccts.size());
    
        Patient__c testPatient = new Patient__c();
        testPatient.First_Name__c = 'Test';
        testPatient.Last_Name__c = 'Tester';
        testPatient.Gender__c = 'Female';
        testPatient.Date_Of_Birth__c = date.newInstance(1970, 3, 3);
        testPatient.Account_Number__c = '1234567890987654321';
        testPatient.Medical_Record_Number__c = 'MR123456';
        testPatient.active__c = true;
        testPatient.Hospital__c = a.id;
        insert testPatient;
        
        Case_Type__c ctyp = new Case_Type__c();
        ctyp.Description__c = 'Total Knee Arthroplasty';
        insert ctyp;
        
		Event_Type__c evtyp = new Event_Type__c();
		evtyp.Name = 'Procedure';
		evtyp.Primary__c = true;
        evtyp.Active__c = true;
		insert evtyp;

        Event_type__c evtyp2 = new Event_Type__c();
        evtyp2.name = 'followup';
        evtyp2.Primary__c = false;
        evtyp2.Active__c = true;
        insert evtyp2;
        
        Case_Type_Event_Type__c ctet = new Case_Type_Event_Type__c();
        ctet.Case_Type__c = ctyp.id;
        ctet.Event_Type__c = evtyp.id;
        insert ctet;
        
        Case_Type_Event_Type__c ctet2 = new Case_Type_Event_Type__c();
        ctet2.Case_Type__c = ctyp.id;
        ctet2.Event_Type__c = evtyp2.id;
        insert ctet2;
        
        survey__c surv = new survey__c();
        surv.Active__c = true;
        surv.Name__c = 'Test Survey';
        surv.Survey_URL__c = 'http://www.test.com/123423awfa.htm';
		insert surv;
        
        event_type_survey__c ets = new event_type_survey__c();
        ets.survey__c = surv.id;
        ets.Event_Type__c = evtyp.id;
        insert ets;
        
        Case__c newCase = new Case__c();
        newCase.Name__c = 'Test Case';
        newCase.Patient__c = testPatient.id;
        newCase.Case_Type__c = ctyp.id;
        Insert newCase;
        
		Event__c newEvent = new Event__c();
        newEvent.Event_Name__c = 'Test Procedure';
        newEvent.Case__c = newCase.Id;
        newEvent.Event_Type__c = evtyp.id;
        newEvent.Physician__c = s.id;
        newEvent.Location__c = a.id;
        Insert newEvent;
        
        Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
        testClinicalData.Event__c = newEvent.id;
        insert testClinicalData;

//Select Id, First_Name__c, Last_Name__c, Gender__c, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id =: selectedHospital.id        
//Select Id from Event_Type__c where primary__c = true
//Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:selectedHospital.Id)
//Select Id, Name from Account Where personcontactID IN: surgeonContacts
//
//Select Id, Name, Appointment_Start__c, Description__c, Case__c, Case__r.Patient__c, Case__r.Patient__r.Name, Case__r.Patient__r.Last_Name__c, 
//		 Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Date_Of_Birth__c,
//		 Case__r.Patient__r.Medical_Record_Number__c, Case__r.Case_Type__r.Description__c, Physician__c, 
//		 Physician__r.Name, Physician__r.Id, Location__c, Location__r.Name
//FROM 	 Event__c WHERE Event_Type__c =\'' + params.get(0) + '\' AND Location__c!=null LIMIT 999'
//
//Select Id, Name, First_Name__c, Last_Name__c, Gender__c, Hospital__r.Name, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id = \'' + params.get(0) + '\'
//
//select event_type__c, survey__c, survey__r.name from event_type_survey__c where survey__r.active__c = true and event_type__c in :eventTypeIds
        ProceduresListingController proceduresListingController = new ProceduresListingController();
        proceduresListingController.LogConstructor();
        procedureslistingcontroller.selectedHospital = a;
        proceduresListingController.getPatients(a.Id);
        proceduresListingController.ChangeSurgeon();
        proceduresListingController.PostBack();
        proceduresListingController.goToProcedureSummary();
        proceduresListingController.GoToCaseSelector();
        proceduresListingController.ChangeHospital();
		proceduresListingController.ChangeDate();
        
		Test.stopTest();
	}
}