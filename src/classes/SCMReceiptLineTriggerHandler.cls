/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
public with sharing class SCMReceiptLineTriggerHandler {

	private boolean isExecuting = false;
	private integer batchSize = 0;

	public SCMReceiptLineTriggerHandler(boolean isExecuting, integer size)
	{
		this.isExecuting = isExecuting;
		this.batchSize = size;
	}

	public void OnAfterInsert(SCMC__Receipt_Line__c[] newItems, Map<ID, SCMC__Receipt_Line__c> newMap)
	{
		// Send the Lines to potentially update related Fixed Assets, a.k.a. "Link Stations".
		SCMFixedAsset.updateFixedAssetsWhenReceived(newItems);
	}

	public void OnAfterUpdate(SCMC__Receipt_Line__c[] oldItems, Map<ID, SCMC__Receipt_Line__c> oldMap, SCMC__Receipt_Line__c[] newItems, Map<ID, SCMC__Receipt_Line__c> newMap)
	{
		// Send the Lines to potentially update related Fixed Assets, a.k.a. "Link Stations".
		SCMFixedAsset.updateFixedAssetsWhenReceived(newItems);
	}
}