@isTest(SeeAllData=true)
private class ReportSelectorExtensionTest
{
    static testmethod void testReportSelector()
    {
        //Construct controller
        ReportSelectorExtension rptController = new ReportSelectorExtension();
        
        if(rptController.reportWrap.size() > 0)
        {
            rptController.ReportID = rptController.reportWrap[0].reports[0].rptId;
            PageReference rptPageRef = rptController.goToSelectedReport();
            rptController.generatePDFReport();
        }
    }
}