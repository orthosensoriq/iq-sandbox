/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Create a journal header and lines for the depreciation schedule.
**/
public with sharing class SCMFFJournal implements SCMFFIJournal {

	private UnitOfWorkMap uowMap = null;

	private map<id, c2g__codaJournal__c> journals_m = null;
	public map<id, c2g__codaJournal__c> getJournals(){
		if (journals_m == null){
			createJournals();
			journals_m = uowMap.getJournals();
		}
		return journals_m;
	}

	private map<id, c2g__codaJournalLineItem__c> journalLines_increasing_m = null;
	public map<id, c2g__codaJournalLineItem__c> getJournalLinesIncreasing(){
		if (journals_m == null) throw new SCMException('Call getJournals() before getting line items.');
		if (journalLines_increasing_m == null){
			createJournalLines();
			journalLines_increasing_m = uowMap.getJournalLines('i');
		}
		return journalLines_increasing_m;
	}

	private map<id, c2g__codaJournalLineItem__c> journalLines_decreasing_m = null;
	public map<id, c2g__codaJournalLineItem__c> getJournalLinesDecreasing(){
		if (journals_m == null) throw new SCMException('Call getJournals() before getting line items.');
		if (journalLines_decreasing_m == null){
			createJournalLines();
			journalLines_decreasing_m = uowMap.getJournalLines('d');
		}
		return journalLines_decreasing_m;
	}

	private void createJournals(){

		for (UnitOfWork uow : uowMap.values()) {
			// Journal information
			uow.journal = new c2g__codaJournal__c(
				ffgl__DeriveCurrency__c = false,
				c2g__JournalCurrency__c = uow.currencyId,
				ffgl__DerivePeriod__c = false,
				c2g__OwnerCompany__c = uow.asset.Company__c,
				c2g__Period__c = uow.periodId,
				c2g__Type__c = 'Manual Journal');
			if (uowMap.rootObjectTypeAbbreviation == 'DS') {
				uow.journal.Depreciation_Schedule__c = uow.depreciationSchedule.Id;
				uow.journal.c2g__JournalDate__c = uow.depreciationSchedule.FAM__Effective_Depreciation_Date__c;
				uow.journal.c2g__JournalDescription__c =
					(!testingContext.isTest) ?
					'Depreciation for ' + uow.asset.Name :
					'Depreciation for Test789';
				uow.journal.c2g__Reference__c = 'Depreciation-Schedule item';
			} else if (uowMap.rootObjectTypeAbbreviation == 'RL') {
				uow.journal.receipt_line__c = uow.receiptLine.Id;
				uow.journal.c2g__JournalDate__c =
					(!testingContext.isTest) ?
					uow.receiptLine.createdDate.date() :
					Date.today();
				uow.journal.c2g__JournalDescription__c =
					(!testingContext.isTest) ?
					'Receipt for ' + uow.asset.Name :
					'Receipt for Test123';
				uow.journal.c2g__Reference__c = 'Receipt-Line item';
			}
		}
	}

	private boolean journalLinesCreated = false;
	private void createJournalLines(){

		if (journalLinesCreated) return;

		for (UnitOfWork uow : uowMap.values()) {

			uow.journalLine_increasingAccount = new c2g__codaJournalLineItem__c(c2g__LineType__c = 'General Ledger Account');
			uow.journalLine_decreasingAccount = new c2g__codaJournalLineItem__c(c2g__LineType__c = 'General Ledger Account');

			if (uowMap.rootObjectTypeAbbreviation == 'DS'){
				uow.journalLine_increasingAccount.c2g__GeneralLedgerAccount__c = uow.asset.Depreciation_GL_Account__c;
				uow.journalLine_decreasingAccount.c2g__GeneralLedgerAccount__c = uow.asset.Fixed_Asset_GL_Account__c;
				uow.journalLine_increasingAccount.c2g__Value__c = uow.depreciationSchedule.FAM__Depreciation_Amount__c.setScale(2);
				uow.journalLine_decreasingAccount.c2g__Value__c = (uow.depreciationSchedule.FAM__Depreciation_Amount__c * -1).setScale(2);
			} else if (uowMap.rootObjectTypeAbbreviation == 'RL'){
				uow.journalLine_increasingAccount.c2g__GeneralLedgerAccount__c = uow.asset.Fixed_Asset_GL_Account__c;
				uow.journalLine_decreasingAccount.c2g__GeneralLedgerAccount__c = uow.receiptLine.SCMC__Purchase_Order_Line_Item__r.SCMFFA__General_Ledger_Account__c;
				uow.journalLine_increasingAccount.c2g__Value__c = uow.receiptLine.SCMC__Purchase_Order_Line_Item__r.SCMC__Unit_Cost__c.setScale(2);
				uow.journalLine_decreasingAccount.c2g__Value__c = (uow.receiptLine.SCMC__Purchase_Order_Line_Item__r.SCMC__Unit_Cost__c * -1).setScale(2);
			}
		}

		journalLinesCreated = true;
	}

	private SCMFFJournal() { journalLinesCreated = false; }

	public static SCMFFJournal create(list<FAM__FA_Depreciation_Schedule__c> depreciationScheduleList, map<id, FAM__Fixed_Asset__c> idToFixedAssetMap){
		SCMFFJournal journal = new SCMFFJournal();
		journal.uowMap = new UnitOfWorkMap(depreciationScheduleList, idToFixedAssetMap);
		return journal;
	}

	public static SCMFFJournal create(list<SCMC__Receipt_Line__c> receiptLines, map<id, FAM__Fixed_Asset__c> idToFixedAssetMap){
		SCMFFJournal journal = new SCMFFJournal();
		journal.uowMap = new UnitOfWorkMap(receiptLines, idToFixedAssetMap);
		return journal;
	}

	private class UnitOfWork{
		public FAM__FA_Depreciation_Schedule__c depreciationSchedule = null;
		public SCMC__Receipt_Line__c receiptLine = null;
		public FAM__Fixed_Asset__c asset = null;
		public Id currencyId = null;
		public Id periodId = null;
		public c2g__codaJournal__c journal = null;
		public c2g__codaJournalLineItem__c journalLine_increasingAccount = null;
		public c2g__codaJournalLineItem__c journalLine_decreasingAccount = null;
		// Methods
		// Constructors
		private UnitOfWork() {}
		public UnitOfWork(FAM__FA_Depreciation_Schedule__c item, FAM__Fixed_Asset__c fixedAsset){
			this.depreciationSchedule = item;
			this.asset = fixedAsset;
		}
		public UnitOfWork(SCMC__Receipt_Line__c item, FAM__Fixed_Asset__c fixedAsset){
			this.receiptLine = item;
			this.asset = fixedAsset;
		}
	}

	private class UnitOfWorkMap {
		// Properties
		public string rootObjectTypeAbbreviation = 'NotSet';  // DS or RL
		private map<id, UnitOfWork> uowMap = new map<id, UnitOfWork>();
		// Public Methods
		public UnitOfWork get(id value) { return uowMap.get(value); }
		public list<UnitOfWork> values() { return uowMap.values(); }
		public map<id, c2g__codaJournal__c> getJournals(){
			map<id, c2g__codaJournal__c> resultMap = new map<id, c2g__codaJournal__c>();
			for (UnitOfWork uow : uowMap.values())
				if (rootObjectTypeAbbreviation == 'DS')
					resultMap.put(uow.depreciationSchedule.id, uow.journal);
				else if (rootObjectTypeAbbreviation == 'RL')
					resultMap.put(uow.receiptLine.id, uow.journal);
			return resultMap;
		}
		public map<id, c2g__codaJournalLineItem__c> getJournalLines(string which){
			if (which != 'i' && which != 'd') throw new SCMException('The value of the \'which\' parameter must be \'i\' or \'d\'.');
			map<id, c2g__codaJournalLineItem__c> resultMap = new map<id, c2g__codaJournalLineItem__c>();
			for (UnitOfWork uow : uowMap.values()) {
				Id tag = ('DS'.equals(this.rootObjectTypeAbbreviation)) ? uow.depreciationSchedule.Id : uow.receiptLine.Id;
				if (which == 'i') resultMap.put(tag, uow.journalLine_increasingAccount);
				if (which == 'd') resultMap.put(tag, uow.journalLine_decreasingAccount);
			}
			return resultMap;
		}
		// Private Methods
		private void put(Id idValue, UnitOfWork item) { uowMap.put(idValue, item); }
		private void getCurrencies(){

			map<id, id> companyToCurrencyMap = new map<id, id>();
			List<c2g__codaAccountingCurrency__c> currencyList = null;
			if (!testingContext.isTest) {
				currencyList = [select id, c2g__OwnerCompany__c from c2g__codaAccountingCurrency__c];
			} else {
				currencyList = testingContext.codaCurrencyList;
			}
			for (c2g__codaAccountingCurrency__c c : currencyList)
				companyToCurrencyMap.put(c.c2g__OwnerCompany__c, c.Id);
	        for (UnitOfWork uow : uowMap.values()) uow.currencyId = companyToCurrencyMap.get(uow.asset.Company__c);
		}
		private void getPeriods(){

			// Fetch the accounting-period information
	        list<c2g__codaPeriod__c> manyPeriods = null;
	        if (!testingContext.isTest) {
	        	manyPeriods = [
		        	select
						Id,
						name,
		                c2g__StartDate__c,
		                c2g__EndDate__c,
		                c2g__OwnerCompany__r.Name
		            from c2g__codaPeriod__c
		            where c2g__Closed__c = false];
			} else {
				manyPeriods = testingContext.codaPeriodList;
			}
			// Iterate through the related-information map's items and populate the period ID.
			date comparisonDate = null;
			for (UnitOfWork uow : uowMap.values()){
				// Extract the desired dates for comparison.
				comparisonDate = uow.depreciationSchedule.FAM__Effective_Depreciation_Date__c;
				if (rootObjectTypeAbbreviation == 'RL') comparisonDate =
					(!testingContext.isTest) ?
					uow.receiptLine.createdDate.date() :
					Date.today();
				for (c2g__codaPeriod__c period : manyPeriods)
		    		if (uow.asset.Company__c == period.c2g__OwnerCompany__c && period.c2g__StartDate__c <= comparisonDate && comparisonDate <= period.c2g__EndDate__c){
						uow.periodId = period.Id;
						break;
		    		}
			}
		}
		// Constructors
		private UnitOfWorkMap() {}
		public UnitOfWorkMap(list<FAM__FA_Depreciation_Schedule__c> depreciations, map<id, FAM__Fixed_Asset__c> idToFixedAssetMap){
			rootObjectTypeAbbreviation = 'DS';
			for (FAM__FA_Depreciation_Schedule__c item : depreciations)
				uowMap.put(item.Id, new UnitOfWork(item, idToFixedAssetMap.get(item.id)));
			getCurrencies();
			getPeriods();
		}
		public UnitOfWorkMap(list<SCMC__Receipt_Line__c> receiptLines, map<id, FAM__Fixed_Asset__c> idToFixedAssetMap){
			rootObjectTypeAbbreviation = 'RL';
			for (SCMC__Receipt_Line__c item : receiptLines)
				uowMap.put(item.Id, new UnitOfWork(item, idToFixedAssetMap.get(item.id)));
			getCurrencies();
			getPeriods();
		}
	}

	//==================================================================================================================================
	//  Test Functionality
	//==================================================================================================================================

	public static TestContext testingContext = new TestContext();

	public class TestContext
	{
		public Boolean isTest = false;

		public List<c2g__codaPeriod__c> codaPeriodList = null;
		public List<c2g__codaAccountingCurrency__c> codaCurrencyList = null;
	}
}