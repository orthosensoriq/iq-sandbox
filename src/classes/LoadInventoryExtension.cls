/*
    Copyright (c) 2011 LessSoftware.com, inc.
    All rights reserved.
    
  Class to start batch process to load inventory
*/
public with sharing class LoadInventoryExtension {
    
	private ApexPages.StandardSetController setController;

    //this  constructor is invoked from the multi select on the requisition tab
    public LoadInventoryExtension(ApexPages.StandardSetController controller) {
    	this.setcontroller = controller;
	}
  
    
  public PageReference process() {
    Database.executeBatch(new LoadInventory(),4);        
	// Get the sObject describe result for the Account object
	Schema.DescribeSObjectResult r = Loadinventory__c.sObjectType.getDescribe(); 
	String keyPrefix = r.getKeyPrefix();
	PageReference retPage = new PageReference('/' + keyPrefix + '/o');
    return retPage;  
    
  }
  public PageReference goback() {
    PageReference retPage = setController.cancel();
    return retPage;
  }
  

}