public class IQ_CreateData {
   
    public Account account {get; set;}
    public string recordTypeName {get; set;}
    public Integer recordsToCreate {get; set;}
    public string namePrefix {get; set;}
    public List<manufacturer__c> manufacturers {get;set;}
    
    public IQ_CreateData() {
        //account = [SELECT Id, Name, LastName, FirstName, Site FROM Account
        //          WHERE Id = :ApexPAges.currentPage().getParameters().get('id')];
        account = new Account();
        manufacturers = GetManufacturerCodes();
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Hospital Account', 'Hospital Account'));
        options.add(new SelectOption('Practice Account', 'Practice Account'));
    	return options;
    }
    
    // public void createRecordsToSave() {
    //     //string recordTypeName = 'Hospital Account';       
        
    //     Id recordId = getRecordType();
    //     //System.debug('Record Type Id: ' + string.valueof(recordId));
    //     //System.debug('Account Name: ' + account.Name);
        
    //     Integer recs = Integer.valueof(recordsToCreate);
    //     for (Integer i = 0; i < recs; i++) {
    //         Account acct = CreateAccount();
    //         save(acct);
    //     }
    // }

    public Account CreateAccount() {
        Account acct = new Account();
        Id recordId = getRecordType();
        acct.Name = namePrefix + String.valueOf(createRandomInteger());
        System.debug('Account Name: ' + acct.Name);
        acct.RecordTypeId = recordId;
        //acct.IsPartner = true; 
        insert acct;
        return acct;
    }

    public void save(Account acct) {
       
        Id recordId = getRecordType();
        acct.RecordTypeId = recordId;        
        insert acct;
    }
        
    public static Integer createRandomInteger() {
        Integer upperLimit = 999999;
    	Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }

    public static Integer CreateRandomInteger(Integer upperLimit) {
    	Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }

    public static Date CreateRandomDobDate() {
        Integer upperLimit = 365*40;
        Date startDate = Date.NewInstance(1927,1,1);
    	Integer rand = Math.round(Math.random()*10000);
        System.debug(rand);
        Integer daysToAdd =  Math.mod(rand, upperLimit);
        System.debug(daysToAdd);
        Date newDate = startDate.addDays(daystoAdd);
        return newDate;
    }

    public static Date CreateRandomProcedureDate() {
        Integer upperLimit = 120;
        Date startDate = Date.today().addDays(-60);
    	Integer rand = Math.round(Math.random()*10000);
        System.debug(rand);
        Integer daysToAdd =  Math.mod(rand, upperLimit);
        System.debug(daysToAdd);
        Date newDate = startDate.addDays(daystoAdd);
        return newDate;
    }

    public Id getRecordType() {
        Schema.DescribeSObjectResult r = Schema.Account.SObjectType.getDescribe();
		List<Schema.RecordTypeInfo> rt = r.getRecordTypeInfos();
        
        for (Integer i = 0; i < rt.size(); i++) {
            System.debug(rt[i].Name + ': ' + string.valueof(rt[i].getRecordTypeId()));
        }

        Id recordId = getRecordType(recordTypeName);
        return recordId;
    }

    public Id getRecordType(string recordTypeName) {
        Id recordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        System.debug('Record Type:' + recordTypeName);
        return recordId;
    }

    public List<GenderName> GetFirstNames() {
        List<GenderName> firstNames = new List<GenderName>();
        firstNames.add(new GenderName('Richard', 'M'));
        firstNames.add(new GenderName('Steven', 'M')); 
        firstNames.add(new GenderName('Walter', 'M')); 
        firstNames.add(new GenderName('Gregory', 'M')); 
        firstNames.add(new GenderName('Paul', 'M')); 
        firstNames.add(new GenderName('Mark', 'M')); 
        firstNames.add(new GenderName('Gordon', 'M')); 
        firstNames.add(new GenderName('Daniel', 'M')); 
        firstNames.add(new GenderName('Yannina', 'F')); 
        firstNames.add(new GenderName('Lucia', 'F')); 
        firstNames.add(new GenderName('Sylvia', 'F')); 
        firstNames.add(new GenderName('Natalia', 'F')); 
        firstNames.add(new GenderName('Mary', 'F')); 
        firstNames.add(new GenderName('Marie', 'F')); 
        firstNames.add(new GenderName('Maria', 'F')); 
        firstNames.add(new GenderName('Maryann', 'F')); 
        firstNames.add(new GenderName('Roseanne', 'F')); 
        firstNames.add(new GenderName('Fred', 'M')); 
        firstNames.add(new GenderName('Ivan', 'M')); 
        firstNames.add(new GenderName('George', 'M')); 
        firstNames.add(new GenderName('Margaret', 'F'));
        firstNames.add(new GenderName('Mildred', 'F'));
        firstNames.add(new GenderName('Otto', 'M'));
        firstNames.add(new GenderName('Jonathon', 'M'));
        firstNames.add(new GenderName('Jane', 'F'));
        firstNames.add(new GenderName('Janet', 'F'));
        firstNames.add(new GenderName('Amanda', 'F'));
        firstNames.add(new GenderName('Juan', 'M'));
        firstNames.add(new GenderName('Edgardo', 'M'));
        firstNames.add(new GenderName('William', 'M'));
        firstNames.add(new GenderName('Elias', 'M'));
        firstNames.add(new GenderName('Matthew', 'M'));
        firstNames.add(new GenderName('Robert', 'M'));
        firstNames.add(new GenderName('Samuel', 'M'));
        firstNames.add(new GenderName('James', 'M'));
        firstNames.add(new GenderName('Donald', 'M'));
        firstNames.add(new GenderName('Annette', 'M'));
        firstNames.add(new GenderName('Jerome', 'M'));
        firstNames.add(new GenderName('Phillip', 'M'));
        return firstNames;
    }
     public static List<string> GetLastNames() {
        List<string> lastNames = new List<string>();
        lastNames.add('Richard');
        lastNames.add('Smith');
        lastNames.add('Jones');
        lastNames.add('Johns');
        lastNames.add('Narnacle');
        lastNames.add('Farwood');
        lastNames.add('Schwartz');
        lastNames.add('Morris');
        lastNames.add('Orion');
        lastNames.add('Feelgood');
        lastNames.add('Johnson');
        lastNames.add('Amarillo');
        lastNames.add('Bonzai');
        lastNames.add('Cousins');
        lastNames.add('Galbraith');
        lastNames.add('McCoy');
        lastNames.add('Ginger');
        lastNames.add('Motto');
        lastNames.add('Foley');
        lastNames.add('Waters');
        lastNames.add('Garcia');
        lastNames.add('Weir');
        lastNames.add('Lesh');
        lastNames.add('Hart');
        lastNames.add('Kreutzman');
        lastNames.add('Tweedy');
        lastNames.add('Stirrat');
        lastNames.add('Godchaux');
        lastNames.add('Cantor');
        lastNames.add('Saunders');
        lastNames.add('Wales');
        lastNames.add('Lagin');
        lastNames.add('Williams');
        lastNames.add('Russell');
        lastNames.add('Corea');
        lastNames.add('Jarrett');
        lastNames.add('Metheny');
        lastNames.add('Hamilton');
        lastNames.add('Washington');
        lastNames.add('Goodman');
        lastNames.add('Woodworth');
        return lastNames;
     }

     public GenderName GetRandomFirstName() {
        List<GenderName> names = GetFirstNames();
        return names[CreateRandomInteger(names.size())];
     }

     public String GetRandomLastName() {
        List<String> names = GetLastNames();
        return names[CreateRandomInteger(names.size())];
     }

     public Sensor GetRandomSensor() {
        List<Sensor> sensors = GetSensors();
        return sensors[CreateRandomInteger(sensors.size())];
     }
     
     public static Case_Type__c RetrieveOrCreateCaseType(String caseTypeName) {
         List<Case_Type__c> ctRecs = [Select Id, Name, Description__c From Case_Type__c];
         Case_Type__c ctRec;
         if (ctRecs == null) {
            ctRec = IQ_SurveyRepository.CreateCaseType(caseTypeName);
         } else {
            boolean found = false;
            for (Case_Type__c ct: ctRecs) {
              if (ct.Description__c == caseTypeName) {
                 ctRec = ct;
                 found = true;
              }
            }
            if (!found) {
               ctRec = IQ_SurveyRepository.CreateCaseType(caseTypeName);
            }
        }
        return ctRec;
     }

     public List<Patient__c> GenerateNamesAndDOBs(Integer count) {
        List<Patient__c> patients = new List<Patient__c>();   
        for (Integer i = 0; i < count; i++) {
            Patient__c patient  = new Patient__c();
            GenderName name = GetRandomFirstName();
            patient.Date_of_Birth__c = CreateRandomDobDate();
            patient.First_Name__c = name.name;
            patient.Last_Name__c = GetRandomLastName();
            patient.Gender__c = name.gender;
            patients.add(patient);
        }
        System.debug(patients);
        return patients;
    }

    public List<Patient__c> CreatePatients(String hospitalId, Integer count) {
        List<Patient__c> patients = new List<Patient__c>();
        List<Patient__c> newPatients = GenerateNamesAndDOBs(count);

        Patient__c patient;
        for (Integer i = 0; i < count; i++) {
            Patient__c newPatient = CreatePatient(hospitalId, newPatients[i].Last_Name__c, newPatients[i].First_Name__c, newPatients[i].Date_Of_Birth__c, newPatients[i].Gender__c);
            patients.add(newPatient);
        }
        return patients;
    }
    
    public Patient__c CreatePatient(String hospitalId, string lastName, string firstName, Date dob, string gender) {
      List<Patient__c> patients = [Select Id, Last_Name__c, First_Name__c, Date_Of_Birth__c
      From Patient__c 
      Where Last_Name__c = :lastName AND First_Name__c = : firstName];
      if (patients.size() > 0) {
        return patients[0];
        } else {          
            System.debug('Creating: ' + lastName + ', ' + firstName);
          string medicalRecordNumber = string.ValueOf(Math.round(Math.random() * 100000));
          string dateOfBirthString = DateTime.newInstance(dob.year(), dob.month(), dob.day()).format('MM/d/yyyy');

          Patient__c patient = IQ_PatientActions.CreateNewPatient( hospitalId, lastName, firstName, 
                            medicalRecordNumber, dateOfBirthString, gender, '999-99-9999', 
                            string.ValueOf(Math.random() * 100000), 
                            string.ValueOf(Math.random() * 100000), 'White', 'English', '');                 
         
          return patient;
        }
   }

   public Patient__c CreatePatient(String hospitalId) {
      List<Patient__c> newPatients = GenerateNamesAndDOBs(1);
      return CreatePatient(hospitalId, newPatients[0].Last_Name__c, newPatients[0].First_Name__c, newPatients[0].Date_Of_Birth__c, newPatients[0].Gender__c);

   }

   public Survey__c CreateSurvey(string surveyName, string surveyUrl) {
        Survey__c survey = new Survey__c();
        survey.name__c = surveyName;
        survey.Active__c = true;
        survey.Can_Graph__c = true;
        survey.Survey_URL__c = surveyUrl;
        //survey.Event_Type_Survey__c = eventTypeSurvey.Id;
        insert survey;
        System.debug(survey);
        return survey;
   }
    public Account CreateSurgeonAccount(String practiceId, String lastName, String firstName) {
    	Account surgeon = new Account();
        surgeon.FirstName = firstName;
        surgeon.LastName = lastName;
        surgeon.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        // surgeon.ParentId = practiceId;
        insert surgeon;
        List<Contact> cont = [Select Id, Name, FirstName, LastName From Contact where FirstName=:firstName and LastName =:lastName]; 
        system.debug(cont);
        if (cont.size() > 0) {
            Surgeon_Hospital__c sh = new Surgeon_Hospital__c();
            sh.Surgeon__c = cont[0].Id;
            sh.Hospital__c = practiceId;
            insert sh;
        }

        return surgeon;
     }

//     public List<Case__c> CreatePatientsAndCases(Id hospitalId, Integer count, string caseName, string caseTypeName,    
//       Survey__c survey, 
//       Date procedureDate, 
//       String surgeonId) {
//         List<Case__c> caseRecords = new List<Case__c>();
// 		List<Patient__c> patients = CreatePatients(hospitalId, count);
//         for (Patient__c patient: patients) {
//             Case__c case1 = CreateCase(caseName, caseTypeName, patient, survey, procedureDate, surgeonId, 'Right');
//             caseRecords.add(case1);
//         }
//         return caseRecords;
//    }
  
   public Case__c CreateCase(string caseName, string caseTypeName, 
      Patient__c patient,
      Survey__c survey, 
      Date procedureDate, 
      String surgeonId,
      String laterality) {
        CreateSensorRecords();
          // Patient__c patient = CreatePatient(hospitalId, lastName, firstName);
        // Event_Type__c eventType = IQ_SurveyRepository.GetEventType(eventTypeName);
        Case_Type__c ctRec = IQ_CreateData.RetrieveOrCreateCaseType(caseTypeName);
        System.debug('Creating Case Type record: ' + ctRec);
        Case__c caseRecord = new Case__c();
        caseRecord = IQ_PatientActions.CreateCase(patient.Id, ctRec.Id, DateTime.newInstance(procedureDate.Year(), procedureDate.month(), procedureDate.day()).format('MM/dd/yyyy'), surgeonId, laterality);
        // Event__c event = GetProcedureEvent(caseRecord.ID);
        // System.debug(event);
        // if (event != null) {
        //     if (event.Appointment_Start__c <= Date.today()) {
        //         Sensor sensor = GetRandomSensor();
        //         string id = AddOrRetrieveSensor(sensor);
                
        //         os_Device_Event__c deviceEvent = new os_Device_Event__c();
        //         deviceEvent.Event__c = event.Id;
        //         deviceEvent.OS_Device__c = id;
        //         insert deviceEvent;
        //     }
        // }
        
        System.debug('Creating Case record: ' + caseRecord);

        // insert caseRecord;
        return caseRecord;          
    }

    public string AddOrRetrieveSensor(Sensor sensor) {
        //check to see if its already there
        System.debug(sensor);
        List<OS_Device__c> result = [Select Id, Name from OS_Device__c where Device_ID__c =: sensor.DeviceId AND Style__c =: sensor.style];
        System.debug(result);
        if (result.size() == 0) {
            OS_Device__c device = new OS_Device__c();
            device.Device_ID__c = sensor.DeviceId;
            device.Expiration_Date__c = sensor.ExpirationDate;
            device.Lot_Number__c = sensor.LotNumber;
            device.OSManufacturer__c = sensor.ManufacturerCode;
            device.Manufacturer_Name__c = sensor.ManufacturerName;
            device.Style__c = sensor.Style;
            device.Year_Assembled__c = sensor.YearAssembled;
            insert device;
            return device.Id;
        } else {
             return result[0].Id;
        }

    }

    public void CreateSensorRecords() {
        List<OS_Device__c> result = [Select Id, Name from OS_Device__c];
        System.debug(result);
        List<OS_Device__c> devices = new List<OS_Device__c>();
        List<Sensor> sensors = GetSensors();
        for (Sensor s : sensors) {
            List<OS_Device__c> found = [Select Id, Name from OS_Device__c where Device_ID__c =: s.DeviceId AND Style__c =: s.style and Lot_Number__c =: s.LotNumber];
            if (found.size() == 0) {
                OS_Device__c device = new OS_Device__c();
                device.Device_ID__c = s.DeviceId;
                device.Expiration_Date__c = s.ExpirationDate;
                device.Lot_Number__c = s.LotNumber;
                device.OSManufacturer__c = s.ManufacturerCode;
                device.Manufacturer_Name__c = s.ManufacturerName;
                device.Style__c = s.Style;
                device.Year_Assembled__c = s.YearAssembled;
                devices.add(device);
            }
        }
        insert devices;
    }

    public Event__c GetProcedureEvent(string caseID) {
        Event__c event = [Select ID, Name, Appointment_Start__c from Event__c where Event_Type_Name__c = 'Procedure' and Case__r.Id =:caseId Limit 1];
        return event;
    }

    public Surgeon_Hospital__c GetSurgeonByName(string hospitalId, string lastName, string firstName) {
        List<Surgeon_Hospital__c> surgeons = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name,
        Surgeon__r.FirstName, Surgeon__r.LastName 
                from Surgeon_Hospital__c 
                Where // Hospital__r.IsPartner = true // should probably be able to include this
                // AND 
                Surgeon__r.Account.RecordType.Name = 'Surgeon Account'
                AND Hospital__c =:hospitalId
                AND Surgeon__r.FirstName =:firstName
                And Surgeon__r.LastName =:lastName];
        if (surgeons.size() > 0) {
            return surgeons[0];
        } else {
            return null;
        }
    }
    public Surgeon_Hospital__c GetSurgeonById(string surgeonId) {
        ID recordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        System.debug(surgeonId);
        List<Surgeon_Hospital__c> surgeons = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name,
        Surgeon__r.FirstName, Surgeon__r.LastName 
                from Surgeon_Hospital__c 
                Where // Hospital__r.IsPartner = true // should probably be able to include this
                // AND 
                // Surgeon__r.Account.RecordTypeId = :recordType
                // AND 
                Surgeon__r.Id =:surgeonId];
        System.debug(surgeons);
        if (surgeons.size() > 0) {
            return surgeons[0];
        } else {
            return null;
        }
    }

    public List<Surgeon_Hospital__c> GetSurgeonsByHospital(string hospitalId) {
        List<Surgeon_Hospital__c> surgeons = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name,
        Surgeon__r.FirstName, Surgeon__r.LastName 
                from Surgeon_Hospital__c 
                Where // Hospital__r.IsPartner = true // should probably be able to include this
                // AND 
                Surgeon__r.Account.RecordType.Name = 'Surgeon Account'
                AND Hospital__c =:hospitalId];
        if (surgeons.size() > 0) {
            return surgeons;
        } else {
            return null;
        }
    }

    public static List<manufacturer__c> GetManufacturerCodes() {
        List<Manufacturer__c> codes = [Select ID, Name from Manufacturer__c];
        return codes; 
    }

    public string GetManufacturerCode(string manufacturerName) {
        string code = null;
        for (Manufacturer__c man: manufacturers) {
            if (man.Name == manufacturerName) {
                return man.Id; 
            }
        }
        return code; 
    }
    
    public class GenderName {
        public string name {get; set;}
        public string gender {get; set;}
        public GenderName(string name1, string gender1) {
            name = name1;
            gender = gender1;
        }
    }

    public class Sensor {
        public Date ExpirationDate {get; set;}
        public string DeviceId {get; set;}
        public string LotNumber {get; set;}
        public string ManufacturerCode {get; set;}
        public string ManufacturerName {get; set;}
        public string Style {get; set;}
        public string YearAssembled {get; set;}
        public Sensor(Date ExpirationDate1, string DeviceId1, string LotNumber1, string manufacturerName1, string manufacturerCode1, string style1, string yearAssembled1) {
            ExpirationDate = ExpirationDate1;
            DeviceId = DeviceId1;
            LotNumber = LotNumber1;
            ManufacturerName = manufacturerName1;
            ManufacturerCode = manufacturerCode1;
            Style = style1;
            YearAssembled = yearAssembled1;
        }
    }
    public List<Sensor> GetSensors() {

        List<Sensor> sensors = new List<Sensor>();
        sensors.add(new Sensor(Date.newInstance(2017,11,1), '312465224', '052616V14548', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYCR34-R', '2015'));
        sensors.add(new Sensor(Date.newInstance(2018,3,1), '314036131', '030716V14214', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR12', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2018,8,1), '314393277', '070816V14961', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYCR56-R', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2018,8,1), '314116153', '041816V15038', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2017,5,1), '312702003', '052215V12737', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYBC34-L', '2015')); 
        sensors.add(new Sensor(Date.newInstance(2018,8,1), '314393031', '062916V15038', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2017,6,1), '312781205', '052915V12838', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYCR56-R', '2015')); 
        sensors.add(new Sensor(Date.newInstance(2018,9,1), '314747113', '072116V15188', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYCR34-R', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315185137', '092916V15502', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016'));
        sensors.add(new Sensor(Date.newInstance(2018,8,1), '314744078', '070816V15031', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-JRNYBCS56-R', '2016'));  
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315229021', '092916V15503', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR34', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '314116153', '041816V15038', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016')); 
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315185142', '092916V15502', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016'));
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315185046', '092916V15502', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR56', '2016'));  
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315229129', '092916V15503', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR34', '2016'));
        sensors.add(new Sensor(Date.newInstance(2018,10,1), '315229144', '092916V15503', 'Smith and Nephew', GetManufacturerCode('Smith and Nephew'), 'SNN-LGNCR34', '2016'));
        return sensors;
    }
}
