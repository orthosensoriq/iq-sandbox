@isTest(SeeAllData=true)
public class IQ_PromDeltaAggregateTest {
	public static Event_Form__c eventForm {get; set;}
	public static string caseTypeName = 'Total Knee Arthroplasty';
	
	static IQ_PromDeltaAggregate setup() {
		IQ_PromDeltaAggregate iq = new IQ_PromDeltaAggregate();
		iq.startDate = Date.newInstance(2017, 1,1);
    	iq.endDate  = Date.newInstance(2017, 1,1);
    	iq.monthsToAnalyze = 1; 
    	iq.firstDayOfFirstMonth = Date.newInstance(2017, 1,1);
    	iq.lastDayofLastMonth  = Date.newInstance(2017, 1,1);
    	iq.monthsInThePast = 1;
    	iq.accts = new List<Surgeon_Hospital__c>();
    	iq.currentPreopEvent = new Event__c();
		iq.surgeons = IQ_PatientActions.LoadAllSurgeons();
        return iq;
    }
	
	@isTest
	static void PropertyTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = new IQ_PromDeltaAggregate();
		iq.startDate = Date.newInstance(2017, 1,1);
    	iq.endDate  = Date.newInstance(2017, 1,1);
    	iq.monthsToAnalyze = 1; 
    	iq.firstDayOfFirstMonth = Date.newInstance(2017, 1,1);
    	iq.lastDayofLastMonth  = Date.newInstance(2017, 1,1);
    	iq.monthsInThePast = 1;
    	iq.accts = new List<Surgeon_Hospital__c>();
    	iq.currentPreopEvent = new Event__c();
	}

	@isTest
	static void GetProcessTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		iq.currentSurvey = [Select ID, Name from Survey__c where Name__c = 'KOOS' limit 1];
		System.debug(iq.currentSurvey);
		iq.Process();
	}

	@isTest
	static void GetProcedureTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		List<Event__c> events = iq.GetProcedures();
		System.debug(events);
		System.assert(events.size() > 0);
		Test.stopTest();
	}

	@isTest
	static void RemoveEventsMissingPreopPromTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		iq.currentSurvey = [Select ID, Name from Survey__c where Name = 'SR00000003' limit 1];
		List<Event__c> events = iq.GetProcedures();
		List<Event__c> preopEvents = iq.RemoveEventsMissingPreopProm(events);
		System.debug(preOpEvents);
	 	// TO DO: make this a test that will pass 
		// System.assert(preOpEvents.size() > 0);
		Test.stopTest();
	}

	@isTest
	static void PreopExistsTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		Event_Type__c et = IQ_SurveyRepository.GetEventType('Pre-Op');
		String surveyName= 'KOOS'; 
		Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		// Survey__c survey = IQ_SurveyRepository.GetSurveyByName(surveyId);
		System.debug(survey);
		List<Patient__c> patients = IQ_SurveyRepository.GetPatients();
		System.debug(patients);
		Patient__c patient = IQ_SurveyRepository.GetPatientById(patients[0].Id);
		System.debug(patient);
		Account surgeon = IQ_SurveyRepository.CreateSurgeonAccount();
		system.debug(surgeon);
		Case__c case1 = IQ_SurveyRepository.CreateCase('Test Case', caseTypeName, 
            patient,
            survey, 
            et, 
            Date.today().format(), 
            surgeon.Id,
            'Left');
		//	get
		Event__c evt = [Select Id, Name, Case__c from Event__c where Case__c =:case1.Id limit 1];
		//IQ_SurveyRepository.CreateEvent('Test Event', et, caseTypeName, 
		// survey, patient, Date.today().format(), surgeon.Id, 'Left');
		// evt.Case__c = case1.Id;
		System.debug(evt);
		Event_Form__c form = IQ_SurveyRepository.CreateEventForm('Test Event', evt, survey);
		System.debug(form);
		iq.currentSurvey = survey;
		List<Event_Form__c> preopEvents = iq.GetPreopEvents();
		Boolean exists = iq.PreopExists(evt, preopEvents);
		System.debug(exists);
		// System.assert(exists);
		Test.stopTest();
	}

	@isTest static void GetPreopEvents() {
		IQ_PromDeltaAggregate iq = setup();
		Survey__c survey = IQ_SurveyRepository.CreateSurvey('KOOS');
		System.debug(survey);
		iq.currentSurvey = survey;
		List<Event_Form__c> preopEvents = iq.GetPreopEvents();
	}

	@isTest
	static void SetFollowUpDeltasTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		String surveyName= 'KOOS'; 
	
		Event_Type__c et = IQ_SurveyRepository.GetEventType('Pre-Op');
		Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		List<Patient__c> patients = IQ_SurveyRepository.GetPatients();
		Patient__c patient = IQ_SurveyRepository.GetPatientById(patients[0].Id);
		Account surgeon = IQ_SurveyRepository.CreateSurgeonAccount();
		// Case__c case1 = IQ_SurveyRepository.CreateCase('Test Case', caseTypeName, 
        //     patient,
        //     survey, 
        //     et, 
        //     Date.today().format(), 
        //     surgeon.Id,
        //     'Left');
		//	get
		// Event__c evt = [Select Id, Name, Case__c from Event__c where Case__c =:case1.Id limit 1];
		// IQ_SurveyRepository.CreateEvent('Test Event', et, caseTypeName, 
		// survey, patient, Date.today().format(), surgeon.Id, 'Left');
		// evt.Case__c = case1.Id;
		Event__c event = CreateTestEvent();

		System.debug(event);
		Event_Form__c form = IQ_SurveyRepository.CreateEventForm('Test Event', event, survey);
		System.debug(form);
		iq.currentSurvey = survey;
		List<Event_Form__c> preopEvents = iq.GetPreopEvents();
		Boolean exists = iq.PreopExists(event, preopEvents);
		
		iq.SetFollowUpDeltas();
		Test.stopTest();
	}

	@isTest static void GetPreOpScoresTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		String surveyName= 'KOOS'; 
		Survey__c survey = [Select ID, Name from Survey__c where Name__c =:surveyName Limit 1];
		if (survey == null) {
			survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		}
		iq.currentSurvey = survey;
		List<Event__c> events = iq.GetProcedures();
		System.debug(events);
		events = iq.RemoveEventsMissingPreopProm(events);
		IQ_PromDeltaAggregate.Delta delta1 = iq.deltas[0];
		List<Event_Form__c> formEvents = [select ID, Name, Event__r.case__c, Event__r.Event_Type_Name__c, Event__r.Physician__c, End_Date__c  
                                              from event_form__c 
												where Form_complete__c = true
												and Name = :iq.currentSurvey.Name
												and event__r.case__c = :delta1.caseId];
		List<IQ_PromDeltaAggregate.DeltaScore> test1 = iq.GetPreOpScores(formEvents);
		Test.stopTest();

	}
	@isTest static void GetCreateFormDeltasTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		String surveyName= 'KOOS'; // should be in every Sandbox and production!!
		Survey__c survey = [Select ID, Name from Survey__c where Name__c =:surveyName Limit 1];
		if (survey == null) {
			survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		}
		iq.currentSurvey = survey;
		List<Event__c> events = iq.GetProcedures();
		System.debug(events);
		events = iq.RemoveEventsMissingPreopProm(events);
		IQ_PromDeltaAggregate.Delta delta1 = iq.deltas[0];
		List<Event_Form__c> formEvents = [select ID, Name, Event__r.case__c, Event__r.Event_Type_Name__c, Event__r.Physician__c, End_Date__c, 
		Event__r.Patient_ID__c, Event__r.Patient__c  
                                              from event_form__c 
												where Form_complete__c = true
												and Name = :iq.currentSurvey.Name
												and event__r.case__c = :delta1.caseId];
		List<IQ_PromDeltaAggregate.DeltaScore> test1 = iq.GetPreOpScores(formEvents);
		System.debug(formEvents);
		System.debug(test1);
		iq.CreateFormDeltas(formEvents, test1);
		Test.stopTest();

	}

	@isTest 
	static void GetSurveyScoresTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		Event_Form__c event = CreateTestEventForm();
		iq.GetSurveyScores(event.Id);
		Test.stopTest();
	}
	
	@isTest static void GetSurgeonNameTest() {
		IQ_PromDeltaAggregate iq = setup();
		List<surgeon_Hospital__c> sh = [SELECT Id, Name, surgeon__r.AccountId, surgeon__r.Name FROM Surgeon_hospital__c];
		System.debug(sh);
		String name = iq.GetSurgeonName(sh[0].surgeon__r.AccountId, sh);
		System.debug(name);
		System.assert(name != null);
	}

	@isTest static void GetPracticeNameTest() {
		IQ_CreateData repo = new IQ_CreateData();
		repo.recordTypeName = 'Practice Account';
		Id practiceTypeId = repo.getRecordType('Practice Account');
		Account acct = repo.CreateAccount();
		// insert acct;
		IQ_PromDeltaAggregate iq = new IQ_PromDeltaAggregate();
		String practiceName = iq.GetPracticeName(acct.id, [Select id, Name from Account where recordType.Id = :practiceTypeId]);
		System.debug(practiceName);
		System.assert(practiceName != null);
	}

	@isTest static void GetAllPromDeltasTest() {
		Test.startTest();
		IQ_PromDeltaAggregate iq = setup();
		List<IQ_Prom_Delta__c> deltas = iq.getAllPromDeltas();
		Test.stopTest();
	}

	private static Event__c CreateTestEvent() {
		Event_Type__c et = IQ_SurveyRepository.GetEventType('Pre-Op');
		String surveyName= 'KOOSY'; 
		Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		// Survey__c survey = IQ_SurveyRepository.GetSurveyByName(surveyId);
		System.debug(survey);
		List<Patient__c> patients = IQ_SurveyRepository.GetPatients();
		Patient__c patient = IQ_SurveyRepository.GetPatientById(patients[0].Id);
		System.debug(patient);
		Account surgeon = IQ_SurveyRepository.CreateSurgeonAccount();
		Case__c case1 = IQ_SurveyRepository.CreateCase('Test Case', caseTypeName, 
            patient,
            survey, 
            et, 
            Date.today().format(), 
            surgeon.Id,
            'Left');
		Event__c evt = IQ_SurveyRepository.CreateEvent('Test Event', et, caseTypeName, 
		survey, patient, Date.today().format(), surgeon.Id, 'Left');
		evt.Case__r = case1;
		System.debug(evt);
		return evt;
	}
	private static Event_Form__c CreateTestEventForm() {
		String surveyName= 'KOOSY'; 
		Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyName);
		Event__c evt = CreateTestEvent();
		eventForm = IQ_SurveyRepository.CreateEventForm('Test Event', evt, survey);
		System.debug(eventForm);
		return eventForm;
	}
}
