@isTest
private class EventViewExtensionTest {
    
    static testMethod void testEventViewExtension() {

        //Create Case, Event and Event Forms to use for Testing
        Case__c testCase = new Case__c();
        testCase.Name__c = 'TestCase';
        testCase.Description__C = 'Test description information';
        insert testCase;

        Event__c testEvent = new Event__c();
        testEvent.Event_Name__c = 'Test Event';
        testEvent.Description__C = 'Event description test data';
        testEvent.Status__c = 'Active';
        testEvent.Case__c = testCase.Id;
        insert testEvent;

        Event_Form__c testEventForm = new Event_Form__c();
        testEventForm.Event__c = testEvent.Id;
        testEventForm.Mandatory__c = true;
        testEventForm.Start_Date__c = date.Parse('02/01/2014');
        testEventForm.End_Date__c = date.Parse('04/01/2014');
        insert testEventForm;

        Event_Form__c testEventForm2 = new Event_Form__c();
        testEventForm2.Event__c = testEvent.Id;
        testEventForm2.Mandatory__c = false;
        testEventForm2.Start_Date__c = date.Parse('02/15/2014');
        testEventForm2.End_Date__c = date.Parse('04/15/2014');
        insert testEventForm2;
        
        //Construct controller
        ApexPages.StandardController stdEvent = new ApexPages.StandardController(testEvent);
        EventViewExtension conExt = new EventViewExtension(stdEvent);

        //Execute code
        //conExt.getEventForms();
        conExt.showPopup();
        conExt.closePopup();
        //conExt.showRefreshMsg();
        conExt.GoToImages();
        conExt.GoToDocuments();
        conExt.GoToImplantComponents();
        conExt.GoToForms();
        //conExt.refreshEventForms();
        
    }

}