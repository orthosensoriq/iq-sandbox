public with sharing class SCM_ExportUtilities {
	public static Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();

	public static id getRecordType(String name, SObject userObject){
		ID retId = null;
		// Get the sObject describe result for the userObject
		Schema.DescribeSObjectResult r = userObject.getSObjectType().getDescribe();
		String objName = r.getName();
		List<RecordType> info = rTypes.get(objName);
		if (info == null){
			Map<ID, Schema.RecordTypeInfo> rInfo = r.getRecordTypeInfosByID();
			info = [select id
					,name
					,developerName
				from RecordType
				where id in :rInfo.keySet()];
			rTypes.put(objName, info);
		}
		for (RecordType rType : rTypes.get(objName)){
			if (rType.developerName == name){
				retId = rType.id;
				break;
			}
		}
		System.assertNotEquals(null, retId, name + ' record type not available for ' + objName);
		return retId;
	}
}