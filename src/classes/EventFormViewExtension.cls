public class EventFormViewExtension
{
    final event_form__c eventForm;
    public boolean showSurveyDiv { get; set; }
    public string newSurveyURL { get; set; }
    public list<survey_response__c> surveyResponses { get; set; }
    string retURL = '';	//<-- This is for procedure summary as referer
    osController baseCtrl;
	public EventFormViewExtension(ApexPages.StandardController stdController)
    {
        baseCtrl = new osController(stdController);
        showSurveyDiv = false;
     	eventForm = (Event_Form__c)stdController.getRecord();
        //This is for procedure summary as referer
        try
        {
        	retURL = ApexPages.currentPage().getParameters().get('retURL');
        }
        catch (exception ex)
        {
            retURL = '';
        }
        //End
        newSurveyURL = eventForm.Event_Form_URL__c;
    }

    public void LogConstructor()
    {
        baseCtrl.LogView(); 
        getSurveyResponses();     	
    }
    
    public void getSurveyResponses()
    {
		map<integer, string> params = new map<integer, string>();
		params.put(0,eventForm.Id);
    	surveyResponses = baseCtrl.getSelectedData('EventFormViewResponses', params);
    }
    
    public PageReference Cancel()
    {
        //THIS WAS UPDATED FOR PROCEDURE SUMMARY REFERER
        if(retURL != null && retURL != '')
            return new PageReference(retURL);
        else
	    	return new PageReference('/apex/EventView?id=' + eventForm.Event__c);
    }
    
    public PageReference clearSurveyResponsesandShowSurvey()
    {
        if(surveyResponses.Size() > 0)
        {
            delete surveyResponses;
	        getSurveyResponses();            
        }

        showSurveyDiv = true;
        return null;
    }
    
    public PageReference closeNewSurvey()
    {
        getSurveyResponses();
        if(surveyResponses.size() > 0)
	     	showSurveyDiv = false;   
    
        return null;
    }
    
}