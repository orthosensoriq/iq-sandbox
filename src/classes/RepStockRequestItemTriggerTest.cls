@isTest(seeAllData=true)
public class RepStockRequestItemTriggerTest {
    
    /*TODO take off seeAllData when time permits
    @testSetup 
    static void setupTestData() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        RecordType trRec = [select id from recordtype where name = 'Transfer Request'];
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        User user1 = new User();
        user1.ProfileId = p.id;
        user1.alias = 'user1';
        user1.firstname = 'Test';
        user1.lastname = 'Smith';
        user1.Username = 'test@jamiesmiths.com';
        user1.Email = 'test@jamiesmiths.com';
        user1.CommunityNickname = 'test';
        user1.TimeZoneSidKey = 'America/New_York';
        user1.LocaleSidKey = 'en_US';
        user1.EmailEncodingKey = 'UTF-8';
        user1.LanguageLocaleKey = 'en_US';
        insert user1;
        
        SCMC__Currency_Master__c cm = new SCMC__Currency_Master__c();
        cm.Name = 'USD';
        cm.SCMC__Active__c = true;
        insert cm;
        
        SCMC__ICP__c icp= new SCMC__ICP__c();
        icp.SCMC__Currency__c = cm.id;
        insert icp;
        
        SCMC__Warehouse__c srcWare = new SCMC__Warehouse__c();
        srcWare.name = 'DCOTA';
        srcWare.SCMC__ICP__c = icp.id;
        srcWare.SCMC__Active__c = true;
        srcWare.Receiving_User__c = user1.id;
        insert srcWare;
        
        SCMC__Warehouse__c desWare = new SCMC__Warehouse__c();
        desWare.name = 'Rep - Jamie Smith';
        desWare.SCMC__ICP__c = icp.id;
        desWare.SCMC__Active__c = true;
        desWare.Receiving_User__c = thisUser.id;
        insert desWare;
        
        SCMC__Reason_Code__c rc = new SCMC__Reason_Code__c();
        rc.Name = 'Rep Stock Replenish';
        insert rc;
        
        //SCMC__Destination_Warehouse__c
        SCMC__Transfer_Request__c tr = new SCMC__Transfer_Request__c();
        tr.SCMC__Reason_Code__c = rc.id;
        tr.SCMC__Destination_Warehouse__c = desWare.id;
        tr.SCMC__Source_Warehouse__c = srcWare.id;
        tr.SCMC__Carrier_Service__c = 'FedEx 2Day';
        tr.recordtype = trRec;
        tr.SCMC__Carrier__c = 'FedEx';
        insert tr;
        
        SCMC__Shipping__c shipment = new SCMC__Shipping__c();
        shipment.SCMC__Status__c = 'test';
        shipment.SCMC__Receiving_Warehouse__c = srcWare.id;
        shipment.SCMC__Transfer_Request__c = tr.id;
        insert shipment;
        
        SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
        sn.SCMC__Serial_Number__c = '0123456789';
        insert sn;
        
        SCMC__Serial_Number_Related_Record__c serialNumRelatedRecs = new SCMC__Serial_Number_Related_Record__c();
        serialNumRelatedRecs.SCMC__Serial_Number__c = sn.id;
        serialNumRelatedRecs.SCMC__Shipping__c = shipment.id;
        insert serialNumRelatedRecs;
        //SCMC__Shipping__c shipment = new SCMC__Shipping__c();
        
        
        
    }
    */
    
    @isTest
    public static void repStockItemTest() {
        
        /*Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User user1 = [select id, name from user where alias = 'user1'];
        SCMC__Warehouse__c srcWare = [select id, name, SCMC__Active__c, Receiving_User__c from SCMC__Warehouse__c where Receiving_User__c = :user1.id];
        SCMC__Warehouse__c desWare = [select id, name, SCMC__Active__c, Receiving_User__c from SCMC__Warehouse__c where Receiving_User__c != :user1.id limit 1];
        SCMC__Transfer_Request__c transRequest = [select id from SCMC__Transfer_Request__c limit 1];
        SCMC__Shipping__c shipment = [select id, SCMC__Status__c, SCMC__Receiving_Warehouse__c, SCMC__Transfer_Request__c from SCMC__Shipping__c limit 1];
        List<SCMC__Serial_Number_Related_Record__c> serialNumRelatedRecsSize = [select id, name from SCMC__Serial_Number_Related_Record__c where SCMC__Shipping__c = :shipment.id];*/
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User thisUser = [select Id from User where ProfileId = :p.id and isActive = true limit 1];
        
        System.RunAs(thisUser){
            //Rep_Stock_Request__c rsr = [select id, SCMC__Item__c from Rep_Stock_Request__c where SCMC__Item__c != null limit 1];
            
            SCMC__Item__c item = [
                select id, name
                from SCMC__Item__c
                where name = 'SYK-TRCR02' 
                limit 1
            ];
            
            Rep_Stock_Request_Item__c rsr = [
                select id, Item_Master__c, Quantity__c, Rep_Stock_Request__c 
                from Rep_Stock_Request_Item__c 
                where Rep_Stock_Request__c != null 
                and Item_Master__c != null 
                and Item_Master__r.name != 'SYK-TRCR02' 
                and Quantity__c != null 
                limit 1
            ];
            
            Rep_Stock_Request_Item__c rs = new Rep_Stock_Request_Item__c();
            rs.Rep_Stock_Request__c = rsr.Rep_Stock_Request__c;
            rs.Item_Master__c = item.id;
            rs.Quantity__c = rsr.Quantity__c;
            insert rs;
        }
    }
}