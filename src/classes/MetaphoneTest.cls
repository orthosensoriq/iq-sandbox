@isTest
private class MetaphoneTest {
	static testMethod void myUnitTest() {
		
		Test.startTest();
		
		Metaphone m = new Metaphone();
		m.encode('Test');
		m.encode('Smith');
		m.encode('Jones');
		m.encode('Ohio');
		m.encode('Adams');
		m.encode('Blaine');
		m.encode('Charles');
		m.encode('Greg');
		m.encode('Kimball');
		m.encode('Peter');
		m.encode('Xavier');
		m.encode('Wilson');
		m.encode('Sci');
		m.encode('Cia');
		m.encode('Sch');
		m.encode('Gned');
		m.encode('Sk');
		m.encode('Que');
		m.encode('Sia');
		m.encode('Tia');
		m.encode('Tch');
		m.encode('Zed');
		m.encode('');
		m.encode('X');
		m.encode('Ae');
		m.encode('Wr');
		m.encode('Wh');
		m.encode('Mb');
		m.encode('Dgi');
		
		m.isMetaphoneEqual('Alpha','Beta');
		m.setMaxCodeLen(2);
		
		Test.stopTest();
	}
}