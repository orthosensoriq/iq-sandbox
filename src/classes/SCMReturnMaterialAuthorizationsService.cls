/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
public with sharing class SCMReturnMaterialAuthorizationsService {

	public static List<SCMC__Return_Material_Authorization__c> updateValueField(List<SCMC__Return_Material_Authorization__c> records)
	{
		// Sanity checks.
		if (records == null || records.isEmpty())
			return records;

		// Get a set of Sales Order Line Ids.
		Set<Id> idSet = new Set<Id>();
		for (SCMC__Return_Material_Authorization__c item : records)
			if (item.SCMC__Sales_Order_Line_Item__c != null)
				idSet.add(item.SCMC__Sales_Order_Line_Item__c);

		// Exit now, if the supplied records did not contain any Sales Order Line Ids.
		if (idSet.isEmpty())
			return records;

		// Get a map of the related Picklist Detail records, having the Sales Order Line for the key.
		Map<Id, SCMC__Pick_list_Detail__c>  picklistDetailMap = new Map<Id, SCMC__Pick_list_Detail__c>();
		for (SCMC__Pick_list_Detail__c item : getPicklistDetails(idSet, PicklistDetailSelector.SalesOrderLine))
			picklistDetailMap.put(item.SCMC__Sales_Order_Line_Item__c, item);

		// Assign the values.
		SCMC__Pick_list_Detail__c picklistDetail = null;
		for (SCMC__Return_Material_Authorization__c item : records) {
			if (item.SCMC__Sales_Order_Line_Item__c == null)
				continue;
			picklistDetail = picklistDetailMap.get(item.SCMC__Sales_Order_Line_Item__c);
			if (picklistDetail == null)
				continue;
			item.Value__c = picklistDetail.SCMC__Cost__c;
		}

		return records;
	}

	private enum PicklistDetailSelector {Picklist, ProductionOrderLine, SalesOrderLine}
	private static List<String> additionalPicklistDetailFields = new List<String>();
	private static List<SCMC__Pick_list_Detail__c> getPicklistDetails(Set<Id> idSet, PicklistDetailSelector selector)
	{
		List<SCMC__Pick_list_Detail__c> result = new List<SCMC__Pick_list_Detail__c>();

		// Sanity checks.
		if (idSet == null || idSet.isEmpty())
		{
			return result;
		}

		// Construct the SOQL
		List<String> fieldList = new List<String>(SCMC__Pick_list_Detail__c.sObjectType.getDescribe().fields.getMap().keySet());
		fieldList.addAll(additionalPicklistDetailFields);
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Pick_list_Detail__c ';
		if (selector == PicklistDetailSelector.Picklist) {
			soql += 'where SCMC__Picklist__c in :idSet';
		} else if (selector == PicklistDetailSelector.ProductionOrderLine) {
			soql += 'where SCMC__Production_Order_Line__c in :idSet';
		} else if (selector == PicklistDetailSelector.SalesOrderLine) {
			soql += 'where SCMC__Sales_Order_Line_Item__c in :idSet';
		}

		// Convert SObject results to desired objects.
		List<SObject> newObjects = Database.query(soql);
		for (SObject obj : newObjects)
		{
			result.add((SCMC__Pick_list_Detail__c)obj);
		}

		return result;
	}
}