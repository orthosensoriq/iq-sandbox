public class customAccount {
    public Account account {get; set;}
    public string recordTypeName {get; set;}
    
    public customAccount() {
        //account = [SELECT Id, Name, LastName, FirstName, Site FROM Account
        //          WHERE Id = :ApexPAges.currentPage().getParameters().get('id')];
                  account = new Account();
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Hospital Account', 'Hospital Account'));
        options.add(new SelectOption('Practice Account', 'Practice Account'));
    	return options;
    }
    
    public void save() {
        //string recordTypeName = 'Hospital Account';
        System.debug('Record Type Id: ' + recordTypeName);
        Id recordId = getRecordType();
        System.debug('Record Type Id: ' + string.valueof(recordId));
        System.debug('Account Name: ' + account.Name);
        account.RecordTypeId = recordId;        
        insert account;
    }
    private Id getRecordType() {
        Schema.DescribeSObjectResult r = Schema.Account.SObjectType.getDescribe();
		List<Schema.RecordTypeInfo> rt = r.getRecordTypeInfos();
        
        for (Integer i = 0; i < rt.size(); i++) {
            System.debug(rt[i].Name + ': ' + string.valueof(rt[i].getRecordTypeId()));
        }

        Id recordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();

        return recordId;
    }
}