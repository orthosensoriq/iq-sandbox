global without sharing class IQ_AggregateNonCompliance {
    
    public IQ_AggregateNonCompliance() {

    }

    //  There are several types of compliance defined in our initial requirements
    //  The first is users that have not filled in their surveys as requested.
    //  The second is surveys or procedures that have data but the "event" has not been marked as Complete.
    //  
    //  To retrieve missing or late surveys we would need to go through events 
    //  (eventForm object?), query the database for related surveys and write without
    //  the missing information to a table object.
    //
    //  Missing completion dates are queried in a similar way. The events
    //  cycled through and determine if 1) its a procedure, the images have been uploaded 
    //  and 2) if its a different type of event (not a procedure), look to see if all proms
    //  are completed. I fall are complete and the event is not marked as complete.

    // Question - do we assume that completed events have their surveys filled out?
    // do we assume if that all surveys are filled out the items should be complete?

    // for both of these situations, the report should have a button to either mark the 
    // event as completed or send an email to relevant patient requesting the survey be filled out.

    public List<Event_Form__c> EventsWithMissingSurveys() {
        return [Select ID, Name from Event_Form__c ];
    }
}



    