@isTest(SeeAllData=true)
private class SCMSalesOrderLineItemTriggerHandlerTest {
	
	static SCMSetUpTest testClass = new SCMSetUpTest();
	
    static testMethod void salesOrderLinePricingTest() {
    	
    	Account acc = testClass.createTestCustomerAccount(true, true, false, null);
    	SCMC__Item__c item = testClass.createTestItem();
		system.debug('+++++ item: ' + item);

	   	SCMC__Price_List__c pList = new SCMC__Price_List__c(
			Name = item.Name
			,SCMC__Exists_in_Item_Master__c = true
			,SCMC__Quote_Valid_For__c = 365);
		insert pList;
		system.debug('+++++ pList: ' + pList);
		
		SCMC__Price__c tempprice = new SCMC__Price__c();
		id rID = SCMUtilities.getRecordType('item', tempprice);
		
    	SCMC__Price__c price = new SCMC__Price__c(
			SCMC__Item_Number__c = pList.Id
			,SCMC__Price__c = 100.00
			,SCMC__Pricing_Type__c = 'Standard'
			,RecordTypeID = rID);
		insert price;
		system.debug('+++++ price: ' + price);
		SCMC__Sales_Order__c so = testClass.createTestSalesOrder(acc, true);

    	SCMC__Sales_Order_Line_Item__c sol = testClass.createSalesOrderLine(so, item, 'New',false);
    	sol.Based_on_Price_List__c = true;
    	sol.SCMC__Price__c = 100;
		upsert sol;
		system.debug('+++++ sol: ' + sol);
		
		test.startTest();
			SCMC__Sales_Order_Line_Item__c updateSOL = [select ID, SCMC__Price__c From SCMC__Sales_Order_Line_Item__c where Id = :sol.Id];
			system.assertEquals(100.00, updateSOL.SCMC__Price__c, 'Price Amount is not set from Price list');
		test.stopTest();

    }

    static testMethod void salesOrderAddExistingPONumber() {
    	
    	Account acc = testClass.createTestCustomerAccount(true, true, false, null);
    	SCMC__Item__c item = testClass.createTestItem();
		system.debug('+++++ item: ' + item);

		SCMC__Sales_Order__c so = testClass.createTestSalesOrder(acc, false);
		so.SCMC__Customer_Purchase_Order__c = 'PO12345';

		upsert so;

    	SCMC__Sales_Order_Line_Item__c sol = testClass.createSalesOrderLine(so, item, 'New',false);
    	sol.Based_on_Price_List__c = true;
    	sol.SCMC__Price__c = 100;
		upsert sol;
		
		test.startTest();
		
			boolean errorOccur = false;
			try{
				SCMC__Sales_Order__c so2 = testClass.createTestSalesOrder(acc, true);
				so2.SCMC__Customer_Purchase_Order__c = 'PO12345';
				update so2;
			} catch (Exception ex){
				errorOccur = true;
			}	
			system.assertEquals(errorOccur, true, 'Should be a warning message to user. ');

		test.stopTest();		
    }
}