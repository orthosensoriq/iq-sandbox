@isTest(SeeAllData=true)
public class RHX_TEST_SCM_Revenue {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM SCM_Revenue__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new SCM_Revenue__c()
            );
        }
    	Database.upsert(sourceList);
    }
}