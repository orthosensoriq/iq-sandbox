/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2016 FinancialForce.com, inc. All rights reserved.
*/
global with sharing class SCMFFA_APVoucherExportPlugin extends SCMFFA.SCMFFAPlugin
{

	global SCMFFA_APVoucherExportPlugin()
	{
		throwException = true;
	}

	global override void execute(
		Map<Id, sObject> scmIdToFFARecord,
		Map<Id, sObject[]> scmIdToFFALineRecords,
		Map<Id, sObject[]> scmIdToFFALineRecords2
	)
	{
		for (SObject obj : scmIdToFFARecord.values()) {
			c2g__codaPurchaseInvoice__c pin = (c2g__codaPurchaseInvoice__c) obj;
			pin.c2g__DeriveCurrency__c = true;
			pin.c2g__CopyAccountValues__c = true;
		}
	}
}