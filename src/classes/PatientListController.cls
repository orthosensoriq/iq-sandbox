public with sharing class PatientListController {
	
	public String searchLastName {get;set;}
    public Id PatientID {get;set;}
    //public List<Event__c> patientlines {get;set;}
    public List<PatientWrapper> patientlines {get;set;}
	public String[] selectedHospitals = new String[]{};
    
	// constructor
    Public PatientListController() {
        //patientlines = new List<Event__c>();
        patientlines = new List<PatientWrapper>();
    }

	// Get Listing of Patient records
	//public ApexPages.StandardSetController PatientsList {
	public list<PatientWrapper> PatientsList {
        get {
            if(PatientsList == null) {
         /*       String viewQuery = 'SELECT Id, First_Name__c, Last_Name__c, Date_Of_Birth__c, Account_Number__c,';
                viewQuery = viewQuery + ' Hospital__r.Name, Gender__c, Medical_Record_Number__c FROM Patient__c';
                viewQuery = viewQuery + ' WHERE Active__c = true AND Last_Name__c != null ORDER BY Last_Name__c LIMIT 10000';
                //viewQuery = viewQuery + ' WHERE Active__c = true AND Last_Name__c LIKE \'%'+searchLastName+'%\' ORDER BY Last_Name__c LIMIT 10000';

                String viewQuery = 'SELECT Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Patient__r.Account_Number__c,';
                viewQuery = viewQuery + ' Location__r.Name, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Medical_Record_Number__c FROM Event__c';
                viewQuery = viewQuery + ' WHERE Case__r.Patient__r.Active__c = true AND Case__r.Patient__r.Last_Name__c != null ORDER BY Case__r.Patient__r.Last_Name__c LIMIT 10000';
                system.debug('viewQuery =====> '+viewQuery);
         */
                String viewQuery = 'SELECT Count(Id), Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Patient__r.Account_Number__c,';
                viewQuery = viewQuery + ' Location__r.Name hospital, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Medical_Record_Number__c FROM Event__c';
                viewQuery = viewQuery + ' WHERE Case__r.Patient__r.Active__c = true AND Case__r.Patient__r.Last_Name__c != null ';
                viewQuery = viewQuery + ' GROUP BY Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Patient__r.Account_Number__c,';
                viewQuery = viewQuery + ' Location__r.Name, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Medical_Record_Number__c';
                viewQuery = viewQuery + ' ORDER BY Case__r.Patient__r.Last_Name__c LIMIT 10000';
                system.debug('viewQuery =====> '+viewQuery);

                //PatientsList = new ApexPages.StandardSetController(Database.getQueryLocator(viewQuery));
                list<aggregateResult> patients = (list<aggregateResult>)Database.query(viewQuery);
                PatientsList = buildWrapper(patients);
            }
            //PatientsList.setPageSize(1000);
            return PatientsList;
        }
        set;
    }
    
    private list<PatientWrapper> buildWrapper(list<aggregateResult> pats)
    {
        list<patientWrapper> wrapper = new list<patientWrapper>();
        for (aggregateResult ar : pats)
        {
            wrapper.add(new PatientWrapper(string.valueOf(ar.get('Patient__c')), string.valueOf(ar.get('First_Name__c')), string.valueOf(ar.get('Last_Name__c')), 
                                           date.valueOf(ar.get('Date_Of_Birth__c')), string.valueOf(ar.get('Account_Number__c')), string.valueOf(ar.get('hospital')),
                                           string.valueOf(ar.get('Gender__c')), string.valueOf(ar.get('Medical_Record_Number__c'))));
        }
        return wrapper;
    }

    public void getResults() {
        if(searchLastName == null || searchLastName == '')
        {
            patientlines.clear();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter a Lastname value to perform the search');
            ApexPages.addMessage(myMsg);
        }
        else
        {
            String searchName = '%'+searchLastName+'%';/*
			String searchQuery = 'SELECT Id,First_Name__c,Last_Name__c,Date_Of_Birth__c,Account_Number__c,Hospital__r.Name,Gender__c,';
            searchQuery = searchQuery + ' Medical_Record_Number__c FROM Patient__c WHERE Last_Name__c LIKE '+'\'' + searchName+'\'';
            
            if (selectedHospitals.size() > 0) {
                String hospitalIDs = '\'' + String.join(selectedHospitals,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Hospital__c IN ('+hospitalIDs+')';
            }
			*/
			//String searchQuery = 'SELECT Case__r.Patient__c,Case__r.Patient__r.First_Name__c,Case__r.Patient__r.Last_Name__c,Case__r.Patient__r.Date_Of_Birth__c,Case__r.Patient__r.Account_Number__c,Location__r.Name,Case__r.Patient__r.Gender__c,';
            //searchQuery = searchQuery + ' Case__r.Patient__r.Medical_Record_Number__c FROM Event__c WHERE Case__r.Patient__r.Last_Name__c LIKE '+'\'' + searchName+'\'';
            //
			String searchQuery = 'SELECT Count(Id), Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Patient__r.Account_Number__c,';
                searchQuery = searchQuery + ' Location__r.Name hospital, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Medical_Record_Number__c FROM Event__c';
            	searchQuery = searchQuery + ' WHERE Case__r.Patient__r.Active__c = true AND Case__r.Patient__r.Last_Name__c LIKE '+'\'' + searchName+'\'';
            
            if (selectedHospitals.size() > 0) {
                String hospitalIDs = '\'' + String.join(selectedHospitals,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Location__c IN ('+hospitalIDs+')';            
            }

            searchQuery = searchQuery + ' GROUP BY Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Patient__r.Account_Number__c,';
            searchQuery = searchQuery + ' Location__r.Name, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Medical_Record_Number__c';
            
            searchQuery = searchQuery + ' ORDER BY Case__r.Patient__r.Last_Name__c LIMIT 1000';
            system.debug('searchQuery =====> '+searchQuery);
            list<aggregateResult> patients = (list<aggregateResult>)Database.query(searchQuery);
            system.debug('Patients.size: ' + patients.size());
            patientlines = buildWrapper(patients);
            
            system.debug('PatientsList.size: ' + patientlines.size());
            if(patientlines.size()==0 || patientlines == null)
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results were found');
              ApexPages.addMessage(myMsg);
            }
        }
    }
	
	// Initialize Patients and return a list of records 
    //public List<Patient__c> getPatients() {
    //    return (List<Patient__c>) PatientsList.getRecords();
    //}
    public List<PatientWrapper> getPatients() {
        return (List<PatientWrapper>) PatientsList; //.getRecords();
    }

    
    // Get previous set of records
    //public void previous() {
    //    PatientsList.previous();
    //}
    
    // Get next set of records
    //public void next() {
    //    PatientsList.next();
    //}

    // Flag if there are more records
    //public Boolean getHasNext(){
    //    return PatientsList.getHasNext();
    //}
    
    // Flag if there are previous records
    //public Boolean getHasPrevious(){  
    //    return PatientsList.getHasPrevious();
    //}

    // Get list size
    public Integer getListSize() {
        Integer i=0;
        if(PatientsList!=null) i= patientsList.size(); //PatientsList.getResultSize();
        return i;
    }
    
    // ReDirect to the Patient View Page
    public PageReference goToPatientView(){
		PageReference pageRef = new PageReference('/apex/PatientView?id=' + PatientID);
		return pageRef;
	}

	public String[] getHospitals() {
        return selectedHospitals;
    }

    public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }    
    
    public List<selectOption> getHospitalItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE isPartner = true and RecordType.Name = 'Hospital Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }
    
    public class PatientWrapper
    {
        public string PatientId {get;set;} 
        public string PatientFirstName {get;set;}
        public string PatientLastName {get;set;}
        public Date PatientDOB {get;set;}
        public string AccountNumber {get;set;}
        public string Hospital {get;set;}
        public string PatientGender {get;set;}
        public string MedicalRecNum {get;set;}
        
        public PatientWrapper(string patId, string patfn, string patln, date patdob, string patacct, string hosp, string patgend, string patmrn)
        {
            PatientId = patId;
            PatientFirstName  = patfn;
            PatientLastName = patln;
            PatientDOB = patdob;
            AccountNumber  = patacct;
            Hospital = hosp;
            PatientGender = patgend;
            MedicalRecNum = patmrn;
        }
    }
}