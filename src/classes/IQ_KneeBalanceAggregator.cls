global without sharing class IQ_KneeBalanceAggregator implements Schedulable {
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Integer monthsToAnalyze {get; set;}
    public Date firstDayOfFirstMonth {get; set; }
    public Date lastDayofLastMonth {get; set;}
    public Integer monthsInThePast {get; set;}
    public List<Surgeon_Hospital__c> accts {get; set;} 
    public List<SensorDataLine> sensorDataLines {get;set;}

    public IQ_KneeBalanceAggregator() {
        monthsInThePast = 6;
        SetDates();
        accts = GetParents();
    }

    global void execute(SchedulableContext ctx) {
        AggregateKneeBalanceData(10, 2016, 5, 2017);
    }
    
    public void AggregateKneeBalanceData(Integer startMonth, Integer startYear, Integer endMonth, Integer endYear) {
         // empty month data TO DO!!!
        List<IQ_MonthlySensorData__c> toDelete = [Select ID, Name from IQ_MonthlySensorData__c];
        delete toDelete;
       
        Integer months = 0;
        Integer years = endYear - startYear;
        System.debug(years);
        if (years > 0) {
           months = ((13 - startMonth) + endMonth) + ((years - 1) * 12);
           System.debug(months);
        } else {
           System.debug('Here');
                months = endMonth - startMonth;
        }
        System.debug(months);
        accts = GetParents();
        System.debug(accts);
        for (Integer i = 0; i < months; i++) {
                Integer month = 0;
                if (startMonth + i > 12) {
                    month = startMonth + i - 12;
                } else { 
                    month = startMonth + i;
                }
                Integer year = 2016;
                if (startMonth + i > 12) {
                    year = startYear + 1; // fix for more than one year (Integer)Math.floor((startMonth + 1)/12);
                } else { 
                    year = startYear;
                }
                System.debug(month +  '/' + year);
                AggregateMonthKneeBalanceData(month, year);
        }
    }

    public List<IQ_MonthlySensorData__c> GetAllMonthlySensorData() {
        return [Select ID, Name, ActualFlex__c, ActualFlexString__c, Event_ID__c, Device_ID__c, Surgeon_ID__c, Surgeon_Name__c, LateralLoad__c, 
                MedialLoad__c, MeasurementDate__c, Rotation__c, TibiaRot__c, TibiaTilt__c, Practice_ID__c
                from IQ_MonthlySensorData__c];
    }

    public void AggregateMonthKneeBalanceData(Integer month, Integer year) {
        // go through months and get events from each event regardless if the event is marked complete;
        Date eventStartDate = Date.newInstance(year, month, 1);
        Date eventEndDate = eventStartDate.AddDays(Date.daysInMonth(year, month)-1);
        // get list of events - (we may not need all of these fields)
        List<AggregateResult> sensorDataLines = GetSensorDataLines(eventStartDate, eventEndDate);
        System.debug(sensorDataLines); 
        
        List<IQ_MonthlySensorData__c> monthlySensorData = new List<IQ_MonthlySensorData__c>();
        List<Event__c> events = GetEventDetails(month, year);
        // loop though the procedures
        List<SensorDataLine> allSensors = new List<SensorDataLine>();
        // retrieve monthly list of surgeon procedures
        for (AggregateResult ar: sensorDataLines) {
            System.debug((string)(ar.get('Event__c')));
            List<SensorDataLine> sensors = LoadSensorDataLines((string)(ar.get('Event__c')));
            System.debug(sensors);

            // get by degree
            List<string> degrees = new List<string> {'10','45', '90'};
            if (sensors.size() > 0 ) {
                // Event__c event = GetEventDetails(sensors[0].SD_EventID);
                Event__c event = GetEventDetails((string)ar.get('Event__c'), events);
                System.debug(event);
                // if (event != new Event__c()) {
                    for (string degree: degrees) {
                        // get degree records and determine last
                        SensorDataLine degreeEvent = GetDegreeByEvent(degree, sensors, event);
                        if (degreeEvent.AttachmentID != '') { //check to make sure null works for no records found....
                            System.debug('Trying to add');
                            System.debug(degreeEvent);
                            System.debug(event);
                            IQ_MonthlySensorData__c eventDegree = ConvertDataToAggregateFormat(degreeEvent, event);
                            monthlySensorData.add(eventDegree);
                        }
                    }
                 // }
            }
        }
        System.debug(monthlySensorData);
        insert monthlySensorData;
    }

    public SensorDataLine GetDegreeByEvent(string degree, List<SensorDataLine> sensors, Event__c event) {
        System.debug('evaluating sensor with degree: ' + degree);
        List<SensorDataLine> degreeSensor = new List<SensorDataLine>();
        for (SensorDataLine sensor: sensors) {
            // System.debug(integer.valueof(sensor.SD_Type_ActualFlex));
            if (integer.valueof(sensor.SD_Type_ActualFlex) == integer.valueOf(degree)) {
                System.debug(degree + ' found');
                degreeSensor.add(sensor);
            }
        }
        SensorDataLine lastDegreeSensor = new SensorDataLine();
        if (degreeSensor.size() > 0) {
            lastDegreeSensor = degreeSensor[0];
            
            for (SensorDataLine toCompare: degreeSensor) {
                if (lastDegreeSensor.SD_MeasurementDate < toCompare.SD_MeasurementDate) {
                    lastDegreeSensor = toCompare;
                }
            }
        }
        System.debug(lastDegreeSensor);
        return lastDegreeSensor;
    }
        
    public IQ_MonthlySensorData__c ConvertDataToAggregateFormat(SensorDataLine sensor, Event__c event) {
        IQ_MonthlySensorData__c aggregateRecord = new IQ_MonthlySensorData__c();
            // aggregateRecord.Event_ID__c         = sensor.SD_EventID;
            aggregateRecord.Event_ID__c         = event.ID;
            aggregateRecord.ActualFlex__c       = sensor.SD_Type_ActualFlex;

            Integer tempFlex = (Integer)sensor.SD_Type_ActualFlex.round();
            System.debug(tempFlex);
            aggregateRecord.ActualFlexString__c       = string.valueOf(tempFlex);
            aggregateRecord.LateralLoad__c      = sensor.SD_Type_LateralLoad;
            aggregateRecord.Surgeon_ID__c       = event.Physician__c;
            aggregateRecord.Surgeon_Name__c     = event.Physician__r.Name;
            Surgeon_Hospital__c acct = GetParent(event.Physician__c);
            if (acct.Hospital__r.Id != null) {
                aggregateRecord.Practice_ID__c      = acct.Hospital__r.Id;
            }
            aggregateRecord.Device_ID__c        = sensor.SD_DeviceID;
            aggregateRecord.MeasurementDate__c  = sensor.SD_MeasurementDate;
            aggregateRecord.PatientID__c        = event.Patient_ID__c;
            aggregateRecord.PatientName__c      = event.Patient__c;
            aggregateRecord.ProcedureDate__c    = Date.newInstance(event.Appointment_Start__c.year(), event.Appointment_Start__c.month(), event.Appointment_Start__c.day());
            string timeParts = sensor.SD_MeasurementDate.substringBefore(' ');
            System.debug(timeParts);
            aggregateRecord.Measurement_Date__c = DateTime.parse(timeParts + ' 10:00 AM');
            // aggregateRecord.Measurement_Date__c = DateTime.parse(sensor.SD_MeasurementDate);

            aggregateRecord.MedialLoad__c       = sensor.SD_Type_MedialLoad;
            aggregateRecord.TibiaRot__c         = sensor.SD_Type_TibiaRot;
            aggregateRecord.TibiaTilt__c        = sensor.SD_Type_TibiaTilt;          
            aggregateRecord.Rotation__c         = sensor.SD_Type_Rotation;

        return aggregateRecord;
    }

    public Event__c GetEventDetails(string eventId, List<Event__c> events) {
        System.debug(eventId);
        System.debug(events);
        // return new Event__c();
        for (Event__c event: events) { 
            if (event.Id == eventId) {
                return event;
            }
        }    
        return new Event__c();
    }

    public List<Event__c> GetEventDetails(Integer month, Integer year) {
        DateTime startDate = Date.newInstance(year, month, 1);
        DateTime endDate = Date.newInstance(year, month, Date.DaysInMonth(year, month));
        // return new Event__c();
        List<Event__c> events = [Select Physician__c,Physician__r.Name, Appointment_Start__c, Location__c, Patient_ID__c, Patient__c 
                from Event__c
                where Appointment_Start__c >=: startDate
                and Appointment_Start__c <=: endDate];
        System.debug(events.size());
        return events;
    }

    public List<Surgeon_Hospital__c> GetParents() {
        List<Surgeon_Hospital__c> accts = [Select Id, Name, Surgeon__r.Account.Id, Hospital__r.Id
                    from Surgeon_Hospital__c 
                    Where Hospital__r.RecordType.Name = 'Practice Account'];
        System.debug(accts.size());
        return accts;
    } 

    public Surgeon_Hospital__c GetParent(String Id) {
        System.debug(Id);
        Surgeon_Hospital__c acct = new Surgeon_Hospital__c();
        System.debug(accts);
        if (Id != null) {
            for (Surgeon_Hospital__c found: accts) {
                // System.debug(found.Surgeon__r.Account.Id);
                if (found.Surgeon__r.Account.Id == Id) {
                    System.debug('found');
                    return found;
                }
            }
        }
        return acct;
    } 

    public List<SensorDataLine> LoadSensorDataLines(string procedureId){ 
        ProcedureSummaryController controller = new ProcedureSummaryController(procedureId);
        controller.sensorDataLines = new List<SensorDataLine>();
        controller.PopulateTable();
        return controller.sensorDataLines;
    }

    public List<AggregateResult> GetSensorDataLines(Date eventStartDate, Date eventEndDate) { 
        List<AggregateResult> sensorDataLines = [ Select Event__c
                                                from Sensor_Data__c
                                                where  
                                                Event__r.Appointment_Start__c >= :eventStartDate
                                                AND 
                                                Event__r.Appointment_Start__c <= :eventEndDate
                                                GROUP By Event__c];
        return sensorDataLines;
    }
            // if the procedure has data then 
            //add to temporary table 

    public void SetDates() {
        monthsToAnalyze = monthsInThePast ;
        Date today = Date.today();
        System.debug(today);
        Date firstDayofThisMonth = today.toStartofMonth();
        System.debug(firstDayofThisMonth);
        firstDayOfFirstMonth = firstDayofThisMonth.AddMonths(-(monthsInThePast));
        System.debug(firstDayOfFirstMonth);
        lastDayofLastMonth = today.toStartofMonth().AddDays(Date.daysInMonth(today.year(), today.month())-1);
        //System.debug(lastDayofLastMonth);
    }
}