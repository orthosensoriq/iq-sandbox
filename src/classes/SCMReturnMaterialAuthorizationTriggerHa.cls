/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/

// Name truncated at the SalesForce limit of 40 characters.
public with sharing class SCMReturnMaterialAuthorizationTriggerHa {

	private boolean isExecuting = false;
	private integer batchSize = 0;

	public SCMReturnMaterialAuthorizationTriggerHa(boolean isExecuting, integer size) {
		this.isExecuting = isExecuting;
		this.batchSize = size;
	}

	public void OnBeforeInsert(List<SCMC__Return_Material_Authorization__c> newItems)
	{
		SCMReturnMaterialAuthorizationsService.updateValueField(newItems);
	}
}