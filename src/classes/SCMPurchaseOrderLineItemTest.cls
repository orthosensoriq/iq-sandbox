/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Suite of tests for SCMPurchaseOrderLineItem.
**/
@isTest(SeeAllData=true)
private class SCMPurchaseOrderLineItemTest {

    static testMethod void RecordTypesIncludeCapitalEquipment() {
        map<string, id> recordTypeNameToIdMap = SCMPurchaseOrderLineItem.getRecordTypeIds();
        system.assert(recordTypeNameToIdMap.get('Capital_Equipment') != null, 'The record types should include this item.');
    }
}