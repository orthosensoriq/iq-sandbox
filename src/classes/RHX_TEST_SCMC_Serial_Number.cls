@isTest(SeeAllData=true)
public class RHX_TEST_SCMC_Serial_Number {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM SCMC__Serial_Number__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new SCMC__Serial_Number__c()
            );
        }
    	Database.upsert(sourceList);
    }
}