@isTest
private class ProcedureSummaryControllerTest {
     static testMethod void myUnitTest() {
     	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('MeasurementsMock');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		
		Test.setMock(HttpCalloutMock.class, mock);
		 	

          //Event__c testprocedure = [Select id from Event__c where Case__r.Patient__c!=null AND Case__r.Patient__r.Date_Of_Birth__c!=null AND Case__r.Patient__r.Date_Of_Birth__c<TODAY LIMIT 1];
          //Account testAccount = [Select Id from Account WHERE IsPartner=TRUE Limit 1];
          Account testAccount = new Account();
          testAccount.Name = 'Test Account 1';
          //testAccount.IsPartner = true;
          Insert testAccount;

          Diagnosis__c diag = new Diagnosis__c();
          diag.Code__c='715.00';
          diag.Long_Name__c = 'Test Diag Code';
          insert diag;

          Patient__c testPatient = new Patient__c();
          testPatient.First_Name__c = 'Test';
          testPatient.Last_Name__c = 'Tester';
          testPatient.Date_Of_Birth__c = date.newInstance(1970, 4, 3);
          insert testPatient;

          Case__c newCase = new Case__c();
          newCase.Name__c = 'Test Case';
          newCase.Patient__c = testPatient.id;
          Insert newCase;

          Event_Type__c evtType = new Event_Type__c();
          evtType.Name = 'TestEventType';
          evtType.Primary__c = true;
          Insert evtType;

          Event__c newEvent = new Event__c();
          newEvent.Event_Name__c = 'Test Procedure';
          //newEvent.Event_Type__c = evtType.Id;
          newEvent.Location__c = testAccount.id;
          newEvent.Case__c = newCase.Id;
          Insert newEvent;
          
         /*
          Hospital_LinkStation__c hl = new Hospital_LinkStation__c();
          hl.Account__c = testAccount.id;
          hl.LinkStation_Serial_Number__c = '123456';
          insert hl;
         */
          FAM__FA_Depreciation_Method__c dm = new FAM__FA_Depreciation_Method__c();
          dm.Name = 'SL';
          dm.FAM__Calculation_Class__c = 'FAM.DM_StraightLine';    
          insert dm;
         
          FAM__Fixed_Asset__c hl = new FAM__Fixed_Asset__c();
          hl.Deployed_To__c = testAccount.id;
          hl.Serial_Number__c = '123456';
          hl.FAM__Date_In_Service__c = system.today();
          hl.FAM__Depreciation_Method__c = dm.id;
          hl.FAM__Asset_Cost__c = 1;
          hl.FAM__Salvage_Value__c = 1;
          hl.FAM__Service_Life_in_Years__c = 1;
          hl.FAM__Status__c = 'New';
          hl.FAM__Asset_Description__c = 'Test Linkstation A';
          insert hl;

          Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
          testClinicalData.Event__c = newEvent.id;
          testClinicalData.height_UOM__c = 'Inches';
          testClinicalData.Height__c = 69.0;
          testClinicalData.Weight__c = 135.0;
          insert testClinicalData;

          OS_Device__c testOSDevice = new OS_Device__c();
          testOSDevice.Style__c = 'TKA';
          testOSDevice.Size__c = 'Size 6';
          testOSDevice.Year_Assembled__c = '2014';
          testOSDevice.Source_Record_ID__c = '12351849';
          insert testOSDevice;

          /*OS_Device_Event__c osde = new OS_Device_Event__c();
          osde.OS_Device__c = testOSDevice.id;
          osde.Event__c = newEvent.id;
          insert osde;*/

          List<Sensor_Data__c> sdtoinsert = new List<Sensor_Data__c>();
          Sensor_Data__c sd1 = new Sensor_Data__c();
          sd1.event__c = newEvent.id;
          sd1.Type__c = 'Medial Load';
          sd1.Value__c = '35';
          sd1.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd1);

          Sensor_Data__c sd2 = new Sensor_Data__c();
          sd2.event__c = newEvent.id;
          sd2.Type__c = 'Lateral Load';
          sd2.Value__c = '35';
          sd2.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd2);

          Sensor_Data__c sd3 = new Sensor_Data__c();
          sd3.event__c = newEvent.id;
          sd3.Type__c = 'Rotation';
          sd3.Value__c = '35';
          sd3.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd3);

          Sensor_Data__c sd4 = new Sensor_Data__c();
          sd4.event__c = newEvent.id;
          sd4.Type__c = 'Actual Flex Ext';
          sd4.Value__c = '35';
          sd4.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd4);

          Sensor_Data__c sd5 = new Sensor_Data__c();
          sd5.event__c = newEvent.id;
          sd5.Type__c = 'AP';
          sd5.Value__c = '35';
          sd5.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd5);

          Sensor_Data__c sd6 = new Sensor_Data__c();
          sd6.event__c = newEvent.id;
          sd6.Type__c = 'HKA';
          sd6.Value__c = '35';
          sd6.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd6);

          Sensor_Data__c sd7 = new Sensor_Data__c();
          sd7.event__c = newEvent.id;
          sd7.Type__c = 'Tibia Rotation';
          sd7.Value__c = '35';
          sd7.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd7);

          Sensor_Data__c sd8 = new Sensor_Data__c();
          sd8.event__c = newEvent.id;
          sd8.Type__c = 'Tibia Tilt';
          sd8.Value__c = '35';
          sd8.ImageFileName__c = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          sdtoinsert.add(sd8);

          if(!sdtoinsert.isEmpty()) insert sdtoinsert;

          Blob b = Blob.valueOf('Test Data');

          Attachment att1 = new Attachment();
          att1.ParentId = newEvent.id;
          att1.Name = '2014-07-24_12-52-17_14055908_Stryker%20Size%203.png';
          att1.Body = b;
          insert(att1);


          Test.startTest();
          ProcedureSummaryController psController = new ProcedureSummaryController();
          psController.patient = testPatient; //[Select id from Patient__c WHERE Date_of_Birth__c < TODAY AND Date_of_Birth__c!=null Limit 1 ];
          psController.clinicalData = testClinicalData; //[Select id from Clinical_Data__c LIMIT 1];
          psController.ProcedureID = newEvent.id;
          psController.selectedHospital = testAccount;
          psController.imageString = 'abc';
          psController.imagefilename = 'abc';
          psController.flexion = 'abc';
          psController.type = 'abc';
          psController.increment = 'abc';
          psController.measurementDate = '11/28/14 06:00 AM';
          psController.medialload = 'abc';
          psController.lateralload = 'abc';
          psController.rotation = 'abc';
          psController.actualflex = 'abc';
          psController.cp_rotation = 'abc';
          psController.ap = 'abc';
          psController.hka = 'abc';
          psController.tibia_rotation = 'abc';
          psController.tibia_tilt = 'abc';
          psController.LogConstructor();
          psController.renderEditView();
          psController.save();
          psController.GoToProcedureImages();
          psController.GoToComponents();
          psController.GoToProcedureForms();
          psController.GoToProcedureDocuments();
          psController.newSurgeonFirstName = 'Test';
          psController.newSurgeonLastName = 'Testdoc';
          psController.newSurgeonNPI = '1234567890';
          psController.AddSurgeon();
          psController.AddImageAttachment();
          psController.DeleteAttachment();
          psController.DeleteNonImageAttachment();

          psController.deleteDeviceID = testOSDevice.id;
          psController.DeleteDevice();
          psController.ChangeHUOM();
          psController.ChangeWUOM();

          PageReference PgRef1 = psController.logPDFRead();
          PageReference PgRef2 = psController.logAttachmentRead();
          PageReference PgRef3 = psController.ParseSensorBoxBarcode();
		Test.stopTest();

          //ProcedureSummaryController.AddSensorData(newEvent.id, '1234');
          //ProcedureSummaryController.AddSensorDataNew(newEvent.id, '1234','Left');
          //ProcedureSummaryController.CheckSoftwareVersion(newEvent.id, '1234','Left');

          //String ImgList = ProcedureSummaryController.GetImageList(newEvent.id, testOSDevice.Id, '1234');
          //ProcedureSummaryController.MatchSensorDataAndAttachment(newEvent.id);
          //ProcedureSummaryController.SaveImageToRecord(newEvent.id, 'http://www.google.com/yellow.jpg');

          Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
          insert testCase;

          manufacturer__c manu1 = new manufacturer__c();
          manu1.Name = 'Stryker';
          manu1.Active__c = true;
          manu1.Short_Name__c = 'Stryker';
          insert manu1;

          manufacturer__c manu2 = new manufacturer__c();
          manu2.Name = 'Zimmer';
          manu2.Active__c = true;
          manu2.Short_Name__c = 'Zimmer';
          insert manu2;

          manufacturer_hibc_code__c mhc1 = new manufacturer_hibc_code__c();
          mhc1.HIBC_Code__c = 'H825';
          mhc1.Manufacturer__c = manu1.id;
          insert mhc1;

          manufacturer_hibc_code__c mhc2 = new manufacturer_hibc_code__c();
          mhc2.HIBC_Code__c = 'H132';
          mhc2.Manufacturer__c = manu2.id;
          insert mhc2;

          product_catalog__c pc1 = new product_catalog__c();
          pc1.Name = '5521B400';
          pc1.Manufacturer__c = manu1.id;
          pc1.Product_Name__c = 'Universal Baseplate';
          pc1.Active__c = true;
          insert pc1;

          product_catalog__c pc2 = new product_catalog__c();
          pc2.Name = '575001501';
          pc2.Manufacturer__c = manu2.id;
          pc2.Product_Name__c = 'CR-FLEX GSF PRECOAT FEMORAL, SIZE E-LT';
          pc2.active__c = true;
          insert pc2;

          //PageReference pageRef = Page.ImplantSummary;
          //Test.setCurrentPageReference(pageRef);
          //ApexPages.currentPage().getParameters().put('Procid',newEvent.Id);

          //ProcedureSummaryController procedureSummaryController = new ProcedureSummaryController();
          //procedureSummaryController.LogConstructor();
          //procedureSummaryController.ProcedureID = testprocedure.id;
          psController.switchBarcode();
          psController.FirstBarcode = '+H8255521B400GG';
          psController.SecondBarcode = '+$$3140202MHAHY4S5';
          psController.SaveAndAddNewRow();
          psController.FirstBarcode = '+H132005750015011/2305962319205B13J';
          psController.ValidateBarCode();
          psController.SecondBarcode = '+H132005750015011/2305962319205B13J';
          psController.ValidateBarCode();

          //psController.FirstBarcode = '+H8255521B400GG';
          //psController.SecondBarcode = '+$$523059MHAHY4S5';
          //psController.SaveAndAddNewRow();
          //procedureSummaryController.FirstBarcode = '+H8255521B400GG';
          //procedureSummaryController.SecondBarcode = '+$$2020216MHAHY4S5';
          //procedureSummaryController.ValidateBarCode();
          //procedureSummaryController.FirstBarcode = '+H8255521B400GG';
          //procedureSummaryController.SecondBarcode = '+$$0216MHAHY4S5';
          //psController.ValidateBarCode();

          list<implant_component__c> curImplants = psController.ImplantSummlist;
          string prodId = psController.ImplantList[0].Product_Catalog_Id__c;
          ApexPages.currentPage().getParameters().put('delId',psController.ImplantList[0].Id);
          psController.delImplant();
          //Test.stopTest();
     }

     static testmethod void MyUnitTest2()
     {
          //Event__c testprocedure = [Select id from Event__c where Case__r.Patient__c!=null AND Case__r.Patient__r.Date_Of_Birth__c!=null AND Case__r.Patient__r.Date_Of_Birth__c<TODAY LIMIT 1];
          //Account testAccount = [Select Id from Account WHERE IsPartner=TRUE Limit 1];
          Account testAccount = new Account();
          testAccount.Name = 'Test Account 1';
          //testAccount.IsPartner = true;
          Insert testAccount;

          Diagnosis__c diag = new Diagnosis__c();
          diag.Code__c='715.00';
          diag.Long_Name__c = 'Test Diag Code';
          insert diag;

          Patient__c testPatient = new Patient__c();
          testPatient.First_Name__c = 'Test';
          testPatient.Last_Name__c = 'Tester';
          testPatient.Date_Of_Birth__c = date.newInstance(1970, 4, 3);
          insert testPatient;

          Case__c newCase = new Case__c();
          newCase.Name__c = 'Test Case';
          newCase.Patient__c = testPatient.id;
          Insert newCase;

          Event_Type__c evtType = new Event_Type__c();
          evtType.Name = 'TestEventType';
          evtType.Primary__c = true;
          Insert evtType;

          Event__c newEvent = new Event__c();
          newEvent.Event_Name__c = 'Test Procedure';
          //newEvent.Event_Type__c = evtType.Id;
          newEvent.Case__c = newCase.Id;
          Insert newEvent;

          Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
          testClinicalData.Event__c = newEvent.id;
          testClinicalData.Height__c = 65;
          testClinicalData.Weight__c = 155;
          insert testClinicalData;

          Test.startTest();
          ProcedureSummaryController psController = new ProcedureSummaryController();
          psController.patient = testPatient; //[Select id from Patient__c WHERE Date_of_Birth__c < TODAY AND Date_of_Birth__c!=null Limit 1 ];
          psController.clinicalData = testClinicalData; //[Select id from Clinical_Data__c LIMIT 1];
          psController.ProcedureID = newEvent.id;
          psController.selectedHospital = testAccount;
          psController.imageString = 'abc';
          psController.imagefilename = 'abc';
          psController.flexion = 'abc';
          psController.type = 'abc';
          psController.increment = 'abc';
          psController.measurementDate = '11/28/14 06:00 AM';
          psController.medialload = 'abc';
          psController.lateralload = 'abc';
          psController.rotation = 'abc';
          psController.actualflex = 'abc';
          psController.LogConstructor();

          psController.showRefreshMsg();

          Event_Form__c testEventForm = (Event_Form__c)epTestUtils.NewObject(new Event_Form__c());
          testEventForm.Event__c = newEvent.id;
          insert testEventForm;

          Survey_Response__c testResponse = new Survey_Response__c();
          testResponse.Event_Form__c = testEventForm.id;
          testResponse.Survey_Question__c = '1';
          testResponse.Survey_Question_Text__c = 'Test Question';
          insert testResponse;

          psController.getEventForms();
          psController.getSurveyResponses();
          psController.refreshEventForms();

          OS_Device__c testOSDvc = new OS_Device__c();
          testOSDvc.Size__c = '20';
          testOSDvc.Style__c = 'New';
          testOSDvc.Year_Assembled__c = '2013';
          insert testOSDvc;

          OS_Device_Event__c osde = new OS_Device_Event__c();
          osde.OS_Device__c = testOSDvc.id;
          osde.Event__c = newEvent.id;
          insert osde;

          //psController.RenderOSDevice();
          psController.CalculateBMI();
          Test.StopTest();
     }

     static testmethod void MyUnitTest3()
     {
          Account testAccount = new Account();
          testAccount.Name = 'Test Account 1';
          //testAccount.IsPartner = true;
          Insert testAccount;

          Diagnosis__c diag = new Diagnosis__c();
          diag.Code__c='715.00';
          diag.Long_Name__c = 'Test Diag Code';
          insert diag;

          Patient__c testPatient = new Patient__c();
          testPatient.First_Name__c = 'Test';
          testPatient.Last_Name__c = 'Tester';
          testPatient.Date_Of_Birth__c = date.newInstance(1970, 4, 3);
          insert testPatient;

          Case__c newCase = new Case__c();
          newCase.Name__c = 'Test Case';
          newCase.Patient__c = testPatient.id;
          Insert newCase;

          Event_Type__c evtType = new Event_Type__c();
          evtType.Name = 'TestEventType';
          evtType.Primary__c = true;
          Insert evtType;

          Event__c newEvent = new Event__c();
          newEvent.Event_Name__c = 'Test Procedure';
          //newEvent.Event_Type__c = evtType.Id;
          newEvent.Case__c = newCase.Id;
          Insert newEvent;

          Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
          testClinicalData.Event__c = newEvent.id;
          insert testClinicalData;

          Test.startTest();
          ProcedureSummaryController psController = new ProcedureSummaryController();
          psController.patient = testPatient; //[Select id from Patient__c WHERE Date_of_Birth__c < TODAY AND Date_of_Birth__c!=null Limit 1 ];
          psController.clinicalData = testClinicalData; //[Select id from Clinical_Data__c LIMIT 1];
          psController.ProcedureID = newEvent.id;
          psController.selectedHospital = testAccount;
          psController.LogConstructor();

          manufacturer__c manu1 = new manufacturer__c();
          manu1.Name = 'Stryker';
          manu1.Active__c = true;
          manu1.Short_Name__c = 'Stryker';
          insert manu1;

          manufacturer_hibc_code__c mhc1 = new manufacturer_hibc_code__c();
          mhc1.HIBC_Code__c = 'H825';
          mhc1.Manufacturer__c = manu1.id;
          insert mhc1;

          product_catalog__c pc1 = new product_catalog__c();
          pc1.Name = '5521B400';
          pc1.Manufacturer__c = manu1.id;
          pc1.Product_Name__c = 'Universal Baseplate';
          pc1.Active__c = true;
          insert pc1;

          psController.switchKeyboard();
          psController.ClearNewRow();
          psController.editImplant.Product_Catalog_Id__c = pc1.id;
          psController.curProdId = pc1.id;
          psController.getProductFromId();

          psController.showCreatePDFForms();
          psController.GenerateForms();

          psController.ValidateBarCode();
          psController.editImplant.Lot_Number__c = '1239874';
          psController.editImplant.Expiration_Date__c = Date.valueOf('2018-09-01');
          psController.editImplant.Status__c = 'Used';
          psController.ValidateBarCode();
          psController.SaveExpiry();
          //procedureSummaryController.VerifyandAccept();
          Test.stopTest();

          psController.CancelExpiry();
          psController.SetEditImplantComponents();
          psController.AcceptImplantComponents();
          psController.SaveImplantUpdates();
          psController.verifyandaccept();
          
          Set<String> stringset = new Set<String>();
          stringset.add('teststring');
          psController.getProductFromBiometBarCode(stringset);
          psController.getLotFromBarCode('+$$3140202MHAHY4S5');
          psController.getExpiryFromBarCode('+$$3140202MHAHY4S5');
          psController.SaveAndAddNewRow();
          psController.EventViewAccept();
     }
     
     static testmethod void MyUnitTest4(){
     	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('MeasurementsMock');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		
		Test.setMock(HttpCalloutMock.class, mock);
		
		          Account testAccount = new Account();
          testAccount.Name = 'Test Account 1';
          //testAccount.IsPartner = true;
          Insert testAccount;

          Diagnosis__c diag = new Diagnosis__c();
          diag.Code__c='715.00';
          diag.Long_Name__c = 'Test Diag Code';
          insert diag;

          Patient__c testPatient = new Patient__c();
          testPatient.First_Name__c = 'Test';
          testPatient.Last_Name__c = 'Tester';
          testPatient.Date_Of_Birth__c = date.newInstance(1970, 4, 3);
          insert testPatient;

          Case__c newCase = new Case__c();
          newCase.Name__c = 'Test Case';
          newCase.Patient__c = testPatient.id;
          Insert newCase;

          Event_Type__c evtType = new Event_Type__c();
          evtType.Name = 'TestEventType';
          evtType.Primary__c = true;
          Insert evtType;

          Event__c newEvent = new Event__c();
          newEvent.Event_Name__c = 'Test Procedure';
          //newEvent.Event_Type__c = evtType.Id;
          newEvent.Location__c = testAccount.id;
          newEvent.Case__c = newCase.Id;
          Insert newEvent;
          
		Test.StartTest();
     	ProcedureSummaryController.AddSensorData(newEvent.id, '1234');
          //ProcedureSummaryController.AddSensorDataNew(newEvent.id, '1234','Left');
          Test.StopTest();
     }
     
      static testmethod void MyUnitTest5(){
     	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('MeasurementsMock');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		
		Test.setMock(HttpCalloutMock.class, mock);
		
		          Account testAccount = new Account();
          testAccount.Name = 'Test Account 1';
          //testAccount.IsPartner = true;
          Insert testAccount;

          Diagnosis__c diag = new Diagnosis__c();
          diag.Code__c='715.00';
          diag.Long_Name__c = 'Test Diag Code';
          insert diag;

          Patient__c testPatient = new Patient__c();
          testPatient.First_Name__c = 'Test';
          testPatient.Last_Name__c = 'Tester';
          testPatient.Date_Of_Birth__c = date.newInstance(1970, 4, 3);
          insert testPatient;

          Case__c newCase = new Case__c();
          newCase.Name__c = 'Test Case';
          newCase.Patient__c = testPatient.id;
          Insert newCase;

          Event_Type__c evtType = new Event_Type__c();
          evtType.Name = 'TestEventType';
          evtType.Primary__c = true;
          Insert evtType;

          Event__c newEvent = new Event__c();
          newEvent.Event_Name__c = 'Test Procedure';
          //newEvent.Event_Type__c = evtType.Id;
          newEvent.Location__c = testAccount.id;
          newEvent.Case__c = newCase.Id;
          Insert newEvent;
          
		Test.StartTest();
     	//ProcedureSummaryController.AddSensorData(newEvent.id, '1234');
          ProcedureSummaryController.AddSensorDataNew(newEvent.id, '1234','Left');
          Test.StopTest();
     }
}