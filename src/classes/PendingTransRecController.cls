global with sharing class PendingTransRecController {
    
    public Id selectedShipment { get;set; }
    public Id selectedWarehouse { get;set; }
    public Integer serialNumSize { get;set; }
    public Boolean showShips { get;set; }
    public Boolean showShip { get;set; }
    public Boolean showCase { get;set; }
    public Boolean reRender { get;set; }
    //public String test { get;set; }
    
    public User user { get;set; }
    public Case kase { get;set; }
    public SCMC__Warehouse__c warehouse { get;set; }
    public SCMC__Warehouse__c mainWarehouse { get;set; }
    public SCMC__Transfer_Request__c transRequest { get;set; }
    
    public SCMC__Shipping__c shipment { get;set; }
    public List<SCMC__Shipping__c> shipments { get;set; }
    
    public list<SCMC__Serial_Number__c> serialNum { get;set; }
    
    public SCMC__Ownership_Code__c ownCode;
    public SCMC__Currency_Master__c usdCode;
    
    public SCMC__Inventory_Position__c pos;
    public List<SCMC__Inventory_Position__c> positions;
    public List<SCMC__Inventory_Position__c> posToUpdate;
    
    public Group queue;
    
    public PendingTransRecController(){
        
        user = [select id, name from user where id = :UserInfo.getUserId() limit 1];
        warehouse = getWarehouse();
        shipments = getShipments();
        ownCode = getOwnCode();
        system.debug('ownCode ' + ownCode);
        usdCode = [select id from SCMC__Currency_Master__c where Name = 'USD' limit 1];
        mainWarehouse = [select id, name from SCMC__Warehouse__c where name like 'DCOTA%' limit 1];
        
        showShip = false;
        showShips = true;
        reRender = true;
        
    }
    
    public void selectedShipment(){
        
        shipment = getShipment();
        serialNum = getSerialNum();
        serialNumSize = serialNum.size();
        transRequest = getTransRequest();
        
        String ownerString = 'Rep - ' + user.name + ' in Main ICP';
        queue = [ select Id from Group where Name = :ownerString and Type = 'Queue'];
        
        showShips = false;
        showShip = true;
        reRender = true;
        
    }
    
    public void createCase(){
        
        Case caseToInsert = new Case();  
        
        RecordType caseRecType = [select id from RecordType where name = 'Shipping Issue' limit 1];
        
        //try {
            
            caseToInsert.Origin = 'Pending Transfer App by ' + user.name;
            caseToInsert.recordType = caseRecType;
            caseToInsert.Reason = 'Shipping Issue';
            caseToInsert.Transfer_Request__c = shipment.SCMC__Transfer_Request__c;
            
            insert caseToInsert;
            
            //Reset UI
            kase = [
                select id, Issue__c, Reason, Subject, Status, Priority, Description, RecordTypeId, Transfer_Request__c,
                    Transfer_Request__r.id
                from case 
                where id = :caseToInsert.id 
                limit 1
            ];
            
            //String URL = '/' + caseToInsert.Id;
            showShip = false;
            showShips = false;
            showCase = true;
            reRender = true;
            
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your case was submitted successfully...'));
            
            //return URL;
            
        /*} catch(exception e) {
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: ' + e));
        }*/
    }
    
    public void updateCase(){
    
        try {
            
            update kase;
            
            kase = new Case();
            
            shipments = getShipments();
            
            showShips = true;
            showCase = false;
            showship = false;
            reRender = true;
                
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your case was submitted successfully...'));
            
        } catch(exception e) {
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating case... ' + e));
        }
        
    }
    
    public void markComplete(){
        system.debug(selectedShipment+' hello');
        
        set<SCMC__Serial_Number__c> snUpdateList = new set<SCMC__Serial_Number__c>();
        List<SCMC__Inventory_Action_Queue__c> iacUpdateList = new List<SCMC__Inventory_Action_Queue__c>();
        List<SCMC__Serial_Number_Related_Record__c> snrUpdateList = new List<SCMC__Serial_Number_Related_Record__c>();
        List<SCMC__Inventory_Transaction_Perpetual_Record__c> itpUpdateList = new List<SCMC__Inventory_Transaction_Perpetual_Record__c>();
        List<SCMC__Inventory_Transaction_Financial_Records__c> itfUpdateList = new List<SCMC__Inventory_Transaction_Financial_Records__c>();
        posToUpdate = new List<SCMC__Inventory_Position__c>();
        /*try{*/
            
            shipment = getShipment();
            shipment.SCMC__Status__c = 'Shipment complete';
            
            SCMC__Transfer_Request__c tr = getTransRequest();
            
            system.debug(' test <|v|> ' + transRequest.SCMC__Source_Warehouse__c);
            
            //positions = getPos();
            //system.debug(' pos size <|v|> ' + positions.size());

            tr.SCMC__Status__c = 'Complete';
            tr.SCMC__Shipment_Status__c = 'Complete';
            
            list<Id> posIds = new list<Id>();
            List<SCMC__Transfer_Request_Line__c> trl = new List<SCMC__Transfer_Request_Line__c>();
            
            trl = [
                select id, SCMC__Quantity__c, SCMC__Quantity_Transferred__c, SCMC__Inventory_Position__c,
                    SCMC__Inventory_Position__r.SCMC__Item_Master__c, SCMC__Inventory_Position__r.SCMC__Receipt_Line__c, 
                    SCMC__Inventory_Position__r.SCMC__Condition_Code__c, SCMC__Inventory_Position__r.SCMC__Receiving_Inspection__c, 
                    SCMC__Inventory_Position__r.SCMC__Lot_Number__c, SCMC__Inventory_Position__r.SCMC__Shelf_Life_Expiration__c,
                    SCMC__Inventory_Position__r.SCMC__Current_Value__c, SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c
                from SCMC__Transfer_Request_Line__c 
                where SCMC__Transfer_Request__c = :tr.id
            ];
            
            SCMC__Inventory_Location__c il = [
                Select id 
                from SCMC__Inventory_Location__c 
                where Name = 'Receiving' 
                and SCMC__Warehouse__c = :shipment.SCMC__Receiving_Warehouse__c 
                limit 1
            ];
            
            //Map<ID, String> snMap = new Map<ID, String>(); 
            integer test = 0;
            for(SCMC__Transfer_Request_Line__c tmp : trl){
            
                posIds.add(tmp.SCMC__Inventory_Position__c);
                
                test++;
                system.debug(tmp.SCMC__Inventory_Position__c +' test'+test);
                tmp.SCMC__Quantity_Transferred__c = tmp.SCMC__Quantity__c;
                
                SCMC__Inventory_Action_Queue__c iac = new SCMC__Inventory_Action_Queue__c();
                iac.SCMC__Inventory_Position__c = tmp.SCMC__Inventory_Position__c;
                iac.SCMC__Action__c = 'Put Away';
                iac.SCMC__Status__c = 'Complete';
                iacUpdateList.add(iac);
                
                SCMC__Inventory_Transaction_Perpetual_Record__c itp = new SCMC__Inventory_Transaction_Perpetual_Record__c();
                //Example record: https://orthosensor--sandbox.cs44.my.salesforce.com/a0w7A000000oPk1
                itp.SCMC__Inventory_Transactions__c = 'Item Transfer Inbound';
                itp.SCMC__Quantity__c = tmp.SCMC__Quantity__c;
                itp.SCMC__Inventory_Location__c = il.id;
                itp.SCMC__New_Location_Balance__c = tmp.SCMC__Quantity__c;
                itp.SCMC__Inventory_Position__c = tmp.SCMC__Inventory_Position__c;
                itp.SCMC__Item_Master__c = tmp.SCMC__Inventory_Position__r.SCMC__Item_Master__c;
                itp.SCMC__Receipt_Line__c = tmp.SCMC__Inventory_Position__r.SCMC__Receipt_Line__c;
                itp.SCMC__Condition_Code__c = tmp.SCMC__Inventory_Position__r.SCMC__Condition_Code__c;
                itp.SCMC__Receiving_Inspection__c = tmp.SCMC__Inventory_Position__r.SCMC__Receiving_Inspection__c;
                itp.SCMC__Lot_Number__c = tmp.SCMC__Inventory_Position__r.SCMC__Lot_Number__c;
                itp.SCMC__Expiry_Date__c = tmp.SCMC__Inventory_Position__r.SCMC__Shelf_Life_Expiration__c;
                itpUpdateList.add(itp);
                
                id snBin;
                id snrBin;
                for(SCMC__Serial_Number__c snTmp : serialNum){
                    system.debug('snTmp ' + snTmp.id );
                    SCMC__Serial_Number_Related_Record__c snr = new SCMC__Serial_Number_Related_Record__c();
                    
                    if(snBin != snTmp.id){
                        snBin = snTmp.id;
                        snTmp.SCMC__On_Hold__c = false;
                        snTmp.SCMC__Shipping__c = null;
                        snTmp.SCMC__Ownership_Code__c = ownCode.Id;
                        snTmp.SCMC__Transfer_Request_Line__c = null;
                        snTmp.SCMC__Last_Transfer_Request_Line__c = tmp.id;
                        snTmp.SCMC__Warehouse__c = tr.SCMC__Destination_Warehouse__c;
                        snUpdateList.add(snTmp);
                        
                    }
                }
            }
            
            //system.debug(posIds);
            

            system.debug('itfUpdateList' + itfUpdateList);
            
            List<SCMC__Serial_Number__c> snList = new List<SCMC__Serial_Number__c>();
            set<SCMC__Serial_Number__c> snSet = new set<SCMC__Serial_Number__c>(); 
            SCMC__Serial_Number_Related_Record__c snr = new SCMC__Serial_Number_Related_Record__c();
            List<SCMC__Serial_Number_Related_Record__c> snrList = new List<SCMC__Serial_Number_Related_Record__c>();
            //map<SCMC__Serial_Number_Related_Record__c> snrMap = new map<SCMC__Serial_Number_Related_Record__c>(); 
            
            integer i = 0;
            for(SCMC__Serial_Number__c sn : snUpdateList){ 
                snSet.add(sn);
            }
			snList.addAll(snSet);
		    for(SCMC__Serial_Number__c sn : snList){
                snr = new SCMC__Serial_Number_Related_Record__c();
                snr.Shipping__c = shipment.Id;
                snr.SCMC__Shipping__c = shipment.Id;
                snr.SCMC__Serial_Number__c = sn.Id;
                snr.SCMC__Warehouse__c = mainWarehouse.Id;
                system.debug(sn.SCMC__Last_Transfer_Request_Line__r.Id+' vs '+sn.SCMC__Last_Transfer_Request_Line__c);
                snr.SCMC__Transfer_Request_Line__c = sn.SCMC__Last_Transfer_Request_Line__c;
                snrUpdateList.add(snr);
            }
            //SCMC__Inventory_Position__c pos = tmp.SCMC__Inventory_Position__c;
            for(SCMC__Inventory_Position__c p : [select SCMC__Bin__c,OwnerId,SCMC__Availability__c,SCMC__Quantity_in_Transit__c,SCMC__Ownership_Code__c,SCMC__Availability_Code__c from SCMC__Inventory_Position__c where id in :posIds]){
                
                system.debug('found test 1');
                p.SCMC__Bin__c = tr.SCMC__Destination_Location__c;
                p.OwnerId = queue.id;
                p.SCMC__Availability__c = true;
                p.SCMC__Quantity_in_Transit__c = 0;
                p.SCMC__Ownership_Code__c  = ownCode.id;
                p.SCMC__Availability_Code__c  = 'In Stock';
                posToUpdate.add(p);
                
            }
            
            system.debug('posUp size' + posToUpdate.size());
            system.debug('snList ' + snList.size() + snList);
            system.debug('snrUpdateList ' + snrUpdateList.size() + snrUpdateList);
            system.debug('iacUpdateList ' + iacUpdateList.size() + iacUpdateList);
            system.debug('itpUpdateList ' + itpUpdateList.size() + itpUpdateList);
            
            //Start updates
            update shipment;
            update tr;
            update trl;
            update posToUpdate;
            update snList;
            
            //Start inserts
            Database.insert(snrUpdateList, true);
            Database.insert(iacUpdateList, true);
            Database.insert(itpUpdateList, true);
            
            list<id> perpIds = new list<id>();
            
            for(SCMC__Inventory_Transaction_Perpetual_Record__c itp : itpUpdateList){
                perpIds.add(itp.id);
            }
            
            for(SCMC__Inventory_Transaction_Perpetual_Record__c itp : [select id,SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c,SCMC__Inventory_Position__r.SCMC__Current_Value__c, SCMC__New_Location_Balance__c from SCMC__Inventory_Transaction_Perpetual_Record__c where id in :perpIds]){
            
                system.debug(itp.SCMC__Inventory_Position__r.SCMC__Current_Value__c +' j '+
                    itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c+' j '+
                    itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c
                );
                
                SCMC__Inventory_Transaction_Financial_Records__c itf = new SCMC__Inventory_Transaction_Financial_Records__c();
                //Example record: https://orthosensor--sandbox.cs44.my.salesforce.com/a0v7A000000epoD
                itf.SCMC__Inventory_Transaction__c = itp.Id;
                itf.SCMC__GL_Account__c = 'Inventory';
                itf.SCMC__ICP_Currency__c = usdCode.Id;
                itf.SCMC__Trade_Currency__c = usdCode.Id;
                itf.SCMC__ICP_Unit_Cost__c = itp.SCMC__Inventory_Position__r.SCMC__Current_Value__c;
                itf.SCMC__Trade_Unit_Cost__c =  itp.SCMC__Inventory_Position__r.SCMC__Current_Value__c;
                itf.SCMC__Quantity__c = itp.SCMC__New_Location_Balance__c;
                itf.SCMC__ICP_Value_Amount__c = itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c;
                itf.SCMC__Trade_Value_Amount__c = itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c;
                itfUpdateList.add(itf);
                
                //*This is a 2nd record with slightly different field values
                SCMC__Inventory_Transaction_Financial_Records__c itf2 = new SCMC__Inventory_Transaction_Financial_Records__c();
                //Example record: https://orthosensor--sandbox.cs44.my.salesforce.com/a0v7A000000epoE
                itf2.SCMC__Trade_Unit_Cost__c = 0;
                itf2.SCMC__GL_Account__c = 'Transfer';
                itf2.SCMC__ICP_Currency__c = usdCode.Id;
                itf2.SCMC__Trade_Currency__c = usdCode.Id;
                itf2.SCMC__Inventory_Transaction__c = itp.Id;
                itf2.SCMC__Quantity__c = itp.SCMC__New_Location_Balance__c * -1;
                itf2.SCMC__ICP_Unit_Cost__c = itp.SCMC__Inventory_Position__r.SCMC__Current_Value__c;
                itf2.SCMC__ICP_Value_Amount__c = itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c * -1;
                itf2.SCMC__Trade_Value_Amount__c = itp.SCMC__Inventory_Position__r.SCMC__Total_Current_Value__c * -1;
                itfUpdateList.add(itf2);
            }
            
            system.debug('itfUpdateList ' + itfUpdateList.size() + itfUpdateList);
            
            Database.SaveResult[] srList = Database.insert(itfUpdateList, true);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted record. ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            //Reset UI
            shipments = getShipments();
            
            showShip = false;
            showShips = true;
            reRender = true;
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Updated successfully'));
            
       /* }catch(exception e){
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error updating... ' + e));
        }*/
    }
    
    public void cancel(){
        
        shipments = getShipments();
        
        showShips = true;
        showShip = false;
        showCase = false;
        reRender = true;
        
    }
    
    @RemoteAction
    global static String mkGoToCase(){
        Case caseToInsert = new Case();  
        
        caseToInsert.Origin = 'Custom Shipping App';
        insert caseToInsert;
        
        String URL = '/' + caseToInsert.Id;
        
        return URL;
    }
    
    
    
    //Queries
    public SCMC__Warehouse__c getWarehouse(){
        warehouse = [
            SELECT Id, Name, Receiving_User__r.Id, SCMC__Active__c, SCMC__Address__c, SCMC__Create_New__c, SCMC__ICP__c, SCMC__ILSMart_Listing__c, 
                    SCMC__RecordType__c, Ownership_Code__c, SCMC__ILS_List_Type__c, Receiving_User__c
            FROM SCMC__Warehouse__c 
            where Receiving_User__c = :user.Id
            AND SCMC__Active__c = true
            LIMIT 1
        ];
        
        return warehouse;
    }
    
    public SCMC__Ownership_Code__c getOwnCode(){
        ownCode = [
            SELECT Id, Name
            FROM SCMC__Ownership_Code__c 
            where Name = :user.Name
            LIMIT 1
        ];
        
        return ownCode;
    } //
    
    public List<SCMC__Shipping__c> getShipments(){
        
        shipments = [
            SELECT Id, Name, SCMC__Receiving_Warehouse__c, SCMC__Status__c, SCMC__Transfer_Request__c, SCMC__Transfer_Request__r.name, 
                SCMC__Transfer_Request__r.id 
            FROM SCMC__Shipping__c
            WHERE SCMC__Receiving_Warehouse__c = :warehouse.Id 
            AND SCMC__Status__c != 'Shipment complete'
        ];
        
        return shipments;
    }
    
    public SCMC__Shipping__c getShipment(){
        
        shipment = [
            SELECT Id, Name, SCMC__Receiving_Warehouse__c, SCMC__Status__c, SCMC__Transfer_Request__c, SCMC__Transfer_Request__r.name,
                SCMC__Transfer_Request__r.id 
            FROM SCMC__Shipping__c
            WHERE id = :selectedShipment
            LIMIT 1
        ];
        
        return shipment;
    }
    
    public SCMC__Transfer_Request__c getTransRequest(){
        
        transRequest = [
            SELECT Id, SCMC__Shipment_Date__c, SCMC__Tracking_Number__c, SCMC__Status__c, SCMC__Source_Warehouse__c, SCMC__Source_Warehouse__r.name, 
            SCMC__Source_Ownership__c, SCMC__Shipment_Status__c, SCMC__Destination_Warehouse__c, SCMC__Destination_Ownership__c, SCMC__Carrier__c, 
                SCMC__Carrier_Service__c , SCMC__Destination_Location__c
            FROM SCMC__Transfer_Request__c
            WHERE id = :shipment.SCMC__Transfer_Request__c
            LIMIT 1
        ];
        
        return transRequest;
    }
    
    public list<SCMC__Serial_Number__c> getSerialNum(){
        
        serialNum = [
        
            SELECT Id, Name, SCMC__Item__c , SCMC__Item__r.name , SCMC__Serial_Number__c, SCMC__Ownership_Code__c, SCMC__Warehouse__c, SCMC__On_Hold__c, 
                SCMC__Transfer_Request_Line__c, SCMC__Last_Transfer_Request_Line__c, SCMC__Shipping__c
            FROM SCMC__Serial_Number__c
            WHERE SCMC__Shipping__c = :selectedShipment
            order by id
        ];
        
        return serialNum;
    }
    
    public List<SCMC__Inventory_Position__c> getPos(){
        
        positions = [
            SELECT Id, Name, SCMC__Bin__c, SCMC__Quantity_in_Transit__c, OwnerId, SCMC__Availability__c, SCMC__Availability_Code__c, SCMC__Ownership_Code__c
            FROM SCMC__Inventory_Position__c
            WHERE SCMC__Warehouse__c = :transRequest.SCMC__Source_Warehouse__r.name
        ];
        
        return positions;
    }
    
    public boolean getPageHasErrors() {
        return ApexPages.hasMessages();
    }
    
}