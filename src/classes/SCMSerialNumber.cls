/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
public class SCMSerialNumber {

    public List<SCMC__Serial_Number__c> Records = new List<SCMC__Serial_Number__c>();
    public Map<Id, SCMC__Serial_Number__c> RecordMap = new Map<Id, SCMC__Serial_Number__c>();

    // =========== Instance Constructors =============
    private SCMSerialNumber() {}

    private SCMSerialNumber(List<SCMC__Serial_Number__c> records)
    {
        this.Records = records;
        for (SCMC__Serial_Number__c item : records) {
            if (item.Id != null)
                this.RecordMap.put(item.Id, item);
        }
    }

    // =========== Instance Constructors =============
    public SCMSerialNumber addAll(SCMSerialNumber addThisClass)
    {
        for (SCMC__Serial_Number__c item : addThisClass.Records) {
            if (item.Id == null) {
                this.Records.add(item);
            } else if (!this.RecordMap.keySet().contains(item.Id)) {
                this.Records.add(item);
                this.RecordMap.put(item.Id, item);
            }
        }
        return this;
    }

    // =========== Field List =============
    private static List<String> m_FieldList = new List<String>();
    public static List<String> getFieldList()
    {
        if (m_FieldList.isEmpty())
            m_FieldList = new List<String>(SCMC__Serial_Number__c.sObjectType.getDescribe().fields.getMap().keySet());
        return m_FieldList.clone();
    }

    public static List<String> getCombinedFieldList()
    {
        List<String> result = getFieldList();
        result.addAll(m_ConsumerFieldList);
        return result;
    }

    public static String getCombinedFieldList_AsSoqlString()
    {
        String result = String.join(getCombinedFieldList(), ',');
        return result;
    }

    // =========== Consumer Field List =============
    private static List<String> m_ConsumerFieldList = new List<String>();
    public static List<String> getConsumerFieldList()
    {
        //
        return m_ConsumerFieldList.clone();
    }

    public static void addConsumerField(String value)
    {
        addConsumerField(value, false);
    }
    
    public static void addConsumerField(String value, Boolean clearFirst)
    {
        if (clearFirst) m_ConsumerFieldList.clear();
        if (!String.isBlank(value))
            m_ConsumerFieldList.add(value);
    }

    // =========== Create Methodology =============
    public static SCMSerialNumber Create_ForIds(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where Id in :idSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }

    public static SCMSerialNumber Create_ForSalesOrderLineIds(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__Sales_Order_Line_Item__c in :idSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }
    
    public static SCMSerialNumber Create_ForSalesOrderLineIds(Set<Id> idSet, List<SCMC__Pick_list_Detail__c>picklistDetailList)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        Set<Id> picklistIdSet = new Set<Id>();
        for (SCMC__Pick_list_Detail__c picklistDetail: picklistDetailList)
            picklistIdSet.add(picklistDetail.SCMC__Picklist__c);

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__Sales_Order_Line_Item__c in :idSet and SCMC__Shipping__r.SCMC__Picklist__c in :picklistIdSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }

    public static SCMSerialNumber Create_ForProductionOrderLineIds(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__InInventory__c = true and SCMC__Production_Order_Line__c in :idSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }

    public static SCMSerialNumber Create_ForReceiptLineIds(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__Receipt_Line__c in :idSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }

    public static SCMSerialNumber Create_ForReceivingInspectionIds(Set<Id> idSet)
    {
        if (idSet == null || idSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__Receiving_Inspection__c in :idSet';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        return new SCMSerialNumber((List<SCMC__Serial_Number__c>)Database.query(soql));
    }

    public static SCMSerialNumber Create_ForSerialNumberValues(Set<String> numberSet)
    {
        // An overload.
        return Create_ForSerialNumberValues(numberSet, false);
    }

    public static SCMSerialNumber Create_ForSerialNumberValues(Set<String> numberSet, Boolean currentOnly)
    {
        if (numberSet == null || numberSet.isEmpty())
            return new SCMSerialNumber();

        String soql = 'select {0} from SCMC__Serial_Number__c where SCMC__Serial_Number__c in :numberSet order by SCMC__Serial_Number__c, Name';
        soql = String.format(soql, new List<String> {getCombinedFieldList_AsSoqlString()});
        List<SCMC__Serial_Number__c> resultingList = (List<SCMC__Serial_Number__c>)Database.query(soql);

        if (currentOnly) {
            // Given the ordered nature of the preceding query, newer records will overwrite the older ones in the map.
            Map<String, SCMC__Serial_Number__c> tempMap = new Map<String, SCMC__Serial_Number__c>();
            for (SCMC__Serial_Number__c item : resultingList) {
                tempMap.put(item.SCMC__Serial_Number__c, item);
            }
            resultingList = tempMap.values();
        }

        return new SCMSerialNumber(resultingList);
    }

    public static SCMSerialNumber Create_WithRecords(List<SCMC__Serial_Number__c> records)
    {
        // Pass-along the records to the constructor.
        return new SCMSerialNumber(records);
    }
}