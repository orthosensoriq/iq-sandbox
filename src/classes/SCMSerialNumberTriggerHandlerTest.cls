@isTest(SeeAllData=true)
private class SCMSerialNumberTriggerHandlerTest {
   static testMethod void transferSNAndChangeOwnership() {

    	SetUpTestForSCM scmTestClass = new SetUpTestForSCM();
    	SCMC__Item__c item = scmTestClass.createTestItem(false);
    	item.SCMC__Inbound_Outbound_Serial_Number__c = true;
    	item.SCMC__Lot_Number_Control__c = true;
    	upsert item;
   		
   		list<SCMC__Ownership_Code__c> ownercodes = [select Id from SCMC__Ownership_Code__c limit 2];	
    	SCMC__Reason_Code__c reason = [select ID from SCMC__Reason_Code__c where SCMC__Inactive__c = false limit 1];
    	list<SCMC__Warehouse__c> warehouse = [select ID, Ownership_Code__c from SCMC__Warehouse__c where SCMC__Active__c = true limit 5];
    	system.assertNotEquals(0, warehouse.size(), 'No Warehouses');
    	
        SCMC__Ownership_Code__c ownercodeA = ownercodes[0];
        SCMC__Ownership_Code__c ownercodeB = ownercodes[1];

    	SCMC__Transfer_Request__c transfer = new SCMC__Transfer_Request__c();
    	transfer.SCMC__Reason_Code__c = reason.Id;
    	transfer.SCMC__Source_Warehouse__c = warehouse[0].Id;
        transfer.SCMC__Source_Ownership__c =ownercodeA.Id;
    	transfer.SCMC__Destination_Warehouse__c = warehouse[1].Id;
    	transfer.SCMC__Destination_Ownership__c = ownercodeB.Id;
		insert transfer;
		
		SCMC__Transfer_Request_Line__c trl = new SCMC__Transfer_Request_Line__c();
		trl.SCMC__Transfer_Request__c = transfer.ID;
		trl.SCMC__Item_Master__c = item.ID;
		trl.SCMC__Quantity__c = 1;
		trl.SCMC__Status__c = 'Open';
        trl.SCMC__Ownership_Code__c = ownercodeA.Id;
		insert trl;
		
    	SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
    	sn.SCMC__Item__c = item.Id;
    	sn.SCMC__Serial_Number__c = '123456';
    	sn.SCMC__InInventory__c = true;
    	sn.SCMC__Warehouse__c = warehouse[0].Id;
        sn.SCMC__Ownership_Code__c = ownercodeA.Id;
    	upsert sn;
		
		sn.SCMC__Last_Transfer_Request_Line__c = trl.Id;
		sn.SCMC__Warehouse__c = warehouse[1].Id;
		update sn;
		
		SCMC__Serial_Number__c updateSN = [select Id, Lot_Number__c, SCMC__Ownership_Code__c from SCMC__Serial_Number__c where Id = :sn.Id];		
		system.assertEquals(warehouse[1].Ownership_Code__c, updateSN.SCMC__Ownership_Code__c, 'Ownership Code Does Not Match The Transfer Request.');     	

    }

    static testMethod void recSerialNumberWithLotNumber() {

    	SetUpTestForSCM scmTestClass = new SetUpTestForSCM();
    	SCMC__Item__c item = scmTestClass.createTestItem(false);
    	item.SCMC__Inbound_Outbound_Serial_Number__c = true;
    	item.SCMC__Lot_Number_Control__c = true;
    	upsert item;
    	
    	SCMC__Supplier_Site__c supplier = [select ID from SCMC__Supplier_Site__c where SCMC__Active__c = true and SCMC__Account__c != null limit 1];
    	SCMC__Purchase_Order__c po = scmTestClass.createPurchaseOrder(supplier, true);
    	SCMC__Purchase_Order_Line_Item__c pol = scmTestClass.createPurchaseOrderLine(po, 'Item', false);
    	
    	po.SCMC__Status__c = 'Approved';
    	update po;
    	
    	SCMC__Receipt__c recHeader = new SCMC__Receipt__c();
    	recHeader.SCMC__Receipt_Type__c = 'SCMC__Receipt_Type__c';
    	recHeader.SCMC__Purchase_Order__c = po.Id;
    	upsert recHeader;
    	
    	SCMC__Receipt_Line__c recLine = new SCMC__Receipt_Line__c();
    	recLine.SCMC__Receiver__c = recHeader.Id;
    	recLine.SCMC__Quantity__c = 1;
    	recLine.SCMC__Lot_Number__c = 'Test Lot Number';
    	recLine.SCMC__Item_Number__c = item.Id;
    	recLine.SCMC__Status__c = 'Received';
    	upsert recLine;
    	
    	SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
    	sn.SCMC__Item__c = item.Id;
    	sn.SCMC__Receipt_Line__c = recLine.Id;
    	sn.SCMC__Serial_Number__c = '123456';
    	sn.SCMC__InInventory__c = true;
    	
    	upsert sn;
    	
    	SCMC__Serial_Number__c updateSN = [select Id, Lot_Number__c from SCMC__Serial_Number__c where Id = :sn.Id];
    	system.assertEquals(recLine.SCMC__Lot_Number__c, updateSN.Lot_Number__c, 'Lot number is not correct');
    	
    }

    static testMethod void riSerialNumberWithLotNumber() {

    	SetUpTestForSCM scmTestClass = new SetUpTestForSCM();
    	SCMC__Item__c item = scmTestClass.createTestItem(false);
    	item.SCMC__Inbound_Outbound_Serial_Number__c = true;
    	item.SCMC__Lot_Number_Control__c = true;
    	upsert item;
    	
    	SCMC__Supplier_Site__c supplier = [select ID from SCMC__Supplier_Site__c where SCMC__Active__c = true and SCMC__Account__c != null limit 1];
    	SCMC__Purchase_Order__c po = scmTestClass.createPurchaseOrder(supplier, true);
    	SCMC__Purchase_Order_Line_Item__c pol = scmTestClass.createPurchaseOrderLine(po, 'Item', false);
    	
    	po.SCMC__Status__c = 'Approved';
    	update po;
    	
    	SCMC__Receipt__c recHeader = new SCMC__Receipt__c();
    	recHeader.SCMC__Receipt_Type__c = 'SCMC__Receipt_Type__c';
    	recHeader.SCMC__Purchase_Order__c = po.Id;
    	upsert recHeader;
    	
    	SCMC__Receipt_Line__c recLine = new SCMC__Receipt_Line__c();
    	recLine.SCMC__Receiver__c = recHeader.Id;
    	recLine.SCMC__Quantity__c = 1;
    	recLine.SCMC__Lot_Number__c = 'Test Lot Number';
    	recLine.SCMC__Item_Number__c = item.Id;
    	recLine.SCMC__Status__c = 'Received';
    	upsert recLine;
    	    	
    	SCMC__Receiving_Inspection__c ri = new SCMC__Receiving_Inspection__c();
    	ri.SCMC__Item_Number__c = item.Id;
    	ri.SCMC__Quantity_to_Accept__c = 1;
    	ri.SCMC__Receipt_Line__c = recLine.Id;
		upsert ri;

    	SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
    	sn.SCMC__Item__c = item.Id;
    	sn.SCMC__Receipt_Line__c = recLine.Id;
    	sn.SCMC__InInventory__c = true;
		sn.SCMC__Receiving_Inspection__c = ri.Id;    	
    	sn.SCMC__Serial_Number__c = '123456';
    	upsert sn;
    	
    	ri.SCMC__Lot_Number__c = 'Test Lot Number 2';
    	update ri;

    	SCMC__Serial_Number__c updateSN = [select Id, Lot_Number__c from SCMC__Serial_Number__c where Id = :sn.Id];
    	system.assertEquals(ri.SCMC__Lot_Number__c, updateSN.Lot_Number__c, 'Lot number is not correct');
    	    	
    }

   static testMethod void riSerialNumberWithLotNumberA() {

    	SetUpTestForSCM scmTestClass = new SetUpTestForSCM();
    	SCMC__Item__c item = scmTestClass.createTestItem(false);
    	item.SCMC__Inbound_Outbound_Serial_Number__c = true;
    	item.SCMC__Lot_Number_Control__c = true;
    	upsert item;
    	
    	SCMC__Supplier_Site__c supplier = [select ID from SCMC__Supplier_Site__c where SCMC__Active__c = true and SCMC__Account__c != null limit 1];
    	SCMC__Purchase_Order__c po = scmTestClass.createPurchaseOrder(supplier, true);
    	SCMC__Purchase_Order_Line_Item__c pol = scmTestClass.createPurchaseOrderLine(po, 'Item', false);
    	
    	po.SCMC__Status__c = 'Approved';
    	update po;
    	
    	SCMC__Receipt__c recHeader = new SCMC__Receipt__c();
    	recHeader.SCMC__Receipt_Type__c = 'SCMC__Receipt_Type__c';
    	recHeader.SCMC__Purchase_Order__c = po.Id;
    	upsert recHeader;
    	
    	SCMC__Receipt_Line__c recLine = new SCMC__Receipt_Line__c();
    	recLine.SCMC__Receiver__c = recHeader.Id;
    	recLine.SCMC__Quantity__c = 1;
    	recLine.SCMC__Lot_Number__c = 'Test Lot Number';
    	recLine.SCMC__Item_Number__c = item.Id;
    	recLine.SCMC__Status__c = 'Received';
    	upsert recLine;
    	    	
    	SCMC__Receiving_Inspection__c ri = new SCMC__Receiving_Inspection__c();
    	ri.SCMC__Item_Number__c = item.Id;
    	ri.SCMC__Quantity_to_Accept__c = 1;
    	ri.SCMC__Receipt_Line__c = recLine.Id;
    	ri.SCMC__Lot_Number__c = 'Test Lot Number 2';
		upsert ri;

    	SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
    	sn.SCMC__Item__c = item.Id;
    	sn.SCMC__Receipt_Line__c = recLine.Id;
    	sn.SCMC__InInventory__c = true;
		sn.SCMC__Receiving_Inspection__c = ri.Id;    	
    	sn.SCMC__Serial_Number__c = '123456';
    	upsert sn;

    	SCMC__Serial_Number__c updateSN = [select Id, Lot_Number__c from SCMC__Serial_Number__c where Id = :sn.Id];
    	system.assertEquals(ri.SCMC__Lot_Number__c, updateSN.Lot_Number__c, 'Lot number is not correct');
    	    	
    }
}