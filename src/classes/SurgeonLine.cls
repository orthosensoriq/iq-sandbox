public with sharing class SurgeonLine{

	public String zip;
	public String taxonomy_id {get;set;}
	public String city {get;set;}
	public String state {get;set;}
	public String first_name {get;set;}
	public String last_name {get;set;}
	public String address;
	public String fax;
	public String npi {get;set;}
	public String id;
	public String code;
	public String description {get;set;}
	public String pt;
	public String taxclass;
	public String speciality;
	public String url;
	public String parent_taxonomycode_id;
	public Boolean individual;

}