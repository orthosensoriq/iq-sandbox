@isTest (SeeAllData=true)
private class ImplantUtilizationControllerTest {
	
	static testMethod void testImplantUtilizationController() {
		
		//Construct controller
          ImplantUtilizationController con = new ImplantUtilizationController();

          //Execute code
          con.implantCompLines.clear();
          con.getResults();
          con.selectedStatuses.add('Used');
          for (Implant_Component__c i : [SELECT Lot_Number__c FROM Implant_Component__c LIMIT 1]) {
          	con.selectedLotNumbers.add(i.Lot_Number__c);
          }
          for (Account a : [SELECT Id FROM Account WHERE RecordType.Name = 'Hospital Account' LIMIT 1]) {
               con.selectedHospitals.add(a.Id);
          }
          for (Account a : [SELECT Id FROM Account WHERE RecordType.Name = 'Surgeon Account' LIMIT 1]) {
               con.selectedSurgeons.add(a.Id);
          }
          con.getResults();
          List<selectOption> hospitalOptions = con.getHospitalItems();
          String[] currHospitals = con.getHospitals();
          List<selectOption> surgeonOptions = con.getSurgeonItems();
          String[] currSurgeons = con.getSurgeons();
          List<selectOption> statusOptions = con.getStatusItems();
          String[] currStatuses = con.getStatuses();
          List<selectOption> lotNumberOptions = con.getLotNumberItems();
          String[] currLotNumbers = con.getLotNumbers();
	}
	
}