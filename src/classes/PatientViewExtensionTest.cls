@isTest
private class PatientViewExtensionTest {
	
	static testMethod void testPatientViewExtension() {

		//Create Patient to use for Testing
		Patient__c testPatient = new Patient__c();
		testPatient.First_Name__c = 'Test';
		testPatient.Last_Name__c = 'Patient';
		testPatient.Gender__c = 'Male';
		testPatient.Date_Of_Birth__c = date.parse('03/25/1950');
		insert testPatient;
		
		//Construct controller
		ApexPages.StandardController stdPatient = new ApexPages.StandardController(testPatient);
		PatientViewExtension conExt = new PatientViewExtension(stdPatient);

		//Execute code
		conExt.LogConstructor();
		string patientAge = conExt.getAge();
		testPatient.Gender__c = 'Female';
		conExt.save();
		PageReference pageRef = conExt.toggleEditView();
		PageReference pageRef2 = conExt.cancelEdit();
	}

}