@isTest
private class UserSettingsControllerTest {
    
    static testMethod void testUserSettingsController() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@orthosensor.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@orthosensor.com.test');
        
        System.runAs(u) {
            // Create Contact for test
            //Contact testContact = new Contact();
            //testContact.FirstName = 'Test';
            //testContact.LastName = 'Contact';
            //testContact.Phone = '770-555-1212';
            //testContact.MobilePhone = '678-555-1212';
            //insert testContact;
            
            //Construct controller
            userSettingsController con = new userSettingsController();
            
            // Execute code
            con.LogConstructor();
            con.showUserEdit();
            con.showPswdChange();
            con.showHIPAA();
            con.showEULA();
            con.showAuditLog();
            PageReference pageRef = con.viewAuditLog();
            PageReference pageRef1 = con.resetPassword();
        }
	}
	
}