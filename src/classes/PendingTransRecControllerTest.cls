@isTest()
public class PendingTransRecControllerTest {

    @testSetup 
    public static void setupTestData() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        RecordType trRec = [select id from recordtype where name = 'Transfer Request'];
        //User thisUser = [select Id from User where name = :UserInfo.getUserId()];
        User user1 = [select id, name from user where name = 'Mark Hottle'];
        
        /*User user1 = new User();
        user1.ProfileId = p.id;
        user1.alias = 'user1';
        user1.firstname = 'Test';
        user1.lastname = 'Smith';
        user1.Username = 'test@jamiesmiths.com';
        user1.Email = 'test@jamiesmiths.com';
        user1.CommunityNickname = 'test';
        user1.TimeZoneSidKey = 'America/New_York';
        user1.LocaleSidKey = 'en_US';
        user1.EmailEncodingKey = 'UTF-8';
        user1.LanguageLocaleKey = 'en_US';
        insert user1;*/
        
        SCMC__Currency_Master__c cm = new SCMC__Currency_Master__c();
        cm.Name = 'USD';
        cm.SCMC__Active__c = true;
        cm.SCMC__Number_of_decimals__c = 2;
        cm.SCMC__Corporate_Currency__c = true;
        insert cm;
        
        SCMC__ICP__c icp = new SCMC__ICP__c();
        icp.SCMC__Currency__c = cm.id;
        insert icp;
        
        SCMC__Warehouse__c srcWare = new SCMC__Warehouse__c();
        srcWare.name = 'DCOTA';
        srcWare.SCMC__ICP__c = icp.id;
        srcWare.SCMC__Active__c = true;
        //srcWare.Receiving_User__c = user1.id;
        insert srcWare;
        
        SCMC__Warehouse__c desWare = new SCMC__Warehouse__c();
        desWare.name = 'Rep - Mark Hottle';
        desWare.SCMC__ICP__c = icp.id;
        desWare.SCMC__Active__c = true;
        desWare.Receiving_User__c = user1.id;
        insert desWare;
        
        SCMC__Reason_Code__c rc = new SCMC__Reason_Code__c();
        rc.Name = 'Rep Stock Replenish';
        insert rc;
        
        SCMC__Item__c item = new SCMC__Item__c();
        item.SCMC__Item_Description__c = 'Test';
        item.SCMC__Item_Type__c ='Kit';
        insert item;
        
        SCMC__Inventory_Position__c pos = new SCMC__Inventory_Position__c();
        pos.SCMC__Acquisition_Currency__c = cm.id;
        pos.SCMC__ICP_Currency__c = icp.SCMC__Currency__c;
        pos.SCMC__Item_Master__c = item.id;
        pos.SCMC__Current_Value__c = 2;
        insert pos;
        
        SCMC__Transfer_Request__c tr = new SCMC__Transfer_Request__c();
        tr.SCMC__Reason_Code__c = rc.id;
        tr.SCMC__Destination_Warehouse__c = desWare.id;
        tr.SCMC__Source_Warehouse__c = srcWare.id;
        tr.SCMC__Carrier_Service__c = 'FedEx 2Day';
        tr.recordtype = trRec;
        tr.SCMC__Carrier__c = 'FedEx';
        //tr.SCMC__Inventory_Position__c = pos.id;
        insert tr;
        
        SCMC__Transfer_Request_Line__c trl = new SCMC__Transfer_Request_Line__c();
        //trl.name = 'SNN-JRNYCR56-R-NF';
        trl.SCMC__Quantity__c = 1;
        trl.SCMC__Inventory_Position__c = pos.id;
        trl.SCMC__Transfer_Request__c = tr.id;
        insert trl;
        
        SCMC__Shipping__c shipment = new SCMC__Shipping__c();
        shipment.SCMC__Status__c = 'test';
        shipment.SCMC__Receiving_Warehouse__c = srcWare.id;
        shipment.SCMC__Transfer_Request__c = tr.id;
        insert shipment;
        
        SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
        sn.SCMC__Serial_Number__c = '0123456789';
        sn.SCMC__Shipping__c = shipment.id;
        insert sn;
        
        SCMC__Serial_Number_Related_Record__c serialNumRelatedRecs = new SCMC__Serial_Number_Related_Record__c();
        serialNumRelatedRecs.SCMC__Serial_Number__c = sn.id;
        serialNumRelatedRecs.SCMC__Shipping__c = shipment.id;
        insert serialNumRelatedRecs;
        
        SCMC__Ownership_Code__c ownCode = new SCMC__Ownership_Code__c();
        ownCode.name = 'Mark Hottle';
        ownCode.ownerId = user1.id;
        insert owncode;//system.debug(owncode.id+' '+owncode.name);
        
    }
    
    @isTest
    public static void PendingTransRecTest() {
        
        //TestDataFactory.setupTestData();
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        //User user1 = [select id, name from user where alias = 'user1'];
        User user1 = [select id, name from user where name = 'Mark Hottle'];
        SCMC__Warehouse__c srcWare = [select id, name, SCMC__Active__c, Receiving_User__c from SCMC__Warehouse__c where Receiving_User__c = :user1.id];
        SCMC__Warehouse__c desWare = [select id, name, SCMC__Active__c, Receiving_User__c from SCMC__Warehouse__c where Receiving_User__c != :user1.id limit 1];
        SCMC__Transfer_Request__c transRequest = [select id from SCMC__Transfer_Request__c limit 1];
        SCMC__Transfer_Request_Line__c transRequestLine = [select id, name, SCMC__Transfer_Request__c from SCMC__Transfer_Request_Line__c where SCMC__Transfer_Request__c = :transRequest.id];
        SCMC__Shipping__c shipment = [select id, SCMC__Status__c, SCMC__Receiving_Warehouse__c, SCMC__Transfer_Request__c from SCMC__Shipping__c limit 1];
        SCMC__Serial_Number__c sn = [select id, name from SCMC__Serial_Number__c limit 1];
        List<SCMC__Serial_Number_Related_Record__c> serialNumRelatedRecsSize = [select id, name from SCMC__Serial_Number_Related_Record__c where SCMC__Shipping__c = :shipment.id];
        SCMC__Ownership_Code__c ownCode = [SELECT Id, Name FROM SCMC__Ownership_Code__c LIMIT 1];
        system.debug(owncode.id+' '+owncode.name);
        
        System.RunAs(user1){
            
            PendingTransRecController cont = new PendingTransRecController();
            
            cont.selectedShipment = shipment.id;
            //cont.serialNumRelatedRecsSize = serialNumRelatedRecsSize.size();
            cont.showShips = true;
            cont.showShip = true;
            cont.reRender = true;
            
            cont.selectedShipment();
            cont.createCase();
            cont.updateCase();
            cont.markComplete();
            
            cont.cancel();
            
            PendingTransRecController.mkGoToCase();
        }
    }
}