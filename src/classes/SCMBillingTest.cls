@isTest
private class SCMBillingTest {
    static SetUpTestForSCM st = new SetUpTestForSCM();

    static testMethod void testBillingJob() {
        //bill(false,false);
    }
    static testMethod void testBillingbutton() {
        //bill(true,false);
    }
    static testMethod void testBadPlugin() {
        //bill(true,true);
    }

    private static void bill(boolean usebutton, boolean badPlugin) {

        SCMC__SCM_Plugins__c plugins = SCMC__SCM_Plugins__c.getInstance();
        if (plugins == null){
            plugins = new SCMC__SCM_Plugins__c();
        }
        if (badPlugin){
            plugins.SCMC__Recur_Invoice_Plugin__c = 'SCMC.Unknown';
        } else {
            plugins.SCMC__Recur_Invoice_Plugin__c = 'SCMRecureInvoice';
        }
        upsert plugins;

        //create serice terms
        SCMC__Service_Term__c terms = st.createTestServiceTerms();

        //create general ledger accounts
        st.createGLAccounts();

        //create account with payment terms
        SCMC__PO_Payment_Terms__c payTerm = st.createPOPaymentTerm();
        Account acct = st.createTestCustomerAccount(true, false, false, null);
        acct.SCMC__Payment_Terms__c = payTerm.id;
        insert acct;

        //create product

        //create product group
        SCMC__Product_Group__c pgroup = st.creteTestProductGroupWithProduct();
        pgroup.Service_Term_for_Product__c = terms.id;
        System.assertNotEquals(null, pgroup.Product__c);
        update pgroup;

        //create service item
        SCMC__Item__c item = st.createTestItem(false);
        item.SCMC__Product_Group__c = pgroup.id;
        insert item;
        SCMC__Sales_Order__c so = st.createTestSalesOrder(true);
        SCMC__Sales_Order_Line_Item__c sol = st.createSalesOrderLine(so, item, 'New', true);

        //create service order
        SCMC__Service_Order__c order = st.createTestServiceOrder(acct, terms);
        SCMC__Service_Order_Line__c line = st.createTestServiceLine(order, item);
        order.SCMC__Billing_Status__c = 'Open';
        order.SCMC__Status__c = 'Activated';
        order.RecordTypeId =  SCM_Utilities.getRecordType('Activated', order);
        update order;

        List<c2g__codaGeneralLedgerAccount__c> codaGLAList = [
            select
                Id
            from c2g__codaGeneralLedgerAccount__c
            where name = 'testInventoryShipped' limit 1];
        System.assert(codaGLAList.size() == 1);

        insert
            new SCMC__GL_Account__c(
                SCMC__SCM_Account__c = 'Shipped not Invoiced',
                SCMFFA__General_Ledger_Account__c = codaGLAList.get(0).Id);

        //start test
        Test.startTest();
        if (usebutton){
            //invoke through button
            List<c2g__codaInvoice__c> ffinvoice = new List<c2g__codaInvoice__c>();
            ApexPages.StandardSetController controller =
                new ApexPages.StandardSetController(ffinvoice);
            SCMBilling extController = new SCMBilling(controller);
            try {
                extController.runBillEngine();
            } catch (Exception ex){
                if (badPlugin){
                    //this is good
                } else {
                    System.assert(false, 'button launch failed');
                }
            }
        } else {
            //run billing job
            SCMScheduledBilling billOrders = new SCMScheduledBilling();
            billOrders.limRecords = 1;
            Database.executeBatch(billOrders,50);
        }
        //stop test
        Test.stopTest();

        //confirm invoice
        if (!badplugin){
            SCMC__Invoicing__c[] scmInv = [
                select
                    id,
                    Name,
                    SCMC__Service_Contract__c,
                    SCMC__Export_Status__c
                from SCMC__Invoicing__c
                where
                    SCMC__Service_Contract__c = :order.id and
                    SCMC__Export_Status__c = 'New'
                limit 1];
            System.assertEquals(1, scmInv.Size());
        }
    }
}