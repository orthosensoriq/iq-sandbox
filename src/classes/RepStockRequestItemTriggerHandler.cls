/* Human.txt Jamie Smith - Coastal Cloud - jamie.smith@coastalcloud.us */
public with sharing class RepStockRequestItemTriggerHandler {
	
	/* Objects, vars to be used */
    Rep_Stock_Request_Item__c item = new Rep_Stock_Request_Item__c();
    List<Rep_Stock_Request_Item__c> items = new List<Rep_Stock_Request_Item__c>();
    List<SCMC__Inventory_Position__c> poss = new List<SCMC__Inventory_Position__c>();
    
    String returnString;
    String errorMsg;
    Boolean hasError;
    
    /* Logic */
    public String checkForDups(Id masterId, Id newItem, Id newItemId){
        
        items = getItems(masterId);
        hasError = false;
        
        for(Rep_Stock_Request_Item__c item : items){
            system.debug('is ' + newItem + ' = to ' +item.Item_Master__c);
            if(item.Item_Master__c == newItem && item.id != newItemId){
                
				errorMsg = '  This item is already added to your request. Please add your additional quantity needed to the existing line item.';  
				hasError = true;
				
            } 
		}
		
		if(hasError){
			return errorMsg;
		}
		
		return 'No Error';
        
    }
    
    public void makeParentStatusNew(Id masterId){
        
        Rep_Stock_Request__c masterReq = [ SELECT Id, Status__c FROM Rep_Stock_Request__c WHERE Id = :masterId ];
			
		if(masterReq.Status__c != 'New'){
			masterReq.Status__c = 'New';
			update masterReq;
			System.debug('masterReq Status set to New');
		} else {
		    System.debug('masterReq Status was already New');
		}
		
    }
    
    public String isWantLessThanInventory(Id itemId, Decimal want){
		
		item = getItem(itemId);
		poss = getInventoryInStock();
		
		Decimal allo = 0;		
		Decimal quan = 0;
		for(SCMC__Inventory_Position__c pos : poss){
		    
		
		    if(item.Item_Master__c == pos.SCMC__Item_Master__c){
		        allo = allo + pos.SCMC__Quantity_Allocated__c;
		        quan = quan + pos.SCMC__Quantity__c;
		    }
		}
		
		Decimal quanAlloFive = quan - allo - 5;
		String returnString = 'No Error';
		
		if(want <= quanAlloFive){
			
			system.debug('Inventory has enough.');
			
		} else {
		    
			Decimal offBy = quanAlloFive - want;
			offBy = offBy * -1;
			returnString = 'Quantity needs at least ' + offBy.stripTrailingZeros() + ' to be removed. (Inventory only has ' + quanAlloFive.stripTrailingZeros() + ')';  
			
		}
		
		return returnString;
		
	}
	
    /* Queries */
    public List<Rep_Stock_Request_Item__c> getItems(Id masterId){
        
        items = [
            SELECT Rep_Stock_Request__c, Quantity__c, Item_Master__c, Name, Item_Master__r.Name, Id 
            FROM Rep_Stock_Request_Item__c
            WHERE Rep_Stock_Request__c = :masterId
        ];
        
        return items;
    }
    
    public Rep_Stock_Request_Item__c getItem(Id itemId){
		
		item = [
			SELECT Rep_Stock_Request__c, Quantity__c, Item_Master__c, Name, Item_Master__r.Name, Id 
			FROM Rep_Stock_Request_Item__c
			WHERE id = :itemId
		];
		
		return item;
	}
    
    public List<SCMC__Inventory_Position__c> getInventoryInStock(){
		
		poss = [
			SELECT Id, Name, SCMC__Item_Master__c, SCMC__Item_Master__r.name, SCMC__Item_Serial_Number__c, SCMC__Item_Warehouse__c, 
				SCMC__Quantity__c, SCMC__Location__c, SCMC__Receipt_Line__c, SCMC__Serial_Number__c, SCMC__Warehouse__c, SCMC__Lot_Number__c, 
				SCMC__Availability_Code__c, SCMC__Bin__c,  SCMC__Bin__r.name, SCMC__Quantity_Allocated__c
			FROM SCMC__Inventory_Position__c
			WHERE SCMC__Availability_Code__c = 'In Stock'
			AND SCMC__Bin__r.name = 'Finished Goods'
			Order BY Name
		];
		
		return poss;
	}
	
}



    /*
    
    
    
    /* Choose what to do when 
    /*public void handleBeforeUpdate(Id masterId){
        System.debug(' ');
        
        System.debug('In handler: ' + masterId + ' was passed in. Running isWantLessThanInventory...');
        //isWantLessThanInventory(masterId);
        
        System.debug(' ');
    }
    public void handleBeforeInsert(Id masterId){
        System.debug(' ');
        
        System.debug('In handler: ' + masterId + ' was passed in. Running isWantLessThanInventory...');
        //isWantLessThanInventory(masterId);
        
        System.debug(' ');
    }
    
    //List<wrappedItems> wrappedItems = new List<wrappedItems>();
    //wrappedItems wrappedItem = new wrappedItems();
    
    // Empty constructor - unnecessary in apex but may be used later
    public RepStockRequestItemTriggerHandler(){}
    
    
    
    
    public String isWantLessThanInventory(Id masterId){
        
        items = new List<Rep_Stock_Request_Item__c>();
        invents = new List<SCMC__Inventory_Position__c>();
        wrappedItems = new List<wrappedItems>();
        wrappedItem = new wrappedItems();
        
        // Fill objs w/ queries
        items = getItems(masterId);
        invents = getInventoryInStock();
        
        // Logic
        for(Rep_Stock_Request_Item__c item : items){
            
            wrappedItem.item = item.Item_Master__c;
            wrappedItem.itemName = item.Item_Master__r.name;
            wrappedItem.wantedQty = item.Quantity__c;
            wrappedItem.haveQty = 0;
            
            for(SCMC__Inventory_Position__c invent : invents){
                
                if(item.Item_Master__c == invent.SCMC__Item_Master__c){
                    //system.debug('old: invent.bin.name = ' + invent.SCMC__Bin__r.name);
                    Decimal oldQty = wrappedItem.haveQty;
                    wrappedItem.haveQty = oldQty + invent.SCMC__Quantity__c;
                    wrappedItem.haveQty = wrappedItem.haveQty - invent.SCMC__Quantity_Allocated__c - 5;
                }
            }
            
            wrappedItems.add(wrappedItem);
            
            System.debug('wrappedItems ' + wrappedItems);
        }
        
        System.debug('');
        System.debug('# of wrapped items ' + wrappedItems.size());
        
        integer i = 0;
        boolean hasError = false;
		
		for(wrappedItems wi : wrappedItems){
			
			if(wi.wantedQty < wi.haveQty){
				
				system.debug(wrappedItem.itemName + ': Inventory has enough.');
				
			} else {
				
				wi.haveQty = wi.haveQty - 5;
				Decimal resultQty = wi.wantedQty - wi.haveQty;
				returnString = '  Item ' + wi.itemName +  ' needs at least ' + resultQty.setScale(2) + ' to be removed. (Inventory only has ' + wi.haveQty.setScale(2) + ')';  
				hasError = true;
				system.debug(wi.itemName + ' needs removed. (not enough in inventory)');
				
			}
		}
		
		// Handle errors / returns
		if(hasError){
			return returnString;
		} else {
			returnString = 'No Error';
		}
		
		return returnString;
    }
    //public List<SCMC__Inventory_Position__c> getInventoryInStock(){
        
        invents = [
            SELECT Id, Name, SCMC__Availability__c, SCMC__Item_Master__c, SCMC__Item_Master__r.name, SCMC__Item_Serial_Number__c, SCMC__Item_Warehouse__c, 
                SCMC__Quantity__c, SCMC__Location__c, SCMC__Receipt_Line__c, SCMC__Serial_Number__c, SCMC__Warehouse__c, SCMC__Lot_Number__c, 
                SCMC__Availability_Code__c, SCMC__Bin__c,  SCMC__Bin__r.name, SCMC__Shelf_Life_Expiration__c, SCMC__Quantity_Allocated__c
            FROM SCMC__Inventory_Position__c
            WHERE SCMC__Availability_Code__c = 'In Stock'
            AND SCMC__Bin__r.name = 'Finished Goods'
            Order BY Name
        ];
        
        return invents;
    }

    
    /* Wrapper class
    public class wrappedItems {
        
        public Id item;
        public String itemName;
        public Decimal wantedQty;
        public Decimal haveQty;
        
        public wrappedItems(){}
        
    }
    
    */