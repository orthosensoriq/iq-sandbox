global class IQ_PhaseCountAggregator implements Schedulable {
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Integer monthsToAnalyze {get; set;}
    Date firstDayOfFirstMonth {get; set; }
    Date lastDayofLastMonth {get; set;}
    public List<IQ_Phase_Count__c> monthlyCountList {get; set;}
    public Map<String, Surgeon_Hospital__c> mappedPractices {get; set;}

    public Integer monthsInFuture {get; set;}
    public Integer monthsInThePast {get; set;}
    
    public IQ_PhaseCountAggregator() {
        //set these if the program durations change
        monthsInFuture = 12;
        monthsInThePast = 6;
        SetDates();
        mappedPractices = GetMappedSurgeonPractices();
        System.debug(mappedPractices);
    }

    global void execute(SchedulableContext ctx) {
        PhaseCountAggregatorBySurgeon();
    }
    
	global void PhaseCountAggregatorBySurgeon() {
        //clear table
        List<IQ_Phase_Count__c> toDelete = [SELECT Id, Name FROM IQ_Phase_Count__c];
        delete toDelete;
        // after counting the month get monthly records  - To DO: if there are too many, we may need to search on each record insert
        List<IQ_Phase_Count__c> existingMonthlyPhaseCounts = GetMonthlyPhaseCounts();
        System.debug( firstDayOfFirstMonth );
        // Cycle for months - start 2 months prior and go 6 months out      
        for (Integer i = 0; i < monthsToAnalyze; i++ ) {
            // System.debug('Month update');
            // get start month and end month
            startDate = firstDayOfFirstMonth.AddMonths(i); 
            endDate = startDate.addDays(Date.daysinMonth(startDate.year(), startDate.month()) - 1);
            System.debug(startDate);

            // retrieve all cases created in the month
            List<Event__c> events = GetMonthlyCases(startDate);
            System.debug(events);
            // list of cases that had already started in that month
            // init list array for month
            monthlyCountList = new List<IQ_Phase_Count__c>();
            
            // this goes through all events to see if the are part of the month
            // ProcessEvents(events);
            CreateFuturePhaseCounts(startDate, events); 
            
            UpsertPhaseCounts(monthlyCountList, existingMonthlyPhaseCounts, mappedPractices);
            // then search and upsert existing records.
        }
    } 

    public void ProcessEvents(List<Event__c> events) {
        // System.debug('Processing events');
        for(Event__c evnt: events) {
            // if the procedure date is in the future and the creation date for the 
            // event is in the current month or before, 
            //add to surgeon aggregate for that month as pre-op
            System.debug(evnt);
            if (evnt.Appointment_Start__c != null) {
                Date appointmentDate = Date.newInstance(evnt.Appointment_Start__c.year(), evnt.Appointment_Start__c.month(), evnt.Appointment_Start__c.day());
                if (evnt.Appointment_Start__c > endDate && evnt.CreatedDate <= endDate) {
                    CreateOrAddToListRecords(evnt, 'PreOp', startDate);
                } else {
                    // if the procedure is in current month and marked as completed
                    //add to the surgeon as Post op for that month   
                    // system.debug(evnt.Appointment_Start__c);
                    if (evnt.Appointment_Start__c <= endDate && evnt.Appointment_Start__c >= startDate && evnt.Status__c == 'Complete') {
                        CreateOrAddToListRecords(evnt, 'PostOp', startDate);
                    } else {
                        if (evnt.Appointment_Start__c < endDate && evnt.Status__c == 'Complete') {
                            // if the procedure is in a past month, calculate the days from the 
                            // completed procedure date 
                            System.debug(appointmentDate.daysBetween(endDate));
                            if (appointmentDate.daysBetween(endDate) >= 90) {
                                CreateOrAddToListRecords(evnt, 'Long Term', startDate);
                            } else {
                                CreateOrAddToListRecords(evnt, 'PostOp', startDate);
                            }
                        }
                    }
                }
            }
        }
    }

    public List<Event__c> GetMonthlyCases(Date startDate) {
        Date lastDayInMonth = Date.newInstance(startDate.year(), startDate.month(), Date.daysInMonth(startDate.year(), startDate.month()));
        Date firstDayInMonth = Date.newInstance(startDate.year(), startDate.month(), 1);
        List<Event__c> events = [Select Id, Name, CreatedDate, Appointment_Start__c, Physician__c, Physician__r.Parent.Id, Physician__r.Name, Status__c 
                                from Event__c 
                                where Event_Type_Name__c = 'Procedure'
                                and CreatedDate >= :firstDayInMonth
                                and CreatedDate <= :lastDayInMonth];
        return events;
    }

    public Map<String, Surgeon_Hospital__c> GetMappedSurgeonPractices() {
        List<Surgeon_Hospital__c> practices = [Select Surgeon__r.Id, Surgeon__r.AccountId, Surgeon__r.Name, Hospital__r.Id, Hospital__r.Name 
                    from Surgeon_Hospital__c 
                    Where Hospital__r.IsPartner = true AND Surgeon__r.Account.RecordType.Name = 'Surgeon Account' AND Hospital__r.RecordType.Name = 'Practice Account' 
                    Order By Surgeon__r.Name];
        Map<String, Surgeon_Hospital__c> maps = new Map<String, Surgeon_Hospital__c>();
        for(Surgeon_Hospital__c practice: practices) {
            maps.put(practice.Surgeon__r.AccountId, practice);
        }            
        System.debug(maps);
        return maps;
    }

    public List<IQ_Phase_Count__c> GetMonthlyPhaseCounts() {
        return [Select ID, Name, Surgeon_ID__c, Parent_ID__c, Count__c, Month_Date__c, Month__c, Year__c 
        from IQ_Phase_Count__c];
                                                   //where Month_Date__c =: startDate];
    }

    public void CreateOrAddToListRecords(Event__c evnt, string phase, Date eventStartDate) {
        // Find existing record
        Boolean tflag = false;
        // System.debug(eventStartDate);

        for(IQ_Phase_Count__c counter: monthlyCountList ) {
            tflag = false;
            // System.debug(counter.Month_Date__c);
            if (counter.Surgeon_ID__c == evnt.Physician__c 
                && counter.Month_Date__c == eventStartDate 
                && counter.Name == phase) {
                    counter.Count__c++;
                    tflag = true;
                    break;
                }
        } 
        if (!tflag) {
            IQ_Phase_Count__c phaseCount = new IQ_Phase_Count__c();
            phaseCount.Surgeon_ID__c = evnt.Physician__c;
            // System.debug(evnt.Physician__c);
            Surgeon_Hospital__c practice = mappedPractices.get(evnt.Physician__c);
            
            // System.debug(practice);
            if (practice != null) {
                phaseCount.Surgeon_Name__c = practice.Surgeon__r.Name;
                phaseCount.Parent_ID__c = practice.Hospital__r.Id;
                phaseCount.Practice_Name__c = practice.Hospital__r.Name;
            } else {
                phaseCount.Surgeon_Name__c = evnt.Physician__r.Name;
            }
            phaseCount.Month_Date__c = eventStartDate;
            phaseCount.Count__c = 1;
            phaseCount.Name = phase;
            // System.debug('adding to array');
            monthlyCountList.add(phaseCount);
            // System.debug(monthlyCountList);
        }
    }

    public void SetDates() {
        monthsToAnalyze = monthsInFuture + monthsInThePast ;
        Date today = Date.today();
        System.debug(today);
        Date firstDayofThisMonth = today.toStartofMonth();
        System.debug(firstDayofThisMonth);
        firstDayOfFirstMonth = firstDayofThisMonth.AddMonths(-(monthsInThePast));
        System.debug(firstDayOfFirstMonth);
        lastDayofLastMonth = today.toStartofMonth().AddDays(Date.daysInMonth(today.year(), today.month())-1).AddMonths(monthsInFuture);
        //System.debug(lastDayofLastMonth);
    }
    
    public void UpsertPhaseCounts(List<IQ_Phase_Count__c> monthlyCountList, List<IQ_Phase_Count__c> existingMonthlyPhaseCounts, Map<String, Surgeon_Hospital__c> mappedPractices) {
        //find record for surgeon month year and phase 
        Boolean tflag = false;
        // System.debug('Upserting!');
        for(IQ_Phase_Count__c counter: monthlyCountList) {
            tflag = false;
            IQ_Phase_Count__c counter1 = counter;
            for (IQ_Phase_Count__c existing: existingMonthlyPhaseCounts) {
                if (counter.Surgeon_ID__c == existing.Surgeon_ID__c
                    && counter.Month_Date__c == existing.Month_Date__c
                    && counter.Name == existing.Name) {
                    existing.Count__c = counter.Count__c;
                    tflag = true;
                    break;
                }
            }
            if (!tflag) {
                existingMonthlyPhaseCounts.Add(counter);
            }
        } 
        // System.debug('About to upsert');
        upsert existingMonthlyPhaseCounts;   
    }

    //for the events created in a particular month, create future phase counts
    public void CreateFuturePhaseCounts(Date beginStartDate, List<Event__c> events) {
        // System.debug(beginStartDate);
        // System.debug(events);
        
        for(Event__c evnt: events) {
            Date eventEndDate = beginStartDate.addDays(Date.daysinMonth(beginStartDate.year(), beginStartDate.month()) - 1);
            Date eventStartDate = beginStartDate;
            System.debug(evnt);
            if (evnt.Appointment_Start__c != null) {
                Date appointmentDate = Date.newInstance(evnt.Appointment_Start__c.year(), evnt.Appointment_Start__c.month(), evnt.Appointment_Start__c.day());
            
                // cancel user event if the user is stale, i.e., they are 90 days past procedure tne ht eprocedure is not marked completed
                if (appointmentDate.daysBetween(eventEndDate) >= 90 && evnt.Status__c != 'Complete' ) {
                    continue;
                }
                AddFuturePhaseCountMonthsForEvent(beginStartDate, evnt);
            }
        }
    }

    public void AddFuturePhaseCountMonthsForEvent(Date beginStartDate, Event__c evnt) {
       Date eventStartDate = beginStartDate;
       Date appointmentDate = Date.newInstance(evnt.Appointment_Start__c.year(), evnt.Appointment_Start__c.month(), evnt.Appointment_Start__c.day());
       for (Integer i = 0; i < monthsInFuture; i++) {
           System.debug(i);
          eventStartDate = beginStartDate.AddMonths(i);
          System.debug(eventStartDate);
          Date eventEndDate = eventStartDate.addDays(Date.daysinMonth(eventStartDate.year(), eventStartDate.month()) - 1);
          // System.debug(eventEndDate);
          
          if (evnt.Appointment_Start__c >= eventStartDate) {
            if (evnt.Appointment_Start__c < eventEndDate) { // && evnt.CreatedDate <= eventEndDate) {
                CreateOrAddToListRecords(evnt, 'PreOp', eventStartDate);
            } else {
                // if the procedure is in current month and marked as completed
                // add to the surgeon as Post op for that month   
                // system.debug(evnt.Appointment_Start__c);
               if (evnt.Appointment_Start__c <= eventEndDate) {
                   if (evnt.Status__c == 'Complete') {
                       CreateOrAddToListRecords(evnt, 'PostOp', eventStartDate);
                   } else {
                       CreateOrAddToListRecords(evnt, 'PreOp', eventStartDate);
                   }
               }
           }
        } else {
           if (evnt.Appointment_Start__c < eventEndDate && evnt.Status__c == 'Complete') {
              // if the procedure is in a past month, calculate the days from the 
              // completed procedure date 
              System.debug(appointmentDate.daysBetween(eventEndDate));
              if (appointmentDate.daysBetween(eventEndDate) >= 90) {
                  CreateOrAddToListRecords(evnt, 'Long Term', eventStartDate);
              } else {
                  CreateOrAddToListRecords(evnt, 'PostOp', eventStartDate);
              }
            }
        } 
       }
    } 
}