/*****************************************************
This class is designed to provide org wide utility
methods.

REVISIONS
------------------------------------------------------
12/17/2013 - support@enablepath.com - Created

*****************************************************/
global class epUtils {
	//************************************************
	//****  this method returns the datetime for the
	//**** number of business hours from now based on
	//**** the input
	//************************************************
	global static Datetime getBusinessHoursDatetime(BusinessHours bh, Decimal hoursFromNow){
		//convert deadline to long milliseconds
		Long interval = Math.roundToLong(hoursFromNow) * 60 * 60 * 1000L;
		return BusinessHours.add(bh.id, Datetime.now(), interval);
	}
	
	/* this method returns a string list of all the names from an object to be used in autocompletion scripts */
	@RemoteAction
	global static List<String> getSObjectNames(String sObjectToken){
		String soql = 'SELECT Name FROM ' + sObjectToken + ' LIMIT 10000';
		List<String> stringList = new List<String>();
		for(SObject sObj   : Database.query(soql)){
			stringList.add((String)sObj.get('Name'));
		}
		return stringList;
	}
    
    // this method returns a select statement for all the fields in a given object
    global static String getSelect(Schema.SObjectType objectType){
        String queryString = 'SELECT Id';
        for(String s: objectType.getDescribe().fields.getMap().keySet()){
            if(s!='Id') queryString +=  ', ' + s;
        }
        return   queryString;
    }
	
	//	CONSTRUCTORS
    global epUtils(ApexPages.StandardController sc){
    	//generic standard controller constructor so that it can be used as an extension on any page 
    }

    static final String CLASSNAME = '\n\n**** epUtils';

	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to determine if a String is NULL or has zero length ("empty")
	//@paramaters : a String to evaluate
	//@returns : a Boolean indicating if a String has an actual value or not
    global static Boolean StringIsNullOrEmpty(String pStringToValidate)
    {
        String METHODNAME = CLASSNAME + '.StringIsNullOrEmpty(String pStringToValidate)';
        Boolean returnValue = (pStringToValidate == null);
        if(!returnValue)
            returnValue = (pStringToValidate.trim().equals(''));
        return returnValue;
    }

	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to evaluate a String and return a default value if the String is invalid
	//@paramaters : a String to evaluate & a String to use as the default value if needed
	//@returns : a String representing the String that was passed in OR a default value if the String is NUll/Empty
    global static String StringGetValueOrDefault(String pStringToValidate, String pDefaultValue)
    {
        String METHODNAME = CLASSNAME + '.StringGetValueOrDefault(String pStringToValidate, String pDefaultValue)';
        system.debug(LoggingLevel.INFO, METHODNAME.replace('**** ', '**** Inside ') + ' :: String to validate = ' + pStringToValidate + ' and Default = '+ pDefaultValue +'\n\n');
        String returnValue = pDefaultValue;
        if (epUtils.StringIsNullOrEmpty(pStringToValidate) == false)
            returnValue = pStringToValidate;
        return returnValue;
    }   
     
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : 
	//@paramaters : none
	//@returns : a Boolean representing ...
	global static Boolean ValidateSaveresultList(list<Database.Saveresult> pResults)
    {
        String METHODNAME = CLASSNAME + '.ValidateSaveresultList(list<Database.Saveresult> pResults)';
        system.debug(LoggingLevel.INFO, METHODNAME.replace('**** ', '**** Inside ') + ' :: pResults.size() = ' + pResults.size() + '\n\n');
        Boolean returnValue = true;
        for(Integer iLoop = 0;iLoop<pResults.size();iLoop++)
        {
        	system.debug(LoggingLevel.INFO, METHODNAME + ' :: Result #' + iLoop + ' isSuccess() = '+ pResults[iLoop].isSuccess() +'\n\n');
        	if(pResults[iLoop].isSuccess() == false)
        	{
        		returnValue = false;
        		break;
        	}
        }
        return returnValue;
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to determine if a collection of Objects is populated or not
	//@paramaters : collection of Objects
	//@returns : a Boolean indicating if the collection of Objects is populated or not
    global static Boolean ValidateList(list<object> pList)
    {
        String METHODNAME = CLASSNAME + '.ValidateList(list<object>)';
        Boolean returnValue = true;
        if(pList == null){return false;}
        if(pList.size() < 1){return false;}
        if(pList.isEmpty()){return false;}
        return returnValue;
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to determine if a collection of SObjects is populated or not
	//@paramaters : collection of SObjects
	//@returns : a Boolean indicating if the collection of SObjects is populated or not
    global static Boolean ValidateList(list<sObject> pList)
    {
        String METHODNAME = CLASSNAME + '.ValidateList(list<sObject>)';
        Boolean returnValue = true;
        if(pList == null){return false;}
        if(pList.size() < 1){return false;}
        if(pList.isEmpty()){return false;}
        return returnValue;
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to determine if a collection of key/value pairs - SObjects and their IDs - is populated or not
	//@paramaters : a collection of key/value pairs - SObjects and their IDs
	//@returns : a Boolean indicating if the collection of key/value pairs is populated or not
    global static Boolean ValidateMap(map<Id, SObject> pMap)
    {
        String METHODNAME = CLASSNAME + '.ValidateMap(map<Id, SObject>)';
        Boolean returnValue = true;
        if(pMap == null){return false;}
        if(pMap.size() < 1){return false;}
        if(pMap.isEmpty()){return false;}
        return returnValue;
    }
/*
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : a method to evaluate a collection of Database.Error objects and extract "Message" & "Status Code" details to return as a String
	//@paramaters : a collection of Database.Error objects to evaluate and extract information from
	//@returns : a String representing the detail information - Messages & Status Codes - from ...
    global static String ListOfDatabaseErrorsToString(list<Database.Error> errList)
    {
        String METHODNAME = CLASSNAME + '.ListOfDatabaseErrorsToString(list<Database.Error> errList)';
        StringBuilder sb = StringBuilder.NewStringBuilder();
        for(Database.Error err : errList)
        {
            sb.AppendLine('DB Error Message: ' + err.getMessage());
            sb.AppendLine('DB Error Status Code: ' + err.getStatusCode());
        }
        return sb.AsString();
    }
*/
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to determine if an Object is found in a collection of Objects
	//@paramaters : an Object to search for, and a collection of Objects to evaluate
	//@returns : a Boolean indicating if the Object param is found in the collection of Objects
    global static Boolean ExistsInList(object toSearchFor, list<object> listOfAny)
    {       
        Boolean returnVal = false;
        for(object a : listOfAny)
        {
            returnVal = (a == toSearchFor);
            if(returnVal) break;
        }
        return returnVal;
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to 
	//@paramaters : a Database.BatchableContext object
	//@returns : nothing
    global static void BatchProcessFinishedNotification(Database.BatchableContext BC, String BatchJob)
    {
		// Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus FROM AsyncApexJob WHERE Id = :BC.getJobId()];  
	   
		// Email the Batch Job's submitter that the Job is finished.  
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
		String[] toAddresses = new String[] {a.CreatedBy.Email};  
		mail.setToAddresses(toAddresses);  
		mail.setSubject(BatchJob + ' Status: ' + a.Status);  
		mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus);  
	    
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to 
	//@paramaters : a String representing the character to use as a separator, and a collection ('Set') of sObject IDs
	//@returns : a delimited String of IDs
    global static String BuildStringOfIDsForInClause(Set<Id> pIds, String pSeparatorChar)
    {
    	String returnVal = '';
        Integer iLoop = 0;
        for(Id i:pIds)
        {
        	returnVal += '\'' + i + '\'';
        	if(iLoop < (pIds.size() - 1))
        		returnVal += pSeparatorChar;
        	iLoop++;
        }

    	return returnVal;
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to 
	//@paramaters : a String representing the character to use as a separator, and a list of sObjects
	//@returns : a delimited String of IDs 
    global static String BuildStringOfIDsForInClause(list<sObject> pSObjects, String pSeparatorChar)
    {
    	String returnVal = '';
        for(Integer iLoop = 0; iLoop <  pSObjects.size(); iLoop++)
        {
        	returnVal += '\'' + pSObjects[iLoop].Id + '\'';
        	if(iLoop < (pSObjects.size() - 1))
        		returnVal += pSeparatorChar;
        }

    	return returnVal;
    }

	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to get a RecordType ID from its Name
	//@paramaters : a String representing a RecordType Name
	//@returns : a String representing the RecordType ID based on the RecordType Name param
    global static String GetRecordTypeIdByRecordTypeName(String pRecordTypeName)
    {
    	map<Id, RecordType> RecordTypeMap = epUtils.GetRecordTypeMapBySobjectType('');
    	return GetRecordTypeIdByRecordTypeName(RecordTypeMap, pRecordTypeName);
    }
    
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to get a RecordType ID from its Name based on SObject type (i.e. "Account")
	//@paramaters : a String representing a RecordType Name and a String representing the Sobject Name (i.e. "Account")
	//@returns : a String representing the RecordType ID based on the params
	//@sample: GetRecordTypeIdBySobjectTypeAndRecordTypeName('Account', 'Prospect')
    global static String GetRecordTypeIdBySobjectTypeAndRecordTypeName(String pSobjectType, String pRecordTypeName)
    {
    	map<Id, RecordType> RecordTypeMap = epUtils.GetRecordTypeMapBySobjectType(pSobjectType);
    	return GetRecordTypeIdByRecordTypeName(RecordTypeMap, pRecordTypeName);
    }
    
    //@author : EnablePath
	//@date : 12/17/2013
	//@description : method gets a map of sObject Prefixes to sObject Names
	//@paramaters : none
	//@returns : Map<String,String>
    global static map<String,String> GetMapOfObjectPrefixesToObjectName(){
    	
    	map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
		map<String,String> keyPrefixMap = new Map<String,String>();		
		set<String> keyPrefixSet = globalDescribe.keySet();
		
		for(String sObj : keyPrefixSet){
		   Schema.DescribeSObjectResult describeSobjResult =  globalDescribe.get(sObj).getDescribe();
		   String tempName = describeSobjResult.getName();
		   String tempPrefix = describeSobjResult.getKeyPrefix();
		   System.debug('Processing Object['+tempName + '] with Prefix ['+ tempPrefix+']');
		   keyPrefixMap.put(tempPrefix,tempName);
		}
	
	    return keyPrefixMap;
	 }
	 
	//@author : EnablePath
	//@date : 12/17/2013
	//@description : method to get a RecordType ID from its Name
	//@paramaters : a collection of key/value pairs - RecordTypes & their IDs - and a String representing a RecordType Name
	//@returns : a String representing a RecordType ID based on the RecordType's Name
    private static String GetRecordTypeIdByRecordTypeName(map<Id, RecordType> pRecordTypeMap, String pRecordTypeName)
    {
    	String returnValue;
    	for(String key : pRecordTypeMap.keySet())
    	{
    		RecordType rt = pRecordTypeMap.get(key);
    		if(rt.Name.equals(pRecordTypeName))
    		{
    			returnValue = rt.Id;
    			break;
    		}
    	}
    	return returnValue;
    }

	//@author : EnablePath
	//@date : 12/17/2013
	//@description : 
	//@paramaters : a String representing the name of the SObject we want RecordTypes for (i.e. "Account")
	//@returns : a collection of key/value pairs - the RecordTypes & their IDs - for the type of object passed in
	//			OR
	//			a collection of key/value pairs - the RecordTypes & their IDs - for ALL ACTIVE RecordTypes
    public static map<Id, RecordType> GetRecordTypeMapBySobjectType(String pSobjectType)
    {
    	map<Id, RecordType> RecordTypeMap = (epUtils.StringIsNullOrEmpty(pSobjectType))
    										? (new map<Id, RecordType>([SELECT r.BusinessProcessId, r.Description, r.DeveloperName, r.Id, r.IsActive, r.Name, r.NamespacePrefix, r.SobjectType FROM RecordType r WHERE r.IsActive = true]))
    										: (new map<Id, RecordType>([SELECT r.BusinessProcessId, r.Description, r.DeveloperName, r.Id, r.IsActive, r.Name, r.NamespacePrefix, r.SobjectType FROM RecordType r WHERE r.IsActive = true and r.SobjectType = :pSobjectType]));
    	
    	return RecordTypeMap;
    }    

	//@author : EnablePath
	//@date : 02/13/2014
	//@description : Based on http://aa.usno.navy.mil/faq/docs/JD_Formula.php
	//@paramaters : a Gregorian Date
	//@returns : an integer representing the corresponding Julian Date (YYJJJ)
    //public static Integer GetDateJulianForGregorian(Date pConvertDate)
    //{   
 	//	integer dateDay = pConvertDate.day();
 	//	integer dateMonth = pConvertDate.month();
 	//	integer dateYear = pConvertDate.year();
    //     
	//  	return	(dateDay-32075+1461*(dateYear+4800+(dateMonth-14)/12)/4+367*(dateMonth-2-(dateMonth-14)/12*12)/12-3*((dateYear+4900+(dateMonth-14)/12)/100)/4 );
  	//} 
    
	//@author : EnablePath
	//@date : 02/13/2014
	//@description : Based on http://aa.usno.navy.mil/faq/docs/JD_Formula.php
	//@paramaters : an integer representing a Julian Date (YYJJJ)
	//@returns : a date representing the corresponding Gregorian Date
    //public static Date GetDateGregorianForJulian(Integer pConvertDate)
    //{   
    //    integer dateYear,dateMonth,dateDay,x,y;
    //    x = pConvertDate + 68569;
    //    y = 4*x/146097;
    //    x = x-(146097*y+3)/4;
    //    dateYear = 4000*(x+1)/1461001;
    //    x = x-1461*dateYear/4+31;
    //    dateMonth = 80*x/2447;
    //    dateDay = x-2447*dateMonth/80;
    //    x = dateMonth/11;
    //    dateMonth = dateMonth+2-12*x;
    //    dateYear = 100*(y-49)+dateYear+x;
            
	//  	return Date.newInstance(dateYear, dateMonth, dateDay);
  	//} 
    
}