@IsTest
private class UserLicenseControllerTest
{
	static testmethod void testUserLicenseSwap()
    {
        //user_login_settings__c settings = new user_login_settings__c(Name='Default', Logins_Per_Month_Threshold__c=3, License_Count_Notification_Address__c='sswiger@enablepath.com');
        //settings.Name = 'Default';
        //insert settings;
        user_login_settings__c uls = new user_login_settings__c();
        uls.Name = 'Default';
      	uls.HL7_API_Key__c = 'D6H8smtD4AvmbV2SPHdp';  
      	uls.HL7_Endpoint_String__c = 'http://api.notonlydev.com/api/index.php?';
        uls.Logins_Per_Month_Threshold__c = 3;
        uls.License_Count_Notification_Address__c = 'sswiger@enablepath.com';
        insert uls;

        control_center_profile_swaps__c ccps = new control_center_profile_swaps__c();
        ccps.Partner_Profile__c = 'Partner Community User';
        ccps.High_Volume_Profile__c = 'Partner Community Login User';
        ccps.Name = 'Partner';
        insert ccps;
        
        UserLicenseController userLic = new UserLicenseController();
        userLic.processUserLicenses();
    }
}