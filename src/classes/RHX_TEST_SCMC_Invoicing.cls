@isTest(SeeAllData=true)
public class RHX_TEST_SCMC_Invoicing {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM SCMC__Invoicing__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new SCMC__Invoicing__c()
            );
        }
    	Database.upsert(sourceList);
    }
}