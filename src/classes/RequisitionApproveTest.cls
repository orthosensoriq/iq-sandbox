/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class RequisitionApproveTest {
    
    public static TestMethod void testRequisitionApproveTrigger() {
        SCMC__Requisition__c requisition = createTestRequisition();
		SCMC__Requisition_Line_Item__c requisitionLine = createTestRequisitionLine(requisition, false);
        requisition.SCMC__Approval_Status__c = 'Approved';
        update requisition;
    }	
    
    public static SCMC__Item__c createTestItem(Boolean inspect, Boolean serialized, Boolean lotControlled) {
		return createTestItem(inspect, serialized, lotControlled, true);
	}
    
	public static SCMC__Item__c createTestItem(Boolean inspect, Boolean serialized, Boolean lotControlled, Boolean doInsert) {
		SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each', 'EA');
		return createTestItem(inspect, serialized, lotControlled, uom, doInsert);
	}
    
	public static SCMC__Item__c createTestItem(Boolean inspect, Boolean serialized, Boolean lotControlled, SCMC__Unit_of_Measure__c uom, Boolean doInsert) {
		List<SCMC__Item__c>items = createTestItems(1, inspect, serialized,lotControlled, uom, doInsert);
		return items[0];
	}
    
	public static List<SCMC__Item__c> createTestItems(Integer numItem, Boolean inspect, Boolean serialized, Boolean lotControlled, SCMC__Unit_of_Measure__c uom, Boolean doInsert) {
		List<SCMC__Item__c>items = new List<SCMC__Item__c>();
		while (numItem-- > 0) {
		SCMC__Item__c im = new SCMC__Item__c();
		im.name = 'Test Item';
		im.SCMC__Item_Description__c = 'Test part for unit tests';
		im.SCMC__Inspection_Required__c  = inspect;
		im.SCMC__Serial_Number_Control__c = serialized;
		im.SCMC__Lot_Number_Control__c = lotControlled;
		im.SCMC__Stocking_UOM__c = uom.id;
			items.add(im);
		}
		if (doInsert){
			insert items;
		}
		return items;
	}
    
    public static SCMC__Unit_of_Measure__c createUnitofMeasure(String name, String ilsName) {
		SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
		try {
			uom = [select id
					from SCMC__Unit_of_Measure__c
					where name = :name];
		} catch(Exception ex) {
			uom.name =name;
			if (ilsName <> null) {
				uom.SCMC__ILS_Unit_of_Measure__c = ilsName;
				uom.SCMC__Valid_for_ILS__c = true;
			}
			insert uom;
		}
		return uom;
	}
    
    public static SCMC__Requisition__c createTestRequisition() {
		// Create the Address, ICP and Warehouse
		SCMC__Address__c address = createTestAddress();
		SCMC__ICP__c icp = createTestICP(address);
		SCMC__Warehouse__c warehouse = createTestWarehouse(icp, address);
		return createTestRequisition(warehouse.Id, true);
	}
    
    public static SCMC__Address__c createTestAddress() {
		SCMC__Address__c address = new SCMC__Address__c();
		address.name = 'Test Address';
		address.SCMC__City__c = 'A City';
		address.SCMC__Country__c = 'Country';
		address.SCMC__Line1__c = 'Address line 1';
		address.SCMC__PostalCode__c = 'Postcd';
		address.SCMC__State__c = 'State';
		insert address;
		return address;
	}
    
    public static SCMC__ICP__c createTestICP(SCMC__Address__c address) { 
        return createTestICP(address, true); 
    }
    
    public static SCMC__ICP__c createTestICP(SCMC__Address__c address, boolean doInsert) { 
        return createTestICP(address, createTestCurrency(), doInsert); 
    }
	
    public static SCMC__ICP__c createTestICP(SCMC__Address__c address, SCMC__Currency_Master__c curr, boolean doInsert) {
		SCMC__ICP__c icp = new SCMC__ICP__c();
		icp.SCMC__Address__c = address.id;
		icp.name = 'Test ICP';
		icp.SCMC__Currency__c = curr.id;
		icp.SCMC__ILS_User__c = 'xxxxxxx';
		icp.SCMC__ILS_Password__c = 'yyyyyy';
		if (doInsert) insert icp;
		return icp;
	}
    
    public static Map<Id, SCMC__Currency_Master__c> getCurrencyRecordsByName(String currencyName) {
		Map<Id, SCMC__Currency_Master__c> outputMap = new Map<Id, SCMC__Currency_Master__c>();
        if (currencyName == null || currencyName.length() == 0) {
			return outputMap;
		}
		for (SCMC__Currency_Master__c cm : getCurrencyRecordMap().values()) {
			if (currencyName.equalsIgnoreCase(cm.Name)) {
				outputMap.put(cm.Id, cm);
			}
		}
		return outputMap;
	}
    
    public static Map<Id, SCMC__Currency_Master__c> getCurrencyRecordMap() {
		Map<Id, SCMC__Currency_Master__c> currencyMasterMap = new Map<Id, SCMC__Currency_Master__c>([
				select 
					Id, 
					Name, 
					SCMC__Active__c, 
					SCMC__Corporate_Currency__c, 
					SCMC__Curr_Conversion_Rate__c, 
					SCMC__Number_of_decimals__c 
				from SCMC__Currency_Master__c 
				limit 1000]);
		return currencyMasterMap;
	}
    
    public static SCMC__Currency_Master__c createTestCurrency() { 
        return createTestCurrency('USD', 1.0, false); 
    }
    
	public static SCMC__Currency_Master__c createTestCurrency(boolean isCorporateCurrency) { 
        return createTestCurrency('USD', 1.0, isCorporateCurrency); 
    }
    
	public static SCMC__Currency_Master__c createTestCurrency(string currName, decimal rate) { 
        return createTestCurrency(currName, rate, false); 
    }
    
	public static SCMC__Currency_Master__c createTestCurrency(string currName, decimal rate, boolean isCorporateCurrency) {
		Map<Id, SCMC__Currency_Master__c> currencyMasterMap = getCurrencyRecordsByName(currName);
		SCMC__Currency_Master__c curr = null;
		if (currencyMasterMap.size() == 0) {
			curr = new SCMC__Currency_Master__c();
			curr.name = currName;
			curr.SCMC__Active__c = true;
			curr.SCMC__Curr_Conversion_Rate__c = rate;
			curr.SCMC__Corporate_Currency__c = isCorporateCurrency;
			insert curr;
		} else {
			curr = currencyMasterMap.values()[0];
		}
		return curr;
	}
    
    public static SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address) { 
        return createTestWarehouse(icp, address, true); 
    
    }
    
    public static SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address, boolean doInsert){
		SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
		warehouse.SCMC__ICP__c = icp.id;
		warehouse.SCMC__Address__c = address.id;
		warehouse.name = 'Test Warehouse';
		warehouse.SCMC__Active__c = True;
		if (doInsert) insert warehouse;
		return warehouse;
	}
    
    public static SCMC__Requisition__c createTestRequisition(Id warehouseId, Boolean doInsert) {
        // create the Requisition
		SCMC__Requisition__c requisition = new SCMC__Requisition__c();
		requisition.SCMC__Warehouse__c = warehouseId;
		if (doInsert) {
			insert requisition;
		}
		return requisition;
	}
    
    public static SCMC__Requisition_Line_Item__c createTestRequisitionLine(SCMC__Requisition__c requisition, Boolean doInsert) {
		// create the Item
		SCMC__Item__c item = createTestItem(true, true, true);

		// create the Requisition Line Item
		return createTestRequisitionLine(requisition.Id, item.Id, 'Open', 5, doInsert);
	}

	public static SCMC__Requisition_Line_Item__c createTestRequisitionLine(Id requisitionId, Id itemId, String status, Decimal qty, Boolean doInsert){
		// create the Requisition Line Item
		SCMC__Requisition_Line_Item__c requisitionLineItem = new SCMC__Requisition_Line_Item__c();
		requisitionLineItem.SCMC__Requisition__c = requisitionId;
		requisitionLineItem.SCMC__Line_Status__c = status;
		requisitionLineItem.SCMC__Item_Master__c = itemId;
		requisitionLineItem.RecordTypeId = getRecordType('Item', requisitionLineItem);
		RequisitionLineItem.SCMC__Quantity__c = qty;
		if (doInsert) {
			insert requisitionLineItem;
		}
		return requisitionLineItem;
	}
    
    /**
	 * determine the record type id for a specific record type
	 * for a specific object
	 */
	public static id getRecordType(String name, SObject userObject){
		ID retId = null;
        Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();
		// Get the sObject describe result for the userObject
		Schema.DescribeSObjectResult r = userObject.getSObjectType().getDescribe();
		String objName = r.getName();
		List<RecordType> info = rTypes.get(objName);
		if (info == null){
			Map<ID, Schema.RecordTypeInfo> rInfo = r.getRecordTypeInfosByID();
			info = [select id
					,name
					,developerName
				from RecordType
				where id in :rInfo.keySet()];
			rTypes.put(objName, info);
		}
		for (RecordType rType : rTypes.get(objName)){
			if (rType.developerName == name){
				retId = rType.id;
				break;
			}
		}
		return retId;
    }
}