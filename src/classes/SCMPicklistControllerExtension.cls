public with sharing class SCMPicklistControllerExtension {
    
    private SCMC.PicklistController controller = null;
	private ApexPages.StandardController stdController;
    
    public SCMC__Picklist__c picklistExtended { get; private set; }
    public string formType { get; private set; }

    public string Carrier { get; private set; }
    public string CarrierService { get; private set; }
    public string CarrierAccount { get; private set; }
    public string FreightPayee { get; private set; }
    public boolean displayBooelanSignatures { get; set; }

    public SCMPicklistControllerExtension(ApexPages.StandardController stdController){
       this.stdController = stdController;
    }

    public SCMPicklistControllerExtension(SCMC.PicklistController baseController){
        this.controller = baseController;
    }
	
	public PageReference goback() {
	  	return stdController.view().setRedirect(true);
  	}

    public PageReference PrintTransferRequest(){
    	PageReference retPage = null;
        list<SCMC__Pick_list_Detail__c> picklistDetails = [select Id, SCMC__Picklist__c
                                                                   ,SCMC__Transfer_Request_Line__c
                                                               from SCMC__Pick_list_Detail__c
                                                              where SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__c = :stdController.getID()
                                                                    limit 1];
    	if(picklistDetails.size() > 0){
	    	retPage = Page.SCMPackslipTransferPDF;	
			retPage.getParameters().put('id', picklistDetails[0].SCMC__Picklist__c);
			retPage.setRedirect(true);		
			return retPage;
		} else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Could not locate a shipment record');
		}

		return null;
    }

    public string picklistExtendId { 
        get;
        set {
            picklistExtendId = value;
            this.Init(picklistExtendId);
        }
    }
	
	private void Init(string picklistId){
		this.picklistExtended = [select Id
									   ,SCMC__Transfer_Request__c
									   ,SCMC__Transfer_Request__r.SCMC__Notes__c
									   ,SCMC__Transfer_Request__r.SCMC__Carrier__c
									   ,SCMC__Transfer_Request__r.SCMC__Carrier_Service__c
									   ,SCMC__Transfer_Request__r.Name
									   ,SCMC__Transfer_Request__r.SCMC__Destination_Warehouse__r.Name
									   ,SCMC__Sales_Order__c
									   ,SCMC__Sales_Order__r.Carrier__c
									   ,SCMC__Sales_Order__r.Carrier_Account__c
									   ,SCMC__Sales_Order__r.Carrier_Service__c
									   ,SCMC__Sales_Order__r.Freight_Payee__c
		                           from SCMC__Picklist__c
		                          where Id = :picklistId];
		
		if(this.picklistExtended.SCMC__Transfer_Request__c != null){
			this.formType = 'TransferRequest';
			this.Carrier = this.picklistExtended.SCMC__Transfer_Request__r.SCMC__Carrier__c;
			this.CarrierService = this.picklistExtended.SCMC__Transfer_Request__r.SCMC__Carrier_Service__c;
			this.CarrierAccount = '';
			this.FreightPayee = '';
		} else {
			this.formType = 'SalesOrder';
			this.Carrier = this.picklistExtended.SCMC__Sales_Order__r.Carrier__c;
			this.CarrierService = this.picklistExtended.SCMC__Sales_Order__r.Carrier_Service__c;
			this.CarrierAccount = this.picklistExtended.SCMC__Sales_Order__r.Carrier_Account__c;
			this.FreightPayee = this.picklistExtended.SCMC__Sales_Order__r.Freight_Payee__c;
		}


        list<SCMC__Pick_list_Detail__c> picklistDetails = [select Id
                                                                   ,SCMC__Transfer_Request_Line__c
                                                               from SCMC__Pick_list_Detail__c
                                                              where SCMC__Picklist__c = :picklistId
                                                                and SCMC__Transfer_Request_Line__c != null
                                                           order by SCMC__Transfer_Request_Line__c];

	}
}