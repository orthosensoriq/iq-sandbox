public without sharing class SurgeonViewExtension {

	public final Account currSurgeon;
    public String[] selectedHospitals = new String[]{};
    public List<Account> currHospitals {get; set;}
    public boolean renderHospitalSelect {get; set;}
    public boolean renderEditView {get; set;}
    public Id deletedHospital {get; set;}
    Id personContactId;
    list<Surgeon_Hospital__c> InsertSurgeonHospital = new list<Surgeon_Hospital__c>();
    list<Surgeon_Hospital__c> DeleteSurgeonHospital = new list<Surgeon_Hospital__c>();

    // The extension constructor initializes the private member
    // variable Account by using the getRecord method from the standard
    // controller.
    public SurgeonViewExtension(ApexPages.StandardController stdController) {
        this.currSurgeon = (Account)stdController.getRecord();
        personContactId = currSurgeon.PersonContactId;
        renderEditView = false;
        renderHospitalSelect = false;
    }

    public void LoadHospitals()
    { 
        getRelatedHospitals();         
    }

    public void getRelatedHospitals() {
        currHospitals = [SELECT Id, Name, Hospital_Type__c, ShippingCity, ShippingState FROM Account WHERE RecordType.Name = 'Hospital Account' AND Id IN (SELECT Hospital__c FROM Surgeon_Hospital__c WHERE Surgeon__c = :personContactId) ORDER BY Name];
        for (Account a : currHospitals) {
            selectedHospitals.add(a.id);
        }
    } 

    public List<selectOption> getitems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE RecordType.Name = 'Hospital Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
     }
     
    public String[] getHospitals() {
        return selectedHospitals;
    }
            
    public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }

    public PageReference saveValues() {
        InsertSurgeonHospital.clear();
        for (Integer i=0; i < selectedHospitals.size(); i++) {
            Surgeon_Hospital__c newSurgeonHospital = new Surgeon_Hospital__c();
            newSurgeonHospital.Hospital__c = selectedHospitals[i];
            newSurgeonHospital.Surgeon__c = personContactId;
            InsertSurgeonHospital.add(newSurgeonHospital);
        }
        if (InsertSurgeonHospital.size() > 0) upsert InsertSurgeonHospital;
        PageReference varPage = Page.SurgeonView;
        varPage.getParameters().put('id',currSurgeon.Id);
        varPage.SetRedirect(TRUE);
        return varPage;
    }

    public PageReference deleteHospital() {
        DeleteSurgeonHospital.clear();
        for (Surgeon_Hospital__c delSurgeonHospital : [SELECT Id, Surgeon__c, Hospital__c FROM Surgeon_Hospital__c WHERE Hospital__c = :deletedHospital AND Surgeon__c = :personContactId]) {
            DeleteSurgeonHospital.add(delSurgeonHospital);
        }
        system.debug('delete the following Surgeon_Hospital__c =====> '+DeleteSurgeonHospital);

        if (DeleteSurgeonHospital.size() > 0) delete DeleteSurgeonHospital;
        PageReference varPage = Page.SurgeonView;
        varPage.getParameters().put('id',currSurgeon.Id);
        varPage.SetRedirect(TRUE);
        return varPage;
    }

    public PageReference toggleHospitalSelect() {
        renderHospitalSelect = true;
        return null;
    }

    public PageReference cancelHospitalEdit() {
        renderHospitalSelect = false;
        return null;
    }

    public PageReference toggleEditView() {
        renderEditView = true;
        return null;
    }

    public void Save(){
        renderEditView = false;
        update currSurgeon;
    }

    public PageReference cancelSurgeonEdit() {
        renderEditView = false;
        return null;
    }
}