@isTest
private class ProcedureCreationControllerTest {
	static testMethod void myUnitTest() {
		
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('TestPhysicianJSONResponse');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, mock);
		
        user_login_settings__c uls = new user_login_settings__c();
        uls.Name = 'Default';
      	uls.HL7_API_Key__c = 'D6H8smtD4AvmbV2SPHdp';  
      	uls.HL7_Endpoint_String__c = 'http://api.notonlydev.com/api/index.php?';
        uls.Logins_Per_Month_Threshold__c = 3;
        uls.License_Count_Notification_Address__c = 'test@test.com';
        insert uls;
        
            
		Diagnosis__c diag = new Diagnosis__c();
        diag.Code__c='715.00';
        diag.Long_Name__c = 'Test Diag Code';
        insert diag;
    
		Account a = new Account();
		a.Name = 'TestAccount';
        a.RecordTypeId = [Select Id from RecordType where Name = 'Hospital Account' LIMIT 1].Id;
		insert a;
        
        Account s = new Account();
        s.FirstName = 'Test';
        s.LastName = 'Tester';
        s.RecordTypeId = [Select Id from RecordType where Name = 'Surgeon Account' Limit 1].id;
        s.BillingState = 'GA';
        s.NPI__c = '1234567890';
        insert s;
        
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test';
		c.Email = 'Tester@Tester.com.Test';
        c.accountId = a.id;
		insert c;
        
        //User testUser = new User();
        //testUser.FirstName = 'Testing';
        //testUser.LastName = 'Tester';
        //testUser.Username = 'tester@OrthoSensor.com.test';
        //testUser.Email = 'tester@OrthoSensor.com.test';
        //testUser.Alias = 'tester';
        //testUser.TimeZoneSidKey = 'America/New_York';
        //testUser.LocaleSidKey = 'en_US';
        //testUser.EmailEncodingKey = 'ISO-8859-1';
        //testUser.LanguageLocaleKey = 'en_US';
        //testUser.ProfileId = [Select id from Profile where name = 'Partner Community User' LIMIT 1].id;
        //testUser.ContactId = c.id;
        //insert testUser;
		
        surgeon_hospital__c surghosp = new surgeon_hospital__c();
        surghosp.Hospital__c = a.id;
        surghosp.Surgeon__c = c.id;
        insert surghosp;
        a.IsPartner = true;
        update a;
		
        
		list<Account> testAccts = [SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000];
        system.debug('Hospital Accounts: ' + testAccts.size());
    
        Patient__c testPatient = new Patient__c();
        testPatient.First_Name__c = 'Test';
        testPatient.Last_Name__c = 'Tester';
        testPatient.Gender__c = 'Female';
        testPatient.Date_Of_Birth__c = date.newInstance(1970, 3, 3);
        testPatient.Account_Number__c = '1234567890987654321';
        testPatient.Medical_Record_Number__c = 'MR123456';
        testPatient.active__c = true;
        testPatient.Hospital__c = a.id;
        insert testPatient;
        
        Case_Type__c ctyp = new Case_Type__c();
        ctyp.Description__c = 'Total Knee Arthroplasty';
        insert ctyp;
        
		Event_Type__c evtyp = new Event_Type__c();
		evtyp.Name = 'Procedure';
		evtyp.Primary__c = true;
        evtyp.Active__c = true;
		insert evtyp;

        Event_type__c evtyp2 = new Event_Type__c();
        evtyp2.name = 'followup';
        evtyp2.Primary__c = false;
        evtyp2.Active__c = true;
        evtyp2.Interval__c = 6;
        insert evtyp2;
        
        Case_Type_Event_Type__c ctet = new Case_Type_Event_Type__c();
        ctet.Case_Type__c = ctyp.id;
        ctet.Event_Type__c = evtyp.id;
        insert ctet;
        
        Case_Type_Event_Type__c ctet2 = new Case_Type_Event_Type__c();
        ctet2.Case_Type__c = ctyp.id;
        ctet2.Event_Type__c = evtyp2.id;
        insert ctet2;
        
        survey__c surv = new survey__c();
        surv.Active__c = true;
        surv.Name__c = 'Test Survey';
        surv.Survey_URL__c = 'http://www.test.com/123423awfa.htm';
		insert surv;
        
        event_type_survey__c ets = new event_type_survey__c();
        ets.survey__c = surv.id;
        ets.Event_Type__c = evtyp.id;
        insert ets;

        Clinical_Data__c clinicalData = new Clinical_Data__c();
        clinicalData.Laterality__c = 'Both';
		clinicalData.Height__c = 50;
		clinicalData.Weight__c = 50;
        clinicalData.Height_UOM__c = 'CM';

        ProcedureCreationController procedureCreationController = new ProcedureCreationController();
		procedureCreationController.patient = testPatient;
		procedureCreationController.clinicalData = clinicalData;
		procedureCreationController.surgeonName = a.Id;
		procedureCreationController.newSurgeonFirstName = 'Test';
		procedureCreationController.newSurgeonLastName = 'Testdoc';
		procedureCreationController.newSurgeonNPI = '1234567890';
		procedureCreationController.selectedHospital = a;
        procedureCreationController.searchLastName = 'Tester';
        procedureCreationController.searchState = 'GA';
        procedureCreationController.getSurgeons();
		procedureCreationController.SelectSurgeon();
		procedureCreationController.closePopup();
		procedureCreationController.showPopup();
        procedureCreationController.CalculateBMI();
        procedureCreationController.allPatients = [Select Id, SSN__c, Soundex_Metaphone_Value__c, Medical_Record_Number__c, First_Name__c, Last_Name__c, Language__c, Gender__c, Race__c, Account_Number__c, Date_Of_Birth__c from Patient__c  where Active__c = true LIMIT 20];
		procedureCreationController.createProcedure();
		procedureCreationController.UpdateExistingPatient();
		procedureCreationController.GoToProcedureSummary();
        procedureCreationController.ChangeHeightUOM();
        procedureCreationController.ChangeWeightUOM();
        procedureCreationController.AddSurgeon();
		
		ProcedureCreationController procedureCreationController2 = new ProcedureCreationController();
		procedureCreationController2.getSurgeons();
		procedureCreationController2.patient = testPatient;
		procedureCreationController2.clinicalData = new Clinical_Data__c(Laterality__c = 'Both', Height_UOM__c = 'Inches', Weight__c=30, Height__c=30);
		procedureCreationController2.UpdateExistingPatient();
		procedureCreationController2.surgeonName = '--Add New--';
		procedureCreationController2.SelectSurgeon();	
		Account testsurgeon = [Select Id, NPI__c from Account where NPI__c!=null LIMIT 1];
		procedureCreationController2.newSurgeonNPI = testsurgeon.NPI__c;
		//procedureCreationController.AddSurgeon();
		
		Test.stopTest();
	}
    
    static testmethod void MyUnitTest2()
    {
		Diagnosis__c diag = new Diagnosis__c();
        diag.Code__c='715.00';
        diag.Long_Name__c = 'Test Diag Code';
        insert diag;
    
		Account a = new Account();
		a.Name = 'TestAccount';
        a.RecordTypeId = [Select Id from RecordType where Name = 'Hospital Account' LIMIT 1].Id;
		insert a;
        
        Account s = new Account();
        s.FirstName = 'Test';
        s.LastName = 'Tester';
        s.RecordTypeId = [Select Id from RecordType where Name = 'Surgeon Account' Limit 1].id;
        s.BillingState = 'GA';
        s.NPI__c = '1234567890';
        insert s;
        
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test';
		c.Email = 'Tester@Tester.com.Test';
        c.accountId = a.id;
		insert c;
		
        surgeon_hospital__c surghosp = new surgeon_hospital__c();
        surghosp.Hospital__c = a.id;
        surghosp.Surgeon__c = c.id;
        insert surghosp;
        a.IsPartner = true;
        update a;
		
        ADT_interface__c HL7Rec = new ADT_interface__c();
        HL7Rec.SFDC_Account__c = a.id;
        HL7Rec.Physician_First_Name__c = 'Test';
        HL7Rec.Physician_Last_Name__c = 'Tester';
        HL7Rec.Appointment_start_time__c = '201404031100';
        HL7Rec.Patient_First_Name__c = 'Patient';
        HL7Rec.Patient_Last_Name__c = 'Testing';
        HL7Rec.Language_Name__c = 'English';
        HL7Rec.Medical_Record_Number__c = 'MR123456';
		HL7Rec.Account_Number__c = '1234567890';
		HL7Rec.Patient_DOB__c = '19700201';
		HL7Rec.Patient_SSN__c = '000-00-0000';
		HL7Rec.Patient_Gender__c = 'Male';
		HL7Rec.Race_Name__c = 'Eskimo';
		HL7Rec.Laterality__c = 'both';
        HL7Rec.Allergen_Description_1__c = 'Pollen';
        HL7Rec.Allergen_Description_2__c = 'Dust';
        HL7Rec.Allergen_Description_3__c = 'Animals';
        HL7Rec.Allergen_Description_4__c = 'Morphine';
        HL7Rec.Allergen_Description_5__c = 'Children';
        HL7Rec.Allergen_Description_6__c = 'Snakes';
        HL7Rec.Allergen_Description_7__c = 'Sharks';
        HL7Rec.Allergen_Description_8__c = 'Opiates';
        HL7Rec.Allergen_Description_9__c = 'Gluten';
		HL7Rec.Anesthesias_Description__c = 'Something Heavy';
		HL7Rec.Height_Units__c = '120';
        HL7Rec.Height_Units__c = 'cm';
		HL7Rec.Weight_Units__c = '67';
        HL7Rec.weight_Units__c = 'kg';
        HL7Rec.Patient_Consent__c = 'true';
        HL7Rec.Diagnosis_Code_1__c = '715.00';
        HL7Rec.Diagnosis_Code_2__c = '715.00';

        insert HL7Rec;
        
        ApexPages.currentPage().getParameters().put('intid',HL7Rec.Id);
		ProcedureCreationController pcc = new ProcedureCreationController();
		pcc.CheckForAutoCreation();
  		pcc.createProcedure();
		pcc.UpdateExistingPatient();
        
    }
    
    static testmethod void MyUnitTest3()
    {
		Diagnosis__c diag = new Diagnosis__c();
        diag.Code__c='715.00';
        diag.Long_Name__c = 'Test Diag Code';
        insert diag;
    
		Account a = new Account();
		a.Name = 'TestAccount';
        a.RecordTypeId = [Select Id from RecordType where Name = 'Hospital Account' LIMIT 1].Id;
		insert a;
        
        Account s = new Account();
        s.FirstName = 'Test';
        s.LastName = 'Tester';
        s.RecordTypeId = [Select Id from RecordType where Name = 'Surgeon Account' Limit 1].id;
        s.BillingState = 'GA';
        s.NPI__c = '1234567890';
        insert s;
        
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test';
		c.Email = 'Tester@Tester.com.Test';
        c.accountId = a.id;
		insert c;
		
        surgeon_hospital__c surghosp = new surgeon_hospital__c();
        surghosp.Hospital__c = a.id;
        surghosp.Surgeon__c = c.id;
        insert surghosp;
        a.IsPartner = true;
        update a;
		
        ADT_interface__c HL7Rec = new ADT_interface__c();
        HL7Rec.SFDC_Account__c = a.id;
        HL7Rec.Physician_First_Name__c = 'Test';
        HL7Rec.Physician_Last_Name__c = 'Tester';
        HL7Rec.Appointment_start_time__c = '201404031100';
        HL7Rec.Patient_First_Name__c = 'Patient';
        HL7Rec.Patient_Last_Name__c = 'Testing';
        HL7Rec.Language_Name__c = 'English';
        HL7Rec.Medical_Record_Number__c = 'MR123456';
		HL7Rec.Account_Number__c = '1234567890';
		HL7Rec.Patient_DOB__c = '19700201';
		HL7Rec.Patient_SSN__c = '000-00-0000';
		HL7Rec.Patient_Gender__c = 'Male';
		HL7Rec.Race_Name__c = 'Eskimo';
		HL7Rec.Laterality__c = 'both';
        HL7Rec.Allergen_Description_1__c = 'Pollen';
        HL7Rec.Allergen_Description_2__c = 'Dust';
        HL7Rec.Allergen_Description_3__c = 'Animals';
        HL7Rec.Allergen_Description_4__c = 'Morphine';
        HL7Rec.Allergen_Description_5__c = 'Children';
        HL7Rec.Allergen_Description_6__c = 'Snakes';
        HL7Rec.Allergen_Description_7__c = 'Sharks';
        HL7Rec.Allergen_Description_8__c = 'Opiates';
        HL7Rec.Allergen_Description_9__c = 'Gluten';
		HL7Rec.Anesthesias_Description__c = 'Something Heavy';
		HL7Rec.Height_Units__c = '65';
        HL7Rec.Height_Units__c = 'in';
		HL7Rec.Weight_Units__c = '155';
        HL7Rec.weight_Units__c = 'lbs';
        HL7Rec.Patient_Consent__c = 'true';
        HL7Rec.Diagnosis_Code_1__c = '715.00';
        HL7Rec.Diagnosis_Code_2__c = '715.00';

        insert HL7Rec;
        
        ApexPages.currentPage().getParameters().put('intid',HL7Rec.Id);
		ProcedureCreationController pcc = new ProcedureCreationController();
		pcc.CheckForAutoCreation();
  		pcc.createProcedure();
		pcc.UpdateExistingPatient();
        
    }
}