public with sharing class SCMPackslipControllerExtension {
    private SCMC.PackslipController controller = null;
    private list<SCMC__Pick_list_Detail__c> picklistDetails = null;
    public SCMC__Picklist__c picklistExtended { get; private set; }
    public string Carrier { get; private set; }
    public string CarrierService { get; private set; }
    public string CarrierAccount { get; private set; }
  public string formType { get; private set; } 
    public string FreightPayee { get; private set; }
    public list<SCMPickListDetailExtended> scmDetailExtended {get; private set;}
    public decimal totalItemsShipped { get; private set; }
    public decimal PackSlipTotalQtyOrder { get; private set; }
    public SCMPackslipControllerExtension(SCMC.PackslipController baseController){
        this.controller = baseController;
    }
    
    public string picklistExtendId { 
        get;
        set {
            picklistExtendId = value;
            this.Init(picklistExtendId);
        }
    }
    
    private void Init(string picklistId){

        this.picklistExtended = [select Id
                  ,SCMC__Transfer_Request__c
                  ,SCMC__Transfer_Request__r.SCMC__Notes__c
                  ,SCMC__Transfer_Request__r.SCMC__Carrier__c
                  ,SCMC__Transfer_Request__r.SCMC__Carrier_Service__c
                  ,SCMC__Transfer_Request__r.Name
                  ,SCMC__Transfer_Request__r.SCMC__Destination_Warehouse__r.Name
                  ,SCMC__Sales_Order__c
                  ,SCMC__Sales_Order__r.Carrier__c
                  ,SCMC__Sales_Order__r.Carrier_Account__c
                  ,SCMC__Sales_Order__r.Carrier_Service__c
                  ,SCMC__Sales_Order__r.Freight_Payee__c
                  ,SCMC__Shipper__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.Name
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Line1__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Line2__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__City__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__State__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__PostalCode__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Country__c
                  ,SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Phone                     
                                   from SCMC__Picklist__c
                                  where Id = :picklistId];

        system.debug('+++++ SCMC__Shipper__c: ' + this.picklistExtended.SCMC__Shipper__c);                 

    if(this.picklistExtended.SCMC__Transfer_Request__c != null){
      this.formType = 'TransferRequest';
      this.Carrier = this.picklistExtended.SCMC__Transfer_Request__r.SCMC__Carrier__c;
      this.CarrierService = this.picklistExtended.SCMC__Transfer_Request__r.SCMC__Carrier_Service__c;
      this.CarrierAccount = '';
      this.FreightPayee = '';
    } else {
      this.formType = 'SalesOrder';
      this.Carrier = this.picklistExtended.SCMC__Sales_Order__r.Carrier__c;
      this.CarrierService = this.picklistExtended.SCMC__Sales_Order__r.Carrier_Service__c;
      this.CarrierAccount = this.picklistExtended.SCMC__Sales_Order__r.Carrier_Account__c;
      this.FreightPayee = this.picklistExtended.SCMC__Sales_Order__r.Freight_Payee__c;
    }
        
        //Invoice Line Detail Code
        picklistDetails = [select Id
                                 ,SCMC__Item__c
                                 ,SCMC__Item__r.SCMC__Kit__c
                                 ,SCMC__Item__r.SCMC__Stocking_UOM__r.Name
                                 ,SCMC__Item__r.Name
                                 ,SCMC__Item__r.SCMC__Serial_Number_Control__c
                                 ,SCMC__Item__r.SCMC__Lot_Number_Control__c
                                 ,SCMC__Item__r.SCMC__Shelf_Life_Control__c
                                 ,SCMC__Item__r.SCMC__Inbound_Outbound_Serial_Number__c
                                 ,SCMC__Item__r.SCMC__Item_Description__c
                                 ,SCMC__Condition_Code__c
                                 ,SCMC__Condition_Code__r.Name
                                 ,SCMC__Lot_Number__c
                                 ,SCMC__Quantity__c
                                 ,SCMC__Serial_Number__c
                                 ,SCMC__Sales_Order_Line_Item__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Line_Number__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Quantity__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Quantity_Allocated__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Quantity_Shipped__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Price__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Kit__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Serial_Number_Control__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Inbound_Outbound_Serial_Number__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.Name
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.Id
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Item_Description__c
                                 ,SCMC__Sales_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Stocking_UOM__r.Name
                                 ,SCMC__Warehouse__c
                                 ,SCMC__Warehouse__r.Name
                                 ,SCMC__Shelf_Life_Expiration__c
                                 ,SCMC__Transfer_Request_Line__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Quantity__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.Name
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Manager__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__State__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__PostalCode__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Line2__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Line1__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Country__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__City__c
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.Name
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__c                 
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Fax
                                 ,SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Phone                                 
                               from SCMC__Pick_list_Detail__c
                              where SCMC__Picklist__c = :picklistId
                             order by SCMC__Sales_Order_Line_Item__r.SCMC__Line_Number__c, SCMC__Item__r.Name];

                set<Id> IdSet = new set<Id>();
                set<string> keyId = new set<string>();
                this.totalItemsShipped = 0;
                this.PackSlipTotalQtyOrder = 0;
                string lastKey = null;
                string lastLotNumber = '';
                SCMC__Pick_list_Detail__c lastPicklistDetail = null;
                list<SCMC__Pick_list_Detail__c> tempPicklistDetails = new list<SCMC__Pick_list_Detail__c>();

                map<string, set<string>> lotNumberMap = new map<string, set<string>>();

                for(SCMC__Pick_list_Detail__c pdLine : this.picklistDetails){
                    string key = '';
                    if(pdLine.SCMC__Sales_Order_Line_Item__c != null){
                        key = pdLine.SCMC__Sales_Order_Line_Item__c;
                        IdSet.add(pdLine.SCMC__Sales_Order_Line_Item__c);
                    system.debug('+++++ SCMC__Sales_Order_Line_Item__c: ' + pdLine.SCMC__Sales_Order_Line_Item__c);                 
                    } else if(pdLine.SCMC__Transfer_Request_Line__c != null){
                        key = pdLine.SCMC__Transfer_Request_Line__c;
                        IdSet.add(pdLine.SCMC__Transfer_Request_Line__c);
                    system.debug('+++++ SCMC__Transfer_Request_Line__c: ' + pdLine.SCMC__Transfer_Request_Line__c);                 
                    }

                    if(pdLine.SCMC__Item__r.SCMC__Lot_Number_Control__c && 
                       pdLine.SCMC__Lot_Number__c != null && 
                       pdLine.SCMC__Lot_Number__c != ''){
                        if(lotNumberMap.containsKey(key)){
                            set<string> getLots = lotNumberMap.get(key);
                            getLots.add(pdLine.SCMC__Lot_Number__c);
                        } else {
                            set<string> newLots = new set<string>();
                            newLots.add(pdLine.SCMC__Lot_Number__c);
                            lotNumberMap.put(key, newLots);
                        }
                    }

                    if(pdLine.SCMC__Item__r.SCMC__Serial_Number_Control__c || lastKey != key) {
                        
                        if(lastPicklistDetail != null){
                            tempPicklistDetails.add(lastPicklistDetail);
                        }

                        lastPicklistDetail = pdLine;
                        lastKey = key;
                        
                    } else {

                        lastPicklistDetail.SCMC__Quantity__c += pdLine.SCMC__Quantity__c;
                    
                    }

                    this.totalItemsShipped += pdLine.SCMC__Quantity__c;
                    if(pdLine.SCMC__Transfer_Request_Line__r.SCMC__Quantity__c != null){
                        this.PackSlipTotalQtyOrder += pdLine.SCMC__Transfer_Request_Line__r.SCMC__Quantity__c;
                    }
                }

                //Get Last Record       
                if(lastPicklistDetail != null){
                    tempPicklistDetails.add(lastPicklistDetail);
                }

                system.debug('+++++ IdSet: ' + IdSet.size());                 
                //mapSerialNumberRecords
                map<string, list<SCMC__Serial_Number__c>> serialNumbersToSolIdMap = new map<string, list<SCMC__Serial_Number__c>>();
                if(IdSet.size() > 0 &&
                   this.picklistExtended.SCMC__Shipper__c != null){
                    for(SCMC__Serial_Number__c sn : [select Id
                                                     ,SCMC__Sales_Order_Line_Item__c
                                                     ,SCMC__Serial_Number__c
                                                     ,Lot_Number__c
                                                     ,SCMC__Item__c
                                                     ,SCMC__Item__r.SCMC__Shelf_Life_Control__c
                                                     ,SCMC__Item__r.SCMC__Lot_Number_Control__c
                                                     ,SCMC__Shelf_Life_Expiration__c
                                                     ,SCMC__Transfer_Request_Line__c
                                                     ,SCMC__Last_Transfer_Request_Line__c
                                                 from SCMC__Serial_Number__c
                                                where SCMC__Shipping__c = :this.picklistExtended.SCMC__Shipper__c
                                         order by SCMC__Serial_Number__c]){
                        
                        string key = '';
                        if(sn.SCMC__Sales_Order_Line_Item__c != null){
                             key = sn.SCMC__Sales_Order_Line_Item__c;
                        } else if(sn.SCMC__Transfer_Request_Line__c != null) {
                             key = sn.SCMC__Transfer_Request_Line__c;
                        } else if(sn.SCMC__Last_Transfer_Request_Line__c != null) {
                            key = sn.SCMC__Last_Transfer_Request_Line__c;
                        }

                        if(serialNumbersToSolIdMap.containsKey(key)){
                            list<SCMC__Serial_Number__c> getList = serialNumbersToSolIdMap.get(key);
                            getList.add(sn);
                        } else {
                            list<SCMC__Serial_Number__c> newList = new list<SCMC__Serial_Number__c>();
                            newList.add(sn);
                            serialNumbersToSolIdMap.put(key, newList);
                        }       
                    }
                }               
                
                //mapPicklistDetail
                integer x = 1;
                keyId.clear();
                this.scmDetailExtended = new list<SCMPickListDetailExtended>();
                for(SCMC__Pick_list_Detail__c pickDetailItem : tempPicklistDetails){

                    string key = '';
                    if(pickDetailItem.SCMC__Sales_Order_Line_Item__c != null){
                        key = pickDetailItem.SCMC__Sales_Order_Line_Item__c;
                    } else if(pickDetailItem.SCMC__Transfer_Request_Line__c != null){
                        key = pickDetailItem.SCMC__Transfer_Request_Line__c;
                    }

                    list<SCMC__Serial_Number__c> serialNumberList = new list<SCMC__Serial_Number__c>();
                    if(key != '' && serialNumbersToSolIdMap.containsKey(key)){
                        serialNumberList = serialNumbersToSolIdMap.get(key);
                    }

                    set<string> lotnumberList = new set<string>();
                    if(key != '' && lotNumberMap.containsKey(key)){
                        lotnumberList = lotNumberMap.get(key);
                    }

                    if(!keyId.contains(key)){
                        list<SCMC__Pick_list_Detail__c> BOMList = new list<SCMC__Pick_list_Detail__c>();
                        this.scmDetailExtended.add(new SCMPickListDetailExtended(x, pickDetailItem, serialNumberList, lotnumberList));
                        x++;
                        keyId.add(key);
                    }

                }
                    

    }

  public String getCompanyAddress(){
    SCMC__Pick_list_Detail__c detail = this.picklistDetails[0];   
    return SCM_Utilities.getFormattedAddress(detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.Name
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Line1__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Line2__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__City__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__State__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__PostalCode__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Address__r.SCMC__Country__c
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Phone
                        , detail.SCMC__Transfer_Request_Line__r.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Fax
                        ,'<br />');                       
  }

  public String getReceivingAddress(){
    return SCM_Utilities.getFormattedAddress(this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.Name
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Line1__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Line2__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__City__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__State__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__PostalCode__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__Address__r.SCMC__Country__c
                        , this.picklistExtended.SCMC__Shipper__r.SCMC__Receiving_Warehouse__r.SCMC__ICP__r.SCMC__Manager__r.Phone
                        , null
                        ,'<br />');
  }
  
}