@isTest(SeeAllData=true)
public class IQ_DashboardDataControllerTest {

    static IQ_DashboardDataController setup() {
		IQ_DashboardDataController ctrl = new IQ_DashboardDataController();
        return ctrl;
    }

    @isTest static void GetSensorDataLinesTest() {
         IQ_DashboardDataController ctrl = setup();
         ctrl.GetSensorDataLines();    
    }

    @isTest static void AggregateMonthKneeBalanceDataTest() {
        IQ_DashboardDataController ctrl = setup();
        ctrl.AggregateMonthKneeBalanceData();
    }

    @isTest static void AggregateMonthKneeBalanceData2Test() {
        IQ_DashboardDataController ctrl = setup();
	    ctrl.AggregateMonthKneeBalanceData2();
    }
	
	@isTest static void AggregateKneeBalanceDataTest() {
        IQ_DashboardDataController ctrl = setup();
	    // ctrl.AggregateKneeBalanceData();
    }

    @isTest static void AggregatePromDeltasTest() {
        IQ_DashboardDataController ctrl = setup();
	 	ctrl.AggregatePromDeltas();
    }
    @isTest static void patientPhaseAggregationTest() {
        IQ_DashboardDataController ctrl = setup();
	 	ctrl.patientPhaseAggregation();
    }

}