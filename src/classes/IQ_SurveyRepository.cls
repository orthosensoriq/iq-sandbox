global without sharing class IQ_SurveyRepository {

  private IQ_PatientActions ctrl;
  public IQ_SurveyRepository(IQ_PatientActions controllerParam) {
    ctrl = controllerParam;
  }

  @RemoteAction
  global static string GetCaseTypeIdByCaseId(string caseId) {
    List<Case__c> caseRec = [Select Case_Type__r.Id from Case__c where Id = :caseId];
    System.debug('Number of cases: ' + caseRec.size());
        //if (caseRec.size() == 0) {
        //    return '';
        //} else {
        //
        System.debug('Case Type Id: ' + caseRec[0].Case_Type__r.Id);
        Case__c caseRec1 = caseRec[0];
        return caseRec1.Case_Type__r.Id;
    	//}
    }
    
    @RemoteAction
    global static List<Survey__c> LoadSurveysFromCaseType(string caseId){
      string caseTypeId = GetCaseTypeIdByCaseId(caseId);
      System.debug('Inside load method: ' + caseTypeId);

      //get case type event type from case Type - case type event type is an object handling the relationship
      //between event and case that can be used to get related surveys 
      List<Event_Type__c> ids = GetEventTypesByCaseTypeId(caseTypeId); 
      System.debug('Event types:' + ids.size());
      if (ids.size() > 0) {
        List<Survey__c> surveys = GetSurveysByEventTypes(ids, true);
        System.debug('Surveys:' + surveys.size());
        return surveys;
        } else {
          return new List<Survey__c>();
        }
      }

      @RemoteAction
      public static List<Survey__c> GetSurveysByEventTypes(List<Event_Type__c> ids, boolean excludeProcedureSurveys) {
        System.debug(ids);

        List<Event_Type_Survey__c> eventTypeSurveys = [Select Survey__r.Id, 
        Survey__r.Name__c, 
        Survey__r.Active__c, 
        Survey__r.SafeId__c, 
        Survey__r.Survey_Url__c, 
        Survey__r.Version__c,
        Survey__r.Can_Graph__c,
        Event_Type__r.Name
        from Event_Type_Survey__c 
        Where Event_Type__c = :ids and Survey__r.Active__c = true];
        System.debug(eventTypeSurveys.size());
        System.debug(eventTypeSurveys);
        List<Survey__c> surveys = new List<Survey__c>();
        Set<string> surveyNames =  new Set<string>(); 
        for(Event_Type_Survey__c evt: eventTypeSurveys) {
          if ((excludeProcedureSurveys == true) && (evt.Event_Type__r.Name == 'Procedure')) {
           //we don't want surveys to be listed for Procedures
          } else {
            //remove duplicates
            if( !SurveyNames.contains(evt.Survey__r.Name__c)) {
              Survey__c s = new Survey__c();
              s.Id  = evt.Survey__r.Id;
              s.Name__c = evt.Survey__r.Name__c;
              s.Active__c = evt.Survey__r.Active__c;
              s.Survey_Url__c = evt.Survey__r.Survey_Url__c;
              s.Can_graph__c = evt.Survey__r.Can_graph__c;
              s.Version__c = evt.Survey__r.Version__c;
              //s.SafeId__c = evt.Survey__r.SafeId__c;
              surveys.add(s);
              surveyNames.add(evt.Survey__r.Name__c);
            }
          }         
        }
        System.debug(surveys);
        return surveys;
      }

      public static List<Event_Type__c> GetEventTypesByCaseTypeId(string caseTypeId) {
        List<Case_Type_Event_Type__c> eventTypes = [Select Id, Name, Event_Type__r.Id 
        from Case_Type_Event_Type__c
        Where Case_Type__r.Id = :caseTypeId
        ];
        //System.debug('Case Event Types count: ' + eventTypes.size());
        System.debug('Case Event Types: ' + eventTypes);
        List<Event_Type__c> ids = new List<Event_Type__c>(); 
        if (eventTypes.size() > 0) {
          for(Case_Type_Event_Type__c eventType: eventTypes) {
            if (eventType.Event_Type__r.id != null) {
              Event_Type__c e1 = new Event_Type__c();
              System.debug('Event Type id:' + eventType.Event_Type__r.Id);
              e1.Id = eventType.Event_Type__r.Id;
              ids.add(e1);
           }
          }
        }
        System.debug('Ids: ' + string.valueOf(ids.size()));
        return ids;
      }

    @RemoteAction
    global static string GetEventTypeIdByEventId(string eventId) {
        List<Event__c> eventCase = [Select Id, Name, Event_Type__r.Id 
        from Event__c
        Where Event__c.Id = :eventId];
        if (eventCase.size() > 0) {
          return eventCase[0].Event_Type__r.Id;
        } else {
          return '';
        }
    }

    @RemoteAction
    global static List<Event_Type_Survey__c> GetSurveyEventTypes(string id) {
      List<Event_Type_Survey__c> eventTypes = [SELECT Id, Name, Event_Type__r.Name, Survey__r.Name__c
                                              From Event_Type_Survey__c Where Survey__r.Id = :id 
                                              Order By Event_Type__r.Interval__c];
      List<Event_Type_Survey__c> distinctList = new List<Event_Type_Survey__c> ();
      //System.debug(eventTypes.size());
      //System.debug(eventTypes[0].Event_Type__r.Name);
      for(Event_Type_Survey__c survey : eventTypes) {
        boolean tFlag = false;
        for(Event_Type_Survey__c newList: distinctList) {
          if (survey.Event_Type__r.Name == newList.Event_Type__r.Name) {
            tFlag = true;
          }
        }
        if (!tFlag) {
            distinctList.add(survey);
            //System.debug(survey.Event_Type__r.Name);
            //System.debug(distinctList.size());
        }
      }
      return distinctList;
    }

    @RemoteAction
    public static Survey__c GetSurveyByName(string surveyName) {
      List<Survey__c> surveys = [Select Id, Name__c, Can_Graph__c, SafeId__c, Survey_Url__c, Version__c 
      from Survey__c 
      where Name__c = :surveyName];
      if (surveys.size() > 0) {
        return surveys[0];
        } else {
          return new Survey__c();
        }
      }

    @RemoteAction
    public static Survey__c GetSurveyById(string id) {
      Survey__c survey = [Select Id, Name__c, Can_Graph__c, SafeId__c, Survey_Url__c, Version__c 
      from Survey__c 
      where Id = :id limit 1];
      System.debug(survey);
      return survey;
    }

    public static Event_Type__c GetEventType(string eventTypeName) {
    List<Event_Type__c> ets = [Select Id, Name, Active__c, Due_Date_Window__c, Primary__c, Recurring__c
    from Event_Type__c
    where Name = :eventTypeName];

    if (ets.size() > 0) {
      return ets[0];
      } else {
        return new Event_Type__c();
      }
    }

    public static Patient__c GetPatientById(string id) {
      List<Patient__c> patients = [Select Id, Last_Name__c, First_Name__c 
      From Patient__c Where Id= :id];
      if (patients.size() > 0) {
        return patients[0];
        } else {
          return new Patient__c();
        }
      }

    @RemoteAction
    public static List<Survey__c> GetSurveys() {
      List<Survey__c> surveys = [Select Id, Name__c, Can_Graph__c, SafeId__c, Survey_Url__c, Version__c 
                                From Survey__c
                                where Active__c = true];
      if (surveys.size() > 0) {
        
        return surveys;
        } else {
          return new List<Survey__c>();
        }
      }

      //this needs to be a SOQL Query
      @RemoteAction
      public static List<String> GetScoreNames(string surveyName) {
          List<String> questions = new List<String>();
          if (surveyName == 'PROM101') {
              questions.add('Symptoms');
              questions.add('Pain');
              questions.add('ADL');
              questions.add('Sport/Rec');
              questions.add('QOL');
          } else if (surveyName == 'XYZ Survey') {
              questions.add('Symptoms');
              questions.add('Pain');
              questions.add('Flex');
              questions.add('Walking');
              questions.add('Running');
          } else if (surveyName == 'KOOS') {
              questions.add('KOOS Symptoms');
              questions.add('KOOS Pain');
              questions.add('KOOS ADL');
              questions.add('KOOS Sport/Rec');
              questions.add('KOOS QOL');
          } else {         
              questions.add('Symptoms');
              questions.add('Pain');
              questions.add('ADL');
              questions.add('Sport/Rec');
              questions.add('QOL');
          }
          return questions;
   }

   public static List<Survey_Response__c> GetSurveyReponses(string patientId) {
      return new List<Survey_Response__c>();
   }

   @RemoteAction
   global static List<Survey__c> GetSurveysByEventType(string eventTypeId) {
      System.debug(eventTypeId);
      
      List<Event_Type_Survey__c> eventTypeSurveys = [Select Survey__r.Id, 
      Survey__r.Name__c, 
      Survey__r.Active__c, 
      Survey__r.SafeId__c, 
      Survey__r.Survey_Url__c, 
      Survey__r.Version__c,
      Survey__r.Can_Graph__c
      from Event_Type_Survey__c 
      Where Event_Type__c = :eventTypeId and Survey__r.Active__c = true];
      System.debug(eventTypeSurveys.size());
      System.debug(eventTypeSurveys);
      List<Survey__c> surveys = new List<Survey__c>();
      for(Event_Type_Survey__c evt: eventTypeSurveys) {
        if (!ExistsInSurveyList(surveys, evt)) {
          Survey__c s = new Survey__c();
          s.Id  = evt.Survey__r.Id;
          s.Name__c = evt.Survey__r.Name__c;
          s.Active__c = evt.Survey__r.Active__c;
          s.Survey_Url__c = evt.Survey__r.Survey_Url__c;
          s.Can_Graph__c = evt.Survey__r.Can_Graph__c;
          s.Version__c = evt.Survey__r.Version__c;
          //s.SafeId__c = evt.Survey__r.SafeId__c;
          surveys.add(s);         
      }
    }
    System.debug(surveys);
    return surveys;
  }

  @RemoteAction
   global static List<Survey__c> GetSurveysByEvent(string eventId) {
      System.debug(eventId);
      
      List<Event_Form__c> eventForms = [Select Survey__r.Id, 
            Survey__r.Name__c, 
            Survey__r.Active__c, 
            Survey__r.SafeId__c, 
            Survey_Url__c, 
            Survey__r.Version__c,
            Survey__r.Can_Graph__c
            From Event_Form__c 
            Where Event__c = :eventId and Survey__r.Active__c = true];
      System.debug(eventForms.size());
      System.debug(eventForms);
      List<Survey__c> surveys = new List<Survey__c>();
      for(Event_Form__c evt: eventForms) {
        if (!ExistsInSurveyList(surveys, evt)) {
          Survey__c s = new Survey__c();
          s.Id  = evt.Survey__r.Id;
          s.Name__c = evt.Survey__r.Name__c;
          s.Active__c = evt.Survey__r.Active__c;
          s.Survey_Url__c = evt.Survey_Url__c;
          s.Can_Graph__c = evt.Survey__r.Can_Graph__c;
          s.Version__c = evt.Survey__r.Version__c;
          //s.SafeId__c = evt.Survey__r.SafeId__c;
          surveys.add(s);         
      }
    }
    System.debug(surveys);
    return surveys;
  }
  
  public static boolean ExistsInSurveyList(List<Survey__c> surveys, Event_Form__c evt) {
    boolean surveyExists = false;
    for(Survey__c survey: surveys) {
        if (evt.Survey__r.Id == survey.Id) { return true; }
    }
    return surveyExists;
  }

  public static boolean ExistsInSurveyList(List<Survey__c> surveys, Event_Type_Survey__c evt) {
    boolean surveyExists = false;
    for(Survey__c survey: surveys) {
        if (evt.Survey__r.Id == survey.Id) { return true; }
    }
    return surveyExists;
  }

  /********************* classes to create test records *****************/
  public static Case__c CreateCase(string caseName, string caseTypeName, 
      Patient__c patient,
      Survey__c survey, 
      Event_Type__c eventType, 
      String procedureDate, 
      String surgeonId,
      String laterality) {
      
      Event_Type_Survey__c eventTypeSurvey = CreateEventTypeSurvey(eventType, survey); 
      System.debug('Created EventTypeSurvey record: ' + eventTypeSurvey);

            
      Case_Type__c ctRec = IQ_CreateData.RetrieveOrCreateCaseType(caseTypeName);
      System.debug('Creating Case Type record: ' + ctRec);

      Case__c caseRecord = new Case__c();
      // caseRecord.Active__c = true;
      // caseRecord.Description__c = caseName;
      // caseRecord.Laterality__c = 'Left';
      // caseRecord.Case_Type__c = ctRec.Id;
      // caseRecord.Name__c = caseName;
      // caseRecord.Patient__c = patient.Id;
      caseRecord = IQ_PatientActions.CreateCase(patient.Id, ctRec.Id, procedureDate, surgeonId, laterality);
      // insert caseRecord;
      System.debug('Creating Case record: ' + caseRecord);
        
      return caseRecord;
    }

      public static Case_Type__c CreateCaseType(string caseTypeDescription) {
        List<Case_Type__c> ctrecs = [Select ID, Description__c from Case_Type__c];
        List<Case_Type__c> found = new List<Case_Type__c>();
        for (Case_Type__c rec: ctrecs) {
          if (rec.Description__c.contains(caseTypeDescription)) {
            found.add(rec);
          }
        }
        System.debug('Found case Type: ' + found.size() );
        if (found.size() == 0) {
          Case_Type__c ctRec = new Case_Type__c(Description__c = caseTypeDescription);

          System.debug('Creating Case Type' + ctRec);
          insert ctRec;
          return ctRec;
        }
        else {
          System.debug('Case Type: ' + found[0].Description__c);
          return found[0];
        }
      }

      public static Case_Type_Event_Type__c CreateCaseTypeEventType(Case_Type__c caseType, Event_Type__c eventType) {
        Case_Type_Event_Type__c ctet = new Case_Type_Event_Type__c();
        ctet.Case_Type__c = caseType.Id;
        ctet.Event_Type__c = eventType.Id;
        insert ctet;
        return ctet;
      }

      public static Event_Type__c CreateEventType(string eventTypeName){
        Event_Type__c eventType = new Event_Type__c();
        eventType.Name = eventTypeName;
        eventType.Active__c = true;
        eventType.Primary__c = false;
        eventType.Recurring__c = false;
        insert eventType;
        system.debug(eventType);
        return eventType;
      }
      
      public static Survey__c CreateSurvey(string surveyName) {
        Survey__c survey = new Survey__c();
        survey.name__c = surveyName;
        survey.Active__c = true;
        survey.Can_Graph__c = true;
        survey.Survey_URL__c = 'testytest.com';
        //survey.Event_Type_Survey__c = eventTypeSurvey.Id;
        insert survey;
        System.debug(survey);
        return survey;

      }


      public static Event_Type_Survey__c CreateEventTypeSurvey(Event_Type__c eventType, Survey__c survey) {
        Event_Type_Survey__c eventTypeSurvey = new Event_Type_Survey__c();
        eventTypeSurvey.Event_Type__c = eventType.Id;
        eventTypeSurvey.Survey__c = survey.Id;
        insert eventTypeSurvey;
        return eventTypeSurvey;
      }

      public static Event_Form__c CreateEventForm(string eventName, Event__c event, Survey__c survey) {
        Event_Form__c ef = new Event_Form__c();
        ef.Name = eventName;
        ef.End_Date__c = date.newInstance(2016, 09, 21);
        ef.Event__c = event.Id;
      //ef.Event_Form_Url__c = ''; //what is this?
      ef.Form_Complete__c = true; 
      ef.Mandatory__c = false;
      ef.Start_Date__c = date.newInstance(2016, 09, 28);
      ef.Survey__c = survey.Id;
      //ef.CreatedBy = UserInfo.getUserName();
      //ef.LastModifiedBy = UserInfo.getUserId().Id;
      insert ef;
      return ef;
    }

    public static Event_Form__c GetEventForm(Survey__c survey, Event__c event) {
      List<Event_Form__c> eventForms = [Select Id, Name
      From Event_Form__c
      Where  Survey__r.id = :survey.Id and Event__r.id = :event.Id];
      if (eventForms.size() > 0) {
        return eventForms[0];
        } else {
          return new Event_Form__c();
        }
      }

    public static Event__c CreateEvent(string eventName, Event_Type__C eventType, string caseTypeName, 
        Survey__c survey, Patient__c patient, String procedureDate, String surgeonId, string laterality) {
          System.debug('Case Type:' + caseTypeName);
        Case_Type__c caseTypeRecord = CreateCaseType(caseTypeName);
        //Case__c caseRecord = CreateCase('Test Case', 'Case Type Name', 
        //                                  patient, survey, eventType); 
        Case__c caseRecord = IQ_PatientActions.CreateCase(patient.Id, caseTypeRecord.Id, procedureDate, surgeonId, laterality);
        System.debug(caseRecord);
        Event__c event = new Event__c();    
        event.Appointment_Comments__c  = '';
        event.Appointment_End__c  = date.newInstance(2016, 09, 28);
        event.Appointment_Start__c = date.newInstance(2016, 09, 28);
        event.Appointment_Status__c = 'Scheduled';
        event.Case__c = caseRecord.Id;
        //event.Date_Status__c = 
        event.Description__c = ''; 
        event.Event_Name__c = eventName;
        event.Event_Type__c = eventType.id;
        event.Status__c ='Pending Completion';
        insert event;
        System.debug(event);
        return event;
    }
  //this was creating for creating test data - its been depcrated so that we can use the existing 
  // routines in Orthologic 
  public static Patient__c CreatePatient(string hospitalId, string lastName, string firstName) {
      List<Patient__c> patients = [Select Id, Last_Name__c, First_Name__c, Date_Of_Birth__c
      From Patient__c 
      Where Last_Name__c = :lastName AND First_Name__c = : firstName];
      if (patients.size() > 0) {
        return patients[0];
        } else {          
          string medicalRecordNumber = string.ValueOf(Math.random() * 100000);
          string dateOfBirthString = '07/20/1954';

          Patient__c patient = IQ_PatientActions.CreateNewPatient( hospitalId, lastName, firstName, 
                            medicalRecordNumber, dateOfBirthString, 'M', '999-99-9999', 
                            string.ValueOf(Math.random() * 100000), 
                            string.ValueOf(Math.random() * 100000), 'White', 'English', 'rsalit@yahoo.com');                 
          //Patient__c patient = new Patient__c();
          //patient.Account_Number__c = string.ValueOf(Math.random() * 100000);
          //patient.Active__c = true;
          //patient.Address_Line_1__c = '1 Main Street';
          //patient.Address_Line_2__c = '';
          //patient.Address_Line_3__c = '';
          //patient.Address_Type__c = 'Main';
          //patient.Anonymous__c = false;
          //patient.Anonymous_Label__c = '';
          //patient.Anonymous_Year_of_Birth__c = '';
          //patient.Calculated_Age__c
          //patient.City__c = 'Plainview';
          //patient.Country__c = 'USA';
          //patient.Date_Of_Birth__c = date.newInstance(1967, 8, 20);
          //patient.First_Name__c = firstName;
          //patient.Full_Patient_Identifier__c
          //patient.Gender__c = 'Male';

          //patient.Hospital__c = Geta hospital !!!!

          //patient.Language__c = 'English';
          //patient.Last_Name__c = lastName;
          //patient.Marital_Status__c = 'Single';
          //patient.Medical_Record_Number__c = string.ValueOf(Math.random() * 100000);
          //patient.Middle_Initial__c = '';
          //patient.Race__c = 'Asian';
          //patient.Soundex_Metaphone_Value__c = '';
          //patient.Source_Hospital_Id__c = '';
          //patient.Source_Record_Id__c  = '';
          //patient.SSN__c = '789871233';
          //patient.State__c = 'UT';   
          //patient.SurveyUUID__c = '';
          //patient.Zip_Postal_Code__c = '06898';
          //insert patient;
          return patient;
        }
      }


      public static List<Patient__c> GetPatients() {
        List<Patient__c> patients = [Select Id, Name, First_Name__c, Last_Name__c, Hospital__c
        From Patient__c
        Where Active__c= true ];
        return patients;
      }

      public static Survey_Response__c CreateSurveyResponse(Event_Form__c eventForm,
        Survey__c survey, string question, string response) {
        System.debug(eventForm);
        System.debug(survey);
        System.debug(question); 
        System.debug(response);
        System.debug('Trying to insert');

        Survey_Response__c sr = new Survey_Response__c();
        sr.Event_Form__c = eventForm.Id;
        sr.Subject_UUID__c = string.ValueOf(Math.random() * 100000);
        sr.Survey__c = survey.Id;
        sr.Survey_Name__c = survey.Name__c;
        sr.Survey_Question__c = question;
        sr.Survey_Question_Text__c = question;
        sr.Survey_Response__c = response;
        sr.Survey_Version__c = 1.0;
        insert sr;
        return sr;
      }

    public static List<Survey_Response__c> CreateSurveyResponses(Event_Form__c eventForm,
        Survey__c survey, List<QuestionResponse> questionresponses) {
        System.debug(eventForm);
        System.debug(survey);
        System.debug(questionresponses); 
        // System.debug(response);
        System.debug('Trying to insert');
        List<Survey_Response__c> srs = new List<Survey_Response__c>();

        for (QuestionResponse qr: questionResponses) {
          System.debug(qr);
          if (qr.question != null && qr.response != null) {
            Survey_Response__c sr = new Survey_Response__c();
            sr.Event_Form__c = eventForm.Id;
            sr.Subject_UUID__c = string.ValueOf(Math.random() * 100000);
            sr.Survey__c = survey.Id;
            sr.Survey_Name__c = survey.Name__c;
            sr.Survey_Question__c = qr.question;
            sr.Survey_Question_Text__c = qr.question;
            sr.Survey_Response__c = String.valueOf(qr.response);
            sr.Survey_Version__c = 1.0;
            srs.add(sr);
          }
        }
        // insert srs;
        // IQ_PatientActions.CompleteEvent(eventForm.Event__r.Id);
        return srs;
      }

      public static Account CreateHospital(string hospitalName) {
          Account acct = GetHospitalByName(hospitalName);
          if (acct == null) { 
            List<RecordType> rectypes; 
            rectypes = [Select Id,SobjectType, Name From RecordType WHERE Name ='Hospital_Account'
                      and SobjectType='Account']; 
            string id;
            if (rectypes.size() > 0) {
              System.debug('Hospital record type is found!');
              id = rectypes[0].Id;
            } else {
              //get something!
              rectypes = [Select Id,SobjectType, Name From RecordType WHERE SobjectType='Account'];
              if (rectypes.size() > 0) {
                System.debug('Hospital record type is not found!');
                id = rectypes[0].Id;
              } else {
                System.debug('Record type is not found!');
                id = ''; 
              } 
            }
            Account newAcct = new Account();
            newAcct.Name = hospitalName;
            newAcct.RecordTypeId = id;
            newAcct.Active__c = 'True';
            //newAcct.IsPartner = true;  
            insert newAcct;
            return newAcct;
          } else {
            return acct;
          } 
      }

      public static Account GetHospitalByName(string acctName) {
        List<Account> accounts = new List<Account>();
        Account acct;

        accounts = [Select Id, Name From Account Where Name =: acctName];

        if (accounts.size() > 0) {
           acct = accounts[0];
           return acct;
       } else {
           return null;
       }
        
      }
      
      public static Account CreateSurgeonAccount() {
    	  Account surgeon = new Account();
        surgeon.FirstName = 'Surgeon';
        surgeon.LastName = 'Tester';
        surgeon.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        insert surgeon;
        return surgeon;
      }

      @RemoteAction
      public static void SendEmail(String fromAddress, String to, String subject, String body) {
        // Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        // Strings to hold the email addresses to which you are sending the email.
        String[] toAddresses = new String[] {to}; 
        String[] ccAddresses = new String[] {};
  
        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);
        mail.setCcAddresses(ccAddresses);

        // Specify the address used when the recipÏients reply to the email. 
        mail.setReplyTo('richard.salit@orthosensor.com');

        // Specify the name used as the display name.
        mail.setSenderDisplayName(fromAddress);

        // Specify the subject line for your email address.
        mail.setSubject(subject);

        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);

        // Optionally append the salesforce.com email signature to the email.
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(true);
        // Specify the text content of the email.
        mail.setPlainTextBody('A survey has been sent to you. Please convert to HTML mail to access');
        mail.setHtmlBody(body);

        // Send the email you have created.
        // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {mail};

        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        if (results[0].success) {
          System.debug('The email was sent successfully.');
        } else {
          System.debug('The email failed to send: ' + results[0].errors[0].message);
        }

      }
      public class QuestionResponse {
        public string question {get; set;}
        public double response {get; set;}
        public QuestionResponse(string question1, Double response1) {
            question = question1;
            response = response1;
        }
    }
    }