@isTest
private class EventFormViewExtensionTest {
	
	static testMethod void testEventFormViewExtension() {

		//Create Case, Event and Event Forms to use for Testing
		Case__c testCase = new Case__c();
		testCase.Name__c = 'TestCase';
		testCase.Description__C = 'Test description information';
		insert testCase;

		Event__c testEvent = new Event__c();
		testEvent.Event_Name__c = 'Test Event';
		testEvent.Description__C = 'Event description test data';
		testEvent.Status__c = 'Active';
		testEvent.Case__c = testCase.Id;
		insert testEvent;
        
        Survey__c survey = new Survey__c();
        survey.Active__c = true;
        survey.Name__c = 'Test Survey';
        survey.Survey_URL__c = 'http://test';
        insert survey;

		Event_Form__c testEventForm = new Event_Form__c();
		testEventForm.Event__c = testEvent.Id;
		testEventForm.Mandatory__c = true;
		testEventForm.Start_Date__c = date.Parse('02/01/2014');
		testEventForm.End_Date__c = date.Parse('04/01/2014');
        testEventForm.Survey__c = survey.Id;
		insert testEventForm;
        
        Survey_Response__c response = new Survey_Response__c();
        response.Event_Form__c = testEventForm.Id;
        response.Survey_Question__c = 'Q1';
        response.Survey__c = survey.id;
        response.Survey_Question_Text__c = 'Question';
        response.Survey_Response__c = 'Answer';
        insert response;

		//Construct controller
		ApexPages.StandardController stdEventForm = new ApexPages.StandardController(testEventForm);
        EventFormViewExtension conExt = new EventFormViewExtension(stdEventForm);

        //Execute code
		conExt.LogConstructor();
        conExt.clearSurveyResponsesandShowSurvey();
        conExt.closeNewSurvey();
        conExt.Cancel();
	}

}