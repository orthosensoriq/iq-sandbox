@isTest
public with sharing class SCMTestSetup {
	private SCMC__Currency_Master__c curr = createTestCurrency();
	private static Integer suffix = 1;
	
	public SCMTestSetup(){
		this.curr = this.createTestCurrency();
	}
	
    public static Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();
     
    public SCMC__Org_Address__c createOrg(){

        SCMC__Org_Address__c orgAddress = SCMC__Org_Address__c.getInstance();

        if(orgAddress == null) {
            orgAddress = new SCMC__Org_Address__c();
            orgAddress.SCMC__City__c = 'Unknown';
            orgAddress.SCMC__Country__c= 'Unknown';
            orgAddress.SCMC__Fax__c = '';
            orgAddress.SCMC__Name__c = 'Unknown';
            orgAddress.SCMC__Phone_Number__c = '901.218.3515';
            orgAddress.SCMC__Postal_Zip_Code__c = 'Unknown';
            orgAddress.SCMC__State__c = 'Unknown';
            orgAddress.SCMC__Street__c = 'Unknown';
			insert orgAddress;
         }

    	 return orgAddress;
    }
	
	public SCMC__Currency_Master__c createTestCurrency() {
    	SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
	    try {
	      curr = [select id, name
	          from SCMC__Currency_Master__c
	          where SCMC__corporate_currency__c = true];
	    } catch (Exception ex) {
	      curr.name = 'USD';
	      curr.SCMC__Active__c = true;
	      curr.SCMC__Curr_Conversion_Rate__c = 1;
	      curr.SCMC__Corporate_Currency__c = true;
	      insert curr;
	    }
	    return curr;
	}
	
	public Account createAccount(){
	    Account custSite = new Account();
	    SCMC__Currency_Master__c currValue = createTestCurrency();
	
	    custSite.AnnualRevenue = null;
	    custSite.SCMC__Corp_City__c = 'San Diego';
	    custSite.SCMC__Currency__c = currValue.Id;
	    custSite.SCMC__Corp_Country__c = 'USA';
	    custSite.SCMC__Corp_State_Province__c = 'CA';
	    custSite.SCMC__Corp_Line1__c = '247 Ocean Blvd.';
	    custSite.SCMC__Corp_Line2__c = null;
	    custSite.SCMC__Corp_PostalCode__c = '92130';
	    custSite.Description = 'Test customer site';
	    custSite.FAX = '555-1234';
	    custSite.ShippingCity = 'San Diego';
	    custSite.ShippingStreet = '247 Ocean Blvd.';
	    custSite.ShippingPostalCode = '92130';
	    custSite.ShippingState = 'CA';
	    custSite.ShippingCountry = 'USA';
	    custSite.Industry = null;
	    custSite.Name = 'Test Customer';
	    custSite.NumberOfEmployees = null;
	    custSite.Phone = null;
	    custSite.Ownership = null;
	    custSite.ParentId = null;
	    custSite.SCMC__Preferred_Communication__c = 'Fax';
	    custSite.SCMC__Sales_Rep__c = null;
	    custSite.SCMC__Sales_Responsibility__c = null;
	    custSite.SCMC__Ship_Via__c = null;
	    custSite.SCMC__Standard_Order_Detail__c = null;
	    custSite.SCMC__Small_Business_Designations__c = null;
	    custSite.SCMC__Tax_Exemption_Group__c = null;
	    custSite.SCMC__Terms__c = null;
	    custSite.TickerSymbol = null;
	    custSite.Type = null;
	    custSite.Website = null;
	    custSite.SCMC__Active__c = true;
	    custSite.SCMC__Customer__c = true;
	    insert custSite;
	
	    return custSite;
	}

	public Contact createContact(boolean active, Account account, string contactType) {
		Contact contact = new Contact();
		contact.SCMC__Active__c = active;
		contact.AccountId = account.id;
		contact.AssistantPhone = null;
		contact.AssistantName = null;
		contact.Birthdate = null;
		contact.Department = null;
		contact.Description = null;
		contact.Email = null;
		contact.FAX = null;
		contact.FirstName = 'Test';
		contact.HomePhone = null;
		contact.SCMC__Language__c = null;
		contact.MailingCity = null;
		contact.MailingCountry = null;
		contact.MailingState = null;
		contact.MailingStreet = null;
		contact.MailingPostalCode = null;
		contact.MobilePhone = null;
		contact.Phone = null;
		contact.OtherPhone = null;
		contact.Salutation = 'Mr.';
		contact.SCMC__Previous_Companies__c = null;
		contact.ReportsTo = null;
		contact.SCMC__Suffix__c = null;
		contact.Title = null;
		contact.LastName = 'Lastname';
		//contact.Customer_Tupe__c = contactType;
		insert contact;
		return contact;    
	}

    public ID createSalesRep(){
        return UserInfo.getUserId();
    }	
	
	public SCMC__Sales_Order__c createTestSalesOrder(Account account, boolean doInsert){
        SCMC__Sales_Order__c so = new SCMC__Sales_Order__c();
        ID salesRep = createSalesRep();

        so.SCMC__Sales_Order_Date__c = Date.today();
        so.SCMC__Primary_Sales_Rep__c = salesRep;
        so.SCMC__Customer_Account__c = account.Id;
        so.RecordTypeId = getRecordType('Inventory', so);

        if (doInsert){
            insert so;
        }

        return so;
    }

	public SCMC__Sales_Order_Line_Item__c createSalesOrderLine(SCMC__Sales_Order__c so, SCMC__Item__c item, String status, boolean doInsert){
        SCMC__Sales_Order_Line_Item__c line = new SCMC__Sales_Order_Line_Item__c();

        line.RecordTypeId = getRecordType('Item', line);
        line.SCMC__Sales_Order__c = so.Id;
        line.SCMC__Quantity__c = 1;
        line.SCMC__Price__c = 25.00;
        line.SCMC__Item_Master__c = item.Id;
        line.SCMC__Customer_Commitment_Date__c = Date.today();
        line.SCMC__Status__c = status;

        if (doInsert){
           insert line;
        }
        return line;
   }
   
   public SCMC__Item__c createTestItem(){
        SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'Test' + suffix++;
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
        insert im;
        return im;
    }
    
    public SCMC__Unit_of_Measure__c createUnitofMeasure(String name, String ilsName) {
        SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
        try {
            uom = [select id
                    from SCMC__Unit_of_Measure__c
                    where name = :name];
        } catch(Exception ex) {
            uom.name =name;
            if (ilsName <> null) {
                uom.SCMC__ILS_Unit_of_Measure__c = ilsName;
                uom.SCMC__Valid_for_ILS__c = true;
            }
            insert uom;
        }
        return uom;
    }

    public id getRecordType(String name, SObject userObject){
        ID retId = null;
        // Get the sObject describe result for the userObject
        Schema.DescribeSObjectResult r = userObject.getSObjectType().getDescribe();
        String objName = r.getName();
        List<RecordType> info = rTypes.get(objName);
        if (info == null){
            Map<ID, Schema.RecordTypeInfo> rInfo = r.getRecordTypeInfosByID();
            info = [select id
                    ,name
                    ,developerName
                from RecordType
                where id in :rInfo.keySet()];
            rTypes.put(objName, info);
        }
        for (RecordType rType : rTypes.get(objName)){
          System.debug('developer name ' + rType.developerName);
            if (rType.developerName == name){
                retId = rType.id;
                break;
            }
        }
        System.assertNotEquals(null, retId, name + ' record type not available for ' + objName);
        return retId;

    }

    public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Item__c item, boolean doInsert) {

		SCMC__Warehouse__c warehouse = setupWarehouse();
		SCMC__Inventory_Location__c invLoc = createTestLocation(warehouse);

		return setupPartsInInventory(warehouse, invLoc, item, doInsert);
	}

    public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Warehouse__c warehouse, SCMC__Inventory_Location__c invLoc, SCMC__Item__c item, boolean doInsert) {
        SCMC__Inventory_Position__c ip = new SCMC__Inventory_Position__c();
        ip.SCMC__Acquisition_Cost__c          = 55.00;
        ip.SCMC__Acquisition_Currency__c      = curr.id;
        ip.SCMC__Availability__c              = true;
        ip.SCMC__Availability_Code__c         = 'In Stock';
        ip.SCMC__Bin__c                       = invLoc.id;
        ip.SCMC__Current_Value__c             = 55.00;
        ip.SCMC__ICP_Acquisition_Cost__c      = 55.00;
        ip.SCMC__ICP_Currency__c              = curr.id;
        ip.SCMC__ILS_Eligible__c              = false;
        ip.SCMC__Item_Master__c               = item.Id;
        ip.SCMC__Listed_on_ILS__c             = false;
        ip.SCMC__Lot_Number__c                = null;
        ip.SCMC__Manufacturer_CAGE__c  = null;
        ip.SCMC__Owned_By__c                  = null;
        ip.SCMC__Quantity_Allocated__c        = 0;
        ip.SCMC__Quantity__c                  = 100;
        ip.SCMC__Quantity_in_Transit__c       = 0;
        ip.SCMC__Receipt_Line__c              = null;
        ip.SCMC__Receiving_Inspection__c      = null;
        ip.SCMC__Reserve_Price__c             = 60.00;
        ip.SCMC__Revision_Level__c            =  null;
        ip.SCMC__Sale_Price__c                = 65.00;
        ip.SCMC__Item_Serial_Number__c             = null;
        ip.SCMC__Shelf_Life_Expiration__c     = null;
        ip.SCMC__List_Type__c					= 'Y';
        if(doInsert){
        	insert ip;
        }
        return ip;
    }

    public SCMC__Warehouse__c setupWarehouse() {
    	SCMC__Address__c address = createTestAddress();
    	SCMC__ICP__c icp = createTestICP(address);
    	return createTestWarehouse(icp, address);
    }

    public SCMC__Address__c createTestAddress() {
        SCMC__Address__c address = new SCMC__Address__c();
        address.name = 'Test Address';
        address.SCMC__City__c = 'A City';
        address.SCMC__Country__c = 'Country';
        address.SCMC__Line1__c = 'Address line 1';
        address.SCMC__PostalCode__c = 'Postcd';
        address.SCMC__State__c = 'State';
        insert address;
        return address;
    }

    public SCMC__ICP__c createTestICP(SCMC__Address__c address) {
    	SCMC__ICP__c[] icp = [select id from SCMC__ICP__c limit 1];
    	if (icp.Size() == 0){
        	SCMC__ICP__c icpn = new SCMC__ICP__c(
        	SCMC__Address__c = address.id
        	,name = 'Test ICP'
        	,SCMC__Currency__c = curr.id);
        	insert icpn;
        	icp.add(icpn);
    	}
        return icp[0];
    }

    public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address){
    	return createTestWarehouse(icp, address, true);
    }

    public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address, boolean doInsert){
        SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
        warehouse.SCMC__ICP__c = icp.id;
        warehouse.SCMC__Address__c = address.id;
        warehouse.name = 'Test Warehouse';
        if (doInsert) {
        insert warehouse;
        }
        return warehouse;
    }

    public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse){
    	return createTestLocation(warehouse, true);
    }

    public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse, boolean doInsert) {
        SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c();
        location.name = 'Level1';
        location.SCMC__Level1__c = 'Level1';
        location.SCMC__Warehouse__c = warehouse.id;
        if (doInsert){
        	insert location;
        }
        return location;
    }

	public Opportunity createOpportunity(boolean doInsert) {
		Opportunity opp = new Opportunity();
		opp.Name = 'Test Opp' + suffix++;
		opp.StageName = 'Lead';
		opp.CloseDate = system.today() + 7;
		if (doInsert) {
			insert opp;
		}
		return opp;
	}

	public SCMC__Customer_Quotation__c createCustomerQuotation(Account account, Opportunity opp, boolean doInsert) {
		SCMC__Customer_Quotation__c quote = new SCMC__Customer_Quotation__c();
		ID salesRep = createSalesRep();

		quote.SCMC__Sales_Rep__c = salesRep;
		quote.SCMC__Status__c = 'Open';
		quote.SCMC__Customer_Account__c = account.Id;
		quote.SCMC__Customer_Account_Contact__c = null;
		quote.SCMC__Delivery_Options__c = 'Fax';
		quote.SCMC__Opportunity__c = opp.Id;
		
		if (doInsert) {
			insert quote;
		}

		return quote;
	}
     	
}