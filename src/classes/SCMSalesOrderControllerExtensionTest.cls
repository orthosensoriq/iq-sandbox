@isTest
private class SCMSalesOrderControllerExtensionTest {
    
    static SCMSetUpTest testClass = new SCMSetUpTest();
     
    static testMethod void testPrint() {
        
        Account acc = testClass.createTestCustomerAccount(true, true, false, null);
        list<SCMC__Item__c> itemList = new list<SCMC__Item__c>();
        SCMC__Item__c item = testClass.createTestItem(false);
        itemList.add(item);
        SCMC__Item__c item2 = testClass.createTestItem(false);
        itemList.add(item2);
        SCMC__Item__c item3 = testClass.createTestItem(false);
        itemList.add(item3);
        SCMC__Item__c item4 = testClass.createTestItem(false);
        itemList.add(item4);
        upsert itemList;
        
        SCMC__Sales_Order__c so = testClass.createTestSalesOrder(acc, true);
        
        list<SCMC__Sales_Order_Line_Item__c> solList = new list<SCMC__Sales_Order_Line_Item__c>();
        SCMC__Sales_Order_Line_Item__c sol = testClass.createSalesOrderLine(so, item, 'New',false);
        solList.add(sol);
        SCMC__Sales_Order_Line_Item__c sol2 = testClass.createSalesOrderLine(so, item2, 'New',false);
        solList.add(sol2);
        SCMC__Sales_Order_Line_Item__c sol3 = testClass.createSalesOrderLine(so, item3, 'New',false);
        solList.add(sol3);
        SCMC__Sales_Order_Line_Item__c sol4 = testClass.createSalesOrderLine(so, item4, 'New',false);
        solList.add(sol4);
        upsert solList;
        
        SCMSalesOrderControllerExtension ext = new SCMSalesOrderControllerExtension();      
        ext.SalesOrderId = so.Id;
        
        system.assertNotEquals(0, ext.getTotalValue(), 'Total value is not set');
        system.assertNotEquals(null, ext.getShippingAddress(), 'Ship to address is not set');
        system.assertNotEquals(null, ext.getBillingAddress(), 'Bill to address is not set');
        system.assertNotEquals(null, ext.getTempSO().SCMC__Fulfillment_Date__c, 'Date is not set');
        
        so.SCMC__Drop_Ship__c = true;
        so.SCMC__Drop_Ship_Name__c = 'Mikey';
        so.SCMC__Drop_Ship_Line1__c = 'Address Line 1';
        so.SCMC__Drop_Ship_Line2__c = 'Address Line 2';
        so.SCMC__Drop_Ship_City__c = 'City';
        so.SCMC__Drop_Ship_State_Province__c = 'State';
        so.SCMC__Drop_Ship_Zip_Postal_Code__c = 'Zipcode';
        so.SCMC__Drop_Ship_Country__c = 'USA';
        
        update so;
        
        string address = SCMUtilities.getFormattedAddress('Name', 'line1', 'line2', 'city', 'state', 'postalCode', 'country', 'Phone', 'Fax', 'Email', '<br />');
        system.assertNotEquals(null,address, 'Drop ship to address is not set');
        
        SCMSalesOrderControllerExtension ext2 = new SCMSalesOrderControllerExtension();     
        ext2.SalesOrderId = so.Id;

        system.assertNotEquals(null, ext2.getShippingAddress(), 'Drop ship to address is not set');
        
        SCMC__Customer_Address__c newAddress = testClass.createCustomerAddress(acc);
        so.SCMC__Drop_Ship__c = false;
        so.SCMC__Actual_Ship_To_Address__c = newAddress.Id;
        so.SCMC__Actual_Bill_To_Address__c = newAddress.Id;
        update so;      

        SCMSalesOrderControllerExtension ext3 = new SCMSalesOrderControllerExtension();     
        ext3.SalesOrderId = so.Id;

        system.assertNotEquals(null, ext3.getShippingAddress(), 'Drop ship to address is not set');
        system.assertNotEquals(null, ext3.getBillingAddress(), 'Bill to address is not set');
        
        so.SCMC__Drop_Ship__c = false;
        so.SCMC__Actual_Ship_To_Address__c = null;
        so.SCMC__Actual_Bill_To_Address__c = null;
        update so;      

        SCMSalesOrderControllerExtension ext4 = new SCMSalesOrderControllerExtension();     
        ext4.SalesOrderId = so.Id;
        
        system.assertNotEquals(null, ext4.getShippingAddress(), 'Drop ship to address is not set');
        system.assertNotEquals(null, ext4.getBillingAddress(), 'Bill to address is not set');
         
    }
    
}