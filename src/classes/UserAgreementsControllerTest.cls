@isTest(SeeAllData=true)
private class UserAgreementsControllerTest
{
	static testmethod void testUserAgreementEULA()
    {
		PageReference ref = new PageReference('/apex/UserAgreements');
        Test.setCurrentPage(ref); 
        ApexPages.currentPage().getParameters().put('EULA', '068Z00000002NevIAE');	//EULA DOC ID
        ApexPages.currentPage().getParameters().put('EULAVer', '1');	//EULA DOC ID        
        ApexPages.currentPage().getParameters().put('HIPAA', '068Z00000002SRjIAM');	//HIPAA DOC ID
        ApexPages.currentPage().getParameters().put('HIPAAVer', '3');	//HIPAA DOC ID
        ApexPages.currentPage().getParameters().put('retURL', URL.getSalesforceBaseUrl().toExternalForm());	//Home
    
        UserAgreementsController controller = new UserAgreementsController();	
        
        controller.AgreedVal = true;
        PageReference newref = controller.SaveAndReturn();
        if(newref == null)
        {
            controller.AgreedVal = true;
            newref = controller.SaveAndReturn();
            //System.assertEquals('/home/home.jsp', nextPage);
            if(newref != null)
	            System.assert(URL.getSalesforceBaseUrl().toExternalForm() != newref.getURL());
        }

    }
    
	static testmethod void testUserAgreementHIPAA()
    {
		PageReference ref = new PageReference('/apex/UserAgreements');
        Test.setCurrentPage(ref); 
        ApexPages.currentPage().getParameters().put('HIPAA', '068Z00000002NssIAE');	//HIPAA DOC ID
    
        UserAgreementsController controller = new UserAgreementsController();	
        
        controller.AgreedVal = true;
        PageReference newref = controller.SaveAndReturn();
        if(newref == null)
        {
            controller.AgreedVal = true;
            newref = controller.SaveAndReturn();
            //System.assertEquals('/home/home.jsp', nextPage);
            if(newref != null)
	            System.assert(URL.getSalesforceBaseUrl().toExternalForm() != newref.getURL());
        }

    }

}