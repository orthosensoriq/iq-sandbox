/*
    Copyright (c) 2013 LessSoftware.com, inc.
    All rights reserved.
    
	Plugin to support default recurring billing process
	Default creates SCM invoice for each contract that is eligible
*/
	global with sharing class SCMRecureInvoice extends SCMC.SCMPlugin{
	
	global  override void execute(ID[] recordIds) {
		List<SCMC__Service_Order__c >orders = [Select id
        		,name
        		,SCMC__Status__c
        		,SCMC__Service_Term_Record__c
        		,SCMC__Service_Term_Record__r.SCMC__Number_Of_Days__c
        		,SCMC__Service_Term_Record__r.SCMC__Number_Of_Months__c
        		,SCMC__Service_Order_First_Bill_Date__c
        		,SCMC__Service_Order_End_Date__c
        		,SCMC__Service_Order_Cancel_Date__c
        		,SCMC__Service_Order_Approved_Date__c
        		,SCMC__Service_Order_Active_Date__c
        		,SCMC__Number_Service_Lines__c
        		,SCMC__Next_Invoice_Date__c
        		,SCMC__Last_Invoice_Date__c
        		,SCMC__Customer_Account_Sold_To__c
        		,SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__c
        		,SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__r.SCMC__Basis_for_Due__c
        		,SCMC__Customer_Account_Sold_To__r.SCMC__Payment_Terms__r.SCMC__Number_of_Days__c
        		,SCMC__Current_Customer_Quotation__c
        		,SCMC__Currency__c
        		,SCMC__Bill_To_Address_Formula__c
        		,SCMC__Remain_Service_Term_Number__c
        		,SCMC__Invoice_Date_Option__c
        		,SCMC__Sales_Order__c
        		, (Select Id
        			, name
        			, SCMC__Service_Order__c
        			, SCMC__Current_Customer_Quotation_Line__c
        			, SCMC__Item_Description_Formula__c
        			, SCMC__Item_Number__c
        			, SCMC__Item_Type_Formula__c
        			, SCMC__Quantity__c
        			, SCMC__RC_After_Discount__c
        			, SCMC__RC_Base__c
        			, SCMC__RC_Contract_Value__c
        			, SCMC__Service_Contract_Line_End_Date__c
        			, SCMC__Service_Contract_Line_Start_Date__c
        			, SCMC__Service_Order_Line_Active_Date__c
        			, SCMC__Service_Order_Line_Approved_Date__c
        			, SCMC__Service_Order_Line_Cancel_Date__c
        			, SCMC__Status__c
        			, SCMC__Sales_Order_Line_Item__c
        			//, SCMC__Delivery_Method__c
        			 From SCMC__Service_Order_Lines__r
        			) 
        		From SCMC__Service_Order__c 
        		where id in :recordIds];
		//array of service contracts that successfully created invoices
		try {		
			//create invoice for each service contract passed in
			SCMBilling billing = new SCMBilling();
			for (SCMC__Service_Order__c order : orders) {
				billing.createSCMInvoice(order);
				billing.updateDates(order);	
			}
			billing.save();
			update orders;
			
		} catch (Exception ex){
			doRollback = true;
			throw new SCMException ('Creating SCM invoices failed ' + ex.getMessage());
		}
		return ;
		
	}

}