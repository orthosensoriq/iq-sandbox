@isTest(SeeAllData=true)
public class IQ_PatientActionsTest {
    @isTest
    static void testMethodStandardInternalUser1() { 
        
        Test.startTest();
        
        User currentUser = IQ_PatientActions.GetCurrentUser();
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        List<Account> practices = IQ_PatientActions.LoadPractices();
        
        System.runAs(currentUser){
            
            Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        	Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = hospitals.get(0).Id);
        	insert hospitalCaseType;            
            List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(hospitals.get(0).Id);

            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(hospitals.get(0).Id);     
            Patient__c patient = IQ_PatientActions.CreateNewPatient(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'M', 'SSN','P1234','A1234','White','English', 'Email@email.com');
            Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Left');
            
            List<Patient__c> patients = IQ_PatientActions.LoadPatientList(hospitals.get(0).Id);
            List<Case__c> cases = IQ_PatientActions.LoadCasesAndEvents(patient.Id);
            List<ADT_Interface__c> hl7Patients = IQ_PatientActions.LoadHL7PatientList(hospitals.get(0).Id);
            List<Patient__c> searchResultPatients = IQ_PatientActions.SearchPatientList(hospitals.get(0).Id, 'test');
            List<ADT_Interface__c> searchResultHL7Patients = IQ_PatientActions.SearchHL7PatientList(hospitals.get(0).Id, 'test');  
            
            List<Patient__c> all_patients = IQ_PatientActions.LoadPatientList(null);
            List<ADT_Interface__c> all_hl7Patients = IQ_PatientActions.LoadHL7PatientList(null);
            List<Patient__c> all_searchResultPatients = IQ_PatientActions.SearchPatientList(null, 'test');
            List<ADT_Interface__c> all_searchResultHL7Patients = IQ_PatientActions.SearchHL7PatientList(null, 'test'); 
            
			IQ_PatientActions.LoadCaseEvents(patientCase.Id);
            IQ_PatientActions.LoadSurveyScores(patientCase.Id, '123');
        }
        Test.stopTest();

    }
    @isTest
    static void testMethodStandardInternalUser2() { 
        
        Test.startTest();
        
        User currentUser = IQ_PatientActions.GetCurrentUser();
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        List<Account> practices = IQ_PatientActions.LoadPractices();
        
        System.runAs(currentUser){
  
            Patient__c patient = IQ_PatientActions.CreateNewPatient(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'M', 'SSN','P1234','A1234','White','English', 'email@email.com');
            patient = IQ_PatientActions.UpdatePatient(patient.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'F', 'SSN','P1234','A1234','White','Spanish', 'xay@test.com');

            Patient__c duplicatePatient = IQ_PatientActions.CheckDuplicate(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English');                    
            Patient__c duplicateAnonymousPatient = IQ_PatientActions.CheckDuplicateAnonymous(hospitals.get(0).Id, 'Label', '1965');                    

        }
        Test.stopTest();

    }

    @isTest
    static void TestMethodStandardInternalUser3() {
        Test.startTest();
        
        User currentUser = IQ_PatientActions.GetCurrentUser();
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        List<Account> practices = IQ_PatientActions.LoadPractices();
        
        System.runAs(currentUser){
            
            Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
            Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = hospitals.get(0).Id);
            insert hospitalCaseType;            
            List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(hospitals.get(0).Id);

            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(hospitals.get(0).Id);     
            Patient__c patient = IQ_PatientActions.CreateNewPatient(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'M', 'SSN','P1234','A1234','White','English', 'email@email.com');
            // Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Both');
            }
            Test.stopTest();
    }
    @isTest
    static void testCheckAnonymous() {
        Test.startTest();
        // this could be a better test!
        Account acct = new Account(Name='XYZ');
        insert acct;
        Boolean tflag = IQ_PatientActions.checkAnonymous(acct.Id);
        System.assertEquals(tFlag, false);
        Test.stopTest();
    }

    static testMethod void testIndependentMethods() {
    
        Test.startTest();
        
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        
        Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = hospitals.get(0).Id);
        insert hospitalCaseType; 
        
        List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(hospitals.get(0).Id); 
        List<Product_Catalog__c> productCatalogs = IQ_PatientActions.LoadProducts();
        List<OS_Device_Ref__c> sensorRefs = IQ_PatientActions.LoadSensorRefs();
        
        Boolean isAnonymous = IQ_PatientActions.checkAnonymous(hospitals.get(0).Id);
        Patient__c anonymousPatient = IQ_PatientActions.CreateNewAnonymousPatient(hospitals.get(0).Id, 'Label', 'MedicalRecordNumber', '1950', 'M', 'SSN','P1234','A1234','White','English', 'email@email.com');
        anonymousPatient = IQ_PatientActions.UpdateAnonymousPatient(anonymousPatient.Id, 'Label', 'MedicalRecordNumber', '1950', 'F', 'SSN','P1234','A1234','White','Spanish', 'test@test.com');
 
        List<Event_Type__c> eventType = [Select Id from Event_Type__c where Name = 'Procedure'];
        Event__c event = [Select Id, Case__c, Physician__c  from Event__c Where Event_Type__c =: eventType[0].Id].get(0);  
        IQ_PatientActions.UpdateProcedure(event.Id, event.Case__c, event.Physician__c, 'Right', '05/05/2020', true, 'General');
        IQ_PatientActions.AddNote('title', 'text', event.Id);
        IQ_PatientActions.UpdateNote('title', 'text', [Select Id from Note].get(0).Id);
        IQ_PatientActions.DeleteNote([Select Id from Note].get(0).Id);
        IQ_PatientActions.AddImplantManually(productCatalogs.get(0).Id, 'lotNumber', '05','2020', event.Id);
        IQ_PatientActions.AddImplant('firstBarcode','secondBarcode', '05','2020', event.Id);
        Implant_Component__c implant = [SELECT Id FROM Implant_Component__c Limit 1].get(0);
        IQ_PatientActions.DeleteImplant(implant.Id);
        OS_Device_Ref__c ref = new OS_Device_Ref__c(Name='RF123', Manufacturer__c='MFGR');
        insert(ref);
        IQ_PatientActions.AddSensorManually(ref.Id, 'man', 'sn', 'lot', '05','2020', event.Id);
        IQ_PatientActions.AddSensorBarcode('barcode', event.Id);
        IQ_PatientActions.LoadSensorDataLines(event.Id);
        List<Survey__c> surveys = IQ_PatientActions.LoadSurveys();
        List<Event_Type_Survey__c> eventTypeSurveys = IQ_PatientActions.LoadEventSurveysForSurvey(surveys.get(0).Id);
        IQ_PatientActions.CompleteEvent(event.Id);
        IQ_PatientActions.LoadLinkStationsIDs(hospitals.get(0).Id);
        IQ_PatientActions.GetImageList(event.Id, 'deviceId', 'linkStationId');
        //IQ_PatientActions.SaveImageToRecord(event.Id,'URL'); //Requires Callout.. Not supported
        IQ_PatientActions.MatchSensorDataAndAttachment(event.Id);
        Os_device__c sensor = [Select Id from Os_device__c Limit 1].get(0);
        IQ_PatientActions.DeleteSensorLine(null,null,null,null,null,null,null,null,event.Id);
        IQ_PatientActions.DeleteSensor(sensor.Id,event.Id);
        Map<Id,Contact> contactMap = new Map<Id, Contact>([Select Id,Name from Contact Limit 3]);
        IQ_PatientActions.logCRUD('Update', contactMap, contactMap);
        IQ_PatientActions.logCRUDMap('Update', contactMap);
        IQ_PatientActions.logCRUDList('Update', contactMap.values());
        IQ_PatientActions.logCRUDsObject('Update', contactMap.values().get(0));
        IQ_PatientActions.logReadsObject([Select Id from Patient__c Limit 1].get(0).Id,'Patient__c');
        IQ_PatientActions.logReadsObject([Select Id from Case__c Limit 1].get(0).Id,'Case__c');
        IQ_PatientActions.logReadsObject([Select Id from Event__c Limit 1].get(0).Id,'Event__c');
        Test.stopTest();
        
    }

    @isTest    
    static void testMethodHospitalUser() { 
        
        
    	Account account_h = new Account(name ='PartnerHospitalAccount') ;
        account_h.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hospital Account').getRecordTypeId();
        insert account_h;

        Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = account_h.Id);
        insert hospitalCaseType;
        List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(account_h.Id);
        
        Account surgeon = new Account();
        surgeon.FirstName = 'Surgeon';
        surgeon.LastName = 'Tester';
        surgeon.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        insert surgeon;
        surgeon = [Select Id,personContactId from Account Where Id=:surgeon.Id];
        
        Surgeon_Hospital__c sh = new Surgeon_Hospital__c();
        sh.Surgeon__c = surgeon.personContactId;
        sh.Hospital__c = account_h.Id;//hospitals.get(0).Id;
        insert sh;
                
        Contact contact_h = new Contact(LastName ='Hospital Tester',AccountId = account_h.Id);
        insert contact_h; 
        
        Id profileId = [select id from profile where name='Partner Community User'].id;
        
        User exUser_h = new User(alias = 'tester h', email='tester_h@email.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                                 ContactId = contact_h.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester_h@email.com');
        insert exUser_h;
            
		Test.startTest();
        System.runAs(exUser_h){
            
            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(null);    
            Patient__c patient = IQ_PatientActions.CreateNewPatient(account_h.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English', 'email@email.com');                    
            Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Left');
            
            List<Patient__c> patients = IQ_PatientActions.LoadPatientList(account_h.Id);
            List<Case__c> cases = IQ_PatientActions.LoadCasesAndEvents(patient.Id);
            List<ADT_Interface__c> hl7Patients = IQ_PatientActions.LoadHL7PatientList(null);
            List<Patient__c> searchResultPatients = IQ_PatientActions.SearchPatientList(null, 'test');
            List<ADT_Interface__c> searchResultHL7Patients = IQ_PatientActions.SearchHL7PatientList(null, 'test');            
            IQ_PatientActions.LoadCaseEvents(patientCase.Id);
            Patient__c duplicatePatient = IQ_PatientActions.CheckDuplicate(account_h.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English');                    
            
            IQ_PatientActions.LoadSurveyScores(patientCase.Id, '123');
            
        }
        Test.stopTest();
        
    }

    @isTest
    static void testMethodPracticeUser() { 
        
        Account account_h = new Account(name ='PartnerHospitalAccount') ;
        account_h.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hospital Account').getRecordTypeId();
        insert account_h;
        
        Account surgeon = new Account();
        surgeon.FirstName = 'Surgeon';
        surgeon.LastName = 'Tester';
        surgeon.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Surgeon Account').getRecordTypeId();
        insert surgeon;
        surgeon = [Select Id,personContactId from Account Where Id=:surgeon.Id];
        
        Account account_p = new Account(name ='PartnerHospitalAccount') ;
        account_p.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Practice Account').getRecordTypeId();
        account_p.ParentId = account_h.Id;
        insert account_p; 

        Surgeon_Hospital__c sh = new Surgeon_Hospital__c();
        sh.Surgeon__c = surgeon.personContactId;
        sh.Hospital__c = account_p.Id;
        insert sh;
        
        Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        Hospital_Case_Type__c practiceCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = account_p.Id);
        insert practiceCaseType;
        List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(account_p.Id);
        
        Contact contact_p = new Contact(LastName ='Practice Tester',AccountId = account_p.Id);
        insert contact_p;
        
        Id profileId = [select id from profile where name='Partner Community User'].id;
        
        User exUser_p = new User(alias = 'tester p', email='tester_p@email.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                                 ContactId = contact_p.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester_p@email.com');
        insert exUser_p;
    
        Test.startTest();
        System.runAs(exUser_p){
            System.debug(account_p);
            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(account_p.Id);                 
            Patient__c patient = IQ_PatientActions.CreateNewPatient(account_p.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English', 'email@email.com');                    
            System.debug(caseTypes.get(0).Case_Type__r.Id);
            System.debug(surgeons);
            System.debug(surgeons.get(0).Surgeon__r.AccountId);
            Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Left');
            
            
            List<Patient__c> patients = IQ_PatientActions.LoadPatientList(account_p.Id);
            List<Case__c> cases = IQ_PatientActions.LoadCasesAndEvents(patient.Id);
            List<ADT_Interface__c> hl7Patients = IQ_PatientActions.LoadHL7PatientList(null);
            List<Patient__c> searchResultPatients = IQ_PatientActions.SearchPatientList(null, 'test');
            List<ADT_Interface__c> searchResultHL7Patients = IQ_PatientActions.SearchHL7PatientList(null, 'test');            
            IQ_PatientActions.LoadCaseEvents(patientCase.Id);
            Patient__c duplicatePatient = IQ_PatientActions.CheckDuplicate(account_p.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English');                    
            
            IQ_PatientActions.LoadSurveyScores(patientCase.Id, '123');
        }
        Test.stopTest();
    }

    @isTest
    static void CheckSoftwareVersionTest() {
        Test.startTest();
        List<Event_Type__c> eventType = [Select Id from Event_Type__c where Name = 'Procedure'];
        Event__c event = [Select Id, Case__c, Physician__c  from Event__c Where Event_Type__c =: eventType[0].Id].get(0);
        // String version = IQ_PatientActions.CheckSoftwareVersion(event.Id, '9999', 'Left');
        System.assert(true);
        Test.stopTest();
    }

    @isTest static void IsUserAdminTest() {
        Test.startTest();
        //Profile p = [SELECT Id FROM Profile WHERE Name='Control Center Admin'];
        //User u2 = new User(Alias = 'newUser', Email='newuser@testorg.com',
        //EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        //LocaleSidKey='en_US', ProfileId = p.Id,
        //TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');

        User u2 = [Select Id, Name from User where Id=: UserInfo.getUserId() limit 1];

        System.debug(u2);
        System.runAs(u2) {
        Boolean tFlag  = IQ_PatientActions.IsUserAdmin();
        // System.Assert(tFlag);
        }
        Test.stopTest();
    }

    @isTest
    static void LoadHospitalPracticesTest() {
        Test.startTest();
        Account account_h = new Account(name ='PartnerHospitalAccount') ;
        account_h.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hospital Account').getRecordTypeId();
        insert account_h;
         
        Account account_p = new Account(name ='PartnerHospitalAccount') ;
        account_p.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Practice Account').getRecordTypeId();
        account_p.ParentId = account_h.Id;
        insert account_p; 
        List<Account> accts = IQ_PatientActions.LoadHospitalPractices(account_h.Id);
        System.assert(accts.size() > 0);
        Test.stopTest();
    }

    @isTest
    static void CreateCaseTest() { 
        
        Test.startTest();
        
        User currentUser = IQ_PatientActions.GetCurrentUser();
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        List<Account> practices = IQ_PatientActions.LoadPractices();
        
        System.runAs(currentUser){
            
            Case_Type__c caseType = [Select Id, Description__c from Case_Type__c].get(0);
        	Hospital_Case_Type__c hospitalCaseType = new Hospital_Case_Type__c (Case_Type__c = caseType.Id, Hospital__c = hospitals.get(0).Id);
        	insert hospitalCaseType;            
            List<Hospital_Case_Type__c> caseTypes = IQ_PatientActions.LoadCaseTypes(hospitals.get(0).Id);

            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(hospitals.get(0).Id);     
            Patient__c patient = IQ_PatientActions.CreateNewPatient(hospitals.get(0).Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'M', 'SSN','P1234','A1234','White','English', 'email@email.com');
            Case__c patientCase = IQ_PatientActions.CreateCase(patient.Id, caseTypes.get(0).Case_Type__r.Id, '05/01/2018', surgeons.get(0).Surgeon__r.AccountId, 'Left');
        
        }
        
        Test.stopTest();
    }

    @isTest
    static void LoadHospitalByPracticeTest() {
        Test.startTest();
        
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
        List<Account> practices = IQ_PatientActions.LoadPractices();
        System.debug(practices);
        if (practices.size() > 0) {
            Account practice = practices[0];
            Account hospital = IQ_PatientActions.LoadHospitalByPractice(practice.Id);
        }
        System.assert(true);
        Test.stopTest();
    }

    @isTest
    static void GetPatientTest() {
        Account account_h = new Account(name ='PartnerHospitalAccount') ;
        account_h.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Hospital Account').getRecordTypeId();
        insert account_h;
        Account account_p = new Account(name ='PartnerHospitalAccount') ;
        account_p.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Practice Account').getRecordTypeId();
        account_p.ParentId = account_h.Id;
        insert account_p; 
        List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(null);                 
        Patient__c patient = IQ_PatientActions.CreateNewPatient(account_p.Id, 'LastName', 'FirstName', 'MedicalRecordNumber', '05/01/1950', 'Male', 'SSN','P1234','A1234','White','English', 'email@email.com');   
        Test.startTest();
        Patient__c patientActual = IQ_PatientActions.GetPatient(patient.Id);
        System.assertEquals('LastName', patientActual.Last_Name__c, 'Last Name is incorrect!');
        Test.stopTest();
    }
}