@IsTest
private class AuditLoggingTest
{
	static testmethod void TestLogUtils()
    {
		//Test Insert
        logUtils logging = new logUtils();
        Account testObject = new Account();
        testObject.Name = 'Test Account 1';
        Insert testObject;
 
        Map<Id, SObject> accounts = new Map<Id, SObject>();
        accounts.put(testObject.Id, testObject);
        List<sObject> accountlist = new List<sObject>();
        accountlist.add(testObject);
        
		logging.logCRUD('Create', accounts);
        logging.logCRUD('Update', accounts, accounts);
        logging.logCRUD('Read', accounts);
        logging.logCRUD('Read', accountlist);
        logging.logCRUD('Read', testObject);
        
        Delete testObject;
        logging.logCRUD('Delete', accounts);
    }
    
    static testmethod void TestLogController()
    {
        //Test retrieve Log records
        logController getLogs = new logController();
        getLogs.filterUser = UserInfo.getUserId();
        getLogs.filterObjectType = 'Account';
        getlogs.filterStartDate = DateTime.now();
        getlogs.filterEndDate = DateTime.now();        
        List<Audit_Log__c> testLogs = getLogs.getAuditLogs();
        system.assertEquals(testLogs.size(), 0);
    }
    
    static testmethod void TestOSTriggerHandler()
    {
     	//Test Trigger Handlers
        osTriggerHandler handleIt = new osTriggerHandler(true, 1, 'Testing');
        if(handleIt.IsTriggerContext)
	        system.assertEquals(true, true);

    	if(handleIt.IsVisualforcePageContext)
            system.assertEquals(true, true);
            
        if(handleIt.IsWebServiceContext)
            system.assertEquals(true, true);
            
        if(handleIt.IsExecuteAnonymousContext)
            system.assertEquals(true, true);

        Map<Id, Account> tempBefore = new Map<Id, Account>([Select Id, Name, FirstName, CreatedDate, CreatedById From Account Order by CreatedDate DESC Limit 5]);
        Map<Id, Account> tempAfter = new Map<Id, Account>([Select Id, Name, FirstName, CreatedDate, CreatedById From Account Order by CreatedDate DESC Limit 5]);
        for(string acctId : tempAfter.keySet())
        {
            Account acct = tempAfter.get(acctId);
            acct.FirstName = 'EPTest';
            tempAfter.put(acctId, acct);
        }
        
        List<User> tempUserInsert = [Select Id, Name, CreatedDate, CreatedById From User Order by CreatedDate DESC Limit 3];
        handleIt.OnBeforeInsert(tempUserInsert);
        handleIt.OnAfterInsert(tempBefore);
        handleIt.OnBeforeUpdate(tempBefore, tempAfter);    
        handleIt.OnAfterUpdate(tempBefore, tempAfter);
        handleIt.OnBeforeDelete(tempBefore);
        handleIt.OnAfterDelete(tempBefore);
        handleIt.OnUndelete(tempUserInsert);
    }
    
    static testmethod void TestOSController()
    {
       	osController emptyConstructor = new osController();
        Account a = new Account(name='Tester');
    	insert a;
    	ApexPages.StandardController sc = new ApexPages.standardController(a);
     	osController baseCtrl = new osController(sc);
        baseCtrl.LogView();
		baseCtrl.LogPDFView();
        baseCtrl.LogPDFGenerated();
        baseCtrl.LogCSVGenerated();
        Patient__c testPatient = new Patient__c();
        testPatient.First_Name__c = 'Test';
        testPatient.Last_Name__c = 'Tester';
        insert testPatient;
        
        testPatient.Account_Number__c = '1234567890987654321';
        update testPatient;
        
        map<integer, string> params = new map<integer, string>();
		params.put(0,testPatient.Name);
        list<Case__c> tempCase = baseCtrl.getSelectedData('PatientViewCase', params);   
		list<Patient__c> tempPat = baseCtrl.getSelectedData(testPatient.Id);   
        tempPat = baseCtrl.getSelectedData(testPatient);   
        
        delete testPatient;
        
        undelete testPatient;
    }

	static testmethod void TestOSSelector()
    {
        osSelector selector = new osSelector(new User());
        list<User> users = selector.getSelectedData();
        string idToTest = users[0].id;
        
        osSelector selector2 = new osSelector(idToTest);
        list<user> users2 = selector2.getSelectedData();
        system.assertEquals(users2.size(), 1);
        
        osSelector selector3 = new osSelector();
        string emptyStr = selector3.selectStatement;
        map<integer, string> tempUserParam = new map<integer, string>();
        tempUserParam.put(0, idToTest);
        selector3.setSelectStatement('UserViewLogging', tempUserParam);

        Account newHosp = new Account();
        newHosp.name = 'Test Hosp';
        Insert newHosp;
        
        Patient__c newPatient = new Patient__c();
        newPatient.First_Name__c = 'Test';
        newPatient.Last_Name__c = 'Tester';
        Insert newPatient;
        
        Case__c newCase = new Case__c();
        newCase.Name__c = 'Test Case';
        Insert newCase;
        
        Event_Type__c evtType = new Event_Type__c();
        evtType.Name = 'TestEventType';
        evtType.Primary__c = true;
        Insert evtType;
        
		Event__c newEvent = new Event__c();
        newEvent.Event_Name__c = 'Test Procedure';
        //newEvent.Event_Type__c = evtType.Id;
        newEvent.Case__c = newCase.Id;
        Insert newEvent;
        
        map<integer, string> tempEventParam = new map<integer, string>();
        tempEventParam.put(0, newEvent.Id);
        selector3.setSelectStatement('CaseSelectorProcedure', tempEventParam);
        selector3.setSelectStatement('ProcedureSummary', tempEventParam);
        selector3.setSelectStatement('ImplantSummaryProcedure',tempEventParam);
        selector3.setSelectStatement('ImplantSummaryList',tempEventParam);
        selector3.setSelectStatement('ImplantComponentsAggregate',tempEventParam);
        selector3.setSelectStatement('ImplantSummaryAttachments',tempEventParam);
        selector3.setSelectStatement('EventFormsList', tempEventParam);
        
        map<integer, string> tempEventCaseParam = new map<integer, string>();
        tempEventCaseParam.put(0, newCase.Name);
        selector3.setSelectStatement('CaseViewEvent', tempEventCaseParam);

        map<integer, string> tempPatientParam = new map<integer, string>();
        tempPatientParam.put(0, newpatient.id);
        selector3.setSelectStatement('ProcedureSummaryPatient', tempPatientParam);

        map<integer, string> tempHospParam = new map<integer, string>();
        tempHospParam.put(0, newHosp.id);
        selector3.setSelectStatement('ProcedureListingPatients', tempHospParam);
        
        Event_Form__c testEventForm = (Event_Form__c)epTestUtils.NewObject(new Event_Form__c());
        testEventForm.Event__c = newEvent.id;
        insert testEventForm;
        
        map<integer, string> tempEventFormParam = new map<integer, string>();
        tempEventFormParam.put(0, testEventForm.Id);
        selector3.setSelectStatement('EventFormViewResponses', tempEventFormParam);
        
        map<integer, string> tempEventTypeParam = new map<integer, string>();
        tempEventTypeParam.put(0,evtType.id);
        selector3.setSelectStatement('ProcedureListings', tempEventTypeParam);
        
        map<integer, string> tempUserId = new map<integer, string>();
        tempUserId.put(0,userinfo.getUserId());
        selector3.setSelectStatement('UserSettingsUser', tempUserId);
        
        map<integer, string> tempContactId = new map<integer, string>();
        tempContactId.put(0,'000000000000000');
        selector3.setSelectStatement('UserSettingsContact', tempContactId);
    }
    
    static testmethod void testOsViewLoggingExtension()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

		//EventTrigger, osTriggerHandler, BaseTriggerHandler
        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;

        ApexPages.StandardController sc = new ApexPages.StandardController(testEvent);
        osViewLoggingExtension logging = new osViewLoggingExtension(sc);
        
        //PageReference pageRef = Page.Event__c;
        //pageRef.getParameters().put('id', String.valueOf(testEvent.Id));
        //Test.setCurrentPage(pageRef);
        
        logging.LogConstructor();
    }
        
    static testmethod void testUserViewLoggingExtension()
    {
        User testUser = [Select id, City from User where id =: System.UserInfo.getUserId()];
		ApexPages.StandardController sc = new ApexPages.StandardController(testUser);
        UserViewLoggingExtension logging = new UserViewLoggingExtension(sc);
        
        logging.LogConstructor();
    }
    
    static testmethod void testAuditViewLoggingExtension()
    {
        //User testUser = [Select id, City from User where id =: System.UserInfo.getUserId()];
        Audit_Log__c testAudit = new Audit_Log__c();
        testAudit.S_object_Type__c = 'Audit_Log__c';
        testAudit.Action__c = 'Read';
        insert testAudit;
        
		ApexPages.StandardController sc = new ApexPages.StandardController(testAudit);
        osViewLoggingExtension logging = new osViewLoggingExtension(sc);
        
        logging.LogConstructor();
    }

    //---------- TEST LOGGING TRIGGERS!! ------------//
    static testmethod void testPatientLoggingTrigger()
    {
		//PatientTrigger, osTriggerHandler, BaseTriggerHandler
        Patient__c testPatient = new Patient__c();
        testPatient.First_Name__c = 'Test';
        testPatient.Last_Name__c = 'Tester';
        insert testPatient;
        
        testPatient.Account_Number__c = '1234567890987654321';
        update testPatient;
        
        delete testPatient;
        
        undelete testPatient;
    }
    
    static testmethod void testCaseLoggingTrigger()
    {
        //CaseTrigger, osTriggerHandler, BaseTriggerHandler
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;
        
        testCase.Name__c = 'TestCase!';
        update testCase;
        
        delete testCase;
        
        undelete testCase;
    }    
 
  
    static testmethod void testEventLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

		//EventTrigger, osTriggerHandler, BaseTriggerHandler
        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
        testEvent.Description__c = 'TestEvent!';
        update testEvent;
        
        delete testEvent;
        
        undelete testEvent;
    }    

    static testmethod void testClinicalDataLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
        //ClinicalDataTrigger, osTriggerHandler, BaseTriggerHandler
        Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
        testClinicalData.Event__c = testEvent.id;
        insert testClinicalData;
        
        testClinicalData.Patient_Consent_Obtained__c = true;
        update testClinicalData;
        
        delete testClinicalData;
        
        undelete testClinicalData;
    }    

    static testmethod void testBarCodeLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
        Product_Catalog__c testProduct = (Product_Catalog__c)epTestUtils.NewObject(new Product_Catalog__c());
        insert testProduct;

        Product_Catalog__c testProduct2 = (Product_Catalog__c)epTestUtils.NewObject(new Product_Catalog__c());
        insert testProduct2;

        Implant_Component__c testImplantComponent = (Implant_Component__c)epTestUtils.NewObject(new Implant_Component__c());
        testImplantComponent.Event__c = testEvent.id;
        insert testImplantComponent;
        
	    //BarCodeTrigger, osTriggerHandler, BaseTriggerHandler
        Bar_Code__c testBarCode = (Bar_Code__c)epTestUtils.NewObject(new Bar_Code__c());
        testBarCode.Implant_Component__c = testImplantComponent.Id;
        //testBarCode.Event__c = testEvent.id;
        //testBarCode.Product_Catalog__c = testProduct2.Id;
        //testBarCode.Raw_Bar_Code__c = '1234567890987654321';
        insert testBarCode;
        
        testBarCode.Raw_Bar_Code__c = '1234567890987654321';
        //testBarCode.Product_Catalog__c = testProduct.Id;
        update testBarCode;
        
        delete testBarCode;
        
        undelete testBarCode;
    }    
	
    @IsTest(SeeAllData=true)
    static void testOSDeviceTrigger()
    {
        Manufacturer__c m = new Manufacturer__c (name = 'Zimmer');
        insert m;
        OS_Device__c testDevice = new OS_Device__c();
        testDevice.size__c = '13';
        testDevice.Year_Assembled__c = '2013';
        testDevice.Manufacturer_Name__c = 'Zimmer'; //((list<Manufacturer__c>)[select name from Manufacturer__c limit 1])[0].Name;
        insert testDevice;
    }
    
    static testmethod void testImplantComponentLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
    	//ImplantComponentTrigger, osTriggerHandler, BaseTriggerHandler
        Implant_Component__c testImplantComponent = (Implant_Component__c)epTestUtils.NewObject(new Implant_Component__c());
        testImplantComponent.Event__c = testEvent.id;
        insert testImplantComponent;
        
        testImplantComponent.Serial_Number__c = '1234567890987654321';
        update testImplantComponent;
        
        delete testImplantComponent;
        
        undelete testImplantComponent;
    }    
    
    static testmethod void testDocumentLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
        //DocumentTrigger, osTriggerHandler, BaseTriggerHandler
        Document__c testDocument = (Document__c)epTestUtils.NewObject(new Document__c());
        testDocument.Event__c = testEvent.id;
        insert testDocument;
        
        testDocument.Type__c = 'Image';
        update testDocument;
        
        delete testDocument;
        
        undelete testDocument;
    }    
    
    static testmethod void testSensorDataLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;
        
        //SensorDataTrigger, osTriggerHandler, BaseTriggerHandler
        Sensor_Data__c testSensorData = (Sensor_Data__c)epTestUtils.NewObject(new Sensor_Data__c());
        testSensorData.Event__c = testEvent.Id;
        insert testSensorData;
        
        testSensorData.ImageFileName__c = 'TestImageName';
        update testSensorData;
        
        delete testSensorData;
        
        undelete testSensorData;
    }    
    
    static testmethod void testEventFormLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;

        Event_Form__c testEventForm = (Event_Form__c)epTestUtils.NewObject(new Event_Form__c());
        testEventForm.Event__c = testEvent.id;
        insert testEventForm;
        
        testEventForm.Mandatory__c = true;
        update testEventForm;
        
        delete testEventForm;
        
        undelete testEventForm;
    }    
    
	static testmethod void testSurveyResponseLoggingTrigger()
    {
        Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;

        Event_Form__c testEventForm = (Event_Form__c)epTestUtils.NewObject(new Event_Form__c());
        testEventForm.Event__c = testEvent.id;
        testEventForm.Mandatory__c = true;
        insert testEventForm;
        
        Survey_Response__c testResponse = new Survey_Response__c();
        testResponse.Event_Form__c = testEventForm.id;
        testResponse.Survey_Question__c = '1';
        testResponse.Survey_Question_Text__c = 'Test Question';
        insert testResponse;
        
        testResponse.Survey_Response__c = 'This is the test value...';
        update testResponse;
        
        delete testResponse;
        
        undelete testResponse;
    }
    static testmethod void testUserLoggingTrigger()
    {
        //UserTrigger, osTriggerHandler, BaseTriggerHandler
        User testUser = new User();
        testUser.FirstName = 'Audit';
        testUser.LastName = 'Test';
        testUser.Username = 'AuditTest@OrthoSensor.com.ctrlctrdev';
        testUser.Email = 'AuditTest@OrthoSensor.com.ctrlctrdev';
        testUser.Alias = 'Audit';
        testUser.TimeZoneSidKey = 'America/New_York';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.ProfileId = '00eU0000000kul0IAA';
        insert testUser;
        
        User testUser2 = [Select id, City from User where id =: System.UserInfo.getUserId()];
        testUser2.City = 'Tampa';
        update testUser2;
        
    }    
    static testmethod void testInterfaceTrigger()
    {
		Case__c testCase = (Case__c)epTestUtils.NewObject(new case__c());
        insert testCase;

        Event__c testEvent = (Event__c)epTestUtils.NewObject(new Event__c());
        testEvent.Case__c = testCase.Id;
        insert testEvent;        
        
        ADT_Interface__c testInterface = (ADT_Interface__c)epTestUtils.NewObject(new ADT_Interface__c());
        insert testInterface;
        
        testInterface.event__c = testEvent.id;
        testInterface.Processed__c = true;
        update testInterface;
        
        delete testInterface;
    }
    
}