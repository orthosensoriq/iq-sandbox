/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class epTestUtilsTest {
	 	static testMethod void testGenerateRandomDateTest() {
	        Date testDate = epTestUtils.GenerateRandomDate(0,0,30,true);
	    }
	    
	    static testMethod void testGenerateRandomTimeTest() {
	    	Time testTime = epTestUtils.GenerateRandomTime(12,59,false);
	    }
	    
	    static testMethod void testGenerateRandomURL() {
	    	String testURL = epTestUtils.GenerateRandomURL();
	    }
	    static testMethod void testGenerateRandomPhone() {
	    	String testPhone = epTestUtils.GenerateRandomPhone();
	    }
	    static testMethod void testGenerateRandomDouble() {
	    	Double testDouble = epTestUtils.GenerateRandomDouble(3, 2);
	    }
	    
	   /* static testMethod void testGenerateRandomObject() {
	    	Test.StartTest();
	    	List<Account> accountList = epTestUtils.ListOfNewObjects(new Account(), 2);
	    	List<Contact> contactList = epTestUtils.ListOfNewObjects(new Contact(), 2);
	    	System.debug(LoggingLevel.INFO,accountList);
	    	Test.StopTest();
	    }*/
}