/*****************************************************
This class is designed to provide security 
    functionality for Orthosensor managed internal users

REVISIONS
------------------------------------------------------
01/06/2013 - support@enablepath.com - Created

*****************************************************/

public class ManagedSecurityController
{
    public boolean UserAcceptedCurrent { get; set; }
    public boolean ExternalRedirect { get; set; }
    ContentVersion EULAdoc;
    ContentVersion HIPAAdoc;
    boolean matchesEULA = false;
    boolean matchesHIPAA = false;

    public ManagedSecurityController()
    {
        ExternalRedirect = false;
        UserAcceptedCurrent = validateCurrentUserAcceptance();
        system.debug('UserAcceptedCurrent: ' + UserAcceptedCurrent);
    }

    public ManagedSecurityController(boolean External)
    {
        ExternalRedirect = External;
        UserAcceptedCurrent = validateCurrentUserAcceptance();
        system.debug('UserAcceptedCurrent: ' + UserAcceptedCurrent);
    }

    private boolean validateCurrentUserAcceptance()
    {
        //Grab the latest version of the EULA and HIPAA from content record
        //  This will grab the content version with the latest LastModifiedDate
        //  that has been publically published and has the IsLatest flag set to 
        //  true.  This will deal with multiple EULA's being uploaded, but in a
        //  perfect world, there will only be one EULA agreement and one HIPAA
        //  Agreement

        system.debug('getting User...' + System.UserInfo.getUserId());
        User curUser = [SELECT  Id,
                                Accepted_EULA_Version__c,
                                Accepted_HIPAA_Version__c
                        FROM    User
                        WHERE   Id =: System.UserInfo.getUserId()];

        //system.debug('getting EULADoc...');
        EULAdoc = [SELECT   Id, 
                            ContentDocument.Id,
                            Title, 
                            VersionNumber, 
                            LastModifiedDate 
                  FROM      ContentVersion 
                  WHERE     Agreement_Type__c = 'EULA' and 
                            IsLatest = true and 
                            PublishStatus = 'P' 
                  ORDER BY  LastModifiedDate desc 
                  LIMIT 1];

        system.debug('getting HIPAADoc...');
        HIPAAdoc = [SELECT  Id, 
                            ContentDocument.Id,
                            Title, 
                            VersionNumber, 
                            LastModifiedDate 
                  FROM      ContentVersion 
                  WHERE     Agreement_Type__c = 'HIPAA' and 
                            IsLatest = true and 
                            PublishStatus = 'P' 
                  ORDER BY  LastModifiedDate desc 
                  LIMIT 1];

        system.debug('got past selects');
        matchesEULA = (curUser.Accepted_EULA_Version__c == EULAdoc.Id);
        matchesHIPAA = (curUser.Accepted_HIPAA_Version__c == HIPAAdoc.Id);
        
        system.debug('matchesEULA: ' + matchesEULA + ', matchesHIPAA: ' + matchesHIPAA);
        return (!matchesEULA || !matchesHIPAA ? false : true);
    }

    public String getRedirectUrl() 
    {
        string URLParams = (!matchesEULA ? 'EULA=' + Euladoc.Id + '&EULAVer=' + Euladoc.VersionNumber : '');
        if(URLParams != '' && !matchesHIPAA)
        {
            URLParams += '&';
        }
        URLParams += (!matchesHIPAA ? 'HIPAA=' + HIPAAdoc.Id + '&HIPAAVer=' + HIPAAdoc.VersionNumber : '');
        URLParams += '&retURL=';
        if(ExternalRedirect)
        {
        	Domain communityURL = [select Id, Domain from Domain where DomainType = 'DNS' LIMIT 1];
            if(communityURL != null)
            {
            	URLParams += 'https://' + communityURL.Domain + '/CommunitiesLanding';
            }
            else
            {
                URLParams += URL.getSalesforceBaseUrl().toExternalForm();
            }
        }
        else
        {
        	URLParams += URL.getSalesforceBaseUrl().toExternalForm();
        }
        
        //system.debug(URLParams);
        String redirectURL = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/UserAgreements?' + URLParams;
        return redirectURL;
    }

}