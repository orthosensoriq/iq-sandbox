/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Handles trigger functionality for SCMC__Receiving_Inspection__c objects.
**/
public with sharing class SCMReceivingInspectionTriggerHandler {

	private boolean isExecuting = false;
	private integer batchSize = 0;
	private Boolean isUpdate = false;

	public SCMReceivingInspectionTriggerHandler(boolean isExecuting, integer size) {
		this.isExecuting = isExecuting;
		this.batchSize = size;
	}

	public void OnAfterUpdate(SCMC__Receiving_Inspection__c[] oldItems, Map<ID, SCMC__Receiving_Inspection__c> oldMap, SCMC__Receiving_Inspection__c[] newItems, Map<ID, SCMC__Receiving_Inspection__c> newMap){

		isUpdate = true;

		createNewFixedAssets(oldMap, newItems);

		set<Id> riIds = new set<Id>();
		for(Integer i = 0; i < this.batchSize; i++){
			SCMC__Receiving_Inspection__c oldRI = oldItems[i];
			SCMC__Receiving_Inspection__c newRI = newItems[i];
			if(oldRI.SCMC__Lot_Number__c != newRI.SCMC__Lot_Number__c){
				riIds.add(newRI.Id);
			}
		}
		if(riIds.size() > 0){
			list<SCMC__Serial_Number__c> snLines = [select Id, Name, Lot_Number__c, SCMC__Receipt_Line__c, SCMC__Receiving_Inspection__c  from SCMC__Serial_Number__c where SCMC__Receiving_Inspection__c In :riIds];
			if(snLines.size() > 0){
				SCMSerialNumberTriggerHandler snTriggerClass = new SCMSerialNumberTriggerHandler(snLines, snLines.size());
				snTriggerClass.updateSerialNumber(riIds);
				update snLines;
			}
		}
	}

	private void createNewFixedAssets(Map<Id, SCMC__Receiving_Inspection__c> oldMap, SCMC__Receiving_Inspection__c[] newItems){

		if (!isUpdate) {
			throw new SCMException('This method may be used for update operations only.');
		}

		if (oldMap == null || oldMap.isEmpty()) {
			throw new SCMException('The old-item map is required.');
		}

		// At the time that the fixed asset should be created, the receiving inspection exists, so we're
		// looking for the Date_Completed__c to be populated as the update of interest.
		List<SCMC__Receiving_Inspection__c> inspectionToProcess = new List<SCMC__Receiving_Inspection__c>();

		for (SCMC__Receiving_Inspection__c newOne : newItems) {

			SCMC__Receiving_Inspection__c oldOne = oldMap.get(newOne.Id);

			if (
				newOne.SCMC__Production_Order__c != null &&
				'Complete'.equals(newOne.SCMC__Status__c) &&
				oldOne.Date_Completed__c == null &&
				newOne.Date_Completed__c != null
			){
				inspectionToProcess.add(newOne);
			}
		}

		if (!inspectionToProcess.isEmpty()) SCMFixedAsset.createFixedAssets(inspectionToProcess);
	}
}