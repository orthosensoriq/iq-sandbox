/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
public with sharing class SCMSerialNumberTriggerHandler {
	private integer batchSize = 0;
	private list<SCMC__Serial_Number__c> snList = null;
	private list<SCMC__Serial_Number_Related_Record__c> snRelatedList = null;

	public SCMSerialNumberTriggerHandler(list<SCMC__Serial_Number__c> lines, integer size) {
		this.batchSize = size;
		this.snList = lines;
	}

	public SCMSerialNumberTriggerHandler(list<SCMC__Serial_Number_Related_Record__c> lines, integer size) {
		this.batchSize = size;
		this.snRelatedList = lines;
	}

	public void OnBeforeInsertOrUpdateRelatedRecord(){
		map<Id, SCMC__Transfer_Request_Line__c> trIdToTRLDetails = new map<Id, SCMC__Transfer_Request_Line__c>();
		for(SCMC__Serial_Number_Related_Record__c snrr : this.snRelatedList){
			if(snrr.SCMC__Transfer_Request_Line__c != null){
				trIdToTRLDetails.put(snrr.SCMC__Transfer_Request_Line__c, null);
			}
		}

		if(trIdToTRLDetails.size() > 0){
			trIdToTRLDetails = 
				new Map<ID, SCMC__Transfer_Request_Line__c>([
					select id
						,name
						,SCMC__Ownership_Code__c
						,SCMC__Lot_Number__c
						,SCMC__Shelf_Life_Expiration__c
						,SCMC__Transfer_Request__c
						,SCMC__Transfer_Request__r.SCMC__Source_Warehouse__c
					from SCMC__Transfer_Request_Line__c
					where id in :trIdToTRLDetails.Keyset()]);

			for(SCMC__Serial_Number_Related_Record__c snrr : this.snRelatedList){
				Id trlId = snrr.SCMC__Transfer_Request_Line__c;
				if(trlId != null){
					SCMC__Transfer_Request_Line__c trl = trIdToTRLDetails.get(trlId);
					if(trl != null){
						snrr.SCMC__Ownership__c = trl.SCMC__Ownership_Code__c;
						snrr.Transfer_Request_Line__c = trl.ID;
						snrr.SCMC__Warehouse__c = trl.SCMC__Transfer_Request__r.SCMC__Source_Warehouse__c;
					}
					snrr.Shipping__c = snrr.SCMC__Shipping__c;
				}
			}

		}
	}

	public void OnBeforeInsert(){
		set<Id> rlIds = new set<Id>();
		set<Id> riIds = new set<Id>();
		for(SCMC__Serial_Number__c sn : this.snList){
			if(sn.SCMC__Receipt_Line__c != null){
				rlIds.add(sn.SCMC__Receipt_Line__c);
			}
			if(sn.SCMC__Receiving_Inspection__c != null){
				riIds.add(sn.SCMC__Receiving_Inspection__c);
			}
		}

		if(rlIds.size() > 0){
			map<Id, SCMC__Receipt_Line__c> recLineDetails = new map<Id, SCMC__Receipt_Line__c>();
			for(SCMC__Receipt_Line__c rec : [select Id
												   ,SCMC__Lot_Number__c
											   from SCMC__Receipt_Line__c
											  where Id  In :rlIds]){

				recLineDetails.put(rec.Id, rec);
			}

			for(SCMC__Serial_Number__c sn : this.snList){
				Id reclineId = sn.SCMC__Receipt_Line__c;
				if(reclineId != null && recLineDetails.containsKey(reclineId)){
					SCMC__Receipt_Line__c rec = recLineDetails.get(reclineId);
					sn.Lot_Number__c = rec.SCMC__Lot_Number__c;
				}
			}
		}

		this.updateSerialNumber(riIds);
	}

	public void OnAfterInsert(List<SCMC__Serial_Number__c> newItems, Map<Id, SCMC__Serial_Number__c> newMap)
	{
		SCMFixedAsset.updateSerialNumberReferences(newItems);
	}

	/*
	public void onAfterUpdate(list<SCMC__Serial_Number__c> oldlines){
		list<SCMC__Serial_Number_Related_Record__c> snRRList = new list<SCMC__Serial_Number_Related_Record__c>();
		for(Integer i = 0; i < this.batchSize; i++){

			SCMC__Serial_Number__c newSN = snList[i];
			SCMC__Serial_Number__c oldSN = oldlines[i];

			if(newSN.SCMC__Last_Transfer_Request_Line__c != null){
				SCMC__Serial_Number_Related_Record__c snrr = new SCMC__Serial_Number_Related_Record__c();
				snrr.SCMC__Serial_Number__c = newSN.Id;
				snrr.Shipping__c = oldSN.SCMC__Shipping__c;
				snrr.Transfer_Request_Line__c = oldSN.SCMC__Transfer_Request_Line__c;
				snRRList.add(snrr);
			}

		}

		if(snRRList.size() > 0){
			upsert snRRList;
		}
	}
	*/

	public void OnBeforeUpdate(list<SCMC__Serial_Number__c> oldlines){
		set<Id> riIds = new set<Id>();
		Map<Id, SCMC__Warehouse__c> wIds = new Map<Id, SCMC__Warehouse__c>();
		for(Integer i = 0; i < this.batchSize; i++){
			SCMC__Serial_Number__c newSN = snList[i];
			SCMC__Serial_Number__c oldSN = oldlines[i];

			if(newSN.SCMC__Receiving_Inspection__c != null &&
			   newSN.SCMC__Receiving_Inspection__c != oldSN.SCMC__Receiving_Inspection__c){
				riIds.add(newSN.SCMC__Receiving_Inspection__c);
			}

			if (newSN.SCMC__Warehouse__c != oldSN.SCMC__Warehouse__c){
				if (newSN.SCMC__Warehouse__c == null){
					newSN.SCMC__Ownership_Code__c = null;
				} else {
					wIds.put(newSN.SCMC__Warehouse__c, null);
				}
			}

			if(newSN.SCMC__Receiving_Inspection__c != null &&
			   newSN.SCMC__Receiving_Inspection__c != oldSN.SCMC__Receiving_Inspection__c){
				riIds.add(newSN.SCMC__Receiving_Inspection__c);
			}

		}

		this.updateSerialNumber(riIds);
		//this.updateOwnershipCode(transferRequestLineIds);
		this.updateOwnership(wIds);
	}

	public void updateSerialNumber(set<Id> riIds){
		if(riIds.size() > 0){
			map<Id, SCMC__Receiving_Inspection__c> inspectDetails = new map<Id, SCMC__Receiving_Inspection__c>();
			for(SCMC__Receiving_Inspection__c ri : [select Id
												   ,SCMC__Lot_Number__c
											   from SCMC__Receiving_Inspection__c
											  where Id In :riIds]){

				inspectDetails.put(ri.Id, ri);
			}

			for(SCMC__Serial_Number__c sn : this.snList){
				Id riId = sn.SCMC__Receiving_Inspection__c;
				if(riId != null && inspectDetails.containsKey(riId)){
					SCMC__Receiving_Inspection__c ri = inspectDetails.get(riId);
					sn.Lot_Number__c = ri.SCMC__Lot_Number__c;
				}
			}
		}
	}

	/*
	private void updateOwnershipCode(set<Id> trlIds){
		if(trlIds.size() > 0){
			map<Id, SCMC__Transfer_Request_Line__c> trlIDToDetailMap = new map<Id, SCMC__Transfer_Request_Line__c>();
			for(SCMC__Transfer_Request_Line__c line : [select ID
															 ,SCMC__Transfer_Request__c
															 ,SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c
														 from SCMC__Transfer_Request_Line__c
														where Id In :trlIds]){
				trlIDToDetailMap.put(line.ID, line);
			}

			list<SCMC__Inventory_Position__c> updateIPList = new list<SCMC__Inventory_Position__c>();
			for(SCMC__Inventory_Position__c ip : [select Id, SCMC__Ownership_Code__c, SCMC__Transfer_Request_Line__c
													from SCMC__Inventory_Position__c
												   where SCMC__Transfer_Request_Line__c In :trlIds]){
				id trlId = ip.SCMC__Transfer_Request_Line__c;
				if(trlIDToDetailMap.containsKey(ip.SCMC__Transfer_Request_Line__c)){
					SCMC__Transfer_Request_Line__c trLine = trlIDToDetailMap.get(trlId);
					if(trLine.SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c != null &&
					   ip.SCMC__Ownership_Code__c != trLine.SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c){
						ip.SCMC__Ownership_Code__c = trLine.SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c;
						updateIPList.add(ip);
					}
				}
			}

			if(updateIPList.size() > 0){
				update updateIPList;
			}

			for(SCMC__Serial_Number__c sn : this.snList){
				id trlId = sn.SCMC__Last_Transfer_Request_Line__c;
				if(trlId != null && trlIDToDetailMap.containsKey(trlId)){
					SCMC__Transfer_Request_Line__c trLine = trlIDToDetailMap.get(trlId);
					if(trLine.SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c != null){
						sn.SCMC__Ownership_Code__c = trLine.SCMC__Transfer_Request__r.SCMC__Destination_Ownership__c;
					}
				}
			}
		}
	}
	*/

	public void updateOwnership(Map<Id, SCMC__Warehouse__c> wIds){

		if (wIds.Keyset().Size() > 0){
			wIds = new Map<Id, SCMC__Warehouse__c> ([select id
					,Ownership_Code__c
				from SCMC__Warehouse__c
				where id in :wIds.Keyset()]);
		}
		for(SCMC__Serial_Number__c sn : this.snList){
			if (sn.SCMC__Warehouse__c == null){
				//skip null warehouse - ownership code has already been changed
				continue;
			}
			SCMC__Warehouse__c wh = wIds.get(sn.SCMC__Warehouse__c);
			if (wh != null){
				sn.SCMC__Ownership_Code__c = wh.Ownership_Code__c;
			}
		}
	}
}