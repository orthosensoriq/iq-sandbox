public with sharing class EventViewExtension {

	public final Event__c currEvent;
    //public List<Event_Form__c> currEventForms {get; set;}
    //public boolean showRefreshLabel { get; set; }
    public boolean displayPopup {get; set;}
    public String surveyURL {get; set;}
    public boolean renderImageBlock {get; set;}
    public boolean renderDocumentBlock {get; set;}
    public boolean renderComponentBlock {get; set;}
    public boolean renderFormBlock {get; set;}
    
    // The extension constructor initializes the private member
    // variable currEvent by using the getRecord method from the standard
    // controller.
    public EventViewExtension(ApexPages.StandardController stdController) {
        this.currEvent = (Event__c)stdController.getRecord();
        //showRefreshLabel = false;
        renderImageBlock = true;
        renderDocumentBlock = false;
        renderComponentBlock = false;
        renderFormBlock = false;
        //getEventForms();
    }

    //	Moved to ProcedureSummaryController
    //public void getEventForms() {
    //    currEventForms = [SELECT Id, Name, survey__r.Name__c, Event_Form_URL__c, Mandatory__c, Form_Complete__c, Start_Date__c, End_Date__c FROM Event_Form__c WHERE Event__c = :currEvent.Id];
    //}
	//
    //public void showRefreshMsg() {
    //    showRefreshLabel = true;
    //    system.debug('showRefreshMsg has been called =====>');
    //}

    public void closePopup() {   
        displayPopup = false;    
    }     
    
    public void showPopup() { 
        displayPopup = true;
        system.debug('showPopup is being called =====>');
    }
    
    public void GoToImages()
    {
        renderImageBlock = true;
        renderDocumentBlock = false;
        renderComponentBlock = false;
        renderFormBlock = false;
        system.debug('Showing Images!');
    }
    
    public void GoToDocuments()
    {
        renderImageBlock = false;
        renderDocumentBlock = true;
        renderComponentBlock = false;
        renderFormBlock = false;
        system.debug('Showing Documents!');
    }
    
    public void GoToImplantComponents()
    {
        system.debug('Showing Components (beginning)!');
        renderImageBlock = false;
        renderDocumentBlock = false;
        renderComponentBlock = true;
        renderFormBlock = false;
        system.debug('Showing Components (end)!');
    }
    
    public void GoToForms()
    {
        renderImageBlock = false;
        renderDocumentBlock = false;
        renderComponentBlock = false;
        renderFormBlock = true;
        system.debug('Showing Forms!');
    }

//		Moved to ProcedureSummaryController    
//    public PageReference refreshEventForms()
//    {
//        getEventForms();
//        if(currEventForms.size() > 0)
//	     	showRefreshLabel = false;   
//    
//        return null;
//    }
}