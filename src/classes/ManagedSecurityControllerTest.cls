@isTest
private class ManagedSecurityControllerTest
{
    //////////////////////////////////////////////////
    //	To test 100%, the testing user needs to 	//
    //	have the following fields on their User 	//
    //	record set to null:							//
    //		Accepted_EULA_Version__c				//
    //		Accepted_HIPAA_Version__c				//
    //////////////////////////////////////////////////
    
    static testmethod void testManagedSecurity()
    {
        User u;
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Insert account as current user
        //System.runAs (thisUser) {        
        //    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        //    UserRole r = [SELECT id FROM UserRole Where Name='Consultant'];
        //    u = new User(Alias = 'OSTest', Email='OSTester@OSTest.com', EmailEncodingKey='UTF-8', 
        //                      LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', 
        //                      ProfileId = p.Id, TimeZoneSidKey='America/New_York', UserRoleId = r.Id, 
        //                      UserName='OSTester@OSTest.com');
        //    Insert u;
        //}
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = 'Acceptance Documents']; 
        RecordType rt = [Select Id From recordtype where Name = 'End-User Agreements' Limit 1];
        ContentVersion testContentInsert = new ContentVersion(); 
        testContentInsert.ContentURL='http://www.google.com/'; 
        testContentInsert.Title = 'OSEula'; 
        testContentInsert.RecordTypeId = rt.id;
        insert testContentInsert; 
        
        ContentVersion testContent = [SELECT ContentDocumentId, PublishStatus, Agreement_Type__c, IsLatest FROM ContentVersion where Id = :testContentInsert.Id]; 
        ContentWorkspaceDoc newWorkspaceDoc = new ContentWorkspaceDoc(); 
        
        system.debug(testContent.PublishStatus + ' | ' + testContent.Agreement_type__c + ' | ' + testContent.IsLatest);
        
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
        
        insert newWorkspaceDoc; 
        
        testContent.Agreement_Type__c = 'EULA';
        //testContent.FirstPublishLocationId = newWorkspaceDoc.id;
        update testContent;

        ContentVersion testContentInsert2 = new ContentVersion(); 
        testContentInsert2.ContentURL='http://www.google.com/test'; 
        testContentInsert2.Title = 'OSHIPAA'; 
        testContentInsert2.RecordTypeId = rt.id;
        insert testContentInsert2; 
        
        ContentVersion testContent2 = [SELECT ContentDocumentId, PublishStatus, Agreement_Type__c, IsLatest FROM ContentVersion where Id = :testContentInsert2.Id]; 
        ContentWorkspaceDoc newWorkspaceDoc2 = new ContentWorkspaceDoc(); 
        
        system.debug(testContent2.PublishStatus + ' | ' + testContent2.Agreement_type__c + ' | ' + testContent2.IsLatest);
        
        newWorkspaceDoc2.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc2.ContentDocumentId = testContent2.ContentDocumentId; 
        
        insert newWorkspaceDoc2; 
        
        testContent2.Agreement_Type__c = 'HIPAA';
        //testContent2.FirstPublishLocationId = newWorkspaceDoc2.id;
        update testContent2;

        System.runAs(thisUser) 
      	{
            PageReference ref = new PageReference('/apex/SiteManagedSecurity'); 
            Test.setCurrentPage(ref); 
            ManagedSecurityController controller = new ManagedSecurityController();	
            system.assert(controller.UserAcceptedCurrent == false);
            
            ref = new PageReference('/apex/SiteManagedSecurity'); 
            Test.setCurrentPage(ref); 
            controller = new ManagedSecurityController();
            String returnTo = controller.getRedirectUrl();
            system.assert(returnTo.length() != 0);
    
            Test.setCurrentPage(ref); 
            controller = new ManagedSecurityController(true);
            returnTo = controller.getRedirectUrl();
            system.assert(returnTo.length() != 0);
        }
   }
    
}