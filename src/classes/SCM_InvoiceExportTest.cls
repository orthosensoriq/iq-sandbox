@isTest
private class SCM_InvoiceExportTest {

	private static SCM_SetupExportTest st = new SCM_SetupExportTest();
	
	@isTest static void testInvoiceExport() {
		
		// create test data
		SCMC__Unit_of_Measure__c uom = st.createUnitofMeasure('TestUOM', '');
		SCMC__Warehouse__c wh = st.setupWarehouse();
		SCMC__Inventory_Location__c loc1 = st.createTestLocation(wh);
		SCMC__Item__c item = st.createTestItem();
		SCMC__Inventory_Position__c ip1 = st.setupPartsInInventory(wh, loc1, item, false);
		ip1.SCMC__Condition_Code__c = null;
		insert ip1;

		SCMC__Sales_Order__c so = st.createTestSalesOrder(true);
		SCMC__Sales_Order_Line_Item__c sol = st.createSalesOrderLine(so, item, 'New', false);
		sol.SCMC__Condition_Code__c = null;
		insert sol;

		Test.startTest();

		SCMC.SalesOrderControllerExtension controller = new SCMC.SalesOrderControllerExtension(new ApexPages.StandardController(so));
		PageReference retPage = controller.Allocate();

		SCMC__Sales_Order__c rSo = [select Id, SCMC__Status__c, SCMC__Shipment_Status__c from SCMC__Sales_Order__c where Id = :so.Id];
		system.debug('soPostAll: ' + rSo);

		// sim approval
		rSo.SCMC__Status__c = 'Approved';
		update rSo;

		Test.stopTest();

		rSo = [select Id, SCMC__Status__c, SCMC__Shipment_Status__c from SCMC__Sales_Order__c where Id = :so.Id];
		system.debug('soPostApp: ' + rSo);
		

		SCMC__Invoicing__c inv = [select Id, SCMC__Export_Status__c 
			from SCMC__Invoicing__c
			where SCMC__Sales_Order__c = :so.Id];

		inv.SCMC__Shipping__c = null;
		inv.SCMC__Export_Status__c = 'Ready to Export';
		update inv;

		SCM_InvoiceExport.ExportedInvoice[] expInvoices = SCM_InvoiceExport.getInvoices();
		system.assertEquals(1, expInvoices.size(), 'unexpected number of invoices to export');

		Id[] invIds = new Id[]{};
		for (SCM_InvoiceExport.ExportedInvoice invoice : expInvoices) {
			invIds.add(invoice.invoiceId);
			invoice.status = 'Exported';
			invoice.message = '';
		}

		SCM_InvoiceExport.flagExported(expInvoices);

		SCMC__Invoicing__c[] refreshedInvoices = [select Id, SCMC__Export_Status__c from SCMC__Invoicing__c where Id in :invIds];
		for (SCMC__Invoicing__c invoice : refreshedInvoices) {
			system.assertEquals('Exported', invoice.SCMC__Export_Status__c, 'unexpected export status');
		}
	}
	
}