@isTest
private class SCMTriggerTests {

	private static SCMSetUpTest st = new SCMSetUpTest();
	
	private static testMethod void testSOInsert() {
		
		Account a = st.createTestCustomerAccount(true, false, false, null);
		a.Carrier__c = 'XYZ';
		a.Carrier_Service__c = 'ABC';
		a.Carrier_Account__c = '123456';
		insert a;
		
		test.startTest();
		
		SCMC__Sales_Order__c so = st.createTestSalesOrder(a, true);
		
		test.stopTest();
		
		SCMC__Sales_Order__c refreshedSO = [select Id
				, Carrier__c
				, Carrier_Service__c
				, Carrier_Account__c
			from SCMC__Sales_Order__c
			where Id = :so.Id];
		
		System.debug('refreshed sales order ' + refreshedSO);
		//system.assertEquals(a.Carrier__c, refreshedSO.Carrier__c, 'unexpected carrier on SO');
		//system.assertEquals(a.Carrier_Service__c, refreshedSO.Carrier_Service__c, 'unexpected carrier service on SO');
		//system.assertEquals(a.Carrier_Account__c, refreshedSO.Carrier_Account__c, 'unexpected carrier acct on SO');
	}
	
	private static testMethod void testShipmentInsert() {
		
		Account a = st.createTestCustomerAccount(true, true, false, null);
		
		Account refresh = [select id
					,Carrier__c
					,Carrier_Service__c
					,Carrier_Account__c 
				from Account
				where id = :a.id];
		System.assertEquals(null, refresh.Carrier__c, 'unexpected carrier on account');
		
		SCMC__Sales_Order__c so = st.createTestSalesOrder(a, false);
		so.Carrier__c = 'XYZ';
		so.Carrier_Service__c = 'ABC';
		so.Carrier_Account__c = '123456';
		insert so;
		
		SCMC__Sales_Order__c refreshedSO = [select id, name
				,Carrier__c
				,Carrier_Service__c
				,Carrier_Account__c
			from SCMC__Sales_Order__c
			where id = :so.id];
		system.assertEquals(so.Carrier__c, refreshedSO.Carrier__c, 'unexpected carrier on SO');
		system.assertEquals(so.Carrier_Service__c, refreshedSO.Carrier_Service__c, 'unexpected carrier service on SO');
		system.assertEquals(so.Carrier_Account__c, refreshedSO.Carrier_Account__c, 'unexpected carrier acct on SO');
		
		test.startTest();
		
		SCMC__Shipping__c ship = new SCMC__Shipping__c(
			SCMC__Sales_Order__c = so.Id
		);
		insert ship;
		
		test.stopTest();
		
		SCMC__Shipping__c refreshedShip = [select Id
				, SCMC__Carrier__c
				, SCMC__Carrier_Service__c
				, Carrier_Account__c
			from SCMC__Shipping__c
			where Id = :ship.Id];
		
		system.assertEquals(so.Carrier__c, refreshedShip.SCMC__Carrier__c, 'unexpected carrier on shipment');
		system.assertEquals(so.Carrier_Service__c, refreshedShip.SCMC__Carrier_Service__c, 'unexpected carrier service on shipment');
		system.assertEquals(so.Carrier_Account__c, refreshedShip.Carrier_Account__c, 'unexpected carrier acct on shipment');
	}

	private static testMethod void testSNRelatedRecordTrigger() {
		
		SCMC__Address__c address = st.createTestAddress();
		SCMC__ICP__c icp = st.createTestICP(address);
		SCMC__Item__c item = st.createTestItem(false);

		list<SCMC__Warehouse__c> warehouses = new list<SCMC__Warehouse__c>();		
		SCMC__Warehouse__c wareHouseA = st.createTestWarehouse(icp, address, false);
		warehouses.add(wareHouseA);
		SCMC__Warehouse__c wareHouseB = st.createTestWarehouse(icp, address, false);
		warehouses.add(wareHouseB);
		insert warehouses;

		SCMC__Reason_Code__c reason = new SCMC__Reason_Code__c();
		reason.Name = 'Test Reasons';
		reason.SCMC__Inactive__c = false;
		upsert reason;

		SCMC__Ownership_Code__c ownership = new SCMC__Ownership_Code__c();
		ownership.Name = 'Test Owner';
		upsert ownership;

		SCMC__Transfer_Request__c transferRequest = new SCMC__Transfer_Request__c();
		transferRequest.SCMC__Reason_Code__c = reason.Id;
		transferRequest.SCMC__Source_Warehouse__c = wareHouseA.Id;
		transferRequest.SCMC__Destination_Warehouse__c = wareHouseB.Id;
		transferRequest.SCMC__Carrier__c = 'Company Truck';
		transferRequest.SCMC__Shipment_Required__c = true;
		upsert transferRequest;

		SCMC__Transfer_Request_Line__c trl = new SCMC__Transfer_Request_Line__c();
		trl.SCMC__Transfer_Request__c = transferRequest.Id;
		trl.SCMC__Quantity__c = 1;
		trl.SCMC__Ownership_Code__c = ownership.Id;
		trl.SCMC__Lot_Number__c = 'Lot A';
		trl.SCMC__Item_Master__c = item.Id;
		upsert trl;

		test.startTest();
			
			SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
			sn.SCMC__Item__c = item.Id;
			sn.SCMC__Serial_Number__c = 'SN123456';
			sn.SCMC__Transfer_Request_Line__c = trl.Id;
			upsert sn;

			SCMC__Serial_Number_Related_Record__c snrr = new SCMC__Serial_Number_Related_Record__c();
			snrr.SCMC__Transfer_Request_Line__c = trl.Id;
			snrr.SCMC__Serial_Number__c = sn.Id;
			upsert snrr;

			SCMC__Serial_Number_Related_Record__c updatedSNRR = [select SCMC__Ownership__c
																	   ,SCMC__Warehouse__c
																   from SCMC__Serial_Number_Related_Record__c
																  where Id = :snrr.Id];
			
			system.assertEquals(ownership.Id, updatedSNRR.SCMC__Ownership__c, 'ownership was not set on the related record');
			system.assertEquals(wareHouseA.Id, updatedSNRR.SCMC__Warehouse__c, 'warehouse was not set on the related record');

		test.stopTest();

	}

}