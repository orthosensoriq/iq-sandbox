@isTest
private class SCM_APVoucherExportTest {

	private static SCM_SetupExportTest st = new SCM_SetupExportTest();
	
	@isTest static void testAPVoucherExport() {
		
		// create test data
		c2g__codaGeneralLedgerAccount__c testGLA = new c2g__codaGeneralLedgerAccount__c(
			Name = 'Accounts Payable'
			, c2g__ReportingCode__c = 'AP'
			, c2g__Type__c = 'Balance Sheet'
            );
		insert testGLA;
		
		SCMC__Unit_of_Measure__c uom = st.createUnitofMeasure('TestUOM', '');
		SCMC__Supplier_Site__c site = st.createTestSupplier();
		SCMC__Warehouse__c wh = st.setupWarehouse();
		SCMC__Item__c item = st.createTestItem();
		
		SCMC__Purchase_Order__c po = st.createPurchaseOrder(site, true);
		SCMC__Purchase_Order_Line_Item__c pol = st.createPurchaseOrderLine(po, item, wh, true, true);

		SCMC__AP_Voucher__c apv = new SCMC__AP_Voucher__c();
		apv.SCMC__Supplier_Site__c = site.Id;
		apv.SCMC__Purchase_Order__c = po.Id;
		apv.SCMC__Invoice_Number__c = 'INV-0001';
		apv.SCMC__Invoice_Date__c = system.today() - 3;
		apv.SCMC__Due_Date__c = system.today() + 27;
		apv.SCMC__Terms__c = 'NET 30';
		apv.SCMC__Export_Status__c = 'New';
		apv.RecordTypeId = SCM_ExportUtilities.getRecordType('Matched', new SCMC__AP_Voucher__c());
		insert apv;
		apv.SCMC__Status__c = 'Matched';
		update apv;

		SCMC__AP_Voucher_Line__c line = new SCMC__AP_Voucher_Line__c(
			SCMC__Purchase_Order_Line_Item__c = pol.Id
			, SCMC__Quantity__c = pol.SCMC__Quantity__c
			, SCMC__Unit_Price__c = pol.SCMC__Unit_Cost__c
			, SCMC__AP_Voucher__c = apv.Id
		);
		insert line;

		SCMC__AP_Voucher__c refreshedVoucher = [select Id, SCMC__Status__c, SCMC__Export_Status__c from SCMC__AP_Voucher__c where Id = :apv.Id];
		system.assertEquals('New', refreshedVoucher.SCMC__Export_Status__c, 'exp status not setup correctly');
		system.assertEquals('Matched', refreshedVoucher.SCMC__Status__c, 'status not setup correctly');

		SCM_APVoucherExport.ExportedAPVoucher[] expVouchers = SCM_APVoucherExport.getAPVouchers();
		system.assertEquals(1, expVouchers.size(), 'unexpected number of vouchers to export');

		Id[] voucherIds = new Id[]{};
		for (SCM_APVoucherExport.ExportedAPVoucher voucher : expVouchers) {
			voucherIds.add(voucher.voucherId);
			voucher.status = 'Exported';
			voucher.message = '';
		}

		SCM_APVoucherExport.flagExported(expVouchers);

		SCMC__AP_Voucher__c[] refreshedVouchers = [select Id, SCMC__Export_Status__c from SCMC__AP_Voucher__c where Id in :voucherIds];
		for (SCMC__AP_Voucher__c voucher : refreshedVouchers) {
			system.assertEquals('Exported', voucher.SCMC__Export_Status__c, 'unexpected export status');
		}
	}
	
}