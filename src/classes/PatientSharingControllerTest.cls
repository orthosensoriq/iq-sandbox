@isTest(SeeAllData=true)
private class PatientSharingControllerTest {

    static testMethod void testPatientSharing() {
        Account newLoc = new Account();
        newLoc.Name = 'Test Hospital';
        insert newLoc;
        
        Account newLoc2 = new Account();
        newLoc2.Name = 'Test Hospital 2';
        insert newLoc2;
        
        Account newPhys = new Account();
        newPhys.FirstName = 'Test';
        newPhys.LastName = 'Doc';
        newPhys.RecordTypeId = [SELECT Id FROM RecordType Where Name = 'Surgeon Account'].Id;
        insert newPhys;
        
        Account newPhys2 = new Account();
        newPhys2.FirstName = 'Test';
        newPhys2.LastName = 'Doc2';
        newPhys2.RecordTypeId = [SELECT Id FROM RecordType Where Name = 'Surgeon Account'].Id;
        insert newPhys2;
        
        
        Patient__c newPatient = new Patient__c();
		newPatient.First_Name__c = 'Test';
		newPatient.Last_Name__c = 'Patient';
		newPatient.Date_Of_Birth__c = System.today();
		newPatient.Gender__c = 'Male';
		insert newPatient;
		
		Case__c newCase = new Case__c();
		newCase.Patient__c = newPatient.Id;
		newCase.Name__c = 'NewCase';
		newCase.Active__c = TRUE;
		insert newCase;
		
		Event__c newEvent = new Event__c();
		newEvent.Case__c = newCase.Id;
		newEvent.Physician__c = newPhys.Id;
		newEvent.Location__c = newLoc.Id;
		insert newEvent;
		
		newEvent.Physician__c = newPhys2.id;
		update newEvent;
		
		newEvent.Location__c = newLoc2.id;
		update newEvent;
		
    }
}