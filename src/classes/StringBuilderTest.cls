@isTest
private class StringBuilderTest{
/* =============================================  TEST METHODS  ============================================= */

    private static testMethod void testStringBuilder1()
    {
        StringBuilder sb = StringBuilder.NewStringBuilder();
        sb.Append('MYRTLE BEACH!');
        System.assertNotEquals(sb.AsString(), '');
        System.assertNotEquals(sb.AsString(), null);
        System.assert(sb.AsString().contains('MYRTLE'));
    }
    
    private static testMethod void testStringBuilder2()
    {
        StringBuilder sb = StringBuilder.NewWithFirstValue('this is a test for orthosensor');
        sb.Append('This is the second line');
        System.assertNotEquals(sb.AsString(), '');
        System.assertNotEquals(sb.AsString(), null);
    }
    
    private static testMethod void testStringBuilder3()
    {
        StringBuilder sb = StringBuilder.NewWithFirstValue('this is a test for orthosensorR');
        sb.Clear();
        System.assertEquals(sb.AsString(), '');
        System.assert(sb.AsString().contains('myrtle') == false);
    }
    
    private static testMethod void testStringBuilder4()
    {
        StringBuilder sb = StringBuilder.NewStringBuilder();
        sb.AppendLine('myrtle');
        sb.Append('beach');
        System.assertNotEquals(sb.AsString(), '');
        System.assertNotEquals(sb.AsString(), null);
        System.assert(sb.AsString().contains('\r\n'));
    }
}