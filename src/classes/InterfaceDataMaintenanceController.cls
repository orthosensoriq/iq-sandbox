global class InterfaceDataMaintenanceController implements Schedulable
{
    
    global void execute(SchedulableContext ctx)
    {
        CleanInterfaceData();
	}

	public boolean CleanInterfaceData()
    {
        list<ADT_Interface__c> interfaceData;
        try
        {
            interfaceData = [Select Id from ADT_Interface__c where appointment_date__c < YESTERDAY];
            if(interfaceData != null && interfaceData.size() > 0)
            {
                delete interfaceData;
            }
            return true;
        }
        catch(exception ex)
        {
            system.debug('An error occurred attempting to retrieve and clear Interface data: ' + ex.getMessage());
            return false;
        }
    }
    
}