@isTest (SeeAllData=true)
private class SensorUtilizationControllerTest {
	
	static testMethod void testSensorUtilizationController() {
		
		//Construct controller
          SensorUtilizationController con = new SensorUtilizationController();

          //Execute code
          con.sensorLines.clear();
          con.getResults();
          con.selectedSensors.add('Lateral Load');
           for (Account a : [SELECT Id FROM Account WHERE RecordType.Name = 'Hospital Account' LIMIT 1]) {
               con.selectedHospitals.add(a.Id);
          }
          for (Account a : [SELECT Id FROM Account WHERE RecordType.Name = 'Surgeon Account' LIMIT 1]) {
               con.selectedSurgeons.add(a.Id);
          }
          con.getResults();
          List<selectOption> hospitalOptions = con.getHospitalItems();
          String[] currHospitals = con.getHospitals();
          List<selectOption> surgeonOptions = con.getSurgeonItems();
          String[] currSurgeons = con.getSurgeons();
          List<selectOption> sensorOptions = con.getSensorItems();
          String[] currSensors = con.getSensorTypes();
	}

}