/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2016 FinancialForce.com, inc. All rights reserved.
*/
@isTest
private with sharing class SCMFFA_InvoiceCreditExportPlugin_Test
{

	@isTest
	private static void NormalSuccessfulTest()
	{
		SCMC__Invoicing__c scmInvoice = (SCMC__Invoicing__c) SCMFFA_SetupTest.createNewSObject(SCMC__Invoicing__c.SObjectType);
		SCMFFA_SetupTest.setId(scmInvoice);

		c2g__codaCreditNote__c creditNote = (c2g__codaCreditNote__c) SCMFFA_SetupTest.createNewSObject(c2g__codaCreditNote__c.SObjectType);
		creditNote.c2g__DerivePeriod__c = true;
		SCMFFA_SetupTest.setId(creditNote);

		Map<Id, SObject> sObjectMap = new Map<Id, SObject>();
		sObjectMap.put(scmInvoice.Id, creditNote);

		SCMFFA_InvoiceCreditExportPlugin plugin = new SCMFFA_InvoiceCreditExportPlugin();
		plugin.execute(sObjectMap, null, null);

		System.assertEquals(
			false,
			creditNote.c2g__DerivePeriod__c,
			'The credit note\'s Derive Period property should be True.');
	}
}