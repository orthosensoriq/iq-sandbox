@isTest
private class SurgeonViewExtensionTest {
	
	static testMethod void testSurgeonViewExtension() {
		
		//Create Contact, Surgeon and Hospital to use for Testing
		Account testSurgeon = new Account();
		testSurgeon.FirstName = 'Test';
		testSurgeon.LastName = 'Surgeon';
		testSurgeon.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Surgeon Account' AND IsPersonType=true].Id;
		insert testSurgeon;

		Account testHospital = new Account();
		testHospital.Name = 'OS Test Hospital';
		testHospital.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Hospital Account'].Id;
		insert testHospital;

		Account testHospital2 = new Account();
		testHospital2.Name = 'OS Test Hospital 2';
		testHospital2.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Hospital Account'].Id;
		insert testHospital2;

		Account testHospital3 = new Account();
		testHospital3.Name = 'OS Test Hospital 3';
		testHospital3.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Hospital Account'].Id;
		insert testHospital3;

		Surgeon_Hospital__c testSurgHosp = new Surgeon_Hospital__c();
		testSurgHosp.Hospital__c = testHospital.Id;
		testSurgHosp.Surgeon__c = [SELECT Id FROM Contact WHERE AccountId = :testSurgeon.Id].Id;
		insert testSurgHosp;

		Surgeon_Hospital__c testSurgHosp2 = new Surgeon_Hospital__c();
		testSurgHosp2.Hospital__c = testHospital2.Id;
		testSurgHosp2.Surgeon__c = [SELECT Id FROM Contact WHERE AccountId = :testSurgeon.Id].Id;
		insert testSurgHosp2;
		
		//Construct controller
		ApexPages.StandardController stdSurgeon = new ApexPages.StandardController(testSurgeon);
        SurgeonViewExtension conExt = new SurgeonViewExtension(stdSurgeon);

        //Execute code
		conExt.LoadHospitals();
		List<String> hospitalList = conExt.getHospitals();
		List<SelectOption> optionItems = conExt.getItems();

		//List<String> newHospitals = new List<String>();

		PageReference pageRef1 = conExt.toggleEditView();
		conExt.Save();
        PageReference pageRef2 = conExt.cancelSurgeonEdit();		

		PageReference pageRef3 = conExt.toggleHospitalSelect();
		PageReference pageRef4 = conExt.saveValues();
        PageReference pageRef5 = conExt.cancelHospitalEdit();

        conExt.deletedHospital = testSurgHosp2.Hospital__c;
        conExt.deleteHospital();
		hospitalList.add(testHospital3.Id);
		conExt.setHospitals(hospitalList);

    }
	
}