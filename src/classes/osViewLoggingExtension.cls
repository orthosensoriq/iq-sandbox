public class osViewLoggingExtension
{
	osController baseCtrl;
    sObject curObject;
    
    public osViewLoggingExtension(ApexPages.StandardController stdController) 
    {
		//baseCtrl = new osController(stdController);
        curObject = stdController.getRecord();
    }
    
    public void LogConstructor()
    {
        baseCtrl = new osController();
     	baseCtrl.getSelectedData(curObject); 
    }
}