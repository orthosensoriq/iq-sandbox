//@author : EnablePath
//@date : 12/18/2013
//@description : class providing public interfaces; software development "best practices" prescribe coding to interfaces INSTEAD of concrete
//               implementations of classes, so these interfaces provide the "contracts" which are coded to.
public with sharing class ITriggerHandler
{
    public virtual interface IBaseTriggerHandler
    {
        /*      
        NOTE: if these event methods are ever needed, they should NOT be uncommented here in "IBaseTriggerHandler" because
        it would impact all classes implementing these interfaces that are already in use.  Instead, put the methods in new
        'child' (derived) interfaces [see below] as needed
        
        void OnAfterInsert(SObject[] newSObjects);
        void OnAfterUpdate(SObject[] oldSObjects, SObject[] updatedSObjects, map<ID, SObject> SObjectMapNew, map<ID, SObject> SObjectMapOld);
        void OnBeforeInsert(SObject[] newSObjects);
        void OnBeforeUpdate(SObject[] oldSObjects, SObject[] updatedSObjects, map<ID, SObject> SObjectMapNew, map<ID, SObject> SObjectMapOld);
        void OnBeforeDelete(SObject[] oldsObjectsToDelete, map<ID, SObject> oldsObjectMap);
        void OnAfterDelete(SObject[] deletedOldSObjects, map<ID, SObject> SObjectOldMap);
        void OnUndelete(SObject[] newRestoredSObjects);
        */
    }
    public interface IAccountTriggerHandler extends IBaseTriggerHandler{        
        void OnBeforeDelete(Account[] AccountsToDelete, map<ID, Account> AccountMapOld);
    }

    public interface IContactTriggerHandler extends IBaseTriggerHandler{        
        void OnBeforeUpdate(SObject[] oldSObjects, SObject[] updatedSObjects, map<ID, SObject> SObjectMapNew, map<ID, SObject> SObjectMapOld);
        void OnBeforeDelete(Contact[] ContactsToDelete, map<ID, Contact> ContactMapOld);
        void OnBeforeInsert(Contact[] newContacts);
        void OnAfterInsert(Contact[] InsertedContacts);
    }
    
    //public interface IPatientTriggerHandler extends IBaseTriggerHandler{
    //    void OnBeforeUpdate(SObject[] oldSObjects, SObject[] updatedSObjects, map<ID, SObject> SObjectMapNew, map<ID, SObject> SObjectMapOld);
    //    void OnBeforeDelete(Patient__c[] PatientsToDelete, map<ID, Patient__c> PatientMapOld);
    //    void OnBeforeInsert(Patient__c[] newPatients);
    //    void OnAfterInsert(Patient__c[] InsertedPatients);
    //}
}