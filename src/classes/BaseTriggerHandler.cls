//@author : EnablePath
//@date : 12/18/2013
//@description : base class for the "*Handler" classes that support triggers; it provides common read-only properties that help
//               determine the context in which code is executing.

global virtual with sharing class BaseTriggerHandler
{
    protected boolean TriggerIsExecuting = false;

    public boolean IsTriggerContext{
        get{ return TriggerIsExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }

    public virtual void OnBeforeInsert(List<SObject> insertObject)
    {
    	logTrigger('Create', null, null, insertObject);
    }
    
    public virtual void OnAfterInsert(Map<Id, SObject> insertObject)
    {
    	logTrigger('Create', insertObject, null, null); 
    }
    
    public virtual void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	logTrigger('Update', beforeObject, afterObject, null);
    }
    
    public virtual void OnAfterUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	logTrigger('Update', beforeObject, afterObject, null);
    }
    
    public virtual void OnBeforeDelete(Map<Id, SObject> deleteObject)
    {
    	logTrigger('Delete', deleteObject, null, null);
    }
    
    public virtual void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
    	logTrigger('Delete', deleteObject, null, null);
    }
    
    public virtual void OnUndelete(List<SObject> undeleteObjects)
    {
    	logTrigger('Create', null, null, undeleteObjects);
    }
    
    public void logTrigger(String action, Map<Id, SObject> beforeData, Map<Id, SObject> afterData, List<SObject> additional)
    {
        logUtils logs = new logUtils();
    	if(afterData != null)
        {
        	logs.logCRUD(action, beforeData, afterData);
        }
        else if(additional != null)
        {
            logs.logCRUD(action, additional);
        }
        else
        {
            logs.logCRUD(action, beforeData);
        }
    }
}