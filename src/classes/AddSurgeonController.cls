public virtual class AddSurgeonController{

	public List<SurgeonLine> surgeonlines {get;set;}
	public String searchFirstName {get;set;}
	public String searchLastName {get;set;}
	public String searchState {get;set;}
	public String newSurgeonFirstName {get; set;} 
	public String newSurgeonLastName {get; set;} 
	public String newSurgeonNPI {get; set;}
	public List<SelectOption> states {get;set;}
	public boolean displayPopup {get; set;}
	public String[] selectedHospitals = new String[]{};
    public List<Account> currHospitals {get; set;}
    list<Surgeon_Hospital__c> InsertSurgeonHospital = new list<Surgeon_Hospital__c>();
    Id personContactId;
    Id newSurgeonId;
	
	public AddSurgeonController(){ 
		
		surgeonlines = new List<SurgeonLine>();
		states = new List<SelectOption>();
		states.add(new SelectOption('', ''));
		states.add(new SelectOption('AL', 'AL'));
		states.add(new SelectOption('AK', 'AK'));
		states.add(new SelectOption('AZ', 'AZ'));
		states.add(new SelectOption('AR', 'AR'));
		states.add(new SelectOption('CA', 'CA'));
		states.add(new SelectOption('CO', 'CO'));
		states.add(new SelectOption('CT', 'CT'));
		states.add(new SelectOption('DE', 'DE'));
		states.add(new SelectOption('FL', 'FL'));
		states.add(new SelectOption('GA', 'GA'));
		states.add(new SelectOption('HI', 'HI'));
		states.add(new SelectOption('ID', 'ID'));
		states.add(new SelectOption('IL', 'IL'));
		states.add(new SelectOption('IN', 'IN'));
		states.add(new SelectOption('IA', 'IA'));
		states.add(new SelectOption('KS', 'KS'));
		states.add(new SelectOption('KY', 'KY'));
		states.add(new SelectOption('LA', 'LA'));
		states.add(new SelectOption('ME', 'ME'));
		states.add(new SelectOption('MD', 'MD'));
		states.add(new SelectOption('MA', 'MA'));
		states.add(new SelectOption('MI', 'MI'));
		states.add(new SelectOption('MN', 'MN'));
		states.add(new SelectOption('MS', 'MS'));
		states.add(new SelectOption('MO', 'MO'));
		states.add(new SelectOption('MT', 'MT'));
		states.add(new SelectOption('NE', 'NE'));
		states.add(new SelectOption('NV', 'NV'));
		states.add(new SelectOption('NH', 'NH'));
		states.add(new SelectOption('NJ', 'NJ'));
		states.add(new SelectOption('NM', 'NM'));
		states.add(new SelectOption('NY', 'NY'));
		states.add(new SelectOption('NC', 'NC'));
		states.add(new SelectOption('ND', 'ND'));
		states.add(new SelectOption('OH', 'OH'));
		states.add(new SelectOption('OR', 'OR'));
		states.add(new SelectOption('PA', 'PA'));
		states.add(new SelectOption('RI', 'RI'));
		states.add(new SelectOption('SC', 'SC'));
		states.add(new SelectOption('SD', 'SD'));
		states.add(new SelectOption('TN', 'TN'));
		states.add(new SelectOption('TX', 'TX'));
		states.add(new SelectOption('UT', 'UT'));
		states.add(new SelectOption('VT', 'VT'));
		states.add(new SelectOption('VA', 'VA'));
		states.add(new SelectOption('WA', 'WA'));
		states.add(new SelectOption('WV', 'WV'));
		states.add(new SelectOption('WI', 'WI'));
		states.add(new SelectOption('WY', 'WY'));
	}

  	public void getContent() {
		
		if(searchLastName == null || searchLastName == '' || searchState == '' || searchState == null)
		{
		  surgeonlines.clear();
		  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter a Lastname value and choose a state.');
		  ApexPages.addMessage(myMsg);
		}
		else
		{
			// Instantiate a new http object
		    Http h = new Http();
		  	surgeonlines.clear();
		  	String API_KEY = 'D6H8smtD4AvmbV2SPHdp';
		  	String endpointString = 'http://api.notonlydev.com/api/index.php?';
		  	endpointString = endpointString + 'apikey=' + API_KEY;
		  	endpointString = endpointString + '&is_person=true&is_address=false&is_org=false&is_ident=false&format=json';
		  	if(searchFirstName!=null) endpointString = endpointString + '&first_name=' + searchFirstName;
		  	if(searchLastName!=null) endpointString = endpointString + '&last_name=' + searchLastName;
		  	if(searchState!=null) endpointString = endpointString + '&state=' + searchState;
		  	
		  	System.debug(LoggingLevel.Error, '---endpointString = ' + endpointString);
		 	// http://api.notonlydev.com/api/index.php?apikey=D6H8smtD4AvmbV2SPHdp&first_name=John&last_name=Smith&org_name=&address=&state=&city_name=&zip=&taxonomy=&ident=&is_person=true&is_address=false&is_org=false&is_ident=false&format=json
			// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
		    HttpRequest req = new HttpRequest();
		    req.setEndpoint(endpointString);
	   		req.setMethod('GET');
			System.debug('---req = ' + req);
			
			// Send the request, and return a response
		    HttpResponse res = h.send(req);
			String response = res.getBody();
		try{
	  		// Parse entire JSON response.
			JSONParser parser = JSON.createParser(response);				
            while (parser.nextToken() != null)
			{
                if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
				{
					//parser.nextToken();
					SurgeonLine surgeon = new SurgeonLine();
					while (parser.nextToken() != null && surgeonlines.size()<1000)
					{
						if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
						{
							String fieldName = parser.getText();
							if(surgeon.first_name != null && surgeon.last_name !=null && surgeon.npi != null && surgeon.state!=null && surgeon.description!=null && surgeon.taxonomy_id!=null)
							{
								surgeonlines.add(surgeon);
								break;	
							}
							else if(fieldName == 'first_name') 
							{
								parser.nextToken();
								surgeon.first_name = parser.getText();
							}
							else if(fieldName == 'last_name')
							{
								parser.nextToken();
								surgeon.last_name = parser.getText();
							}
							else if(fieldName == 'npi')
							{
								parser.nextToken();
								surgeon.npi = parser.getText();	
							}		
							else if(fieldName == 'state')
							{
								parser.nextToken();
								surgeon.state = parser.getText();	
							}		
							else if(fieldName == 'city')
							{
								parser.nextToken();
								surgeon.city = parser.getText();	
							}	
							else if(fieldName == 'description')
							{
								parser.nextToken();
								surgeon.description = parser.getText();	
							}	
							else if(fieldName == 'taxonomy_id')
							{
								parser.nextToken();
								surgeon.taxonomy_id = parser.getText();	
							}		
						}
					}
                }
            }
		}
		catch(Exception ex)
		  {
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Response from web service.');
				ApexPages.addMessage(myMsg);
		  }		
			if(surgeonlines.size()==0 || surgeonlines == null)
			{
			  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results');
			  ApexPages.addMessage(myMsg);
			}
	  	}
	  		
  	}

  	public PageReference AddSurgeon(){

		Account[] existingSurgeons;
		Account existingSurgeon;
		if(newSurgeonNPI!=null)	existingSurgeons = [Select Id, PersonContactId from Account where NPI__c = :newSurgeonNPI LIMIT 1];
		if(existingsurgeons!=null && existingsurgeons.size()>0) existingSurgeon = existingSurgeons[0];

		Account newSurgeon = new Account();
		if (existingSurgeon == null)
		{
			newSurgeon.NPI__c = newSurgeonNPI;
			newSurgeon.FirstName = newSurgeonFirstName;
			newSurgeon.LastName = newSurgeonLastName;
			newSurgeon.BillingState = searchState;
			newSurgeon.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Surgeon Account'].Id;	
			insert newSurgeon;
			
			newSurgeon = [Select Id, Name, PersonContactId from Account where Id=:newSurgeon.Id];
			personContactId = newSurgeon.PersonContactId;
			newSurgeonId = newSurgeon.Id;
		}
		else
		{
			newSurgeon = existingSurgeon;
			personContactId = newSurgeon.PersonContactId;
			newSurgeonId = newSurgeon.Id;	
		}

		showPopup();
		
		//PageReference pageRef = new PageReference('/apex/SurgeonListing?sfdc.tabName=01rZ00000004k66');
		//pageRef.setRedirect(true);
        //return pageRef;
        return null;
	}

	public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }

    public List<selectOption> getitems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE RecordType.Name = 'Hospital Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
	}

	public String[] getHospitals() {
        return selectedHospitals;
    }
    
	public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }

    public PageReference saveValues() {
        InsertSurgeonHospital.clear();
        for (Integer i=0; i < selectedHospitals.size(); i++) {
            Surgeon_Hospital__c newSurgeonHospital = new Surgeon_Hospital__c();
            newSurgeonHospital.Hospital__c = selectedHospitals[i];
            newSurgeonHospital.Surgeon__c = personContactId;
            InsertSurgeonHospital.add(newSurgeonHospital);
        }
        if (InsertSurgeonHospital.size() > 0) insert InsertSurgeonHospital;
        PageReference varPage = Page.SurgeonView;
        varPage.getParameters().put('id',newSurgeonId);
        varPage.SetRedirect(TRUE);
        return varPage;
    }
}