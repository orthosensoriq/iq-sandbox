/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Provides functionality for operations with SCMC__Purchase_Order_Line_Item__c
**/
public with sharing class SCMPurchaseOrderLineItem {

	private static map<string, id> recordTypeNameToId = null;
	public static map<string, id> getRecordTypeIds(){
		if (recordTypeNameToId == null){
			recordTypeNameToId = new map<string, id>();
			for (RecordType rt : [select id, DeveloperName from recordtype where sobjecttype='SCMC__Purchase_Order_Line_Item__c'])
				recordTypeNameToId.put(rt.developerName, rt.Id);
		}
		return recordTypeNameToId;
	}
	
}