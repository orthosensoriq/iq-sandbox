global with sharing class SCM_GLExport {
	
	global with sharing class ExportedGLTransaction {
		Webservice string financialSystemTrans;
		webservice string glAccount;
		webservice string icpCurrency;
		webservice double icpUnitCost;
		webservice double icpValueAmount;
		webservice string invTransId;
		webservice string message;
		webservice string QBAPAccount;
		webservice string memo;
		webservice double quantity;
		webservice string status;
		webservice Date transDate;
		webservice Id transId;
		webservice Id finTransId;
		webservice string transName;
	}

	Webservice static ExportedGLTransaction[] getGLTransactions() {

		SCMC__Inventory_Transaction_Financial_Records__c[] transactions = [select Id
				, SCMC__Inventory_Transaction__c
				, SCMC__Inventory_Transaction__r.name
				, SCMC__Inventory_Transaction__r.SCMC__Inventory_Transactions__c
				, SCMC__Inventory_Transaction__r.SCMC__Reason_Code__r.name
				, SCMC__GL_Account__c
				, SCMC__ICP_Value_Amount__c
				, CreatedDate
			from SCMC__Inventory_Transaction_Financial_Records__c
			where SCMC__Export_Status__c = 'New'
				and SCMC__ICP_Value_Amount__c != 0];

		Map<string, string> glAcctMap = getGLAccountMapping();

		ExportedGLTransaction[] expTransactions = new ExportedGLTransaction[]{};

		for (SCMC__Inventory_Transaction_Financial_Records__c trans : transactions) {

			trans.SCMC__Export_Status__c = 'Export in Process';

			ExportedGLTransaction glTrans = new ExportedGLTransaction();

			glTrans.transId = trans.Id;
			glTrans.invTransId = trans.SCMC__Inventory_Transaction__c;
			glTrans.transDate = Date.newInstance(trans.CreatedDate.year(), trans.CreatedDate.month(), trans.CreatedDate.day());
			glTrans.icpValueAmount = trans.SCMC__ICP_Value_Amount__c;
			glTrans.memo = trans.SCMC__Inventory_Transaction__r.SCMC__Inventory_Transactions__c + ' - ' + trans.SCMC__Inventory_Transaction__r.SCMC__Reason_Code__r.name;

			// try to determine account mapping
			if (glAcctMap.containsKey(trans.SCMC__GL_Account__c)) {
				glTrans.glAccount = glAcctMap.get(trans.SCMC__GL_Account__c);
			} else {
				glTrans.glAccount = trans.SCMC__GL_Account__c;
			}

			expTransactions.add(glTrans);
		}

		update transactions;
		return expTransactions;
	}

	Webservice static void flagExported(ExportedGLTransaction[] transactions) {
		Map<Id, SCMC__Inventory_Transaction_Financial_Records__c> transIdToTrans = new Map<Id, SCMC__Inventory_Transaction_Financial_Records__c>();
    	for (ExportedGLTransaction trans : transactions) {
    		transIdToTrans.put(trans.transId, null);	
    	}
    	List<SCMC__Inventory_Transaction_Financial_Records__c> updTransactions = [select id, name
    			,SCMC__Export_Status__c
    			,SCMC__Export_Error__c
    			,SCMC__Foreign_Transaction_Number__c
    		from SCMC__Inventory_Transaction_Financial_Records__c
    		where Id in :transIdToTrans.keyset()];

    	for (SCMC__Inventory_Transaction_Financial_Records__c updTransaction : updTransactions) {
    		transIdToTrans.put(updTransaction.Id, updTransaction);
    	}

    	for (ExportedGLTransaction trans : transactions) {
    		SCMC__Inventory_Transaction_Financial_Records__c updTrans = transIdToTrans.get(trans.transId);
    		updTrans.SCMC__Export_Status__c = trans.status;
    		updTrans.SCMC__Export_Error__c  = trans.message;
    		updTrans.SCMC__Foreign_Transaction_Number__c = trans.financialSystemTrans;
    	}
    	update updTransactions;
	}

	public static Map<string, string> getGLAccountMapping() {
		SCMC__GL_Account__c[] glAccts = [Select SCMC__SCM_Account__c, SCMC__Financial_GL_Account__c From SCMC__GL_Account__c];
		Map<string, string> glAcctMap = new Map<string, string>();
		
		for(SCMC__GL_Account__c glAcct : glAccts) {
			glAcctMap.put(glAcct.SCMC__SCM_Account__c, glAcct.SCMC__Financial_GL_Account__c);
		}
		
		return glAcctMap;
	} 

}