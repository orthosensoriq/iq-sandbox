public with sharing class SensorUtilizationController {
	
	public String[] selectedHospitals = new String[]{};
    public String[] selectedSurgeons = new String[]{};
    public String[] selectedSensors = new String[]{};
    public List<Sensor_Data__c> sensorLines {get;set;}
	public boolean showExport {get;set;}
    public string searchParams {get;set;}

	public SensorUtilizationController() {
		sensorLines = new List<Sensor_Data__c>();
        showExport = false;
        searchParams = '';
	}

	public void getResults() 
    {
        searchParams = '';
        sensorLines.clear();
        
        if(selectedHospitals.size() == 0 && selectedSurgeons.size() == 0 && selectedSensors.size() == 0)
        {
            showExport = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter at Least one search criteria value');
            ApexPages.addMessage(myMsg);
        }
        else
        {
            showExport = true;
            String searchQuery = 'SELECT Id, Name, Type__c, Value__c, Measurement_Date__c, DateTime__c, Event__c, Event__r.Name,';
            searchQuery = searchQuery + ' ImageFileName__c, Imported_Procedure_Values_Id__c, Event__r.Location__r.Name, Event__r.Physician__r.Name';
            searchQuery = searchQuery + ' FROM Sensor_Data__c WHERE IsDeleted = False';


            if (selectedHospitals.size() > 0) {
                String hospitalIDs = '\'' + String.join(selectedHospitals,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Event__r.Location__c IN ('+hospitalIDs+')';

                set<string> tempSet = new set<string>(selectedHospitals);
                searchParams +=  'Hospital in (';
                for(selectOption hosp : getHospitalItems())
                {
                    if(tempSet.contains(hosp.getValue()))
	                    searchParams += hosp.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedSurgeons.size() > 0) {
                String surgeonIDs = '\'' + String.join(selectedSurgeons,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Event__r.Physician__c IN ('+surgeonIDs+')';
                
                set<string> tempSet = new set<string>(selectedSurgeons);
                searchParams +=  'Surgeon in (';
                for(selectOption surg : getSurgeonItems())
                {
                    if(tempSet.contains(surg.getValue()))
	                    searchParams += surg.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedSensors.size() > 0) {
                String typeIDs = '\'' + String.join(selectedSensors,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Type__c IN ('+typeIDs+')';
                
                set<string> tempSet = new set<string>(selectedSensors);
                searchParams +=  'Sensor in (';
                for(selectOption sensor : getSensorItems())
                {
                    if(tempSet.contains(sensor.getValue()))
	                    searchParams += sensor.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            searchQuery = searchQuery + ' ORDER BY Name LIMIT 1000';
            system.debug('searchQuery =====> '+searchQuery);
            sensorLines = Database.query(searchQuery);

            if(sensorLines.size()==0 || sensorLines == null)
            {
                showExport = false;
              	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results were found');
              	ApexPages.addMessage(myMsg);
            }
            searchParams = searchParams.substringBeforeLast(',');
        }
    }

	public String[] getHospitals() {
        return selectedHospitals;
    }

    public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }

    public String[] getSurgeons() {
        return selectedSurgeons;
    }

    public void setSurgeons(String[] surgeons) {
        this.selectedSurgeons = surgeons;
    }

    public String[] getSensorTypes() {
        return selectedSensors;
    }

    public void setSensorTypes(String[] sensorTypes) {
        this.selectedSensors = sensorTypes;
    }

    public List<selectOption> getHospitalItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE isPartner = true and RecordType.Name = 'Hospital Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }

    public List<selectOption> getSurgeonItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE RecordType.Name = 'Surgeon Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }

    public List<selectOption> getSensorItems() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult typeFieldDescription = Sensor_Data__c.Type__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : typeFieldDescription.getPicklistValues()) {
            options.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return options;
    }


}