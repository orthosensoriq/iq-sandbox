public class SetupObjectTriggerHandler{
    @future
    public static void logSetupUpdate(string action, string beforeJSON, string afterJSON, string sobjectType)
    {
        Type t = Type.forName('List<' + sobjectType + '>');
        List<SObject> beforeObjs = (List<SObject>)JSON.deserialize(beforeJSON, t);
        List<sObject> afterObjs = (List<sObject>)JSON.deserialize(afterJSON, t);
        
        map<Id,SObject> beforeSend = new map<Id,sObject>();
        for(sObject beforeObj : beforeObjs)
        {
            beforeSend.put(beforeObj.id, beforeObj);
        }
        
        map<Id,SObject> afterSend = new map<Id,sObject>();
        for(sObject afterObj : afterObjs)
        {
            afterSend.put(afterObj.id, afterObj);
        }
		
        BaseTriggerHandler handler = new BaseTriggerHandler();
        handler.logTrigger(action, beforeSend, afterSend, null);
    }
}