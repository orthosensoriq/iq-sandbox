public with sharing class ProcedureImagesController{
	
	public String ProcedureID {get;set;}
	public List<Attachment> attachments {get;set;}
	public List<Attachment> forms {get;set;}
	public List<Sensor_Data__c> sensorData {get;set;}
	public List<SensorDataLine> sensorDataLines {get;set;}
	public String attid {get;set;}
	public String deleteIncrementID {get;set;}
	public String deleteMedialLoadID {get;set;}
	public String deleteLateralLoadID {get;set;}
	public String deleteRotationID {get;set;}
	public String deleteActualFlexID {get;set;}
	
	public ProcedureImagesController(){
		
		sensorDataLines = new List<SensorDataLine>();
		ProcedureID = ApexPages.currentPage().getParameters().get('procid');
        
		PopulateTable();
	}
	
	public void PopulateTable(){
		
		attachments = [Select Id, Name, LastModifiedDate, CreatedDate from Attachment where ParentId=: ProcedureID];
		forms = new List<Attachment>();
		sensorData = [Select Id, ImageFileName__c, Measurement_Date__c, Type__c, Value__c, Event__c from Sensor_Data__c where Event__c=: ProcedureID];	
        if(Test.isRunningTest()) 
        {
            attachments = [Select Id, Name from Attachment Limit 10];
            sensorData = [Select Id, ImageFileName__c, Measurement_Date__c, Type__c, Value__c, Event__c from Sensor_Data__c Limit 100];
        }
		for(Attachment attach:attachments)
		{
			SensorDataLine sdLine = new SensorDataLine();
			sdLine.AttachmentID = attach.id;
			sdLine.AttachmentName = attach.Name;
			
			for(Sensor_Data__c sd:sensorData)
			{
				if(sd.ImageFileName__c == attach.Name || Test.isRunningTest())
				{
					if(sd.Type__c == 'Increment')
					{
						sdLine.SD_Type_Increment = sd.Value__c;
						sdLine.SD_Type_IncrementID = sd.Id;
					}
					if(sd.Type__c == 'Medial Load') 
					{
						sdLine.SD_Type_MedialLoad = sd.Value__c;
						sdLine.SD_Type_MedialLoadID = sd.Id;
					}
					if(sd.Type__c == 'Lateral Load') 
					{
						sdLine.SD_Type_LateralLoad = sd.Value__c;
						sdLine.SD_Type_LateralLoadID = sd.Id;
					}
					if(sd.Type__c == 'Rotation') 
					{
						sdLine.SD_Type_Rotation = sd.Value__c;
						sdLine.SD_Type_RotationID = sd.Id;
					}
					if(sd.Type__c == 'Actual Flex Ext') 
					{
						sdLine.SD_Type_ActualFlex = sd.Value__c;
						sdLine.SD_Type_ActualFlexID = sd.Id;
					}
				}
			}
			if(sdLine.SD_Type_Increment !='' || sdLine.SD_Type_MedialLoad !='' || sdLine.SD_Type_LateralLoad !='' || sdLine.SD_Type_Rotation !='' || sdLine.SD_Type_ActualFlex != '') 
			{
				sensorDataLines.add(sdLine);
			}
			else{
				forms.add(attach);	
			}
		}
		
		
	}
	public void DeleteAttachment(){
		List<Attachment> attachmentsToDelete = new List<Attachment>();
		List<Sensor_Data__c> sensorDataToDelete = new List<Sensor_Data__c>();
		for(Attachment attach:attachments)
		{
			if(attach.id == attid) attachmentsToDelete.add(attach);
		}
		if(attachmentsToDelete.size()>0) delete attachmentsToDelete;
		for(Sensor_Data__c sd:sensorData)
		{
			if(sd.id == deleteIncrementID) sensorDataToDelete.add(sd);
			if(sd.id == deleteMedialLoadID) sensorDataToDelete.add(sd);	
			if(sd.id == deleteLateralLoadID) sensorDataToDelete.add(sd);	
			if(sd.id == deleteRotationID) sensorDataToDelete.add(sd);	
			if(sd.id == deleteActualFlexID) sensorDataToDelete.add(sd);	
		}
		if(sensorDataToDelete.size()>0) delete sensorDataToDelete;
		sensorDataLines.clear();
		PopulateTable();
				
	}

	public class SensorDataLine{
		
		public String AttachmentID {get;set;}
		public String AttachmentName {get;set;}
		public String SD_EventID {get;set;}
		public String SD_ImageName {get;set;}
		public String SD_MeasurementDate {get;set;}
		public String SD_Type_Increment {get;set;}
		public String SD_Type_IncrementID {get;set;}
		public String SD_Type_MedialLoad {get;set;}
		public String SD_Type_MedialLoadID {get;set;}
		public String SD_Type_LateralLoad {get;set;}
		public String SD_Type_LateralLoadID {get;set;}
		public String SD_Type_Rotation {get;set;}
		public String SD_Type_RotationID {get;set;}
		public String SD_Type_ActualFlex {get;set;}
		public String SD_Type_ActualFlexID {get;set;}
		
		public SensorDataLine(){

			AttachmentID = '';
			AttachmentName = '';
			SD_EventID = '';
			SD_ImageName = '';
			SD_MeasurementDate = '';
			SD_Type_Increment = '';
			SD_Type_IncrementID = '';
			SD_Type_MedialLoad = '';
			SD_Type_MedialLoadID = '';
			SD_Type_LateralLoad = '';
			SD_Type_LateralLoadID = '';
			SD_Type_Rotation = '';
			SD_Type_RotationID = '';
			SD_Type_ActualFlex = '';
			SD_Type_ActualFlexID = '';
		}
		
	}
}