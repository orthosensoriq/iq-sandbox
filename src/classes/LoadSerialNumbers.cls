public with sharing class LoadSerialNumbers {

	private ApexPages.StandardSetController setController	{get; set;}

	public list<Load_Serial_Numbers__c> snList { get; set; }
	public string splitSerialNumber { get; set; }
	
	public LoadSerialNumbers(ApexPages.StandardSetController controller){
		this.setController = controller;
	}

	public void clearLoadObject(){
		ClearLoadSNObjectBatch clearBatch = new ClearLoadSNObjectBatch();
		Database.executeBatch(clearBatch, 200);
	}

	public void loadSNControlNumbers(){
		LoadInventorySNBatch loadSNBatch = new LoadInventorySNBatch();
		Database.executeBatch(loadSNBatch, 500);
	}
	
	public void loadIOSerialNumbers(){
		LoadInboundOutboundSNBatch loadSNBatch = new LoadInboundOutboundSNBatch();
		Database.executeBatch(loadSNBatch, 200);
	}
	
	public PageReference goBackList(){
		return this.setController.cancel();
	}

}