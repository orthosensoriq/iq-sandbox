/*
    Copyright (c) 2013 LessSoftware.com, inc.
    All rights reserved.

	Support for triggers on the Shipment object

*/
public with sharing class SCMShipmentTriggerHandler
{

	public void shipmentAfterUpdate(List<SCMC__Shipping__c> oldShips, List<SCMC__Shipping__c> newShips)
	{

		Map<Id, SCMC__Shipping__c> completeShippingMap = new Map<Id, SCMC__Shipping__c>();

		for (integer i = 0; i < newShips.size(); i++) {
			if ('Shipment complete'.equals(newShips[i].SCMC__Status__c) && !oldShips[i].SCMC__Status__c.equals(newShips[i].SCMC__Status__c)) {
					//collect recently completed shipments to create invoice in FF
					completeShippingMap.put(newShips[i].Id, newShips[i]);
				}
			}

		if (!completeShippingMap.keySet().isEmpty()) {
			// Only process those with related Sales Order.  Other shipping activity may occur,
			// which should not be treated as the sale of a Fixed Asset.
			List<SCMC__Shipping__c> shipmentsWithRelatedSalesOrder = new List<SCMC__Shipping__c>();
			for (SCMC__Shipping__c item : completeShippingMap.values()) {
				if (item.SCMC__Sales_Order__c != null)
					shipmentsWithRelatedSalesOrder.add(item);
		}
			SCMFixedAsset.updateFixedAssetsWhenShipped(shipmentsWithRelatedSalesOrder);
		}
	}

	public void OnBeforeInsert(SCMC__Shipping__c[] newShipments)
	{
		Id[] soIds = new Id[]{};

		for (SCMC__Shipping__c newShipment : newShipments) {
			soIds.add(newShipment.SCMC__Sales_Order__c);
		}

		Map<Id, SCMC__Sales_Order__c> soIdToSO = new Map<Id, SCMC__Sales_Order__c>([select Id
				, Carrier__c
				, Carrier_Service__c
				, Carrier_Account__c
			from SCMC__Sales_Order__c
			where Id in :soIds]);

		for (SCMC__Shipping__c newShipment : newShipments) {
			SCMC__Sales_Order__c so = soIdToSO.get(newShipment.SCMC__Sales_Order__c);
			if (so != null){
				newShipment.SCMC__Carrier__c = so.Carrier__c;
				newShipment.SCMC__Carrier_Service__c = so.Carrier_Service__c;
				newShipment.Carrier_Account__c = so.Carrier_Account__c;
			}
		}
	}
}