/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/
global with sharing class SCMCreateInventorySnapShotUM implements Schedulable, Database.Batchable<SObject>, Database.Stateful{
 
 	private DateTime ssDateTime = System.now();   
	private Set<String> fnames = new Set<String>();
 	private String startsWith;    

    global void execute(SchedulableContext ctx)
    {       
        // Start Batch Apex job to pull requests and create requisitions
        Database.executeBatch(new SCMCreateInventorySnapShotUM());              
    }
    
    global Database.QueryLocator start(Database.BatchableContext ctx)
    {
 		Map<String, Schema.SObjectField> ipFields = Schema.SObjectType.SCMC__Month_End_Inventory__c.fields.getMap();
		startsWith = 'scmc__ip_';
		String queryPosn = 'select id';
		for(String fname : ipFields.Keyset()){
            SObjectField field = ipFields.get(fname);
            Schema.DescribeFieldResult f = field.getDescribe();
			//make sure all columns are included in the select
			if (fname != 'id' &&
				fname != 'ownerid' &&
				!fname.startsWith(startsWith) &&
				fname != 'scmc__snapshot_date_time__c' )
			{
                String qname = f.getName();
     			queryPosn += ', ' + qname;
			}
			//now create set of fields to set
			if (f.isCreateable())
			{
				fnames.add(fname);
			}
				  
		}
		queryPosn += ' from ';
		queryPosn += 'SCMC__Inventory_Position__c' ;
        if (test.isRunningTest()) {
			queryPosn += ' limit 5';
		}        
		System.debug('query positions for for snapshot ' + queryPosn);
    	return Database.getQueryLocator( queryPosn);
    }
    
   
    global void execute(Database.BatchableContext ctx, List<SObject> records)
    {
    	//create a month end record for each inventory position
    	//and then insert all inventory positions
    	List<SCMC__Month_End_Inventory__c>sshots = new List<SCMC__Month_End_Inventory__c>();
    	for (SObject record : records){
    		SCMC__Inventory_Position__c posn = (SCMC__Inventory_Position__c)record;
    		SCMC__Month_End_Inventory__c sshot = new SCMC__Month_End_Inventory__c();
	   		sshot.SCMC__Snapshot_Date_Time__c = ssDateTime;
			sshot.SCMC__IP_Modified_DateTime__c = posn.LastModifiedDate;
			sshot.SCMC__IP_Modified_By__c = posn.LastModifiedById;
			sshot.SCMC__IP_Created_DateTime__c = posn.CreatedDate;
			sshot.SCMC__IP_Created_By__c = posn.CreatedById;
			for(String fname : fnames){
				if (fname != 'ownerid' &&
					!fname.startsWith(startsWith) &&
					fname != 'scmc__snapshot_date_time__c' )
				{
					sshot.put(fname,posn.get(fname));
				}
	    	}
    		sshots.add(sshot);
    	}
		System.debug('sshots ' + sshots);
    	insert sshots;
    }
    
    global void finish(Database.BatchableContext ctx)
    {
    }
}