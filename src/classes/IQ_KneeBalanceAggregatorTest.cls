@isTest(SeeAllData=true)
public class IQ_KneeBalanceAggregatorTest {

    Date eventStartDate = Date.newInstance(2016,11, 1);
    Date eventEndDate = Date.newInstance(2016,12, 31);

    static IQ_KneeBalanceAggregator setup() {
		IQ_KneeBalanceAggregator iq = new IQ_KneeBalanceAggregator();
        Date eventStartDate = Date.newInstance(2016,11, 1);
    	Date eventEndDate = Date.newInstance(2016,12, 31);
        iq.startDate = Date.newInstance(2016,11, 1);
        iq.endDate = Date.newInstance(2016,11, 1);
        iq.accts = iq.GetParents();
        return iq;
    }

    @isTest static void IQ_KneeBalancePropertyTest() {
        IQ_KneeBalanceAggregator repo = setup();
       
        repo.startDate = Date.newInstance(2016,11, 1);
        repo.endDate = Date.newInstance(2016,11, 1);
        repo.monthsToAnalyze = 4;
        repo.firstDayOfFirstMonth = Date.newInstance(2016,11, 1);
        repo.lastDayofLastMonth = Date.newInstance(2016,11, 1);
        repo.monthsInThePast = 4;
        repo.accts  = new List<Surgeon_Hospital__c>();
        // repo.sensorDataLines  = new List<SensorDataLine>();
    }
    @isTest
    static void GetSensorDataLinesTest() {
        IQ_KneeBalanceAggregator repo = setup();

        List<AggregateResult> sensors = repo.GetSensorDataLines(repo.startDate, repo.endDate);
        System.debug(sensors);
        System.Assert(sensors.size() == 0);
    }

    @isTest
    static void AggregateMonthKneeBalanceDataTest() {
        IQ_KneeBalanceAggregator repo = setup();
		repo.AggregateMonthKneeBalanceData(11, 2016);
		// System.debug(allSensors);
		// System.assert(allSensors.size() > 0);
    }

    @isTest
    static void AggregateKneeBalanceDataTest() {
        IQ_KneeBalanceAggregator repo = setup();
		repo.AggregateKneeBalanceData(12, 2016, 2, 2017);
		//query database??
	}

    @isTest static void GetAllMonthlySensorDataTest() {
        IQ_KneeBalanceAggregator repo = setup();
        List<IQ_MonthlySensorData__c> data = repo.GetAllMonthlySensorData();
    }

    @isTest static void GetDegreeByEventTest() {
        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
        List<SensorDataLine> allSensors = new List<SensorDataLine>();
        Date eventStartDate = Date.newInstance(2016, 5, 1);
        Date eventEndDate = eventStartDate.AddDays(Date.daysInMonth(2016, 12)-1);
        List<AggregateResult> sensorDataLines = repo.GetSensorDataLines(eventStartDate, eventEndDate);
        System.debug(sensorDataLines);
        List<Surgeon_Hospital__c> accts = repo.GetParents();
        System.debug(accts);
        List<Event__c> events = repo.GetEventDetails(11, 2017);
        
        // retrieve monthly list of surgeon procedures
        for (AggregateResult ar: sensorDataLines) {
            System.debug((string)(ar.get('Event__c')));
            Event__c evt = repo.GetEventDetails((string)(ar.get('Event__c')), events);
            System.debug(evt);
            if (evt.Id != null) {
                List<SensorDataLine> sensors = repo.LoadSensorDataLines(evt.Id);
                System.debug(sensors);
                if (sensors[0].SD_EventID != null) {
                    Event__c event = repo.GetEventDetails(evt.Id, events);
                    SensorDataLine dataLine = repo.GetDegreeByEvent('10', sensors, event);
                }
            }
            break;
        }
    }

    @isTest static void GetParentsTest() {
        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
         List<Surgeon_Hospital__c> accts = repo.GetParents();
    }

    @isTest static void GetParentTest() {
        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
        List<Surgeon_Hospital__c> accts = repo.GetParents();
        repo.accts = accts;
         if (repo.accts.size() > 0) {
            repo.GetParent(repo.accts[0].Id);
         }
    }

    @isTest static void LoadSensorDataLinesTest() {
        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
        List<SensorDataLine> allSensors = new List<SensorDataLine>();
        Date eventStartDate = Date.newInstance(2016, 5, 1);
        Date eventEndDate = eventStartDate.AddDays(Date.daysInMonth(2016, 12)-1);
        List<AggregateResult> sensorDataLines = repo.GetSensorDataLines(eventStartDate, eventEndDate);
        System.debug(sensorDataLines);
        List<Surgeon_Hospital__c> accts = repo.GetParents();
        System.debug(accts);
        Test.startTest();
        // retrieve monthly list of surgeon procedures
        for (AggregateResult ar: sensorDataLines) {
            System.debug((string)(ar.get('Event__c')));
            List<SensorDataLine> dataLines = repo.LoadSensorDataLines((string)(ar.get('Event__c')));
            break;
        }
        Test.stopTest();
     }

     @isTest static void SetDatesTest() {
        IQ_KneeBalanceAggregator repo = setup();
        Test.startTest();
        repo.SetDates();
        Test.stopTest();
     }
}