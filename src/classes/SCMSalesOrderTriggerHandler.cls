public with sharing class SCMSalesOrderTriggerHandler {

	private boolean isExecuting = false;
	private integer batchSize = 0;

	public SCMSalesOrderTriggerHandler(boolean isExecuting, integer size) {
		this.isExecuting = isExecuting;
		this.batchSize = size;
	}

	public void OnBeforeInsert(SCMC__Sales_Order__c[] newOrders) {
		Id[] acctIds = new Id[]{};
		String[] custPONumbers = new String[]{};
		List<SCMC__Sales_Order__c> soList = new List<SCMC__Sales_Order__c>();
		
		for (SCMC__Sales_Order__c newOrder : newOrders) {
			acctIds.add(newOrder.SCMC__Customer_Account__c);
			if(newOrder.SCMC__Customer_Purchase_Order__c != null && 
			   newOrder.SalesOrderAlreadyExist__c == false){
				custPONumbers.add(newOrder.SCMC__Customer_Purchase_Order__c);
				soList.add(newOrder);
			} else {
				newOrder.SCMC__Customer_Purchase_Order__c = null;
			}
		}

		if(soList.size() > 0){
			this.checkCustomerPurchaseOrderNumbers(acctIds, custPONumbers, soList);
		}
		
		Map<Id, Account> acctIdToAcct = new Map<Id, Account>([select Id
				, Carrier__c
				, Carrier_Service__c
				, Carrier_Account__c
				, OwnerId
			from Account
			where Id in :acctIds]);
		
       	QueueSobject qName = [Select Id,queueId
                  From QueueSobject where SobjectType ='SCMC__Sales_Order__c' 
                  and Queue.DeveloperName ='Sales_Order_Team'];
		for (SCMC__Sales_Order__c newOrder : newOrders) {
			Account a = acctIdToAcct.get(newOrder.SCMC__Customer_Account__c);
			
			if (newOrder.Carrier__c == null){
				newOrder.Carrier__c = a.Carrier__c;
			}
			if (newOrder.Carrier_Service__c == null){
				newOrder.Carrier_Service__c = a.Carrier_Service__c;
			}
			if (newOrder.Carrier_Account__c == null){
				newOrder.Carrier_Account__c = a.Carrier_Account__c;
			}
			//set ownership to a queue
          	newOrder.OwnerId = qName.queueId;
          	newOrder.SalesOrderAlreadyExist__c = true;
          	
          	//set primary sales rep
          	if (newOrder.SCMC__Primary_Sales_Rep__c == null){
          		newOrder.SCMC__Primary_Sales_Rep__c = a.OwnerId;
          	}
		}		
		system.debug('new orders ' + newOrders);		
	}

	public void OnBeforeUpdate(SCMC__Sales_Order__c[] newOrders, SCMC__Sales_Order__c[] oldOrders) {
		
		Id[] acctIds = new Id[]{};
		String[] custPONumbers = new String[]{};
		List<SCMC__Sales_Order__c> soList = new List<SCMC__Sales_Order__c>();

		for(integer i = 0; i < batchSize; i++){
			SCMC__Sales_Order__c newSO = newOrders[i];
			SCMC__Sales_Order__c oldSO = oldOrders[i];
			if(newSO.SCMC__Customer_Purchase_Order__c != null &&
			   newSO.SCMC__Customer_Purchase_Order__c != oldSO.SCMC__Customer_Purchase_Order__c){
				acctIds.add(newSO.SCMC__Customer_Account__c);
				custPONumbers.add(newSO.SCMC__Customer_Purchase_Order__c);
				system.debug('+++++ key A: ' + newSO.SCMC__Customer_Account__c + '_' + newSO.SCMC__Customer_Purchase_Order__c);
				soList.add(newSO);
			}
		}

		if(soList.size() > 0){
			this.checkCustomerPurchaseOrderNumbers(acctIds, custPONumbers, soList);
		}

	}

	private void checkCustomerPurchaseOrderNumbers(Id[] acctIds, String[] custPONumbers, SCMC__Sales_Order__c[] soList){
		SCMC__Sales_Order__c[] tempSO = [select  Id, Name
												,SCMC__Customer_Account__c
												,SCMC__Customer_Account__r.Name
												,SCMC__Customer_Purchase_Order__c
											from SCMC__Sales_Order__c
										   where SCMC__Customer_Account__c In :acctIds
										     and SCMC__Customer_Purchase_Order__c != null
										     and SCMC__Customer_Purchase_Order__c In :custPONumbers
										order by SCMC__Customer_Account__c];

		map<string, SCMC__Sales_Order__c> keyToSODetailMap = new map<string, SCMC__Sales_Order__c>();
		for(SCMC__Sales_Order__c so : tempSO){
			string key = so.SCMC__Customer_Account__c + '_' + so.SCMC__Customer_Purchase_Order__c;
			keyToSODetailMap.put(key, so);
		}

		for(SCMC__Sales_Order__c updatedList : soList){
			if(updatedList.SCMC__Customer_Purchase_Order__c != null){
				string key = updatedList.SCMC__Customer_Account__c + '_' + updatedList.SCMC__Customer_Purchase_Order__c;
				if(keyToSODetailMap.containsKey(key)){
					SCMC__Sales_Order__c currSO = keyToSODetailMap.get(key);
					updatedList.SCMC__Customer_Purchase_Order__c.addError(' The customer purchase order number ' + currSO.SCMC__Customer_Purchase_Order__c + ' already exist on ' + currSO.Name + ' for ' + currSO.SCMC__Customer_Account__r.Name + '.');
				}
			}

		}	
	}	
}