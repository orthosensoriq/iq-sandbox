/* Human.txt Jamie Smith - Coastal Cloud - jamie.smith@coastalcloud.us */
global without sharing class CreateTransferRequest{
    
    webservice static String mkTransferRequest(Id repId) {
        
        Integer i = 0;
        String returnMsg;
        Decimal haveQty = 0;
        Decimal wantQty = 0;
        Decimal resultQty = 0;
        
        /* Get data to populate fields with - webservice must be static so queries have to go here :-( */
        Rep_Stock_Request__c request = [
            SELECT Id, Name, Status__c, Date_of_Request__c, Rep_Name__c, Rep_Name__r.Name, Shipping_Address__c, Shipping_Method__c, Notes__c
            FROM Rep_Stock_Request__c
            WHERE Id = :repId
            LIMIT 1
        ];
        
        List<Rep_Stock_Request_Item__c> items = [
            SELECT Rep_Stock_Request__c, Quantity__c, Item_Master__c, Name, Item_Master__r.Name, Id 
            FROM Rep_Stock_Request_Item__c
            WHERE Rep_Stock_Request__c = :request.Id
        ];
        
        SCMC__Warehouse__c sourceWarehouse = [
            SELECT Id, Name
            FROM SCMC__Warehouse__c
            WHERE name = 'DCOTA FLORIDA Office & Warehouse'
            LIMIT 1
        ];
        
        SCMC__Inventory_Position__c inventPos = [
            SELECT Id, Name, SCMC__Bin__c, SCMC__Condition_Code__c, SCMC__Shelf_Life_Expiration__c
			FROM SCMC__Inventory_Position__c
			WHERE SCMC__Bin__r.name = 'Finished Goods'
			LIMIT 1
		];
		
        SCMC__Currency_Master__c usdCode = [select id from SCMC__Currency_Master__c where Name = 'USD' limit 1];
		
		String userWarehouseString = 'Rep - ' + request.Rep_Name__r.Name;
		SCMC__Warehouse__c userWarehouse = [select Id,name FROM SCMC__Warehouse__c where name = :userWarehouseString LIMIT 1];
		
        String userStockString = 'Rep Stock - ' + request.Rep_Name__r.Name; 
        SCMC__Inventory_Position__c inventPosUser = [
            SELECT Id, Name, SCMC__Bin__c, SCMC__Bin__r.name, SCMC__Condition_Code__c, SCMC__Shelf_Life_Expiration__c
			FROM SCMC__Inventory_Position__c 
			WHERE SCMC__Bin__r.name = :userStockString
			LIMIT 1
		];
		
        SCMC__Inventory_Location__c inventLocUser = [
            SELECT Id, Name
			FROM SCMC__Inventory_Location__c
			WHERE name = :userStockString
			LIMIT 1
		];
		
		SCMC__Reason_Code__c reasonCode = [SELECT Id, name FROM SCMC__Reason_Code__c where name = 'Rep Stock Replenish' LIMIT 1];
        
        List<SCMC__Inventory_Position__c> positions = [
            SELECT Id, Name, SCMC__Availability__c, SCMC__Item_Master__c, SCMC__Item_Master__r.name, SCMC__Item_Serial_Number__c, SCMC__Item_Warehouse__c, 
                SCMC__Quantity__c, SCMC__Location__c, SCMC__Receipt_Line__c, SCMC__Serial_Number__c, SCMC__Warehouse__c, SCMC__Lot_Number__c,
                SCMC__Quantity_Allocated__c, SCMC__Condition_Code__c, SCMC__Shelf_Life_Expiration__c, SCMC__Current_Value__c, 
                SCMC__Availability_Code__c, SCMC__Bin__c, SCMC__Receiving_Inspection__c, SCMC__Acquisition_Cost__c, SCMC__ICP_Acquisition_Cost__c
                
            FROM SCMC__Inventory_Position__c
            WHERE SCMC__Availability_Code__c = 'In Stock'
            AND SCMC__Bin__r.name = 'Finished Goods'
            Order BY SCMC__Shelf_Life_Expiration__c ASC NULLS LAST
        ];
        
        /* Mk Trans Req */
        SCMC__Transfer_Request__c transReq = new SCMC__Transfer_Request__c();
        
        transReq.Rep_Stock_Request__c = repId;
        transReq.SCMC__Source_Warehouse__c = sourceWarehouse.id;
        transReq.SCMC__Source_Location__c = inventPos.SCMC__Bin__c;
        transReq.SCMC__Reason_Code__c = reasonCode.Id;
        transReq.SCMC__Destination_Warehouse__c = userWarehouse.Id;
        transReq.SCMC__Destination_Location__c = inventLocUser.Id;
        transReq.SCMC__Status__c = 'New';
        transReq.SCMC__Shipment_Required__c = true;
        transReq.SCMC__Shipment_Status__c = 'Allocated';
        transReq.SCMC__Carrier_Service__c = request.Shipping_Method__c;
        
        if(request.Shipping_Method__c == 'Hand Deliver'){
            transReq.SCMC__Carrier__c = 'Company Truck';
        } else {
            transReq.SCMC__Carrier__c = 'FedEx';
        }
        
        /*try{*/
            insert transReq;
            System.debug('You\'ve successfully created a transfer request... Adding requested items now...');
        /*} catch(Exception e){
            returnMsg = 'Houston, we have a problem inserting your Request. Error: ' + e;
        }*/
        
        
        // Mk Trans Req Lines / items
        List<SCMC__Transfer_Request_Line__c> transItems = new List<SCMC__Transfer_Request_Line__c>();
        List<SCMC__Inventory_Position__c> positionsToUpdate = new List<SCMC__Inventory_Position__c>();//  AND SCMC__Quantity_Allocated__c = 0
        
        SCMC__Transfer_Request_Line__c transItem = new SCMC__Transfer_Request_Line__c();
        
        //system.debug('pos size + ' + positions.size());
        for(Rep_Stock_Request_Item__c item : items){
            System.debug(' for loop - ' + item + ' start ');
            
            i++;
            haveQty = 0;
            resultQty = 0;
            wantQty =  item.Quantity__c;
            
            if(wantQty > haveQty && wantQty > 0){
                for(SCMC__Inventory_Position__c pos : positions){
                    
                    if(pos.SCMC__Item_Master__c == item.Item_Master__c){
                        
                        haveQty = pos.SCMC__Quantity__c - pos.SCMC__Quantity_Allocated__c;
                        
                        if(haveQty > 0 && wantQty > 0){
                            
                            transItem = new SCMC__Transfer_Request_Line__c();
                            transItem.SCMC__Item_Master__c = item.Item_Master__c;
                            transItem.SCMC__Transfer_Request__c = transReq.Id;
                            transItem.SCMC__Condition_Code__c = pos.SCMC__Condition_Code__c;
                            transItem.SCMC__Shelf_Life_Expiration__c = pos.SCMC__Shelf_Life_Expiration__c;
                            transItem.Item_Value__c = pos.SCMC__Current_Value__c;
                            transItem.SCMC__Status__c = 'Allocated';
                            transItem.SCMC__Lot_Number__c = pos.SCMC__Lot_Number__c;
                            
                            Decimal posAllocation = pos.SCMC__Quantity_Allocated__c;
                            
                            if(haveQty < wantQty || haveQty == wantQty){
                                
                                transItem.SCMC__Quantity__c = haveQty;
                                transItem.SCMC__Quantity_Allocated__c = haveQty;
                                pos.SCMC__Quantity_Allocated__c = haveQty + posAllocation;
                                transItem.SCMC__Inventory_Position__c = pos.Id;
                                
                            }
                            
                            if(haveQty > wantQty){
                                
                                transItem.SCMC__Quantity__c = wantQty;
                                
                                transItem.SCMC__Quantity_Allocated__c = wantQty;
                                pos.SCMC__Quantity_Allocated__c = 0;
                                
                                SCMC__Inventory_Position__c newPos = pos.clone(false,false,false,false);
                                
                                pos.SCMC__Quantity__c = haveQty - wantQty;
                                newPos.SCMC__Quantity_Allocated__c = wantQty;
                                
                                newPos.SCMC__Quantity__c = wantQty;
                                newPos.SCMC__Acquisition_Currency__c = usdCode.id;
                                newPos.SCMC__ICP_Currency__c = usdCode.id;
                                system.debug(' rec= '+pos.SCMC__Receiving_Inspection__c +' acq= '+pos.SCMC__Acquisition_Cost__c +' icp= '+ pos.SCMC__ICP_Acquisition_Cost__c);
                                newPos.SCMC__Receiving_Inspection__c = pos.SCMC__Receiving_Inspection__c;
                                newPos.SCMC__Acquisition_Cost__c = pos.SCMC__Acquisition_Cost__c;
                                newPos.SCMC__ICP_Acquisition_Cost__c = pos.SCMC__ICP_Acquisition_Cost__c;
                                insert newPos;
                                transItem.SCMC__Inventory_Position__c = newPos.id;
                                
                            }
                            
                            wantQty = wantQty - haveQty;
                            
                            transItems.add(transItem);
                            positionsToUpdate.add(pos);
                            //positionsToUpdate.add(newPos);
                            
                            System.debug('For in While loop :' + i + ' * transItem = ' + item.Item_Master__c + ' * wantQty = ' + wantQty + ' * position to update = ' + pos.Name + ' * position has left(SCMC__Quantity_Allocated__c) = ' + pos.SCMC__Quantity_Allocated__c);
                        }
                    }    
                } // System.debug(' *if qty match -  DONE ');
            }// System.debug('*FOR POSITION DONE');
        } // System.debug(' * ITEM FOR DONE');
        
        /*try{*/
            
            upsert positionsToUpdate;
            insert transItems;
            
            returnMsg = 'You\'ve successfully created a transfer request.';
            system.debug('You\'ve successfully created a transfer request. ' + transItems);
            //System.debug(' *can i haz id\'s...  ' + transItems[0].id );
            
        /*} catch(Exception e){
            
            system.debug('transItems after insert = ' + transItems + '... Error is ' + e);
            returnMsg = 'Houston, we have a problem. Error: ' + e + ' items = ' + transItems;
            //System.debug(' *can i haz id\'s...  ' + transItems[0].id );
            
        }*/
        
        SCMC__Picklist__c picklist = new SCMC__Picklist__c();
        SCMC__Pick_list_Detail__c picklistDetail = new SCMC__Pick_list_Detail__c();
        List<SCMC__Pick_list_Detail__c> picklistDetails = new List<SCMC__Pick_list_Detail__c>();
        
        RecordType transToDifWarehouse = [SELECT Id, name FROM RecordType where name = 'Transfer to a Different Warehouse Pick-List'];
        
        // build picklist from transReq
        picklist.SCMC__Transfer_Request__c = transReq.id;
        picklist.SCMC__Status__c = 'New';
        picklist.RecordTypeId = transToDifWarehouse.Id;
        // picklist.SCMC__Pick_List_Type__c = transToDifWarehouse.id;
        
        /*try{*/
            insert picklist;
            system.debug('You\'ve successfully created a transfer request & picklist. ');
        /*} catch(Exception e){
            system.debug('picklist error ... Error is ' + e);
            returnMsg = 'Houston, we have a PL problem. Error: ' + e + ' items = ' + transItems;
        }*/
        
        // picklistDetails transItems
        for(SCMC__Transfer_Request_Line__c ti : transItems){
            
            picklistDetail = new SCMC__Pick_list_Detail__c();
            
            system.debug('trying to mk PL detail ');
            picklistDetail.SCMC__Picklist__c = picklist.id;
            picklistDetail.SCMC__Inventory_Detail__c = ti.SCMC__Inventory_Position__c;
            picklistDetail.SCMC__Status__c = 'New';
            picklistDetail.SCMC__Transfer_Request_Line__c = ti.id;
            picklistDetail.SCMC__Condition_Code__c = ti.SCMC__Condition_Code__c;
            picklistDetail.SCMC__Lot_Number__c = ti.SCMC__Lot_Number__c;
            picklistDetail.SCMC__Item__c = ti.SCMC__Item_Master__c;
            picklistDetail.SCMC__Shelf_Life_Expiration__c = ti.SCMC__Shelf_Life_Expiration__c;
            picklistDetail.SCMC__Warehouse__c = sourceWarehouse.id;
            picklistDetail.SCMC__Quantity__c = ti.SCMC__Quantity__c;
            picklistDetail.SCMC__Cost__c = transItem.Item_Value__c;
            // picklistDetail.SCMC__Serial_Number__c,  = 'IDEK';
            // picklistDetail.SCMC__Issueing_Warehouse__c = sourceWarehouse.id;
            
            picklistDetails.add(picklistDetail);
        }
        
        /*try{*/
            
            insert picklistDetails;
            
            system.debug('You\'ve successfully created a transfer request & picklist & picklist details ');
            returnMsg = 'You\'ve successfully created a transfer request.';
            
        /*} catch(Exception e){
            //System.debug(' *can i haz id\'s...  '  );
            
            system.debug('picklist details error ... Error is ' + e);
            returnMsg = 'Houston, we have a PL details problem. Error: ' + e + ' items = ';
            
        }*/
        
        return returnMsg;
    }
}