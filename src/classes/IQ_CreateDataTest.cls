@isTest(SeeAllData=true)
public class IQ_CreateDataTest {

	static string recordTypeName = 'Practice Account';
	static string namePrefix = 'UnitTest';

	static IQ_CreateData setup() {
		IQ_CreateData repo = new IQ_Createdata();
		return repo; 
	}

	@isTest
	static void testGetItems() {
		IQ_CreateData iq = new IQ_CreateData();
		List<SelectOption> options = iq.getItems();
		System.assert(options.size() > 0);
	}

	@isTest
	static void createRandomIntegerTest() {
		IQ_CreateData iq = new IQ_CreateData();
		Integer ran = IQ_CreateData.createRandomInteger();
		System.debug('Random Number: ' + string.valueOf(ran));
		System.assert( ran > 0);

	}

	@isTest static void CreateAccountTest() {
		IQ_CreateData iq = new IQ_CreateData();
		iq.recordTypeName = 'Practice Account';
		Account acct = iq.CreateAccount();
		System.debug(acct);
		System.assert(acct != null);
	}

	@isTest	static void testSave() {
		IQ_CreateData iq = new IQ_CreateData();
		iq.recordTypeName = recordTypeName;
		Account acct = createTestRecord();
		string acctName = acct.Name;
        iq.save(acct);
        Account[] accts = [SELECT Id, Name FROM Account WHERE Name = :acctName];
        System.assertEquals(accts.size(), 1);
	}

	// @isTest
	// static void createRecordsToSave() {
	// 	IQ_CreateData iq = new IQ_CreateData();
	// 	iq.recordTypeName = recordTypeName;
	// 	iq.namePrefix = namePrefix;
	// 	Account acct = createTestRecord();
	// 	iq.recordsToCreate = 3;
	// 	iq.createRecordsToSave(); 
    //     Account[] accts = [SELECT Id, Name FROM Account WHERE Name like :namePrefix+'%'];
    //     System.assert(accts.size() > 2);
	// }

	@isTest 
	static void testGetRecordType() {
		IQ_CreateData iq = new IQ_CreateData();
		Id recId = iq.getRecordType(recordTypeName);
		string srecId = string.valueof(recId); 
		System.assert(srecId.length() > 0);
	}

	static Account createTestRecord() {
		Account acct = new Account();
		string acctName = namePrefix + String.valueOf(IQ_CreateData.createRandomInteger());
        acct.Name = acctName;
        return acct;
	}

	@isTest static void CreateRandomDobDateTest() {
		Date dob = IQ_CreateData.CreateRandomDobDate();
		System.debug(dob);
		System.assert(dob > Date.NewInstance(1927,1,1));
	}
	
	@isTest static void CreateRandomProcedureDate() {
		Date procedureDate = IQ_CreateData.CreateRandomProcedureDate();
		System.debug(procedureDate);
		Date daysBefore = Date.today().addDays(-91);
		System.debug(daysBefore);
		System.assert(procedureDate > daysBefore);
	}

	@isTest static void CreateRandomIntegerTest2() {
		Integer x = IQ_CreateData.createRandomInteger(100);
		System.assert(x <= (100 - 1));
		System.assert(x > 0);
	}
	@isTest
	static void CreateRandomIntegerTest3() {
		IQ_CreateData iq = new IQ_CreateData();
		Integer ran = IQ_CreateData.createRandomInteger(20);
		System.debug('Random Number: ' + string.valueOf(ran));
		System.assert(ran != null);
	}

	@isTest static void GetFirstNamesTest() {
		IQ_CreateData iq = setup();
		List<IQ_CreateData.GenderName> firstNames = iq.GetFirstNames();
		System.assert(firstNames.size() > 0);
	}

	@isTest static void GetLastNamesTest() {
		List<string> lastNames = IQ_CreateData.GetLastNames();
		System.assert(lastNames.size() > 0);
	}

	@isTest static void GetRandomFirstNameTest() {
		IQ_CreateData iq = setup();
		String firstName = iq.GetRandomFirstName().name;
		System.assert(firstName != null);
	}

	@isTest static void GetRandomLastNameTest() {
		IQ_CreateData iq = setup();
		String lastName = iq.GetRandomLastName();
		System.assert(lastName != null);
	}
	
	@isTest static void RetrieveOrCreateCaseTypeTest() {
		Case_Type__c caseType = IQ_CreateData.RetrieveOrCreateCaseType('Total Knee Arthroplasty');
		System.debug(caseType);
		System.assert(caseType != null);
	}

	@isTest static void GenerateNamesAndDOBsTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		Integer count = 10;
		List<Patient__c> patients = repo.GenerateNamesAndDOBs(count);
		System.debug(patients);
		System.assertEquals(patients.size(), 10);
		Test.stopTest();
	}

	@isTest static void CreatePatientsTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		Integer count = 10;
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		System.debug(hospital);
		List<Patient__c> patients = repo.CreatePatients(hospital, count);
		System.debug(patients);
		System.assertEquals(10, patients.size());
		Test.stopTest();
	}

	@isTest static void CreatePatientTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		Patient__c patient = repo.CreatePatient(hospital, 'LastTest', 'FirstTest', Date.newInstance(2010, 1, 2), 'M');
		System.debug(patient);
		System.assertEquals('LastTest', Patient.Last_Name__c );
	}

	@isTest static void CreateSurveyTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		Survey__c survey = repo.CreateSurvey('KOOS', 'https://orthosensor.co1.qualtrics.com/SE/?SID=SV_5C0OMuQLsSQhRWJ');
		System.debug(survey);
		System.assert(survey != null);
		Test.stopTest();
	}
	@isTest static void CreateSurgeonTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		Account practice = [Select Id, Name from Account where recordType.Name = 'Practice Account' Limit 1];
		Id practiceId = practice.Id;
		string lastName = repo.GetRandomLastName();
		string firstName = repo.GetRandomFirstName().name;
		Account surgeon = repo.createSurgeonAccount(practiceId, lastName, firstName);
		System.debug(surgeon);
		System.assert(surgeon != null);
	}

	@isTest static void CreateCaseTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		Survey__c survey = repo.CreateSurvey('KOOS', 'https://orthosensor.co1.qualtrics.com/SE/?SID=SV_5C0OMuQLsSQhRWJ');
		List<Patient__c> patients = repo.GenerateNamesAndDOBs(1);
		Patient__c patient = repo.CreatePatient(hospital, patients[0].Last_Name__c, patients[0].First_Name__c, patients[0].Date_Of_Birth__c, patients[0].Gender__c);
		Account practice = [Select Id, Name from Account where recordType.Name = 'Practice Account' Limit 1];
		Id practiceId = practice.Id;
		string lastName = repo.GetRandomLastName();
		string firstName = repo.GetRandomFirstName().name;
		Account surgeon = repo.createSurgeonAccount(practiceId, lastName, firstName);
		Case__c case1  = repo.CreateCase('Test', 'Total Knee Arthroplasty', patient, survey, 
		IQ_CreateData.CreateRandomProcedureDate(),
      		surgeon.Id, 'Right');
		System.debug(case1);
		System.assert(case1 != null);
	}
	// @isTest static void CreatePatientsAndCasesTest() {
	// 	test.startTest();
	// 	IQ_CreateData repo = setup();
	// 	repo.recordTypeName = 'Practice Account';
	// 	string hospital = repo.CreateAccount().Id;
	// 	Survey__c survey = repo.CreateSurvey('KOOS', 'https://orthosensor.co1.qualtrics.com/SE/?SID=SV_5C0OMuQLsSQhRWJ');
	// 	Account practice = [Select Id, Name from Account where recordType.Name = 'Practice Account' Limit 1];
	// 	Id practiceId = practice.Id;
	// 	string lastName = repo.GetRandomLastName();
	// 	string firstName = repo.GetRandomFirstName().name;
	// 	Account surgeon = repo.createSurgeonAccount(practiceId, lastName, firstName);
	// 	List<Case__c> cases = repo.CreatePatientsAndCases(hospital, 10, 'test', 
	// 		'Total Knee Arthroplasty', survey, 
    //   		IQ_CreateData.CreateRandomProcedureDate(), 
    //   		surgeon.Id);
	// 	System.debug(cases);
	// }

	@isTest static void GetSurgeonByNameTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		string lastName = repo.GetRandomLastName();
		string firstName = repo.GetRandomFirstName().name;
		Account newSurgeon = repo.createSurgeonAccount(hospital, lastName, firstName);
		Surgeon_Hospital__c surgeon = repo.GetSurgeonByName(hospital, lastName, firstName);
		System.debug(newSurgeon);
		System.debug(surgeon);
		System.assertEquals(newSurgeon.LastName, surgeon.surgeon__r.LastName);
	}

	@isTest static void GetSurgeonByIdTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		string lastName = repo.GetRandomLastName();
		string firstName = repo.GetRandomFirstName().name;
		Account newSurgeon = repo.createSurgeonAccount(hospital, lastName, firstName);
		System.debug(newSurgeon);
		List<Contact> cont = [Select Id, Name, FirstName, LastName From Contact where FirstName=:firstName and LastName =:lastName]; 
        system.debug(cont);
        if (cont.size() > 0) {
            String s = cont[0].Id;
			Surgeon_Hospital__c surgeon = repo.GetSurgeonById(s);
			System.debug(surgeon);
			System.assertEquals(newSurgeon.LastName, surgeon.surgeon__r.LastName);
		}
	}

	@isTest static void GetSurgeonsByHospitalTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		repo.recordTypeName = 'Practice Account';
		string hospital = repo.CreateAccount().Id;
		string lastName = repo.GetRandomLastName();
		string firstName = repo.GetRandomFirstName().name;
		Account newSurgeon = repo.createSurgeonAccount(hospital, lastName, firstName);
		List<Surgeon_Hospital__c> surgeons = repo.GetSurgeonsByHospital(hospital);
		System.debug(newSurgeon);
		System.debug(surgeons);
		System.assertEquals(surgeons.size(), 1);
		test.stopTest();
	}

	@isTest static void GetManufacturerCodesTest() {
	 	test.startTest();
        List<Manufacturer__c> codes = IQ_CreateData.GetManufacturerCodes();
        System.debug(codes);
	 	System.assert(codes.size() > 0);
	 	Test.stopTest();
	}

	@isTest static void GetManufacturerCodeTest() {
		test.startTest();
		IQ_CreateData repo = setup();
		//asssume there are records in the manufacturer object
		List<Manufacturer__c> mans = [Select ID, Name from Manufacturer__c];
		System.debug(mans);
		if (mans.size() > 0) {
        	String manId = repo.GetManufacturerCode(mans[0].Name);
        	System.debug(manId);
			System.assertEquals(mans[0].ID, manId);
		}
		Test.stopTest();
	}   
}