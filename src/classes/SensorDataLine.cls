global class SensorDataLine{
        
        public String SD_DeviceID {get;set;}
        public String AttachmentID {get;set;}
        public String AttachmentName {get;set;}
        public String SD_EventID {get;set;}
        public String SD_ImageName {get;set;}
        public String SD_MeasurementDate {get;set;}
        public Decimal SD_Type_Increment {get;set;}
        public String SD_Type_IncrementID {get;set;}
        public Decimal SD_Type_MedialLoad {get;set;}
        public String SD_Type_MedialLoadID {get;set;}
        public Decimal SD_Type_LateralLoad {get;set;}
        public String SD_Type_LateralLoadID {get;set;}
        public Decimal SD_Type_Rotation {get;set;}
        public String SD_Type_RotationID {get;set;}
        public Decimal SD_Type_ActualFlex {get;set;}
        public String SD_Type_ActualFlexID {get;set;}
        public Decimal SD_Type_AP{get;set;}
        public String SD_Type_APID {get;set;}
        public Decimal SD_Type_HKA {get;set;}
        public String SD_Type_HKAID {get;set;}
        public Decimal SD_Type_TibiaRot {get;set;}
        public String SD_Type_TibiaRotID {get;set;}
        public Decimal SD_Type_TibiaTilt {get;set;}
        public String SD_Type_TibiaTiltID {get;set;}
        
        public SensorDataLine(){

            SD_DeviceID = '';
            AttachmentID = '';
            AttachmentName = '';
            SD_EventID = '';
            SD_ImageName = '';
            SD_MeasurementDate = '';
            SD_Type_Increment = 9999;
            SD_Type_IncrementID = '';
            SD_Type_MedialLoad = 9999;
            SD_Type_MedialLoadID = '';
            SD_Type_LateralLoad = 9999;
            SD_Type_LateralLoadID = '';
            SD_Type_Rotation = 9999;
            SD_Type_RotationID = '';
            SD_Type_ActualFlex = 9999;
            SD_Type_ActualFlexID = '';
            SD_Type_AP = 9999;
            SD_Type_APID  = '';
            SD_Type_HKA  = 9999;
            SD_Type_HKAID  = '';
            SD_Type_TibiaRot  = 9999;
            SD_Type_TibiaRotID  = '';
            SD_Type_TibiaTilt  = 9999;
            SD_Type_TibiaTiltID  = '';
        }
        
    }