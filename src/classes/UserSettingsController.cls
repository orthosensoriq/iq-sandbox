public with sharing class UserSettingsController {
	
	public boolean displayUserEdit {get; set;}
	public boolean displayPswdChange {get; set;}
	public boolean displayEULA {get; set;}
	public boolean displayHIPAA {get; set;}
	public boolean displayAuditLog {get; set;}
	public String eulaID {get; set;}
	public String hipaaID {get; set;}
	public Contact currContact {get; set;}
	public String resetId {get;set;}
	User currUser;
	
	public UserSettingsController() {
		//try {
		//	currContact = [SELECT Id, FirstName, LastName, Phone, MobilePhone FROM Contact WHERE Id = :currUser.ContactId];
		//} catch (exception e) {
		//	ApexPages.addMessages(e);
		//}
		displayUserEdit = false;
		displayPswdChange = false;
		displayHIPAA = false;
		displayEULA = false;
		displayAuditLog = false;
	}
    
    public void LogConstructor()
    {
		map<integer, string> params = new map<integer, string>();
        params.put(0, System.UserInfo.getUserId());
        osController baseCtrl = new osController();
		
        //currUser = [SELECT Id, ContactId, Accepted_EULA_Version__c, Accepted_HIPAA_Version__c FROM User WHERE ID = :System.UserInfo.getUserId()];
        currUser = ((list<User>)baseCtrl.getSelectedData('UserSettingsUser', params))[0];
		eulaID = currUser.Accepted_EULA_Version__c;
		hipaaID = currUser.Accepted_HIPAA_Version__c;
		if (currUser.ContactId != null) 
        {
            system.debug('ContactId: ' + currUser.ContactId);
            params.put(0, currUser.ContactId);
        	//currContact = [SELECT Id, FirstName, LastName, Phone, MobilePhone FROM Contact WHERE Id = :currUser.ContactId];
        	currContact = ((list<Contact>)baseCtrl.getSelectedData('UserSettingsContact', params))[0];
        }
        else
            system.debug('No contact for UserId: ' + params.get(0));
    }

	public void showUserEdit () {
		if (currUser.ContactId != null) {
			displayUserEdit = true;
			displayPswdChange = false;
			displayHIPAA = false;
			displayEULA = false;
			displayAuditLog = false;
		}
		else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR - Current User does not have a related Contact');
			ApexPages.addMessage(myMsg);
			displayPswdChange = false;
			displayHIPAA = false;
			displayEULA = false;
			displayAuditLog = false;
		}
	}

	public void showPswdChange () {
		displayUserEdit = false;
		displayPswdChange = true;
		displayHIPAA = false;
		displayEULA = false;
		displayAuditLog = false;
	}

	public void showHIPAA () {
		displayUserEdit = false;
		displayPswdChange = false;
		displayHIPAA = true;
		displayEULA = false;
		displayAuditLog = false;
	}

	public void showEULA () {
		displayUserEdit = false;
		displayPswdChange = false;
		displayHIPAA = false;
		displayEULA = true;
		displayAuditLog = false;
	}

	public void showAuditLog () {
		displayUserEdit = false;
		displayPswdChange = false;
		displayHIPAA = false;
		displayEULA = false;
		displayAuditLog = true;
	}

	public PageReference resetPassword(){
		System.debug(resetId);
		try{
			User u = [SELECT Id FROM User WHERE contactId = :resetId LIMIT 1];
			if(!Test.isRunningTest()) System.ResetPasswordResult r = System.resetPassword(u.Id, true);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Password reset.  The user has been notified.'));
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Could not reset password, please contact support.'));
		}
		return null;
	}

	public PageReference viewAuditLog() {
		PageReference pageRef = new PageReference('/a52/o');
		pageRef.SetRedirect(TRUE);
		return pageRef;
	}

}