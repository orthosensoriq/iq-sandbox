@isTest(SeeAllData=true)
public class IQ_CaseActionsTest {

    public static string G1barcode = '0100816818020013111610171718101710092016V1547421215001017';

    @isTest
    static void AddSensorTest() {
        Test.startTest();
        string procId = 'a697A0000000udRQAQ';
        string barcode = '312624242,SNN-JRNYBCS56,050515V12401,07-24-2016,02-28-2023';
		List<Event_Type__c> eventTypes = [Select Id from Event_Type__c where Name = 'Procedure'];
        Event__c event = [Select Id, Case__c, Physician__c  from Event__c Where Event_Type__c =: eventTypes[0].Id].get(0);  
        IQ_CaseActions.AddSensor('barcode', event.Id);

        System.Assert(true); 
        Test.stopTest();
    }

    @isTest
    static void AddSensorManuallyTest() {
    	Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        System.debug('created event: ' + event.id);
        OS_Device_Ref__c ref = new OS_Device_Ref__c(Name='RF1235', Manufacturer__c='MFGR2');
        insert ref;
        System.debug(ref.Id);
    	IQ_CaseActions.AddSensorManually(ref.Id, 'man', 'sn', 'lot', '05','2020', event.Id);
		System.Assert(true); 
    	//add test to see if record was inserted.	
		Test.stopTest();    	
    }

    @isTest
    static void SetStringToDateForG1Test() {
        string gDate = '151017';
        Date newDate = IQ_CaseActions.SetStringToDateForG1(gDate);
        System.debug(newDate);
        System.assertEquals(newDate, Date.newInstance(2015, 10, 17));
    }

    @isTest
    static void SetStringToDateForG1BadTest() {
        string gDate = '1B017';
        Date newDate = IQ_CaseActions.SetStringToDateForG1(gDate);
        System.debug(newDate);
        System.assertEquals(newDate, null);
    }

    @isTest
    static void GetOSDeviceSensorByGtinTest() {
        Test.startTest();
        OS_Device_Ref__c deviceRef = IQ_CaseActions.GetOSDeviceSensorByGtin('00816818020013');
        // System.assertEquals(deviceRef.Name, 'BMT-VGCR63'); //this should work in sandbox and production
        // Test.stopTest();
    }

    @isTest
    static void ParseG1SensorBoxBarcodeTest() {
        //string barcode = '0100816818020013111610171718101710092016V1547421215001017';
        Test.startTest();
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(G1barcode);
        System.debug(device);
        // System.assertEquals(device.Manufacturer_Name__c, 'Biomet');
        // System.assertEquals(device.Lot_Number__c, '092016V15474');
        Test.stopTest();
    }

    @isTest
    static void ParseG1SensorBoxBarcodeTest2() {
        string barcode = '0100816818020228111610121718101210092616V1545121315008241';
        Test.startTest();
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(barcode);
        System.debug(device);
        //System.assertEquals(device.Manufacturer_Name__c, 'Smith and Nephew');
        // System.assertEquals(device.Lot_Number__c, '092616V15451');
        // System.assertEquals(device.Device_Id__c, '315008241');
        // System.assertEquals(device.Year_Assembled__c, '2016');
        Test.stopTest();
    }

    @isTest
    static void ParseG1SensorBoxBarcodeTest3() {
        string barcode = '0100816818020228029111610120291718101202910092616v1545102921315008241';
        Test.startTest();
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(barcode);
        System.debug(device);
        // System.assertEquals(device.Manufacturer_Name__c, 'Smith and Nephew');
        // System.assertEquals(device.Lot_Number__c, '092616v15451');
        // System.assertEquals(device.Device_Id__c, '315008241');
        // System.assertEquals(device.Year_Assembled__c, '2016');
        Test.stopTest();
    }

    @isTest
    static void ProcessSensorBoxBarcodeTest2() {
        string barcode = '0100816818020228111610121718101210092616V1545121315008241';
        DeleteTestDeviceEvents();
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        string result = IQ_CaseActions.ProcessSensorBoxBarcode(barcode, event.Id);
        System.debug(result);
        // System.assertEquals('Successful', result);
        Test.stopTest();
    }
    @isTest
    static void ParseSensorBoxBarcodeSmithAndNephewTest() {
        DeleteTestDeviceEvents();
        string barcode = '312624242,SNN-JRNYBCS56,050515V12401,07-24-2016,02-28-2023';
        Test.startTest();
        OS_Device__c device = IQ_CaseActions.ParseSensorBoxBarcode(barcode);
        System.debug(device);
        System.assertEquals(device.Manufacturer_Name__c, 'Smith and Nephew');
        System.assertEquals(device.Lot_Number__c, '050515V12401');
        Test.stopTest();
    }

    @isTest
    static void ParseSensorBoxBarcodeStrykerTest() {
        DeleteTestDeviceEvents();
        string barcode = '312624242,SNN-JRNYBCS56,050515V12401,07-24-2016,02-28-2023';
        Test.startTest();
        OS_Device__c device = IQ_CaseActions.ParseSensorBoxBarcode(barcode);
        System.debug(device);
        System.assertNotEquals(device.Manufacturer_Name__c, 'Stryker');
        
        Test.stopTest();
    }
    
    @isTest
    static void DeviceExistsTest() {
        DeleteTestDeviceEvents();
        Test.startTest();
        boolean exists = true; // want to prove its not found
        Event__c event = CreateTestProcedureEvent();
        system.debug(event);
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(G1barcode);
        System.debug(device);
        exists = IQ_CaseActions.DeviceExists(device, event.Id);
        System.AssertEquals(exists, false);
        Test.stopTest();
    }

    @isTest
    static void DeviceExistsTest2() {
        Test.startTest();
        boolean exists = true; // want to prove its not found
        Event__c event = CreateTestProcedureEvent();
        system.debug(event);
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(G1barcode);
        exists = IQ_CaseActions.DeviceExists(device, event.Id);
        //System.AssertEquals(exists, false);

        //IQ_CaseActions.SaveOSDevice(device, event.id);
        //exists = IQ_CaseActions.DeviceExists(device, event.Id);
        
        //try same record again
        //OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(G1barcode);
        //exists = IQ_CaseActions.DeviceExists(device, event.Id);
        //System.AssertEquals(exists, true);

        Test.stopTest();
    }

    @isTest
    static void SaveOSDeviceTest() {
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        system.debug(event);
        OS_Device__c device = IQ_CaseActions.ParseG1SensorBoxBarcode(G1barcode);
        IQ_CaseActions.SaveOSDevice(device, event.id);
        List<OS_Device__c> currDevices = [Select Name from OS_Device__c 
                                            WHERE Device_Id__c = :device.Device_Id__c];
        System.Assert(currDevices.size() > 0);
        // List<OS_Device_Event__c> currEDevices = [Select Name from OS_Device_Event__c 
        //                                    WHERE Event__c =:event.Id AND OS_Device__c = :device.Id];
        // System.Assert(currEDevices.size() > 0);
        Test.stopTest();
    }

    @isTest
    static void ProcessSensorBoxBarcodeG1Test() {
        DeleteTestDeviceEvents();
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        string result = IQ_CaseActions.ProcessSensorBoxBarcode(G1barcode, event.Id);
        List<OS_Device__c> currDevices = [Select Name from OS_Device__c 
                                           WHERE Device_Id__c = '215001017'];
        //System.Assert(currDevices.size() > 0);
        //List<OS_Device__c> currEDevices = [Select Device_ID__c from OS_Device__c 
        //                                  WHERE Id IN 
        //                                  (Select OS_Device__c from OS_Device_Event__c 
        //                                   WHERE Event__c =:event.Id) AND IsDeleted__c = false AND Device_ID__c = '215001017'];
        //System.Assert(currEDevices.size() > 0);
        //System.AssertEquals(result, 'Successful');
        Test.stopTest();
    }

    @isTest
    static void ProcessSensorBoxBarcodeEmptyCodeTest() {
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        string result = IQ_CaseActions.ProcessSensorBoxBarcode('', event.Id);
        System.AssertEquals(result, 'Empty bar code!');
        Test.stopTest();
    }

    @isTest
    static void ProcessSensorBoxBarcodeInvalidTest() {
        string badcode = '01008168180200131116R0071718101710092016V1547421215001017';
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        string result = IQ_CaseActions.ProcessSensorBoxBarcode(badcode, event.Id);
        System.AssertEquals(result, 'Invalid bar code!');
        Test.stopTest();
    }

    @isTest
    static void ProcessSensorBoxBarcodeTest() {
        string barcode = '312624242,SNN-JRNYBCS56,050515V12401,07-24-2016,02-28-2023';
        Test.startTest();
        Event__c event = CreateTestProcedureEvent();
        IQ_CaseActions.ProcessSensorBoxBarcode(barcode, event.Id);
        //List<OS_Device__c> currDevices = [Select Name from OS_Device__c 
        //                                   WHERE Device_Id__c = :device.Device_Id__c];
        //System.Assert(currDevices.size() > 0);
        //List<OS_Device_Event__c> currEDevices = [Select Name from OS_Device_Event__c 
        //                                   WHERE Event__c =:event.Id AND OS_Device__c = :device.Id];
        //System.Assert(currEDevices.size() > 0);
        Test.stopTest();
    }

    private static Event__c CreateTestProcedureEvent() {
        Account acct = IQ_SurveyRepository.CreateHospital('BBB Hospital');
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Procedure');
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        Event_Type_Survey__c eventTypeSurvey = IQ_SurveyRepository.CreateEventTypeSurvey(eventType, survey);      
        string eventName = 'Test Event Name';
        Event__c event = IQ_SurveyRepository.CreateEvent(eventName, eventType, 'Test Case Type Name', survey, 
        patient, '10/15/2016', null, 'Left');
        System.debug(event);
        return event;
    }
    private static void DeleteTestDeviceEvents() {
        List<OS_Device_Event__c> ds = [Select Id from OS_Device_Event__c WHERE OS_Device__r.Device_ID__c = '215001017'];
        delete ds;
    }

    ////////stopped here
    @isTest
    static void GetCaseEventsTest() {
        Case__c testCase = new Case__c();
        testCase.Name__c = 'Test';
        insert testCase;
        String caseId = testCase.Id;
        System.debug(caseId);
        
        Event__c event = new Event__c();
        event.Case__c = caseId;
        event.Event_Name__c ='Event Name';
        insert event;
        System.debug(testCase);
        List<Event__c> events = IQ_CaseActions.GetCaseEvents(testCase.Id);
        System.assert(events.size() > 0);
    }

    @isTest
    static void GetSurveyCompletionDatesTest() {
        Case__c testCase = new Case__c();
        testCase.Name__c = 'Test';
        insert testCase;

        Survey__c survey = new Survey__c();
        survey.Name__c = 'Test Survey';

        Event__c event = new Event__c();
        event.Case__c = testCase.Id;
        event.Event_Name__c ='Event 1';
        insert event;

        Event_Form__c eventForm = new Event_Form__c();
        eventForm.Name = 'Test Event Form';
        eventForm.Survey__c = survey.Id;
        eventForm.Event__c = event.Id;
        insert eventForm;

        List<Event_Form__c> eventForms = IQ_CaseActions.GetSurveyCompletionDates(testCase.Id);
        System.Assert(eventForms.size() > 0);
    }
}