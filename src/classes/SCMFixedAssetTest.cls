/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Suite of tests for SCMFixedAsset.
**/
@isTest()
public with sharing class SCMFixedAssetTest {

	private static SetUpTestForSCM testHelper = new SetUpTestForSCM();

	//@isTest
	//static void createFixedAssetAsCapitialEquipmentPurchase()
	//{
	//	c2g__codaCompany__c company = new c2g__codaCompany__c();
	//	company.Name = 'Test Company, LLC';
	//	company.Id = SCMSetUpTest.generateMockId(c2g__codaCompany__c.SObjectType);

	//	SCMC__ICP__c icp = new SCMC__ICP__c();
	//	icp.Company__c = company.Id;
	//	icp.Company__r = company;
	//	icp.Id = SCMSetUpTest.generateMockId(SCMC__ICP__c.SObjectType);

	//	SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
	//	warehouse.SCMC__ICP__c = icp.Id;
	//	warehouse.SCMC__ICP__r = icp;
	//	warehouse.Id = SCMSetUpTest.generateMockId(SCMC__Warehouse__c.SObjectType);

	//	SCMC__Purchase_Order__c purchaseOrder = new SCMC__Purchase_Order__c();
	//	purchaseOrder.SCMC__Ship_To_Warehouse__c = warehouse.Id;
	//	purchaseOrder.SCMC__Ship_To_Warehouse__r = warehouse;
	//	purchaseOrder.Id = SCMSetUpTest.generateMockId(SCMC__Purchase_Order__c.SObjectType);

	//	SCMC__Purchase_Order_Line_Item__c purchaseOrderLine = new SCMC__Purchase_Order_Line_Item__c();
	//	purchaseOrderLine.SCMC__Purchase_Order__c = purchaseOrder.Id;
	//	purchaseOrderLine.SCMC__Purchase_Order__r = purchaseOrder;
	//	purchaseOrderLine.SCMC__Unit_Cost__c = 77.77;
	//	purchaseOrderLine.Id = SCMSetUpTest.generateMockId(SCMC__Purchase_Order_Line_Item__c.SObjectType);

	//	FAM__FA_Depreciation_Method__c depreciationMethod = new FAM__FA_Depreciation_Method__c();
	//	depreciationMethod.FAM__Calculation_Class__c = 'FAM.DM_StraightLine';
	//	depreciationMethod.Id = SCMSetUpTest.generateMockId(FAM__FA_Depreciation_Method__c.SObjectType);

	//	List<c2g__codaGeneralLedgerAccount__c> glAccountList = new List<c2g__codaGeneralLedgerAccount__c>();
	//	c2g__codaGeneralLedgerAccount__c glAccount = new c2g__codaGeneralLedgerAccount__c();
	//	glAccount.c2g__ReportingCode__c = '17710';
	//	glAccount.Id = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
	//	glAccountList.add(glAccount);

	//	glAccount = new c2g__codaGeneralLedgerAccount__c();
	//	glAccount.c2g__ReportingCode__c = '18000';
	//	glAccount.Id = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
	//	glAccountList.add(glAccount);

	//	// Create and pass the testing context.
	//	SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
	//	tc.isTest = true;
	//	tc.depreciationMethods = new List<FAM__FA_Depreciation_Method__c> {depreciationMethod};
	//	tc.generalLedgerAccounts = glAccountList;
	//	tc.purchaseOrderLines = new List<SCMC__Purchase_Order_Line_Item__c> {purchaseOrderLine};
	//	SCMFixedAsset.testingContext = tc;

	//	// Data for method call.
	//	SCMC__Receipt_Line__c receiptLine = new SCMC__Receipt_Line__c();
	//	receiptLine.SCMC__Purchase_Order_Line_Item__c = purchaseOrderLine.Id;
	//	receiptLine.SCMC__Purchase_Order_Line_Item__r = purchaseOrderLine;
	//	receiptLine.Fixed_Asset__c = null;
	//	receiptLine.Id = SCMSetUpTest.generateMockId(SCMC__Receipt_Line__c.SObjectType);

	//	List<FAM__Fixed_Asset__c> result = SCMFixedAsset.createFixedAssets(new List<SCMC__Receipt_Line__c> {receiptLine});
	//	System.debug('<ojs> resulting fixed assets:\n' + result);
	//	System.assert(!result.isEmpty(), 'A new fixed asset should have been returned.');
	//	System.assertEquals(result.get(0).FAM__Asset_Cost__c, purchaseOrderLine.SCMC__Unit_Cost__c, 'The cost of the asset is NOT correct.');
	//}

	@isTest
	static void createJournalEntriesWithFixedAssets()
	{

		List<SCMC__Receipt__c> receipts = new List<SCMC__Receipt__c>();
		SCMC__Receipt__c receipt = new SCMC__Receipt__c();
		receipt.Id = SCMSetUpTest.generateMockId(SCMC__Receipt__c.SObjectType);
		receipts.add(receipt);

		List<SCMC__Receipt_Line__c> receiptLines = new List<SCMC__Receipt_Line__c>();
		SCMC__Receipt_Line__c receiptLine = new SCMC__Receipt_Line__c();
		receiptLine.Id = SCMSetUpTest.generateMockId(SCMC__Receipt_Line__c.SObjectType);
		receiptLine.SCMC__Receiver__r = receipts.get(0);
		receiptLines.add(receiptLine);

		Map<Id, c2g__codaJournal__c> journalMap = new Map<Id, c2g__codaJournal__c>();
		c2g__codaJournal__c journal = new c2g__codaJournal__c();
		journal.Receipt_Line__c = receiptLine.Id;
		journalMap.put(receiptLine.Id, journal);

		Map<Id, c2g__codaJournalLineItem__c> journalLineMap_Increasing = new Map<Id, c2g__codaJournalLineItem__c>();
		c2g__codaJournalLineItem__c journalLine = new c2g__codaJournalLineItem__c();
		journalLineMap_Increasing.put(journal.Receipt_Line__c, journalLine);

		Map<Id, c2g__codaJournalLineItem__c> journalLineMap_Decreasing = new Map<Id, c2g__codaJournalLineItem__c>();
		journalLine = new c2g__codaJournalLineItem__c();
		journalLineMap_Decreasing.put(journal.Receipt_Line__c, journalLine);

		SCMFFJournalMock journalMock = new SCMFFJournalMock();
		journalMock.getJournalsMap = journalMap;
		journalMock.getJournalLinesIncreasingMap = journalLineMap_Increasing;
		journalMock.getJournalLinesDecreasingMap = journalLineMap_Decreasing;

		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.journalMock = journalMock;
		SCMFixedAsset.testingContext = tc;

		List<FAM__Fixed_Asset__c> fixedAssetList = new List<FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c fixedAsset = new FAM__Fixed_Asset__c();
		fixedAsset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		fixedAsset.Receipt_Line__c = SCMSetUpTest.generateMockId(SCMC__Receipt_Line__c.SObjectType);
		fixedAsset.FAM__Status__c = 'Active';
		fixedAssetList.add(fixedAsset);

		List<c2g__codaJournal__c> results = SCMFixedAsset.createJournalEntries(fixedAssetList);
		System.assert(results.size() == 1, 'Only 1 journal should\'ve been created by the test.');
	}

	@isTest
	static void updateFixedAssetHistory()
	{
		List<FAM__Fixed_Asset__c> assetList = new List<FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Deployed_Date__c = Date.today();
		asset.Deployed_To__c = SCMSetUpTest.generateMockId(Account.SObjectType);
		assetList.add(asset);

		List<Fixed_Asset_History__c> histories = new List<Fixed_Asset_History__c>();
		Fixed_Asset_History__c historyItem = new Fixed_Asset_History__c();
		historyItem.Fixed_Asset__c = asset.Id;
		historyItem.Date_Returned__c = Date.today();
		histories.add(historyItem);

		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.fixedAssetHistories = histories;
		SCMFixedAsset.testingContext = tc;

		List<Fixed_Asset_History__c> result = SCMFixedAsset.createHistory(assetList);
		System.debug('<ojs> result:\n' + result);
	}

	@isTest
	static void updateFixedAssetWhenShipped()
	{

		Account acct = new Account();
		acct.Id = SCMSetUpTest.generateMockId(Account.SObjectType);

		FAM__Asset_Group__c assetGroup = new FAM__Asset_Group__c();
		assetGroup.Id = SCMSetUpTest.generateMockId(FAM__Asset_Group__c.SObjectType);

		SCMC__Item__c item = new SCMC__Item__c();
		item.Asset_Group__c = assetGroup.Id;
		item.Asset_Group__r = assetGroup;
		item.Id = SCMSetUpTest.generateMockId(SCMC__Item__c.SObjectType);

		SCMC__Sales_Order__c salesOrder = new SCMC__Sales_Order__c();
		salesOrder.Id = SCMSetUpTest.generateMockId(SCMC__Sales_Order__c.SObjectType);
		salesOrder.SCMC__Customer_Account__r = acct;
		salesOrder.SCMC__Customer_Account__c = acct.Id;

		SCMC__Sales_Order_Line_Item__c salesOrderLine = new SCMC__Sales_Order_Line_Item__c();
		salesOrderLine.SCMC__Item_Master__c = item.Id;
		salesOrderLine.SCMC__Item_Master__r = item;
		salesOrderLine.SCMC__Sales_Order__r = salesOrder;
		salesOrderLine.Id = SCMSetUpTest.generateMockId(SCMC__Sales_Order_Line_Item__c.SObjectType);
		System.debug('<ojs> salesOrderLine:\n' + salesOrderLine);

		List<SCMC__Serial_Number__c> serialNumberList = new List<SCMC__Serial_Number__c>();
		SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
		sn.SCMC__Sales_Order_Line_Item__r = salesOrderLine;
		sn.SCMC__Sales_Order_Line_Item__c = salesOrderLine.Id;
		sn.SCMC__Serial_Number__c = '123-789';
		sn.Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.SObjectType);
		serialNumberList.add(sn);
		System.debug('<ojs> serialNumberList:\n' + serialNumberList);

		SCMC__Picklist__c picklist = new SCMC__Picklist__c();
		picklist.SCMC__Sales_Order__c = salesOrder.Id;
		picklist.SCMC__Sales_Order__r = salesOrder;
		picklist.Id = SCMSetUpTest.generateMockId(SCMC__Picklist__c.SObjectType);

		List<SCMC__Pick_list_Detail__c> picklistDetails = new List<SCMC__Pick_list_Detail__c>();
		SCMC__Pick_list_Detail__c picklistDetail = new SCMC__Pick_list_Detail__c();
		picklistDetail.SCMC__Picklist__r = picklist;
		picklistDetail.SCMC__Serial_Number__c = serialNumberList.get(0).SCMC__Serial_Number__c;
		picklistDetail.SCMC__Sales_Order_Line_Item__r = salesOrderLine;
		picklistDetail.SCMC__Sales_Order_Line_Item__c = salesOrderLine.Id;
		picklistDetail.Id = SCMSetUpTest.generateMockId(SCMC__Pick_list_Detail__c.SObjectType);
		picklistDetails.add(picklistDetail);
		System.debug('<ojs> picklistDetails:\n' + picklistDetails);

		List<SCMC__Shipping__c> shippingList = new List<SCMC__Shipping__c>();
		SCMC__Shipping__c shipping = new SCMC__Shipping__c();
		shipping.SCMC__Picklist__r = picklist;
		shipping.SCMC__Picklist__c = picklist.Id;
		shippingList.add(shipping);
		System.debug('<ojs> shippingList:\n' + shippingList);

		List<FAM__Fixed_Asset__c> fixedAssetList = new List<FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Serial_Number_Reference__c = serialNumberList.get(0).Id;
		asset.Serial_Number_Reference__r = serialNumberList.get(0);
		fixedAssetList.add(asset);

		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.assetGroups = new List<FAM__Asset_Group__c> {assetGroup};
		tc.serialNumberList = serialNumberList;
		tc.picklistDetails = picklistDetails;
		tc.fixedAssets = fixedAssetList;
		SCMFixedAsset.testingContext = tc;

		List<FAM__Fixed_Asset__c> result = SCMFixedAsset.updateFixedAssetsWhenShipped(shippingList);

		System.assertEquals(1, result.size(), 'The test should return 1 fixed asset.');
		FAM__Fixed_Asset__c resultingFixedAsset = result.get(0);
		System.debug('<ojs> resultingFixedAsset:\n' + resultingFixedAsset);
		System.assert(resultingFixedAsset.Serial_Number_Reference__c == sn.Id, 'The fixed asset does NOT have the correct Serial Number.');
		System.assert(resultingFixedAsset.Deployed_To__c == acct.Id, 'The fixed asset is NOT deployed to the correct account.');
		System.assert(resultingFixedAsset.Deployed_Date__c == Date.today(), 'The fixed asset does NOT have the correct deployment date.');
	}

	@isTest
	static void updateFixedAssetWhenReceived()
	{
		List<Account> accounts = new List<Account>();
		Account acct = new Account();
		acct.Id = SCMSetUpTest.generateMockId(Account.SObjectType);
		accounts.add(acct);

		FAM__Asset_Group__c assetGroup = new FAM__Asset_Group__c();
		assetGroup.Id = SCMSetUpTest.generateMockId(FAM__Asset_Group__c.SObjectType);

		SCMC__Item__c item = new SCMC__Item__c();
		item.Id = SCMSetUpTest.generateMockId(SCMC__Item__c.SObjectType);

		SCMC__Sales_Order_Line_Item__c salesOrderLine = new SCMC__Sales_Order_Line_Item__c();
		salesOrderLine.Id = SCMSetUpTest.generateMockId(SCMC__Sales_Order_Line_Item__c.SObjectType);

		SCMC__Return_Material_Authorization__c rma = new SCMC__Return_Material_Authorization__c();
		rma.Id = SCMSetUpTest.generateMockId(SCMC__Return_Material_Authorization__c.SObjectType);
		rma.SCMC__Sales_Order_Line_Item__c = salesOrderLine.Id;
		rma.SCMC__Sales_Order_Line_Item__r = salesOrderLine;
		rma.SCMC__Item_Master__c = item.Id;
		rma.SCMC__Item_Master__r = item;

		List<SCMC__Receipt__c> receipts = new List<SCMC__Receipt__c>();
		SCMC__Receipt__c receipt = new SCMC__Receipt__c();
		receipt.Id = SCMSetUpTest.generateMockId(SCMC__Receipt__c.SObjectType);
		receipt.SCMC__Return_Material_Authorization__c = rma.Id;
		receipt.SCMC__Return_Material_Authorization__r = rma;
		receipts.add(receipt);

		List<SCMC__Receipt_Line__c> receiptLines = new List<SCMC__Receipt_Line__c>();
		SCMC__Receipt_Line__c receiptLine = new SCMC__Receipt_Line__c();
		receiptLine.SCMC__Receiver__c = receipt.Id;
		receiptLine.SCMC__Receiver__r = receipt;
		receiptLine.SCMC__Item_Number__c = item.Id;
		receiptLine.SCMC__Item_Number__r = item;
		receiptLine.Id = SCMSetUpTest.generateMockId(SCMC__Receipt_Line__c.SObjectType);
		receiptLines.add(receiptLine);

		List<SCMC__Serial_Number__c> serialNumberList = new List<SCMC__Serial_Number__c>();
		SCMC__Serial_Number__c sn = new SCMC__Serial_Number__c();
		sn.Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.SObjectType);
		sn.SCMC__Sales_Order_Line_Item__c = salesOrderLine.Id;
		sn.SCMC__Sales_Order_Line_Item__r = salesOrderLine;
		serialNumberList.add(sn);

		List<FAM__Fixed_Asset__c> fixedAssetList = new List<FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Serial_Number_Reference__c = serialNumberList.get(0).Id;
		asset.Serial_Number_Reference__r = serialNumberList.get(0);
		fixedAssetList.add(asset);

		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.accounts = accounts;
		tc.assetGroups = new List<FAM__Asset_Group__c> {assetGroup};
		tc.items = new List<SCMC__Item__c> {item};
		tc.receipts = receipts;
		tc.receiptLines = receiptLines;
		tc.serialNumberList = serialNumberList;
		tc.fixedAssets = fixedAssetList;
		SCMFixedAsset.testingContext = tc;

		List<FAM__Fixed_Asset__c> result = SCMFixedAsset.updateFixedAssetsWhenReceived(receiptLines);

		System.assert(result.size() == 1, 'The test should return 1 fixed asset.');
		FAM__Fixed_Asset__c resultingFixedAsset = result.get(0);
		System.debug('<ojs> resultingFixedAsset:\n' + resultingFixedAsset);
		System.assert(resultingFixedAsset.Serial_Number_Reference__c == sn.Id, 'The fixed asset does NOT have the correct Serial Number.');
		System.assert(resultingFixedAsset.Deployed_To__c == acct.Id, 'The fixed asset is NOT deployed to the correct account.');
		System.assertEquals(
			Date.today(), 
			resultingFixedAsset.Deployed_Date__c, 
			'The fixed asset does NOT have the correct received date.');
	}

	@isTest
	static void updateFixedAssetSerialNumberReferences()
	{
		// Create old Serial Number record.
		SCMC__Serial_Number__c oldSerialNumber = new SCMC__Serial_Number__c();
		oldSerialNumber.Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.SObjectType);
		oldSerialNumber.SCMC__Serial_Number__c = '12345';

		// Create new Serial Number record.
		SCMC__Serial_Number__c newSerialNumber = new SCMC__Serial_Number__c();
		newSerialNumber.Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.SObjectType);
		newSerialNumber.SCMC__Serial_Number__c = '12345';

		// Create Fixed Asset.
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Serial_Number_Reference__c = oldSerialNumber.Id;
		asset.Serial_Number_Reference__r = oldSerialNumber;

		// Configure Testing Context.
		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.fixedAssets = new List<FAM__Fixed_Asset__c> {asset};
		SCMFixedAsset.testingContext = tc;

		// Execute test.
		List<FAM__Fixed_Asset__c> result = SCMFixedAsset.updateSerialNumberReferences(new List<SCMC__Serial_Number__c> {newSerialNumber});

		// Analyze results.
		System.assert(result != null && !result.isEmpty(), 'The results were null or empty.');
		System.assertEquals(1, result.size(), 'The number of results should be 1.');
		System.assertEquals(
			newSerialNumber.Id, 
			result.get(0).Serial_Number_Reference__c, 
			'The resulting Fixed Asset\'s Serial Number reference is not correct.');
	}

	@isTest
	static void fixedAssetTriggerTest()
	{
		c2g__codaCompany__c company = new c2g__codaCompany__c();
		company.Name = 'Test Company, LLC';
		insert company;

		FAM__FA_Depreciation_Method__c depreciationMethod = new FAM__FA_Depreciation_Method__c();
		depreciationMethod.Name = 'Test Depreciation Method';
		depreciationMethod.FAM__Calculation_Class__c = 'Test';
		insert depreciationMethod;

		FAM__Fixed_Asset__c fixedAsset = new FAM__Fixed_Asset__c();
		fixedAsset.FAM__Asset_Description__c = 'A fixed Asset for testing.';
		fixedAsset.FAM__Date_In_Service__c = Date.today();
		fixedAsset.FAM__Time_Period_Basis__c = 'Monthly';
		fixedAsset.FAM__Asset_Cost__c = 7777.77;
		fixedAsset.FAM__Salvage_Value__c = 777.77;
		fixedAsset.FAM__Service_Life_in_Years__c = 7.0;
		fixedAsset.Company__c = company.Id;
		fixedAsset.FAM__Depreciation_Method__c = depreciationMethod.Id;
		insert fixedAsset;

		System.assertNotEquals(null, fixedAsset.Id, 'The Fixed Asset insert did NOT work properly.');

		fixedAsset.FAM__Asset_Description__c = 'updated';
		update fixedAsset;

		System.assertEquals('updated', fixedAsset.FAM__Asset_Description__c, 'The Fixed Asset update did NOT work properly.');
	}

	@isTest(SeeAllData=true)
	static void exerciseDomainFeatures()
	{
		SCMFixedAsset.addConsumerField('Company__r.Name');
		SCMFixedAsset fixedAssets = SCMFixedAsset.Create_AllUpToLimit();
		SCMFixedAsset.clearConsumerFieldList();
		System.debug('<ojs> fixedAssets.Records:\n' + fixedAssets.Records);

		Integer count = 0;
		Set<Id> idSet = new Set<Id>();
		for (FAM__Fixed_Asset__c item : fixedAssets.Records) {
			if (item.Serial_Number_Reference__c != null) {
				idSet.add(item.Serial_Number_Reference__c);
				count++;
			}
			if (count >= 5) break;
		}
		SCMFixedAsset fa = SCMFixedAsset.Create_FromSerialNumberIds(idSet);

		count = 0;
		idSet = new Set<Id>();
		for (FAM__Fixed_Asset__c item : fixedAssets.Records) {
			idSet.add(item.Id);
			if (count++ >= 5) break;
		}
		fa = SCMFixedAsset.Create_ForIds(idSet);

		fa = SCMFixedAsset.Create_WithRecords(fixedAssets.Records);
	}

	@isTest(SeeAllData=true)
	static void createFixedAsset_properCase() {
		createElementsForTest();

		List<c2g__codaGeneralLedgerAccount__c> glAccountList = new List<c2g__codaGeneralLedgerAccount__c>();
		glAccountList.add(new c2g__codaGeneralLedgerAccount__c());
		glAccountList.get(0).c2g__ReportingCode__c = '17710';
		glAccountList.get(0).Id = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		glAccountList.add(new c2g__codaGeneralLedgerAccount__c());
		glAccountList.get(1).c2g__ReportingCode__c = '18000';
		glAccountList.get(1).Id = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);

		List<Account> accounts = new List<Account>();
		Account acct = new Account();
		acct.Id = SCMSetUpTest.generateMockId(Account.SObjectType);
		accounts.add(acct);

		SCMFixedAsset.TestContext tc = new SCMFixedAsset.TestContext();
		tc.isTest = true;
		tc.accounts = accounts;
		tc.generalLedgerAccounts = glAccountList;
		tc.items = new List<SCMC__Item__c> {item};
		tc.inventoryPositions = new List<SCMC__Inventory_Position__c> {inventoryPosition};
		tc.companyId = SCMSetUpTest.generateMockId(c2g__codaCompany__c.SObjectType);
		tc.picklistDetails = new List<SCMC__Pick_list_Detail__c> {picklistDetail};
		tc.productionOrders = new List<SCMC__Production_Order__c> {productionOrder};
		tc.productionOrderLines = productionOrderLines;
		tc.serialNumberList = serialNumberList;
		SCMFixedAsset.testingContext = tc;

		List<FAM__Fixed_Asset__c> result = SCMFixedAsset.createFixedAssets(new List<SCMC__Receiving_Inspection__c> {receivingInspection});
		System.debug('<ojs> result:\n' + result);
	}

	private static SCMC__Item__c item = null;
	private static SCMC__Inventory_Position__c inventoryPosition = null;
	private static SCMC__Pick_list_Detail__c picklistDetail = null;
	private static SCMC__Production_Order__c productionOrder = null;
	private static List<SCMC__Production_Order_Line__c> productionOrderLines = new List<SCMC__Production_Order_Line__c>();
	private static SCMC__Receiving_Inspection__c receivingInspection = null;
	private static List<SCMC__Serial_Number__c> serialNumberList = null;

	private static void createElementsForTest(){

		// Create the dependent stuff.
		SCMC__Address__c address = testHelper.createTestAddress();
		SCMC__ICP__c icp = testHelper.createTestICP(address);
		SCMC__Warehouse__c warehouse = testHelper.createTestWarehouse(icp, address);
		warehouse = queryWarehouses(WarehouseSelector.Warehouse, new Set<Id> {warehouse.Id}).get(0);
		System.debug('<ojs> warehouse:\n' + warehouse);

		SCMC__Inventory_Location__c location = testHelper.createTestLocation(warehouse);
		System.debug('<ojs> Inventory Locations:\n' + queryInventoryLocations(InventoryLocationSelector.Warehouse, new Set<Id> {warehouse.Id}));

		item =
			new SCMC__Item__c(
				Id = SCMSetUpTest.generateMockId(SCMC__Item__c.SObjectType),
				Asset_Group__c = SCMSetUpTest.generateMockId(FAM__Asset_Group__c.SObjectType),
				SCMC__Item_Description__c = 'An item for testing Fixed Asset creation',
				SCMC__Kit__c = 'Manufactured kit');
		System.debug('<ojs> Item:\n' + item);

		// Create the Production Order.
		productionOrder =
			new SCMC__Production_Order__c(
				Id = SCMSetUpTest.generateMockId(SCMC__Production_Order__c.SObjectType),
				SCMC__Completion_Date__c = System.today());
		System.debug('<ojs> productionOrder:\n' + productionOrder);

		// Create the Production Order Lines.
		productionOrderLines.add(
			new SCMC__Production_Order_Line__c(
				Id = SCMSetUpTest.generateMockId(SCMC__Production_Order_Line__c.SObjectType),
				SCMC__Item_Master__c = item.Id,
				SCMC__Production_Order__c = productionOrder.Id,
				SCMC__Quantity__c = 2));
		System.debug('<ojs> productionOrderLines:\n' + productionOrderLines);

		// Create Picklist detail.
		picklistDetail = new SCMC__Pick_list_Detail__c(
			Id = SCMSetUpTest.generateMockId(SCMC__Pick_list_Detail__c.SObjectType),
			SCMC__Production_Order_Line__c = productionOrderLines.get(0).Id,
			SCMC__Cost__c = 777.77);
		System.debug('<ojs> picklistDetail:\n' + picklistDetail);

		// Create the Receiving Inspection.
		receivingInspection = new SCMC__Receiving_Inspection__c(
			Id = SCMSetUpTest.generateMockId(SCMC__Receiving_Inspection__c.SObjectType),
			SCMC__Production_Order__c = productionOrder.Id,
			SCMC__Quantity_to_Accept__c = 2,
			SCMC__Status__c = 'Complete');
		System.debug('<ojs> receivingInspection:\n' + receivingInspection);

		// Create the Inventory Position.
		inventoryPosition = new SCMC__Inventory_Position__c(
			Id = SCMSetUpTest.generateMockId(SCMC__Inventory_Position__c.SObjectType),
			SCMC__Current_Value__c = 777.55,
			SCMC__Item_Master__c = item.Id,
			SCMC__Receiving_Inspection__c = receivingInspection.Id,
			SCMC__Serial_Number_Lookup__c = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.SObjectType));
		System.debug('<ojs> inventoryPosition:\n' + inventoryPosition);

		serialNumberList = new List<SCMC__Serial_Number__c>();
		serialNumberList.add (
			new SCMC__Serial_Number__c (
				Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.sObjectType),
				SCMC__Receiving_Inspection__c = receivingInspection.Id,
				SCMC__Production_Order_Line__c = productionOrderLines.get(0).Id));
		serialNumberList.add (
			new SCMC__Serial_Number__c (
				Id = SCMSetUpTest.generateMockId(SCMC__Serial_Number__c.sObjectType),
				SCMC__Receiving_Inspection__c = receivingInspection.Id,
				SCMC__Production_Order_Line__c = productionOrderLines.get(0).Id));
		System.debug('<ojs> serialNumberList:\n' + serialNumberList);
	}

	private enum ActionQueueSelector {PullForPicklist}
	private static List<SCMC__Inventory_Action_Queue__c> queryActionQueues(ActionQueueSelector selector, Set<Id> idSet)
	{
		List<SCMC__Inventory_Action_Queue__c> result = new List<SCMC__Inventory_Action_Queue__c>();
		List<String> fieldList = new List<String>(SCMC__Inventory_Action_Queue__c.sObjectType.getDescribe().fields.getMap().keySet());
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Inventory_Action_Queue__c ';
		if (selector == ActionQueueSelector.PullForPicklist)
		{
			soql += 'where SCMC__Action__c = \'Pull Parts for Production Order\' and SCMC__Picklist__c in :idSet';
		}
		List<SObject> objectList = Database.query(soql);
		for (SObject obj : objectList) result.add((SCMC__Inventory_Action_Queue__c)obj);
		return result;
	}

	private enum InventoryLocationSelector {Warehouse}
	private static List<SCMC__Inventory_Location__c> queryInventoryLocations(InventoryLocationSelector selector, Set<Id> idSet)
	{
		List<SCMC__Inventory_Location__c> result = new List<SCMC__Inventory_Location__c>();
		List<String> fieldList = new List<String>(SCMC__Inventory_Location__c.sObjectType.getDescribe().fields.getMap().keySet());
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Inventory_Location__c ';
		if (selector == InventoryLocationSelector.Warehouse)
		{
			soql += 'where SCMC__Warehouse__c in :idSet';
		}
		List<SObject> objectList = Database.query(soql);
		for (SObject obj : objectList) result.add((SCMC__Inventory_Location__c)obj);
		return result;
	}

	private enum ItemMasterSelector {ItemMaster}
	private static List<SCMC__Item__c> queryItemMasters(ItemMasterSelector selector, Set<Id> idSet)
	{
		List<SCMC__Item__c> result = new List<SCMC__Item__c>();
		List<String> fieldList = new List<String>(SCMC__Item__c.sObjectType.getDescribe().fields.getMap().keySet());
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Item__c ';
		if (selector == ItemMasterSelector.ItemMaster)
		{
			soql += 'where Id in :idSet';
		}
		List<SObject> objectList = Database.query(soql);
		for (SObject obj : objectList) result.add((SCMC__Item__c)obj);
		return result;
	}

	private enum SerialNumberSelector {None}
	private static List<SCMC__Serial_Number__c> querySerialNumbers(SerialNumberSelector selector, Set<Id> idSet)
	{
		List<SCMC__Serial_Number__c> result = new List<SCMC__Serial_Number__c>();
		List<String> fieldList = new List<String>(SCMC__Serial_Number__c.sObjectType.getDescribe().fields.getMap().keySet());
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Serial_Number__c ';
		List<SObject> objectList = Database.query(soql);
		for (SObject obj : objectList) result.add((SCMC__Serial_Number__c)obj);
		return result;
	}

	private static List<String> queryWarehouses_AdditionalFields = new List<String>();
	private enum WarehouseSelector {Warehouse}
	private static List<SCMC__Warehouse__c> queryWarehouses(WarehouseSelector selector, Set<Id> idSet)
	{
		List<SCMC__Warehouse__c> result = new List<SCMC__Warehouse__c>();
		List<String> fieldList = new List<String>(SCMC__Warehouse__c.sObjectType.getDescribe().fields.getMap().keySet());
		fieldList.addAll(queryWarehouses_AdditionalFields);
		String soql = 'select ' + String.join(fieldList, ',') + ' from SCMC__Warehouse__c ';
		if (selector == WarehouseSelector.Warehouse)
		{
			soql += 'where id in :idSet';
		}
		List<SObject> objectList = Database.query(soql);
		for (SObject obj : objectList) result.add((SCMC__Warehouse__c)obj);
		return result;
	}
}