/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/
public with sharing class FAMDepreciationScheduleService {

	private static SCMFFIJournal factory = null;

	public static List<c2g__codaJournal__c> postDepreciation(list<FAM__FA_Depreciation_Schedule__c> depreciations) {

		if (depreciations == null || depreciations.isEmpty()) return new List<c2g__codaJournal__c>();

		// Create a clone of the input so that the customer's data is not removed.
		depreciations = depreciations.clone();

		// Remove the items having Export Error Status other than 'New'.
		for (Integer i = depreciations.size() - 1; 0 <= i; i--) {
			FAM__FA_Depreciation_Schedule__c sched = depreciations.get(i);
			if (!sched.FAM__Active__c && !'New'.equals(sched.Export_Error_Status__c)) {
				depreciations.remove(i);
			}
		}

		// Create map structure containing all of the information for each depreciation schedule.
		UnitOfWorkMap uowMap = new UnitOfWorkMap(depreciations);

		// Ensure that related Fixed Assets have both GL accounts.
		for (Integer i = uowMap.values().size() - 1; 0 <= i; i--) {
			UnitOfWork uow = uowMap.values().get(i);
			if (uow.asset.Fixed_Asset_GL_Account__c == null || uow.asset.Depreciation_GL_Account__c == null) {
				uowMap.uowMap.remove(uow.depreciationSchedule.Id);
			}
		}

		boolean transactionError = false;

		// Create and populate the export-error status and message maps.
		for (UnitOfWork uow : uowMap.values()){
			uow.depreciationSchedule.Export_Error_Status__c = 'New';
			uow.depreciationSchedule.Export_Error_Message__c = '';
		}

		// Journal ===============================================================
		uowMap = createJournals(uowMap);

		system.savepoint databaseSavepoint = database.setSavepoint();

		// Post the journals
		List<c2g__codaJournal__c> journalObjects = uowMap.getJournals();
		System.debug('Journals to insert ' + journalObjects);
		List<Database.Saveresult> saveResults = new List<Database.Saveresult>();
		if (!testContext.isTest) {
			saveResults = database.insert(journalObjects, false);
		} else {
			journalObjects = testContext.addIdValue(journalObjects);
		}

		// Address any failures --
		//  * Record the error to the DepreciationSchedule item
		//  * Use AddError so that the problem is elevated up to the originating page.
		database.Saveresult saveResultItem = null;
		UnitOfWork uowItem = null;
		for (integer i = 0; i < saveResults.size(); i++){
			saveResultItem = saveResults[i];
			if (!saveResultItem.isSuccess()){
				transactionError = true;
				uowItem = uowMap.get(journalObjects[i].Depreciation_Schedule__c);
				uowItem.hasError = true;
				uowItem.depreciationSchedule.Export_Error_Status__c = 'Export Error';
				uowItem.depreciationSchedule.Export_Error_Message__c = JSON.serializePretty(saveResultItem.getErrors());
				System.debug('hasError ' + JSON.serializePretty(saveResultItem.getErrors()));
			}
		}

		// Journal Lines ===============================================================
		uowMap = createJournalLines(uowMap);

		// Populate the line items with the journal's new ID, and post.
		for (UnitOfWork uow : uowMap.values()){
			if (uow.hasError) continue;
			uow.journalLine_increasing.c2g__Journal__c = uow.journal.id;
			uow.journalLine_decreasing.c2g__Journal__c = uow.journal.id;
		}

		// Post the journal lines
		list<c2g__codaJournalLineItem__c> journalLines = uowMap.getJournalLines();
		System.debug('journal lines to insert ' + journalLines);
		saveResults = new List<Database.Saveresult>();
		if (!testContext.isTest) {
			saveResults = database.insert(journalLines, false);
		} else {
			journalLines = testContext.addIdValue(journalLines);
		}

		// Address any failures, as above.
		for (integer i = 0; i < saveResults.size(); i++){
			saveResultItem = saveResults[i];
			if (!saveResultItem.isSuccess()){
				transactionError = true;
				uowItem = uowMap.findDepreciationScheduleForJournalId(journalLines[i].c2g__Journal__c);
				uowItem.hasError = true;
				uowItem.depreciationSchedule.Export_Error_Status__c = 'Export Error';
				uowItem.depreciationSchedule.Export_Error_Message__c = JSON.serializePretty(saveResultItem.getErrors());
				System.debug('has Error insert error ' + saveResultItem.getErrors());
			}
		}

		// Conclusion ===============================================================
		if (transactionError){
			database.rollback(databaseSavepoint);
			for (UnitOfWork uow : uowMap.values()) uow.depreciationSchedule.FAM__Active__c = false;
		} else
			for (UnitOfWork uow : uowMap.values()){
				uow.depreciationSchedule.Export_Error_Status__c = 'Exported';
				uow.depreciationSchedule.Export_Error_Message__c = '';
			}

		return journalObjects;
	}

	private static UnitOfWorkMap createJournals(UnitOfWorkMap uowMap){

		if (uowMap == null || uowMap.getCountWithoutErrors() < 1) return uowMap;

		// Construct the structures of data required for the Journal and line factory object.
		list<FAM__FA_Depreciation_Schedule__c> depreciationScheduleList = new list<FAM__FA_Depreciation_Schedule__c>();
		map<id, FAM__Fixed_Asset__c> idToFixedAssetMap = new map<id, FAM__Fixed_Asset__c>();
		for (UnitOfWork uow : uowMap.values()) {
			if (String.isNotBlank(uow.errorMessage)) continue;
			depreciationScheduleList.add(uow.depreciationSchedule);
			idToFixedAssetMap.put(uow.depreciationSchedule.id, uow.asset);
		}
		if (depreciationScheduleList.size() == 0) return uowMap;  // There's nothing to do.

		// Implement the factory.
		if (!testContext.isTest) {
			factory = SCMFFJournal.create(depreciationScheduleList, idToFixedAssetMap);
		} else {
			factory = testContext.journalMock;
		}
		map<id, c2g__codaJournal__c> resultingMap = factory.getJournals();

		// Extract the resulting data from the factory.
		for (UnitOfWork uow : uowMap.values()) {
			uow.journal = resultingMap.get(uow.depreciationSchedule.id);
		}

		return uowMap;
	}

	private static UnitOfWorkMap createJournalLines(UnitOfWorkMap uowMap){

		System.debug('uowmap ' + uowMap);
		if (uowMap == null || uowMap.getCountWithoutErrors() < 1) return uowMap;
		if (factory == null) throw new SCMException('Create the journals before creating lines.');

		// Implement the factory.
		map<id, c2g__codaJournalLineItem__c> resultingJournalLineIncreasingMap = factory.getJournalLinesDecreasing();
		map<id, c2g__codaJournalLineItem__c> resultingJournalLineDecreasingMap = factory.getJournalLinesIncreasing();

		// Extract the resulting data from the factory.
		for (UnitOfWork uow : uowMap.values()) {
			if (uow.hasError) continue;
			uow.journalLine_increasing = resultingJournalLineIncreasingMap.get(uow.journal.Depreciation_Schedule__c);
			uow.journalLine_decreasing = resultingJournalLineDecreasingMap.get(uow.journal.Depreciation_Schedule__c);
		}

		return uowMap;
	}

	private class UnitOfWork{
		public FAM__FA_Depreciation_Schedule__c depreciationSchedule = null;
		public FAM__Fixed_Asset__c asset = null;
		public boolean hasError = false;
		public String errorMessage = null;
		public c2g__codaJournal__c journal = null;
		public c2g__codaJournalLineItem__c journalLine_increasing = null;
		public c2g__codaJournalLineItem__c journalLine_decreasing = null;
		// Methods
		// Constructors
		private UnitOfWork() {}
		public UnitOfWork(FAM__FA_Depreciation_Schedule__c item){
			depreciationSchedule = item;
		}
	}

	private class UnitOfWorkMap {
		// Properties
		public map<id, UnitOfWork> uowMap = new map<id, UnitOfWork>();
		// Public Methods
		public UnitOfWork get(id value) { return uowMap.get(value); }
		public list<UnitOfWork> values() { return uowMap.values(); }
		public integer getCountWithoutErrors(){
			integer count = 0;
			for(UnitOfWork uow : uowMap.values())
				if (!uow.hasError)
					count++;
			return count;
		}
		public list<FAM__FA_Depreciation_Schedule__c> getDepreciationSchedules(){
			list<FAM__FA_Depreciation_Schedule__c> resultList = new list<FAM__FA_Depreciation_Schedule__c>();
			for(UnitOfWork uow : uowMap.values()) resultList.add(uow.depreciationSchedule);
			return resultList;
		}
		public list<c2g__codaJournal__c> getJournals(){
			list<c2g__codaJournal__c> resultList = new list<c2g__codaJournal__c>();
			for (UnitOfWork uow : uowMap.values()) {
				if (!uow.hasError) resultList.add(uow.journal);
			}
			return resultList;
		}
		public list<c2g__codaJournalLineItem__c> getJournalLines(){
			list<c2g__codaJournalLineItem__c> resultList = new list<c2g__codaJournalLineItem__c>();
			for (UnitOfWork uow : uowMap.values()) {
				if (!uow.hasError) {
					resultList.add(uow.journalLine_increasing);
					resultList.add(uow.journalLine_decreasing);
				}
			}
			return resultList;
		}
		public UnitOfWork findDepreciationScheduleForJournalId(id journalId){
			for (UnitOfWork uow : uowMap.values())
				if (uow.journal.id == journalId) return uow;
			return null;
		}
		// Private Methods
		private void put(Id idValue, UnitOfWork item) { uowMap.put(idValue, item); }
		private void getFixedAssets(){

			// Create a set of asset Ids.
			set<ID> idSet = new set<ID>();
			for (UnitOfWork uow : uowMap.values()) idSet.add(uow.depreciationSchedule.FAM__Fixed_Asset__r.Id);

			// Retrieve the assets.
			SCMFixedAsset.addConsumerField('FAM__Asset_Group__r.Depreciation_GL_Account__c');
			SCMFixedAsset.addConsumerField('FAM__Asset_Group__r.Accumulated_Depreciation_GL_Account__c');
			SCMFixedAsset.addConsumerField('FAM__Asset_Group__r.Fixed_Assets_GL_Account__c');

			SCMFixedAsset assets = null;
			if (!testContext.isTest) {
				assets = SCMFixedAsset.Create_ForIds(idSet);
			} else {
				assets = SCMFixedAsset.Create_WithRecords(testContext.fixedAssets);
			}
			System.debug('FixedAssets ' + assets.Records);

			// Place the retrieved assets in the related-information map.
			for (UnitOfWork uow : uowMap.values())
			{
				uow.asset = assets.RecordMap.get(uow.depreciationSchedule.FAM__Fixed_Asset__r.Id);
				if(String.isNotBlank(uow.errorMessage)) {
					uow.hasError = true;
					System.debug('hasError ' + uow.errorMessage);
				}
			}
		}
		// Constructors
		private UnitOfWorkMap() {}
		public UnitOfWorkMap(list<FAM__FA_Depreciation_Schedule__c> depreciations){
			for (FAM__FA_Depreciation_Schedule__c item : depreciations)
				uowMap.put(item.Id, new UnitOfWork(item));
			getFixedAssets();
		}
	}

	//==================================================================================================================================
	//  Test Functionality
	//==================================================================================================================================

	@TestVisible
	private static TestingContext testContext = new TestingContext();

	public class TestingContext
	{
		public Boolean isTest = false;

		public SCMFFJournalMock journalMock = null;

		public List<FAM__Fixed_Asset__c> fixedAssets = null;

		public List<SObject> addIdValue(List<SObject> items)
		{
			if (items == null || items.isEmpty()) return items;
			Integer fakeIdCount = 0;
			String keyPrefix = items.get(0).getSObjectType().getDescribe().getKeyPrefix();
			for (SObject item : items) {
				String fakeIdPrefix = '000000000000'.substring(0, 12 - fakeIdCount.format().length());
				item.Id = Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount++);
			}
			return items;
		}
	}
}