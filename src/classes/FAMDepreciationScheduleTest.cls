/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
@isTest()
public with sharing class FAMDepreciationScheduleTest {

	@isTest()
	static void exportToFFA()
	{
		FAM__Fixed_Asset__c fixedAsset = new FAM__Fixed_Asset__c();
		fixedAsset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		fixedAsset.FAM__Status__c = 'Active';
		fixedAsset.Depreciation_GL_Account__c = SCMSetupTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		fixedAsset.Fixed_Asset_GL_Account__c = SCMSetupTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);

		FAM__FA_Depreciation_Schedule__c depreciation = new FAM__FA_Depreciation_Schedule__c();
		depreciation.Id = SCMSetupTest.generateMockId(FAM__FA_Depreciation_Schedule__c.SObjectType);
		depreciation.FAM__Active__c = true;
		depreciation.Export_Error_Message__c = '';
		depreciation.Export_Error_Status__c = 'New';
		depreciation.FAM__Fixed_Asset__r = fixedAsset;

		// ---------- Configure the Journal Mock ----------
		Map<Id, c2g__codaJournal__c> journalMap = new Map<Id, c2g__codaJournal__c>();
		c2g__codaJournal__c journal = new c2g__codaJournal__c();
		journal.Depreciation_Schedule__c = depreciation.Id;
		journalMap.put(depreciation.Id, journal);

		Map<Id, c2g__codaJournalLineItem__c> journalLineMap_Increasing = new Map<Id, c2g__codaJournalLineItem__c>();
		c2g__codaJournalLineItem__c journalLine = new c2g__codaJournalLineItem__c();
		journalLineMap_Increasing.put(depreciation.Id, journalLine);

		Map<Id, c2g__codaJournalLineItem__c> journalLineMap_Decreasing = new Map<Id, c2g__codaJournalLineItem__c>();
		journalLine = new c2g__codaJournalLineItem__c();
		journalLineMap_Decreasing.put(depreciation.Id, journalLine);

		SCMFFJournalMock journalMock = new SCMFFJournalMock();
		journalMock.getJournalsMap = journalMap;
		journalMock.getJournalLinesIncreasingMap = journalLineMap_Increasing;
		journalMock.getJournalLinesDecreasingMap = journalLineMap_Decreasing;

		// Configure the text context.
		FAMDepreciationScheduleService.TestingContext tc = new FAMDepreciationScheduleService.TestingContext();
		tc.isTest = true;
		tc.journalMock = journalMock;
		tc.fixedAssets = new List<FAM__Fixed_Asset__c> {fixedAsset};
		FAMDepreciationScheduleService.testContext = tc;

		// ---------- Run the test ----------
		List<c2g__codaJournal__c> results = FAMDepreciationScheduleService.postDepreciation(new List<FAM__FA_Depreciation_Schedule__c> {depreciation});

		// ---------- Analyze the results -----------
		System.debug('<ojs> results:\n' + results);
		System.assert(results != null && !results.isEmpty(), '');
	}
}