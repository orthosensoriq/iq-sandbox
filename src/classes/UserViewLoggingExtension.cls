public with sharing class UserViewLoggingExtension
{
    string userId;
    
    public UserViewLoggingExtension(ApexPages.StandardController stdController)
    {
        userId = ((User)stdController.getRecord()).id;
    }
    
    public void LogConstructor()
    {
        if(userId == System.UserInfo.getUserId())
        {
            map<integer, string> params = new map<integer, string>();
            params.put(0, userId);
            osController baseCtrl = new osController();
            list<sObject> tempUser = baseCtrl.getSelectedData('UserViewLogging', params); 
        }
    }
}