/*****************************************************
This class is designed to provide definitions for 
Trigger CRUD audit logging for OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public with sharing class osTriggerHandler extends BaseTriggerHandler // implements ITriggerHandler.IPatientTriggerHandler
{
	final String CLASSNAME = '\n\n**** osTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
	//@date : 12/18/2013
	//@description : the class constructor method
	//@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
	//@returns : nothing
    public osTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
    	String METHODNAME = CLASSNAME.replace('METHODNAME', 'osTriggerHandler') + ' - class constructor';
    	system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

		// trigger is executing
        TriggerIsExecuting = isExecuting;
		
		// set batch size
        BatchSize = pTriggerSize;
		
		// set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }
}