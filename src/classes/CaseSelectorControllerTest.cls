@IsTest(SeeAllData=true)
private class CaseSelectorControllerTest
{
    static testmethod void testCaseSelectorController()
    {
		//constructor
		PageReference testPage1 = Page.CaseSelector;
		Test.setCurrentPage(testPage1);   
		ApexPages.currentPage().getParameters().put('accid', '000000000000000');
        
        CaseSelectorController csc1 = new CaseSelectorController();
        //gotoProcedureCreation
        PageReference out1 = csc1.gotoProcedureCreation();

  		PageReference testPage2 = Page.CaseSelector;
		Test.setCurrentPage(testPage2);   
		ApexPages.currentPage().getParameters().put('intid', '000000000000000');
        
        CaseSelectorController csc2 = new CaseSelectorController();
        //gotoProcedureCreation
        PageReference out2 = csc2.gotoProcedureCreation();

	    //getCaseTypeOptions
	    List<selectOption> testOptions = csc2.getCaseTypeOptions();
        
	}
}