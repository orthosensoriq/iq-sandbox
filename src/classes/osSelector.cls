/*****************************************************
This class is designed to provide select functionality
as part of the audit logging for OrthoSensor

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
public without sharing class osSelector
{
    private sObject obj {get;set;}
    private String objName
    {
        get
        {
            if(String.isBlank(objName))
            {
                objName = objToken.getDescribe().getName();
            }
            return objName;
        }
        set;
    }
    private Schema.SObjectType objToken
    {
        get
        {
            if(objToken == null && obj != null)
            {
                objToken = obj.getSObjectType();
                objName = objToken.getDescribe().getName();
            }
            return objToken;
        }
        set;
    }

    private string objId
    {
        get
        {
            if(String.isBlank(objId))
            {
                objId = (String)obj.get('Id');
            }
            return objId;
        }
        set;
    }

	public String selectStatement
    {
        get
        {
            if(String.isBlank(selectStatement))
            {
                Set<string> fields;
                if(obj != null)
                {
                    fields = obj.getSObjectType().getDescribe().fields.getMap().keySet();
                }
                else if(objToken != null)
                {
                    fields = objToken.getDescribe().fields.getMap().keySet();
                }
                else
                {
                    return null;
                }
                String queryString = 'SELECT Id';
                for(String s: fields)
                {
                    if(s!='Id') queryString +=  ', ' + s;
                }
                queryString += ' FROM ' + objName;
                if(String.isNotBlank(objId))
                {
                    queryString += ' WHERE id = \'' + objId + '\'';
                }
                system.debug(queryString);
                selectStatement = queryString;
            }
            return selectStatement;
        }
        private set;
    }

    public osSelector(){}
    public osSelector(String inputId)
    {
        if(String.isBlank(inputId)) return;
        objToken = Id.valueOf(inputId).getSObjectType();
        objName = objToken.getDescribe().getName();
        objId = inputId;
    }
    public osSelector(sObject inputObject)
    {
        obj = inputObject;
    }

    public List<sObject> getSelectedData()
    {
        if(String.isBlank(selectStatement)) return null;
        system.debug('osSelector.getSelectedData: ' + selectStatement);
        return Database.query(selectStatement);
    }

    public void setSelectStatement(string pWhich, map<integer, string> params)
    {
        if(pWhich == 'PatientViewCase')
            selectStatement = 'SELECT Id, Name, Name__c, Case_Type__c,Case_Type__r.Description__c, Description__c, Active__c, CreatedDate FROM Case__c where Patient__r.Name = \'' + params.get(0) + '\'';
        else if (pWhich == 'CaseSelectorProcedure')
            selectStatement = 'Select Id, Case__c, Case__r.Case_Type__c from Event__c where Id = \'' + params.get(0) + '\'';
        else if (pWhich == 'CaseViewEvent')
            selectStatement = 'SELECT Id, Name, Case__c, Patient__c, Physician__r.Name, Physician__c, Status__c, Location__r.Name, Location__c, Event_Type__r.Name, Laterality_For_Forms__c, Parent_Event__c, Parent_Event__r.Laterality_For_Forms__c, appointment_start__c FROM Event__c where Case__r.Name = \'' + params.get(0) + '\' order by appointment_start__c';
        else if (pWhich == 'ProcedureSummary')
        {
            string tempSelect = 'Select Id, Name, Description__c, Patient_ID__c, Parent_Event__c, Patient__c, Physician__c, Physician__r.Id, Physician__r.Name, Case__r.Patient__c, Case__r.Patient__r.First_Name__c, Location__c, Location__r.Id, Location__r.Name, Event_Name__c, Appointment_Start__c, ';
			tempSelect += 'Event_Type__r.Name, Status__c, Appointment_Status__c, Appointment_End__c, Appointment_Comments__c , Case__r.Patient__r.Last_Name__c, Case__r.Patient__r.Gender__c, Case__r.Name, Case__r.Patient__r.Date_Of_Birth__c, Case__r.Case_Type__r.Description__c, ';
			tempSelect += 'Case__r.Patient__r.Medical_Record_Number__c, Case__r.Patient__r.SSN__c, Case__r.Patient__r.Account_Number__c, ';
			tempSelect += '(Select Id, Height__c, Weight__c, Height_UOM__c, Weight_UOM__c, Laterality__c, Calculated_BMI__c, Primary_Diagnosis__c, Primary_Diagnosis__r.Name, ';
			tempSelect += 'Secondary_Diagnosis__c, Secondary_Diagnosis__r.Name, Anesthesia_Type__c, Allergy_Name__c, Patient_Consent_Obtained__c from Clinical_Data__r) ';
			tempSelect += 'from Event__c where ID = \'' + params.get(0) + '\'';
        	selectStatement = tempSelect;
        }
        else if (pWhich == 'UserViewLogging')
            selectStatement = 'Select Id, Name, FirstName, LastName, Email, UserName, ProfileId, Accepted_EULA_Version__c, Accepted_HIPAA_Version__c, LastViewedDate FROM User where id = \'' + params.get(0) + '\' ORDER by LastViewedDate Desc LIMIT 1';
        else if (pWhich == 'EventFormViewResponses')
            selectStatement = 'Select Id, Name, Survey_Question__c, Survey_Question_Text__c, Survey_Response__c FROM Survey_Response__c where event_form__c = \'' + params.get(0) + '\' ORDER by Survey_Question__c';
        else if (pWhich == 'ImplantSummaryProcedure')
        {
            string tempSelect = 'Select Id, Name, Patient__c, Case__r.Patient__r.Medical_Record_Number__c, ';
        	tempSelect += 'Location__r.Name, Location__r.BillingStreet,Location__r.BillingCity, Case__r.Patient__c, ';
        	tempSelect += 'Location__r.BillingState, Location__r.BillingCountry, Location__r.BillingPostalCode, ';
			tempSelect += 'Case__r.Patient__r.Account_Number__c, Physician__r.Name, Description__c,' ;
            tempSelect += 'Appointment_Start__c, Case__r.Patient__r.Date_Of_Birth__c,Case__r.Case_Type__r.Description__c, ';
            tempSelect += 'Case__r.Patient__r.Address_Line_1__c, Case__r.Patient__r.Address_Line_2__c, Case__r.Patient__r.Address_Line_3__c, ';
            tempSelect += 'Case__r.Patient__r.City__c, Case__r.Patient__r.State__c, Case__r.Patient__r.Zip_Postal_Code__c ';
			tempSelect += 'from event__c where id =\'' + params.get(0) + '\'';
            selectStatement = tempSelect;
        }
        else if (pWhich == 'ImplantSummaryList')
        {
            string tempSelect = 'select Id, Event__c, Name, Expiration_Date__c, Is_Hospital_Product__c, ';
            tempSelect += 'Lot_Number__c, Manufacturer__c, Manufacturer_SKU__c, ';
            tempSelect += 'Product_Catalog_Id__c, Product_Catalog_Id__r.Product_Name__c, ';
            tempSelect += 'Product_Catalog_Id__r.Manufacturer__r.Name, Product_Catalog_Id__r.Manufacturer__r.Source_Record_id__c,';
            tempSelect += 'Source_Record_ID__c, Status__c ';
            tempSelect += 'from   implant_component__c ';
            tempSelect += 'where  Event__c =\'' + params.get(0) + '\' ';
            tempSelect += 'order by CreatedDate';
            selectStatement = tempSelect;
        }
        else if (pWhich == 'ImplantComponentsAggregate')
        {
        	string tempSelect = 'Select count(Id) qty, Product_Catalog_Id__c, Product_Catalog_Id__r.Name partnumber, lot_number__c, ';
            tempSelect += 'Product_Catalog_Id__r.Product_Name__c,Product_Catalog_Id__r.Manufacturer__r.Name, Status__c ';
            tempSelect += 'from   implant_component__c ';
            tempSelect += 'where  Event__c =\'' + params.get(0) + '\' ';
            tempSelect += 'group by Product_Catalog_Id__c, Product_Catalog_Id__r.Name, Product_Catalog_Id__r.Product_Name__c, lot_number__c, ';
            tempSelect += 'Product_Catalog_Id__r.Manufacturer__r.Name, Status__c';
			selectStatement = tempSelect;
        }
        else if (pWhich == 'ImplantSummaryAttachments')
            selectStatement = 'Select Id, Name From Attachment where Name = \'' + params.get(1) + '\' and ParentId =\'' + params.get(0) + '\' order by LastModifiedDate desc Limit 1';
        else if (pWhich == 'UserSettingsUser')
            selectStatement = 'SELECT Id, Name, ContactId, Accepted_EULA_Version__c, Accepted_HIPAA_Version__c FROM User WHERE ID =\'' + params.get(0) + '\'';
        else if (pWhich == 'UserSettingsContact')
            selectStatement = 'SELECT Id, Name, FirstName, LastName, Phone, MobilePhone FROM Contact WHERE Id =\'' + params.get(0) + '\'';
        else if (pWhich == 'ProcedureListings')
        {
            string tempSelect = 'Select Id, Name, Appointment_Start__c, Description__c,Laterality_For_Forms__c, ';
			tempSelect += 'Case__c, Case__r.Patient__c, Case__r.Patient__r.Name, Case__r.Patient__r.Last_Name__c, ';
			tempSelect += 'Case__r.Patient__r.First_Name__c, Case__r.Patient__r.Gender__c, Case__r.Patient__r.Date_Of_Birth__c, ';
			tempSelect += 'Case__r.Patient__r.Medical_Record_Number__c, Case__r.Case_Type__r.Description__c, Physician__c, ';
			tempSelect += 'Physician__r.Name, Physician__r.Id, Location__c, Location__r.Name ';
			tempSelect += 'FROM Event__c WHERE Event_Type__c =\'' + params.get(0) + '\' AND Location__c!=null LIMIT 999';
            selectStatement = tempSelect;
        }
        else if (pWhich == 'ProcedureListingPatients')
            selectStatement = 'Select Id, Name, First_Name__c, Last_Name__c, Gender__c, Hospital__r.Name, Date_Of_Birth__c, Medical_Record_Number__c from Patient__c  where Active__c = true AND Hospital__r.Id = \'' + params.get(0) + '\'';
        else if (pWhich == 'EventFormsList')
            selectStatement = 'SELECT Id, Name, survey__r.Name__c, Event_Form_URL__c, Mandatory__c, Form_Complete__c, Start_Date__c, End_Date__c FROM Event_Form__c WHERE Event__c = \'' + params.get(0) + '\'';
        else if (pWhich == 'ProcedureSummaryPatient')
            selectStatement = 'Select Id, Name, First_Name__c, Last_Name__c, Calculated_Age__c, Language__c, Medical_Record_Number__c, Account_Number__c, SSN__c, Date_Of_Birth__c, Gender__c, Race__c from Patient__c where Id= \'' + params.get(0) + '\'';

        system.debug('osSelector(soql): ' + selectStatement);
    }

}