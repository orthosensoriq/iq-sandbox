public with sharing class AttachmentUploadController {

     public Id recId {get;set;}
     public string fileName {get;set;}
     public Blob fileBody {get;set;}

     private ApexPages.StandardController stdController;
     public String redirectUrl {get;set;}
     public Boolean shouldRedirect {get;set;}

     public AttachmentUploadController(ApexPages.StandardController ctlr)
     {
		recId = ctlr.getRecord().Id;
		this.stdController = stdController;
		shouldRedirect = false;
     }

     public PageReference UploadFile()
     {
          //PageReference pr = new PageReference('/apex/AttachmentUpload?id='+recID);
          if(fileBody != null && fileName != null)
          {
			system.debug('fileName =====> '+fileName);
               Integer dotLoc = fileName.lastIndexOf('.');
			Attachment myAttachment  = new Attachment();
			myAttachment.Body = fileBody;
			myAttachment.Name = fileName;
			myAttachment.Description = 'Manual Upload';
               if (dotLoc != -1) myAttachment.ContentType = fileName.substring(dotLoc+1);
               if (dotLoc == -1) myAttachment.ContentType = 'unknown';
			myAttachment.ParentId = recId;
			insert myAttachment;

			//clear body of uploaded file to remove from view state
               fileBody = null;

			shouldRedirect = true;
			redirectUrl = '/apex/ProcedureSummary?procid='+recId+'&reloadDocs=Y';
			system.debug('redirectUrl =====> '+redirectUrl);

			//pr.setRedirect(true);
			//return pr;

			// Don't do a page redirect when you plan to redirect the URL of the parent page
               return null;
          }
          return null;
     }
}