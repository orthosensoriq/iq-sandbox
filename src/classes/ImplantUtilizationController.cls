public with sharing class ImplantUtilizationController {
	
	public String[] selectedHospitals = new String[]{};
    public String[] selectedSurgeons = new String[]{};
    public String[] selectedStatuses = new String[]{};
    public String[] selectedLotNumbers = new String[]{};
    public List<Implant_Component__c> implantCompLines {get;set;}
	public boolean showExport {get;set;}
    public string searchParams {get;set;}

    public ImplantUtilizationController() {
		implantCompLines = new List<Implant_Component__c>();
        showExport = false;
        searchParams = '';
	}

	public void getResults() 
    {
        searchParams = '';
        implantCompLines.clear();

        if(selectedHospitals.size() == 0 && selectedSurgeons.size() == 0 && selectedStatuses.size() == 0 && selectedLotNumbers.size() == 0)
        {
            showExport = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter at Least one search criteria value');
            ApexPages.addMessage(myMsg);
        }
        else
        {
            showExport = true;
            String searchQuery = 'SELECT Id, Name, Status__c, Lot_Number__c, Manufacturer__c, Manufacturer_SKU__c, Serial_Number__c, Expiration_Date__c,';
            searchQuery = searchQuery + ' Event__r.Name, Event__r.Event_Type__r.Name, Event__r.Location__r.Name, Event__r.Physician__r.Name FROM Implant_Component__c WHERE IsDeleted = False';


            if (selectedHospitals.size() > 0) {
                String hospitalIDs = '\'' + String.join(selectedHospitals,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Event__r.Location__c IN ('+hospitalIDs+')';
                
                set<string> tempSet = new set<string>(selectedHospitals);
                searchParams +=  'Hospital in (';
                for(selectOption hosp : getHospitalItems())
                {
                    if(tempSet.contains(hosp.getValue()))
	                    searchParams += hosp.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedSurgeons.size() > 0) {
                String surgeonIDs = '\'' + String.join(selectedSurgeons,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Event__r.Physician__c IN ('+surgeonIDs+')';
                
                set<string> tempSet = new set<string>(selectedSurgeons);
                searchParams +=  'Surgeon in (';
                for(selectOption surg : getSurgeonItems())
                {
                    if(tempSet.contains(surg.getValue()))
	                    searchParams += surg.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedStatuses.size() > 0) {
                String statusIDs = '\'' + String.join(selectedStatuses,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Status__c IN ('+statusIDs+')';
                
                set<string> tempSet = new set<string>(selectedStatuses);
                searchParams +=  'Status in (';
                for(selectOption status : getStatusItems())
                {
                    if(tempSet.contains(status.getValue()))
	                    searchParams += status.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            if (selectedLotNumbers.size() > 0) {
                String lotNumberIDs = '\'' + String.join(selectedLotNumbers,'\',\'') + '\'';
                searchQuery = searchQuery + ' AND Lot_Number__c IN ('+lotNumberIDs+')';
                
                set<string> tempSet = new set<string>(selectedLotNumbers);
                searchParams +=  'Lot Number in (';
                for(selectOption LotNum : getLotNumberItems())
                {
                    if(tempSet.contains(LotNum.getValue()))
	                    searchParams += LotNum.getLabel() + ',';
                }
                searchParams = searchParams.substringBeforeLast(',') + '),';
            }

            searchQuery = searchQuery + ' ORDER BY Name LIMIT 1000';
            system.debug('searchQuery =====> '+searchQuery);
            implantCompLines = Database.query(searchQuery);

            if(implantCompLines.size()==0 || implantCompLines == null)
            {
                showExport = false;
              	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results were found');
              	ApexPages.addMessage(myMsg);
            }
            searchParams = searchParams.substringBeforeLast(',');
        }
    }

    public String[] getHospitals() {
        return selectedHospitals;
    }

    public void setHospitals(String[] hospitals) {
        this.selectedHospitals = hospitals;
    }

    public String[] getSurgeons() {
        return selectedSurgeons;
    }

    public void setSurgeons(String[] surgeons) {
        this.selectedSurgeons = surgeons;
    }

    public String[] getLotNumbers() {
        return selectedLotNumbers;
    }

    public void setLotNumbers(String[] lotNumbers) {
        this.selectedLotNumbers = lotNumbers;
    }

    public String[] getStatuses() {
        return selectedStatuses;
    }

    public void setStatuses(String[] statuses) {
        this.selectedStatuses = statuses;
    }

    public List<selectOption> getHospitalItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE isPartner = true and RecordType.Name = 'Hospital Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }

    public List<selectOption> getSurgeonItems() {
        List<selectOption> options = new List<selectOption>();
        for (Account a : [SELECT Id, Name  from Account WHERE RecordType.Name = 'Surgeon Account' ORDER BY Name ASC LIMIT 1000]) { 
            options.add(new selectOption(a.id, a.Name));
        }
        return options;
    }

    public List<selectOption> getStatusItems() {
        List<selectOption> options = new List<selectOption>();
        Schema.DescribeFieldResult statusFieldDescription = Implant_Component__c.Status__c.getDescribe();
        for (Schema.Picklistentry picklistEntry : statusFieldDescription.getPicklistValues()) {
            options.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
        }
        return options;
    }

    public List<selectOption> getLotNumberItems() {
        List<selectOption> options = new List<selectOption>();
        AggregateResult[] lotNumAggr = [SELECT Count(Id), Lot_Number__c from Implant_Component__c WHERE Lot_Number__c != null GROUP BY Lot_Number__c Order by Lot_Number__c];
        for (AggregateResult ln : lotNumAggr) {
            String lotNumVal = String.valueOf(ln.get('Lot_Number__c')); 
            options.add(new selectOption(lotNumVal, lotNumVal));
        }
        return options;
    }
}