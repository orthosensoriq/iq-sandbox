public with sharing class MedicalProceduresController{
/*
require 'date'
class MedicalProceduresController < ApplicationController

  before_filter :validated_object, :only => [:edit, :update, :show]

  def new
    @defaults  = {:primary_diagnosis => 'GENERAL OSTEOARTHROSIS', :procedure => 'TOTAL KNEE ARTHROPLASTY'}
    @medical_procedure = MedicalProcedure.new
    @patient           = Patient.new
    @appointment       = Appointment.new
    @address           = Address.new
    @profile           = Profile.new
    @business_entity   = BusinessEntity.find(params[:business_entity_id])
    @physician         = Physician.new
  end

  def create
    # Extract each object involved in patient/procedure creation
    _procedure   = params[:medical_procedure]
    _appointment = params[:appointment].merge({:end_time => params[:appointment][:start_time]})
    _patient     = params[:patient].merge({:business_entity_id => params[:business_entity_id]})
    _profile     = params[:profile]
    _diagnoses   = params[:secondary_diagnoses].split("; ").compact.reject(&:blank?)
    _allergies   = params[:allergies].split(",").collect(&:strip).compact.reject(&:blank?)
    _address     = params[:address]

    # HACK: DOB needs reformating because current format isn't Timestamp compatible
    unless _profile['date_of_birth'].nil?
      d = _profile['date_of_birth'].split('/')
      _profile['date_of_birth'] = [d[2], d[0], d[1]].join('/') # fmt: YYYY/MM/DD 
    end

    # SETUP FOR COLLECTION
    @errors = []

    # Check if patient already exists in the system and if he
    # belongs in the current business entity
    patient = Patient.where(:medical_record_number => _patient[:medical_record_number]).first
    if patient and _patient[:business_entity_id].to_i != patient.business_entity_id
      @errors << "NOT AUTHORIZED TO CREATE PROCEDURE FOR THE MEDICAL RECORD"
    else
      # Patient does not exist
      # Create Profile, Patient, and Address 
      
      if patient.nil?
        profile = Profile.new(_profile.symbolize_keys)
        patient = Patient.new(_patient.symbolize_keys)
        if patient.valid? and profile.valid?
          if profile.save
            patient.profile_id = profile.id
            patient.save
          end
        else
          @errors << profile unless profile.valid?
          @errors << patient unless patient.valid?
        end
      end

      _address = _address.merge({:addressable => patient})
      address = Address.new(_address.symbolize_keys)
      if @errors.length == 0 and patient.addresses.length == 0
        @errors << address unless address.save
      # TODO: Uncomment if we want to update info at this point
      # elsif @errors.length == 0 and patient.addresses.length != 0 
      #   address = patient.addresses.first
      #   address.update_attributes(_address.symbolize_keys)
      else
        @errors << address unless address.valid?
      end
      ### 

      # Create Appointment and Procedure
      appointment = patient.appointments.new(_appointment.symbolize_keys)
      _procedure = _procedure.merge({
        :medical_procedure_status_id => MedicalProcedureStatus.where(:status_desc => 'Scheduled').first.id,
        :procedure_id => Procedure.find_by_short_description(params[:procedures].strip).try(:id),
        :primary_diagnoses => Diagnosis.find_by_short_description(params[:primary_diagnosis]),
        :secondary_diagnoses => Diagnosis.where('short_description in (?)', _diagnoses),
        :allergies => Allergy.where('name in (?)', _allergies)
      })
      procedure = MedicalProcedure.new(_procedure.symbolize_keys)
      if @errors.length == 0 and appointment.valid? and procedure.valid?
        appointment.save
        procedure.appointment_id = appointment.id
        procedure.save

        dkey = appointment.start_time.strftime("%m/%d/%Y")
        bkey = appointment.patient.business_entity_id
        logger.info "RebuildCache JOB ENQUEUED: {:type => appointment, :date => #{dkey}, :business_entity_id => #{bkey}}"
        Resque.enqueue(RebuildCache, {:type => 'appointment', :date => dkey, :business_entity_id => bkey}) unless Rails.env == 'staging' or Rails.env == 'development'
        redirect_to patient_medical_procedure_path(patient, procedure)
      else
        @errors << appointment unless appointment.valid?
        @errors << procedure   unless procedure.valid?
      end
      ###
      
    end

    render :action => "new" unless @errors.length == 0

  end

  def start_procedure
    @patient = Patient.find params[:patient_id]
    @medical_procedure = MedicalProcedure.find(params[:id])
    @profile = @patient.profile   
    response = get_response("http://localhost:8080/OrthoSensor/gui-on")
    if(response.include? "ON")
      @result = true
      @medical_procedure.update_attribute('medical_procedure_status_id',2)
    else
      @result = false
    end
    respond_to do |format|
      format.js
    end
  end

  def secondary_diagnoses
    @medical_procedure_id=params[:medical_procedure_id]
    @name = []
    ids = params[:"secondary_diagnosis_ids"]
    ids = ids.split(',').compact.reject(&:blank?).collect{|id| @name << Diagnosis.find_by_code(id).short_description } unless ids.blank?
    @name = @name.join('; ')
  end

  def primary_diagnosis
    @medical_procedure_id=params[:medical_procedure_id]
    if params[:primary_diagnosis]
      @primary_diagnosis_name = Diagnosis.find_by_code(params[:primary_diagnosis]).short_description
    end
  end

  def allergies
    @medical_procedure_id=params[:medical_procedure_id]
    @allergies_name = []
    ids = params[:allergy_ids]
    ids = ids.split(',').compact.reject(&:blank?).collect{|id| @allergies_name << Allergy.find(id).name } unless ids.blank?
    @allergies_name = @allergies_name.join(',')
  end

  def procedures
    @medical_procedure_id=params[:medical_procedure_id]
    if params[:procedure]
      @procedure_name = Procedure.find(params[:procedure]).short_description.gsub("\n", '')
    end
    respond_to do |format|
      format.js
    end

  end

  def show
    @medical_procedure = MedicalProcedure.find(params[:id])
  end

  def edit
    @success   = true
    @defaults  = {:primary_diagnosis => 'GENERAL OSTEOARTHROSIS', :procedure => 'TOTAL KNEE ARTHROPLASTY'}
    @profile   = @patient.profile
    @address   = @patient.addresses.first
    @procedure = @medical_procedure.procedure
    @business_entity = @patient.business_entity
  end

  def update
    @primary_diagnosis = Diagnosis.paginate(:per_page => Diagnosis::PRIMARY_PER_PAGE, :page => params[:page])
    @allergies= Allergy.paginate(page: params[:page], per_page: Diagnosis::PRIMARY_PER_PAGE)
    @diagnosis = Diagnosis.paginate(:per_page => Diagnosis::PRIMARY_PER_PAGE, :page => params[:page])
    @procedures = Procedure.paginate(:per_page => Diagnosis::PRIMARY_PER_PAGE, :page => params[:page])
    @address = @patient.addresses.first rescue nil
    @success = true
    dob_to_a(params[:medical_procedure]) #format specific to the database
    params[:medical_procedure][:procedure_id] = Procedure.find_by_short_description(params[:procedures].strip).try(:id)
    pri_diagnosis = Diagnosis.find_by_short_description(params[:primary_diagnosis])
    if pri_diagnosis
      @medical_procedure.primary_diagnoses = pri_diagnosis
    else
      @medical_procedure.primary_diagnoses = nil
    end
    if @medical_procedure.update_attributes(params[:medical_procedure])
      diagnoses = params[:secondary_diagnoses].split("; ").compact.reject(&:blank?) #filter names from params
      allergies = params[:allergies].split(",").collect(&:strip).compact.reject(&:blank?)
      @appointment.update_attribute('status','started') if @appointment.status.blank?
      @medical_procedure.secondary_diagnoses = Diagnosis.where('short_description IN (?)', diagnoses) rescue nil
      @medical_procedure.allergies = Allergy.where('name IN (?)', allergies) rescue nil
      #@medical_procedure.laterality = params[:laterality]
      @medical_procedure.save
      #@medical_procedure.update_attribute(:laterality, params[:laterality])

      # why are we updating laterality here?
      #@medical_procedure.procedure.update_attribute(:laterality, params[:laterality])
      
      @address.update_attributes(params[:address]) 
      @medical_procedure.reload

      dkey = @appointment.start_time.strftime("%m/%d/%Y")
      bkey = @appointment.patient.business_entity_id
      logger.info "RebuildCache JOB ENQUEUED: {:type => appointment, :date => #{dkey}, :business_entity_id => #{bkey}}"
      Resque.enqueue(RebuildCache, {:type => 'appointment', :date => dkey, :business_entity_id => bkey}) unless Rails.env == 'staging' or Rails.env == 'development'
    else
      @success = false
    end
    @recently_viewed_patients =  @patient.business_entity.viewed_patients(3)
    unless @success
      params.delete(:p_paginate)
      params.delete(:s_paginate)
      params.delete(:proc_paginate)
      params.delete(:a_paginate)
      @profile = @patient.profile
      #      render :action => 'edit'
    end
  end

  private

  def validated_object
    if params[:patient_id]
      #@patient = Rails.cache.fetch("cached_patient_id_#{params[:patient_id]}", :expires_in => 30.days) do
         @patient = Patient.where(id: params[:patient_id]).includes('profile').first rescue nil
      #end
      if !@patient
        flash[:notice] = 'Patient not found'
        redirect_to root_path
        return
      end
    end

    if params[:appointment_id]
      #@appointment = Rails.cache.fetch("cached_appointment_id_#{params[:appointment_id]}", :expires_in => 30.days) do
        @appointment = Appointment.where(id: params[:appointment_id]).first rescue nil
      #end
      #      if (@appointment.blank? or (@appointment.id != @patient.appointments.first.id))
      #        flash[:notice] = "No Medical Procedure record found for the patient #{@patient.name}" #        redirect_to root_path
      #        return
      #      end
    end

    #@medical_procedure = Rails.cache.fetch("cached_procedure_id_#{params[:id]}", :expires_in => 30.days) do
     @medical_procedure = MedicalProcedure.where(id: params[:id]).includes('appointment').first rescue nil
    #end
    #    if (@medical_procedure.blank? or (@appointment && @appointment.medical_procedure.id != @medical_procedure.id))
    #      #      flash[:notice] = "No Medical Procedure record found for the patient #{@patient.name}"
    #      redirect_to root_path
    #    end

  end

  def dob_to_a(dob)
    dob = dob[:appointment_attributes][:patient_attributes][:profile_attributes][:date_of_birth]
    return nil if dob.blank?
    dob_arr = dob.split('/')
    params[:medical_procedure][:appointment_attributes][:patient_attributes][:profile_attributes][:date_of_birth] = dob_arr[2] + "-" + dob_arr[0] + "-" + dob_arr[1]
  end

  def pagination_request?
    @pagination_request ||=  (params[:p_paginate] || params[:s_paginate] || params[:proc_paginate] || params[:a_paginate])
  end
end */
}