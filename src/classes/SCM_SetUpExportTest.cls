public with sharing class SCM_SetUpExportTest {
	
	private SCMC__Condition_Code__c cCode = this.createConditionCode('New', 'NE');
    private SCMC__Currency_Master__c curr = this.createTestCurrency();
    private static Integer suffix = 0;

    public static Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();

    public SCMC__Purchase_Order__c createPurchaseOrder(SCMC__Supplier_Site__c supplier, boolean doSave)
    {
        SCMC__Purchase_Order__c po = new SCMC__Purchase_Order__c();

        po.SCMC__Supplier_Site__c = supplier.Id;
        po.SCMC__Status__c = 'Open';
        po.SCMC__Purchase_Order_Date__c = Date.today();

        if (doSave){
        	insert po;
        }
        return po;
    }

    public SCMC__Supplier_Site__c createTestSupplier() {
        SCMC__Supplier_Site__c sc = new SCMC__Supplier_Site__c();
        sc.name = 'Test supplier for unit tests';
        sc.SCMC__Currency__c = curr.Id;
        sc.SCMC__Preferred_Communication_Sourcing__c = 'E-Mail';
        sc.SCMC__Preferred_Communication_Purchase_Order__c = 'E-Mail';
        sc.SCMC__E_Mail__c = 'test@test.com';
        sc.SCMC__Active__c = True;
        insert sc;

        //needed to by pass the validation rule.
        sc.SCMC__Active__c = True;
        update sc;

        return sc;
    }

   public SCMC__Currency_Master__c createTestCurrency() {
   		return createTestCurrency('CAD', true);
   }
   public SCMC__Currency_Master__c createTestCurrency(String cname, Boolean corporate) {
        SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
        try {
            curr = [select id, name
                    from SCMC__Currency_Master__c
                    where name = :cname];
        } catch (Exception ex) {
            curr.name = cname;
            curr.SCMC__Active__c = true;
            curr.SCMC__Conversion_Rate__c = 1;
            curr.SCMC__Corporate_Currency__c = corporate;
            insert curr;
        }
        return curr;
    }

    public SCMC__Condition_Code__c createConditionCode(String code, String ilscode) {
        SCMC__Condition_Code__c cCode = new SCMC__Condition_Code__c();
        try {
            cCode = [select id
                       from SCMC__Condition_Code__c
                      where name = :code];
        } catch (Exception ex) {
            cCode.name = code;
            cCode.SCMC__description__c = 'Description for ' + code;
            cCode.SCMC__Disposal_Code__c = '1 -Excellent';
            cCode.SCMC__Supply_Code__c = 'A -Serviceable';
            insert cCode;
        }
        return cCode;
    }

    public SCMC__Purchase_Order_Line_Item__c createPurchaseOrderLine(SCMC__Purchase_Order__c po, SCMC__Item__c item, SCMC__Warehouse__c warehouse, Boolean doInsert, Boolean active) {

        SCMC__Purchase_Order_Line_Item__c line = new SCMC__Purchase_Order_Line_Item__c();

        line.RecordTypeId = SCM_ExportUtilities.getRecordType('Item', line);
        line.SCMC__Purchase_Order__c = po.Id;
        line.SCMC__Supplier_Commitment_Date__c = Date.today();
        line.SCMC__Supplier_Current_Promise_Date__c = Date.today();
        line.SCMC__Status__c = 'Open';
        line.SCMC__Item_Master__c = item.id;
        line.SCMC__Quantity__c = 10;
        line.SCMC__Unit_Cost__c = 7.50;

        if (warehouse != null)
        {
            line.SCMC__Warehouse__c = warehouse.Id;
        }

        if (doInsert){
            insert line;
        }

        return line;

    }

 	public SCMC__Item__c createTestItem() {
        SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'Test' + suffix++;
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
    	//im.RecordTypeId = SCM_ExportUtilities.getRecordType('Master', im);
    	insert im;
        return im;
    }

    public SCMC__Unit_of_Measure__c createUnitofMeasure(String name, String ilsName) {
        SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
        try {
            uom = [select id
                    from SCMC__Unit_of_Measure__c
                    where name = :name];
        } catch(Exception ex) {
            uom.name =name;
            if (ilsName <> null) {
                uom.SCMC__ILS_Unit_of_Measure__c = ilsName;
                uom.SCMC__Valid_for_ILS__c = true;
            }
            insert uom;
        }
        return uom;
    }

    public SCMC__Warehouse__c setupWarehouse() {
    	SCMC__Address__c address = createTestAddress();
    	SCMC__ICP__c icp = createTestICP(address);
    	return createTestWarehouse(icp, address);
    }

    public SCMC__Address__c createTestAddress() {
        SCMC__Address__c address = new SCMC__Address__c();
        address.name = 'Test Address';
        address.SCMC__City__c = 'A City';
        address.SCMC__Country__c = 'Country';
        address.SCMC__Line1__c = 'Address line 1';
        address.SCMC__PostalCode__c = 'Postcd';
        address.SCMC__State__c = 'State';
        insert address;
        return address;
    }

    public SCMC__ICP__c createTestICP(SCMC__Address__c address) {
        SCMC__ICP__c icp = new SCMC__ICP__c();
        icp.SCMC__Address__c = address.id;
        icp.name = 'Test ICP';
        icp.SCMC__Currency__c = curr.id;
        insert icp;
        return icp;
    }

    public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address){
    	return createTestWarehouse(icp, address, true);
    }

    public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address, boolean doInsert){
        SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
        warehouse.SCMC__ICP__c = icp.id;
        warehouse.SCMC__Address__c = address.id;
        warehouse.name = 'Test Warehouse';
        if (doInsert) {
        	insert warehouse;
        }
        return warehouse;
    }

    public ID createSalesRep(){
    	//TODO - at some point actually create a user
        return UserInfo.getUserId();
    }

	public SCMC__Tax_Code__c createTaxCode(){
		SCMC__Tax_Code__c newTaxcode = new SCMC__Tax_Code__c();
		list<SCMC__Tax_Code__c> taxtList = [Select Id from SCMC__Tax_Code__c where Name = 'YT'];
		if(taxtList.size() > 0){
			newTaxcode = taxtList[0];
		} else {
			newTaxcode.name = 'YT';
			newTaxcode.SCMC__Description__c = 'Tax for billing to the YT';
			newTaxcode.SCMC__Effective_From__c = system.today();
			newTaxcode.SCMC__Inactive__c = false;
			newTaxcode.SCMC__Tax_Rate__c = 0;
			newTaxcode.SCMC__Rate__c = 0;
			newTaxcode.SCMC__Tax_Agency__c = null;
			newTaxcode.SCMC__Tax_Type__c = 'GST';
			insert newTaxcode;
		}
		return newTaxcode;
	}

    public Account createTestCustomerAccount(Boolean custActive, Boolean doInsert, Boolean onHold, id warehouseSpecific) {
 		
 		SCMC__PO_Payment_Terms__c term = createPOPaymentTerm();
        
        Account custSite = new Account();
        custSite.SCMC__Active__c = custActive;

        SCMC__Tax_Code__c tax = createTaxCode();

        if (onHold){
        	custSite.SCMC__Temporary_Hold__c = 'Finance';
        }else {
        	custSite.SCMC__Temporary_Hold__c = null;
        }
		SCMC__Price_Type__c pType = new SCMC__Price_Type__c(name = 'tstType');
		insert pType;
		
        custSite.AnnualRevenue = null;
        custSite.SCMC__CAGE_Code__c = null;
        custSite.SCMC__Capability_Listing__c = null;
        custSite.SCMC__Certifications__c = null;
        custSite.SCMC__Payment_Terms__c = term.id;
        custSite.SCMC__Credit_Card_Expiration_Date__c = null;
        custSite.SCMC__Credit_Card_Number__c = null;
        custSite.SCMC__Credit_Card_Type__c = null;
        custSite.SCMC__Credit_Card_Validation_Code__c = null;
        custSite.SCMC__Customer__c = true;
        custSite.SCMC__DUNS_Number__c = null;
        custSite.SCMC__Default_Payment_Type__c = null;
        custSite.SCMC__Corp_City__c = 'San Diego';
        custSite.SCMC__Corp_Country__c = 'USA';
        custSite.SCMC__Corp_State_Province__c = 'CA';
        custSite.SCMC__Corp_Line1__c = '247 Ocean Blvd.';
        custSite.SCMC__Corp_Line2__c = null;
        custSite.SCMC__Corp_PostalCode__c = '92130';
        custSite.Description = 'Test customer site';
        custSite.FAX = '555-1234';
        custSite.SCMC__FOB__c = null;
        custSite.SCMC__Freight__c = null;
        custSite.Industry = null;
        custSite.SCMC__Language__c = null;
        custSite.Name = 'Test Customer';
        custSite.NumberOfEmployees = null;
        custSite.Phone = null;
        custSite.Ownership = null;
        custSite.ParentId = null;
        custSite.SCMC__Preferred_Communication__c = 'Fax';
        custSite.SCMC__Provide_Advanced_Ship_Notice__c = false;
        custSite.SIC = null;
        custSite.SCMC__Sales_Rep__c = null;
        custSite.SCMC__Sales_Responsibility__c = null;
        custSite.SCMC__Ship_Via__c = null;
        custSite.SCMC__Small_Business_Designations__c = null;
        custSite.SCMC__Tax_Exemption_Group__c = null;
        custSite.SCMC__Terms__c = null;
        custSite.TickerSymbol = null;
        custSite.Type = null;
        custSite.Website = null;
       	custSite.SCMC__warehouse__c = warehouseSpecific;
       	custSite.SCMC__Price_Type__c = pType.id;

       	custSite.BillingStreet = '123 Main St\nSuite 101\nc/0George';
       	custSite.BillingCity = 'San Francisco';
       	custSite.BillingState = 'CA';
       	custSite.BillingPostalCode = '12345';
       	custSite.BillingCountry = 'US';

       	custSite.ShippingStreet = '123 Main St\nSuite 101\nc/0George';
       	custSite.ShippingCity = 'San Francisco';
       	custSite.ShippingState = 'CA';
       	custSite.ShippingPostalCode = '12345';
       	custSite.ShippingCountry = 'US';

		custSite.SCMC__Ship_Via__c = 'UPS Ground';
		custSite.SCMC__Shipping_Account__c = '123456789';

        if (doInsert){
        	insert custSite;
        }
        return custSite;
    }
	
	public SCMC__PO_Payment_Terms__c createPOPaymentTerm(){
		SCMC__PO_Payment_Terms__c newTerm = new SCMC__PO_Payment_Terms__c();
		newTerm.SCMC__Description__c = '2% 10 Net 30';
		newTerm.SCMC__Basis_for_Due__c = 'Invoice Date';
		newTerm.SCMC__Number_of_Days__c = 30;
		newTerm.SCMC__Discount_Determined_by__c = 'Days';
		newTerm.SCMC__Discount_Percentage__c = 2;
		newTerm.SCMC__Terms_Name__c = '2% 10 Net 30';
		newTerm.SCMC__Terms_Description__c = '2%';
		upsert newTerm;
		return newTerm;
	}
	
    public SCMC__Sales_Order__c createTestSalesOrder(boolean doInsert){
        SCMC__Sales_Order__c so = new SCMC__Sales_Order__c();
        ID salesRep = createSalesRep();
        Account account = createTestCustomerAccount(true, true, false, null);

        so.SCMC__Sales_Order_Date__c = Date.today();
        so.SCMC__Primary_Sales_Rep__c = salesRep;
        so.SCMC__Customer_Account__c = account.Id;
        so.RecordTypeId = SCM_ExportUtilities.getRecordType('Inventory', so);

        if (doInsert){
            insert so;
        }

        return so;
    }

    public SCMC__Sales_Order_Line_Item__c createSalesOrderLine(SCMC__Sales_Order__c so, SCMC__Item__c item, String status, boolean doInsert){
        SCMC__Sales_Order_Line_Item__c line = new SCMC__Sales_Order_Line_Item__c();

        line.RecordTypeId = SCM_ExportUtilities.getRecordType('Item', line);
        line.SCMC__Sales_Order__c = so.Id;
        line.SCMC__Quantity__c = 1;
        line.SCMC__Price__c = 25.00;
        line.SCMC__Item_Master__c = item.Id;
        line.SCMC__Customer_Commitment_Date__c = Date.today();
        line.SCMC__Status__c = status;
        line.SCMC__Condition_Code__c = cCode.Id;

        if (doInsert){
           insert line;
        }
        return line;
    }

    public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Warehouse__c warehouse, SCMC__Inventory_Location__c invLoc, SCMC__Item__c item, boolean doInsert) {

        SCMC__Inventory_Position__c ip = new SCMC__Inventory_Position__c();
        ip.SCMC__Acquisition_Cost__c          = 55.00;
        ip.SCMC__Acquisition_Currency__c      = curr.id;
        ip.SCMC__Availability__c              = true;
        ip.SCMC__Availability_Code__c         = 'In Stock';
        ip.SCMC__Bin__c                       = invLoc.id;
        ip.SCMC__Condition_Code__c            = cCode.id;
        ip.SCMC__Current_Value__c             = 55.00;
        ip.SCMC__ICP_Acquisition_Cost__c      = 55.00;
        ip.SCMC__ICP_Currency__c              = curr.id;
        ip.SCMC__ILS_Eligible__c              = false;
        ip.SCMC__Item_Master__c               = item.Id;
        ip.SCMC__Listed_on_ILS__c             = false;
        ip.SCMC__Lot_Number__c                = null;
        ip.SCMC__Manufacturer_CAGE__c  		  = null;
        ip.SCMC__Owned_By__c                  = null;
        ip.SCMC__Quantity_Allocated__c        = 0;
        ip.SCMC__Quantity__c                  = 100;
        ip.SCMC__Quantity_in_Transit__c       = 0;
        ip.SCMC__Receipt_Line__c              = null;
        ip.SCMC__Receiving_Inspection__c      = null;
        ip.SCMC__Reserve_Price__c             = 60.00;
        ip.SCMC__Revision_Level__c            =  null;
        ip.SCMC__Sale_Price__c                = 65.00;
        ip.SCMC__Item_Serial_Number__c        = null;
        ip.SCMC__Shelf_Life_Expiration__c     = null;
        ip.SCMC__List_Type__c				  = 'Y';
        if(doInsert){
        	insert ip;
        }
        return ip;
    }

    public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse){
    	return createTestLocation(warehouse, true);
    }

    public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse, boolean doInsert) {
        SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c();
        location.name = 'Level1';
        location.SCMC__Level1__c = 'Level1';
        location.SCMC__Warehouse__c = warehouse.id;
        if (doInsert){
        	insert location;
        }
        return location;
    }

	
}