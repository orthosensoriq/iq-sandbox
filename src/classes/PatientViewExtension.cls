public without sharing class PatientViewExtension{
	
	public String PatientID {get;set;}
	public final Patient__c currPatient;
	public List<Case__c> currCases {get; set;}
	public Map<Id, String> caseLateralityMap {get; set;}
	public boolean showImg {get; set;}
	public boolean renderEditView {get; set;}
	osController baseCtrl;
	
	// The extension constructor initializes the private member
	// variable currPatient by using the getRecord method from the standard
	// controller.
	public PatientViewExtension(ApexPages.StandardController stdController) {
		baseCtrl = new osController(stdController);
		this.currPatient = (Patient__c)stdController.getRecord();
		if (currPatient.Gender__c == 'Female') showImg=true;
		if (currPatient.Gender__c != 'Female') showImg=false;
		renderEditView = false;
		PatientID = ApexPages.currentPage().getParameters().get('id');
	}
	
	public void LogConstructor()
	{
		baseCtrl.LogView(); 
		getCases();     	
	}

	public String getAge() {
		Datetime DOB;
		if(currPatient.Date_Of_Birth__c!=null) DOB = currPatient.Date_Of_Birth__c;
    		Integer i;
		if(currPatient.Date_Of_Birth__c!=null) i = currPatient.Date_Of_Birth__c.daysBetween(Date.Today());
		Integer patientAge;
		if(i!=null) patientAge = Integer.valueof(i/365);
    		String fmtDOB;
		if(DOB!=null) fmtDOB= DOB.format('MM/dd/yyyy');
        	return patientAge +' years old ';//('+fmtDOB+')';
	}
    
	public void getCases() {
		map<integer, string> params = new map<integer, string>();
		params.put(0,currPatient.Name);
    		currCases = baseCtrl.getSelectedData('PatientViewCase', params);
		
		caseLateralityMap = new Map<Id, String>();
		for(Event__c e:[Select Id, Laterality_For_Forms__c, Event_Type_Name__c, Case__c from Event__c where Case__c in: currCases AND Event_Type_Name__c='Procedure'])
		{
			String s = caseLateralityMap.get(e.Case__c);
			
			if(s!=null) caseLateralityMap.put(e.Case__c, 'Both');
			else if(e.Laterality_For_Forms__c!=null) caseLateralityMap.put(e.Case__c, e.Laterality_For_Forms__c);
		}
	}

	public PageReference toggleEditView() {
		renderEditView = true;
		return null;
	}

	public void Save(){
		renderEditView = false;
		update currPatient;
	}

	public PageReference cancelEdit() {
		renderEditView = false;
		return null;
	}
}