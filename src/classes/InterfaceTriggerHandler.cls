public class InterfaceTriggerHandler extends BaseTriggerHandler
{
	final String CLASSNAME = '\n\n**** InterfaceTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
	//@date : 12/18/2013
	//@description : the class constructor method
	//@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
	//@returns : nothing
    public InterfaceTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
    	String METHODNAME = CLASSNAME.replace('METHODNAME', 'InterfaceTriggerHandler') + ' - class constructor';
    	system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

		// trigger is executing
        TriggerIsExecuting = isExecuting;
		
		// set batch size
        BatchSize = pTriggerSize;
		
		// set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
    	//Not Called for this Trigger
    }
    
    public override void OnAfterInsert(Map<Id, SObject> insertObject)
    {
    	//Not Called for this Trigger
    }

    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	//Not Called for this Trigger
    }

    public override void OnAfterUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	logTrigger('Update', beforeObject, afterObject, null);
    }
    
    public override void OnBeforeDelete(Map<Id, SObject> deleteObject)
    {
    	//Not Called for this Trigger
    }
    
    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
    	//Not Called for this Trigger
    }
}