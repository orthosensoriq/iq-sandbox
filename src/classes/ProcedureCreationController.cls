public virtual without sharing class ProcedureCreationController{

  public Patient__c patient {get;set;}
  public Event__c procedure {get;set;}
  public Case__c newcase {get;set;}
  public Clinical_Data__c clinicalData {get;set;}
  public Account selectedHospital {get;set;}
  public Account selectedSurgeon {get;set;}
  public List<Contact> surgeonContacts {get;set;}
  public List<Account> surgeonAccounts {get;set;}
  public List<SelectOption> surgeonNames  {get;set;}
  public String surgeonName {get;set;}
  public boolean displayPopup {get; set;} 
  public boolean displayPatientCheckPopUp {get; set;} 
  public String newSurgeonFirstName {get; set;} 
  public String newSurgeonLastName {get; set;} 
  public String newSurgeonNPI {get; set;} 
    transient public List<Patient__c> allPatients {get; set;} 
  public String HospitalID;
  String CaseTypeID;
    string interfaceId;
  ADT_interface__c HL7Rec;
  public List<SurgeonLine> surgeonlines {get;set;}
  public String searchFirstName {get;set;}
  public String searchLastName {get;set;}
  public String searchState {get;set;}
  public List<SelectOption> states  {get;set;}
  public Case_Type__c casetype  {get;set;}
  public Decimal calculatedBMI {get;set;}
  Diagnosis__c defaultPrimaryDiagnosis;
  public Event_Type__c eventType {get;set;}
  public Opportunity tempOpportunity {get;set;}
  public Patient__c matchedPatient {get;set;}
  

  public ProcedureCreationController(String procedureId){
       //Dummy 
  }
        
  public ProcedureCreationController()
    {
        patient = new Patient__c();
        procedure = new Event__c();
        newcase = new Case__c();
        clinicalData = new Clinical_Data__c();
        surgeonNames = new List<SelectOption>();
        allPatients = new List<Patient__c>();
        tempOpportunity = new Opportunity(CloseDate = system.today());
        matchedPatient = new Patient__c();
        
        surgeonlines = new List<SurgeonLine>();
        states = new List<SelectOption>();
        states.add(new SelectOption('', ''));
        states.add(new SelectOption('AL', 'AL'));
        states.add(new SelectOption('AK', 'AK'));
        states.add(new SelectOption('AZ', 'AZ'));
        states.add(new SelectOption('AR', 'AR'));
        states.add(new SelectOption('CA', 'CA'));
        states.add(new SelectOption('CO', 'CO'));
        states.add(new SelectOption('CT', 'CT'));
        states.add(new SelectOption('DE', 'DE'));
        states.add(new SelectOption('FL', 'FL'));
        states.add(new SelectOption('GA', 'GA'));
        states.add(new SelectOption('HI', 'HI'));
        states.add(new SelectOption('ID', 'ID'));
        states.add(new SelectOption('IL', 'IL'));
        states.add(new SelectOption('IN', 'IN'));
        states.add(new SelectOption('IA', 'IA'));
        states.add(new SelectOption('KS', 'KS'));
        states.add(new SelectOption('KY', 'KY'));
        states.add(new SelectOption('LA', 'LA'));
        states.add(new SelectOption('ME', 'ME'));
        states.add(new SelectOption('MD', 'MD'));
        states.add(new SelectOption('MA', 'MA'));
        states.add(new SelectOption('MI', 'MI'));
        states.add(new SelectOption('MN', 'MN'));
        states.add(new SelectOption('MS', 'MS'));
        states.add(new SelectOption('MO', 'MO'));
        states.add(new SelectOption('MT', 'MT'));
        states.add(new SelectOption('NE', 'NE'));
        states.add(new SelectOption('NV', 'NV'));
        states.add(new SelectOption('NH', 'NH'));
        states.add(new SelectOption('NJ', 'NJ'));
        states.add(new SelectOption('NM', 'NM'));
        states.add(new SelectOption('NY', 'NY'));
        states.add(new SelectOption('NC', 'NC'));
        states.add(new SelectOption('ND', 'ND'));
        states.add(new SelectOption('OH', 'OH'));
        states.add(new SelectOption('OR', 'OR'));
        states.add(new SelectOption('PA', 'PA'));
        states.add(new SelectOption('RI', 'RI'));
        states.add(new SelectOption('SC', 'SC'));
        states.add(new SelectOption('SD', 'SD'));
        states.add(new SelectOption('TN', 'TN'));
        states.add(new SelectOption('TX', 'TX'));
        states.add(new SelectOption('UT', 'UT'));
        states.add(new SelectOption('VT', 'VT'));
        states.add(new SelectOption('VA', 'VA'));
        states.add(new SelectOption('WA', 'WA'));
        states.add(new SelectOption('WV', 'WV'));
        states.add(new SelectOption('WI', 'WI'));
        states.add(new SelectOption('WY', 'WY'));
        
        HospitalID = ApexPages.currentPage().getParameters().get('accid');
        surgeonName = ApexPages.currentPage().getParameters().get('surgid');
        CaseTypeID = ApexPages.currentPage().getParameters().get('ctid');
        interfaceId = ApexPages.currentPage().getParameters().get('intid');
        
        if(CaseTypeID!=null)
        {
            caseType = [Select Id, Description__c from Case_Type__c where Id =: CaseTypeID];  
      		eventType = [Select Id, Name, Primary__c from Event_Type__c where Primary__c=true and id in (select event_type__c from case_type_event_type__c where case_type__c =:CaseTypeID) LIMIT 1];        
        }
        if(string.isBlank(interfaceId))
        {
            if(HospitalID!=null) 
            {  
                selectedHospital = [Select Id, Name from Account where Id=: HospitalID];
            }
            if(surgeonName!=null) selectedSurgeon = [Select Id from Account where Id=: surgeonName];
            surgeonContacts = [Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:HospitalID)];
            surgeonAccounts = [Select Id, Name from Account Where personcontactID IN: surgeonContacts];
            surgeonNames.add(new SelectOption('--None--','--None--'));
            for(Account surgeon:surgeonAccounts)
            {
                if(surgeon.Name!=null) surgeonNames.add(new SelectOption(surgeon.Id,surgeon.Name));  
            }
            //surgeonNames.add(new SelectOption('--Add New--','--Add New--'));
        }
        list<Diagnosis__c> defaultPrimaryDiagnosis = new list<Diagnosis__c>();
        for(Diagnosis__c d : [Select Id from Diagnosis__c where Code__c='715.00' LIMIT 1]){
            defaultPrimaryDiagnosis.add(d);
        }
        if(defaultPrimaryDiagnosis!=null) clinicalData.Primary_Diagnosis__c = defaultPrimaryDiagnosis[0].id;
        
  }
    
    Public PageReference CheckForAutoCreation()
    {
        if(!string.isBlank(interfaceId) && interfaceId!='null')
        {
            loadInterfaceProcedure();
        }
        return null;
    }
    
    private void loadInterfaceProcedure()
    {
        //Interface was selected to create this Procedure, need to populate the form.
        osController baseCtrl = new osController();
        list<ADT_interface__c> interfaceRecord = (list<ADT_interface__c>)baseCtrl.getSelectedData(interfaceId);
        if(interfaceRecord.size() == 1)
        {
            HL7Rec = interfaceRecord[0];
            //Build Procedure page
            //Find Hospital
            if(!string.isblank(HL7Rec.SFDC_Account__c))
            {
                selectedHospital = [Select Id, Name, BillingState from Account where Id =:HL7Rec.SFDC_Account__c];
                if(selectedHospital != null)
                    HospitalID = selectedHospital.id;
            }
            
            surgeonContacts = [Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:HospitalID)];
            try
            {
      			selectedSurgeon = [Select Id from Account where personcontactID IN: surgeonContacts and (FirstName =: HL7Rec.Physician_First_Name__c and LastName =: HL7Rec.Physician_Last_Name__c)];
            }
            catch(exception ex)
            {
                selectedSurgeon = null;
            }
            
            //surgeonContacts = [Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:HospitalID)];
            surgeonAccounts = [Select Id, Name from Account Where personcontactID IN: surgeonContacts];
            surgeonNames.add(new SelectOption('--None--','--None--'));
            for(Account surgeon:surgeonAccounts)
            {
                if(surgeon.Name!=null) surgeonNames.add(new SelectOption(surgeon.Id,surgeon.Name));  
            }
            //surgeonNames.add(new SelectOption('--Add New--','--Add New--'));
            if(selectedSurgeon != null)
              surgeonName = selectedSurgeon.id;
            else
            {
                //surgeonName = '--Add New--';
                searchLastName = HL7Rec.Physician_Last_Name__c;
                searchState = selectedHospital.BillingState;
                displayPopUp = true;
            }
            
            //Now set field values
            tempOpportunity.CloseDate = HL7Rec.Appointment_Date__c;
            patient.First_Name__c = HL7Rec.Patient_First_Name__c;
            patient.Last_Name__c = HL7Rec.Patient_Last_Name__c;
            patient.Language__c = HL7Rec.Language_Name__c;
            patient.Medical_Record_Number__c = HL7Rec.Medical_Record_Number__c;
            patient.Account_Number__c = HL7Rec.Account_Number__c;
            patient.Date_Of_Birth__c = Date.newInstance(integer.valueOf(HL7Rec.Patient_DOB__c.substring(0,4)), integer.valueOf(HL7Rec.Patient_DOB__c.substring(4,6)), integer.valueOf(HL7Rec.Patient_DOB__c.substring(6)));
            patient.SSN__c = HL7Rec.Patient_SSN__c;
            patient.Gender__c = HL7Rec.Patient_Gender__c;
            patient.Race__c = HL7Rec.Race_Name__c;
            patient.Hospital__c = selectedHospital.id;
            clinicalData.Laterality__c = HL7Rec.Laterality__c;
            string allergyList = '';
            if(!string.isBlank(HL7Rec.Allergen_Description_1__c)) allergyList = HL7Rec.Allergen_Description_1__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_2__c)) allergyList = HL7Rec.Allergen_Description_2__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_3__c)) allergyList = HL7Rec.Allergen_Description_3__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_4__c)) allergyList = HL7Rec.Allergen_Description_4__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_5__c)) allergyList = HL7Rec.Allergen_Description_5__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_6__c)) allergyList = HL7Rec.Allergen_Description_6__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_7__c)) allergyList = HL7Rec.Allergen_Description_7__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_8__c)) allergyList = HL7Rec.Allergen_Description_8__c + ',';
            if(!string.isBlank(HL7Rec.Allergen_Description_9__c)) allergyList = HL7Rec.Allergen_Description_9__c + ',';
      if(allergyList.contains(',')) allergyList = allergyList.substringBeforeLast(',');
            
            clinicalData.Allergy_Name__c = allergyList;
            clinicalData.Anesthesia_Type__c = HL7Rec.Anesthesias_Description__c;
            if(!string.isBlank(HL7Rec.Height_Units__c) && HL7Rec.Height_Units__c == 'cm')
				clinicalData.Height__c = (!string.isblank(HL7Rec.Height__c) ? (decimal.valueOf(HL7Rec.Height__c)*0.3937008) : null);
            else
                clinicalData.Height__c = (!string.isblank(HL7Rec.Height__c) ? decimal.valueOf(HL7Rec.Height__c) : null);
            
            if(!string.isBlank(HL7Rec.Weight_Units__c) && HL7Rec.weight_Units__c == 'kg')
                clinicalData.Weight__c = (!string.isblank(HL7Rec.Weight__c) ? decimal.valueOf(HL7Rec.Weight__c)*2.2046 : null);
	        else
                clinicalData.Weight__c = (!string.isblank(HL7Rec.Weight__c) ? decimal.valueOf(HL7Rec.Weight__c) : null);
            
            clinicalData.Patient_Consent_Obtained__c = (!string.isblank(HL7Rec.Patient_Consent__c) ? boolean.valueOf(HL7Rec.Patient_Consent__c) : null);

            if(!string.isBlank(HL7Rec.Diagnosis_Code_2__c))
            {
                diagnosis__c HL7Diag2;
                try
                {
                	HL7Diag2 = [Select Id from diagnosis__c where Code__c =: HL7Rec.Diagnosis_Code_2__c];
                }
                catch(exception ex)
                {
                    HL7Diag2 = null;
                }
                if(HL7Diag2 != null)
                  clinicalData.Secondary_Diagnosis__c = HL7Diag2.id;
            }
            calculateBMI();

        }
        
    }
  
  public PageReference createProcedure(){
    if(!Test.IsRunningTest() && (tempOpportunity.CloseDate==null || patient.Last_Name__c==null || patient.First_Name__c==null || patient.Medical_Record_Number__c==null || patient.Date_Of_Birth__c==null || patient.Gender__c==null || surgeonName=='--None--' || surgeonName=='--Add New--' || clinicalData.Laterality__c==null))
    {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Enter all required fields.');
      ApexPages.addMessage(myMsg);
      return null;
    }
    if(!Test.isRunningTest()) allPatients = [Select Id, SSN__c, Name, Soundex_Metaphone_Value__c, Medical_Record_Number__c, First_Name__c, Last_Name__c, Language__c, Gender__c, Race__c, Account_Number__c, Date_Of_Birth__c from Patient__c  where Active__c = true AND Last_Name__c!=null AND First_Name__c!=null AND Hospital__c=: HospitalID LIMIT 50000];
    Boolean matchFound = false;
    Metaphone m = new Metaphone();
    String patientLastNameMetaphone = m.getMetaphone(patient.Last_Name__c);

    for(Patient__c patientToCheck:allPatients)
    {
      if(patient.Last_Name__c!=null && patientToCheck.Last_Name__c!=null && patientToCheck.SSN__c!=null &&  patient.SSN__c!=null && patient.SSN__c == patientToCheck.SSN__c && patient.Last_Name__c.substring(0) == patientToCheck.Last_Name__c.substring(0))
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;
        matchFound = true;
        return null;
      }
      else if(patient.Medical_Record_Number__c!=null && patient.Last_Name__c!=null && patient.Medical_Record_Number__c.equalsignorecase(patientToCheck.Medical_Record_Number__c)  && patient.Last_Name__c.equalsIgnoreCase(patientToCheck.Last_Name__c))
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;  
        matchFound = true;
        return null;
      }
      else if(patient.Date_Of_Birth__c!=null && patientToCheck.Date_Of_Birth__c!=null && patient.SSN__c!=null && patientToCheck.SSN__c!=null && patient.SSN__c == patientToCheck.SSN__c && patient.Date_Of_Birth__c.year() == patientToCheck.Date_Of_Birth__c.year())
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;
        matchFound = true;
        return null;
      }
      else if(patient.Medical_Record_Number__c!=null && patientToCheck.Medical_Record_Number__c!=null && patient.Date_Of_Birth__c!=null && patientToCheck.Date_Of_Birth__c!=null && patient.Medical_Record_Number__c.equalsignorecase(patientToCheck.Medical_Record_Number__c) && patient.Date_Of_Birth__c.year() == patientToCheck.Date_Of_Birth__c.year())
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;
        matchFound = true;
        return null;  
      }
      else if(patient.First_Name__c!=null && patient.Last_Name__c!=null && patient.Date_Of_Birth__c!=null && patientToCheck.First_Name__c!=null && patientToCheck.Last_Name__c!=null && patientToCheck.Date_Of_Birth__c!=null &&  patient.SSN__c!=null &&  patientToCheck.SSN__c!=null && patient.First_Name__c.equalsignorecase(patientToCheck.First_Name__c) && patient.Last_Name__c.equalsignorecase(patientToCheck.Last_Name__c) && patient.Date_Of_Birth__c.year() == patientToCheck.Date_Of_Birth__c.year() && patient.SSN__c == patientToCheck.SSN__c)
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;
        matchFound = true;
        return null;
      }
      
      else if(patient.Last_Name__c!=null && patientToCheck.Last_Name__c!=null &&  patient.SSN__c!=null &&  patientToCheck.SSN__c!=null && patientToCheck.Soundex_Metaphone_Value__c!=null && patientLastNameMetaphone == patientToCheck.Soundex_Metaphone_Value__c && patient.Date_Of_Birth__c!=null && patientToCheck.Date_Of_Birth__c!=null && patient.Date_Of_Birth__c.year() == patientToCheck.Date_Of_Birth__c.year() && patient.SSN__c == patientToCheck.SSN__c)    
      {
        matchedPatient = patientToCheck;
        displayPatientCheckPopUp = true;
        matchFound = true;
        return null;
      }
    
    }
    if(!matchFound || Test.isRunningTest())
    {
      return CreateNewPatient();
      
    }
    return null;
  }
  
  public PageReference CreateNewPatient()
  {

    closePopup();
    try
    {     
      insert patient;  
      
      newcase.Patient__c = patient.id;
      newcase.Case_Type__c = CaseTypeID;
      newcase.Active__c = true;
      if(selectedSurgeon!=null) newcase.Physician__c = selectedSurgeon.id;
      insert newcase;

      if(newcase.id!=null) procedure.Case__c = newcase.id;
      if(selectedSurgeon!=null) procedure.Physician__c = selectedSurgeon.id;
      if(selectedHospital!=null) procedure.Location__c = selectedHospital.id;
      if(eventType!=null) procedure.Event_Type__c = eventType.id;
      if(tempOpportunity.CloseDate!=null) procedure.Appointment_Start__c = datetime.newInstance(tempOpportunity.CloseDate.year(), tempOpportunity.CloseDate.month(),tempOpportunity.CloseDate.day());
      insert procedure;
    

       CreateFollowUpEvents(procedure.Case__c, procedure.Id, CaseTypeID);
    
      Boolean createSecondProcedure = false;
      clinicalData.Event__c = procedure.Id;
      if(clinicalData.Laterality__c == 'Both') 
      {
        clinicalData.Laterality__c = 'Right';
        createSecondProcedure = true;
      }
      insert clinicalData;
      
      if(HL7Rec != null)
        {
            if(HL7Rec.Event__c==null)
            {
            	HL7Rec.Event__c = procedure.Id;
            }
            else
            {
            	HL7Rec.Event2__c = procedure.Id;
            }
            HL7Rec.Processed__c = true;
            if(!createSecondProcedure) update HL7Rec;
        }
      
      if(createSecondProcedure)
      {
        Event__c secondProcedure = procedure.clone();
        insert secondProcedure;
        
        CreateFollowUpEvents(secondProcedure.Case__c, secondProcedure.id, CaseTypeID);
        
        Clinical_Data__c secondClinicalData = clinicalData.clone();
        secondClinicalData.Laterality__c = 'Left';
        secondClinicalData.Event__c = secondProcedure.Id;
        insert secondClinicalData;
        
      	if(HL7Rec != null)
        {
            HL7Rec.Event2__c = secondProcedure.Id;
            update HL7Rec;
        }
      }
      
      return GoToProcedureSummary();  
    }
    catch(System.DMLException e)
    {
      ApexPages.addMessages(e);  
    }
    
    return null;
  }
  
  public PageReference UpdateExistingPatient() //Patient__c patientToUpdate
  {
    closePopup();
    try
    {
      newcase.Patient__c = matchedPatient.id;
      newcase.Case_Type__c = CaseTypeID;
      newcase.Active__c = true;
      if(selectedSurgeon!=null) newcase.Physician__c = selectedSurgeon.id;
      insert newcase;

      if(newcase.id!=null) procedure.Case__c = newcase.id;
      if(selectedSurgeon!=null) procedure.Physician__c = selectedSurgeon.id;
      if(selectedHospital!=null) procedure.Location__c = selectedHospital.id;
      if(eventType!=null) procedure.Event_Type__c = eventType.id;
      if(tempOpportunity.CloseDate!=null) procedure.Appointment_Start__c = datetime.newInstance(tempOpportunity.CloseDate.year(), tempOpportunity.CloseDate.month(),tempOpportunity.CloseDate.day());
      insert procedure;  
	  
	  Boolean createSecondProcedure = false;
      clinicalData.Event__c = procedure.Id;
      if(clinicalData.Laterality__c == 'Both') 
      {
        clinicalData.Laterality__c = 'Right';
        createSecondProcedure = true;
      }
	  
      if(HL7Rec != null)
        {
            if(HL7Rec.Event__c==null)
            {
            	HL7Rec.Event__c = procedure.Id;
            }
            else
            {
            	HL7Rec.Event2__c = procedure.Id;
            }
            HL7Rec.Processed__c = true;
            if(!createSecondProcedure) update HL7Rec;
        }
                
      CreateFollowUpEvents(procedure.Case__c, procedure.id, CaseTypeID);

      if(defaultPrimaryDiagnosis!=null) clinicalData.Primary_Diagnosis__c = defaultPrimaryDiagnosis.id;
      insert clinicalData;
      
      if(createSecondProcedure)
      {
        Event__c secondProcedure = procedure.clone();
        insert secondProcedure;
        
        CreateFollowUpEvents(secondProcedure.Case__c, secondProcedure.id, CaseTypeID);
        
        Clinical_Data__c secondClinicalData = clinicalData.clone();
        secondClinicalData.Laterality__c = 'Left';
        secondClinicalData.Event__c = secondProcedure.Id;
        insert secondClinicalData;
        
      	if(HL7Rec != null)
        {
            HL7Rec.Event2__c = secondProcedure.Id;
            update HL7Rec;
        }
      }
      
      return GoToProcedureSummary();  
    }
    catch(System.DMLException e)
    {
      ApexPages.addMessages(e);  
    }
    
    return null;
  }
  
  @future
  public static void CreateFollowUpEvents(Id CaseId, Id procedureID, Id CaseType){
    
    List<Event_Type__c> eventTypes = new List<Event_Type__c>();
    eventTypes = [Select Id, Interval__c from Event_Type__c WHERE Interval__c!=null AND Primary__c=false AND Id IN (Select Event_Type__c from Case_Type_Event_Type__c WHERE Case_Type__c =:CaseType)];
    //eventTypes = [Select Id, Interval__c from Event_Type__c Where Interval__c!=null AND Primary__c=false];
    Event__c p = [Select Id, Appointment_start__c, Location__c, Physician__c from Event__c WHERE Id=: procedureID];
    
    List<Event__c> eventsToAdd = new List<Event__c>();
    
    for(Event_Type__c et: eventTypes)
    {
      Event__c fuEvent = new Event__c();
      fuEvent.Event_Type__c = et.id; 
      fuEvent.Case__c = CaseId;
      fuEvent.Appointment_Start__c = p.Appointment_Start__c + (et.Interval__c * 7);
      fuEvent.Location__c = p.Location__c;
      fuEvent.Physician__c = p.Physician__c;
      fuEvent.Parent_Event__c = p.id;
      
      eventsToAdd.add(fuEvent);
    }
    
    if(eventsToAdd.size()>0) insert eventsToAdd;
  }
  
  public PageReference GoToProcedureSummary(){
  
    PageReference pageref =  new PageReference('/apex/ProcedureSummary?accid=' + procedure.location__c + '&procid=' + procedure.id + '&surgid=' + procedure.Physician__c);
    return pageref;
    
  }
  
  public PageReference SelectSurgeon(){
    
      if(surgeonName =='--Add New--')
      {
        showPopup();
      }
      else if(surgeonName != '--None--')
      {
        selectedSurgeon = [Select Id from Account where Id=: surgeonName];
      }
      
      return null;
  }
  
  public virtual PageReference AddSurgeon(){
    
    closePopup();

    Account[] existingSurgeons;
    Account existingSurgeon;
    if(newSurgeonNPI!=null)  existingSurgeons = [Select Id, PersonContactId from Account where NPI__c =: newSurgeonNPI LIMIT 1];
    System.debug(LoggingLevel.ERROR,'---newSurgeonNPI = ' + newSurgeonNPI);
	  
    if(existingsurgeons!=null && existingsurgeons.size()>0) existingSurgeon = existingSurgeons[0];

    Account newSurgeon = new Account();
	  System.debug(LoggingLevel.ERROR,'---existingSurgeon = ' + existingSurgeon);
    if (existingSurgeon == null || Test.IsRunningTest())
    {
      newSurgeon.NPI__c = newSurgeonNPI;
      newSurgeon.FirstName = newSurgeonFirstName;
      newSurgeon.LastName = newSurgeonLastName;
      Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
      Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
      Schema.RecordTypeInfo rtByName =  rtMapByName.get('Surgeon Account');
      newSurgeon.RecordTypeId = rtByName.getRecordTypeId();  //'012U0000000UQ3OIAW';
		
		System.debug(LoggingLevel.ERROR,'---newSurgeon = ' + newSurgeon);
      
      insert newSurgeon;
      
      newSurgeon = [Select Id, Name, personContactId from Account where Id=:newSurgeon.Id];
    }
    else
    {
      newSurgeon = existingSurgeon;  
    }
    
    surgeonName = newSurgeon.Id;
    selectedSurgeon = newSurgeon;
    
	System.debug(LoggingLevel.ERROR,'---selectedHospital = ' + selectedHospital);
	System.debug(LoggingLevel.ERROR,'---newSurgeon.PersonContactId = ' + newSurgeon.PersonContactId);
	
    if(selectedHospital!=null && newSurgeon.PersonContactId!=null)
    {
      Surgeon_Hospital__c newSurgeonHospital = new Surgeon_Hospital__c();
      newSurgeonHospital.Surgeon__c = newSurgeon.PersonContactId;
      newSurgeonHospital.Hospital__c = selectedHospital.id;
            try{
        insert newSurgeonHospital;
          }catch(DMLException e){
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
    }
    
    surgeonContacts = [Select Id, Name from Contact WHERE Id IN (Select Surgeon__c from Surgeon_Hospital__c WHERE Hospital__c =:HospitalID)];
    surgeonAccounts = [Select Id, Name from Account Where personcontactID IN: surgeonContacts];
    surgeonNames.clear();
    surgeonNames.add(new SelectOption('--None--','--None--'));
    surgeonNames.add(new SelectOption('--Add New--','--Add New--'));
    for(Account surgeon:surgeonAccounts)
    {
      if(surgeon.Name!=null) surgeonNames.add(new SelectOption(surgeon.Id,surgeon.Name));  
    }
    
    return null;
  }

  public virtual void CalculateBMI(){
    
    if(clinicalData.Weight__c!=null && clinicalData.Height__c!=null)
    {
      Decimal heightDecimal = clinicalData.Height__c;
      Decimal weightDecimal = clinicalData.Weight__c;

      if(clinicalData.Height_UOM__c == 'Inches')
        {
          calculatedBMI = (weightDecimal / (heightDecimal * heightDecimal)) * 703;
          calculatedBMI = calculatedBMI.setScale(2);
        }
      if(clinicalData.Height_UOM__c == 'CM')
        {
            heightDecimal = heightDecimal/100; //convert CM to Meters
          calculatedBMI = (weightDecimal / (heightDecimal * heightDecimal));
          calculatedBMI = calculatedBMI.setScale(2);
        }
    }
  
  }  
  
  public void getSurgeons(){
    
    getContent();  
    
  }
  public void closePopup() {       
    if(surgeonName == '--Add New--') surgeonName = '--None--';
        displayPopup = false;  
    displayPatientCheckPopUp = false;
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
  
   public void getContent() {
    
    if(searchLastName == null || searchLastName == '' || searchState=='' || searchState==null)
    {
      surgeonlines.clear();
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter all required fields.');
      ApexPages.addMessage(myMsg);
    }
    else
    {
  // Instantiate a new http object
      Http h = new Http();
      
      surgeonlines.clear();
        user_login_settings__c uls = user_login_settings__c.getValues('Default');
      String API_KEY = uls.HL7_API_Key__c; //'D6H8smtD4AvmbV2SPHdp';  
      String endpointString = uls.HL7_Endpoint_String__c;  //'http://api.notonlydev.com/api/index.php?';
      endpointString = endpointString + 'apikey=' + API_KEY;
      endpointString = endpointString + '&is_person=true&is_address=false&is_org=false&is_ident=false&format=json';
      if(searchFirstName!=null) endpointString = endpointString + '&first_name=' + searchFirstName;
      if(searchLastName!=null) endpointString = endpointString + '&last_name=' + searchLastName;
      if(searchState!=null) endpointString = endpointString + '&state=' + searchState;
      
   //http://api.notonlydev.com/api/index.php?apikey=D6H8smtD4AvmbV2SPHdp&first_name=John&last_name=Smith&org_name=&address=&state=&city_name=&zip=&taxonomy=&ident=&is_person=true&is_address=false&is_org=false&is_ident=false&format=json
  // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
      HttpRequest req = new HttpRequest();
      req.setEndpoint(endpointString);
       req.setMethod('GET');
      
  // Send the request, and return a response
  	String response = '';
  	if(!test.isRunningTest())
    {
      HttpResponse res = h.send(req);
    	response = res.getBody();
    }
  try{
      // Parse entire JSON response.
     JSONParser parser = JSON.createParser(response);
                while (parser.nextToken() != null)
          {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
            {
              //parser.nextToken();
              SurgeonLine surgeon = new SurgeonLine();
              while (parser.nextToken() != null && surgeonlines.size()<1000)
              {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
                {
                String fieldName = parser.getText();
                if(surgeon.first_name != null && surgeon.last_name !=null && surgeon.npi != null && surgeon.state!=null && surgeon.description!=null && surgeon.taxonomy_id!=null)
                {
                  surgeonlines.add(surgeon);
                  break;  
                }
                else if(fieldName == 'first_name') 
                {
                  parser.nextToken();
                  surgeon.first_name = parser.getText();
                }
                else if(fieldName == 'last_name')
                {
                  parser.nextToken();
                  surgeon.last_name = parser.getText();
                }
                else if(fieldName == 'npi')
                {
                  parser.nextToken();
                  surgeon.npi = parser.getText();  
                }    
                else if(fieldName == 'state')
                {
                  parser.nextToken();
                  surgeon.state = parser.getText();  
                }    
                else if(fieldName == 'city')
                {
                  parser.nextToken();
                  surgeon.city = parser.getText();  
                }  
                else if(fieldName == 'description')
                {
                  parser.nextToken();
                  surgeon.description = parser.getText();  
                }  
                else if(fieldName == 'taxonomy_id')
                {
                  parser.nextToken();
                  surgeon.taxonomy_id = parser.getText();  
                }    
                
                }
              }
                    }
                }
      }
    catch(Exception ex)
      {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Response from web service.');
        ApexPages.addMessage(myMsg);
      }
          if(surgeonlines.size()==0 || surgeonlines == null)
          {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No results');
            ApexPages.addMessage(myMsg);
          }
        }
        
      }

  public void ChangeHeightUOM(){

        if(clinicalData.height_UOM__c == 'CM') clinicalData.weight_UOM__c = 'KG';
        if(clinicalData.height_UOM__c == 'Inches') clinicalData.weight_UOM__c = 'Pounds';

        CalculateBMI();
      }

  public void ChangeWeightUOM(){

        if(clinicalData.weight_UOM__c == 'KG') clinicalData.height_UOM__c = 'CM';
        if(clinicalData.weight_UOM__c == 'Pounds') clinicalData.height_UOM__c = 'Inches';

        CalculateBMI();
      }
  
}