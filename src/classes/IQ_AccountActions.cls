global without sharing class IQ_AccountActions {
     
    @RemoteAction
    global static List<Account> GetHospitals(){
        return [Select Id, Name From Account Where RecordType.Name = 'Hospital Account' And IsPartner = true Order By Name];
    }
    
    @RemoteAction
    global static List<Account> GetPractices(){
        return [Select Id, Name From Account Where RecordType.Name = 'Practice Account' And IsPartner = true Order By Name];
    }

}