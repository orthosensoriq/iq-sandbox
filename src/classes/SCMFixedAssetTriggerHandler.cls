/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
public with sharing class SCMFixedAssetTriggerHandler {
	
	private boolean isExecuting = false;
	private integer batchSize = 0;

	public SCMFixedAssetTriggerHandler(boolean isExecuting, integer size) {
		this.isExecuting = isExecuting;
		this.batchSize = size;
	}

	public void OnAfterInsert(FAM__Fixed_Asset__c[] newItems, Map<ID, FAM__Fixed_Asset__c> newMap){

		// Create a journal entry for records having both GL accounts newly populated.
		list<FAM__Fixed_Asset__c> fixedAssetsForJournalCreation = new list<FAM__Fixed_Asset__c>();
		for (FAM__Fixed_Asset__c item : newItems)
			if (item.Depreciation_GL_Account__c != null && item.Fixed_Asset_GL_Account__c != null)
				 fixedAssetsForJournalCreation.add(item);
		if (0 < fixedAssetsForJournalCreation.size()) SCMFixedAsset.createJournalEntries(fixedAssetsForJournalCreation);

		SCMFixedAsset.createHistory(newItems);
		
		// Update the related receipt line's, fixed asset field.
		set<id> fixedAssetIdsForUpdatingReceiptLines = new set<id>();
		List<FAM__Fixed_Asset__c> deployingOrReturningFixedAssets = new List<FAM__Fixed_Asset__c>();
		for (FAM__Fixed_Asset__c item : newItems)
			if (item.Receipt_Line__c != null) fixedAssetIdsForUpdatingReceiptLines.add(item.id);
		if (0 < fixedAssetIdsForUpdatingReceiptLines.size()) SCMReceiptLine.updateFixedAssetReference(fixedAssetIdsForUpdatingReceiptLines);
	}

	public void OnAfterUpdate(FAM__Fixed_Asset__c[] old, map<id, FAM__Fixed_Asset__c> oldMap, FAM__Fixed_Asset__c[] newItems, Map<ID, FAM__Fixed_Asset__c> newMap){

		// Create a journal entry for records having both GL accounts newly populated.
		list<FAM__Fixed_Asset__c> fixedAssetsForJournalCreation = new list<FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c oldAsset = null;
		for (FAM__Fixed_Asset__c item : newItems)
			if (item.Depreciation_GL_Account__c != null && item.Fixed_Asset_GL_Account__c != null){
				oldAsset = oldMap.get(item.id);
				if (oldAsset.Depreciation_GL_Account__c == null || oldAsset.Fixed_Asset_GL_Account__c == null)
					fixedAssetsForJournalCreation.add(item);
			}

		if (0 < fixedAssetsForJournalCreation.size()) 
		{
			SCMFixedAsset.createJournalEntries(fixedAssetsForJournalCreation);
		}

		// Execute the Fixed Asset History process for those having a changed Deployed_To__c field.
		List<FAM__Fixed_Asset__c> deployingOrReturningFixedAssets = new List<FAM__Fixed_Asset__c>();
		for (FAM__Fixed_Asset__c item : newItems) {
			FAM__Fixed_Asset__c oldItem = oldMap.get(item.Id);
			if (item.Deployed_To__c != oldItem.Deployed_To__c) {
				deployingOrReturningFixedAssets.add(item);
			}
		}

		SCMFixedAsset.createHistory(deployingOrReturningFixedAssets);
	}
}