/**
 * Copyright (c) 2013, Less Software, inc
 * All rights reserved.
 *
 * Provides functionality for operations with SCMC__Receipt_Line__c
**/
public with sharing class SCMReceiptLine{
	
	@future
	public static void updateFixedAssetReference(set<id> assetIds){
		
		list<FAM__Fixed_Asset__c> assets = [
			select
				id,
				receipt_line__c
			from FAM__Fixed_Asset__c
			where id in :assetIds];
		
		set<id> receiptLineIdsToUpdate = new set<id>();
		for (FAM__Fixed_Asset__c fa : assets) receiptLineIdsToUpdate.add(fa.Receipt_Line__c);
		map<id, SCMC__Receipt_Line__c> receiptLinesToUpdate =
			new map<id, SCMC__Receipt_Line__c>([
				select
					id,
					Fixed_Asset__c
				from SCMC__Receipt_Line__c
				where id in :receiptLineIdsToUpdate]);
		
		SCMC__Receipt_Line__c line = null;
		for (FAM__Fixed_Asset__c fa : assets){
			line = receiptLinesToUpdate.get(fa.id);
			if (line == null) continue;
			line.Fixed_Asset__c = fa.id;
		}
		
		update receiptLinesToUpdate.values();
	}

}