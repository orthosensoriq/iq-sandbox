global with sharing class SCM_InvoiceExport {
	
	global with sharing class ExportedInvoiceLine{
		webservice Id lineID;
		webservice String lineName;
		webservice String ItemDescription;
		webservice String ItemNumber;
		webservice Double Price;
		webservice Double Quantity;
		webservice string accountOverride;
	}

	global with sharing class ExportedInvoice {
		webservice Id invoiceId;					/*id of the invoice in SCM*/
		webservice String financialSystemInvoice;	/* invoice number in the financial System */
		webservice String status;					/*status of export - Exported or Exported Error */
		webservice String message;					/*error message if status is Exported Error */
		webservice List<ExportedInvoiceLine>lines;  /*list of lines on invoice */
		webservice String invoiceName;
		webservice String CustomerAccount;
		webservice String BillingStreet1;
		webservice String BillingStreet2;
		webservice String BillingStreet3;
		webservice String BillingStreet4;
		webservice String BillingCity;
		webservice String BillingState;
		webservice String BillingPostalCode;
		webservice String BillingCountry;
		webservice String ShippingStreet1;
		webservice String ShippingStreet2;
		webservice String ShippingStreet3;
		webservice String ShippingStreet4;
		webservice String ShippingCity;
		webservice String ShippingState;
		webservice String ShippingPostalCode;
		webservice String ShippingCountry;
		webservice String CustomerPurchaseOrderNumber;
		webservice String ExportError;
		webservice String ExportStatus;
		webservice String ForeignInvoiceNumber;
		webservice Date InvoiceDate;
		webservice Date InvoiceDueDate;
		webservice String Terms;
		webservice String QBARAccount;
	}

	/**
	 * Retrieve all invoices that are eligible for export
	 */
	WebService static List<ExportedInvoice> getInvoices() {

		Date earliestInvoiceDate = Date.newInstance(2014, 4, 1);
		
		List<SCMC__Invoicing__c> invoices = [select id
				,name
				, SCMC__Customer__c
				, SCMC__Customer__r.name
				, SCMC__Invoice_Date__c

				, SCMC__Bill_to_Street__c
				, SCMC__Bill_to_Street_2__c
				, SCMC__Bill_to_City__c
				, SCMC__Bill_to_State_Province__c
				, SCMC__Bill_to_Zip_Postal_Code__c
				, SCMC__Bill_to_Country__c

				, SCMC__Ship_to_Street__c
				, SCMC__Ship_to_Street_2__c
				, SCMC__Ship_to_City__c
				, SCMC__Ship_to_State_Province__c
				, SCMC__Ship_to_Zip_Postal_Code__c
				, SCMC__Ship_to_Country__c

				, SCMC__Terms__c
				, SCMC__Invoice_Due_Date__c
				, SCMC__Customer_Purchase_Order_Number__c
				, SCMC__Foreign_Invoice_Number__c

				, SCMC__Export_Error__c
				, SCMC__Export_Status__c
				
				,(select id, name
						, SCMC__Quantity__c
						, SCMC__Unit_Price__c
						, SCMC__Item__c
						, SCMC__Item__r.name
						, SCMC__Item__r.SCMC__Item_Description__c
						, SCMC__Item__r.SCMC__Product_Group__c
						, SCMC__Item__r.SCMC__Product_Group__r.name
						, SCMC__Amount_Description__c
						, SCMC__Item_Name_Formula__c
						, SCMC__Item_Description__c
					from SCMC__Invoice_Line_Items__r)
				from SCMC__Invoicing__c
				where SCMC__Invoice_Date__c > :earliestInvoiceDate and 
                    ((SCMC__Shipping__c = null and SCMC__Export_Status__c = 'Ready to Export')
                       or
					(SCMC__Shipping__r.SCMC__Status__c = 'Shipment complete' and SCMC__Export_Status__c = 'New'))];
		
		List<ExportedInvoice>expInvoices = new List<ExportedInvoice>();

		for (SCMC__Invoicing__c invoice : invoices) {
			
			invoice.SCMC__Export_Status__c = 'Export in Process';

			ExportedInvoice expInvoice = new ExportedInvoice();
			expInvoice.invoiceId =	invoice.Id;
			expInvoice.financialSystemInvoice = invoice.SCMC__Foreign_Invoice_Number__c;
			expInvoice.status = invoice.SCMC__Export_Status__c;
			expInvoice.message = invoice.SCMC__Export_Error__c;
			// note: uncomment the next line of code to use the SCM invoice # in the accounting system
			//expInvoice.invoiceName = invoice.name.substring(invoice.name.indexOf('-')+1);
			
			expInvoice.CustomerAccount = invoice.SCMC__Customer__r.name;

			//expInvoice.QBARAccount = invoice.SCMC__Sales_Order__r.SCMC__Customer_Account__r.QB_AR_Account__c;
			
			expInvoice.BillingStreet1 = invoice.SCMC__Bill_to_Street__c;
			expInvoice.BillingStreet2 = invoice.SCMC__Bill_to_Street_2__c;
			expInvoice.BillingCity = invoice.SCMC__Bill_to_City__c;
			expInvoice.BillingState = invoice.SCMC__Bill_to_State_Province__c;
			expInvoice.BillingPostalCode = invoice.SCMC__Bill_to_Zip_Postal_Code__c;
			expInvoice.BillingCountry = invoice.SCMC__Bill_to_Country__c;

			expInvoice.ShippingStreet1 = invoice.SCMC__Ship_to_Street__c;
			expInvoice.ShippingStreet2 = invoice.SCMC__Ship_to_Street_2__c;
			expInvoice.ShippingCity = invoice.SCMC__Ship_to_City__c;
			expInvoice.ShippingState = invoice.SCMC__Ship_to_State_Province__c;
			expInvoice.ShippingPostalCode = invoice.SCMC__Ship_to_Zip_Postal_Code__c;
			expInvoice.ShippingCountry = invoice.SCMC__Ship_to_Country__c;
			
			expInvoice.CustomerPurchaseOrderNumber = invoice.SCMC__Customer_Purchase_Order_Number__c;
			expInvoice.InvoiceDate = invoice.SCMC__Invoice_Date__c;
			expInvoice.InvoiceDueDate = invoice.SCMC__Invoice_Due_Date__c;
			expInvoice.Terms = invoice.SCMC__Terms__c;
			
			expInvoice.lines = new List<ExportedInvoiceLine>();
			
			for (SCMC__Invoice_Line_Item__c invLine : invoice.SCMC__Invoice_Line_Items__r){
				ExportedInvoiceLine expLine = new ExportedInvoiceLine();
				expLine.lineId = invLine.id;
				expLine.lineName = invLine.name;
				

				if (invLine.SCMC__Item__c != null && invLine.SCMC__Item__r.SCMC__Product_Group__c != null) {
					expLine.ItemNumber = invLine.SCMC__Item__r.SCMC__Product_Group__r.name;
					expLine.ItemDescription = invLine.SCMC__Item__r.name + ' - ' + invLine.SCMC__Item__r.SCMC__Item_Description__c;
				} else {
					expLine.ItemNumber = invLine.SCMC__Item_Name_Formula__c;
					expLine.ItemDescription = invLine.SCMC__Item_Description__c;
				}

				expLine.Price = invLine.SCMC__Unit_Price__c;
				expLine.Quantity = invLine.SCMC__Quantity__c;
				expInvoice.lines.add(expLine);
			}
			
			expInvoices.add(expInvoice);
		}
		update invoices;
		return expInvoices;
	}

	// update all invoices that have been exported
	WebService static void flagExported(ExportedInvoice[] invoices) {
		Map<Id, SCMC__Invoicing__c> invoiceIdToInvoice = new Map<Id, SCMC__Invoicing__c>();
		
		for (ExportedInvoice invoice : invoices){
			invoiceIdToInvoice.put(invoice.invoiceId, null);	
		}

		List<SCMC__Invoicing__c> updInvoices = [select id, name
				,SCMC__Export_Status__c
				,SCMC__Export_Error__c
				,SCMC__Foreign_Invoice_Number__c
				,SCMC__Sales_Order__c
			from SCMC__Invoicing__c
			where id in :invoiceIdToInvoice.keyset()];

		for (SCMC__Invoicing__c updInvoice : updInvoices){
			invoiceIdToInvoice.put(updInvoice.id, updInvoice);
		}

		for (ExportedInvoice invoice : invoices) {
			SCMC__Invoicing__c updInvoice = invoiceIdToInvoice.get(invoice.invoiceId);
			updInvoice.SCMC__Export_Status__c = invoice.status;
			updInvoice.SCMC__Export_Error__c  = invoice.message;
			updInvoice.SCMC__Foreign_Invoice_Number__c = invoice.financialSystemInvoice;
		}

		update updInvoices;
	}

}