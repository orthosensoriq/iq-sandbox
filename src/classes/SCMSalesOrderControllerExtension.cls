public with sharing class SCMSalesOrderControllerExtension {
    
    private SCMC__Org_Address__c orgAddress = null;
    
    public SCMC__Sales_Order__c soDetail { get; private set; }
    public list<SCMC__Sales_Order_Line_Item__c> solList { get; private set; }
    public string CompanyLogo { get; private set; }
    public string CompanyName { get; private set; }
    public string CompanyAddress { get; private set; }
    
    public SCMSalesOrderControllerExtension(){
        this.CompanyLogo = SCMUtilities.getCompanyLogo();
        this.orgAddress = SCMUtilities.getOrganization();
    }
    
    public string SalesOrderId {
        get;
        set{
            SalesOrderId = value;
            this.init(SalesOrderId);
        }
    }
    
    public void init(string soId){
        
        this.CompanyName = orgAddress.SCMC__Name__c;
        this.CompanyAddress = SCMUtilities.getFormattedAddress(null, orgAddress.SCMC__Street__c, orgAddress.Street_2__c, orgAddress.SCMC__City__c, 
                                                               orgAddress.SCMC__State__c, orgAddress.SCMC__Postal_Zip_Code__c, orgAddress.SCMC__Country__c, 
                                                               orgAddress.SCMC__Phone_Number__c, orgAddress.SCMC__Fax__c, orgAddress.Email__c, '<br />');
        
        //Get Sales Order Details   
        this.soDetail = [select Id
                                ,Name
                                ,SCMC__Customer_Account__c
                                ,SCMC__Customer_Account__r.Name
                                ,SCMC__Customer_Account__r.Parent.Name
                                ,SCMC__Customer_Account__r.ShippingStreet
                                ,SCMC__Customer_Account__r.ShippingCity
                                ,SCMC__Customer_Account__r.ShippingState
                                ,SCMC__Customer_Account__r.ShippingPostalCode
                                ,SCMC__Customer_Account__r.ShippingCountry
                                ,SCMC__Customer_Account__r.BillingStreet
                                ,SCMC__Customer_Account__r.BillingCity
                                ,SCMC__Customer_Account__r.BillingState
                                ,SCMC__Customer_Account__r.BillingPostalCode
                                ,SCMC__Customer_Account__r.BillingCountry
                                ,SCMC__Drop_Ship__c
                                ,SCMC__Drop_Ship_City__c
                                ,SCMC__Drop_Ship_Country__c
                                ,SCMC__Drop_Ship_Line1__c
                                ,SCMC__Drop_Ship_Line2__c
                                ,SCMC__Drop_Ship_Name__c
                                ,SCMC__Drop_Ship_State_Province__c
                                ,SCMC__Drop_Ship_Zip_Postal_Code__c
                                ,SCMC__Actual_Ship_To_Address__c
                                ,SCMC__Actual_Ship_To_Address__r.name
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Building_Information__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_City__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Country__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_State_Province__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street_Additional_Information__c
                                ,SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Zip_Postal_Code__c
                                ,SCMC__Actual_Bill_To_Address__c
                                ,SCMC__Actual_Bill_To_Address__r.name
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Building_Information__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_City__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Country__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_State_Province__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Street__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Street_Additional_Information__c
                                ,SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Zip_Postal_Code__c
                                ,SCMC__Payment_Type__c
                                ,SCMC__FOB__c
                                ,SCMC__Sales_Order_Date__c
                                ,Customer_Contact__c
                                ,Customer_Contact__r.Name
                                ,SCMC__Customer_Purchase_Order__c
                                ,SCMC__Total_Value__c
                                ,Carrier__c
                                ,Carrier_Service__c
                                ,Requested_Ship_Date__c
                            from SCMC__Sales_Order__c
                           where Id = :soId];
                           
        this.solList = [select Id
                              ,Name 
                              ,SCMC__Quantity__c
                              ,SCMC__Price__c
                              ,SCMC__Item_Master__c
                              ,SCMC__Item_Master__r.Name
                              ,SCMC__Item_Description__c
                              ,SCMC__Extended_Price__c
                              ,SCMC__Line_Number__c
                              ,SCMC__Unit_of_Measure__c
                         from SCMC__Sales_Order_Line_Item__c
                        where SCMC__Sales_Order__c = :soId
                     order by SCMC__Line_Number__c];
                     
    }
    
    public string getShippingAddress(){
        string address = '';
        if(this.soDetail.SCMC__Drop_Ship__c){
             address = SCMUtilities.getFormattedAddress(this.soDetail.SCMC__Drop_Ship_Name__c
                                                        ,this.soDetail.SCMC__Drop_Ship_Line1__c
                                                        ,this.soDetail.SCMC__Drop_Ship_Line2__c
                                                        ,this.soDetail.SCMC__Drop_Ship_City__c
                                                        ,this.soDetail.SCMC__Drop_Ship_State_Province__c
                                                        ,this.soDetail.SCMC__Drop_Ship_Zip_Postal_Code__c
                                                        ,this.soDetail.SCMC__Drop_Ship_Country__c
                                                        ,null
                                                        ,null
                                                        ,null
                                                        ,'<br />');
        } else if(this.soDetail.SCMC__Actual_Ship_To_Address__c != null){
             address = SCMUtilities.getFormattedAddress(this.soDetail.SCMC__Actual_Ship_To_Address__r.Name
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street__c
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Street_Additional_Information__c
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_City__c
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_State_Province__c
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Zip_Postal_Code__c
                                                        ,this.soDetail.SCMC__Actual_Ship_To_Address__r.SCMC__Mailing_Country__c
                                                        ,null
                                                        ,null
                                                        ,null
                                                        ,'<br />');
        } else {
             address = SCMUtilities.getFormattedAddress(this.soDetail.SCMC__Customer_Account__r.Name
                                                        ,this.soDetail.SCMC__Customer_Account__r.ShippingStreet
                                                        ,null
                                                        ,this.soDetail.SCMC__Customer_Account__r.ShippingCity
                                                        ,this.soDetail.SCMC__Customer_Account__r.ShippingState
                                                        ,this.soDetail.SCMC__Customer_Account__r.ShippingPostalCode
                                                        ,this.soDetail.SCMC__Customer_Account__r.ShippingCountry
                                                        ,null
                                                        ,null
                                                        ,null
                                                        ,'<br />');
        }
        
        return address;
    }

    public string getBillingAddress(){
        string address = '';
        if(this.soDetail.SCMC__Actual_Bill_To_Address__c != null){
             address = SCMUtilities.getFormattedAddress(this.soDetail.SCMC__Actual_Bill_To_Address__r.Name
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Street__c
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Street_Additional_Information__c
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_City__c
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_State_Province__c
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Zip_Postal_Code__c
                                                        ,this.soDetail.SCMC__Actual_Bill_To_Address__r.SCMC__Mailing_Country__c
                                                        ,null
                                                        ,null
                                                        ,null
                                                        ,'<br />');
        } else {
             address = SCMUtilities.getFormattedAddress(this.soDetail.SCMC__Customer_Account__r.Name
                                                        ,this.soDetail.SCMC__Customer_Account__r.BillingStreet
                                                        ,null
                                                        ,this.soDetail.SCMC__Customer_Account__r.BillingCity
                                                        ,this.soDetail.SCMC__Customer_Account__r.BillingState
                                                        ,this.soDetail.SCMC__Customer_Account__r.BillingPostalCode
                                                        ,this.soDetail.SCMC__Customer_Account__r.BillingCountry
                                                        ,null
                                                        ,null
                                                        ,null
                                                        ,'<br />');
        }
        return address;
    }
    
    public SCMC__Sales_Order__c getTempSO(){
        SCMC__Sales_Order__c temp = new SCMC__Sales_Order__c();
        temp.SCMC__Fulfillment_Date__c = datetime.now();
        return temp;
    }
    
    public decimal getTotalValue(){
        decimal totalValue = 0;
        if(this.soDetail.SCMC__Total_Value__c != null){
            totalValue += this.soDetail.SCMC__Total_Value__c;
        }
        return totalValue;
    }
        
}