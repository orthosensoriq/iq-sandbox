public class epTestUtils{
    public Account      aAccount            {get; set;}
    public Contact      aContact            {get; set;}
    public Attachment   aAttachment     {get; set;}
    
    private static Map<String,Schema.Sobjectfield> objectFieldMap;
    private static String objectName;
    private static Map<String,Schema.Describefieldresult> objectFieldResultMap; 
 
     static String unique = String.valueof(Datetime.now()).replace(' ','_').replace('-','_').replace(':','_').replace('.','_');
    static map<Id, Profile> MainProfileMap = new map<Id, Profile>([SELECT Id, Name FROM Profile]);
    static map<String,Id> ProfileMap;
    static{
        ProfileMap = new map<String,Id>();
        for(Id profileId: MainProfileMap.keyset()){
            if(!ProfileMap.containskey(MainProfileMap.get(profileId).Name))
                ProfileMap.put(MainProfileMap.get(profileId).Name,profileId);
        }
    }

    public static epTestUtils generateTest(){
        epTestUtils e = new epTestUtils();
        e.generateAccount();
        e.generateContact(e.aAccount.Id);
        return e;
    }


    public static Id GetProfileIdByName(String pProfileName)
    {
        Id returnValue = null;
        
        if(ProfileMap.containskey(pProfileName)) returnValue=ProfileMap.get(pProfileName);
        return returnValue;
    } 
    public static User CreateUser(String pProfileName) {
        String profileName = pProfileName;
        Profile p = [select id from profile where name = :profileName];
        if(p==null){
            profileName = 'System Administrator';
            p = [select id from profile where name = :profileName];
        }
        User testUser = new User(alias = 'u1',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US'
        , profileid = epTestUtils.GetProfileIdByName(profileName)//p.Id
        , country='United States'
        , timezonesidkey='America/New_York'
        , email='testuseremail'+unique+'@enablepath.com'
        , username='testuser'+String.valueof(Math.random())+unique+'@enablepath.com');
        insert testUser;
        return testUser;
    }   
    public void generateAccount(){

        this.aAccount           = new Account();
        this.aAccount.Name      = 'Test Account'+unique; 
        
        insert this.aAccount;

        this.aAttachment = new Attachment();
        this.aAttachment.Body = Blob.valueOf('String');
    }
    
    public void generateContact(Id AccId){

        this.aContact               = new Contact();
        this.aContact.AccountId     = AccId;
        this.aContact.FirstName     = 'Test';
        this.aContact.LastName      = 'Test'+unique; 
        
        insert this.aContact;

        this.aAttachment = new Attachment();
        this.aAttachment.Body = Blob.valueOf('String');
    }

    public static list<Contact> CreateContactOfEachActiveRecordType(Id pRelatedAccountId) {
        map<Id, RecordType> ContactRecordTypeMap = epUtils.GetRecordTypeMapBySobjectType('Contact');
        list<Contact> conts = new list<Contact>();
        for(Id ContactRecordTypeId : ContactRecordTypeMap.keyset()){
            Contact testContact = new Contact(AccountId=pRelatedAccountId,LastName='Cheek'+unique, RecordTypeId=ContactRecordTypeId);
            conts.add(testContact);
        }
        insert conts;
        return conts;
    }
    
     static public Sobject NewObject(Sobject objIn)
      {
            if(objectFieldResultMap == null) {
                objectFieldResultMap = new Map<String,Schema.Describefieldresult>();
            }
            Boolean sameObj = false;
            string objName = objIn.getSObjectType().getDescribe().getName();
            if(objectName != null && objectName == objName) {
                sameObj = true;
            } 
           
            If(!sameObj) {
                objectFieldMap = objIn.getSObjectType().getDescribe().fields.getMap();
            }
            //System.Debug(LoggingLevel.INFO,'\n\nNumber Of Fields: ' + objectFieldMap.size());
           
            for(string fieldName : objectFieldMap.keySet())
            {
                  Schema.DescribeFieldResult dfr;
                  if(!sameObj) {
                     dfr = objectFieldMap.get(fieldName).getDescribe();
                     objectFieldResultMap.put(fieldName,dfr);
                  } else {
                    dfr =  objectFieldResultMap.get(fieldName);
                  }
                  if (dfr.isCreateable())
                  {
                        Schema.Displaytype fieldType = dfr.getType();
                        if (string.valueOf(fieldType) == 'STRING') objIn.put(fieldName,GenerateRandomString(dfr.getLength()));
                        else if (string.valueOf(fieldType) == 'BOOLEAN') objIn.put(fieldName, true);
                        else if (string.valueOf(fieldType) == 'DATE') objIn.put(fieldName, GenerateRandomDate(0,0,60,true));
                        else if (string.valueOf(fieldType) == 'DATETIME') objIn.put(fieldName, GenerateRandomDateTime(0, 0, 60, 60, 60, true));
                        else if (string.valueOf(fieldType) == 'DOUBLE') objIn.put(fieldName, GenerateRandomDouble(dfr.getDigits(),dfr.getScale()));
                        else if (string.valueOf(fieldType) == 'EMAIL') objIn.put(fieldName, GenerateRandomEmail());
                        else if (string.valueOf(fieldType) == 'INTEGER') objIn.put(fieldName, GenerateRandomNumber(dfr.getDigits()));
                        else if (string.valueOf(fieldType) == 'PERCENTAGE') objIn.put(fieldName, 50);
                        else if (string.valueOf(fieldType) == 'PHONE') objIn.put(fieldName, GenerateRandomPhone());
                        else if (string.valueOf(fieldType) == 'TEXTAREA') objIn.put(fieldName, GenerateRandomString(dfr.getLength()));
                        else if (string.valueOf(fieldType) == 'TIME') objIn.put(fieldName, GenerateRandomTime(24, 60, true));
                        else if (string.valueOf(fieldType) == 'URL') objIn.put(fieldName, GenerateRandomURL());
                        else if (string.valueOf(fieldType) == 'PICKLIST') objIn.put(fieldName, GetRandomPicklistValue(dfr.getPicklistValues()));
                  }
            }
            System.debug(LoggingLevel.INFO,'Object Created ::' + objIn);
            return objIn;
      }
      
      /**
        * This method is dangerous to use because of the likely possibility of 
        * going over the 100 decribe statement limit call.
        *
        * @param SObject object to create
        * @param Integer number of Object to create in List.
        */
      public static List<SObject> ListOfNewObjects(SObject objIn, Integer numOfObjects)
      {
        List<SObject> listOfObjectsToReturn = new List<SObject>();
        for(Integer counter = 0; counter < numOfObjects; counter++)
        {
            listOfObjectsToReturn.add(NewObject(objIn.clone(false,true)));
        }
        return listOfObjectsToReturn;
      }
      
      public static String GenerateRandomString(Integer stringLength) 
      {
        
        if(stringLength > 5) {
            stringLength = 5;
        }
        
        String characterPot = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        String stringOut = '';
        for(Integer counter = 0; counter < stringLength; counter++)
        {
            Integer randomNumber = Math.round(Math.random() * (characterPot.length()-1));
            stringOut += characterPot.substring(randomNumber, randomNumber+1);
        }   
        return stringOut;
      }
      
      public static String GenerateRandomEmail()
      {
        return GenerateRandomString(15) + '@testing.com';
      }
      
      public static Integer GenerateRandomNumber(Integer numberLength)
      {
        String numberPot = '123456789';
        String stringOut ='';
        if(numberLength == 0) {
            stringOut = '0';    
        } else if(numberLength > 5) {
            numberLength = 5;
        } 
        for(Integer counter = 0; counter < numberLength; counter++)
        {
            Integer randomNumber = Math.round(Math.random() * (numberPot.length()-1));
            stringOut += numberPot.subString(randomNumber, randomNumber+1);
        }
        return Long.valueOf(stringOut).intValue();
      }
      
      public static Double GenerateRandomDouble(Integer digits, Integer scale)
      {
        string doubleNumber = '';
        if(digits == 0 && scale == 0) {
            doubleNumber = '0.0';   
        } else {
            doubleNumber = String.valueOf(GenerateRandomNumber(digits)) + '.' + String.valueOf(GenerateRandomNumber(scale));
        }
        return Double.valueOf(doubleNumber);
      }
      
      public static String GenerateRandomPhone()
      {
        return String.valueOf(GenerateRandomNumber(3)) + '-' + String.valueOf(GenerateRandomNumber(3)) + '-' + String.valueOf(GenerateRandomNumber(4));
      }
      public static String GenerateRandomURL()
      {
        return 'http://www.' + GenerateRandomString(15) + '.com';
      }
      
      public static String GetRandomPicklistValue(List<Schema.PicklistEntry> picklist)
      {
        List<Schema.PicklistEntry> activePicklistItems = new List<Schema.Picklistentry>();
        for(Schema.Picklistentry pickItem:picklist)
        {
            if(pickItem.isActive())
            {
                activePicklistItems.add(pickItem);
            }
        }
        Schema.PicklistEntry pickListItem = activePickListItems[Math.round(Math.random() * (activePicklistItems.size()-1))];
        return pickListItem.getValue();
      }
      
      public static DateTime GenerateRandomDateTime(Integer yearRange, Integer monthRange, Integer dayRange, Integer hourRange, Integer minuteRange, Boolean inThePast)
      {
        Integer yearsToAdd = Math.round(Math.random() * (inThePast ? (yearRange * -1):yearRange));
        Integer monthsToAdd = Math.round(Math.random() * (inThePast ? (monthRange * -1):monthRange));
        Integer daysToAdd = Math.round(Math.random() * (inThePast ? (dayRange * -1):dayRange));
        Integer hoursToAdd = Math.round(Math.random() * (inThePast ? (hourRange * -1):hourRange));
        Integer minutesToAdd = Math.round(Math.random() * (inThePast ? (minuteRange * -1):minuteRange));
        
        DateTime randomizedDateTime = DateTime.now().addYears(yearsToAdd).addMonths(monthsToAdd).addDays(daysToAdd).addHours(hoursToAdd).addMinutes(minutesToAdd);
        return randomizedDateTime;
      }
       
      public static Time GenerateRandomTime(Integer hourRange, Integer minuteRange, Boolean inThePast)
      {
        return GenerateRandomDateTime(0,0,0,hourRange,minuteRange,inThePast).time();
      }
      
      public static Date GenerateRandomDate(Integer yearRange, Integer monthRange, Integer dayRange, Boolean inThePast)
      {
        return GenerateRandomDateTime(yearRange,monthRange,dayRange,0,0,inThePast).date();      
      }
}