@isTest
private class PatientPopulationSearchControllerTest {
	
     static testMethod void testPatientPopulationSearchController() {
		Diagnosis__c diag = new Diagnosis__c();
        diag.Code__c='715.00';
        diag.Long_Name__c = 'Test Diag Code';
        insert diag;
    
		Account a = new Account();
		a.Name = 'TestAccount';
        a.RecordTypeId = [Select Id from RecordType where Name = 'Hospital Account' LIMIT 1].Id;
		insert a;
        
        Account s = new Account();
        s.FirstName = 'Test';
        s.LastName = 'Tester';
        s.RecordTypeId = [Select Id from RecordType where Name = 'Surgeon Account' Limit 1].id;
        s.BillingState = 'GA';
        s.NPI__c = '1234567890';
        insert s;
        
		Contact c = new Contact();
		c.FirstName = 'Test';
		c.LastName = 'Test';
		c.Email = 'Tester@Tester.com.Test';
        c.accountId = a.id;
		insert c;
        
        //User testUser = new User();
        //testUser.FirstName = 'Testing';
        //testUser.LastName = 'Tester';
        //testUser.Username = 'tester@OrthoSensor.com.test';
        //testUser.Email = 'tester@OrthoSensor.com.test';
        //testUser.Alias = 'tester';
        //testUser.TimeZoneSidKey = 'America/New_York';
        //testUser.LocaleSidKey = 'en_US';
        //testUser.EmailEncodingKey = 'ISO-8859-1';
        //testUser.LanguageLocaleKey = 'en_US';
        //testUser.ProfileId = [Select id from Profile where name = 'Partner Community User' LIMIT 1].id;
        //testUser.ContactId = c.id;
        //insert testUser;
		
        surgeon_hospital__c surghosp = new surgeon_hospital__c();
        surghosp.Hospital__c = a.id;
        surghosp.Surgeon__c = c.id;
        insert surghosp;
        a.IsPartner = true;
        update a;
		
        
		list<Account> testAccts = [SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000];
        system.debug('Hospital Accounts: ' + testAccts.size());
    
        Patient__c testPatient = new Patient__c();
        testPatient.First_Name__c = 'Test';
        testPatient.Last_Name__c = 'Tester';
        testPatient.Gender__c = 'Female';
         testPatient.Race__c = 'American Indian';
        testPatient.Date_Of_Birth__c = date.newInstance(1984, 3, 3);
        testPatient.Account_Number__c = '1234567890987654321';
        testPatient.Medical_Record_Number__c = 'MR123456';
        testPatient.active__c = true;
        testPatient.Hospital__c = a.id;
        insert testPatient;
        
        Case_Type__c ctyp = new Case_Type__c();
        ctyp.Description__c = 'Total Knee Arthroplasty';
        insert ctyp;
        
		Event_Type__c evtyp = new Event_Type__c();
		evtyp.Name = 'Procedure';
		evtyp.Primary__c = true;
        evtyp.Active__c = true;
		insert evtyp;

        Event_type__c evtyp2 = new Event_Type__c();
        evtyp2.name = 'followup';
        evtyp2.Primary__c = false;
        evtyp2.Active__c = true;
        evtyp2.Interval__c = 6;
        insert evtyp2;
        
        Case_Type_Event_Type__c ctet = new Case_Type_Event_Type__c();
        ctet.Case_Type__c = ctyp.id;
        ctet.Event_Type__c = evtyp.id;
        insert ctet;
        
        Case_Type_Event_Type__c ctet2 = new Case_Type_Event_Type__c();
        ctet2.Case_Type__c = ctyp.id;
        ctet2.Event_Type__c = evtyp2.id;
        insert ctet2;
        
        survey__c surv = new survey__c();
        surv.Active__c = true;
        surv.Name__c = 'Test Survey';
        surv.Survey_URL__c = 'http://www.test.com/123423awfa.htm';
		insert surv;
        
        event_type_survey__c ets = new event_type_survey__c();
        ets.survey__c = surv.id;
        ets.Event_Type__c = evtyp.id;
        insert ets;

        Case__c newCase = new Case__c();
        newCase.Name__c = 'Test Case';
        newCase.Patient__c = testPatient.id;
        newCase.Case_Type__c = ctyp.id;
        Insert newCase;

		Event__c newEvent = new Event__c();
        newEvent.Event_Name__c = 'Test Procedure';
        //newEvent.Event_Type__c = evtType.Id;
        newEvent.Case__c = newCase.Id;
         newEvent.Location__c = a.id;
         newEvent.Appointment_Start__c = datetime.newInstance(2014, 2, 14);
        Insert newEvent;
        
        Clinical_Data__c testClinicalData = (Clinical_Data__c)epTestUtils.NewObject(new Clinical_Data__c());
        testClinicalData.Event__c = newEvent.id;
        testClinicalData.Height__c = 68;
        testClinicalData.Weight__c = 150;
         testClinicalData.Laterality__c = 'Both';
        insert testClinicalData;

        Event_Form__c testEventForm = (Event_Form__c)epTestUtils.NewObject(new Event_Form__c());
        testEventForm.Event__c = newEvent.id;
        insert testEventForm;

        Survey_Response__c testResponse = new Survey_Response__c();
        testResponse.Event_Form__c = testEventForm.id;
        testResponse.Survey_Question__c = '1';
        testResponse.Survey_Question_Text__c = 'Test Question';
        insert testResponse;
        
        OS_Device__c testOSDvc = new OS_Device__c();
        testOSDvc.Size__c = '20';
        testOSDvc.Style__c = 'New';
        testOSDvc.Year_Assembled__c = '2013';
        insert testOSDvc;
        
        OS_Device_Event__c osde = new OS_Device_Event__c();
        osde.OS_Device__c = testOSDvc.id;
        osde.Event__c = newEvent.id;
        insert osde;
        
		Sensor_Data__c newSensDat = new Sensor_Data__c();
         newSensDat.Event__c = newEvent.id;
         newSensDat.ImageFileName__c = 'TestValue.png';
         newSensDat.Measurement_Date__c = datetime.newInstance(2014, 2, 14, 9, 37, 00);
         newSensDat.Type__c = 'Flexion';
         newSensDat.Value__c = '456';

		//Construct controller
          PatientPopulationSearchController con = new PatientPopulationSearchController();

          //Execute code
          con.selectedHospitals.clear();
          con.AgeOperation = 'Between';
          con.setUpperAgeVisibility();
          con.AgeOperation = '=';
          con.setUpperAgeVisibility();
          con.ProcedureDateOperation = 'Between';
          con.setUpperDateVisibility();
          con.ProcedureDateOperation = '=';
          con.setUpperDateVisibility();
          con.searchHeight = 0.0;
          con.searchWeight = 0.0;
          con.searchBMI = 0.0;
          con.searchDOB = '';
          con.searchAge = '';
          con.searchUpperAge = '';
          con.getResults();
		  con.selectedSurgeons.add(s.Id);
          con.selectedHospitals.add(a.id);
          con.selectedCaseTypes.add(ctyp.Id);
          con.selectedLaterality.add('Both');
          con.searchHeight = 68;
          con.searchWeight = 150;
          //con.searchBMI = 38;
          con.searchDOB = '1/25/1950';
          con.selectedRaces.add('American Indian');
          con.selectedGenders.add('Female');
          con.AgeOperation = 'Between';
          con.setUpperAgeVisibility();
		  con.searchAge = '25';
          con.searchUpperAge = '35';
          con.ProcedureDateOperation = 'Between';
          con.setUpperDateVisibility();
          con.searchProcDate = '2/3/2014';
          con.searchUpperProcDate = '2/28/2014';
         for (Clinical_Data__c cd1 : [SELECT Primary_Diagnosis__c FROM Clinical_Data__c WHERE Primary_Diagnosis__c != null LIMIT 1]){
               con.selectedPrimaryDiagnosis.add(cd1.Id);
          }
           for (Clinical_Data__c cd2 : [SELECT Secondary_Diagnosis__c FROM Clinical_Data__c WHERE Secondary_Diagnosis__c != null LIMIT 1]){
               con.selectedSecondaryDiagnosis.add(cd2.Id);
          }
          con.getResults();
          con.AgeOperation = '=';
          con.ProcedureDateOperation = '=';
          con.searchUpperProcDate = '';
          con.searchUpperAge = '';

          List<selectOption> hospitalOptions = con.getHospitalItems();
          String[] currHospitals = con.getHospitals();
          List<selectOption> surgeonOptions = con.getSurgeonItems();
          String[] currSurgeons = con.getSurgeons();
          List<selectOption> caseTypeOptions = con.getCaseTypeItems();
          String[] currCaseTypes = con.getCaseTypes();
          List<selectOption> lateralityOptions = con.getLateralityItems();
          String[] currLateralityTypes = con.getLaterality();

          List<selectOption> raceOptions = con.getRaceItems();
          String[] currRaces = con.getRaces();
          List<selectOption> genderOptions = con.getGenderItems();
          String[] currGenders = con.getGenders();
          
          List<selectOption> primeDiagOptions = con.getPrimaryDiagItems();
          String[] currPrimeDiagTypes = con.getPrimaryDiagnosis();
          List<selectOption> secondaryDiagOptions = con.getSecondaryDiagItems();
          String[] currSecondaryDiagTypes = con.getSecondaryDiagnosis();
         
          List<selectOption> AgeOpOptions = con.getAgeOperationItems();
          List<selectOption> DateOpOptions = con.getProcedureDateOperationItems();
          List<selectOption> ManuOptions = con.getManufacturerItems();
          String[] currManu = con.getManufacturers();
         

          PageReference pageRef = con.exportToCSV();
          PageReference pageRef2 = con.goToPatientView();
         
         set<Id> eventIds = new set<id>();
         eventIds.add(newEvent.id);
         PatientPopulationExportController ppec = new PatientPopulationExportController();
         ppec.BuildExportCSV(eventIds,true);
	}
}