public with sharing class SurgeonListController {
	
	public Id SurgeonID {get;set;}

	// Get Listing of Surgeon records
	public ApexPages.StandardSetController SurgeonList {
        get {
            if(SurgeonList == null) {
                String viewQuery = 'SELECT Id, LastName, FirstName, NPI__c, Speciality_Code__c, BillingCity, BillingState';
                viewQuery = viewQuery + ' FROM Account WHERE IsPersonAccount = true';
                viewQuery = viewQuery + ' AND RecordType.Name = \'Surgeon Account\' LIMIT 10000';
                
                SurgeonList = new ApexPages.StandardSetController(Database.getQueryLocator(viewQuery));
            }
            SurgeonList.setPageSize(2000);
            return SurgeonList;
        }
        set;
    }

	// Initialize Surgeons and return a list of records 
    public List<Account> getSurgeons() {
        return (List<Account>) SurgeonList.getRecords();
    }

	// ReDirect to the Surgeon View Page
    public PageReference goToSurgeonView(){
		PageReference pageRef = new PageReference('/apex/SurgeonView?id=' + SurgeonID);
		return pageRef;
	}
}