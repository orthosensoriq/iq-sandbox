/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/

@isTest
public with sharing class SCMFFJournalTest {

	//@isTest
	static void runSomeCode()
	{
	}

	@isTest
	static void createJournalsWithDepreciationSchedules()
	{

		c2g__codaCompany__c company = new c2g__codaCompany__c();
		company.Id = SCMSetUpTest.generateMockId(c2g__codaCompany__c.SObjectType);

		List<c2g__codaAccountingCurrency__c> currencyList = new List<c2g__codaAccountingCurrency__c>();
		c2g__codaAccountingCurrency__c curr = new c2g__codaAccountingCurrency__c();
		curr.Id = SCMSetUpTest.generateMockId(c2g__codaAccountingCurrency__c.SObjectType);
		currencyList.add(curr);

		List<c2g__codaPeriod__c> periodList = new List<c2g__codaPeriod__c>();
		c2g__codaPeriod__c period = new c2g__codaPeriod__c();
		period.Id = SCMSetUpTest.generateMockId(c2g__codaPeriod__c.SObjectType);
		period.c2g__OwnerCompany__r = company;
		period.c2g__StartDate__c = Date.today().addDays(-15);
		period.c2g__EndDate__c = Date.today().addDays(15);
		periodList.add(period);

		List<FAM__FA_Depreciation_Schedule__c> deprecSchedList = new List<FAM__FA_Depreciation_Schedule__c>();
		FAM__FA_Depreciation_Schedule__c ds = new FAM__FA_Depreciation_Schedule__c();
		ds.Id = SCMSetUpTest.generateMockId(FAM__FA_Depreciation_Schedule__c.SObjectType);
		ds.FAM__Effective_Depreciation_Date__c = Date.today();
		ds.FAM__Depreciation_Amount__c = 77.77;
		deprecSchedList.add(ds);

		Map<Id, FAM__Fixed_Asset__c> idToFixedAssetMap = new Map<Id, FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Fixed_Asset_GL_Account__c = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		asset.Depreciation_GL_Account__c = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		asset.Company__r = company;
		idToFixedAssetMap.put(ds.Id, asset);

		SCMFFJournal.TestContext tc = new SCMFFJournal.TestContext();
		tc.isTest = true;
		tc.codaCurrencyList = currencyList;
		tc.codaPeriodList = periodList;
		SCMFFJournal.testingContext = tc;

		SCMFFJournal factory = SCMFFJournal.create(deprecSchedList, idToFixedAssetMap);
		Map<Id, c2g__codaJournal__c> resultingJournalMap = factory.getJournals();
		System.debug('<ojs> resultingJournalMap:\n' + resultingJournalMap);
		Map<Id, c2g__codaJournalLineItem__c> resultingJournalLines_Decreasing = factory.getJournalLinesDecreasing();
		System.debug('<ojs> resultingJournalLines_Decreasing:\n' + resultingJournalLines_Decreasing);
		Map<Id, c2g__codaJournalLineItem__c> resultingJournalLines_Increasing = factory.getJournalLinesIncreasing();
		System.debug('<ojs> resultingJournalLines_Increasing:\n' + resultingJournalLines_Increasing);

		System.assertEquals(1, resultingJournalMap.values().size(), 'Only 1 journal should\'ve been created.');
		c2g__codaJournal__c journal = resultingJournalMap.values().get(0);
		System.assertEquals(ds.Id, journal.Depreciation_Schedule__c, 'The journal\'s depreciating schedule Id should match.');
		c2g__codaJournalLineItem__c journalLine = resultingJournalLines_Increasing.get(journal.Depreciation_Schedule__c);
		System.assertEquals(ds.FAM__Depreciation_Amount__c, journalLine.c2g__Value__c, 'The increasing journal line\'s value is NOT correct.');
		journalLine = resultingJournalLines_Decreasing.get(journal.Depreciation_Schedule__c);
		System.assertEquals(-1 * ds.FAM__Depreciation_Amount__c, journalLine.c2g__Value__c, 'The increasing journal line\'s value is NOT correct.');
	}

	@isTest
	static void createJournalsWithReceiptLines()
	{

		c2g__codaCompany__c company = new c2g__codaCompany__c();
		company.Id = SCMSetUpTest.generateMockId(c2g__codaCompany__c.SObjectType);

		List<c2g__codaAccountingCurrency__c> currencyList = new List<c2g__codaAccountingCurrency__c>();
		c2g__codaAccountingCurrency__c curr = new c2g__codaAccountingCurrency__c();
		curr.Id = SCMSetUpTest.generateMockId(c2g__codaAccountingCurrency__c.SObjectType);
		currencyList.add(curr);

		List<c2g__codaPeriod__c> periodList = new List<c2g__codaPeriod__c>();
		c2g__codaPeriod__c period = new c2g__codaPeriod__c();
		period.Id = SCMSetUpTest.generateMockId(c2g__codaPeriod__c.SObjectType);
		period.c2g__OwnerCompany__r = company;
		period.c2g__StartDate__c = Date.today().addDays(-15);
		period.c2g__EndDate__c = Date.today().addDays(15);
		periodList.add(period);

		SCMC__Purchase_Order_Line_Item__c purchaseOrderLine = new SCMC__Purchase_Order_Line_Item__c();
		purchaseOrderLine.Id = SCMSetUpTest.generateMockId(SCMC__Purchase_Order_Line_Item__c.SObjectType);
		purchaseOrderLine.SCMFFA__General_Ledger_Account__c = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		purchaseOrderLine.SCMC__Unit_Cost__c = 777.77;

		List<SCMC__Receipt_Line__c> receiptLines = new List<SCMC__Receipt_Line__c>();
		SCMC__Receipt_Line__c receiptLine = new SCMC__Receipt_Line__c();
		receiptLine.Id = SCMSetUpTest.generateMockId(SCMC__Receipt_Line__c.SObjectType);
		receiptLine.SCMC__Purchase_Order_Line_Item__c = purchaseOrderLine.Id;
		receiptLine.SCMC__Purchase_Order_Line_Item__r = purchaseOrderLine;
		receiptLines.add(receiptLine);

		Map<Id, FAM__Fixed_Asset__c> idToFixedAssetMap = new Map<Id, FAM__Fixed_Asset__c>();
		FAM__Fixed_Asset__c asset = new FAM__Fixed_Asset__c();
		asset.Id = SCMSetUpTest.generateMockId(FAM__Fixed_Asset__c.SObjectType);
		asset.Fixed_Asset_GL_Account__c = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		asset.Depreciation_GL_Account__c = SCMSetUpTest.generateMockId(c2g__codaGeneralLedgerAccount__c.SObjectType);
		asset.Company__r = company;
		idToFixedAssetMap.put(receiptLine.Id, asset);

		SCMFFJournal.TestContext tc = new SCMFFJournal.TestContext();
		tc.isTest = true;
		tc.codaCurrencyList = currencyList;
		tc.codaPeriodList = periodList;
		SCMFFJournal.testingContext = tc;

		SCMFFJournal factory = SCMFFJournal.create(receiptLines, idToFixedAssetMap);
		Map<Id, c2g__codaJournal__c> resultingJournalMap = factory.getJournals();
		System.debug('<ojs> resultingJournalMap:\n' + resultingJournalMap);
		Map<Id, c2g__codaJournalLineItem__c> resultingJournalLines_Increasing = factory.getJournalLinesIncreasing();
		System.debug('<ojs> resultingJournalLines_Increasing:\n' + resultingJournalLines_Increasing);
		Map<Id, c2g__codaJournalLineItem__c> resultingJournalLines_Decreasing = factory.getJournalLinesDecreasing();
		System.debug('<ojs> resultingJournalLines_Decreasing:\n' + resultingJournalLines_Decreasing);

		System.assertEquals(1, resultingJournalMap.values().size(), 'Only 1 journal should\'ve been created.');
		c2g__codaJournal__c journal = resultingJournalMap.values().get(0);
		System.assertEquals(receiptLine.Id, journal.Receipt_Line__c, 'The journal\'s receipt line Id should match.');
		c2g__codaJournalLineItem__c journalLine = resultingJournalLines_Increasing.get(journal.Receipt_Line__c);
		System.assertEquals(purchaseOrderLine.SCMC__Unit_Cost__c, journalLine.c2g__Value__c, 'The increasing journal line\'s value is NOT correct.');
		journalLine = resultingJournalLines_Decreasing.get(journal.Receipt_Line__c);
		System.assertEquals(-1 * purchaseOrderLine.SCMC__Unit_Cost__c, journalLine.c2g__Value__c, 'The increasing journal line\'s value is NOT correct.');
	}
}