/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2016 FinancialForce.com, inc. All rights reserved.
*/
@isTest
private with sharing class SCMFFA_InvoiceExportPlugin_Test
{

	@isTest
	private static void NormalSuccessfulTest()
	{
		SCMC__Invoicing__c scmInvoice = (SCMC__Invoicing__c) SCMFFA_SetupTest.createNewSObject(SCMC__Invoicing__c.SObjectType);
		SCMFFA_SetupTest.setId(scmInvoice);

		c2g__codaInvoice__c sin = (c2g__codaInvoice__c) SCMFFA_SetupTest.createNewSObject(c2g__codaInvoice__c.SObjectType);
		sin.c2g__DeriveCurrency__c = false;
		SCMFFA_SetupTest.setId(sin);

		Map<Id, SObject> sObjectMap = new Map<Id, SObject>();
		sObjectMap.put(scmInvoice.Id, sin);

		SCMFFA_InvoiceExportPlugin plugin = new SCMFFA_InvoiceExportPlugin();
		plugin.execute(sObjectMap, null, null);

		System.assertEquals(
			true,
			sin.c2g__DeriveCurrency__c,
			'The SIN\'s Derive Currency property should be True.');
	}
}