/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/
@isTest
public with sharing class FFA_SetupTest
{
	/** Test Data **/
	public static final String TEST_ACCOUNT_NAME = '$$$$TestAccount$$$$';
	public static final String TEST_GLA_NAME = '$$$$TestGLA$$$$';
	public static final String TEST_GLA_REPORTINGCODE = '$$$$TestGLARC$$$$';
	public static final String TEST_PRODUCT_NAME = '$$$$TestProduct$$$$';

	public static final String TEST_DIM_1_NAME = '$$$$TestDim1$$$$';
	public static final String TEST_DIM_2_NAME = '$$$$TestDim2$$$$';
	public static final String TEST_DIM_3_NAME = '$$$$TestDim3$$$$';
	public static final String TEST_DIM_4_NAME = '$$$$TestDim4$$$$';

	public static final String TEST_ACCT_TAX_1_NAME = '$$$$TestAcTax1$$$$';
	public static final String TEST_ACCT_TAX_2_NAME = '$$$$TestAcTax2$$$$';
	public static final String TEST_ACCT_TAX_3_NAME = '$$$$TestAcTax3$$$$';
	public static final String TEST_ACCT_OUTPUT_VAT_TAX_NAME = '$$$$TestAcVATOutTax$$$$';
	public static final String TEST_PROD_TAX_NAME = '$$$$TestPrTax$$$$';
	public static final String TEST_COMP_TAX_NAME = '$$$$TestCoTax$$$$';

	public static final String TEST_COMPANY_NAME = '$$$$Test$Company$$$$';

	public static final String TEST_BANK_ACCOUNT_NAME = '$$$$TestBankAccount$$$$';
	public static final String TEST_BANK_ACCOUNT_REPORTINGCODE = '$$$$TestBankAccount$$$$';

	public static final Integer TEST_CURRENT_YEAR = Date.today().year();
	public static final Integer TEST_FUTURE_YEAR = TEST_CURRENT_YEAR + 1;
	public static final Integer TEST_PREVIOUS_YEAR = TEST_CURRENT_YEAR - 1;
	public static final Integer TEST_NON_FINANCIAL_CALENDAR_YEAR = TEST_CURRENT_YEAR + 4;
	public static final Date TEST_FIRST_DATE = Date.newInstance(TEST_CURRENT_YEAR,01,01);
	public static final Date TEST_CURRENT_DATE = Date.newInstance(TEST_CURRENT_YEAR,06,01);
	public static final Date TEST_LAST_DATE = Date.newInstance(TEST_CURRENT_YEAR,12,31);
	public static final Date TEST_FUTURE_DATE = Date.newInstance(TEST_FUTURE_YEAR,06,01);
	public static final Date TEST_PAST_DATE = Date.newInstance(TEST_PREVIOUS_YEAR,06,01);
	public static final Date NON_FINANCIAL_CALENDAR_DATE = Date.newInstance(TEST_NON_FINANCIAL_CALENDAR_YEAR,06,01);

	public static final String TEST_DEF_INV_TXT_DEF_1_NAME = '$$$$DefInvTxtDef1$$$$';
	public static final String TEST_DEF_INV_TXT_DEF_2_NAME = '$$$$DefInvTxtDef2$$$$';
	public static final String TEST_DEF_INV_TXT_DEF_3_NAME = '$$$$DefInvTxtDef3$$$$';
	public static final String TEST_DEF_INV_TXT_DEF_4_NAME = '$$$$DefInvTxtDef4$$$$';
	public static final String TEST_DEF_INV_TXT_DEF_5_NAME = '$$$$DefInvTxtDef5$$$$';

	public static final Decimal TEST_PRICE = 1234.56;
	public static final Decimal TEST_ALT_PRICE = 6543.21;

	public static final Decimal TEST_ACCT_TAX_1_PAST_RATE = 1.1;
	public static final Decimal TEST_ACCT_TAX_2_PAST_RATE = 2.1;
	public static final Decimal TEST_ACCT_TAX_3_PAST_RATE = 3.1;
	public static final Decimal TEST_ACCT_OUTPUT_VAT_TAX_PAST_RATE = 4.1;
	public static final Decimal TEST_PROD_TAX_PAST_RATE = 5.1;
	public static final Decimal TEST_COMP_TAX_PAST_RATE = 6.1;

	public static final Decimal TEST_ACCT_TAX_1_CURRENT_RATE = 1.2;
	public static final Decimal TEST_ACCT_TAX_2_CURRENT_RATE = 2.2;
	public static final Decimal TEST_ACCT_TAX_3_CURRENT_RATE = 3.2;
	public static final Decimal TEST_ACCT_OUTPUT_VAT_TAX_CURRENT_RATE = 4.2;
	public static final Decimal TEST_PROD_TAX_CURRENT_RATE = 5.2;
	public static final Decimal TEST_COMP_TAX_CURRENT_RATE = 6.2;

	public static final Decimal TEST_ACCT_TAX_1_FUTURE_RATE = 1.3;
	public static final Decimal TEST_ACCT_TAX_2_FUTURE_RATE = 2.3;
	public static final Decimal TEST_ACCT_TAX_3_FUTURE_RATE = 3.3;
	public static final Decimal TEST_ACCT_OUTPUT_VAT_TAX_FUTURE_RATE = 4.3;
	public static final Decimal TEST_PROD_TAX_FUTURE_RATE = 5.3;
	public static final Decimal TEST_COMP_TAX_FUTURE_RATE = 6.3;

	public static final Decimal TEST_ACCT_DISC_1_RATE = 7.1;
	public static final Decimal TEST_ACCT_DISC_2_RATE = 8.1;
	public static final Decimal TEST_ACCT_DISC_3_RATE = 9.1;
	public static final Decimal TEST_ACCT_DISC_4_RATE = 10.1;

	public static final String TEST_COUNTRY_CODE = 'GB';
	public static final String TEST_VAT_REGISTRATION_NUMBER = '123456789';
	public static final String TEST_GLA_TYPE = 'Balance Sheet';
	public static final String TEST_VAT_STATUS = 'EC Registered';
	public static final String TEST_TAX_CALCULATION_METHOD = 'Gross';

	public static Account testAccount;
	public static c2g__codaGeneralLedgerAccount__c testGLA;
	public static c2g__codaGeneralLedgerAccount__c testGLA2;
	public static c2g__codaGeneralLedgerAccount__c testGLA3;
	public static c2g__codaGeneralLedgerAccount__c testGLA4;
	public static Product2 testProduct;
	public static PricebookEntry testPricebookEntry,testAltPricebookEntry;
	public static c2g__codaCompany__c testCompany,testCompany2;
	public static c2g__codaAccountingCurrency__c testCurrency,testAltCurrency;
	public static c2g__codaPeriod__c testPeriod,testPeriodPrevious,testPeriodNext;
	public static c2g__codaYear__c testYear,testYearPrevious,testYearNext;
	public static c2g__codaDimension1__c testDimension1;
	public static c2g__codaDimension2__c testDimension2;
	public static c2g__codaDimension3__c testDimension3;
	public static c2g__codaDimension4__c testDimension4;
	public static c2g__codaTaxCode__c testAccountTaxCode1,testAccountTaxCode2,testAccountTaxCode3,testAccountOutputVatCode,testProductTaxCode,testCompanyTaxCode;
	public static c2g__codaTaxRate__c testAccountTaxCode1PastRate,testAccountTaxCode2PastRate,testAccountTaxCode3PastRate,testAccountOutputVatCodePastRate,testProductTaxCodePastRate,testCompanyTaxCodePastRate;
	public static c2g__codaTaxRate__c testAccountTaxCode1CurrentRate,testAccountTaxCode2CurrentRate,testAccountTaxCode3CurrentRate,testAccountOutputVatCodeCurrentRate,testProductTaxCodeCurrentRate,testCompanyTaxCodeCurrentRate;
	public static c2g__codaTaxRate__c testAccountTaxCode1FutureRate,testAccountTaxCode2FutureRate,testAccountTaxCode3FutureRate,testAccountOutputVatCodeFutureRate,testProductTaxCodeFutureRate,testCompanyTaxCodeFutureRate;
	public static c2g__codaTextDefinition__c testDefaultInvoiceTextDefinition1;
	public static c2g__codaTextDefinition__c testDefaultInvoiceTextDefinition2;
	public static c2g__codaTextDefinition__c testDefaultInvoiceTextDefinition3;
	public static c2g__codaTextDefinition__c testDefaultInvoiceTextDefinition4;
	public static c2g__codaTextDefinition__c testDefaultInvoiceTextDefinition5;
	private static String s_corpCurr;
	private static String s_altCurr;
	private static Boolean s_currenciesLoaded=false;
	public static User testUser;
	public static c2g__codaBankAccount__c testBankAccount;

	public static Boolean s_useCurrentUserForSubscribedQueue = false;

	private static User s_thisUser;

	@isTest
  	static void test_something()
  	{
  	}

	public static void setUseCurrentUserForSubscribedQueue(Boolean value)
	{
		s_useCurrentUserForSubscribedQueue = value;
	}

	public static Boolean UseCurrentUserForSubscribedQueue()
	{
		return Test.isRunningTest() && s_useCurrentUserForSubscribedQueue;
	}


	private static User getThisUser()
	{
		if(s_thisUser==null)
			s_thisUser = [select c2g__SupportDataAccessKey__c from User where Id = :UserInfo.getUserId()];
		return s_thisUser;
	}

	public static void assertRunningTest()
	{
	}

	public static String getCorporateCurrency()
	{
		if (!s_currenciesLoaded)
			loadOrgCurrencies();

		return s_corpCurr;
	}

	public static String getAltCurrency()
	{
		if (!s_currenciesLoaded)
			loadOrgCurrencies();

		return s_altCurr;
	}

	private static void loadOrgCurrencies()
	{
		if (!s_currenciesLoaded)
		{
			s_currenciesLoaded = true;

			if (UserInfo.isMultiCurrencyOrganization())
			{
				for (SObject so : Database.query('select IsoCode, IsCorporate From CurrencyType where IsActive=true order by IsCorporate desc limit 2'))
				{
					if ((Boolean)so.get('IsCorporate'))
					{
						s_corpCurr = (String) so.get('IsoCode');
					}
					else
					{
						s_altCurr = (String) so.get('IsoCode');
					}
				}
			}
			else
			{
				s_corpCurr = 'USD';
			}
		}
	}

	public static void createDimensions()
	{
		testDimension1 = new c2g__codaDimension1__c(Name=TEST_DIM_1_NAME,c2g__ReportingCode__c=TEST_DIM_1_NAME);
		testDimension2 = new c2g__codaDimension2__c(Name=TEST_DIM_2_NAME,c2g__ReportingCode__c=TEST_DIM_2_NAME);
		testDimension3 = new c2g__codaDimension3__c(Name=TEST_DIM_3_NAME,c2g__ReportingCode__c=TEST_DIM_3_NAME);
		testDimension4 = new c2g__codaDimension4__c(Name=TEST_DIM_4_NAME,c2g__ReportingCode__c=TEST_DIM_4_NAME);

		insert testDimension1;
		insert testDimension2;
		insert testDimension3;
		insert testDimension4;
	}

	public static void createTaxCodes()
	{
		testAccountTaxCode1 = new c2g__codaTaxCode__c(Name=TEST_ACCT_TAX_1_NAME,c2g__Description__c='test tax code', c2g__GeneralLedgerAccount__c=testGLA.id);
		testAccountTaxCode2 = new c2g__codaTaxCode__c(Name=TEST_ACCT_TAX_2_NAME,c2g__GeneralLedgerAccount__c=testGLA.id);
		testAccountTaxCode3 = new c2g__codaTaxCode__c(Name=TEST_ACCT_TAX_3_NAME,c2g__GeneralLedgerAccount__c=testGLA.id);
		testAccountOutputVatCode = new c2g__codaTaxCode__c(Name=TEST_ACCT_OUTPUT_VAT_TAX_NAME,c2g__GeneralLedgerAccount__c=testGLA.id);
		testProductTaxCode = new c2g__codaTaxCode__c(Name=TEST_PROD_TAX_NAME,c2g__GeneralLedgerAccount__c=testGLA.id);
		testCompanyTaxCode = new c2g__codaTaxCode__c(Name=TEST_COMP_TAX_NAME,c2g__GeneralLedgerAccount__c=testGLA.id,c2g__Description__c='Test TaxCode');
		testAccountTaxCode1.c2g__TaxGroup__c = 'VAT100' ;
		testAccountTaxCode1.c2g__UnitOfWork__c = 1;
		insert new List<c2g__codaTaxCode__c> {testAccountTaxCode1,testAccountTaxCode2,testAccountTaxCode3,testAccountOutputVatCode,testProductTaxCode,testCompanyTaxCode};
	}

	public static void createTaxRates()
	{
		List<c2g__codaTaxRate__c> rates = new List<c2g__codaTaxRate__c>();

		if (testAccountTaxCode1!=null)
		{
			testAccountTaxCode1PastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode1.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_ACCT_TAX_1_PAST_RATE, c2g__UnitOfWork__c=1);
			testAccountTaxCode1CurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode1.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_ACCT_TAX_1_CURRENT_RATE);
			testAccountTaxCode1FutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode1.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_ACCT_TAX_1_FUTURE_RATE);
			rates.add(testAccountTaxCode1PastRate);
			rates.add(testAccountTaxCode1CurrentRate);
			rates.add(testAccountTaxCode1FutureRate);
		}
		if (testAccountTaxCode2!=null)
		{
			testAccountTaxCode2PastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode2.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_ACCT_TAX_2_PAST_RATE);
			testAccountTaxCode2CurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode2.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_ACCT_TAX_2_CURRENT_RATE);
			testAccountTaxCode2FutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode2.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_ACCT_TAX_2_FUTURE_RATE);
			rates.add(testAccountTaxCode2PastRate);
			rates.add(testAccountTaxCode2CurrentRate);
			rates.add(testAccountTaxCode2FutureRate);
		}
		if (testAccountTaxCode3!=null)
		{
			testAccountTaxCode3PastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode3.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_ACCT_TAX_3_PAST_RATE);
			testAccountTaxCode3CurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode3.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_ACCT_TAX_3_CURRENT_RATE);
			testAccountTaxCode3FutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountTaxCode3.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_ACCT_TAX_3_FUTURE_RATE);
			rates.add(testAccountTaxCode3PastRate);
			rates.add(testAccountTaxCode3CurrentRate);
			rates.add(testAccountTaxCode3FutureRate);
		}
		if (testAccountOutputVatCode!=null)
		{
			testAccountOutputVatCodePastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountOutputVatCode.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_ACCT_OUTPUT_VAT_TAX_PAST_RATE);
			testAccountOutputVatCodeCurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountOutputVatCode.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_ACCT_OUTPUT_VAT_TAX_CURRENT_RATE);
			testAccountOutputVatCodeFutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testAccountOutputVatCode.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_ACCT_OUTPUT_VAT_TAX_FUTURE_RATE);
			rates.add(testAccountOutputVatCodePastRate);
			rates.add(testAccountOutputVatCodeCurrentRate);
			rates.add(testAccountOutputVatCodeFutureRate);
		}
		if (testProductTaxCode!=null)
		{
			testProductTaxCodePastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testProductTaxCode.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_PROD_TAX_PAST_RATE);
			testProductTaxCodeCurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testProductTaxCode.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_PROD_TAX_CURRENT_RATE);
			testProductTaxCodeFutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testProductTaxCode.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_PROD_TAX_FUTURE_RATE);
			rates.add(testProductTaxCodePastRate);
			rates.add(testProductTaxCodeCurrentRate);
			rates.add(testProductTaxCodeFutureRate);
		}
		if (testCompanyTaxCode!=null)
		{
			testCompanyTaxCodePastRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testCompanyTaxCode.Id, c2g__StartDate__c=TEST_PAST_DATE, c2g__Rate__c=TEST_COMP_TAX_PAST_RATE);
			testCompanyTaxCodeCurrentRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testCompanyTaxCode.Id, c2g__StartDate__c=TEST_CURRENT_DATE, c2g__Rate__c=TEST_COMP_TAX_CURRENT_RATE);
			testCompanyTaxCodeFutureRate = new c2g__codaTaxRate__c(c2g__TaxCode__c=testCompanyTaxCode.Id, c2g__StartDate__c=TEST_FUTURE_DATE, c2g__Rate__c=TEST_COMP_TAX_FUTURE_RATE);
			rates.add(testCompanyTaxCodePastRate);
			rates.add(testCompanyTaxCodeCurrentRate);
			rates.add(testCompanyTaxCodeFutureRate);
		}

		insert rates;
	}

	public static void createGLAs()
	{
		testGLA = new c2g__codaGeneralLedgerAccount__c();
		testGLA.Name = TEST_GLA_NAME;
		testGLA.c2g__ReportingCode__c = TEST_GLA_REPORTINGCODE;
		testGLA.c2g__Type__c = TEST_GLA_TYPE;

		if(UserInfo.isMultiCurrencyOrganization())
			testGLA.put('CurrencyIsoCode', getCorporateCurrency());

		testGLA2 = new c2g__codaGeneralLedgerAccount__c();
		testGLA2.Name = TEST_GLA_NAME + '-2';
		testGLA2.c2g__ReportingCode__c = TEST_GLA_REPORTINGCODE + '-2';
		testGLA2.c2g__Type__c = TEST_GLA_TYPE;

		if(UserInfo.isMultiCurrencyOrganization())
			testGLA2.put('CurrencyIsoCode', getCorporateCurrency());

		testGLA3 = new c2g__codaGeneralLedgerAccount__c();
		testGLA3.Name = TEST_GLA_NAME + '-3';
		testGLA3.c2g__ReportingCode__c = TEST_GLA_REPORTINGCODE + '-3';
		testGLA3.c2g__Type__c = TEST_GLA_TYPE;

		if(UserInfo.isMultiCurrencyOrganization())
			testGLA3.put('CurrencyIsoCode', getCorporateCurrency());

		testGLA4 = new c2g__codaGeneralLedgerAccount__c();
		testGLA4.Name = TEST_GLA_NAME + '-4';
		testGLA4.c2g__ReportingCode__c = TEST_GLA_REPORTINGCODE + '-4';
		testGLA4.c2g__Type__c = TEST_GLA_TYPE;

		if(UserInfo.isMultiCurrencyOrganization())
			testGLA4.put('CurrencyIsoCode', getCorporateCurrency());

		c2g__codaGeneralLedgerAccount__c[] testGLAs = new c2g__codaGeneralLedgerAccount__c[]{testGLA, testGLA2, testGLA3, testGLA4};

		insert testGLAs;
	}

	public static c2g__codaGeneralLedgerAccount__c createGLA(string glName, string repCode, boolean doInsert)
	{
		c2g__codaGeneralLedgerAccount__c newGLA = new c2g__codaGeneralLedgerAccount__c(
			Name = glName
			,c2g__ReportingCode__c = repCode
			,c2g__Type__c = TEST_GLA_TYPE
		);

		if(UserInfo.isMultiCurrencyOrganization())
			newGLA.put('CurrencyIsoCode', getCorporateCurrency());

		if (doInsert)
		{
			insert newGLA;
		}

		return newGLA;
	}

	public static void createAccount()
	{
		createAccount(false, TEST_VAT_STATUS, TEST_TAX_CALCULATION_METHOD);
	}

	public static void createAccountAltCurrency()
	{
		createAccount(true,null,null);
	}

	public static void createVATAccount(String vatStatus, String taxCalculationMethod)
	{
		createAccount(false,vatStatus, taxCalculationMethod);
	}

	public static void createAccount(Boolean useAltCurrency, String vatStatus, String taxCalculationMethod)
	{
		testAccount = new Account(Name=TEST_ACCOUNT_NAME);

		if (UserInfo.isMultiCurrencyOrganization())
		{
			if (useAltCurrency)
				testAccount.put('CurrencyIsoCode',getAltCurrency());
			else
				testAccount.put('CurrencyIsoCode',getCorporateCurrency());
		}

		if (testDimension1!=null)
			testAccount.c2g__CODADimension1__c = testDimension1.Id;
		if (testDimension2!=null)
			testAccount.c2g__CODADimension2__c = testDimension2.Id;
		if (testDimension3!=null)
			testAccount.c2g__CODADimension3__c = testDimension3.Id;
		if (testDimension4!=null)
			testAccount.c2g__CODADimension4__c = testDimension4.Id;
		if (testAccountTaxCode1!=null)
			testAccount.c2g__CODATaxCode1__c = testAccountTaxCode1.Id;
		if (testAccountTaxCode2!=null)
			testAccount.c2g__CODATaxCode2__c = testAccountTaxCode2.Id;
		if (testAccountTaxCode3!=null)
			testAccount.c2g__CODATaxCode3__c = testAccountTaxCode3.Id;

		testAccount.c2g__CODAVATStatus__c = vatStatus;

		if (testAccountOutputVatCode!=null)
			testAccount.c2g__CODAOutputVATCode__c = testAccountOutputVatCode.Id;

		testAccount.c2g__CODATaxCalculationMethod__c = taxCalculationMethod;
		testAccount.c2g__CODAAccountsReceivableControl__c = testGLA.id;
		testAccount.c2g__CODAAccountsPayableControl__c = testGLA.id;
		testAccount.c2g__CODADiscount1__c = TEST_ACCT_DISC_1_RATE;
		testAccount.c2g__CODABaseDate1__c = 'Invoice Date';
		testAccount.c2g__CODADaysOffset1__c = 1;
		testAccount.c2g__CODADescription1__c = 'blah';
		testAccount.c2g__CODAECCountryCode__c = TEST_COUNTRY_CODE ;
		testAccount.c2g__CODAVATRegistrationNumber__c = TEST_VAT_REGISTRATION_NUMBER ;

		insert testAccount;
	}

	public static void createProduct()
	{
		createProduct(false);
	}

	public static void createProduct(Boolean exempt)
	{
		testProduct = new Product2(Name=TEST_PRODUCT_NAME);

		if (testProductTaxCode!=null)
			testProduct.c2g__CODATaxCode__c = testAccountTaxCode1.Id;
		if (exempt)
			testProduct.c2g__CODASalesTaxStatus__c = 'Exempt';

		testProduct.c2g__CODASalesRevenueAccount__c = testGLA.id;
		testProduct.c2g__CODAPurchaseAnalysisAccount__c = testGLA.id;

		insert testProduct;
	}

	public static void createPricebookEntries()
	{
		createPricebookEntriesMC();
	}

	private static void createPricebookEntriesMC()
	{
		if (testProduct==null)
			return;

		Pricebook2 pb = [select Id from Pricebook2 where IsStandard=true];
		List<PricebookEntry> pbeList = new List<PricebookEntry>();

		testPricebookEntry = new PricebookEntry(Pricebook2Id=pb.Id,Product2Id=testProduct.Id,UnitPrice=TEST_PRICE,IsActive=true);

		if (UserInfo.isMultiCurrencyOrganization())
			testPricebookEntry.put('CurrencyIsoCode',getCorporateCurrency());

		pbeList.add(testPricebookEntry);

		if (UserInfo.isMultiCurrencyOrganization() && getAltCurrency()!=null)
		{
			testAltPricebookEntry = new PricebookEntry(Pricebook2Id=pb.Id,Product2Id=testProduct.Id,UnitPrice=TEST_ALT_PRICE,IsActive=true);
			testAltPricebookEntry.put('CurrencyIsoCode',getAltCurrency());
			pbeList.add(testAltPricebookEntry);
		}

		insert pbeList;
	}

	public static void createCompany()
	{
		createCompany(true);
	}

	public static void createVATCompany()
	{
		createCompany(false);
	}

	public static void createCompany(Boolean isSUT)
	{
		RecordType recType;

		if (isSUT)
		{
			//Commented Query is correct query, but due to a saleforce bug we can't use it now. On package upload this query will not
			//Return any rows, once salesforce fix this bug we will revert it.
			recType = [select Id, DeveloperName From RecordType where SobjectType = 'c2g__codaCompany__c' and DeveloperName='SUT'];
			testCompany = new c2g__codaCompany__c(Name=TEST_COMPANY_NAME,RecordTypeId=recType.Id);
		}
		else
		{
			recType = [select Id, DeveloperName From RecordType where SobjectType = 'c2g__codaCompany__c' and DeveloperName='VAT'];
			testCompany = new c2g__codaCompany__c(Name=TEST_COMPANY_NAME, RecordTypeId=recType.Id, c2g__ECCountryCode__c=TEST_COUNTRY_CODE, c2g__VATRegistrationNumber__c=TEST_VAT_REGISTRATION_NUMBER);
		}

		//system.runAs(new User(Id=UserInfo.getUserId())) {
		prepCompany(testCompany);
		insert testCompany;
		//}
	}

	public static void createCompanies()
	{
		RecordType recType;
		recType = [select Id, DeveloperName From RecordType where SobjectType = 'c2g__codaCompany__c' and DeveloperName='VAT'];
		testCompany = new c2g__codaCompany__c(Name=TEST_COMPANY_NAME,RecordTypeId=recType.Id);
		prepCompany(testCompany);
		testCompany2 = new c2g__codaCompany__c(Name=TEST_COMPANY_NAME+'2',RecordTypeId=recType.Id);
		prepCompany(testCompany2);
		insert new List<c2g__codaCompany__c>{testCompany,testCompany2};
	}

	private static void prepCompany(c2g__codaCompany__c comp)
	{
		if (testCompanyTaxCode!=null)
			comp.c2g__TaxCode__c = testCompanyTaxCode.Id;
	}

	public static void createCurrency()
	{
		id testownerid = [Select ownerid from c2g__codaCompany__c where id=:testCompany.id limit 1].ownerid;
		testCurrency = new c2g__codaAccountingCurrency__c(Name=getCorporateCurrency(), c2g__DecimalPlaces__c=2, c2g__OwnerCompany__c=testCompany.Id, c2g__Home__c = true, ownerid=testownerid);

		if (UserInfo.isMultiCurrencyOrganization())
		{
			testCurrency.put('CurrencyIsoCode',getCorporateCurrency());
		}

		insert testCurrency;
	}

	public static void createCurrencies()
	{
		String corpCurr = getCorporateCurrency();
		String altCutt = getAltCurrency();

		if (corpCurr!=null)
			testCurrency = new c2g__codaAccountingCurrency__c(Name=corpCurr, c2g__DecimalPlaces__c=2, c2g__OwnerCompany__c=testCompany.Id, c2g__Home__c = true);

		if (altCutt!=null)
			testAltCurrency = new c2g__codaAccountingCurrency__c(Name=altCutt, c2g__DecimalPlaces__c=2, c2g__OwnerCompany__c=testCompany.Id, c2g__Dual__c = true);

		if (UserInfo.isMultiCurrencyOrganization())
		{
			if (testCurrency!=null)
				testCurrency.put('CurrencyIsoCode',corpCurr);

			if (testAltCurrency!=null)
				testAltCurrency.put('CurrencyIsoCode',altCutt);
		}

		List<c2g__codaAccountingCurrency__c> currencies = new List<c2g__codaAccountingCurrency__c>();

		if (testCurrency != null)
			currencies.add(testCurrency);

		if (testAltCurrency != null)
			currencies.add(testAltCurrency);

		if (currencies.size() > 0)
			insert currencies;
	}

	public static void createExchangeRates()
	{
		if (testAltCurrency!=NULL)
		{
		   c2g__codaExchangeRate__c exchangeRate = new c2g__codaExchangeRate__c(c2g__OwnerCompany__c = testCompany.id,
				c2g__ExchangeRateCurrency__c = testAltCurrency.id,  c2g__Rate__c = 0.7, c2g__StartDate__c = Date.Today());

		   if (UserInfo.isMultiCurrencyOrganization())
		   {
			   exchangeRate.put('CurrencyIsoCode', getAltCurrency());
		   }

		   insert exchangeRate;
		}
	}

	public static void createFinancialCalendar()
	{
		system.assert(testCompany!=null);
		id testownerid = [Select ownerid from c2g__codaCompany__c where id=:testCompany.id limit 1].ownerid;
		testYear = new c2g__codaYear__c(Name=String.valueOf(TEST_CURRENT_YEAR), c2g__StartDate__c=Date.newInstance(TEST_CURRENT_YEAR,01,01), c2g__NumberOfPeriods__c=12, c2g__OwnerCompany__c=testCompany.Id, ownerid=testownerid, c2g__PeriodCalculationBasis__c='Month End',c2g__EndDate__c=Date.newInstance(TEST_CURRENT_YEAR,12,31));
		testYearPrevious = new c2g__codaYear__c(Name=String.valueOf(TEST_PREVIOUS_YEAR), c2g__StartDate__c=Date.newInstance(TEST_PREVIOUS_YEAR,01,01), c2g__NumberOfPeriods__c=1, c2g__OwnerCompany__c=testCompany.Id, ownerid=testownerid, c2g__PeriodCalculationBasis__c='Month End');
		testYearNext = new c2g__codaYear__c(Name=String.valueOf(TEST_FUTURE_YEAR), c2g__StartDate__c=Date.newInstance(TEST_FUTURE_YEAR,01,01), c2g__NumberOfPeriods__c=1, c2g__OwnerCompany__c=testCompany.Id, ownerid=testownerid, c2g__PeriodCalculationBasis__c='Month End');
		insert new List<c2g__codaYear__c>{testYear,testYearPrevious,testYearNext};

		/*group[] gs = [select Id, name from group where type='Queue' and name like :'%' + TEST_COMPANY_NAME + '%'];
		system.debug('+++queues:' + gs);
		queuesobject[] qsos = [select Id, sobjecttype
			from queuesobject
			where queueid = :gs[0].Id];
		system.debug('+++qsos:' + qsos);*/

		String returnedString = c2g.CODAYearWebService.calculatePeriods(testYear.id);
		system.debug('+++calc period return: ' + returnedString);
		list<c2g__codaPeriod__c> testperiods = [Select id,name from c2g__codaPeriod__c where c2g__YearName__c =:testYear.id and c2g__OwnerCompany__c=:testCompany.id];
		testPeriod = testperiods.get(0);
	}

	public static void createTextDefinitions()
	{
		testDefaultInvoiceTextDefinition1 = new c2g__codaTextDefinition__c(Name=TEST_DEF_INV_TXT_DEF_1_NAME, c2g__Invoice__c='Default', c2g__TextPosition__c='1', c2g__Heading__c=TEST_DEF_INV_TXT_DEF_1_NAME, c2g__Text__c=TEST_DEF_INV_TXT_DEF_1_NAME, c2g__AllowEdit__c=true, c2g__OwnerCompany__c=testCompany.Id);
		testDefaultInvoiceTextDefinition2 = new c2g__codaTextDefinition__c(Name=TEST_DEF_INV_TXT_DEF_2_NAME, c2g__Invoice__c='Default', c2g__TextPosition__c='2', c2g__Heading__c=TEST_DEF_INV_TXT_DEF_2_NAME, c2g__Text__c=TEST_DEF_INV_TXT_DEF_2_NAME, c2g__AllowEdit__c=true, c2g__OwnerCompany__c=testCompany.Id);
		testDefaultInvoiceTextDefinition3 = new c2g__codaTextDefinition__c(Name=TEST_DEF_INV_TXT_DEF_3_NAME, c2g__Invoice__c='Default', c2g__TextPosition__c='3', c2g__Heading__c=TEST_DEF_INV_TXT_DEF_3_NAME, c2g__Text__c=TEST_DEF_INV_TXT_DEF_3_NAME, c2g__AllowEdit__c=true, c2g__OwnerCompany__c=testCompany.Id);
		testDefaultInvoiceTextDefinition4 = new c2g__codaTextDefinition__c(Name=TEST_DEF_INV_TXT_DEF_4_NAME, c2g__Invoice__c='Default', c2g__TextPosition__c='4', c2g__Heading__c=TEST_DEF_INV_TXT_DEF_4_NAME, c2g__Text__c=TEST_DEF_INV_TXT_DEF_4_NAME, c2g__AllowEdit__c=true, c2g__OwnerCompany__c=testCompany.Id);
		testDefaultInvoiceTextDefinition5 = new c2g__codaTextDefinition__c(Name=TEST_DEF_INV_TXT_DEF_5_NAME, c2g__Invoice__c='Default', c2g__TextPosition__c='5', c2g__Heading__c=TEST_DEF_INV_TXT_DEF_5_NAME, c2g__Text__c=TEST_DEF_INV_TXT_DEF_5_NAME, c2g__AllowEdit__c=true, c2g__OwnerCompany__c=testCompany.Id);
		insert new List<c2g__codaTextDefinition__c> {testDefaultInvoiceTextDefinition1,testDefaultInvoiceTextDefinition2,testDefaultInvoiceTextDefinition3,testDefaultInvoiceTextDefinition4,testDefaultInvoiceTextDefinition5};
	}

	public static void createUser()
	{
		Profile p = [select id from profile where name='System Administrator'];

		testUser = new User(alias = 'standt', email='standarduser@testorg.com',
			emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='testadminuser@testorg.com');

		insert testUser;
	}

	public static void createHomeAndDualCurrency(String CurrencyCode)
	{
		id testownerid = [Select ownerid from c2g__codaCompany__c where id=:testCompany.id limit 1].ownerid;
		testCurrency = new c2g__codaAccountingCurrency__c(Name=CurrencyCode, c2g__DecimalPlaces__c=2, c2g__OwnerCompany__c=testCompany.Id, c2g__Home__c = true, c2g__Dual__c = true, OwnerId = testownerid);

		if (UserInfo.isMultiCurrencyOrganization())
		{
			testCurrency.put('CurrencyIsoCode', CurrencyCode);
		}

		insert testCurrency;
	}

	public static void SelectCompanyForUser()
	{
		//activate company
		c2g.CODACompanyWebService.createQueue2(testCompany.id , null );
		c2g.CODACompanyWebService.activateCompany2(testCompany.id , null );

		//assign company to user
		c2g__codaUserCompany__c testUserCompany1 = new c2g__codaUserCompany__c(c2g__User__c=UserInfo.getUserId(),
			c2g__Company__c=testCompany.id);

		insert testUserCompany1;

		// select the company for user
		string queueName = 'FF ' + testCompany.name;
		list<Group> lstGroup =  [Select id, name from Group where OwnerId=:UserInfo.getUserId() and name = :queueName order by createdDate];
		Group testGroup = lstGroup.get(0);

		GroupMember testGroupMember = new GroupMember(UserOrGroupId=UserInfo.getUserId(),GroupId=testGroup.id);
		insert testGroupMember;

		//assign group to company
		testCompany.OwnerId = testGroup.id;
		update testCompany;
	}

	public static void SelectMultipleCompanies()
	{
		//activate company
		c2g.CODACompanyWebService.createQueue2(FFA_SetupTest.testCompany.id , null );
		c2g.CODACompanyWebService.activateCompany2(FFA_SetupTest.testCompany.id , null );
		c2g.CODACompanyWebService.createQueue2(FFA_SetupTest.testCompany2.id , null );
		c2g.CODACompanyWebService.activateCompany2(FFA_SetupTest.testCompany2.id , null );

		// select the company for user
		list<Group> lstGroup =  [Select g.Type, g.OwnerId, g.Name, g.Id, g.CreatedById From Group g where OwnerId=:UserInfo.getUserId() order by createdDate];
		Group testGroup1 = lstGroup[0];
		Group testGroup2 = lstGroup[1];
		GroupMember testGroupMember1 = new GroupMember(UserOrGroupId=UserInfo.getUserId(),GroupId=testGroup1.id);
		GroupMember testGroupMember2 = new GroupMember(UserOrGroupId=UserInfo.getUserId(),GroupId=testGroup2.id);

		insert new list<GroupMember>{testGroupMember1,testGroupMember2};

		//assign group to company
		FFA_SetupTest.testCompany.OwnerId = testGroup1.id;
		FFA_SetupTest.testCompany2.OwnerId = testGroup2.id;
		update new list<c2g__codaCompany__c>{FFA_SetupTest.testCompany, FFA_SetupTest.testCompany2};
	}

	public static void createBankAccount()
	{
		testBankAccount = new c2g__codaBankAccount__c();
		testBankAccount.name = TEST_BANK_ACCOUNT_NAME ;
		testBankAccount.c2g__ReportingCode__c = TEST_BANK_ACCOUNT_REPORTINGCODE ;
		testBankAccount.c2g__BankName__c = TEST_BANK_ACCOUNT_NAME ;
		testBankAccount.c2g__AccountName__c = TEST_BANK_ACCOUNT_NAME ;
		testBankAccount.c2g__AccountNumber__c = TEST_BANK_ACCOUNT_NAME ;
		testBankAccount.c2g__BankAccountCurrency__c = testCurrency.id;
		testBankAccount.c2g__GeneralLedgerAccount__c = testGLA.id;
		insert testBankAccount;
	}

	//=================== Function to support removal of SCMFFSetupTest class ============================
	public static List<c2g__codaGeneralLedgerAccount__c> createGLAccounts()
	{
		List<c2g__codaGeneralLedgerAccount__c> gls = new List<c2g__codaGeneralLedgerAccount__c>();
		c2g__codaGeneralLedgerAccount__c glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testRevenue'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Profit and Loss'
				,c2g__TrialBalance1__c = 'Profit and Loss'
				,c2g__TrialBalance2__c = 'Operating Expenses'
				,c2g__TrialBalance3__c = 'Personnel Costs'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '5097'
				,c2g__BalanceSheet1__c = null
				,c2g__BalanceSheet2__c = null
				,c2g__BalanceSheet3__c = null
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		c2g__codaGeneralLedgerAccount__c[] apAcct = [
			select
				id
			from c2g__codaGeneralLedgerAccount__c
			where c2g__ReportingCode__c = '2000'];

		if (apAcct.Size() == 0){
			c2g__codaGeneralLedgerAccount__c apAcct2 = new c2g__codaGeneralLedgerAccount__c(
				name = '2000 - Accounts Payable'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Payable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '2000'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Payable'
				,c2g__AllowRevaluation__c = false);
			gls.add(apAcct2);
		}

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testPayable'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Payable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '2007'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Payable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testReceivable'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Receivable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '3007'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Receivable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testInventory'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Receivable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '4007'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Receivable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testInventoryShipped'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Receivable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '4008'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Receivable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testCOG'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Receivable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '5007'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Receivable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		glAccount =
			new c2g__codaGeneralLedgerAccount__c(
				name = 'testWIP'
				,c2g__UnitOfWork__c = 1
				,c2g__Type__c = 'Balance Sheet'
				,c2g__TrialBalance1__c = 'Balance Sheet'
				,c2g__TrialBalance2__c = 'Current Liabilities'
				,c2g__TrialBalance3__c = 'Accounts Receivable'
				,c2g__TrialBalance4__c = null
				,c2g__ReportingCode__c = '6007'
				,c2g__BalanceSheet1__c = 'Balance Sheet'
				,c2g__BalanceSheet2__c = 'Current Liabilities'
				,c2g__BalanceSheet3__c = 'Accounts Receivable'
				,c2g__AllowRevaluation__c = false);
		gls.add(glAccount);

		insert gls;

		return gls;
	}
}