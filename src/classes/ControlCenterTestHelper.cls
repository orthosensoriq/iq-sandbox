public without sharing class ControlCenterTestHelper{
		
	public ControlCenterTestHelper(){
		
		Patient__c patient1 = new Patient__c();
		patient1.First_Name__c = 'Test';
		patient1.Last_Name__c = 'TestPat';
		patient1.Date_Of_Birth__c = System.today();
		patient1.Gender__c = 'Male';
		
		insert patient1;
		
		Event procedure1 = new Event();
		procedure1.WhatId = patient1.Id;
		procedure1.DurationInMinutes = 1;
		procedure1.ActivityDateTime = System.today();
		
		insert procedure1;
	}
}