/**
 *  Copyright 2013 Less Software, Inc.
 *  Utility class to set up test data for tests
 */
@isTest
public with sharing class SCMFFSetUpTest {

    private SCMC__Condition_Code__c cCode = this.createConditionCode('New', 'NE');
    private SCMC__Currency_Master__c curr = this.createTestCurrency();
    private static Integer suffix = 0;

    public static Map<String,List<RecordType>>rTypes = new Map<String, List<RecordType>>();

    public SCMC__PO_Payment_Terms__c createPOPaymentTerm() {
    	SCMC__PO_Payment_Terms__c term = new SCMC__PO_Payment_Terms__c(
			SCMC__Terms_Name__c = '3/30 Net 60'
			,SCMC__Description__c = '3% discount if paid within 30 days'
			,SCMC__Number_of_Days__c = 60
			,SCMC__Discount_Percentage__c = 3
			,SCMC__Discount_Days__c = 30
			//,SCMC__Date_Due__c = ' '
			,SCMC__Basis_for_Due__c = 'Invoice Date'
			);
		insert term;
		return term;
    }
    public SCMC__Purchase_Order__c createPurchaseOrder(){
        return createPurchaseOrder(true);
    }

    public SCMC__Purchase_Order__c createPurchaseOrder(boolean doSave){
        SCMC__Supplier_Site__c supplier = createTestSupplier();
        return createPurchaseOrder(supplier, doSave);
    }

    public SCMC__Purchase_Order__c createPurchaseOrder(SCMC__Supplier_Site__c supplier, boolean doSave)
    {
        SCMC__Purchase_Order__c po = new SCMC__Purchase_Order__c();

        po.SCMC__Supplier_Site__c = supplier.Id;
        po.SCMC__Status__c = 'Open';
        po.SCMC__Purchase_Order_Date__c = Date.today();

        if (doSave){
            insert po;
        }
        return po;
    }

    public SCMC__Org_Address__c createOrg(){

        SCMC__Org_Address__c orgAddress = SCMC__Org_Address__c.getInstance();

        if(orgAddress == null) {
            orgAddress = new SCMC__Org_Address__c();
            orgAddress.SCMC__City__c = 'Unknown';
            orgAddress.SCMC__Country__c= 'Unknown';
            orgAddress.SCMC__Fax__c = '';
            orgAddress.SCMC__Name__c = 'Unknown';
            orgAddress.SCMC__Phone_Number__c = '901.218.3515';
            orgAddress.SCMC__Postal_Zip_Code__c = 'Unknown';
            orgAddress.SCMC__State__c = 'Unknown';
            orgAddress.SCMC__Street__c = 'Unknown';
            insert orgAddress;
         }

         return orgAddress;
    }

    public SCMC__Supplier_Site__c createTestSupplier() {

        SCMC__PO_Payment_Terms__c term = createPOPaymentTerm();
        SCMC__Supplier_Site__c sc = new SCMC__Supplier_Site__c();
        sc.name = 'Test supplier for unit tests';
        sc.SCMC__Currency__c = curr.Id;
        sc.SCMC__Preferred_Communication_Sourcing__c = 'E-Mail';
        sc.SCMC__Preferred_Communication_Purchase_Order__c = 'E-Mail';
        sc.SCMC__E_Mail__c = 'test@test.com';
        sc.SCMC__PO_Payment_Terms__c = term.id;
        sc.SCMC__Active__c = true;
        insert sc;

        sc.SCMC__Active__c = true;
        update sc;

        return sc;
    }

   public SCMC__Currency_Master__c createTestCurrency() {
        return createTestCurrency('USD', true);
   }
   public SCMC__Currency_Master__c createTestCurrency(String cname, Boolean corporate) {
        SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
        try {
            curr = [select id, name
                    from SCMC__Currency_Master__c
                    where name = :cname];
        } catch (Exception ex) {
            curr.name = cname;
            curr.SCMC__Active__c = true;
            curr.SCMC__Conversion_Rate__c = 1;
            curr.SCMC__Corporate_Currency__c = corporate;
            insert curr;
        }
        return curr;
    }

    public SCMC__Condition_Code__c createConditionCode(String code, String ilscode) {
        SCMC__Condition_Code__c cCode = new SCMC__Condition_Code__c();
        try {
            cCode = [select id
                       from SCMC__Condition_Code__c
                      where name = :code];
        } catch (Exception ex) {
            cCode.name = code;
            cCode.SCMC__description__c = 'Description for ' + code;
            cCode.SCMC__Disposal_Code__c = '1 -Excellent';
            cCode.SCMC__Supply_Code__c = 'A -Serviceable';
            insert cCode;
        }
        return cCode;
    }

    public SCMC__Purchase_Order_Line_Item__c createPurchaseOrderLine(SCMC__Purchase_Order__c po){
        //create warehouse
        SCMC__Warehouse__c whouse = setupWarehouse();
        return createPurchaseOrderLine(po, whouse, true, true);
    }

    public SCMC__Purchase_Order_Line_Item__c createPurchaseOrderLine(SCMC__Purchase_Order__c po, SCMC__Warehouse__c warehouse, Boolean doInsert, Boolean active) {
        SCMC__Item__c item = createTestItem();
        return createPurchaseOrderLine(po, item, warehouse, doInsert, active);
    }

    public SCMC__Purchase_Order_Line_Item__c createPurchaseOrderLine(SCMC__Purchase_Order__c po, SCMC__Item__c item, SCMC__Warehouse__c warehouse, Boolean doInsert, Boolean active) {

        SCMC__Purchase_Order_Line_Item__c line = new SCMC__Purchase_Order_Line_Item__c();

        line.RecordTypeId = SCMFF_Utilities.getRecordType('Item', line);
        line.SCMC__Purchase_Order__c = po.Id;
        line.SCMC__Supplier_Commitment_Date__c = Date.today();
        line.SCMC__Supplier_Current_Promise_Date__c = Date.today();
        line.SCMC__Status__c = 'Open';
        line.SCMC__Item_Master__c = item.id;
        line.SCMC__Quantity__c = 10;

        if (warehouse != null)
        {
            line.SCMC__Warehouse__c = warehouse.Id;
        }

        if (doInsert){
            insert line;
        }

        return line;

    }

    public SCMC__Item__c createTestItem(boolean doInsert) {
        SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'Test' + suffix++;
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
        //im.RecordTypeId = SCMFF_Utilities.getRecordType('Master', im);
        if (doInsert){
        insert im;
        }
        return im;
    }

    public SCMC__Item__c createTestVirtualKitItem(SCMC__Item__c childItem) {
        SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'Test' + suffix++;
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
        im.SCMC__Kit__c = 'Virtual Kit';
        //im.RecordTypeId = SCMFF_Utilities.getRecordType('Master', im);
        insert im;
        SCMC__Bill_of_Material__c bom = createBillOfMaterials(im, childItem);
        return im;
    }

    public SCMC__Bill_of_Material__c createBillOfMaterials(SCMC__Item__c ParentIdeas, SCMC__Item__c childItem){
        SCMC__Bill_of_Material__c bom = new SCMC__Bill_of_Material__c();
        bom.SCMC__Item__c = ParentIdeas.Id;
        bom.SCMC__Level__c = 1;
        //bom.CurrencyIsoCode = 'USD';
        bom.SCMC__QPA_Use__c = 1;
        bom.SCMC__QPA__c = 0;
        insert bom;

        SCMC__Bill_of_Material__c childBom = new SCMC__Bill_of_Material__c();
        childBom.SCMC__Item__c = childItem.Id;
        childBom.SCMC__Parent__c = bom.Id;
        childBom.SCMC__Level__c = 2;
        //childBom.CurrencyIsoCode = 'USD';
        childBom.SCMC__QPA_Use__c = 3;
        childBom.SCMC__QPA__c = 0;
        insert childBom;

        return bom;
    }

    public SCMC__Unit_of_Measure__c createUnitofMeasure(String name, String ilsName) {
        SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
        try {
            uom = [select id
                    from SCMC__Unit_of_Measure__c
                    where name = :name];
        } catch(Exception ex) {
            uom.name =name;
            if (ilsName <> null) {
                uom.SCMC__ILS_Unit_of_Measure__c = ilsName;
                uom.SCMC__Valid_for_ILS__c = true;
            }
            insert uom;
    }
        return uom;
    }


    public SCMC__Warehouse__c setupWarehouse() {
        SCMC__Address__c address = createTestAddress();
        SCMC__ICP__c icp = createTestICP(address);
        return createTestWarehouse(icp, address);
    }

    public SCMC__Address__c createTestAddress() {
        SCMC__Address__c address = new SCMC__Address__c();
        address.name = 'Test Address';
        address.SCMC__City__c = 'A City';
        address.SCMC__Country__c = 'Country';
        address.SCMC__Line1__c = 'Address line 1';
        address.SCMC__PostalCode__c = 'Postcd';
        address.SCMC__State__c = 'State';
        insert address;
        return address;
    }

    public SCMC__ICP__c createTestICP(SCMC__Address__c address) {
    	List<SCMC__ICP__c>icps = [select id, SCMFFA__Company__c from SCMC__ICP__c];
        SCMC__ICP__c icp = new SCMC__ICP__c();
        if (icps.Size() == 0){
        icp.SCMC__Address__c = address.id;
        icp.name = 'Test ICP';
        icp.SCMC__Currency__c = curr.id;
        insert icp;
        } else {
        	icp = icps[0];
        }
        return icp;
    }

    public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address){
        SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
        warehouse.SCMC__ICP__c = icp.id;
        warehouse.SCMC__Address__c = address.id;
        warehouse.name = 'Test Warehouse';
        insert warehouse;
        return warehouse;
    }
    public SCMC__Item__c createTestItem() {
        SCMC__Unit_of_Measure__c uom = createUnitofMeasure('Each',null);
        SCMC__Item__c im = new SCMC__Item__c();
        im.name = 'Test' + suffix++;
        im.SCMC__Item_Description__c = 'Test part for unit tests';
        im.SCMC__Inspection_Required__c  = false;
        im.SCMC__Serial_Number_Control__c = false;
        im.SCMC__Lot_Number_Control__c = false;
        im.SCMC__Stocking_UOM__c = uom.id;
        //im.RecordTypeId = SCMFF_Utilities.getRecordType('Master', im);
        insert im;
        return im;
    }

   public SCMC__Customer_Quotation__c createCustomerQuotation(boolean needAccount){
        SCMC__Customer_Quotation__c quote = new SCMC__Customer_Quotation__c();
        ID salesRep = createSalesRep();
        ID invType = SCMFF_Utilities.getRecordType('Inventory', quote);
        if(needAccount){
        	SCMC__Currency_Master__c curr = createTestCurrency('USD', true);
            Account account = createTestCustomerAccount(true, false, false, null);
            account.SCMC__Currency__c = curr.id;
            insert account;
            quote.SCMC__Customer_Account__c = account.Id;
        } else {
            quote.SCMC__Prospect_Name__c = 'test name';
            quote.SCMC__Mailing_Street__c = '12346 west ';
            quote.SCMC__Prospect_Company__c = 'Test';
        }
        quote.SCMC__Sales_Rep__c = salesRep;
        quote.SCMC__Status__c = 'Open';
        quote.SCMC__Customer_Account_Contact__c = null;
        quote.SCMC__Delivery_Options__c = 'Fax';
        insert quote;

        return quote;
    }
    public SCMC__Customer_Quotation_Line__c createCustomerQuotationLine(SCMC__Customer_Quotation__c quote, SCMC__Item__c item, Boolean doInsert){
    	return createCustomerQuotationLine(quote, item, doInsert, 'Item');
    }

    public SCMC__Customer_Quotation_Line__c createCustomerQuotationLine(SCMC__Customer_Quotation__c quote, SCMC__Item__c item, Boolean doInsert, String rtype){
        SCMC__Customer_Quotation_Line__c quoteLine = new SCMC__Customer_Quotation_Line__c();
        quoteLine.SCMC__Customer_Quotation__c = quote.Id;
        quoteLine.SCMC__Quantity__c = 2;
        if (item != null){
        quoteLine.SCMC__Item_Number__c = item.Id;
        } else {
            quoteLine.SCMC__Item_Number_New__c = 'test item';
        }
        quoteLine.SCMC__Price__c = 10.99;
        quoteLine.SCMC__Customer_Price__c = 11.99;
        quoteLine.RecordTypeId = SCMFF_Utilities.getRecordType(rType, quoteLine);
        quoteLine.SCMC__Condition_Code__c = cCode.Id;
        if (doInsert){
            insert quoteLine;
        }
        return quoteLine;
    }

    public ID createSalesRep(){
        //TODO - at some point actually create a user
        return UserInfo.getUserId();
    }
    /*

    public SCMC__Tax_Code__c createTaxCode(){
        SCMC__Tax_Code__c newTaxcode = new SCMC__Tax_Code__c();
        list<SCMC__Tax_Code__c> taxtList = [Select Id from SCMC__Tax_Code__c where Name = 'YT'];
        if(taxtList.size() > 0){
            newTaxcode = taxtList[0];
        } else {
            newTaxcode.name = 'YT';
            newTaxcode.SCMC__Description__c = 'Tax for billing to the YT';
            newTaxcode.SCMC__Effective_From__c = system.today();
            newTaxcode.SCMC__Inactive__c = false;
            newTaxcode.SCMC__Tax_Rate__c = 0;
            newTaxcode.SCMC__Rate__c = 0;
            newTaxcode.SCMC__Tax_Agency__c = null;
            newTaxcode.SCMC__Tax_Type__c = 'GST';
            insert newTaxcode;
        }
        return newTaxcode;
    }

    */
    public Account createTestCustomerAccount(Boolean custActive, Boolean doInsert, Boolean onHold, id warehouseSpecific) {

        Account custSite = new Account();
        custSite.SCMC__Active__c = custActive;


        if (onHold){
            custSite.SCMC__Temporary_Hold__c = 'Finance';
        }else {
            custSite.SCMC__Temporary_Hold__c = null;
        }

        custSite.AnnualRevenue = null;
        custSite.SCMC__CAGE_Code__c = null;
        custSite.SCMC__Capability_Listing__c = null;
        custSite.SCMC__Certifications__c = null;
        custSite.SCMC__Credit_Card_Expiration_Date__c = null;
        custSite.SCMC__Credit_Card_Number__c = null;
        custSite.SCMC__Credit_Card_Type__c = null;
        custSite.SCMC__Credit_Card_Validation_Code__c = null;
        custSite.SCMC__Customer__c = true;
        custSite.SCMC__DUNS_Number__c = null;
        custSite.SCMC__Default_Payment_Type__c = null;
        custSite.SCMC__Corp_City__c = 'San Diego';
        custSite.SCMC__Corp_Country__c = 'USA';
        custSite.SCMC__Corp_State_Province__c = 'CA';
        custSite.SCMC__Corp_Line1__c = '247 Ocean Blvd.';
        custSite.SCMC__Corp_Line2__c = null;
        custSite.SCMC__Corp_PostalCode__c = '92130';
        custSite.Description = 'Test customer site';
        custSite.FAX = '555-1234';
        custSite.SCMC__FOB__c = null;
        custSite.SCMC__Freight__c = null;
        custSite.Industry = null;
        custSite.SCMC__Language__c = null;
        custSite.Name = 'Test Customer' + string.valueOf(suffix++);
        custSite.NumberOfEmployees = null;
        custSite.Phone = null;
        custSite.Ownership = null;
        custSite.ParentId = null;
        custSite.SCMC__Preferred_Communication__c = 'Fax';
        custSite.SCMC__Provide_Advanced_Ship_Notice__c = false;
        custSite.SIC = null;
        custSite.SCMC__Sales_Rep__c = null;
        custSite.SCMC__Sales_Responsibility__c = null;
        custSite.SCMC__Ship_Via__c = null;
        custSite.SCMC__Small_Business_Designations__c = null;
        custSite.SCMC__Tax_Exemption_Group__c = null;
        custSite.SCMC__Terms__c = null;
        custSite.TickerSymbol = null;
        custSite.Type = null;
        custSite.Website = null;
        custSite.SCMC__warehouse__c = warehouseSpecific;

        custSite.BillingStreet = '123 Main St\nSuite 101\nc/0George';
        custSite.BillingCity = 'San Francisco';
        custSite.BillingState = 'CA';
        custSite.BillingPostalCode = '12345';
        custSite.BillingCountry = 'US';

        custSite.ShippingStreet = '123 Main St\nSuite 101\nc/0George';
        custSite.ShippingCity = 'San Francisco';
        custSite.ShippingState = 'CA';
        custSite.ShippingPostalCode = '12345';
        custSite.ShippingCountry = 'US';

        custSite.SCMC__Ship_Via__c = 'UPS Ground';
        custSite.SCMC__Shipping_Account__c = '123456789';

        if (doInsert){
            insert custSite;
        }
        return custSite;
    }


    public SCMC__ICP__c createTestICP(SCMC__Address__c address, boolean doInsert) {
        return createTestICP(address, createTestCurrency(), doInsert);
    }

    public SCMC__ICP__c createTestICP(SCMC__Address__c address, SCMC__Currency_Master__c curr, boolean doInsert) {
        SCMC__ICP__c icp = new SCMC__ICP__c();
        icp.SCMC__Address__c = address.id;
        icp.name = 'Test ICP';
        icp.SCMC__Currency__c = curr.id;
        icp.SCMC__ILS_User__c = 'xxxxxxx';
        icp.SCMC__ILS_Password__c = 'yyyyyy';
        if (doInsert) insert icp;
        return icp;
    }

    public SCMC__Invoice_Line_Item__c createInvoice(SCMC__Invoicing__c inv, string LineItem){
        return createInvoiceLine(inv, LineItem, true);
    }

    public SCMC__Invoice_Line_Item__c createInvoiceLine(SCMC__Invoicing__c inv, string LineItem, boolean doSave){
        SCMC__Invoice_Line_Item__c invoiceLine = new SCMC__Invoice_Line_Item__c();
        invoiceLine.SCMC__Amount__c = 25.00;
        invoiceLine.SCMC__Quantity__c = 1;
        invoiceLine.SCMC__Invoicing__c = inv.Id;
        invoiceLine.RecordTypeId = SCMFF_Utilities.getRecordType(LineItem, invoiceLine);
        insert invoiceLine;
        return invoiceLine;
    }

    public SCMC__Supplier_Catalogue__c createTestSupplierCatalogue(SCMC__Supplier_Site__c site, boolean doInsert) {
        SCMC__Supplier_Catalogue__c sa = new SCMC__Supplier_Catalogue__c();

        sa.SCMC__Supplier_Site__c = site.Id;
        sa.SCMC__Catalogue_Name__c = 'TestSA1234!$#@%#$%';
        sa.SCMC__Buyer_User__c = this.createTestBuyer();
        sa.SCMC__Available_for_Use__c = true;

        if (doInsert) {
            insert sa;
        }
        return sa;
    }

    public SCMC__Supplier_Catalogue_Item__c createTestSupplierCatalogueItem(SCMC__Supplier_Catalogue__c sc, SCMC__Item__c item, boolean doInsert) {
        SCMC__Supplier_Catalogue_Item__c catItem = new SCMC__Supplier_Catalogue_Item__c();

        catItem.SCMC__Supplier_Catalogue__c = sc.Id;
        if (item != null){
        catItem.SCMC__Item_Master__c = item.Id;
        } else {
            catItem.SCMC__New_Item_Number__c = 'testitem';
            catItem.SCMC__New_Item_Description__c = 'test item description';
        }
        catItem.SCMC__Active__c = true;

        if (doInsert) {
            insert catItem;
        }

        return catItem;
    }

    public SCMC__Supplier_Catalogue_Price_Break__c createTestPriceBreak(SCMC__Supplier_Catalogue_Item__c catItem, boolean doInsert) {
        SCMC__Supplier_Catalogue_Price_Break__c priceBreak = new SCMC__Supplier_Catalogue_Price_Break__c();

        priceBreak.SCMC__Supplier_Catalogue_Item__c = catItem.Id;
        priceBreak.SCMC__Price__c = 10.99;
        priceBreak.SCMC__Valid_Until_Quantity__c = 50;

        if (doInsert){
            insert priceBreak;
        }

        return priceBreak;
    }

    public Id createTestBuyer() {
        //At some point create a user but for now return current id
        return UserInfo.getUserId();
    }

    public SCMC__Customer_Address__c createCustomerAddress(Account a) {
        SCMC__Customer_Address__c address = new SCMC__Customer_Address__c();
        address.SCMC__Active__c = true;
        //address.Address_Number__c
        address.SCMC__Mailing_City__c = 'San Diego';
        address.SCMC__Mailing_Country__c = 'USA';
        address.SCMC__Mailing_State_Province__c = 'CA';
        address.SCMC__Mailing_Street__c = '247 Ocean Blvd.';
        address.SCMC__Mailing_Zip_Postal_Code__c = '92130';
        address.SCMC__Customer_Site__c = a.Id;
        insert address;

        return address;
    }

    public List<c2g__codaGeneralLedgerAccount__c> createGLAccounts() {
        List<c2g__codaGeneralLedgerAccount__c>gls = new List<c2g__codaGeneralLedgerAccount__c>();
        c2g__codaGeneralLedgerAccount__c salesAcct = new c2g__codaGeneralLedgerAccount__c(
            name = 'testRevenue'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Profit and Loss'
            ,c2g__TrialBalance1__c = 'Profit and Loss'
            ,c2g__TrialBalance2__c = 'Operating Expenses'
            ,c2g__TrialBalance3__c = 'Personnel Costs'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '5097'
            ,c2g__BalanceSheet1__c = null
            ,c2g__BalanceSheet2__c = null
            ,c2g__BalanceSheet3__c = null
            ,c2g__AllowRevaluation__c = false);
        gls.add(salesAcct);
		c2g__codaGeneralLedgerAccount__c[] apAcct = [select id
			from c2g__codaGeneralLedgerAccount__c
			where c2g__ReportingCode__c = '2000'];
		if (apAcct.Size() == 0){
			c2g__codaGeneralLedgerAccount__c apAcct2 = new c2g__codaGeneralLedgerAccount__c(
	            name = '2000 - Accounts Payable'
	            ,c2g__UnitOfWork__c = 1
	            ,c2g__Type__c = 'Balance Sheet'
	            ,c2g__TrialBalance1__c = 'Balance Sheet'
	            ,c2g__TrialBalance2__c = 'Current Liabilities'
	            ,c2g__TrialBalance3__c = 'Accounts Payable'
	            ,c2g__TrialBalance4__c = null
	            ,c2g__ReportingCode__c = '2000'
	            ,c2g__BalanceSheet1__c = 'Balance Sheet'
	            ,c2g__BalanceSheet2__c = 'Current Liabilities'
	            ,c2g__BalanceSheet3__c = 'Accounts Payable'
	            ,c2g__AllowRevaluation__c = false);
	        gls.add(apAcct2);
		}
        c2g__codaGeneralLedgerAccount__c apAcct1 = new c2g__codaGeneralLedgerAccount__c(
            name = 'testPayable'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Payable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '2007'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Payable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(apAcct1);
        c2g__codaGeneralLedgerAccount__c arAcct = new c2g__codaGeneralLedgerAccount__c(
            name = 'testReceivable'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Receivable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '3007'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Receivable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(arAcct);
        c2g__codaGeneralLedgerAccount__c invAcct = new c2g__codaGeneralLedgerAccount__c(
            name = 'testInventory'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Receivable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '4007'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Receivable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(invAcct);
        c2g__codaGeneralLedgerAccount__c invShipped = new c2g__codaGeneralLedgerAccount__c(
            name = 'testInventoryShipped'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Receivable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '4008'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Receivable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(invShipped);
        c2g__codaGeneralLedgerAccount__c cogAcct = new c2g__codaGeneralLedgerAccount__c(
            name = 'testCOG'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Receivable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '5007'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Receivable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(cogAcct);
         c2g__codaGeneralLedgerAccount__c wipAcct = new c2g__codaGeneralLedgerAccount__c(
            name = 'testWIP'
            ,c2g__UnitOfWork__c = 1
            ,c2g__Type__c = 'Balance Sheet'
            ,c2g__TrialBalance1__c = 'Balance Sheet'
            ,c2g__TrialBalance2__c = 'Current Liabilities'
            ,c2g__TrialBalance3__c = 'Accounts Receivable'
            ,c2g__TrialBalance4__c = null
            ,c2g__ReportingCode__c = '6007'
            ,c2g__BalanceSheet1__c = 'Balance Sheet'
            ,c2g__BalanceSheet2__c = 'Current Liabilities'
            ,c2g__BalanceSheet3__c = 'Accounts Receivable'
            ,c2g__AllowRevaluation__c = false);
        gls.add(wipAcct);
        insert gls;

		return gls;
    }
	public SCMC__Product_Group__c creteTestProductGroupWithProduct() {
		Product2 std = new Product2(name = 'Test'
    			,IsActive = true
    			,c2g__CODASalesRevenueAccount__c = [select id from
    				c2g__codaGeneralLedgerAccount__c
    				where name = 'testRevenue'].id
    			,c2g__CODAPurchaseAnalysisAccount__c = [select id from
    				c2g__codaGeneralLedgerAccount__c
    				where name = 'testPayable'].id
    			,Family = 'Sales Pumps');
    	insert std;
    	SCMC__Product_Group__c prodGroup = createProdGroup([select id from
    				c2g__codaGeneralLedgerAccount__c
    				where name = 'testInventory'],
					[select id from
    					c2g__codaGeneralLedgerAccount__c
    					where name = 'testCOG']);
    	prodGroup.Product__c = std.id;
    	update prodGroup;
    	return prodGroup;
	}
    public SCMC__Product_Group__c createProdGroup(c2g__codaGeneralLedgerAccount__c inv, c2g__codaGeneralLedgerAccount__c cog) {
        SCMC__Product_Group__c prodGroup = new SCMC__Product_Group__c  (name = 'test group');
        if (inv != null){
            prodGroup.SCMFFA__Inventory_Account__c = inv.id;
        }
        if (cog != null){
            prodGroup.SCMFFA__COGS_Account__c = cog.id;
        }
        c2g__codaDimension1__c dim1 = [select id from c2g__codaDimension1__c limit 1];
        prodGroup.SCMFFA__Dimension_1__c = dim1.id;
        insert prodGroup;
        return prodGroup;
    }

    public SCMC__Price_List__C createPriceList(){
    	SCMC__Currency_Master__c curr = createTestCurrency('CAD',false);

    	SCMC__Price_List__c pl = new SCMC__Price_List__c(name = 'testItem'
    			,SCMC__Quote_Valid_For__c = 15);
    	insert pl;
    	SCMC__Price__c price = new SCMC__Price__c(SCMC__Item_Number__c = pl.id
    			,SCMC__Currency__c = curr.id
    			,SCMC__Pricing_Type__c = 'Standard'
    			,SCMC__Price__c = 10.05);
    	insert price;
    	return pl;
}

    public Contact createTestCustomerContact(boolean active, Account account) {
		Contact contact = new Contact();
		contact.SCMC__Active__c = active;
		contact.AccountId = account.id;
		contact.AssistantPhone = null;
		contact.AssistantName = null;
		contact.Birthdate = null;
		contact.Department = null;
		contact.Description = null;
		contact.Email = 'test@aol.com';
		contact.FAX = null;
		contact.FirstName = 'Test';
		contact.HomePhone = null;
		contact.SCMC__Language__c = null;
		contact.MailingCity = null;
		contact.MailingCountry = null;
		contact.MailingState = null;
		contact.MailingStreet = null;
		contact.MailingPostalCode = null;
		contact.MobilePhone = null;
		contact.Phone = '9015888555';
		contact.OtherPhone = null;
		contact.Salutation = 'Mr.';
		contact.SCMC__Previous_Companies__c = null;
		contact.ReportsTo = null;
		contact.SCMC__Suffix__c = null;
		contact.Title = null;
		contact.LastName = 'Lastname';
		insert contact;
		return contact;
    }

    public SCMC__Sales_Order__c createTestSalesOrder(boolean doInsert){
        Account account = createTestCustomerAccount(true, true, false, null);
		return createTestSalesOrder(account.id, doInsert);
	}
	public SCMC__Sales_Order__c createTestSalesOrder(ID account, boolean doInsert){
        SCMC__Sales_Order__c so = new SCMC__Sales_Order__c();
        ID salesRep = createSalesRep();

        so.SCMC__Sales_Order_Date__c = Date.today();
        so.SCMC__Primary_Sales_Rep__c = salesRep;
        so.SCMC__Customer_Account__c = account;
        so.RecordTypeId = SCMFF_Utilities.getRecordType('Inventory', so);

        if (doInsert){
            insert so;
        }

        return so;
    }

    public SCMC__Sales_Order_Line_Item__c createSalesOrderLine(SCMC__Sales_Order__c so, SCMC__Item__c item, String status, boolean doInsert){
        SCMC__Sales_Order_Line_Item__c line = new SCMC__Sales_Order_Line_Item__c();

        line.RecordTypeId = SCMFF_Utilities.getRecordType('Item', line);
        line.SCMC__Sales_Order__c = so.Id;
        line.SCMC__Quantity__c = 1;
        line.SCMC__Price__c = 25.00;
        line.SCMC__Item_Master__c = item.Id;
        line.SCMC__Customer_Commitment_Date__c = Date.today();
        line.SCMC__Status__c = status;
        line.SCMC__Condition_Code__c = cCode.Id;

        if (doInsert){
           insert line;
        }
        return line;
    }

	public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Item__c item, boolean doInsert) {
		SCMC__Warehouse__c warehouse = setupWarehouse();
		SCMC__Inventory_Location__c invLoc = createTestLocation(warehouse);
		return setupPartsInInventory(warehouse, invLoc, item, doInsert);
	}

	public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse){
    	return createTestLocation(warehouse, true);
    }

    public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse, boolean doInsert) {
        SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c();
        location.name = 'Level1';
        location.SCMC__Level1__c = 'Level1';
        location.SCMC__Warehouse__c = warehouse.id;
        if (doInsert){
        	insert location;
        }
        return location;
    }
    public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Warehouse__c warehouse, SCMC__Inventory_Location__c invLoc, SCMC__Item__c item, boolean doInsert) {
        SCMC__Inventory_Position__c ip = new SCMC__Inventory_Position__c();
        ip.SCMC__Acquisition_Cost__c          = 55.00;
        ip.SCMC__Acquisition_Currency__c      = curr.id;
        ip.SCMC__Availability__c              = true;
        ip.SCMC__Availability_Code__c         = 'In Stock';
        ip.SCMC__Bin__c                       = invLoc.id;
        ip.SCMC__Condition_Code__c            = cCode.id;
        ip.SCMC__Current_Value__c             = 55.00;
        ip.SCMC__ICP_Acquisition_Cost__c      = 55.00;
        ip.SCMC__ICP_Currency__c              = curr.id;
        ip.SCMC__ILS_Eligible__c              = false;
        ip.SCMC__Item_Master__c               = item.Id;
        ip.SCMC__Listed_on_ILS__c             = false;
        ip.SCMC__Lot_Number__c                = null;
        ip.SCMC__Manufacturer_CAGE__c  		  = null;
        ip.SCMC__Owned_By__c                  = null;
        ip.SCMC__Quantity_Allocated__c        = 0;
        ip.SCMC__Quantity__c                  = 100;
        ip.SCMC__Quantity_in_Transit__c       = 0;
        ip.SCMC__Receipt_Line__c              = null;
        ip.SCMC__Receiving_Inspection__c      = null;
        ip.SCMC__Reserve_Price__c             = 60.00;
        ip.SCMC__Revision_Level__c            =  null;
        ip.SCMC__Sale_Price__c                = 65.00;
        ip.SCMC__Item_Serial_Number__c        = null;
        ip.SCMC__Shelf_Life_Expiration__c     = null;
        ip.SCMC__List_Type__c				  = 'Y';
        if(doInsert){
        	insert ip;
        }
        return ip;
    }



   public SCMC__Customer_Quotation__c createCustomerQuotation(boolean needAccount, Boolean needContact){
        SCMC__Customer_Quotation__c quote = new SCMC__Customer_Quotation__c();
        ID salesRep = createSalesRep();
        if(needContact){
	        Account account = createTestCustomerAccount(true, true, false, null);
        	Contact con = createTestCustomerContact(true, account);
	        quote.SCMC__Customer_Account__c = account.Id;
	        quote.SCMC__Customer_Account_Contact__c = con.Id;
   		} else if(needAccount){
	        Account account = createTestCustomerAccount(true, true, false, null);
	        quote.SCMC__Customer_Account__c = account.Id;
        } else {
   			quote.SCMC__Prospect_Name__c = 'test name';
    		quote.SCMC__Mailing_Street__c = '12346 west ';
    		quote.SCMC__Prospect_Company__c = 'Test';
    		quote.SCMC__Mailing_Street_Additional_Information__c = 'line 2';
    		quote.SCMC__Mailing_City__c = 'Austin';
    		quote.SCMC__Mailing_State_Province__c = 'TX';
    		quote.SCMC__Mailing_Zip_Postal_Code__c = '78754';
    		quote.SCMC__Mailing_Country__c = 'USA';
        }
        quote.SCMC__Sales_Rep__c = salesRep;
        quote.SCMC__Status__c = 'Open';
        quote.SCMC__Customer_Account_Contact__c = null;
		quote.SCMC__Delivery_Options__c = 'Fax';
        insert quote;

        return quote;
    }

	public Opportunity createTestOpportunity (Boolean doInsert) {

		Account acct = createTestCustomerAccount(true, true, false, null);
		Contact cacct = createTestCustomerContact(true, acct);

		return createTestOpportunity(acct, cacct, doInsert);

	}

	public Opportunity createTestOpportunity (Account acct, Boolean doInsert) {
		Contact cacct = createTestCustomerContact(true, acct);

		return createTestOpportunity(acct, cacct, doInsert);

	}

	public Opportunity createTestOpportunity (Account acct, Contact cacct, Boolean doInsert) {

		Opportunity opp = new Opportunity(
      		Name = 'TestOpp1'
      		, StageName = 'New'
      		, CloseDate = system.today()
      		, AccountId = acct.id);

      	if (doInsert){
      		insert opp;
      	}
		return opp;
	}
    public SCMC__Service_Term__c createTestServiceTerms(){
    	SCMC__Service_Term__c st = new SCMC__Service_Term__c(
    			SCMC__Number_Of_Months__c = 12
    			,SCMC__Active__c = True);
    	insert st;
    	return st;
}
    public SCMC__Service_Order__c createTestServiceOrder(Account acct, SCMC__Service_Term__c term){
    	return createTestServiceOrder(acct, term, true);
     }
    public SCMC__Service_Order__c createTestServiceOrder(Account acct, SCMC__Service_Term__c term, boolean doInsert){
    	SCMC__Service_Order__c contract = new SCMC__Service_Order__c(SCMC__Status__c = 'Approved'
    		,SCMC__Service_Term_Record__c = term.id
    		,SCMC__Next_Invoice_Date__c = System.Today()
    		,SCMC__Last_Invoice_Date__c = null
    		,SCMC__Customer_Account_Sold_To__c = acct.id);
    	if (doinsert){
    	insert contract;
    	}
    	return contract;
	}
	public SCMC__Service_Order_Line__c createTestServiceLine(SCMC__Service_Order__c contract
				,SCMC__Item__c item) {

		SCMC__Service_Order_Line__c line = new SCMC__Service_Order_Line__c (SCMC__Status__c = 'Approved'
				,SCMC__Quantity__c = 3
				,SCMC__RC_Base__c = 55
				,SCMC__Item_Number__c = item.id
				,SCMC__Service_Order__c = contract.id);
		insert line;
		return line;
	}


}