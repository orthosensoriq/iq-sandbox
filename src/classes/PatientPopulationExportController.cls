public class PatientPopulationExportController
{
    public PatientPopulationExportController(){}
    
    public List<string> BuildExportCSV(set<Id> eventIds, boolean singleCaseType)
    {
        list<string> returnList = new list<string>();
        string lastId = '';
        map<string,string> eventMap = new map<string,string>();
        map<string,string> eventComponents = new map<string,string>();
        map<string,string> eventSensors = new map<string,string>();
        map<string,string> eventForms = new map<string,string>(); 
        map<string,string> eventFormResponses = new map<string,string>(); 
		string emptyClinicalData = '';
        string emptyComponents = '';
        string emptySensorData = '';
        string emptyForms = '';
        string emptyResponses = '';
        try
        {
            //<-------- Get Event Data --------->
            list<string> eventFields = getFields(new event__c());
            string eventFieldList = string.join(eventFields, ',');
            string eventQuery = 'Select ' + eventFieldList + ' from event__c where Id in :eventIds';
            system.debug('Event Query ===> ' + eventQuery);
            map<Id, Event__c> events = new map<Id, Event__c>();
			events.putAll((list<event__c>)database.query(eventQuery));

            eventFieldList = 'evt_' + string.join(eventFields, ',evt_');
			system.debug(eventFieldList);
            string eventHeader = eventFieldList;
            System.debug('Events: ' + events.size());
            
            //<-------- Get Case Data --------->
            list<string> caseFields = getFields(new case__c());
            string caseFieldList = string.join(caseFields, ',');
            string caseQuery = 'Select ' + caseFieldList + ' From case__c where Id in (Select case__c from event__c where Id in :eventIds)';
            system.debug('Case Query ===> ' + caseQuery);
            map<Id,Case__c> cases = new map<Id,Case__c>();
			cases.putAll((list<case__c>)database.query(caseQuery));

            caseFieldList = 'c_' + string.join(caseFields, ',c_');
			system.debug(caseFieldList);
            string caseHeader = caseFieldList;
            System.debug('Cases: ' + cases.size());
    
            //<-------- Get Patient Data --------->
            list<string> patientFields = getFields(new patient__c());
            string patientFieldList = string.join(patientFields, ',');
            set<Id> caseIds = cases.keySet();
			string patientQuery = 'Select ' + patientFieldList + ' From patient__c where Id in (Select Patient__c from case__c where id in :caseIds)';
            system.debug('Patient Query ===> ' + patientQuery);
            map<Id,Patient__c> patients = new map<Id,Patient__c>();
			patients.putAll((list<Patient__c>)database.query(patientQuery));

            patientFieldList = 'pat_' + string.join(patientFields, ',pat_');
			system.debug(patientFieldList);
            string patientHeader = patientFieldList;
            System.debug('Patients: ' + patients.size());
    
           //<-------- Get Clinical Data --------->
            list<string> clinicalDataFields = getFields(new clinical_data__c());
            for(integer i=0;i<clinicalDataFields.size();i++) emptyClinicalData += ',';
            string clinicalFieldList = string.join(clinicalDataFields, ',');
            string clinicalDataQuery = 'Select ' + clinicalFieldList + ' from Clinical_Data__c where event__c in :eventIds';
            system.debug('ClinicalData Query ===> ' + clinicalDataQuery);
            map<Id,Clinical_Data__c> clinicals = new map<Id,Clinical_Data__c>();
			clinicals.putAll((list<Clinical_Data__c>)database.query(clinicalDataQuery));

            clinicalFieldList = 'cd_' + string.join(clinicalDataFields, ',cd_');
			system.debug(clinicalFieldList);
            string clinicalHeader = clinicalFieldList;
            System.debug('Clinicals: ' + clinicals.size());
    
            for(event__c e : events.values())
            {
                //string eventData = e.Id + ',' + e.Name; //<-- NEEDS TO CHANGE TO REAL FIELDS
                string eventData = '';
                for(string field : eventFields)
                    eventData += (string.valueOf(e.get(field)) == null ? '' : string.valueOf(e.get(field))).replace(',','') + ',';
                eventData = eventData.substringBeforeLast(',');
                    
                case__c c = cases.get(e.case__c);
                string caseData = '';
                for(string field : caseFields)
                    caseData += ',' + (string.valueOf(c.get(field)) == null ? '' : string.valueOf(c.get(field))).replace(',','');
                    
                patient__c p = patients.get(c.Patient__c);
                string patientData = '';
                for(string field : patientFields)
                    patientData += ',' + (string.valueOf(p.get(field)) == null ? '' : string.valueOf(p.get(field))).replace(',','');
                
                string clinicalData = '';
                for(clinical_data__c cd : clinicals.values())
                {
                    if(cd.event__c == string.valueOf(e.Id))
                        for(string field : clinicalDataFields)
                    		clinicalData += ',' + (string.valueOf(cd.get(field)) == null ? '' : string.valueOf(cd.get(field))).replace(',','');
                }
                if(clinicalData == null || clinicalData == '')
                    clinicalData = emptyClinicalData;
                
                eventMap.put(string.valueOf(e.Id),eventData + patientData + caseData + clinicalData);
            }
            
            //Get Component Data
            integer compcount = 0;
            integer compHighestCount = 0;
            lastId = '';
            map<string,integer> compCounts = new map<string,integer>();
            for(AggregateResult evtcomp : [select count(id) compCount, event__c from Implant_Component__c where event__c in: eventIds group by event__c order by count(id) desc])
            {
                compCounts.put(string.valueOf(evtcomp.get('event__c')),integer.valueOf(evtcomp.get('compCount')));
                if(compHighestCount < integer.valueOf(evtcomp.get('compCount')))
                    compHighestCount = integer.valueOf(evtcomp.get('compCount'));
            }
            list<string> componentFields = getFields(new Implant_Component__c());
			for(integer i=0;i<componentFields.size();i++) emptyComponents += ',';
            string componentFieldList = string.join(componentFields, ',');
            string componentQuery = 'Select ' + componentFieldList + ' from Implant_Component__c where event__c in: eventIds order by event__c';
            system.debug('Component Query ===> ' + componentQuery);
            map<Id,Implant_Component__c> components = new map<Id,Implant_Component__c>();
			components.putAll((list<Implant_Component__c>)database.query(componentQuery));
            
            //map<Id,Implant_Component__c> components = new map<Id,Implant_Component__c>([Select Id, Name, event__c from Implant_Component__c where event__c in: eventIds order by event__c]);
            System.debug('Components: ' + components.size());
            for(Implant_Component__c ic : components.values())
            {
                //string valueString = ',' + ic.Id + ',' + ic.Name; //<-- NEEDS TO CHANGE TO REAL FIELDS
                string valueString = '';
				for(string field : componentFields)
                    valueString += ',' + (string.valueOf(ic.get(field)) == null ? '' : string.valueOf(ic.get(field))).replace(',','');                
                
                eventComponents.put(ic.event__c,(eventComponents.get(ic.event__c) == null ? '' : eventComponents.get(ic.event__c)) + valueString);
                //system.debug('Event: ' + ic.event__c + ', lastId: ' + lastId);
            }

            componentFieldList = 'ic_' + string.join(componentFields, ',ic_');
			system.debug(componentFieldList);
            string componentHeader = buildHeaderForObject(componentFieldList, compHighestCount);
            //Add Children need to be handled
            for(Id evt : eventIds)
            {
                //Make sure all events components are handled
                if(string.isBlank(string.valueOf(compCounts.get(string.valueOf(evt)))))
                    compCounts.put(string.valueOf(evt),0);
    
                if(compCounts.get(string.valueOf(evt)) < compHighestCount)
                {
                    string emptyValues = '';
                    for(integer i = compCounts.get(string.valueOf(evt)) + 1; i <= compHighestCount; i++ )
                    {
                        emptyValues += emptyComponents;	//NEEDS TO BE UPDATED BASED ON THE TOTAL NUMBER OF FIELDS PER RECORD
                    }
                    eventComponents.put(string.valueOf(evt),(eventComponents.get(string.valueOf(evt)) == null ? '' : eventComponents.get(string.valueOf(evt))) + emptyValues);
                }
            }
            
            //Get Sensor Data
            integer sensorHighestCount = 0;
            lastId = '';
            map<string,integer> sensorCounts = new map<string,integer>();
            for(AggregateResult sensorcomp : [select count(id) sensorCount, event__c from sensor_data__c where event__c in: eventIds group by event__c order by count(id) desc])
            {
                sensorCounts.put(string.valueOf(sensorcomp.get('event__c')),integer.valueOf(sensorcomp.get('sensorCount')));
                if(sensorHighestCount < integer.valueOf(sensorcomp.get('sensorCount')))
                    sensorHighestCount = integer.valueOf(sensorcomp.get('sensorCount'));
            }
            list<string> sensorDataFields = getFields(new sensor_data__c());
			for(integer i=0;i<sensorDataFields.size();i++) emptySensorData += ',';
            string sensorDataFieldList = string.join(sensorDataFields, ',');
            string sensorDataQuery = 'Select ' + sensorDataFieldList + ' from sensor_data__c where event__c in: eventIds order by event__c';
            system.debug('Sensor Data Query ===> ' + sensorDataQuery);
            map<Id,sensor_data__c> sensors = new map<Id,sensor_data__c>();
			sensors.putAll((list<sensor_data__c>)database.query(sensorDataQuery));

            //map<Id,sensor_data__c> sensors = new map<Id,sensor_data__c>([Select Id, Name, event__c from sensor_data__c where event__c in: eventIds order by event__c]);
            System.debug('Sensors: ' + sensors.size());
            for(sensor_data__c sd : sensors.values())
            {
                //string valueString = ',' + sd.Id + ',' + sd.Name; //<-- NEEDS TO CHANGE TO REAL FIELDS
                string valueString = '';
				for(string field : sensorDataFields)
                    valueString += ',' + (string.valueOf(sd.get(field)) == null ? '' : string.valueOf(sd.get(field))).replace(',','');
                
                eventSensors.put(sd.event__c,(eventSensors.get(sd.event__c) == null ? '' : eventSensors.get(sd.event__c)) + valueString);
                //system.debug('Event: ' + sd.event__c + ', lastId: ' + lastId);
            }

            sensorDataFieldList = 'sd_' + string.join(sensorDataFields, ',sd_');
			system.debug(sensorDataFieldList);
            string sensorHeader = buildHeaderForObject(sensorDataFieldList, sensorHighestCount);
            //Add Children need to be handled
            for(Id evt : eventIds)
            {
                //Make sure all events sensordatas are handled
                if(string.isBlank(string.valueOf(sensorCounts.get(string.valueOf(evt)))))
                    sensorCounts.put(string.valueOf(evt),0);
    
                if(sensorCounts.get(string.valueOf(evt)) < sensorHighestCount)
                {
                    string emptyValues = '';
                    for(integer i = sensorCounts.get(string.valueOf(evt)) + 1; i <= sensorHighestCount; i++ )
                    {
                        emptyValues += emptySensorData;	//NEEDS TO BE UPDATED BASED ON THE TOTAL NUMBER OF FIELDS PER RECORD
                    }
                    eventSensors.put(string.valueOf(evt),(eventSensors.get(string.valueOf(evt)) == null ? '' : eventSensors.get(string.valueOf(evt))) + emptyValues);
                }
            }
            
            //EVENT FORMS ONLY GET DONE FOR A SINGLE CASE TYPE!
			if(singleCaseType)
            {
                list<id> tempList = new list<id>();
                tempList.addAll(eventIds);
                string tempString = string.join(tempList, '\',\'');
                system.debug('EventIds: \'' + tempString + '\'');
                map<string,list<responseWrapper>> responseMap = new map<string,list<responseWrapper>>();
                map<string,string> surveyMap = new map<string,string>();
                map<string,string> surveyQHeader = new map<string,string>();
                map<string,integer> surveyQCount = new map<string,integer>();
                map<string,set<string>> eventSurveys = new map<string,set<string>>();

                //Get Event Forms
                string formQuery = 'Select id, survey__c, event__c, survey__r.Name__c from Event_Form__c where event__c in: eventIds order by event__c, survey__c';
                system.debug('Form Query ===> ' + formQuery);
                map<Id,Event_Form__c> forms = new map<Id,Event_Form__c>();
                forms.putAll((list<Event_Form__c>)database.query(formQuery));
				system.debug('forms.size(): ' + forms.size());
                
                list<survey_response__c> responseList = [Select Id, Event_Form__c, Survey__c, Survey__r.Version__c, Survey_Question__c, Survey_Response__c, Survey_Version__c, Survey__r.name__c from survey_response__c where event_form__c in :forms.keyset() order by event_form__c, survey__c, Survey_Question__c];
                system.debug('responseList.size(): ' + responseList.size());
                if(responseList.size() > 0)
                {
                    for(survey_response__c sr : responseList)
                    {
                        string columnHead = sr.Survey__r.Name__c.Replace(' ','') + '_v' + string.valueOf(sr.Survey__r.Version__c) + '_' + sr.Survey_Question__c.replace(' ','');
                        list<responseWrapper> curWraps = (responseMap.get(sr.Event_form__c) == null ? new list<responseWrapper>() : responseMap.get(sr.Event_Form__c));
                        responseWrapper tempWrapper = new responseWrapper(sr.id, sr.Survey__c, sr.Event_Form__c, columnHead, sr.Survey_Response__c);
                        system.debug('Adding to responseWrapper: ' + sr.id + ', ' + sr.Survey__c + ', ' + sr.Event_Form__c + ', ' + columnHead + ', ' + sr.Survey_Response__c);
                        curWraps.add(tempWrapper);
                        responseMap.put(sr.Event_Form__c,curWraps);
                        
                    }
                    
                    for(event_form__c ef : forms.values())
                    {
                        if(eventSurveys.get(ef.event__c) == null)
                        {
							eventSurveys.put(ef.event__c, new set<string>());
                        }
                            
                        set<string> tempSet = eventSurveys.get(ef.event__c);
                        tempSet.add(ef.Survey__c);
                        eventSurveys.put(ef.event__c, tempSet);
                        
                        system.debug('ef id: ' + ef.id + ', survey id' + ef.Survey__c + ', survey name ' + ef.Survey__r.Name__c);
                        surveyMap.put(ef.Survey__c, ef.Survey__r.Name__c);
                        if(surveyQHeader.get(ef.Survey__c) == null)
                        {
                            string surveyHeaderVal = '';
                            integer count = 0;
                            if(responseMap.get(ef.id) != null)
                            {
                                for(responsewrapper rw : responseMap.get(ef.id))
                                {
                                    count += 1;
                                    surveyHeaderVal += rw.ColumnHeader + ',' ;
                                }
                                surveyQCount.put(ef.survey__c, count);
                                surveyQHeader.put(ef.Survey__c, surveyHeaderVal.substringBeforeLast(','));
                            }
                        }
                    }
                    string formHeader = '';
                    for(string sid : surveyMap.keySet())
                    {
                        formHeader += surveyMap.get(sid).replace(' ','') + '_id,' + surveyQHeader.get(sid) + ',';
                        //Populate eventForms now
                        for(event_form__c ef : forms.values())
                        {
                            if(eventForms.get(ef.Event__c) == null)
                                eventForms.put(ef.Event__c, ',');

                            if(eventSurveys.get(ef.event__c).contains(sid)) 
                            {
                                if(ef.Survey__c == sid)
                                {
                                    if(responseMap.get(ef.id) != null)
                                    {
                                        string q = ef.id + ',';
                                        for(responsewrapper rw : responseMap.get(ef.id))
                                        {
                                            q += (rw.ResponseValues == null ? '' : rw.ResponseValues.replace(',',' ')) + ',';
                                        }
                                        eventForms.put(ef.event__c, eventForms.get(ef.event__c) + q);
                                    }
                                    else
                                    {
                                        eventForms.put(ef.event__c, eventForms.get(ef.event__c) + buildEmptySurveyVal(',', surveyQCount.get(ef.Survey__c)));
                                    }
                                }
                            }
                            else
                            {
                                eventForms.put(ef.event__c, eventForms.get(ef.event__c) + buildEmptySurveyVal(',', surveyQCount.get(ef.Survey__c)));
                            }
                        }
                        
                    }
                    formHeader = ',' + formHeader.substringBeforeLast(',');
                    
                    //Build Header (per sObject, loop and get counts by eventId)
                    string headerRow = eventHeader + ',' + patientHeader + ','  + caseHeader + ','  + clinicalHeader + componentHeader + sensorHeader + formHeader;
                    returnList.add(headerRow);
                    system.debug(formHeader);
                    //Build Detail lines
                    for(string evtId : eventMap.keySet())
                    {
                        returnList.Add(eventMap.get(evtId) + eventComponents.get(evtId) + eventSensors.get(evtId) + eventForms.get(evtId));
                    }
                }
                else
                {
                    //Build Header (per sObject, loop and get counts by eventId)
                    string headerRow = eventHeader + ',' + patientHeader + ','  + caseHeader + ','  + clinicalHeader + componentHeader + sensorHeader;
                    returnList.add(headerRow);
                    
                    //Build Detail lines
                    for(string evtId : eventMap.keySet())
                    {
                        returnList.Add(eventMap.get(evtId) + eventComponents.get(evtId) + eventSensors.get(evtId));
                    }
                }
            }
            else
            {
                //Build Header (per sObject, loop and get counts by eventId)
                string headerRow = eventHeader + ',' + patientHeader + ','  + caseHeader + ','  + clinicalHeader + componentHeader + sensorHeader;
                returnList.add(headerRow);
                
                //Build Detail lines
                for(string evtId : eventMap.keySet())
                {
                    returnList.Add(eventMap.get(evtId) + eventComponents.get(evtId) + eventSensors.get(evtId));
                }
            }
            return returnList;
        }
        catch(exception ex)
        {
            system.debug('ERROR!!' + ex.getMessage());
            return null;
        }
    }
    
    public class eventFormSurveyWrapper
    {
        public string EventId {get;set;}
        public string SurveyId {get;set;}
        public string EventFormId {get;set;}
        
        public eventFormSurveyWrapper(string eid, string sid, string efid)
        {
            EventId = eid;
            SurveyId = sid;
            EventFormId = efid;
        }
    }
    
    public class responseWrapper
    {
        public String SurveyResponseId {get;set;}
        public String SurveyId {get;set;}
        public String EventFormId {get;set;}
        public string ColumnHeader {get;set;}
        public string ResponseValues {get;set;}
        public responseWrapper(string srid, string sid, string efid, string ch, string rv)
        {
            SurveyResponseId = srid;
            SurveyId = sid;
            EventFormId = efid;
            ColumnHeader = ch;
            ResponseValues = rv;
        }
    }
    
    private string buildEmptySurveyVal(string separator, integer totalNumber)
    {
        string retString = '';
        for(integer i = 0; i < totalNumber; i++ )
        {
            retString += separator;
        }
        return retString;
    }
    
    private string buildHeaderForObject(string fieldNames, integer totalNumber)
    {
        list<string> temp = fieldNames.split(',');
        string returnStr = '';
        for(integer i = 1; i <= totalNumber; i++)
        {
            for(string s : temp)
            {
				returnStr += (',' + s + '_' + i);
            }
        }
        return returnStr;
    }

    private string buildHeaderForChildObject(string fieldNames, integer totalNumber, string childFieldNames, integer childTotalNumber)
    {
        list<string> parent = fieldNames.split(',');
        list<string> child = childFieldNames.split(',');
        string returnStr = '';
        for(integer i = 1; i <= totalNumber; i++)
        {
            for(string p : parent)
            {
				returnStr += (',' + p + '_' + i);
            }
            for(integer x = 1; x <= childTotalNumber; x++)
            {
                for(string c : child)
                {
                    returnStr += (',' + c + '_' + i + '_' + x);
                    
                }
            }
        }
        return returnStr;
    }
    
	private List<String> getFields(sObject sobj) 
    {
	    Set<String> fieldSet = new Set<String>();
    	for (String field : sobj.getSobjectType().getDescribe().fields.getMap().keySet()) 
        {
        	try 
            {
            	//a.get(field);
            	fieldSet.add(field);
        	}
            catch (Exception e) 
            {}
    	}
        List<string> returnList = new list<string>();
        returnList.addAll(fieldset);
    	return returnList;
	}
}