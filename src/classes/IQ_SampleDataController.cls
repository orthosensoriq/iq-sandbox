// a collection of routines to create sample data. Some are not working and were created 
// in an adhoc, as needed approach.
public with sharing class IQ_SampleDataController {
    public string hospitalId {get; set;}
    public string practiceId {get; set;}
    public string surgeonId {get; set;}
    public string patientId {get; set;}
    public string surveyId {get; set;} //this is actually the name of the survey!
    public string lastName {get; set;}
    public string firstName {get; set;}
    public string patientCaseCount {get; set;}
    public string caseName {get; set;}
    public string caseTypeName {get; set;}
    public string procedureDate {get; set;}
    public string laterality {get; set;}
    public string eventName {get; set;}
    public string question1 {get; set;}
    public string response1 {get; set;}
    public string question2 {get; set;}
    public string response2 {get; set;}
    public string question3 {get; set;}
    public string response3 {get; set;}
    public string question4 {get; set;}
    public string response4 {get; set;}
    public string question5 {get; set;}
    public string response5 {get; set;}
    public List<string> questions {get; set;}
    transient public Integer csvRecords {get; set;}
    public String eventTypeName {get; set;}
    public List<Survey__c> surveyList {get; set;}
    public List<Survey_Response__c> responses {get; set;}
    //public String patientId {get; set;}
    transient public Blob csvFileBody{get;set;}
    transient public string csvAsString{get;set;}
    transient public String[] csvFileLines{get;set;}
    public List<OS_Device__c> osDevicelist{get;set;}
    public List<OS_Device_Ref__c> osDeviceReflist{get;set;}
    public List<Product_Catalog__c> productCataloglist{get;set;}
    public List<Manufacturer_Hibc_Code__c> manufacturerHibcCodeList{get; set;}
    public List<Manufacturer__c> manufacturerList {get; set;}
    public List<Attachment> attachmentList {get; set;}
    public List<Sensor_Data__c> sensorDataList {get; set;}
    public User currentUser {get; set;} 
    public List<IQ_Phase_Count__c> phaseCounts {get; set;}
    public Case__c currentCase {get; set;}
    public Survey__c currentSurvey {get; set;}

    public IQ_SampleDataController() {
        csvFileLines = new String[]{};
        csvRecords = 0;
        osDevicelist = new List<OS_Device__c>(); 
        osDeviceReflist = new List<OS_Device_Ref__c>(); 
        productCatalogList = new List<Product_Catalog__c>();
        manufacturerHibcCodeList = new List<Manufacturer_HIBC_Code__c>();
        attachmentList = new List<Attachment>();
        sensorDataList = new List<Sensor_Data__c>();

        surveyList = IQ_PatientActions.LoadSurveys();
        surveyId= 'PROM101';
        questions = IQ_SurveyRepository.GetScoreNames(surveyList[0].Name__c);
        SetQuestions();
        currentUser = getCurrentUser(); 
        GetManufacturerList();
    }
    
    public List<SelectOption> getHospitals() {
        List<Account> hospitals = IQ_PatientActions.LoadHospitals();
         List<SelectOption> options = new List<SelectOption>();
        for(Account hospital: hospitals) {
            options.add(new SelectOption(hospital.Id, hospital.Name));
        }
        return options;
    }   

    public List<SelectOption> getPractices() {
        List<Account> practices = IQ_PatientActions.LoadPractices();
         List<SelectOption> options = new List<SelectOption>();
        for(Account practice: practices) {
            options.add(new SelectOption(practice.Id, practice.Name));
        }
        return options;
    }   

    public void CreatePatient() {
        IQ_SurveyRepository.CreatePatient(hospitalId, lastName, firstName);
    }

    public void CreateCase() {
        Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyId); 
        Patient__c patient = IQ_SurveyRepository.CreatePatient(hospitalId, lastName, firstName);
        Event_Type__c eventType = IQ_SurveyRepository.GetEventType(eventTypeName);

        IQ_SurveyRepository.CreateCase(caseName, 'Total Knee Arthroplasty', patient, survey, eventType,
            procedureDate, null, laterality);
    }

//   public void CreateSurveyResponses() {
//     System.debug('Creating responses');
//     Event_Type__c et = IQ_SurveyRepository.GetEventType(eventTypeName);
//     System.debug(et);
//     Survey__c survey = IQ_SurveyRepository.GetSurveyByName(surveyId);
//     System.debug(survey);
//     Patient__c patient = IQ_SurveyRepository.GetPatientById(patientId);
//     System.debug(patient);
//     //get case for patient
//     List<Case__c> cases = IQ_PatientActions.LoadCasesAndEvents(patient.Id);
//         System.debug(cases.size());
//         //get events for case
//         Event__c  event;
//         if (cases.size() > 0) {
//             List<Event__c> events = IQ_PatientActions.LoadCaseEvents(cases[0].Id);
//             if (events.size() > 0) {
//                 for(Event__c event1: events) {
//                    if (event1.Event_Type_Name__c == eventTypeName) {
//                        event = event1;
//                    }     
//                 }
//                 } else {
//                     event = IQ_SurveyRepository.CreateEvent('Test Event', et, caseTypeName, survey, patient, procedureDate,
//                             null, laterality);
//                 }
//         } else {
//             event = IQ_SurveyRepository.CreateEvent('Test Event', et, caseTypeName, survey, patient, procedureDate,
//                 surgeonId, laterality);
//         }
//         System.debug(event);
//         Event_Form__c eventForm = IQ_SurveyRepository.GetEventForm(survey, event);
//         if (eventForm.Id == null) {
//             eventForm = IQ_SurveyRepository.CreateEventForm(Survey.Name__c +': ' + eventTypeName, event, survey);
//         }
//         System.debug(eventForm);
//         System.debug(question1);
//         System.debug(response1);
//         if (eventForm.id != null) {
//             if (question1 != null && response1 != null) {
//                 IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question1, response1);
//             }
//             if (question2 != null && response2 != null) {
//                 IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question2, response2);
//             }
//             if (question3 != null && response3 != null) {
//                 IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question3, response3);
//             }
//             if (question4 != null && response4 != null) {
//                 IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question4, response4);
//             }
//             if (question5 != null && response5 != null) {
//                 IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question5, response5);
//             }
//             Event_Form__c updateEvent = new Event_Form__c();
//             updateEvent.Id = eventForm.Id;
//             updateEvent.End_Date__c = Date.today();
//             updateEvent.Form_Complete__c = true;
//             update updateEvent;
//         }
//     }
    
    private void SetQuestions() {
       question1 = questions[0];
       question2 = questions[1];
       question3 = questions[2];
       question4 = questions[3];
       question5 = questions[4];
    }
    
    public List<SelectOption> getItems() {
      List<SelectOption> options = new List<SelectOption>();
        Survey__c survey = IQ_SurveyRepository.getSurveyByName(surveyId);
        List<Event_Type_Survey__c> periods = IQ_SurveyRepository.GetSurveyEventTypes(survey.Id);
        //System.debug(periods);
        for(Event_Type_Survey__c period: periods ) {
            options.add(new SelectOption(period.Event_Type__r.Name, period.Event_Type__r.Name));
        } 
      
      return options;
    } 

  public List<SelectOption> getPatients() {
    List<Patient__c> patients = IQ_SurveyRepository.GetPatients();
    List<SelectOption> options = new List<SelectOption>();
      for(patient__c patient: patients) {
        options.add(new SelectOption(patient.Id, patient.Last_Name__c + ', ' + patient.First_Name__c));
      }
      return options;
  }

    public  List<SelectOption> getSurgeons() {
            List<Surgeon_Hospital__c> surgeons = IQ_PatientActions.LoadSurgeons(practiceId);
        List<SelectOption> options = new List<SelectOption>();
        for(Surgeon_Hospital__c surgeon: surgeons) {
            options.add(new SelectOption(surgeon.surgeon__r.Id, surgeon.surgeon__r.Name));
        }
        return options;

    }

    public  List<SelectOption> getSurveys() {
        
        List<SelectOption> options = new List<SelectOption>();
        for(Survey__c survey: surveyList) {
            options.add(new SelectOption(survey.Name__c, survey.Name__c));
        }
        
        return options;

    }

    public PageReference updateSurveyQuestions() {
        questions = IQ_SurveyRepository.GetScoreNames(surveyId);
        System.debug(questions);
        SetQuestions();
        return null;
    }

    public PageReference updatePatientInfo() {

        Patient__c patient = IQ_SurveyRepository.GetPatientById(patientId);
        //get case for patient
        List<Case__c> cases = IQ_PatientActions.LoadCasesAndEvents(patient.Id);
        if (cases.size() > 0) {
            caseName = cases[0].Name__c;
            caseTypeName = cases[0].Case_Type__r.Description__c;
            laterality = cases[0].Laterality__c;

        }
        for (Case__c patientCase: cases) {
            getSurveyResponses(patientCase.Id);
        }
        return null;
    }

    public List<SelectOption> getLateralities() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Left', 'Left'));
        options.add(new SelectOption('Right', 'Right'));
        return options;
    }

    public void getSurveyResponses(String caseId) {
        responses = new List<Survey_Response__c>();
        for(Survey__c survey: surveyList) {
            List<Survey_Response__c> responses2 = IQ_PatientActions.LoadSurveyScores(caseId, survey.Id);
            for (Survey_Response__c response: responses2) {
                responses.add(response);
            }
        }
        //return responses;   
    }

    public void importOSDeviceRefCSVFile(){
       try{
           DeleteOS_Device_Refs();
           User currentUser = getCurrentUser(); 
           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 
            
           for(Integer i=1;i<csvRecords;i++){
                string[] csvRecordData = csvFileLines[i].split(',');
                OS_Device_Ref__c prod = createOSDeviceRefRecordFromArray(csvRecordData);
                
              osDeviceRefList.add(prod);   
           }
           insert osDeviceReflist;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importin data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void importProductCatalogCSVFile(){
       try{
           System.debug('entered routine');
           //DeleteProduct_Catalog();
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            System.debug(csvFileLines.size());
            csvRecords = 1700;//csvFileLines.size();
            
            for(Integer i=1;i<csvRecords;i++){
               string[] csvRecordData = csvFileLines[i].split(',');
               System.debug(csvRecordData);
               Product_Catalog__c prod = createProductCatalogRecordFromArray(csvRecordData);
               productCatalogList.add(prod);  
               System.debug(productCatalogList.size());
           }
        insert productCatalogList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void importManufacturerHibcCodeCSVFile(){
       try{
           System.debug('entered routine');
           //DeleteProduct_Catalog();
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            System.debug(csvFileLines.size());
            csvRecords = csvFileLines.size();
            
            for(Integer i=1;i<csvRecords;i++){
               string[] csvRecordData = csvFileLines[i].split(',');
               System.debug(csvRecordData);
               Manufacturer_Hibc_Code__c prod = createManufacturerHibcCodeRecordFromArray(csvRecordData);
               manufacturerHibcCodeList.add(prod);  
               System.debug(manufacturerHibcCodeList.size());
           }
        insert manufacturerHibcCodeList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void importOSDeviceCSVFile(){
       try{
            DeleteOS_Devices();
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            csvRecords = csvFileLines.size();
            //csvRecords = 10;
            for(Integer i=1;i<csvRecords;i++){
                string[] csvRecordData = csvFileLines[i].split(',');
                OS_Device__c prod = createOSDeviceRecordFromArray(csvRecordData);
                osDeviceList.add(prod);  
                //insert prod; 
            }
            insert osDeviceList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    
    public void importAttachmentsCSVFile(){
       try{
            //DeleteOS_Devices();
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            csvRecords = csvFileLines.size();
            //csvRecords = 10;
            for(Integer i=1;i<csvRecords;i++){
                string[] csvRecordData = csvFileLines[i].split(',');
                Attachment result = createAttachmentRecordFromArray(csvRecordData);
                System.debug(result);
                //attachmentList.add(result);  
                insert result;
                //System.debug(attachmentList.size());
                //insert prod; 
            }
            //insert attachmentList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void importSensorDataCSVFile(){
       try{
           System.debug('entered routine');
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            csvRecords = csvFileLines.size();
            
            for(Integer i=1;i<45;i++){
                string[] csvRecordData = csvFileLines[i].split(',');
                Sensor_Data__c result = createSensorDataRecordFromArray(csvRecordData);
                System.debug(result);
                sensorDataList.add(result);  
                //insert result;
           }
        insert sensorDataList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void importSurveyResponseCSVFile(){
       try{
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            csvRecords = csvFileLines.size();
            
            for(Integer i=1;i<45;i++){
                string[] csvRecordData = csvFileLines[i].split(',');
                //Sensor_Data__c result = createSensorDataRecordFromArray(csvRecordData);
                //System.debug(result);
                //sensorDataList.add(result);  
                //insert result;
           }
        //insert sensorDataList;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            System.debug(errorMessage );
            System.debug(e);
        }  
    }

    public void DeleteOS_Devices() {
        List<OS_Device__c> objs = new List<OS_Device__c>();
        objs = [Select ID From OS_Device__c];
        delete objs;
    }

    public void DeleteOS_Device_Refs() {
        List<OS_Device_Ref__c> objs = new List<OS_Device_Ref__c>();
        objs = [Select ID From OS_Device_Ref__c];
        delete objs;
    }

    public void DeleteProduct_Catalog() {
        List<Product_Catalog__c> objs = new List<Product_Catalog__c>();
        objs = [Select ID From Product_Catalog__c];
        delete objs;
    }
    
    public OS_Device__c createOSDeviceRecordFromArray(string[] csvRecordData) {
          OS_Device__c prod = new OS_Device__c() ;
               
          // prod.CreatedBy = currentUser;
          // prod.LastModifiedBy = currentUser;
          //osDevRef.SystemModStamp
          //prod.Date_of_Manufacture__c = Date();
          System.debug('deviceID:' + csvRecordData[1]);
          prod.Device_ID__c = csvRecordData[1];
          //prod.Expiration_Date__c = Date()csvRecordData[11];
          prod.isDeleted__c = false;
          System.debug('Lot_Number: ' + csvRecordData[9]);
          prod.Lot_Number__c = csvRecordData[9];
          System.debug('Manufacturer: ' + csvRecordData[10]);
          Manufacturer__c man1 = GetManufacturer(csvRecordData[10]);
          //prod.Manufacturer__c = man1.Id;//csvRecordData[10];
          System.debug(GetManufacturer(csvRecordData[10]));
          Manufacturer__c manufacturer = GetManufacturer(csvRecordData[10]);
          if (manufacturer.Name != '') {
            System.debug('Found manufacturer!');
  
            //prod.Manufacturer__c = man1.Id;//csvRecordData[10];
            prod.OSManufacturer__c = man1.Id;//csvRecordData[12];
          }
          prod.Manufacturer_Name__c = csvRecordData[10];
          prod.Size__c = csvRecordData[14];
          prod.Source_Record_ID__c = csvRecordData[15];
          prod.Style__c = csvRecordData[16];
          prod.Year_Assembled__c = csvRecordData[18];
          return prod;      
    }

    public OS_Device_Ref__c createOSDeviceRefRecordFromArray(string[] csvRecordData) {
        OS_Device_Ref__c osDevRef = new OS_Device_Ref__c();
        osDevRef.name = csvRecordData[7] ; 
        // os.CreatedDate__c = Date(); //4
        // osDevRef.CreatedBy = currentUser;
        // osDevRef.LastModifiedBy = currentUser;
        // osDevRef.SystemModStamp
        osDevRef.Manufacturer__c = csvRecordData[6];
        return osDevRef;
    }

    public Product_Catalog__c createProductCatalogRecordFromArray(string[] csvRecordData) {
        Product_Catalog__c prod = new Product_Catalog__c() ;
        //osDevRef.IsDeleted = false; //2
        System.debug('Name: ' + csvRecordData[4]);
        prod.name = csvRecordData[4] ; 
               //os.CreatedDate__c = Date(); //4
        // prod.CreatedBy = currentUser;
        // prod.LastModifiedBy = currentUser;
               //osDevRef.SystemModStamp
        prod.Active__c = true;
        //System.debug(csvRecordData[1]);
        //prod.Brand__c = csvRecordData[1];
        System.debug('Description: ' + csvRecordData[2]);
        prod.Description__c = csvRecordData[2];
        System.debug('Implant: + ' + csvRecordData[3]);
        prod.Implant_Type__c = csvRecordData[3];
        //prod.Initiative__c = csvRecordData[8];
        string manufacturerName = getManufacturerFromProduction( csvRecordData[4]);
        if (manufacturerName != '') {
          Manufacturer__c man1 = GetManufacturer(manufacturerName);
          if (man1.Name != '') {
            //System.debug('Found manufacturer!');

            prod.Manufacturer__c = man1.Id;//csvRecordData[12];
          }
        }
        //System.debug(csvRecordData[15]);
        //prod.Material__C = csvRecordData[15];
        System.debug(csvRecordData[6]);
        prod.Product_Detail__c = csvRecordData[6];
        //prod.Product_MSRP__c = csvRecordData[17];
        System.debug(csvRecordData[8]);
        prod.Product_Name__c = csvRecordData[8];
        prod.Name = csvRecordData[5];
        System.debug(csvRecordData[5]);
        //prod.Product_MSRP__c = Decimal.valueOf(csvRecordData[19]);
        //prod.Product_Positioning__c = csvRecordData[22];
        //System.debug(csvRecordData[23]);
        //prod.Service_Line__c = csvRecordData[23];
        //        System.debug('Short description: ' + csvRecordData[24]);
        //prod.Short_Description__c = csvRecordData[24];
                System.debug(csvRecordData[9]);
        prod.Source_Record_ID__c = csvRecordData[9];
                //System.debug(csvRecordData[27]);
        //prod.Tracked_By_Lot__c = Boolean.valueOf(csvRecordData[27]);
                //System.debug('Tracked by serial#:' + csvRecordData[28]);

        //prod.Tracked_By_Serial__c = Boolean.valueOf(csvRecordData[28]);
        //        System.debug('Unit of Measure: ' + csvRecordData[29]);
        //prod.Unit_of_Measure__c = csvRecordData[29];
        //prod.GTIN_Number__c = csvRecordData[5];
        return prod; 
    }
    
    public Manufacturer_HIBC_Code__c createManufacturerHibcCodeRecordFromArray(string[] csvRecordData) {
        Manufacturer_HIBC_Code__c man = new Manufacturer_HIBC_Code__c();
        //man.name = csvRecordData[7] ; 
        //os.CreatedDate__c = Date(); //4
        // man.CreatedBy = currentUser;
        // man.LastModifiedBy = currentUser;
        man.HIBC_Code__c= csvRecordData[0];
        System.debug(csvRecordData[1]);
        string manufacturerName = getManufacturerFromProduction( csvRecordData[1]);
        System.debug(manufacturerName);
        if (manufacturerName != '') {
          Manufacturer__c man1 = GetManufacturer(manufacturerName);
          System.debug(man1);
          man.Manufacturer__c = man1.Id;//csvRecordData[12];
        } 
        //man.Manufacturer__c = csvRecordData[6];
        //man.Source_Record_ID__c = csvRecordData[8];
        return man;
    }
    public Attachment createAttachmentRecordFromArray(string[] csvRecordData) {
        Attachment att = new Attachment();
        System.debug(csvRecordData[6]);
        att.ParentId = csvRecordData[6];
        System.debug(csvRecordData[1]);
        att.Name = csvRecordData[1];
        //System.debug(csvRecordData[2]);
        //att.Description = csvRecordData[2];
        System.debug(csvRecordData[7]);
        att.Body = Blob.valueOf(csvRecordData[7]);
        System.debug(att);
        return att;
    }

    public Sensor_Data__c createSensorDataRecordFromArray(string[] csvRecordData) {
        Sensor_Data__c prod = new Sensor_Data__c() ;
        // prod.CreatedBy = currentUser;
        // prod.LastModifiedBy = currentUser;
        //prod.IsDeleted = false;
        //osDevRef.SystemModStamp
        System.debug(csvRecordData[0]);
        prod.Device_ID__c = csvRecordData[0];
        prod.Event__c = csvRecordData[1];
        //prod.ImageFileName__c = csvRecordData[3]; //empy now

        System.debug(csvRecordData[4]);
        prod.Measurement_Date__c = DateTime.now();//Datetime.valueOf(csvRecordData[4]);
        //prod.Name = csvRecordData[5];
        prod.Type__c = csvRecordData[6];
        prod.Value__c = csvRecordData[7];
        return prod;
    }


    //populates page variable 
    public void GetManufacturerList() {
        manufacturerList = [Select Id, Name from Manufacturer__c];
    }

    public string getManufacturerFromProduction(string manufacturerId) {
        if (manufacturerId == 'a6FU00000004CD3MAM') {
          return 'Smith and Nephew';
        } else if (manufacturerId == 'a6FU00000004CD4MAM') {
          return 'Medtronic';
        } else if (manufacturerId == 'a6FU00000004CD5MAM') {
          return 'DePuy';
        } else if (manufacturerId == 'a6FU00000004CD6MAM') {
          return 'Biomet';
        } else if (manufacturerId == 'a6FU00000004CD7MAM') {
          return 'Zimmer';
        } else if (manufacturerId == 'a6FU00000004CD0MAM') {
          return 'Stryker';
        } else if (manufacturerId == 'a6FU00000004CD1MAM') {
          return 'Orthosensor';
        } else if (manufacturerId == 'a6FU00000004CD2MAM') {
          return 'Mako';
        } else { return '';} 
     
    }

    private Manufacturer__c GetManufacturer(string manufacturer) {
        System.debug(manufacturer);
        for (Manufacturer__c man: manufacturerList) {
          //System.debug(man);
          if (man.Name == manufacturer) {
            return man;
          }
        }
        return new Manufacturer__c();
    }

    public User getCurrentUser() {
         return [Select Id, Name from User where Id=: UserInfo.getUserId()];
    }

    // public void retrieveMingusSensorData() {
    //   ProcedureSummaryController.CheckSoftwareVersion('a697A0000000HldQAE', 'G1JHAG000056', 'Right');
    // }

    public void patientPhaseAggregation() {
        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        agg.PhaseCountAggregatorBySurgeon();
        phaseCounts = [Select Id, Name, Month_Date__c, Practice_Name__c, Surgeon_Name__c, Count__c from IQ_Phase_Count__c order by Month_Date__c, Surgeon_Name__c];

    }

    public PageReference GeneratePatients() {
        IQ_CreateData repo = new IQ_CreateData();
        List<Patient__c> patients = new List<Patient__c>();
        repo.recordTypeName = 'Practice Account';
        // List<Patient__c> patientCount = [Select ID, Name from Patient__c];
        // System.debug(patientCount.size());
        List<Account> accts = [Select Id, Name from Account where ID =: practiceId];
        if (accts.size() > 0) {
		    string hospital = accts[0].Id;
		    System.debug(hospital);
		    patients = repo.CreatePatients(hospital, Integer.valueOf(patientCaseCount));
		    System.debug(patients);
            // patientCount = [Select ID, Name from Patient__c];
            // System.debug(patientCount.size());
        }
        return null;
    }

    // public PageReference GeneratePatientCases() {
    //     Integer count = Integer.valueOf(1);
    //     IQ_CreateData repo = new IQ_CreateData();
    //     Patient__c patient = new Patient__c();
    //     repo.recordTypeName = 'Practice Account';
    //     List<Patient__c> patientCount = [Select ID, Name from Patient__c];
    //     System.debug(patientCount.size());
       
    //     Account acct = [Select Id, Name from Account where ID =: practiceId limit 1];
    //     if (acct != null) {
	// 	    string hospital = acct.Id;
	// 	    System.debug(hospital);
	// 	    patient = repo.CreatePatient(hospital);
	// 	    System.debug(patient);
    //         currentSurvey = IQ_SurveyRepository.GetSurveyByName('KOOS');
    //         System.debug(currentSurvey);
    //         List<Hospital_Case_Type__c> ctRecs = [Select Case_Type__r.Id, Case_Type__r.Description__c 
    //         from Hospital_Case_Type__c Where Hospital__r.Id =: hospital];
    //         string caseTypeName = 'Total Knee Arthroplasty';
    //         if (ctRecs.size() > 0) {
    //             caseTypeName = ctRecs[0].Case_Type__r.Description__c;
    //         }
    //         Case_Type__c caseType = IQ_CreateData.RetrieveOrCreateCaseType(caseTypeName);
    //         System.debug(caseType);
           
    //         Account surgeonAccount = repo.CreateSurgeonAccount(acct.Id, 'TestLastName', 'TestFirstName' );
    //         System.debug(surgeonAccount);
    //         Surgeon_Hospital__c surgeon = repo.GetSurgeonById(surgeonAccount.Id);
    //         System.debug(surgeon);
    //         System.debug(caseTypeName); System.debug(patient); System.debug(currentSurvey); 
    //   		System.debug(surgeon.surgeon__r.AccountId);
    //         currentCase  = repo.CreateCase('Test', caseTypeName, patient, currentSurvey, 
    // 		    IQ_CreateData.CreateRandomProcedureDate(),
    //   		    surgeon.surgeon__r.AccountId, 'Right');
                
    //     }
    //     return null;
    // }

    public void AddSurveyResponses() {
        AddSurveyResponses(currentCase.Id, currentSurvey);
    }
    
    public void AddSensor() {
        IQ_CreateData repo = new IQ_CreateData();
        Event__c event = repo.GetProcedureEvent(currentCase.Id);
        
        List<Event__c> events = [Select ID, Name, Appointment_Start__c from Event__c where Event_Type_Name__c = 'Procedure' and Appointment_Start__c <= today];
        List<OS_Device_Event__c> deviceEvents = new List<OS_Device_Event__c> ();
        IQ_CreateData.Sensor sensor = repo.GetRandomSensor();
        string id = repo.AddOrRetrieveSensor(sensor);
        os_Device_Event__c deviceEvent = new os_Device_Event__c();
        deviceEvent.Event__c = event.Id;
        deviceEvent.OS_Device__c = id;
        insert deviceEvent;
    }

    public void AddSurveyResponses(string caseId, Survey__c survey) {
        List<Event_Form__c> efs =  GetEventSurveysByCase('KOOS', caseId);
        System.debug(efs);
        List<Survey_Response__c> allResponses = new List<Survey_Response__c>();
        for (Event_Form__c ef: efs) {
             if (ef.event__r.Appointment_Start__c <= Date.Today()) {
                //        System.debug('Getting Survey Responses');
                List<IQ_SurveyRepository.QuestionResponse> qrs = GenerateSurveyResponses(ef.event__r.event_Type_Name__c, 'KOOS');
                System.debug(qrs);
                List<Survey_Response__c> responses = IQ_SurveyRepository.CreateSurveyResponses(ef, survey, qrs);
                for (Survey_Response__c r : responses) {
                    allResponses.add(r);
                }
             }
        }
        System.debug(allResponses);
        upsert allResponses;
        // List<Event_Form__c> eventFormsToUpdate  = new List<Event_Form__c>();
        // for (Event_Form__c ef: efs) {
        //     Event_Form__c updateEvent = new Event_Form__c();
        //     updateEvent.Id = ef.Id;
        //     updateEvent.End_Date__c = Date.today();
        //     updateEvent.Form_Complete__c = true;
        //     eventFormsToUpdate.add(updateEvent);
        // }
        // upsert eventFormsToUpdate;
            
    }
    public List<IQ_SurveyRepository.QuestionResponse> GenerateSurveyResponses(String event, string surveyName) {
        List<IQ_SurveyRepository.QuestionResponse> qrs = new List<IQ_SurveyRepository.QuestionResponse>();
        string eventName = event;
        if (eventName == 'Pre-Op') {
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Symptoms', GetSurveyRange('30')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Pain', GetSurveyRange('41.3')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS ADL', GetSurveyRange('48.5')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Sport/Rec', GetSurveyRange('25.3')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS QOL', GetSurveyRange('22.9')));
       
        } else if (eventName == '4-8 Week Follow-up') {
            qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Symptoms', GetSurveyRange('40')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Pain', GetSurveyRange('62')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS ADL', GetSurveyRange('68')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Sport/Rec', GetSurveyRange('40')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS QOL', GetSurveyRange('42')));

        } else if (eventName == '6 Month Follow-up') {
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Symptoms', GetSurveyRange('78')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Pain', GetSurveyRange('79')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS ADL', GetSurveyRange('82')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Sport/Rec', GetSurveyRange('62')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS QOL', GetSurveyRange('61')));

        } else if (eventName == '12 Month Follow-up') {
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Symptoms', GetSurveyRange('80')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Pain', GetSurveyRange('83.3')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS ADL', GetSurveyRange('83')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Sport/Rec', GetSurveyRange('64')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS QOL', GetSurveyRange('67.5')));
        } else if (eventName == '2 year follow-up') {
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Symptoms', GetSurveyRange('90')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Pain', GetSurveyRange('84')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS ADL', GetSurveyRange('85')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS Sport/Rec', GetSurveyRange('68')));
                qrs.add(new IQ_SurveyRepository.QuestionResponse('KOOS QOL', GetSurveyRange('69')));
        } 
        
        return qrs;

    }

    public Double GetSurveyRange(String average) {
        Double start = Double.valueOf(average);
        return start;

    }
    public void getSurveyResponses(Event_Form__c eventForm, Survey__c survey, List<IQ_SurveyRepository.QuestionResponse> questionResponses) {
        System.debug(eventForm);
        System.debug(survey);
        System.debug(questionResponses);
        if (eventForm.id != null) {
            IQ_SurveyRepository.CreateSurveyResponses(eventForm, survey, questionResponses);
            Event_Form__c updateEvent = new Event_Form__c();
            updateEvent.Id = eventForm.Id;
            updateEvent.End_Date__c = Date.today();
            updateEvent.Form_Complete__c = true;
            update updateEvent;
        }
    }

    // Select ID, Name, Event_Form__r.event__r.Event_name__c,Event_Form__r.event__r.Event_Type_name__c, Survey__c, Survey_Question__c, Survey_response__c from Survey_Response__c
    public List<Event_Form__c> GetEventSurveysByCase(string surveyName, Id caseId) {
        List<Event_Form__c> ef = [Select ID, Name, event__r.case__r.Id, event__r.Appointment_Start__c, event__r.Event_Type_Name__c 
                From Event_Form__c 
                Where survey__r.name__c =: surveyName 
                And event__r.case__r.Id =: caseId];

        System.debug(ef);
        if (ef != null) {
            return ef;
        } else {
            return null;
        }
    }

    public void SendEmail() {
        IQ_SurveyRepository.SendEmail('richard salit', 'richard.salit@orthosensor.com', 'Test', 'Test body' );
    }

    // public class QuestionResponse {
    //     public string question {get; set;}
    //     public double response {get; set;}
    //     public QuestionResponse(string question1, Double response1) {
    //         question = question1;
    //         response = response1;
    //     }
    // }

//     KOOS Symptoms
// Pain
// Preoperative: 41.3
// 2 Weeks: 61.8
// 6 Weeks: 62.9
// 3 Months: 74.6
// 6 Months: 79.1
// 1 Year: 83.2
// ADL
// Preoperative: 48.5
// 2 Weeks: 66.1
// 6 Weeks: 69.3
// 3 Months: 78.5
// 6 Months: 82.0
// 1 Year: 83.6
// Sport/Rec 
// Preoperative: 25.3
// 2 Weeks: 36.3
// 6 Weeks: 42.0
// 3 Months: 56.9
// 6 Months: 62.0
// 1 Year: 64.0
// QOL
// Preoperative: 22.9
// 2 Weeks: 44.2
// 6 Weeks: 44.6
// 3 Months: 56.5
// 6 Months: 61.6
// 1 Year: 67.9 
}