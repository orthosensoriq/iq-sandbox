public with sharing class SurgeonHospitalActions{


    public static void AddContact(List<Surgeon_Hospital__c> surgeonHospitals)
    {
            List<Contact> contactsToInsert = new List<Contact>();
            
            Map<Id, Surgeon_Hospital__c> shMap  = new Map<Id, Surgeon_Hospital__c>([SELECT Id , Hospital__r.IsPartner, Surgeon__r.FirstName, Surgeon__r.LastName, Hospital__r.Id
                                                                                    FROM Surgeon_Hospital__c 
                                                                                    WHERE Id IN :surgeonHospitals]);
                                                                                    
            for(Surgeon_Hospital__c sh:surgeonHospitals)
            {
                
                Surgeon_Hospital__c querySH = shMap.get(sh.id);

                if(querySH.Hospital__r.IsPartner)
                {                  
                    Contact contactToAdd = new Contact();
                    contactToAdd.FirstName = querySH.Surgeon__r.FirstName;
                    contactToAdd.LastName = querySH.Surgeon__r.LastName;
                    contactToAdd.AccountId = querySH.Hospital__r.Id;
                    
                    contactsToInsert.add(contactToAdd);
                    
                }
                    
            }

            if(contactsToInsert.size()>0) insert contactsToInsert;      
        
       
        
    }
}