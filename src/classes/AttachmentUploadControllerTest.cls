@isTest(seeAllData=true)
public with sharing class AttachmentUploadControllerTest {
	
	static testMethod void testAttachmentUploadController() {
		
		// Create Case__c for test
		Case__c testCase = new Case__c();
		testCase.Name__c = 'TestName';
		insert testCase;
		
		// Create Event___c for test
		Event__c testEvent = new Event__c();
		testEvent.Case__c = testCase.Id;
		testEvent.Status__c = 'Complete';
		insert testEvent;
		
		//Construct controller
    	AttachmentUploadController con = new AttachmentUploadController(new ApexPages.StandardController(testEvent));
		
		// Execute code
		con.FileBody = [SELECT Body FROM Attachment LIMIT 1].Body;
		con.fileName = 'Test Attachment';
		PageReference pageRef1 = con.UploadFile();
		con.FileBody = null;
		con.FileName = '';
		PageReference pageRef2 = con.UploadFile();
	}

}