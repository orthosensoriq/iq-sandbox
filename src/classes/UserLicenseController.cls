/*****************************************************
This class is designed to handle User License Swapping
	for Community High-volume users vs. named users

REVISIONS
------------------------------------------------------
01/15/2014 - support@enablepath.com - Created

*****************************************************/
global class UserLicenseController implements Schedulable
{
    integer loginsThreshold = 0;
    string notificationEmail = '';
    final map<string, string> PartnerToHV = new map<string, string>();
    final map<string, string> HVToPartner = new map<string, string>();
    integer currentLicenseCount = 0;
    
    public UserLicenseController()
    {
        currentLicenseCount = getCurrentLicenseCount();
        if(Test.isRunningTest())
            currentLicenseCount = -1;
            
        //system.debug('CurrentLicenseCount: ' + currentLicenseCount);
        user_login_settings__c settings = [Select Logins_Per_Month_Threshold__c, License_Count_Notification_Address__c
                                           From user_login_settings__c
                                           Where Name = 'Default'];
        system.debug('settings is null: ' + (settings == null));
        loginsThreshold = settings.Logins_Per_Month_Threshold__c.intValue();
        notificationEmail = settings.License_Count_Notification_Address__c;

        list<control_center_profile_swaps__c> profileSwaps = [Select Partner_Profile__c, High_Volume_Profile__c from control_center_profile_swaps__c];
        for(control_center_profile_swaps__c swap : profileSwaps)
        {
			PartnerToHV.put(swap.Partner_Profile__c, swap.High_Volume_Profile__c);
            HVToPartner.put(swap.High_Volume_Profile__c, swap.Partner_Profile__c);
        }
    }

    global void execute(SchedulableContext ctx)
    {
        ProcessUserLicenses();
	}
    
	public boolean processUserLicenses()
    {
     	try
        {
            map<string, User> topUsers = new map<string, User>();
            map<string, User> bottomUsers = new map<string, User>();
            map<Id, Profile> profiles = new map<Id, Profile>([SELECT Id, name, UserLicense.Name 
                                                              FROM Profile 
                                                              WHERE UserLicenseID in (
                                                                  SELECT Id 
                                                                  FROM UserLicense 
                                                                  where Name in ('Partner Community','Partner Community Login'))]);  
            
            map<string, Id> profileNames = new map<string, Id>();
            map<string, User> users = new map<string, User>([SELECT Id, Name, ProfileId, Profile.Name, Profile.UserLicense.Name
                                                     From User 
                                                     where isActive = true and Ignore_Login_Threshold__c = false and
                                                           ProfileId in :profiles.keyset()]);
            list<AggregateResult> userRank = [SELECT UserId, count(id) numLogins 
                                              from LoginHistory 
                                              where LoginTime = THIS_MONTH 
                                              group by UserId 
                                              order by Count(Id) desc];
            for(Profile prof : profiles.values())
				profileNames.put(prof.Name, Prof.Id);
            
            System.debug('userRank.size: ' + userRank.size());
            integer j = 0;
            for(integer i = 0; i < userRank.size(); i++)
            {
                AggregateResult ar = userRank[i];
                if(users.keyset().contains(string.valueOf(ar.get('UserId'))))
                {
                    system.debug(string.valueOf(ar.get('numLogins')) + ' >= ' + loginsThreshold);
                    if(integer.valueOf(ar.get('numLogins')) >= loginsThreshold)
                    {
                        system.debug('TopUser: ' + i);
                        topUsers.put(string.valueOf(ar.get('UserId')), users.get(string.valueOf(ar.get('UserId'))));
                    }
                    else
                    {
                        system.debug('BottomUser: ' + i);
                        bottomUsers.put(string.valueOf(ar.get('UserId')), users.get(string.valueOf(ar.get('UserId'))));
                    }
                }
            }
            System.Debug('topUsers.Size: ' + topUsers.Size());
            System.Debug('bottomUsers.Size: ' + bottomUsers.Size());
            
            for(User bottom : bottomUsers.Values())
            {
                system.debug('bottom: ' + bottom.Name);
                if(bottom.Profile.UserLicense.Name != 'Partner Community Login')
	                bottom.ProfileId = profileNames.get(PartnerToHV.get(bottom.Profile.Name));
                else
                    bottomUsers.remove(bottom.Id);
            }
            
            system.debug(topUsers.size() + ' | ' + currentLicenseCount);
            if(topUsers.size() > currentLicenseCount)
            {
             	sendNotificationEmail('Current Partner License Count: ' + currentLicenseCount + '\n' + 'Total Users Over Threshold: ' + topUsers.size());
            }
            
            integer i = 0;
            for(User top : topUsers.Values())
            {
                i++;
                if(i <= currentLicenseCount)
                {
                    system.debug('top: ' + top.Name);
                    if(top.Profile.UserLicense.Name != 'Partner Community')
                        top.ProfileId = profileNames.get(HVToPartner.get(top.Profile.Name));
                    else
                        topUsers.remove(top.Id);
                }
                else
                {
                    if(top.Profile.UserLicense.Name == 'Partner Community')
                        top.ProfileId = profileNames.get(PartnerToHV.get(top.Profile.Name));
                    
                    system.debug('top becoming bottom: ' + top.ProfileId);
                    
                }
            }
            
            update bottomUsers.values();
            update topUsers.values();
            
	        return true;
        }
        catch(exception ex)
        {
			return false;            
        }
    }
    
    private integer getCurrentLicenseCount()
    {
        //THIS FUNCTION IS ADAPTED FROM: 
        // http://salesforce.stackexchange.com/questions/3916/getting-remaining-licenses-of-customer-portal-using-apex
        String result = '';
        String rawData = '';        
        String orgName;
        String orgCountry;

        //Get Organization to get content of company info details
        Organization orgDetails =[select Id, Name, Country from Organization limit 1];
        Id orgId = orgDetails.Id;
        orgName = orgDetails.Name;
        orgCountry = orgDetails.Country;
        
        PageReference pr=new PageReference('/'+orgId);
        //called screenscraping: get the data from the page
        if(!Test.isRunningTest())
	        rawData = pr.getContent().toString();

        
        //locate a particular element within the raw data
        //the info after this line contains the active Partner Community license count
        String licRow = '>Partner Community</th><td class=" dataCell  ">Active</td><td class=" dataCell  numericalColumn">';
        Integer licLen = licRow.length();
        System.debug('******************** licLen: ' + licLen);

        Integer pos=0;
        if(!Test.isRunningTest())
            pos = rawData.indexOf(licRow);
    
        if (-1!=pos)
        {
            Integer licStart = pos + licLen;
            
	        if(!Test.isRunningTest())            
    	        result = rawData.substring(licStart, rawData.IndexOf('<',licStart));
            else
                result = '1';
        
            System.debug('***************** SubString: ' + result);
        }
        
        return Integer.ValueOf(result);
    }
    
    private void sendNotificationEmail(string counts)
    {
        string emailBody = 'The ControlCenter Community has more users above the Login Number threshold: \n';
        emailBody += counts + '\n\n';
        emailBody += 'Please review the license report and adjust as necessary.';
        system.debug(emailbody);
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       	String[] toAddresses = new String[] {notificationEmail}; 
       	mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('ControlCenter Licensing');
        mail.setSubject('ControlCenter Notification: Not Enough Partner Community Licenses');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}