/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/
@isTest
private class SCMSnapshotUMTest {

    static testMethod void runcreate() {
    	SCMC__Inventory_Position__c posn = SCMSnapshotUMTest.setupPartInInventory(); 
        ID batchprocessid = null;
        Test.startTest();
        batchprocessid= Database.executeBatch(new SCMCreateInventorySnapShotUM()); 
        Test.stopTest();
        SCMC__Month_End_Inventory__c[] sshots = [select id
                        , SCMC__IP_Modified_DateTime__c
                        , SCMC__Item_Master__c
                        , SCMC__Bin__c
        			from SCMC__Month_End_Inventory__c];
        system.assertEquals(1, sshots.size(), 'unexpected number of snapshot records');
        system.debug('snap:' + sshots);
        system.debug('posn:' + posn);
        for (SCMC__Month_end_Inventory__c sshot : sshots)
        {
        	System.assertNotEquals(null, sshot.SCMC__IP_Modified_DateTime__c);
            system.assertEquals(posn.SCMC__Item_Master__c, sshot.SCMC__Item_Master__c, 'unexpected item master');
            system.assertEquals(posn.SCMC__Bin__c, sshot.SCMC__Bin__c, 'unexpected inv loc');
        }

        AsyncApexJob aJob = [Select Status
            , NumberOfErrors 
            From AsyncApexJob 
            where id = :batchprocessid];
        System.assertEquals('Completed', aJob.Status);
        System.assertEquals(0, aJob.NumberOfErrors, 'Batch process failed');
    }

    private static SCMC__Inventory_Position__c setupPartInInventory() {

        SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
        uom.name = 'TestUOMsdfg4sr8t6';
        insert uom;

        SCMC__Item__c item = new SCMC__Item__c();
        item.name = 'Test9asdrgs5454sdfg';
        item.SCMC__Item_Description__c = 'Test part for unit tests';
        item.SCMC__Inspection_Required__c  = false;
        item.SCMC__Serial_Number_Control__c = false;
        item.SCMC__Lot_Number_Control__c = false;
        item.SCMC__Stocking_UOM__c = uom.id;
        insert item;

        SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
		curr.name = 'qqq';
		curr.SCMC__Active__c = true;
		curr.SCMC__Curr_Conversion_Rate__c = 1;
		curr.SCMC__Corporate_Currency__c = true;
		insert curr;

        SCMC__Address__c address = new SCMC__Address__c();
        address.name = 'Test Address';
        address.SCMC__City__c = 'A City';
        address.SCMC__Country__c = 'Country';
        address.SCMC__Line1__c = 'Address line 1';
        address.SCMC__PostalCode__c = 'Postcd';
        address.SCMC__State__c = 'State';
        insert address;

        SCMC__ICP__c icp = new SCMC__ICP__c();
        icp.SCMC__Address__c = address.id;
        icp.name = 'TICPasdga4564';
        icp.SCMC__Currency__c = curr.id;
        insert icp;
        
        SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
        warehouse.SCMC__ICP__c = icp.id;
        warehouse.SCMC__Address__c = address.id;
        warehouse.name = 'TWHasdf767dfg';
        warehouse.SCMC__Active__c = true;
        insert warehouse;

        SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c();
        location.name = 'Level1';
        location.SCMC__Level1__c = 'Level1';
        location.SCMC__Warehouse__c = warehouse.id;
        insert location;
        
        SCMC__Inventory_Position__c ip = new SCMC__Inventory_Position__c();
        ip.SCMC__Acquisition_Cost__c          = 55.00;
        ip.SCMC__Acquisition_Currency__c      = curr.id;
        ip.SCMC__Availability__c              = true;
        ip.SCMC__Availability_Code__c         = 'In Stock';
        ip.SCMC__Bin__c                       = location.id;
        ip.SCMC__Condition_Code__c            = null;
        ip.SCMC__Current_Value__c             = 55.00;
        ip.SCMC__ICP_Acquisition_Cost__c      = 55.00;
        ip.SCMC__ICP_Currency__c              = curr.id;
        ip.SCMC__ILS_Eligible__c              = false;
        ip.SCMC__Item_Master__c               = item.Id;
        ip.SCMC__Listed_on_ILS__c             = false;
        ip.SCMC__Lot_Number__c                = null;
        ip.SCMC__Manufacturer_CAGE__c         = null;
        ip.SCMC__Owned_By__c                  = null;
        ip.SCMC__Ownership_Code__c            = null;
        ip.SCMC__Quantity_Allocated__c        = 0;
        ip.SCMC__Quantity__c                  = 5;
        ip.SCMC__Quantity_in_Transit__c       = 0;
        ip.SCMC__Receipt_Line__c              = null;
        ip.SCMC__Receiving_Inspection__c      = null;
        ip.SCMC__Reserve_Price__c             = 60.00;
        ip.SCMC__Revision_Level__c            =  null;
        ip.SCMC__Sale_Price__c                = 65.00;
        ip.SCMC__Item_Serial_Number__c        = null;
        ip.SCMC__Shelf_Life_Expiration__c     = null;
        ip.SCMC__List_Type__c                 = 'Y';

        insert ip;
        return ip;
    } 
}