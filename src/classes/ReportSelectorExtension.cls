public with sharing class ReportSelectorExtension {
    
    public List<Report> ctrlCtrReports{get;set;}
    public map<Id, string> ctrlCtrFolderIds {get;set;}
    public List<reportWrapper> reportWrap {get;set;}
    public Report selectedReport { get; set; }
    public Id ReportID {get;set;}
    logUtils osLogUtils = new logUtils();
    
    public ReportSelectorExtension(){
        ctrlCtrReports = new List<Report>();
        reportWrap = new List<reportWrapper>();
        ctrlCtrFolderIds = new map<Id, string>();
        for(folder fldr : [select id, name from folder where developername in ('VERASENSE_iQ_Reports', 'VERASENSE_iQ_Admin')])
            ctrlCtrFolderIds.put(fldr.Id,fldr.name);
        
        ctrlCtrReports = [select id, name, description, ownerid from report where ownerid in :ctrlCtrFolderIds.keyset()];
        for(ID folderId : ctrlCtrFolderIds.keySet())
        {
            list<rptWrap> wrappercontent = new list<rptWrap>();
            for(report curRpt : ctrlctrReports)
            {
                if(curRpt.OwnerId == folderId)
                    wrappercontent.add(new rptWrap('', curRpt.Id, curRpt));
            }
            system.debug(ctrlCtrFolderIds.get(folderId) + ': ' + wrappercontent.size());
            if(wrappercontent != null)
            {
                reportWrapper wrapper = new reportWrapper(ctrlCtrFolderIds.get(folderId), wrappercontent);
                reportWrap.add(wrapper);
            }
        }
    }
    
    // Log info to audit log and open report
    public PageReference goToSelectedReport()
    {
        selectedReport = [select id, name from report where id = :ReportId];
        PageReference pageRef = new PageReference('/apex/ViewReports');
        pageRef.setRedirect(false);
        if(!Test.isRunningTest())
	        osLogUtils.logCRUD('View Report', selectedReport);
        
        return pageRef;
    }
    
    public PageReference generatePDFReport()
    {
        try
        {
            if(string.isnotblank(ReportId)) 
            {
                
                Report[] r = [select id,name from report where id = :ReportId];
                osLogUtils.logCRUD('Export Report', r[0]);
                //Folder saveTo = [select id, name from folder where type='document' and name='TempReportStash'];
                //string folderid = saveTo.id;
                string folderId = userinfo.getuserid();
                string filename = r[0].name + string.valueOf(Datetime.now().format('YYYYmmddhhmmss'))+'.pdf';
                system.debug(folderId + ' | ' + filename);
                document d = new document(folderid=folderid,name=filename,contenttype='application/pdf',body=Test.isRunningTest()?Blob.valueOf(''):new pagereference('/'+ReportId+'?excel=1').getcontentaspdf());
                insert d;
				system.debug('inserted PDF');
                for(reportWrapper repwrp : reportWrap)
                {
                    for(rptWrap rw : repwrp.reports)
                    {
                        if(rw.rptId == ReportId)
                        {
                            rw.downloadURL = '/servlet/servlet.FileDownload?file='+d.id;
                            system.debug('Got downloadURL: ' + rw.downloadURL);
                        }
                    }
                }
            }
        }
        catch(exception ex)
        {
            System.debug('Error:' + ex);
            ApexPages.addMessages(ex);
        }
        return null;
    }

    public class reportWrapper
    {
        public string folderName {get;set;}
        public list<rptWrap> reports {get;set;}
        
        public reportWrapper(string fldrName, list<rptWrap> rpts)
        {
            folderName = fldrName;
            reports = rpts;
        }
    }
    
    public class rptWrap
    {
        public string downloadURL {get;set;}
        public string rptId {get;set;}
        public Report rpt {get;set;}
        
        public rptWrap(string dnld, string rid, Report rept)
        {
            downloadURL = dnld;
            rptId = rid;
            rpt = rept;
        }
    }

}