/*
    Copyright (c) 2013 LessSoftware.com, inc.
    All rights reserved.
    
  Class to start batch process to load sales order
*/
public with sharing class LoadSOExtension {
    
	private ApexPages.StandardSetController setController;

    //this  constructor is invoked from the multi select on the requisition tab
    public LoadSOExtension(ApexPages.StandardSetController controller) {
    	this.setcontroller = controller;
	}
  
    
  public PageReference process() {
    Database.executeBatch(new LoadSO(),4);        
	// Get the sObject describe result for the Account object
	Schema.DescribeSObjectResult r = SOStage__c.sObjectType.getDescribe(); 
	String keyPrefix = r.getKeyPrefix();
	PageReference retPage = new PageReference('/' + keyPrefix + '/o');
    return retPage;  
    
  }
  public PageReference goback() {
    PageReference retPage = setController.cancel();
    return retPage;
  }
  

}