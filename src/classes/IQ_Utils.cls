public class IQ_Utils {
    public static void RemoveDuplicatesFromOS_Dev_Ref() {
    List<OS_Device_Ref__c> devs = [Select ID, Name, Manufacturer__c, GTIN_Number__c 
                                   from os_device_ref__c 
                                   where GTIN_Number__c != null];
    
        for (OS_Device_Ref__c dev: devs) {
            List<OS_Device_Ref__c> devsToDelete = [Select ID, Name, Manufacturer__c, GTIN_Number__c 
                                       from os_device_ref__c 
                                       where Name  = :dev.Name 
                                       AND GTIN_Number__c = null];
            delete devsToDelete;
        }

//         List<Sensor_Data__c> s = [SELECT Name, (Select id, Name From Sensor_Data__c.Attachments)
//  FROM Sensor_Data__c where CreatedDate < 2016-10-01T00:00:00.000Z];
    // List<Attachment> a = [select id from attachment where parentid in :s.id];
    //for (if (a.size() > 0) { delete a;}

    //}
    // List<Attachment> a = [Select Id from Attachment where CreatedDate < 2016-10-01T00:00:00.000Z];
}