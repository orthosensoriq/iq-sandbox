global without sharing class IQ_PromDeltaAggregate implements Schedulable{
	public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Integer monthsToAnalyze {get; set;}
    public Date firstDayOfFirstMonth {get; set; }
    public Date lastDayofLastMonth {get; set;}
    public Integer monthsInThePast {get; set;}
    public Event__c currentPreopEvent {get; set;}
    public List<Delta> deltas {get; set;}
    public Survey__c currentSurvey {get; set;}
	public List<Surgeon_Hospital__c> accts {get; set;}
	public List<Account> practices {get; set;}
	public List<Surgeon_Hospital__c> surgeons {get; set;}

	global void execute(SchedulableContext sc) {
		Process();
	}

	public void Process() {
		accts = IQ_DashboardRepository.GetParents(); 
		practices = [Select ID, Name from Account Where RecordType.Name = 'Practice Account'];
		surgeons = IQ_PatientActions.LoadAllSurgeons();

		// Delete table
		List<IQ_Prom_Delta__c> toDelete = [Select ID, Name from IQ_Prom_Delta__c];
        delete toDelete;
		//iterate through procedure dates if they have pre-op surveys
		List<Event__c> events = GetProcedures();
		System.debug(events.size());
		events = RemoveEventsMissingPreopProm(events);

		//after retrieving list, cycle through patients evaluating  	
		SetFollowUpDeltas();
	}

	public List<Event__c> GetProcedures() {
		List<Event__c> events = [Select Id, Name, event_Type__c, event_Type__r.Name, Case__c, Appointment_Start__c, status__c
								from event__c
								where status__c = 'Complete' and event_Type__r.Name = 'Procedure'];
		return events;
	}

	public List<Event__c> RemoveEventsMissingPreopProm(List<Event__c> events) {
		List<Event__c> eventsWithPreop = new List<Event__c>();
		deltas = new List<Delta>();
		List<Event_Form__c> preopEvents = GetPreopEvents();
		for (Event__c event: events) {
			System.debug(event);
			if (PreopExists(event, preopEvents)) {
				eventsWithPreop.add(event);
			}
		}
		System.debug(eventsWithPreop.size());
		return eventsWithPreop;
	}

	public List<Event_Form__c> GetPreopEvents() {
		return [Select Id, Name, event__r.event_Type_Name__c, 
									event__r.Appointment_Start__c, end_Date__c, event__r.Case__c
									From event_form__c
									Where form_complete__c = true 
									and event__r.event_Type_Name__c = 'Pre-Op'
									and Survey__r.Name =:currentSurvey.Name];
	}

	public boolean PreopExists(Event__c event, List<Event_Form__c> preopEvents) {
		 System.debug(event);
		 System.debug(currentSurvey);

		// get pre-op events for the case
		Event_Form__c preopEvent = new Event_Form__c();
		Boolean eventFound = false;
		for (Event_Form__c pEvent: preopEvents) {
			if (pevent.event__r.case__c == event.Case__c) {
				preopEvent = pevent;
				eventFound = true;
				break;
			}
		}
		System.debug(preopEvent);
		// save pre-op data in a temporary object
		if (eventFound) {
			if (preopEvent.Name != '') {
				System.debug(preopEvent);
				Delta d = new Delta();
				d.caseId = event.Case__c;
				d.eventId = event.Id;
				d.eventFormId = preopEvent.Id;
				d.surveyId = currentSurvey.Id;
				deltas.add(d);
			}
			return (event.id != null);
		} else {
			return false;
		}
	}

	public void SetFollowUpDeltas() {
		System.debug(deltas);
		if (deltas != null && deltas.size() > 0) {
			for (Delta delta1: deltas) {
				// get all case surveys - mak sure all patients are populated - if not create another process to get them!
				List<Event_Form__c> formEvents = [Select ID, Name, Event__r.case__c, Event__r.Event_Type_Name__c, Event__r.Physician__c, End_Date__c,
												Event__r.Patient_ID__c, Event__r.Patient__c
                                              	From event_form__c 
												Where Form_complete__c = true
												and Name = :currentSurvey.Name
												and event__r.case__c = :delta1.caseId];
				System.debug(formEvents.size());
				System.debug(formEvents);

				List<DeltaScore> preOpScores = GetPreopScores(formEvents);

	            // get other events dates if preop was found
            	if (preopScores != null) {
					// cycle through case events
					CreateFormDeltas(formEvents, preOpScores);           	
				}
			}
		}
	}

	public List<DeltaScore> GetPreopScores(List<Event_Form__c> formEvents) {
		List<DeltaScore> preOpScores;
		DeltaScore preOpScore;
		for (Event_Form__c event: formEvents) {
          if (event.Event__r.Event_Type_Name__c == 'Pre-Op') {
			System.debug(event.Event__r.Event_Type_Name__c);
            preOpScores = new List<DeltaScore>();
            List<Survey_Response__c> preOpResponses = GetSurveyScores(event.Id);
            for (Survey_Response__c response: preOpResponses) {
               	preOpScore = new DeltaScore();
               	preOpScore.surveyId = currentSurvey.Id;
				preOpScore.eventFormId = event.Id;
    			preOpScore.stage = event.Event__r.Event_Type_Name__c;
    			preOpScore.section = response.Survey_Question__c;
                try {
    				preOpScore.score = Decimal.valueOf(response.Survey_Response__c);
                } catch (TypeException e) {
                    preOpScore.score = 0;
                }
                preOpScores.add(preOpScore);
           }
        }
       }
		return preOpScores;
	}
	
	public void CreateFormDeltas(List<Event_Form__c> formEvents, List<DeltaScore> preOpScores) {
		if (formEvents != null && formEvents.size() > 0) {
		for (Event_Form__c event: formEvents) {
			System.debug(event);
            if (event.Event__r.Event_Type_Name__c != 'Pre-Op') {
				// get survey reposnses related to event
               	List<Survey_Response__c> preOpResponses = GetSurveyScores(event.Id);
               	for (Survey_Response__c response: preOpResponses) {
					System.debug(response);
               		IQ_Prom_Delta__c score = new IQ_Prom_Delta__c();
               		score.SurveyId__c = currentSurvey.Id;
					score.EventFormId__c = event.Id;
					score.SurgeonID__c = event.Event__r.Physician__c;
					score.SurgeonName__c = GetSurgeonName(event.Event__r.Physician__c, surgeons);
					Surgeon_Hospital__c acct = IQ_DashboardRepository.GetParent(accts,  event.Event__r.Physician__c);
            		if (acct.Hospital__r.Id != null) {
                		score.PracticeID__c      = acct.Hospital__r.Id;
						score.PracticeName__c = GetPracticeName(score.PracticeID__c, practices);
            		}
    				score.Stage__c = event.Event__r.Event_Type_Name__c;
    				score.Section__c = response.Survey_Question__c;
    				score.ProcedureDate__c = event.End_Date__c;

					if (event.Event__r.Patient_ID__c != null) {
						score.PatientID__c = event.Event__r.Patient_ID__c;
						score.PatientName__c = event.Event__r.Patient__c;
					}
    				Boolean found = false;
    				for (DeltaScore preOpResponse: preOpScores) {			
    					if (preOpResponse.section == response.Survey_Question__c) {
  							found = true;
							System.debug(event.Event__r.Event_Type_Name__c);
    	            		System.debug(preOpResponse);
    						System.debug(response);
                        	Decimal followUpScore = Decimal.valueOf(response.Survey_Response__c);
                        	System.debug(followUpScore);
    						score.score__c = followUpScore - preOpResponse.score;
                        	upsert score;
	                        // break;
                    	}
                    }
            	}
    		}
	   	}
		}
	}

    public List<Survey_Response__c> GetSurveyScores(String eventFormId) {
        List<Survey_Response__c> surveyResponses = [select ID, Name, Event_Form__c, Survey_Question__c, Survey_Response__c 
                                                    from Survey_Response__c
                                                    where Event_Form__c = :eventFormId];
        return surveyResponses;
    }

	public String GetSurgeonName(String surgeonId, List<Surgeon_Hospital__c> surgeons) {
		System.debug(surgeonId);
		System.debug(surgeons);
		for (Surgeon_Hospital__c surgeon: surgeons) {
			if (surgeon.surgeon__r.AccountId == surgeonID) {
				return surgeon.surgeon__r.Name;
			}
		}
		return '';
	}

	public String GetPracticeName(String practiceId, List<Account> practices) {
		for (Account practice: practices) {
			if (practice.ID == practiceId) {
				return practice.Name;
			}
		}
		return '';
	}
    
	public List<IQ_Prom_Delta__c> GetAllPromDeltas() {
    	return [Select SurveyId__c, EventFormId__c, PracticeID__c, SurgeonID__c, stage__c, section__c, score__c, ProcedureDate__c 
    			from IQ_Prom_Delta__c score];
    }

	public class Delta {
		public string caseId {get; set;}
		public string eventId {get; set;}
		public string eventFormId { get; set; }
		public string surveyId {get; set;}
		public string procedureDate {get; set;}
		public string surgeonID {get; set;}
		public string practiceID {get; set;}
		public Decimal post8Week {get; set;}
		public Decimal post6Month {get; set;}
		public Decimal post12Month {get; set;}
	}

	public class DeltaScore {
		public string surveyId { get; set; }
	public string eventFormId { get; set; }	
    public string stage { get; set; }
    public string section { get; set; }
    public decimal score { get; set; }
}
}