public with sharing class CaseSelectorController{

	public Event__c procedure {get;set;}
    public String HospitalID {get;set;}
    String interfaceId;
    public boolean showHosp {get;set;}
	public String SelectedCaseType {get;set;}

	public CaseSelectorController(){
		
		HospitalID = ApexPages.currentPage().getParameters().get('accid');
		interfaceId = ApexPages.currentPage().getParameters().get('intid');
		//if(ProcedureID!=null) procedure = [Select Id, Case__c, Case__r.Case_Type__c from Event__c where Id =: ProcedureID];
		if(HospitalID != null || interfaceId != null)
        {
            showHosp = false;
        }
        else
            showHosp = true;
	}
    
	/**
    public void LogConstructor()
    {
        if(ProcedureID != null)
        {
            map<integer, string> params = new map<integer, string>();
            params.put(0, ProcedureID);
         	osController baseCtrl = new osController();
            procedure = (Event__c)baseCtrl.getSelectedData('CaseSelectorProcedure', params)[0];
        }
    }
	**/
	public PageReference gotoProcedureCreation()
	{
		if(HospitalID!=null && interfaceId != null) 
		{
			PageReference pageref = new PageReference('/apex/ProcedureCreation?intid=' + interfaceId + '&accid=' + HospitalID + '&ctid=' + SelectedCaseType);
			return pageref;
		}
		else if(HospitalID!=null)
		{
			PageReference pageref = new PageReference('/apex/ProcedureCreation?accid=' + HospitalID + '&ctid=' + SelectedCaseType);
			return pageref;
		}
        else if(interfaceId != null)
        {
			PageReference pageref = new PageReference('/apex/ProcedureCreation?intid=' + interfaceId + '&ctid=' + SelectedCaseType);
			return pageref;
        }
		
		return null;
	}
	
	public List<selectOption> getCaseTypeOptions()
	{
		List<Case_Type__c> caseTypes = [Select Id, Description__c from Case_Type__c Limit 1000];
		List<selectOption> options = new List<selectOption>(); 	
		
		for(Case_Type__c ct : caseTypes)
		{
			options.add(new selectOption(ct.Id, ct.Description__c));	
		}

		
		return options;
	}

    public List<selectOption> getHospitalOptions()
	{
		List<Account> hospitals = [SELECT Id, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner=true ORDER BY Name ASC LIMIT 1000];
		List<selectOption> options = new List<selectOption>(); 	
		
		for(Account a : hospitals)
		{
			options.add(new selectOption(a.Id, a.Name));	
		}
		return options;
	}
    
}