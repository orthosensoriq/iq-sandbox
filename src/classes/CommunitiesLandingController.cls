/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        return Network.communitiesLanding();
    }
    
    public PageReference customForwardToStartPage() {
        //ManagedSecurityController comSecure = new ManagedSecurityController(True);
        //if(!comSecure.UserAcceptedCurrent)
        //    return new PageReference(comSecure.getRedirectUrl());
        //else
            //return Network.communitiesLanding();
            return getLandingPageForProfile();
    }
    
    public CommunitiesLandingController() {}
    
    private PageReference getLandingPageForProfile()
    {
        System.debug('#####' + ConnectApi.Communities.getCommunities());
        //return Network.communitiesLanding();

        //string curProfileId = system.UserInfo.getProfileId();
        //Profile curProfile = [Select Id, Name from Profile where Id =: curProfileId];
     	//if(curProfile.Name.contains('Surgeon'))
        //    return Page.DashboardPatientMatrix;
        //else if(curProfile.Name.contains('Office/Research'))
        //	return Page.DashboardPatientMatrix;
        //else if(curProfile.Name.contains('Hospital'))
        //  return Page.DashboardPatientMatrix;
        //else 
            //return Page.DashboardPatientMatrix;
        
        // if(IQ_Settings__c.getInstance().New_UI__c){
            // return 
            return Page.index;
        // }else{
        //    return Page.DashboardPatientMatrix;
        // }   
    }
}