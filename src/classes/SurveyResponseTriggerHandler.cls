public with sharing class SurveyResponseTriggerHandler extends BaseTriggerHandler
{
    final String CLASSNAME = '\n\n**** SurveyResponseTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : the class constructor method
    //@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
    //@returns : nothing
    public SurveyResponseTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
        String METHODNAME = CLASSNAME.replace('METHODNAME', 'SurveyResponseTriggerHandler') + ' - class constructor';
        system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

        // trigger is executing
        TriggerIsExecuting = isExecuting;
        
        // set batch size
        BatchSize = pTriggerSize;
        
        // set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
        //Not Called for this Trigger
    }
    
    public override void OnAfterInsert(Map<Id, SObject> insertObject)
    {
        Super.OnAfterInsert(insertObject);
        set<Id> efId = new set<Id>();
        For(survey_response__c resp : (list<survey_response__c>)insertObject.values())
        {
            efId.add(resp.event_form__c);
        }
        list<event_form__c> forms = new list<event_form__c>();
        For(event_form__c frm : [select Id, form_complete__c, End_Date__c 
                                 from event_form__c
                                 where Id in :efId])
        {
            frm.form_complete__c = true;
            frm.End_Date__c = date.today();
            forms.add(frm);
        }
        update forms;
    }
    
    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
        //Not Called for this Trigger
    }

    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
        set<string> efId = new set<string>();
        For(survey_response__c resp : (list<survey_response__c>)deleteObject.values())
        {
            efId.add(resp.event_form__c);
        }
        list<AggregateResult> existingFormData = [Select event_form__c, count(id) formCount
                                                    from survey_response__c
                                                    where event_form__c in :efId
                                                    group by event_form__c
                                                    order by event_form__c];

        for(integer i = 0; i < existingFormData.size(); i++)
        {
            AggregateResult ar = existingFormData[i];
            if(integer.valueOf(ar.get('formCount')) > 0)
            {
                efId.remove(string.valueOf(ar.get('event_form__c')));
            }
        }
        if(efId.size() > 0)
        {
            list<event_form__c> updateFormData = [Select id, form_complete__c, End_Date__c 
                                                  from event_form__c
                                                  where id in :efId];
            for(event_form__c upd : updateFormData)
            {
                upd.form_complete__c = false;
                upd.End_Date__c = null;
            }
            
            update updateFormData;
        }
    }
}