/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2016 FinancialForce.com, inc. All rights reserved.
*/
@isTest
public with sharing class SCMFFA_SetupTest {

	private static Integer suffix = 0;

	public void SetUpFFA() {
		// so we can create FFA things (requires FinancialForce.com - Developer Utilities package)
		system.runAs(new User(Id = UserInfo.getUserId())) {
			Type t = Type.forName('ffps_util.SDAUtils');
			System.Schedulable looseCoupledInstance = (System.Schedulable)t.newInstance();
			looseCoupledInstance.execute(null);

			//Create essential data for Batch
			//FFA_SetupTest.createDimensions();
			FFA_SetupTest.createGLAs();
			FFA_SetupTest.createTaxCodes();
			FFA_SetupTest.createTaxRates();
			//FFA_SetupTest.createVATCompany();
			FFA_SetupTest.createCompanies();
			FFA_SetupTest.SelectCompanyForUser();
			if(UserInfo.isMultiCurrencyOrganization())
			{
				FFA_SetupTest.createCurrencies();
				FFA_SetupTest.createExchangeRates();
			}
			else
			{
				FFA_SetupTest.createHomeAndDualCurrency(FFA_SetupTest.getCorporateCurrency());
			}
			FFA_SetupTest.createFinancialCalendar();
			FFA_SetupTest.createProduct() ;
			FFA_SetupTest.createAccount() ;
			FFA_SetupTest.createBankAccount();
		}
	}

	public SCMC__Currency_Master__c createTestCurrency() {
		return createTestCurrency('USD', true);
	}

	public SCMC__Currency_Master__c createTestCurrency(String cname, Boolean corporate) {
		SCMC__Currency_Master__c curr = new SCMC__Currency_Master__c();
		try {
			curr = [select id, name
					from SCMC__Currency_Master__c
					where name = :cname];
		} catch (Exception ex) {
			curr.name = cname;
			curr.SCMC__Active__c = true;
			curr.SCMC__Corporate_Currency__c = corporate;
			insert curr;
		}
		return curr;
	}

	public SCMC__Condition_Code__c createConditionCode(String code, String ilscode) {
		SCMC__Condition_Code__c cCode = new SCMC__Condition_Code__c();
		try {
			cCode = [select id
					   from SCMC__Condition_Code__c
					  where name = :code];
		} catch (Exception ex) {
			cCode.name = code;
			cCode.SCMC__description__c = 'Description for ' + code;
			cCode.SCMC__Disposal_Code__c = '1 -Excellent';
			cCode.SCMC__Supply_Code__c = 'A -Serviceable';
			insert cCode;
		}
		return cCode;
	}

	public Product2 createProduct(string prodName, c2g__codaGeneralLedgerAccount__c glAcct, boolean doInsert) {
		Product2 p = new Product2(
				name = prodName
				, IsActive = true
				, c2g__CODASalesRevenueAccount__c = glAcct.Id
			);

		if (doInsert) {
			insert p;
		}
		return p;
	}

	public SCMC__Unit_of_Measure__c createUnitofMeasure(String name) {
		SCMC__Unit_of_Measure__c uom = new SCMC__Unit_of_Measure__c();
		try {
			uom = [select id
					from SCMC__Unit_of_Measure__c
					where name = :name];
		} catch(Exception ex) {
			uom.name =name;
			insert uom;
		}
		return uom;
	}

	public SCMC__PO_Payment_Terms__c createPOPaymentTerm() {
		SCMC__PO_Payment_Terms__c term = new SCMC__PO_Payment_Terms__c(
			SCMC__Terms_Name__c = '3/30 Net 60'
			,SCMC__Description__c = '3% discount if paid within 30 days'
			,SCMC__Number_of_Days__c = 60
			,SCMC__Discount_Percentage__c = 3
			,SCMC__Discount_Days__c = 30
			//,SCMC__Date_Due__c = ' '
			,SCMC__Basis_for_Due__c = 'Invoice Date'
			);
		insert term;
		return term;
	}

	public SCMC__Supplier_Site__c createTestSupplier(SCMC__Currency_Master__c curr, boolean doInsert) {
		SCMC__PO_Payment_Terms__c term = createPOPaymentTerm();
		SCMC__Supplier_Site__c sc = new SCMC__Supplier_Site__c();
		sc.name = 'Test supplier for unit tests';
		sc.SCMC__Currency__c = curr.Id;
		sc.SCMC__Preferred_Communication_Sourcing__c = 'E-Mail';
		sc.SCMC__Preferred_Communication_Purchase_Order__c = 'E-Mail';
		sc.SCMC__E_Mail__c = 'test@test.com';
		sc.SCMC__PO_Payment_Terms__c = term.id;

		if (doInsert) {
			insert sc;
		}
		return sc;
	}

	public Account createTestCustomerAccount(Boolean custActive, Boolean doInsert, Boolean onHold, id warehouseSpecific) {

		Account custSite = new Account(
				SCMC__Active__c = custActive
				,AnnualRevenue = null
				,SCMC__CAGE_Code__c = null
				,SCMC__Capability_Listing__c = null
				,SCMC__Certifications__c = null
				,SCMC__Credit_Card_Expiration_Date__c = null
				,SCMC__Credit_Card_Number__c = null
				,SCMC__Credit_Card_Type__c = null
				,SCMC__Credit_Card_Validation_Code__c = null
				,SCMC__Customer__c = true
				,SCMC__DUNS_Number__c = null
				,SCMC__Default_Payment_Type__c = null
				,SCMC__Corp_City__c = 'San Diego'
				,SCMC__Corp_Country__c = 'USA'
				,SCMC__Corp_State_Province__c = 'CA'
				,SCMC__Corp_Line1__c = '247 Ocean Blvd.'
				,SCMC__Corp_Line2__c = null
				,SCMC__Corp_PostalCode__c = '92130'
				,Description = 'Test customer site'
				,FAX = '555-1234'
				,SCMC__FOB__c = null
				,SCMC__Freight__c = null
				,Industry = null
				,SCMC__Language__c = null
				,NumberOfEmployees = null
				,Phone = null
				,Ownership = null
				,ParentId = null
				,SCMC__Preferred_Communication__c = 'Fax'
				,SCMC__Provide_Advanced_Ship_Notice__c = false
				,SIC = null
				,SCMC__Sales_Rep__c = null
				,SCMC__Sales_Responsibility__c = null
				,SCMC__Small_Business_Designations__c = null
				,SCMC__Tax_Exemption_Group__c = null
				,SCMC__Terms__c = null
				,TickerSymbol = null
				,Type = null
				,Website = null
				,SCMC__warehouse__c = warehouseSpecific
				,BillingStreet = '123 Main St\nSuite 101\nc/0George'
				,BillingCity = 'San Francisco'
				,BillingState = 'CA'
				,BillingPostalCode = '12345'
				,BillingCountry = 'US'

				,ShippingStreet = '123 Main St\nSuite 101\nc/0George'
				,ShippingCity = 'San Francisco'
				,ShippingState = 'CA'
				,ShippingPostalCode = '12345'
				,ShippingCountry = 'US'

				,SCMC__Ship_Via__c = 'UPS Ground'
				,SCMC__Shipping_Account__c = '123456789'
			);

		if (onHold){
			custSite.SCMC__Temporary_Hold__c = 'Finance';
		}else {
			custSite.SCMC__Temporary_Hold__c = null;
		}

		custSite.Name = 'Test Customer' + string.valueOf(suffix++);

		if (doInsert){
			insert custSite;
		}
		return custSite;
	}

	public SCMC__Address__c createTestAddress() {
		SCMC__Address__c address = new SCMC__Address__c();
		address.name = 'Test Address';
		address.SCMC__City__c = 'A City';
		address.SCMC__Country__c = 'Country';
		address.SCMC__Line1__c = 'Address line 1';
		address.SCMC__PostalCode__c = 'Postcd';
		address.SCMC__State__c = 'State';
		insert address;
		return address;
	}

	public SCMC__ICP__c createTestICP(SCMC__Address__c address, SCMC__Currency_Master__c curr, boolean doInsert) {
		SCMC__ICP__c icp = new SCMC__ICP__c();
		icp.SCMC__Address__c = address.id;
		icp.name = 'Test ICP';
		icp.SCMC__Currency__c = curr.id;

		if (doInsert) {
			insert icp;
		}
		return icp;
	}

	public SCMC__Warehouse__c createTestWarehouse(SCMC__ICP__c icp, SCMC__Address__c address){
		SCMC__Warehouse__c warehouse = new SCMC__Warehouse__c();
		warehouse.SCMC__ICP__c = icp.id;
		warehouse.SCMC__Address__c = address.id;
		warehouse.name = 'Test Warehouse';
		insert warehouse;
		return warehouse;
	}

	public SCMC__Product_Group__c createProdGroup(string groupName, c2g__codaGeneralLedgerAccount__c inv, c2g__codaGeneralLedgerAccount__c cog, boolean doInsert) {
		SCMC__Product_Group__c prodGroup = new SCMC__Product_Group__c  (name = groupName);
		if (inv != null){
			prodGroup.SCMFFA__Inventory_Account__c = inv.id;
		}
		if (cog != null){
			prodGroup.SCMFFA__Cogs_Account__c = cog.id;
		}

		if (doInsert) {
			insert prodGroup;
		}
		return prodGroup;
	}

	public SCMC__Item__c createTestItem(SCMC__Unit_of_Measure__c uom, boolean doInsert) {
		if (uom == null) {
			uom = createUnitofMeasure('Each');
		}
		SCMC__Item__c im = new SCMC__Item__c();
		im.name = 'Test' + suffix++;
		im.SCMC__Item_Description__c = 'Test part for unit tests';
		im.SCMC__Inspection_Required__c  = false;
		im.SCMC__Serial_Number_Control__c = false;
		im.SCMC__Lot_Number_Control__c = false;
		im.SCMC__Stocking_UOM__c = uom.id;

		if (doInsert){
			insert im;
		}
		return im;
	}

	public SCMC__Purchase_Order__c createPurchaseOrder(SCMC__Supplier_Site__c supplier, boolean doSave) {
		SCMC__Purchase_Order__c po = new SCMC__Purchase_Order__c();

		po.SCMC__Supplier_Site__c = supplier.Id;
		po.SCMC__Status__c = 'Open';
		po.SCMC__Purchase_Order_Date__c = Date.today();

		if (doSave){
			insert po;
		}
		return po;
	}

	public SCMC__Purchase_Order_Line_Item__c createPurchaseOrderLine(SCMC__Purchase_Order__c po, SCMC__Item__c item, SCMC__Warehouse__c warehouse, Boolean doInsert) {

		SCMC__Purchase_Order_Line_Item__c line = new SCMC__Purchase_Order_Line_Item__c();

		//line.RecordTypeId = SCMFF_Utilities.getRecordType('Item', line).Id;
		line.SCMC__Purchase_Order__c = po.Id;
		line.SCMC__Supplier_Commitment_Date__c = Date.today();
		line.SCMC__Supplier_Current_Promise_Date__c = Date.today();
		line.SCMC__Status__c = 'Open';
		line.SCMC__Item_Master__c = item.id;
		line.SCMC__Quantity__c = 10;
		line.SCMC__Unit_Cost__c = 15.00;

		if (warehouse != null)
		{
			line.SCMC__Warehouse__c = warehouse.Id;
		}

		if (doInsert){
			insert line;
		}

		return line;
	}

	public SCMC__GL_Account__c createGLMapping(string scmAcct, c2g__codaGeneralLedgerAccount__c glAcct, boolean doInsert) {
		SCMC__GL_Account__c scmGL = new SCMC__GL_Account__c(
				SCMC__SCM_Account__c = scmAcct
				, SCMFFA__General_Ledger_Account__c = glAcct.Id
				, SCMC__Financial_GL_Account__c = 'Not Used'
			);

		if (doInsert) {
			insert scmGL;
		}
		return scmGL;
	}

	public SCMC__Inventory_Transaction_Perpetual_Record__c createPerpetualTransaction(
			string transactionType
			, SCMC__Item__c item
			, SCMC__Reason_Code__c reasonCode
			, SCMC__Unit_of_Measure__c uom
			, SCMC__Inventory_Location__c loc
			, boolean doInsert) {

		/* Supported Transaction Types:
			Allocate Customer Items
			Reverse Customer Allocation
			Issue Customer Items Pick-list
			Reverse Issue Customer Items Pick-list
			Allocate Supplier Items
			Reverse Allocate Supplier Items
			Issue Supplier Items Pick-list
			Reverse Issue Supplier Items Pick-List
			Removed from Inventory - Consumed in Service Purchase Order
			Receive Purchased Material
			Reverse Receive Purchased Material
			Receive Exchanged Items (Cores)
			Reverse Receive Exchanged Items (Cores)
			Receive Customer Returned Material
			Reverse Receive Customer Returned Material
			Inspection Accept
			Inspection Accept with Deviation
			Inspection Accept Use as Is
			Inspection Reject Scrap Parts
			Inspection Reject Return Parts to Vendor
			Reverse Inspection
			Return to Stock
			Reverse Return to Stock
			Scrap Inventory
			Reverse Scrap Inventory
			Locate Inventory
			Reverse Locate Inventory
			Cycle Count
			Reverse Cycle Count
			Miscellaneous Issue
			Reverse Miscellaneous Issue
			Miscellaneous Receipt
			Item Transfer Outbound
			Reverse Item Transfer Outbound
			Item Hold
			Reverse Item Hold
			Item Transfer Inbound
			Reverse Item Transfer Inbound
			Inspection
			Inspection Reject
			Inspection Accept Us as Is
			Adjust Current Value
			Adjust Ownership
			Adjust Condition
			PPV
			Void PPV
			Bill Drop Ship
			Credit Bill Drop Ship
		*/


		SCMC__Inventory_Transaction_Perpetual_Record__c itpr = new SCMC__Inventory_Transaction_Perpetual_Record__c(
				SCMC__Inventory_Transactions__c = transactionType
				, SCMC__Condition_Code__c = null
				, SCMC__Inventory_Location__c = loc.Id
				, SCMC__Item_Master__c = item.Id
				, SCMC__New_Location_Balance__c = 10
				, SCMC__Old_Location_Balance__c = 10
				, SCMC__Quantity__c = 1
				, SCMC__Reason_Code__c = reasonCode.Id
				, SCMC__Unit_of_Measure__c = uom.Id
				, SCMC__Export_Status__c = 'New'
			);

			//	'SCMC__Sales_Order_Line_Item__r.SCMC__Sales_Order__r.SCMC__Customer_Account__c',
			//	'SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Product_Group__r.Inventory_Account__c',

		/*
				'SCMC__Item_Master__c',
				'SCMC__Item_Master__r.name',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Inventory_Account__c',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Inventory_Account__r.name',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.COGS_Account__c',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.COGS_Account__r.name',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Dimension_1__c',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Dimension_2__c',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Dimension_3__c',
				'SCMC__Item_Master__r.SCMC__Product_Group__r.Dimension_4__c',
		*/

		if (doInsert) {
			insert itpr;
		}
		return itpr;
	}

	public SCMC__Inventory_Transaction_Financial_Records__c[] createFinancialTransactions(SCMC__Inventory_Transaction_Perpetual_Record__c itpr
			, SCMC__Currency_Master__c curr
			, string acct1
			, string acct2
			, decimal unitCost
			, decimal quantity
			, boolean doInsert) {
		SCMC__Inventory_Transaction_Financial_Records__c[] itfrs = new SCMC__Inventory_Transaction_Financial_Records__c[]{};

		SCMC__Inventory_Transaction_Financial_Records__c itfr1 = new SCMC__Inventory_Transaction_Financial_Records__c(
				SCMC__Trade_Value_Amount__c = quantity * unitCost
				, SCMC__Trade_Unit_Cost__c = unitCost
				, SCMC__Trade_Currency__c = curr.Id
				, SCMC__Quantity__c = quantity
				, SCMC__Inventory_Transaction__c = itpr.Id
				, SCMC__ICP_Value_Amount__c = quantity * unitCost
				, SCMC__ICP_Unit_Cost__c = unitCost
				, SCMC__ICP_Currency__c = curr.Id
				, SCMC__GL_Account__c = acct1
			);
		itfrs.add(itfr1);

		SCMC__Inventory_Transaction_Financial_Records__c itfr2 = new SCMC__Inventory_Transaction_Financial_Records__c(
				SCMC__Trade_Value_Amount__c = quantity * unitCost * -1
				, SCMC__Trade_Unit_Cost__c = unitCost
				, SCMC__Trade_Currency__c = curr.Id
				, SCMC__Quantity__c = quantity * -1
				, SCMC__Inventory_Transaction__c = itpr.Id
				, SCMC__ICP_Value_Amount__c = quantity * unitCost * -1
				, SCMC__ICP_Unit_Cost__c = unitCost
				, SCMC__ICP_Currency__c = curr.Id
				, SCMC__GL_Account__c = acct2
			);
		itfrs.add(itfr2);

		/* GL Account possibilities:
				Inventory
				AP Liability
				Cycle Count
				Miscellaneous
				Cost of Goods Sold
				OutsideService
				Transfer
				Exchange
				Return
				Scrap
				Non-Inventory
				Cost Adjustment
				WIP
		*/

		if (doInsert) {
			insert itfrs;
		}
		return itfrs;
	}

	public SCMC__Inventory_Location__c createTestLocation(SCMC__Warehouse__c warehouse, boolean doInsert) {
		SCMC__Inventory_Location__c location = new SCMC__Inventory_Location__c(
				name = 'Level1'
				, SCMC__Level1__c = 'Level1'
				, SCMC__Warehouse__c = warehouse.id
			);
		if (doInsert){
			insert location;
		}
		return location;
	}

	public SCMC__Reason_Code__c createTestReason(c2g__codaGeneralLedgerAccount__c glAcct) {
		SCMC__Reason_Code__c reason = new SCMC__Reason_Code__c(
				Name = 'test reason'
				, SCMFFA__General_Ledger_Account__c = glAcct.Id
			);
		insert reason;
		return reason;
	}

	public SCMC__Sales_Order__c createTestSalesOrder(Account account, boolean doInsert) {
		SCMC__Sales_Order__c so = new SCMC__Sales_Order__c(
				SCMC__Sales_Order_Date__c = Date.today()
				, SCMC__Primary_Sales_Rep__c = UserInfo.getUserId()
				, SCMC__Customer_Account__c = account.Id
				//, RecordTypeId = SCMFF_Utilities.getRecordType('Inventory', new SCMC__Sales_Order__c()).Id
			);

		if (doInsert) {
			insert so;
		}

		return so;
	}

	public SCMC__Sales_Order_Line_Item__c createSalesOrderLine(SCMC__Sales_Order__c so, SCMC__Item__c item, String status, boolean isDropShip, boolean doInsert) {
		Id recTypeId = null;

		if (isDropShip) {
			//recTypeId = SCMFF_Utilities.getRecordType('Drop_Ship', new SCMC__Sales_Order_Line_Item__c()).Id;
		} else {
			//recTypeId = SCMFF_Utilities.getRecordType('Item', new SCMC__Sales_Order_Line_Item__c()).Id;
		}

		SCMC__Sales_Order_Line_Item__c line = new SCMC__Sales_Order_Line_Item__c(
				RecordTypeId = recTypeId
				, SCMC__Sales_Order__c = so.Id
				, SCMC__Quantity__c = 1
				, SCMC__Price__c = 25.00
				, SCMC__Item_Master__c = item.Id
				, SCMC__Customer_Commitment_Date__c = Date.today()
				, SCMC__Status__c = status
				, SCMC__Condition_Code__c = null
			);

		if (doInsert) {
			insert line;
		}
		return line;
	}

	public SCMC__Inventory_Position__c setupPartsInInventory(SCMC__Warehouse__c warehouse, SCMC__Inventory_Location__c invLoc, SCMC__Item__c item, SCMC__Currency_Master__c curr, boolean doInsert) {
		SCMC__Inventory_Position__c ip = new SCMC__Inventory_Position__c(
				SCMC__Acquisition_Cost__c		  = 55.00
				, SCMC__Acquisition_Currency__c	  = curr.id
				, SCMC__Availability__c			  = true
				, SCMC__Availability_Code__c		 = 'In Stock'
				, SCMC__Bin__c					   = invLoc.id
				, SCMC__Condition_Code__c			= null
				, SCMC__Current_Value__c			 = 55.00
				, SCMC__ICP_Acquisition_Cost__c	  = 55.00
				, SCMC__ICP_Currency__c			  = curr.id
				, SCMC__ILS_Eligible__c			  = false
				, SCMC__Item_Master__c			   = item.Id
				, SCMC__Listed_on_ILS__c			 = false
				, SCMC__Lot_Number__c				= null
				, SCMC__Manufacturer_CAGE__c		 = null
				, SCMC__Owned_By__c				  = null
				, SCMC__Quantity_Allocated__c		= 0
				, SCMC__Quantity__c				  = 100
				, SCMC__Quantity_in_Transit__c	   = 0
				, SCMC__Receipt_Line__c			  = null
				, SCMC__Receiving_Inspection__c	  = null
				, SCMC__Reserve_Price__c			 = 60.00
				, SCMC__Revision_Level__c			=  null
				, SCMC__Sale_Price__c				= 65.00
				, SCMC__Item_Serial_Number__c		= null
				, SCMC__Shelf_Life_Expiration__c	 = null
				, SCMC__List_Type__c				 = 'Y'
			);

		if (doInsert) {
			insert ip;
		}
		return ip;
	}

	public static SObject createNewSObject(Schema.SObjectType t)
	{
		SObject obj = t.newSObject();
		return obj;
	}

	public static void setId(SObject obj)
	{
		obj.Id = generateMockId(obj.getSObjectType());
	}

	private static Integer fakeIdCount = 0;
	public static Id generateMockId(Schema.SObjectType t)
	{
		String keyPrefix = t.getDescribe().getKeyPrefix();
		fakeIdCount++;
		String fakeIdPrefix = '000000000000'.substring(0, 12 - fakeIdCount.format().length());
		return Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount);
	}
}