/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2016 FinancialForce.com, inc. All rights reserved.
*/
@isTest
private with sharing class SCMFFA_APVoucherExportPlugin_Test
{

	@isTest
	private static void NormalSuccessfulTest()
	{
		SCMC__AP_Voucher__c apVoucher = (SCMC__AP_Voucher__c) SCMFFA_SetupTest.createNewSObject(SCMC__AP_Voucher__c.SObjectType);
		SCMFFA_SetupTest.setId(apVoucher);

		c2g__codaPurchaseInvoice__c pin = (c2g__codaPurchaseInvoice__c) SCMFFA_SetupTest.createNewSObject(c2g__codaPurchaseInvoice__c.SObjectType);
		pin.c2g__DeriveCurrency__c = false;
		pin.c2g__CopyAccountValues__c = false;
		SCMFFA_SetupTest.setId(pin);

		Map<Id, SObject> sObjectMap = new Map<Id, SObject>();
		sObjectMap.put(apVoucher.Id, pin);

		SCMFFA_APVoucherExportPlugin plugin = new SCMFFA_APVoucherExportPlugin();
		plugin.execute(sObjectMap, null, null);

		System.assertEquals(
			true,
			pin.c2g__DeriveCurrency__c,
			'The PIN\'s Derive Currency property should be True.');

		System.assertEquals(
			true,
			pin.c2g__CopyAccountValues__c,
			'The PIN\'s Copy Account Values property should be True.');
	}
}