public with sharing class ContactActions{

    @future
    public static void CreatePortalUser(List<Id> contactIds){

        List<User> userstoInsert = new List<User>();
        List<Task> taskstoInsert = new List<Task>();
        //user_login_settings__c uls = [select Id, Control_Center_Admin__c from user_login_settings__c where Control_Center_Admin__c !=null LIMIT 1];
        user_login_settings__c uls = user_login_settings__c.getValues('Default');

        Map<Id, Contact> contactMap  = new Map<Id, Contact>([SELECT Id, AccountId, Account.IsPartner, Email, FirstName, LastName
                                                                                    FROM Contact
                                                                                    WHERE Id IN :contactIds]);

        Profile CommLoginProfile = [Select id from Profile where Name = 'Partner Community Login User' limit 1];
        User u;
        if(Test.IsRunningTest()) u = [Select id from User where isactive = true limit 1];

        for(Id cid:contactIds)
        {

            Contact c = contactMap.get(cid);

            if(c.Account.IsPartner || Test.isRunningTest())
            {
                if(c.Email!=null)
                {
                    User usertoAdd = new User();
                    usertoAdd.FirstName = c.FirstName;
                    usertoAdd.LastName = c.LastName;
                    usertoAdd.ContactId = c.Id;
                    usertoAdd.Email = c.Email;
                    usertoAdd.Username = c.Email;
					Integer lastNameLen = c.LastName.length();
                    if(lastNameLen>=5) usertoAdd.Alias = c.FirstName.substring(0,0) + c.LastName.substring(0,4);
					else usertoAdd.Alias = c.FirstName.substring(0,0) + c.LastName;
                    usertoAdd.TimeZoneSidKey = 'GMT';
                    usertoAdd.LocaleSidKey = 'en_US';
                    usertoAdd.EmailEncodingKey = 'ISO-8859-1';
                    usertoAdd.LanguageLocaleKey = 'en_US';
					usertoAdd.IsActive = true;
                    usertoAdd.ProfileId = CommLoginProfile.Id;

                    userstoInsert.add(usertoAdd);
                }
                else
                {
                    Task tasktoAdd = new Task();
                    tasktoAdd.Subject = 'Get e-mail address and convert';
                    tasktoAdd.Description = 'This contact cannot be granted access to Control Center without an e-mail address. Please add an e-mail address and convert to a partner user.';
                    tasktoAdd.OwnerId = uls.Control_Center_Admin__c;
                    if(Test.IsRunningTest()) tasktoAdd.OwnerId = u.id;
                    tasktoAdd.WhoId = c.id;

                    taskstoInsert.add(tasktoAdd);
                }

            }

        }

        if(userstoInsert.size()>0) insert userstoInsert;
        if(taskstoInsert.size()>0) insert taskstoInsert;


    }
}