global with sharing class SCM_APVoucherExport {
	
	global with sharing class ExportedAPVoucher {
		webservice Date dueDate;
		webservice string financialSystemBill;
		webservice Date invoiceDate;
		webservice ExportedAPVoucherLine[] lines;
		webservice string message;
		webservice string QBAPAccount;
		webservice string status;
		webservice string terms;
		webservice string vendorInvoiceNumber;
		webservice string vendorName;
		webservice Id voucherId;
		webservice string voucherName;
	}

	global with sharing class ExportedAPVoucherLine {
		webservice string accountOverride;
		webservice double amount;
		webservice double cost;
		webservice string itemDescription;
		webservice string itemNumber;
		webservice Id lineId;
		webservice double quantity;
	}

	Webservice static ExportedAPVoucher[] getAPVouchers() {

		SCMC__AP_Voucher__c[] vouchers = [select id
    			, name
    			, SCMC__Supplier_Site__c
    			, SCMC__Supplier_Site__r.name
    			, SCMC__Invoice_Number__c
    			, SCMC__Status__c
    			, SCMC__Terms__c
    			, SCMC__Invoice_Date__c
    			, SCMC__Due_Date__c
    			, SCMC__Export_Status__c
    			, SCMC__Export_Error__c
    			, SCMC__Foreign_Invoice_Number__c
    			, (select Id
    					, name
    					, SCMC__Quantity__c
    					, SCMC__Extended_Price__c
    					, SCMC__Unit_Price__c
    					, SCMC__Accounting_Item__c
    					, SCMC__Accounting_Item__r.name
    					, SCMC__Accounting_Item__r.SCMC__Item_Description__c
    					, SCMC__Accounting_Item__r.SCMC__Product_Group__r.name
    					, SCMC__Purchase_Order_Line_Item__c
    					, SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.Name
    					, SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Item_Description__c
    					, SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Product_Group__r.name

    				from SCMC__AP_Voucher_Lines__r)

    			from SCMC__AP_Voucher__c
    			where SCMC__Export_Status__c = 'New'
    				and SCMC__Status__c = 'Matched'];
		
		system.debug('+++vopuchers:' + vouchers);
		ExportedAPVoucher[] expVouchers = new ExportedAPVoucher[]{};

		for (SCMC__AP_Voucher__c voucher : vouchers) {

			voucher.SCMC__Export_Status__c = 'Export in Process';

			ExportedAPVoucher expVoucher = new ExportedAPVoucher();
			expVoucher.voucherId = voucher.Id;
			expVoucher.voucherName = voucher.name;
			expVoucher.vendorName = voucher.SCMC__Supplier_Site__r.name;
			expVoucher.vendorInvoiceNumber = voucher.SCMC__Invoice_Number__c;
			expVoucher.dueDate = voucher.SCMC__Due_Date__c;
			expVoucher.terms = voucher.SCMC__Terms__c;
			expVoucher.invoiceDate = voucher.SCMC__Invoice_Date__c;
			expVoucher.financialSystemBill = voucher.SCMC__Foreign_Invoice_Number__c;

			expVoucher.lines = new ExportedAPVoucherLine[]{};

			for (SCMC__AP_Voucher_Line__c line : voucher.SCMC__AP_Voucher_Lines__r) {
				ExportedAPVoucherLine expLine = new ExportedAPVoucherLine();
				expLine.accountOverride = '';
				expLine.amount = line.SCMC__Extended_Price__c;
				expLine.cost = line.SCMC__Unit_Price__c;

				if (line.SCMC__Purchase_Order_Line_Item__c != null) {
					expLine.itemNumber = line.SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Product_Group__r.name;
					expLine.itemDescription = line.SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.Name + ' - ' + line.SCMC__Purchase_Order_Line_Item__r.SCMC__Item_Master__r.SCMC__Item_Description__c;
				} else if (line.SCMC__Accounting_Item__c != null) {
					expLine.itemNumber = line.SCMC__Accounting_Item__r.SCMC__Product_Group__r.name;
					expLine.itemDescription = line.SCMC__Accounting_Item__r.name + ' - ' + line.SCMC__Accounting_Item__r.SCMC__Item_Description__c;
				} else {
					// TODO: is this a valid scenario? do we use 'non-receivable' here?
					expLine.itemDescription = '';
					expLine.itemNumber = '';
				}

				expLine.lineId = line.Id;
				expLine.quantity = line.SCMC__Quantity__c;
				expVoucher.lines.add(expLine);
			}

			expVouchers.add(expVoucher);
		}

		update vouchers;
		return expVouchers;
	}

	Webservice static void flagExported(ExportedAPVoucher[] vouchers) {
		Map<Id, SCMC__AP_Voucher__c> voucherIdToVoucher = new Map<Id, SCMC__AP_Voucher__c>();
    	for (ExportedAPVoucher voucher : vouchers) {
    		voucherIdToVoucher.put(voucher.voucherId, null);	
    	}
    	List<SCMC__AP_Voucher__c> updVouchers = [select id, name
    			,SCMC__Export_Status__c
    			,SCMC__Export_Error__c
    			,SCMC__Foreign_Invoice_Number__c
    		from SCMC__AP_Voucher__c
    		where Id in :voucherIdToVoucher.keyset()];

    	for (SCMC__AP_Voucher__c updVoucher : updVouchers){
    		voucherIdToVoucher.put(updVoucher.id, updVoucher);
    	}

    	for (ExportedAPVoucher voucher : vouchers) {
    		SCMC__AP_Voucher__c updVoucher = voucherIdToVoucher.get(voucher.voucherId);
    		updVoucher.SCMC__Export_Status__c = voucher.status;
    		updVoucher.SCMC__Export_Error__c  = voucher.message;
    		updVoucher.SCMC__Foreign_Invoice_Number__c = voucher.financialSystemBill;
    	}
    	update updVouchers;
	}


}