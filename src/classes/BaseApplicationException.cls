//@author : EnablePath
//@date : 12/18/2013
//@description :    class to extend the Exception class and provide the user the ability to throw custom exceptions 
//                  related to business rules, etc.; supports user-defined error messages as well as "user-friendly" error messages
global virtual class BaseApplicationException extends Exception
{
    final String CLASSNAME = 'BaseApplicationException.METHODNAME()';
    
    public String UserFriendlyMessage{get; set;}
    public String Message
    {
        get
        {
            return this.getMessage();
        }
    }

    private String privateMethodName;
    public String SourceMethodName
    {
        get
        {
            return privateMethodName; 
        }
        set
        {
            privateMethodName = value.Replace('*','').Replace(' ','');
        }
    }

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    protected BaseApplicationException(String pSourceMethodName, String pMessage)
    {
        this.SourceMethodName = pSourceMethodName;
        this.setMessage(pMessage);      
    } 
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    protected BaseApplicationException(String pSourceMethodName, String pMessage, Exception pException)
    {
        this(pSourceMethodName, pMessage);
        this.initCause(pException);
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    public static BaseApplicationException NewException(String pSourceMethodName, String pMessage, String pUserFriendlyMessage)
    {
        BaseApplicationException bae = new BaseApplicationException(pSourceMethodName,pMessage);
        bae.UserFriendlyMessage = pUserFriendlyMessage;
        bae.LogException();
        return bae;
    }

    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    public static BaseApplicationException NewException(String pSourceMethodName, String pMessage)
    {
        return BaseApplicationException.NewException(pSourceMethodName, pMessage, pMessage);
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    public static BaseApplicationException NewExceptionWithInnerException(String pSourceMethodName, String pMessage, Exception pException)
    {
        return BaseApplicationException.NewExceptionWithInnerException(pSourceMethodName, pMessage, pMessage, pException);
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    public static BaseApplicationException NewExceptionWithInnerException(String pSourceMethodName, String pMessage, String pUserFriendlyMessage, Exception pException)
    {
        BaseApplicationException bae = new BaseApplicationException(pSourceMethodName, pMessage, pException);
        bae.UserFriendlyMessage = pUserFriendlyMessage;
        bae.LogException();
        return bae;
    }
    
    //@author : CloudTrigger, Inc.
    //@date : 11/17/2011
    //@description : method to ...
    //@paramaters : none
    //@returns : nothing
    public virtual void LogException()
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','LogException');
        try
        {
            //String errorDetails = this.SourceMethodName + ' :: ' + ExceptionHandler.GetExceptionDetailsAsString(this);
           
            //StringBuilder sb = CT_StringBuilder.NewWithFirstValue('\n\n******************************************************************************************************');
            //sb.Append('\n          ERROR DETAILS          ');
            //sb.Append('\n******************************************************************************************************\n');
            //sb.Append('\n' + errorDetails + '\n');
            //sb.Append('\n\n******************************************************************************************************\n\n');
            //system.Debug(LoggingLevel.INFO, sb.AsString());
        }
        catch (Exception ex)
        {
            // TODO
        } 
    }
    
    //@author : EnablePath
    //@date : 12/18/2013
    //@description : method to ...
    //@paramaters : 
    //@returns : 
    public virtual String GetExceptionDetailsAsString()
    {
        final string METHODNAME = CLASSNAME.replace('METHODNAME','GetExceptionDetailsAsString');
        return '';
    }
}