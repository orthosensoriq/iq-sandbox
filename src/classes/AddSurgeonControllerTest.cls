@isTest
private class AddSurgeonControllerTest {
	
	static testMethod void testAddSurgeonController() {
		
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('TestPhysicianJSONResponse');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, mock);
		
		// Create PersonAccount for test
		Account testSurgeon = new Account();
		testSurgeon.NPI__c = '1234567890';
		testSurgeon.FirstName = 'Test';
		testSurgeon.LastName = 'Surgeon';
		testSurgeon.BillingState = 'GA';
		testSurgeon.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Surgeon Account'].Id;	
		//insert testSurgeon;

		//Construct controller
		AddSurgeonController con = new AddSurgeonController();
		con.newSurgeonNPI = '123456';
		con.newSurgeonFirstName = 'John';
		con.newSurgeonLastName = 'Smith';
		con.searchState = 'GA';

		// Execute code
		con.getContent();
		con.searchLastName = 'Browning';
		con.searchState = 'GA';
		con.getContent();

		con.newSurgeonNPI = testSurgeon.NPI__c;

		con.closePopup();
		con.showPopup();

		List<selectOption> options = con.getitems();
		String[] currHospitals = con.getHospitals();
		PageReference pageRef1 = con.saveValues();
		
		Test.stopTest();
		
		PageReference pageRef = con.AddSurgeon();
	}
	
}