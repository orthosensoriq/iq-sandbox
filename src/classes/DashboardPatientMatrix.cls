public with sharing class DashboardPatientMatrix {
	
	public List<SummaryWrapper> Summaries { get; set; }
    public List<ADT_interface__c> ScheduledPatients { get; set; }
    public string whichView {get;set;}
    public string whichHospital {get;set;}
    public string whichSurgeon {get;set;}
    public string whichCaseType {get;set;}
    public list<SelectOption> hospitalItems {get;set;}
    public list<SelectOption> surgeonItems {get;set;}
    public list<SelectOption> caseTypeItems {get;set;}
    public boolean showAll {get;set;}
    public List<String> hospitalIDs {get;set;}
    public List<PatientTableLine> ptLines {get;set;}
    
    public DashboardPatientMatrix() 
    {
    	hospitalItems = new list<SelectOption>();
    	surgeonItems = new list<SelectOption>();        
    	caseTypeItems = new list<SelectOption>();
    	hospitalIDs = new list<String>();
    	ptLines = new List<PatientTableLine>();
    	ScheduledPatients = new List<ADT_interface__c>();
        whichHospital = '';
        whichSurgeon = '';
        whichCaseType = '';
		showAll = false;
		getHospitals();
        loadPatientMatrix();
        system.debug('---Scheduled Patients size = ' + ScheduledPatients.size());
        if(ScheduledPatients != null && ScheduledPatients.size() > 0) {
        	system.debug('---Scheduled Patients > 0');
            whichView = '2';
        }
        else
        {
            whichView = '1';
            loadCaseMatrix();
        }
        
        //getHospitals();
        getSurgeons();
        getCaseTypes();
        
        //loadCaseMatrix();
    }
    
    private void getHospitals()
    {
    	system.debug('BEGIN GET HOSPITALS');
        hospitalItems.clear();
        hospitalItems.add(new SelectOption('','All'));
        hospitalIDs.clear();
        if(whichView == '2')
        {
            set<string> hospIds = new set<string>();
            if(!showAll)
            {
                for(AggregateResult hosp : [select count(id), SFDC_Account__c
                                         From 	ADT_Interface__c
                                         Where (Appointment_Date__c = TODAY OR Appointment_Date__c = YESTERDAY) 
                                         group by SFDC_Account__c
                                         order by SFDC_Account__c])
                {
                    hospIds.add(string.valueOf(hosp.get('SFDC_Account__c')));
                }
            }
			else
            {
                for(AggregateResult hosp : [select count(id), SFDC_Account__c
                                         From 	ADT_Interface__c
                                         group by SFDC_Account__c
                                         order by SFDC_Account__c])
                {
                    hospIds.add(string.valueOf(hosp.get('SFDC_Account__c')));
                }
            }
		
            for(Account hosp: [Select Id, Name
                               From account
                               where Id in :hospIds])
            {
                hospitalItems.add(new SelectOption(hosp.id,hosp.name));
                hospitalIDs.add('\'' + hosp.id + '\'');
            }
        }
        else
        {
            set<string> hospIds = new set<string>();
            for(AggregateResult hosp : [select count(id), location__c
										From	event__c
                                        where   Case__r.active__c = true and 
                                       			Event_type__r.primary__c = true and
                                        		location__c != null and location__r.name != ''
                               			group by location__c])
            {
                hospIds.add(string.valueOf(hosp.get('location__c')));
            }
            if(hospIds.size() > 0)
            {
                for(account a : [select id, name from account where id in :hospIds])
                {
                    hospitalItems.add(new SelectOption(string.valueOf(a.id), a.name));
                    hospitalIDs.add('\'' + a.id + '\'');
                }
            }
        }
    }
    
    public void getSurgeons()
    {
    	system.debug('BEGIN GET SURGEONS');
        surgeonItems.clear();
        surgeonItems.add(new SelectOption('','All'));
        system.debug('GET SURGEONS whichView = ' + whichView);
        if(whichView == '2')
        {
        	system.debug('GET SURGEONS showAll = ' + showAll);
            if(!showAll)
            {
            	system.debug('GET SURGEONS whichHospital = ' + whichHospital);
                if (whichHospital != null && whichHospital != '') {
                    for(AggregateResult surg : [select count(id), Physician_First_Name__c, Physician_Last_Name__c 
                                                From 	ADT_Interface__c
                                                Where (Appointment_Date__c = TODAY OR Appointment_Date__c = YESTERDAY) 
                                                and Physician_First_Name__c != ''
                                                and Physician_Last_Name__c != ''
                                                and SFDC_Account__c = :whichHospital
                                                group by Physician_Last_Name__c, Physician_First_Name__c 
                                                order by Physician_Last_Name__c, Physician_First_Name__c ])
                    {
                        surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician_Last_Name__c')+'|'+string.valueOf(surg.get('Physician_First_Name__c'))),string.valueOf(surg.get('Physician_Last_Name__c'))+ ', ' + string.valueOf(surg.get('Physician_First_Name__c'))));                
                    }
                }
                else
                {
                    for(AggregateResult surg : [select count(id), Physician_First_Name__c, Physician_Last_Name__c 
                                                From    ADT_Interface__c
                                                Where (Appointment_Date__c = TODAY OR Appointment_Date__c = YESTERDAY) 
                                                and Physician_First_Name__c != ''
                                                and Physician_Last_Name__c != ''
                                                group by Physician_Last_Name__c, Physician_First_Name__c 
                                                order by Physician_Last_Name__c, Physician_First_Name__c ])
                    {
                        surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician_Last_Name__c')+'|'+string.valueOf(surg.get('Physician_First_Name__c'))),string.valueOf(surg.get('Physician_Last_Name__c'))+ ', ' + string.valueOf(surg.get('Physician_First_Name__c'))));                
                    }

                }
            }
            else
            {
            	system.debug('GET SURGEONS ELSE');
                if (whichHospital != null && whichHospital != '') {
                    for(AggregateResult surg : [select count(id), Physician_First_Name__c, Physician_Last_Name__c 
                                                From 	ADT_Interface__c
                                                Where Physician_First_Name__c != ''
                                                and Physician_Last_Name__c != ''
                                                and SFDC_Account__c = :whichHospital
                                                group by Physician_Last_Name__c, Physician_First_Name__c 
                                                order by Physician_Last_Name__c, Physician_First_Name__c ])
                    {
                        surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician_Last_Name__c')+'|'+string.valueOf(surg.get('Physician_First_Name__c'))),string.valueOf(surg.get('Physician_Last_Name__c'))+ ', ' + string.valueOf(surg.get('Physician_First_Name__c'))));                
                    }
                }
                else
                {

                    for(AggregateResult surg : [select count(id), Physician_First_Name__c, Physician_Last_Name__c 
                                                From    ADT_Interface__c
                                                Where Physician_First_Name__c != ''
                                                and Physician_Last_Name__c != ''
                                                group by Physician_Last_Name__c, Physician_First_Name__c 
                                                order by Physician_Last_Name__c, Physician_First_Name__c ])
                    {
                        surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician_Last_Name__c')+'|'+string.valueOf(surg.get('Physician_First_Name__c'))),string.valueOf(surg.get('Physician_Last_Name__c'))+ ', ' + string.valueOf(surg.get('Physician_First_Name__c'))));                
                    }
                }
            }
        }
        else
        {
            set<string> surgIds = new set<string>();
            if (whichHospital != null && whichHospital != '') {
                for(AggregateResult surg : [select count(id), Physician__c, Physician__r.Name 
    										From	event__c
                                     		where	Appointment_Start__c < TOMORROW and 
                                     				Case__r.active__c = true and 
                                           			Event_type__r.primary__c = true and
                                            		Physician__c != null and Physician__r.name != '' and
                                                    Location__c = :whichHospital
                                   			group by Physician__c,Physician__r.Name])
                {
                    surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician__c')),string.valueOf(surg.get('Name'))));
                }
            }
            else
            {
                for(AggregateResult surg : [select count(id), Physician__c, Physician__r.Name 
                                            From    event__c
                                            where   Appointment_Start__c < TOMORROW and 
                                                    Case__r.active__c = true and 
                                                    Event_type__r.primary__c = true and
                                                    Physician__c != null and Physician__r.name != ''
                                            group by Physician__c,Physician__r.Name])
                {
                    surgeonItems.add(new SelectOption(string.valueOf(surg.get('Physician__c')),string.valueOf(surg.get('Name'))));
                }
            }
        }
    }
    
    private void getCaseTypes()
    {
        caseTypeItems.clear();
        caseTypeItems.add(new SelectOption('','All'));
        set<string> casetypeIds = new set<string>();
        for(AggregateResult ct : [select count(id), case__r.case_type__c
                                    From	event__c
                                    where	Appointment_Start__c < TOMORROW and 
                                    Case__r.active__c = true and 
                                    Event_type__r.primary__c = true and
                                    case__r.case_type__c != null and case__r.case_type__r.name != ''
                                    group by case__r.case_type__c])
        {
            casetypeIds.add(string.valueOf(ct.get('case_type__c')));
        }
        if(casetypeIds.size() > 0)
        {
            for(case_type__c c : [select id, description__c from case_type__c where id in :casetypeIds])
            {
                caseTypeItems.add(new SelectOption(string.valueOf(c.id), c.description__c));
            }
        }
    }
    
    public void loadCaseMatrix()
    {
        set<string> EventIds = new set<string>();
        string query = 'select Id, case__c, Appointment_Start__c, Case__r.Patient__r.Id, Case__r.Patient__r.First_Name__c, ';
		query += 'Case__r.Patient__r.Last_Name__c, case__r.Case_Type__r.Name, event_type__r.Name, event_type__r.due_date_window__c, ';
		query += 'event_type__r.primary__c, Status__c, parent_event__c ';
        query += 'From event__c where (Appointment_Start__c < TOMORROW) and Case__r.active__c = true and Event_type__r.primary__c = true ';
        if(whichHospital != null && whichHospital != '')
            query += 'and location__c = \'' + whichHospital + '\' '; 
        if(whichSurgeon != null && whichSurgeon != '')
            query += 'and Physician__c = \'' + whichSurgeon + '\' '; 
        if(whichCaseType != null && whichCaseType != '')
            query += 'and case__r.case_type__c = \'' + whichCaseType + '\' '; 
		query += 'limit 1000';
        system.debug('Getting Case Matrix ===> ' + query);
        list<event__c> events = database.query(query);
        if(events != null && events.size() > 0)
        {
	    	Map<Id, SummaryWrapper> patSummaryMap = new Map<Id,SummaryWrapper>();
            For(event__c evt : events)
            {
                system.debug('adding Event ' + evt.Id);
                EventIds.add(evt.Id);
                string patName = evt.Case__r.Patient__r.Last_Name__c + ', ' + evt.Case__r.Patient__r.First_Name__c;
                SummaryWrapper tempWrapper = new SummaryWrapper(evt.Case__r.Patient__r.Id,patName);
                tempWrapper.CaseId = evt.Case__c;
                tempWrapper.CaseType = evt.case__r.Case_Type__r.Name;
                if(evt.Status__c != null && evt.Appointment_Start__c != null)
                {
                    tempWrapper.ProcedureDate = evt.appointment_Start__c; //.format('MM/dd/yyyy');
                    tempWrapper.ProcedureDateSort = evt.Appointment_Start__c.format('yyyyMMddhhmmss');
                    tempWrapper.ProcedureId = evt.id;
                    if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c) < datetime.now())
                    {
                        tempWrapper.ProcedureStatus = 'Overdue';
                        tempWrapper.ProcedureSortVal = 1;
                    }
                    else if(evt.Status__c == 'Pending Completion' && evt.Appointment_Start__c.format('MM/dd/yyyy') == datetime.now().format('MM/dd/yyyy'))
                    {
                        tempWrapper.ProcedureStatus = 'Pending';
                        tempWrapper.ProcedureSortVal = 2;
                    }
                    else if(evt.Status__c == 'Complete')
                    {
                        tempWrapper.ProcedureStatus = 'Complete';
                        tempWrapper.ProcedureSortVal = 4;
                    }
                    else
                    {
                        tempWrapper.ProcedureStatus = 'Future';
                        tempWrapper.ProcedureSortVal = 5;
                    }
                }
                //Ensure procedure start date, Id and Status are set when the related event 
                //contains an appointment start date but the status is null
                if(evt.Status__c == null && evt.Appointment_Start__c != null) {
                    tempWrapper.ProcedureDate = evt.appointment_Start__c; //.format('MM/dd/yyyy');
                    tempWrapper.ProcedureDateSort = evt.Appointment_Start__c.format('yyyyMMddhhmmss');
                    tempWrapper.ProcedureId = evt.id;
                    tempWrapper.ProcedureStatus = 'Future';
                    tempWrapper.ProcedureSortVal = 5;
                }
                patSummaryMap.put(evt.Id,tempWrapper);
            }
        	
            system.debug('Summary Map contains ' + patSummaryMap.size() + ' records.');
            
            list<Event__c> initPatients = [select	id, case__c,
	                                       			case__r.Case_Type__r.Name,
                                                    event_type__r.Name,
                                           			event_type__r.due_date_window__c,
                                           			event_type__r.primary__c,
                                                    appointment_Start__c,
                                                    Status__c,
                                           			parent_event__c
                                           from		event__c
                                           where	parent_event__c in: EventIds
                                           order by	Appointment_Start__c,
                                                    event_type__r.Name,
                                                    Case__r.Patient__r.Last_Name__c
                                           limit	10000];
            //System.debug('Procedure: ' + initPatients[0].Status__c + ' | ' + initPatients[0].Appointment_Start__c.Format('MM/dd/yyyy'));
            //System.debug(initPatients[0].Appointment_Start__c < datetime.now());
            for(event__c evt : initPatients)
            {
                system.debug(evt.event_type__r.Name + ' Event from ' + evt.parent_event__c + ' Procedure');
                SummaryWrapper tempWrapper = patSummaryMap.get(evt.parent_event__c);
                //if(evt.event_type__r.Name == 'Procedure')
                if(evt.event_type__r.Name == '4-8 Week Follow-up')  //SSWIGER TODO: Remove hard-coded event type names *bkl
                {
                    if(evt.Status__c != null && evt.Appointment_Start__c != null)
                    {
                        tempWrapper.EventOneDate = evt.appointment_Start__c.format('MM/dd/yyyy');
                        tempWrapper.EventOneId = evt.id;
                        if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) < datetime.now())
                        {
                            tempWrapper.EventOneStatus = 'Overdue';
                            tempWrapper.EventOneSortVal = 1;
                        }
                        else if(evt.Status__c == 'Pending Completion' && 
                                (dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) >= datetime.now() &&
                                dateTime.valueOf(evt.Appointment_Start__c) <= datetime.now()))
                        {
                            tempWrapper.EventOneStatus = 'Pending';
                            tempWrapper.EventOneSortVal = 2;
                        }
                        else if(evt.Status__c == 'Complete')
                        {
                            tempWrapper.EventOneStatus = 'Complete';
                            tempWrapper.EventOneSortVal = 4;
                        }
                        else
                        {
                            tempWrapper.EventOneStatus = 'Future';
                            tempWrapper.EventOneSortVal = 5;
                        }
                    }
                }
                else if(evt.event_type__r.Name == '6 Month Follow-up')
                {
                    if(evt.Status__c != null && evt.Appointment_Start__c != null)
                    {
                        tempWrapper.EventTwoDate = evt.appointment_Start__c.format('MM/dd/yyyy');
                        tempWrapper.EventTwoId = evt.id;
                        if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) < datetime.now())
                        {
                            tempWrapper.EventTwoStatus = 'Overdue';
                            tempWrapper.EventTwoSortVal = 1;
                        }
                        else if(evt.Status__c == 'Pending Completion' && 
                                (dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) >= datetime.now() &&
                                dateTime.valueOf(evt.Appointment_Start__c) <= datetime.now()))
                        {
                            tempWrapper.EventTwoStatus = 'Pending';
                            tempWrapper.EventTwoSortVal = 2;
                        }
                        else if(evt.Status__c == 'Complete')
                        {
                            tempWrapper.EventTwoStatus = 'Complete';
                            tempWrapper.EventTwoSortVal = 4;
                        }
                        else
                        {
                            tempWrapper.EventTwoStatus = 'Future';
                            tempWrapper.EventTwoSortVal = 5;
                        }
                    }
                }
                else if(evt.event_type__r.Name == '12 Month Follow-up')
                {
                    if(evt.Status__c != null && evt.Appointment_Start__c != null)
                    {
                        tempWrapper.EventThreeDate = evt.appointment_Start__c.format('MM/dd/yyyy');
                        tempWrapper.EventThreeId = evt.id;
                        if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) < datetime.now())
                        {
                            tempWrapper.EventThreeStatus = 'Overdue';
                            tempWrapper.EventThreeSortVal = 1;
                        }
                        else if(evt.Status__c == 'Pending Completion' && 
                                (dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) >= datetime.now() &&
                                dateTime.valueOf(evt.Appointment_Start__c) <= datetime.now()))
                        {
                            tempWrapper.EventThreeStatus = 'Pending';
                            tempWrapper.EventThreeSortVal = 2;
                        }
                        else if(evt.Status__c == 'Complete')
                        {
                            tempWrapper.EventThreeStatus = 'Complete';
                            tempWrapper.EventThreeSortVal = 4;
                        }
                        else
                        {
                            tempWrapper.EventThreeStatus = 'Future';
                            tempWrapper.EventThreeSortVal = 5;
                        }
                    }
                }
                else if(evt.event_type__r.Name == '2 Year Follow-up')
                {
                    if(evt.Status__c != null && evt.Appointment_Start__c != null)
                    {
                        tempWrapper.EventFourDate = evt.appointment_Start__c.format('MM/dd/yyyy');
                        tempWrapper.EventFourId = evt.id;
                        if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) < datetime.now())
                        {
                            tempWrapper.EventFourStatus = 'Overdue';
                            tempWrapper.EventFourSortVal = 1;
                        }
                        else if(evt.Status__c == 'Pending Completion' && 
                                (dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) >= datetime.now() &&
                                dateTime.valueOf(evt.Appointment_Start__c) <= datetime.now()))
                        {
                            tempWrapper.EventFourStatus = 'Pending';
                            tempWrapper.EventFourSortVal = 2;
                        }
                        else if(evt.Status__c == 'Complete')
                        {
                            tempWrapper.EventFourStatus = 'Complete';
                            tempWrapper.EventFourSortVal = 4;
                        }
                        else
                        {
                            tempWrapper.EventFourStatus = 'Future';
                            tempWrapper.EventFourSortVal = 5;
                        }
                    }
                }
                else if(evt.event_type__r.Name == '4 Year Follow-up')
                {
                    
                    if(evt.Status__c != null && evt.Appointment_Start__c != null)
                    {
                        tempWrapper.EventFiveDate = evt.appointment_Start__c.format('MM/dd/yyyy');
                        tempWrapper.EventFiveId = evt.id;
                        if(evt.Status__c == 'Pending Completion' && dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) < datetime.now())
                        {
                            tempWrapper.EventFiveStatus = 'Overdue';
                            tempWrapper.EventFiveSortVal = 1;
                        }
                        else if(evt.Status__c == 'Pending Completion' && 
                                (dateTime.valueOf(evt.Appointment_Start__c).addDays(integer.valueOf(evt.event_type__r.due_date_window__c)) >= datetime.now() &&
                                dateTime.valueOf(evt.Appointment_Start__c) <= datetime.now()))
                        {
                            tempWrapper.EventFiveStatus = 'Pending';
                            tempWrapper.EventFiveSortVal = 2;
                        }
                        else if(evt.Status__c == 'Complete')
                        {
                            tempWrapper.EventFiveStatus = 'Complete';
                            tempWrapper.EventFiveSortVal = 4;
                        }
                        else
                        {
                            tempWrapper.EventFiveStatus = 'Future';
                            tempWrapper.EventFiveSortVal = 5;
                        }
                    }
                }

                patSummaryMap.put(evt.parent_event__c,tempWrapper);
            }
            Summaries = patSummaryMap.values();
        }
    }
    
    // wrapper class to hold aggregate data
    public class SummaryWrapper {
    	public Id caseId { get; set; }
    	public Id patientId { get; set; }
    	public String PatientName {get; set;}
    	public String CaseType {get; set;}
    	public Datetime ProcedureDate {get; set;}
        public String ProcedureDateSort {get; set;}
        public String ProcedureId {get; set;}
        public String ProcedureStatus {get; set;}
        public integer ProcedureSortVal {get; set;}
        public String EventOneDate {get; set;}
        public String EventOneId {get; set;}
        public String EventOneStatus {get; set;}
        public integer EventOneSortVal {get; set;}
        public String EventTwoDate {get; set;}
        public String EventTwoId {get; set;}
        public String EventTwoStatus {get; set;}
        public integer EventTwoSortVal {get; set;}
        public String EventThreeDate {get; set;}
        public String EventThreeId {get; set;}
        public String EventThreeStatus {get; set;}
        public integer EventThreeSortVal {get; set;}
        public String EventFourDate {get; set;}
        public String EventFourId {get; set;}
        public String EventFourStatus {get; set;}
        public integer EventFourSortVal {get; set;}
        public String EventFiveDate {get; set;}
        public String EventFiveId {get; set;}
        public String EventFiveStatus {get; set;}
        public integer EventFiveSortVal {get; set;}
    
		/** Constructor **/
        public SummaryWrapper(Id patId, string patName)
        {
            this.patientId = patId;
            this.patientName = patName;
            this.CaseType = '';
            this.ProcedureDate = datetime.newInstance(2099,1,1);
            this.ProcedureId = '000000000000000000';
            this.ProcedureStatus = 'Future';
            this.ProcedureSortVal = 5;
            this.EventOneDate = '1/1/2099';
            this.EventOneId = '000000000000000000';
            this.EventOneStatus = 'Future';
            this.EventOneSortVal = 5;
            this.EventTwoDate = '1/1/2099';
            this.EventTwoId = '000000000000000000';
            this.EventTwoStatus = 'Future';
            this.EventTwoSortVal = 5;
            this.EventThreeDate = '1/1/2099';
            this.EventThreeId = '000000000000000000';
            this.EventThreeStatus = 'Future';
            this.EventThreeSortVal = 5;
            this.EventFourDate = '1/1/2099';
            this.EventFourId = '000000000000000000';
            this.EventFourStatus = 'Future';
            this.EventFourSortVal = 5;
            this.EventFiveDate = '1/1/2099';
            this.EventFiveId = '000000000000000000';
            this.EventFiveStatus = 'Future';
            this.EventFiveSortVal = 5;
        }
        
    }   
    
    public void loadPatientMatrix()
    {
        //system.debug('whichSurgeon: ' + whichSurgeon);
        string query = 'Select Id, Patient_Last_Name__c, Patient_First_Name__c, Patient_Date_Of_Birth__c , Patient_Gender__c, Physician_Last_Name__c, ';
        query += 'Physician_First_Name__c, Procedures_Description__c, SFDC_Account__r.Name, Appointment_Date__c, event__c, event__r.Laterality_For_Forms__c, event__r.Case__r.Case_type__r.Description__c,  event__r.Appointment_Start__c, event__r.createddate, event__r.location__c, event__r.physician__c, ';
        query += 'Event__r.Physician__r.FirstName,Event__r.Physician__r.LastName,Event2__r.Physician__r.FirstName,Event2__r.Physician__r.LastName,';
        query += 'event2__c, event2__r.Laterality_For_Forms__c, event2__r.Case__r.Case_type__r.Description__c,  event2__r.Appointment_Start__c, event2__r.createddate, event2__r.location__c, event2__r.physician__c ';
        query += 'From ADT_Interface__c Where ';
        if(!showAll) {
            query += '(Appointment_Date__c = TODAY OR Appointment_Date__c = YESTERDAY) ';
        }
        else
        {
            query += 'Appointment_Date__c != null ';
        }
        if(whichHospital != null && whichHospital != '')
            query += 'AND SFDC_Account__c = \'' + whichHospital + '\'';
        if(whichSurgeon != null && whichSurgeon != '')
        {
            query += 'AND Physician_Last_Name__c = \'' + whichSurgeon.split('[|]')[0] + '\'';
            query += ' AND Physician_First_Name__c = \'' + whichSurgeon.split('[|]')[1] + '\'';
        }
        query += 'AND SFDC_Account__c != null ';
        system.debug('hospitalIDs ==== ' + hospitalIDs);
        if(hospitalIDs.size()>0) query += 'AND SFDC_Account__c IN ' + hospitalIDs;
        query += ' LIMIT 1000';
        system.debug('Getting Patient Matrix ===> ' + query);
		ScheduledPatients = database.query(query);
		
		//If the ADT record has a related Procedure and the Procedure's Start Date is before yesterday, remove that ADT record from the list regardless of the Appointment Date on the ADT record itself.
		for(Integer i = 0 ; i < ScheduledPatients.size() ; i++)
		{
			if(ScheduledPatients[i].event__c!=null && ScheduledPatients[i].event__r.Appointment_Start__c !=null && ScheduledPatients[i].event__r.Appointment_Start__c < system.today()-1)
			{
				ScheduledPatients.remove(i);
			}
		}

		ptLines.clear();
		for(ADT_Interface__c pat : ScheduledPatients)
		{
			system.debug('pat.event__c ==== ' + pat.event__c);
		
			PatientTableLine ptl = new PatientTableLine();
			ptl.PatientId = pat.Id;
    		ptl.EventId = pat.event__c;
    		if(pat.event__c!=null)
    		{
    			 ptl.EventDate = pat.event__r.createdDate;
    			 system.debug('pat.event__r.createdDate ==== ' + pat.event__r.createdDate);
    		}
    		ptl.EventLocationId = pat.event__r.location__c;
    		ptl.EventPhysicianId = pat.event__r.physician__c;
    		ptl.PatientLastName = pat.Patient_Last_Name__c;
    		ptl.PatientFirstName = pat.Patient_First_Name__c;
    		ptl.PatientdateOfBirth = pat.Patient_Date_Of_Birth__c;
    		ptl.PatientGender = pat.Patient_Gender__c;
    		ptl.PhysicianLastName = pat.Physician_First_Name__c;
    		ptl.PhysicianFirstName = pat.Physician_Last_Name__c;
    		if(pat.Event__r.Physician__c!=null)
    		{
    			ptl.PhysicianLastName = pat.Event__r.Physician__r.LastName;
    			ptl.PhysicianFirstName = pat.Event__r.Physician__r.FirstName;
    		}
    		ptl.AppointmentDate = pat.Appointment_Date__c;
    		ptl.PatientAccountName = pat.SFDC_Account__r.Name;
    		ptl.Laterality = pat.event__r.Laterality_For_Forms__c;
    		ptl.CaseType = pat.event__r.Case__r.Case_type__r.Description__c;
    		ptLines.add(ptl);
    		if(pat.Event2__c!=null)
    		{
				PatientTableLine ptl3 = new PatientTableLine();
				ptl3.PatientId = pat.Id;
	    		ptl3.EventId = pat.event2__c;
	    		if(pat.event__c!=null)
	    		{
	    			 ptl3.EventDate = pat.event2__r.createdDate;
	    			 system.debug('pat.event__r.createdDate ==== ' + pat.event2__r.createdDate);
	    		}
	    		ptl3.EventLocationId = pat.event2__r.location__c;
	    		ptl3.EventPhysicianId = pat.event2__r.physician__c;
	    		ptl3.PatientLastName = pat.Patient_Last_Name__c;
	    		ptl3.PatientFirstName = pat.Patient_First_Name__c;
	    		ptl3.PatientdateOfBirth = pat.Patient_Date_Of_Birth__c;
	    		ptl3.PatientGender = pat.Patient_Gender__c;
	    		ptl3.PhysicianLastName = pat.Physician_First_Name__c;
	    		ptl3.PhysicianFirstName = pat.Physician_Last_Name__c;
	    		if(pat.Event2__r.Physician__c!=null)
	    		{
	    			ptl3.PhysicianLastName = pat.Event2__r.Physician__r.LastName;
	    			ptl3.PhysicianFirstName = pat.Event2__r.Physician__r.FirstName;
	    		}
	    		ptl3.AppointmentDate = pat.Appointment_Date__c;
	    		ptl3.PatientAccountName = pat.SFDC_Account__r.Name;
	    		ptl3.Laterality = pat.event2__r.Laterality_For_Forms__c;
	    		ptl3.CaseType = pat.event2__r.Case__r.Case_type__r.Description__c;
	    		ptLines.add(ptl3);
    		}
    		if(ptl.EventDate!=null && ptl.EventDate <= system.now() && ptl.EventDate >= system.now().addDays(-2))
    		{
				PatientTableLine ptl2 = new PatientTableLine();
				ptl2.PatientId = pat.Id;
	    		ptl2.EventId = '';
	    		ptl2.EventLocationId = pat.event__r.location__c;
	    		ptl2.EventPhysicianId = pat.event__r.physician__c;
	    		ptl2.PatientLastName = pat.Patient_Last_Name__c;
	    		ptl2.PatientFirstName = pat.Patient_First_Name__c;
	    		ptl2.PatientdateOfBirth = pat.Patient_Date_Of_Birth__c;
	    		ptl2.PatientGender = pat.Patient_Gender__c;
	    		ptl2.PhysicianLastName = pat.Physician_First_Name__c;
	    		ptl2.PhysicianFirstName = pat.Physician_Last_Name__c;
	    		if(pat.Event__r.Physician__c!=null)
	    		{
	    			ptl2.PhysicianLastName = pat.Event__r.Physician__r.LastName;
	    			ptl2.PhysicianFirstName = pat.Event__r.Physician__r.FirstName;
	    		}
	    		ptl2.AppointmentDate = pat.Appointment_Date__c;
	    		ptl2.PatientAccountName = pat.SFDC_Account__r.Name;
	    		ptl2.Laterality = '';
	    		ptl2.CaseType = '';
	    		
	    		ptLines.add(ptl2);
    		}
    		
		}
    }
    
    public pageReference loadDetails()
    {    	
        hospitalItems = new list<SelectOption>();
        surgeonItems = new list<SelectOption>();
    	caseTypeItems = new list<SelectOption>();
        whichHospital = '';
        whichSurgeon = '';
        whichCaseType = '';
        showAll = false;

        if(whichView == '2')
        {
            getHospitals();
            getSurgeons();
            loadPatientMatrix();
        }
        else
        {   
            getHospitals();
            getSurgeons();
        	getCaseTypes();
            loadCaseMatrix();
        }
            
        return null;
    }

    public pageReference filterDetails()
    {
        if(whichView == '2')
        {
            getHospitals();
            getSurgeons();
            loadPatientMatrix();
        }
        else
        {   
            getHospitals();
            getSurgeons();
            getCaseTypes();
            loadCaseMatrix();
        }
        return null;
    }

	public PageReference GoToCaseSelector()
    {		
		PageReference pageRef = new PageReference('/apex/CaseSelector');
        return pageRef;
    }
    
    public class PatientTableLine{
    	
    	public String PatientId {get;set;}
    	public String EventId {get;set;}
    	public Datetime EventDate {get;set;}
    	public String EventLocationId {get;set;}
    	public String EventPhysicianId {get;set;}
    	public String PatientLastName {get;set;}
    	public String PatientFirstName {get;set;}
    	public Date PatientDateOfBirth {get;set;}
    	public String PatientGender {get;set;}
    	public Date AppointmentDate {get;set;}
    	public String PatientAccountName {get;set;}
    	public String PhysicianLastName {get;set;}
    	public String PhysicianFirstName {get;set;}
    	public String Laterality {get;set;}
    	public String CaseType {get;set;}
    	
    	public PatientTableLine(){
    		PatientId = '';
    		EventId = '';
    		EventDate = null;
    		EventLocationId = '';
    		EventPhysicianId = '';
    		PatientLastName = '';
    		PatientFirstName = '';
    		PatientDateOfBirth = system.today();
    		PatientGender = '';
    		AppointmentDate = system.today();
    		PatientAccountName = '';
    		PhysicianLastName = '';
    		PhysicianFirstName = '';
    		Laterality = '';
    		CaseType = '';
    	}
    }
}