@isTest(SeeAllData=true)
private class SCMFormTesting {

    static testMethod void testPackslipPDF() {
       SCMTestSetup testClass = new SCMTestSetup();
       SCMC__Org_Address__c org = testClass.createOrg();
       
       SCMC__Currency_Master__c curr = testClass.createTestCurrency();
       Account custAccount = testClass.createAccount();  
       Contact newContact = testClass.createContact(true, custAccount, 'A/P Contact');
       
       SCMC__Sales_Order__c so = testClass.createTestSalesOrder(custAccount, false);
       so.Carrier__c = 'Company Truck';
       so.Carrier_Account__c = '1234566';
       so.Carrier_Service__c = 'Ground LTL';
       so.Freight_Payee__c = 'Customer';
       upsert so;
       
       SCMC__Item__c item = testClass.createTestItem();
       SCMC__Inventory_Position__c ip1 = testClass.setupPartsInInventory(item, true);
       SCMC__Sales_Order_Line_Item__c sol1 = testClass.createSalesOrderLine(so, item, '', true);       
        
       ApexPages.StandardController stdController = new ApexPages.StandardController(so);
       SCMC.SalesOrderControllerExtension soController = new SCMC.SalesOrderControllerExtension(stdController);
       PageReference allocate = soController.allocate();
       so.SCMC__Status__c = 'Approved';
       update so;
        
       list<SCMC__Picklist__c> picklists = [Select id
                                              from SCMC__Picklist__c
                                             where SCMC__Sales_Order__c = :so.Id];
        
       system.assertNotEquals(0, picklists.size(), 'There is not a picklist for this sales order');
       SCMC__Picklist__c testPicklist = picklists[0];
                
       test.startTest();
                
        ApexPages.StandardController stdController2 = new ApexPages.StandardController(testPicklist);
        SCMC.PicklistControllerExtension controllerExtension = new SCMC.PicklistControllerExtension(stdController2);
        SCMC.PackslipController packSlipController = new SCMC.PackslipController();

		packSlipController.controller = controllerExtension;
		
		SCMPackslipControllerExtension ext = new SCMPackslipControllerExtension(packSlipController);
		ext.picklistExtendId = testPicklist.Id;
		
		system.assertNotEquals(null, ext.Carrier);

       test.stopTest();       
    }

    static testMethod void picklistFormTest() {
    	
    	SCMSetUpTest testClass = new SCMSetUpTest();
    	Account acc = testClass.createTestCustomerAccount(true, true, false, null);
    	SCMC__Sales_Order__c so = testClass.createTestSalesOrder(acc, false);
    	so.Carrier__c = 'Company Truck';
    	so.Carrier_Account__c = '1234566';
    	so.Carrier_Service__c = 'Ground LTL';
    	so.Freight_Payee__c = 'Customer';
    	upsert so;
    	
		SCMC__Picklist__c soPicklist = new SCMC__Picklist__c();
        soPicklist.SCMC__Sales_Order__c = so.Id;
        soPicklist.SCMC__Status__c = 'New';
        upsert soPicklist;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(soPicklist);
        SCMC.PicklistControllerExtension controllerExtension = new SCMC.PicklistControllerExtension(stdController);
		SCMC.PicklistController picklistController = new SCMC.PicklistController();
		
		SCMPicklistControllerExtension ext = new SCMPicklistControllerExtension(picklistController);
		ext.picklistExtendId = soPicklist.Id;
		
		system.assertEquals('SalesOrder', ext.formType, 'Not the correct from type, should be Sales Order.');

		//Transfer Request 
		SetUpTestForSCM testClass2 = new SetUpTestForSCM();
		SCMC__Warehouse__c warehouseA = testClass2.setupWarehouse();

    SCMC__Warehouse__c warehouseB = new SCMC__Warehouse__c();
    warehouseB.SCMC__ICP__c = warehouseA.SCMC__ICP__c;
    warehouseB.SCMC__Address__c = warehouseA.SCMC__Address__c;
    warehouseB.name = 'Test Warehouse ABCDEF';
    insert warehouseB;


		SCMC__Reason_Code__c reason = new SCMC__Reason_Code__c();
		reason.Name = 'Test';
		reason.SCMC__Inactive__c = false;
		upsert reason; 
		
		SCMC__Transfer_Request__c tr = new SCMC__Transfer_Request__c();
		tr.RecordTypeId = SCMUtilities.getRecordType('Transfer_Request', tr);
		tr.SCMC__Carrier__c = 'Company Truck';
		tr.SCMC__Carrier__c = 'Ground LTL';
		tr.SCMC__Destination_Warehouse__c = warehouseB.Id;
		tr.SCMC__Source_Warehouse__c = warehouseA.Id;
		tr.SCMC__Reason_Code__c = reason.Id;
		tr.SCMC__Status__c = 'New';
		tr.SCMC__Shipment_Required__c = true;
		upsert tr;
		SCMC__Picklist__c trPicklist = new SCMC__Picklist__c();
        trPicklist.SCMC__Transfer_Request__c = tr.Id;
        trPicklist.SCMC__Status__c = 'New';
        upsert trPicklist;

        ApexPages.StandardController stdController2 = new ApexPages.StandardController(trPicklist);
        SCMC.PicklistControllerExtension controllerExtension2 = new SCMC.PicklistControllerExtension(stdController2);
		SCMPicklistControllerExtension ext2 = new SCMPicklistControllerExtension(picklistController);
		ext2.picklistExtendId = trPicklist.Id;

		system.assertEquals('TransferRequest', ext2.formType, 'Not the correct from type, should be Sales Order.');

    }

}