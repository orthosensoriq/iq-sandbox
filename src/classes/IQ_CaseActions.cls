global without sharing class IQ_CaseActions {
    
    private IQ_PatientActions ctrl;
    
    public IQ_CaseActions(IQ_PatientActions controllerParam) {
        ctrl = controllerParam;
    }
    
	@RemoteAction
    global static String AddSensor(String barcode, String procedureId){
        // deprecated and changed to code in this class
    	//ProcedureSummaryController controller = new ProcedureSummaryController(procedureId);
        //controller.sensorBoxCode = barcode;
        //controller.ParseSensorBoxBarcode(); 
        string result = ProcessSensorBoxBarcode(barCode, procedureId);
        return result;

    }
    
    @RemoteAction
    global static void AddSensorManually(String ref, String man, String sn, String lot, String expMonths, String expYears, String procedureId){
       
        OS_Device__c device = new OS_Device__c();
        device.Style__c = ref.substring(4);
        device.Device_ID__c = sn;
        device.Manufacturer_Name__c = man;
        device.Lot_Number__c = lot;
        device.Year_Assembled__c = '--';
        if(expMonths.length()>0 && expYears.length()>0){
            device.Expiration_Date__c = Date.parse(expMonths + '/' + '01' + '/' +  expYears);
        }
        boolean deviceExists = false;
        
        List<OS_Device__c> currDevices = [Select Device_ID__c from OS_Device__c WHERE Id IN (Select OS_Device__c from OS_Device_Event__c WHERE Event__c =:procedureID) AND IsDeleted__c = false AND Device_ID__c = :sn];
        if(currDevices.size()>0) deviceExists = true;
        
        if (deviceExists){
            return;
            System.debug('The Device information you are attempting to add already exists for this procedure');
            
        }    

         // Do not insert a duplicate Device or Device Event
        if (!deviceExists) {
            
            insert device;
            
            OS_Device_Event__c de = new OS_Device_Event__c();
            de.os_device__c = device.id;
            de.event__c = ProcedureID;
            
            insert de;
        }
	}

    @RemoteAction
    public static OS_Device__c ParseSensorBoxBarcode(string sensorBoxCode){
            
        if(sensorBoxCode!=null && sensorBoxCode!='')
        {
            try
            {
                List<String> deviceData = sensorBoxCode.split(',');
                
                String manu = 'None Specified';
                if(deviceData[1].substring(0,3) == 'SYK')
                {
                    manu = 'Stryker';
                }
                else if (deviceData[1].substring(0,3) == 'BMT')
                {
                    manu = 'Biomet'; 
                }
                else if (deviceData[1].substring(0,3) == 'ZMR')
                {
                    manu = 'Zimmer'; 
                }
                else if (deviceData[1].substring(0,3) == 'SNN')
                {
                    manu = 'Smith and Nephew'; 
                }
                
                List<String> assemblyDate = deviceData[3].split('-');
                String[] expDate = deviceData[4].split('-');
                String expirationdate = deviceData[4];
                
                //Handles different date formats for the expiration date
                //YYYY-MM
                if(expDate.size()<3 && expDate[0].length() == 4) expirationdate = expDate[1] + '-01-' + expDate[0];
                //MM-YYYY
                else if(expDate.size()<3 && expDate[0].length() == 2) expirationdate = expDate[0] + '-01-' + expDate[1];
                //MM-DD-YYYY
                else if(expDate.size()==3 && expDate[2].length() == 4) expirationdate = expDate[0] + '-01-' + expDate[2];
                //All other date formats are invalid
                else
                {
                    //ApexPages.addMessages(new CommonException('Invalid Bar Code'));
                    //handle error
                    //return null;
                }
                Date expiry = setStringToDateFormat(expirationdate);
                
                OS_Device__c device = new OS_Device__c();
                device.Style__c = deviceData[1].split('-')[1];
                device.Device_ID__c = deviceData[0];
                device.Manufacturer_Name__c = manu;
                device.Lot_Number__c = deviceData[2];
                device.Year_Assembled__c = assemblyDate[2];
                device.Expiration_Date__c = expiry;
                return device;
                
                //RenderOSDevice();
            }
            catch(Exception e){
                //ApexPages.addMessages(new CommonException('Invalid Bar Code'));
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error Detail: ' + e.getMessage() + ' row: ' + e.getLineNumber());
                //ApexPages.addMessage(myMsg);
            }
            return null;
        }
        
        return null;
    }
    
    @RemoteAction 
    global static String ProcessSensorBoxBarcode(string sensorBoxCode, string procedureId){
        string result;
        List<String> deviceData = sensorBoxCode.split(',');
        OS_Device__c device;
        if (sensorBoxCode!=null && sensorBoxCode!='') {
           if (deviceData.size() > 1) {
                device = ParseSensorBoxBarcode(sensorBoxCode);
            } else {
                device = ParseG1SensorBoxBarcode(sensorBoxCode);
            }
            System.debug(device);
            if (device == null) {
                return 'Invalid bar code!';
            } else {
               //does the record exist?
                boolean exists = DeviceExists(device, procedureId);
                if (exists) {
                    return 'Device already exists in system';////ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'The Device information you are attempting to add already exists for this procedure'));
                } else {
                    SaveOSDevice(device, procedureId);
                    return 'Successful';
                }
            }
        } else {
            return 'Empty bar code!';
        }
    }   
    
    @RemoteAction
    public static OS_Device__c ParseG1SensorBoxBarcode(string sensorBoxCode){
            //             58 characters
            // 0100816818020013  11     161017  17      181017      10  092016V15474    21      215001017
            // 0100816818020228 02911   161012  02917   181012  029 10  092616v15451    02921   315008241 
            // 0100816818020938 11      170330  17      190330      10  20082           02921   237015171
            // 0100816818021256 11      170411  17      190411      10  32374aal        21      323015061
        System.debug(sensorBoxCode);
        if (sensorBoxCode != null && sensorBoxCode != '') {
            try
            {
                Integer barcodeLength = sensorBoxCode.length();
                System.debug(barcodeLength);
                Integer gtinStart = 2;
                Integer gtinEnd = 16;
                Integer manuDateStart = 18;
                Integer manuDateEnd = 24;
                Integer expiryDateStart = 26;
                Integer expiryDateEnd = 32;
                Integer lotStart = 34;
                Integer lotEnd = 46;
                Integer deviceIdStart = 48;
                Integer deviceIdEnd = 57;
                if (barCodeLength == 58) {

                } else if (barCodeLength == 69) {
                    manuDateStart = 21;
                    manuDateEnd = 27;
                    expiryDateStart = 32;
                    expiryDateEnd = 38;
                    lotStart = 43;
                    lotEnd = 55;
                    deviceIdStart = 60;
                    deviceIdEnd = 69;
                } else if (barCodeLength == 54) {
                    if (sensorBoxCode.substring(lotStart).contains('02921')) {
                        lotEnd = 43;
                    } 
                } 

                // Broken down example:
                // 01 (Separator - Ignore)
                string gtin = sensorBoxCode.substring(gtinStart, gtinEnd);
                // 00816818020013 (CHARACTER 3-16 = GTIN)
                // 11 (Separator - Ignore)
                // 161017 (CHARACTER 19-24 = MANUFACTURE DATE)
                string manuDateString = sensorBoxCode.substring(manuDateStart, manuDateEnd);
                System.debug(manuDateString);
                Date manuDate = SetStringToDateForG1(manuDateString);
                System.debug(manuDate);
                if (manuDate ==  null) {
                    return null;
                }
                    //System.debug(manuDate);
                // 17 (Separator - Ignore)
                // 181017 (CHARACTER 27-32 = EXPIRY DATE)
                string expiryDateString = sensorBoxCode.substring(expiryDateStart, expiryDateEnd);
                System.debug(expiryDateString);
                Date expiryDate = SetStringToDateForG1(expiryDateString);
                if (expiryDate == null) {
                    return null;
                }
                System.debug(expiryDate);
                // 10 (Separator - Ignore)
                // 092016V15474 (CHARACTER 35-46 = LOT)
                string lot = sensorBoxCode.substring(lotStart, lotEnd);
                //System.debug(lot);
                // 21 (Separator - Ignore)
                // 215001017 (49-57 SERIAL/DEVICEID)
                string deviceId = sensorBoxCode.substring(deviceIdStart, deviceIdEnd);
                //System.debug(deviceId);
                // Add GTIN column to OS_Sensor_Ref__c
                OS_Device_Ref__c osDeviceRef = GetOSDeviceSensorByGtin(gtin);
                if (osDeviceRef == null) {
                    return null;
                }
                System.debug(osDeviceRef);
                // write to db
                OS_Device__c device = new OS_Device__c();
                device.Device_ID__c = deviceId;
                device.Manufacturer_Name__c = osDeviceRef.Manufacturer__c; 
                device.Style__c = osDeviceRef.Name;
                device.Lot_Number__c = lot;
                device.Year_Assembled__c = string.valueOf(manuDate.year());
                device.Expiration_Date__c = expiryDate;
                System.debug(device);
                return device;
            }
            catch(Exception e){
                return null;
            }
        }
        return null; //need to see if this can be reached
    }

    public static boolean DeviceExists(OS_Device__c device, string procedureId) {
        boolean deviceExists = false;
        System.debug(device);
        try {
        List<OS_Device__c> currDevices = [Select Device_ID__c from OS_Device__c 
                                          WHERE Id IN 
                                          (Select OS_Device__c from OS_Device_Event__c 
                                           WHERE Event__c =:procedureID) AND IsDeleted__c = false AND Device_ID__c = :device.Id];
        
            //List<OS_Device_Event__c> currDevices = [Select Id from OS_Device_Event__c 
            //                                   WHERE OS_Device__r.Device_ID__c = :device.Device_ID__c];

            System.debug(currDevices);                                       
            deviceExists = currDevices.size()>0;
            System.debug(deviceExists);
        return deviceExists;
        } catch(Exception e) {
            System.debug(e);
            return false;
        }        
    }

    public static void SaveOSDevice(OS_Device__c device, string procedureId) {
                    
        insert device;
        System.debug('device is saved');

        OS_Device_Event__c de = new OS_Device_Event__c();
        de.os_device__c = device.id;
        de.event__c = procedureId;
                    
        insert de;  
        System.debug('device event is saved');
    }

    public static OS_Device_Ref__c GetOSDeviceSensorByGtin(string gtin) {
        System.debug(gtin);
        List<OS_Device_Ref__c> deviceRefs = [Select ID, Name, Manufacturer__c, GTIN_Number__c
                                            From OS_Device_Ref__c Where GTIN_Number__c = :gtin];
        if (deviceRefs.size() > 0) {
            return deviceRefs[0];
        } else {
            return null;
        }
    }

    // may want to put this in a util lib
    private static Date setStringToDateFormat(String myDate) {
        try {
            String[] myDateOnly = myDate.split(' ');
            String[] strDate = myDateOnly[0].split('-');
            Integer myIntDate = integer.valueOf(strDate[1]);
            Integer myIntMonth = integer.valueOf(strDate[0]);
            Integer myIntYear = integer.valueOf(strDate[2]);
            Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
            return d;
         } 
        catch (Exception e) 
        {
            //bad date
            return null;
        }          
    }

    global static Date SetStringToDateForG1(string gDate) {
        System.debug(gDate);
        System.debug(gDate.substring(0,2));
        //System.debug(gDate.substring(2,4));
        //System.debug(gDate.substring(4,5));
        try {
            Integer myIntMonth = integer.valueOf(gDate.substring(2,4));
            Integer myIntDate = integer.valueOf(gDate.substring(4));
            Integer myIntYear = integer.valueOf('20' + gDate.substring(0,2));
            Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
            System.debug(d);
            return d;
        } 
        catch (Exception e) 
        {
            //bad date
            return null;
        }          
    }

    @RemoteAction
    global static List<Event__c> GetCaseEvents(string caseId) {
        List<Event__c> events = [SELECT ID, Event_Type_Name__c, case__r.Id from Event__c where case__r.Id =: caseId];
        return events;
    }

    ////////stopped here
    
    @RemoteAction
    global static List<Event_Form__c> GetSurveyCompletionDates(string caseId) {
        List<Event__c> events = GetCaseEvents(caseId);
        List<Event_Form__c> eventForms = 
                    [Select Id, Name, End_Date__c, Event__r.Id, Event__r.Event_Type_Name__c, Survey__r.Id, Survey__r.Name__c, LastModifiedDate 
                    from Event_Form__c 
                    Where Event__r.ID in :events];
        System.debug(eventForms);
        return eventForms;
    }

    /// we may not need this - if we can get completion dates from Event_Forms
    global class EventSurveyResponse {
        public string EventId;
        public string EventName;
        public Datetime SurveyCompleteDate;
        public string SurveyName;
        public string SurveyId;
    }

    // CreateFollowUpEvents is in IT-PatientActions - this is intended to fix cases where additional actions have been added
    // @RemoteAction
    // global static void CreateMissingFollowUpEvents(String CaseID, String ProcedureID, String CaseTypeID)
    // {
    //     List<Event_Type__c> eventTypes = new List<Event_Type__c>();
    //     eventTypes = [Select Id, Interval__c from Event_Type__c WHERE Interval__c!=null AND Primary__c=false AND Id IN (Select Event_Type__c from Case_Type_Event_Type__c WHERE Case_Type__c =:CaseTypeID)];
    //     //eventTypes = [Select Id, Interval__c from Event_Type__c Where Interval__c!=null AND Primary__c=false];
    //     Event__c p = [Select Id, Appointment_start__c, Location__c, Physician__c from Event__c WHERE Id=: ProcedureID];
        
    //     List<Event__c> eventsToAdd = new List<Event__c>();
        
    //     for(Event_Type__c et: eventTypes)
    //     {
    //         //check to see if the event alread exists
    //         if ([Select Id, ]) ////// not complete!!!!!   //////
    //         Event__c fuEvent = new Event__c();
    //         fuEvent.Event_Type__c = et.id; 
    //         fuEvent.Case__c = CaseID;
    //         fuEvent.Appointment_Start__c = p.Appointment_Start__c + (et.Interval__c * 7);
    //         fuEvent.Location__c = p.Location__c;
    //         fuEvent.Physician__c = p.Physician__c;
    //         fuEvent.Parent_Event__c = p.id;
    //         eventsToAdd.add(fuEvent);
    //     }
        
    //     if(eventsToAdd.size()>0) insert eventsToAdd;  
    // }


}