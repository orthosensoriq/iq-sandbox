@isTest(SeeAllData=true)
public class IQ_SurveyRepositoryTest {
	
    @isTest static void IQ_SurveyRepositoryTest() {
        IQ_PatientActions actions = new IQ_PatientActions();
        IQ_SurveyRepository repo = new IQ_SurveyRepository(actions);
        
    }
    @isTest static void GetCaseTypeIdByCaseIdTest() {
        Test.startTest();
        Case__c c = CreateTestCase('Test');
        String caseType = IQ_SurveyRepository.GetCaseTypeIdByCaseId(c.Id);
        System.assert(caseType != null);
        Test.stopTest();
    }

    @isTest static void LoadSurveysFromCaseTypeTest() {
        Test.startTest();
        string caseName = 'Test Case';
        Account acct = IQ_SurveyRepository.CreateHospital('LAJDS Hospital');
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Event Type Name');
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        Event_Type_Survey__c eventTypeSurvey = IQ_SurveyRepository.CreateEventTypeSurvey(eventType, survey);        
        System.debug('Created EventTypeSurvey record: ' + eventTypeSurvey);
        Case_Type__c caseType = CreateCaseType('TestCaseType');
        Case__c caseRecord = IQ_SurveyRepository.CreateCase(
            caseName, 'TestCaseType', patient, survey, eventType, '12/01/2017', null, 'Left');
        string caseTypeId = IQ_SurveyRepository.GetCaseTypeIdByCaseId(caseRecord.Id);
        
        Case_Type_Event_Type__c caseTypeEventType = CreateCaseTypeEventType(caseType, eventType);
        List<Survey__c> surveys = IQ_SurveyRepository.LoadSurveysFromCaseType(caseRecord.Id);
        System.Assert(surveys.size() > 0); 
        Test.stopTest();
    }
    
    @isTest
    static void testGetCaseTypeIdByCaseId() {
        Test.startTest();
        string caseName = 'Test Case';
        Case__c caseRecord= CreateTestCase(caseName);
    	string caseIdString = IQ_SurveyRepository.GetCaseTypeIdByCaseId(caseRecord.Id);
        System.debug(caseIdString);
        System.Assert(caseIdString != ''); 
        Test.stopTest();
    }
    
    @isTest
    static void testCreateCaseType() {
        Test.startTest();
        string description = 'Test Case Type Description';
        Case_Type__c ctRec = CreateCaseType(description);
        string caseTypeId = ctRec.Id;
        System.debug('Case Type Id; ' + caseTypeId);
        List<Case_Type__c> newRec = [Select Id, name, description__c from Case_Type__c where id = :caseTypeId];
        System.debug(newRec.size());
        System.Assert(newRec.size() > 0);
        System.Assert(newRec[0].Description__c == description);
        Test.stopTest();
    }
    
    @isTest static void CreateCaseTest() {
        Test.startTest();
        string caseName = 'Test Case';
        Case__c caseRecord = CreateTestCase(caseName);
        System.Assert(caseRecord.Id != null);
        //System.assertEquals(caseRecord.Name__c, caseName);
        Test.stopTest();
    }

    @isTest
    static void testCreateCaseTypeEventType() {
        Case_Type__c caseType = CreateCaseType('Test Case Type');
        Event_Type__c eventType = CreateEventType();
        Case_Type_Event_Type__c ctet = CreateCaseTypeEventType(caseType, eventType);
        List<Case_Type_Event_Type__c> newctet = [Select Id, Name from Case_Type_Event_Type__c 
                                                    where id = :ctet.id];
        System.debug('Number of CTET records: ' + string.valueOf(newctet.size()));
        System.Assert(newctet.size() == 1);
    }

    @isTest
    static void GetEventTypesByCaseTypeTest() {
        Case_Type__c caseType = CreateCaseType('Test Case Type');
        Event_Type__c eventType = CreateEventType();
        Case_Type_Event_Type__c ctet = CreateCaseTypeEventType(caseType, eventType);
        
        List<Event_Type__c> ids = IQ_SurveyRepository.GetEventTypesByCaseTypeId(caseType.Id);
        System.assert(ids.size() > 0);
    }

    @isTest
    static void GetSurveysByEventTypesTest() {
        string caseName = 'Test Case';
        Case__c caseRecord = CreateTestCase(caseName);
        string caseTypeId = IQ_SurveyRepository.GetCaseTypeIdByCaseId(caseRecord.Id);
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType = IQ_SurveyRepository.CreateEventType('Test Event');
        Event_Type_Survey__c eventTypeSurvey = CreateEventTypeSurvey(eventType, survey);
        Case_Type__c caseType = CreateCaseType('TestCaseType');
        Case_Type_Event_Type__c caseTypeEventType = CreateCaseTypeEventType(caseType, eventType);
        List<Event_Type__c> ids = IQ_SurveyRepository.GetEventTypesByCaseTypeId(caseType.Id);
        
        System.debug(ids);
        List<Survey__c> surveys = IQ_SurveyRepository.GetSurveysByEventTypes(ids, true);
        System.debug(surveys);
        System.assert(surveys.size() > 0); //make sure we have surveys!    
    }

@isTest
    static void GetSurveysByEventTest() {
        string caseName = 'Test Case';
        Account acct = IQ_SurveyRepository.CreateHospital('LPLP Hospital');
        Case__c caseRecord = CreateTestCase(caseName);
        string caseTypeId = IQ_SurveyRepository.GetCaseTypeIdByCaseId(caseRecord.Id);
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        
        Event_Type__c eventType = IQ_SurveyRepository.CreateEventType('Test Event');
        Event_Type_Survey__c eventTypeSurvey = CreateEventTypeSurvey(eventType, survey);
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        Case_Type__c caseType = CreateCaseType('TestCaseType');
        Case_Type_Event_Type__c caseTypeEventType = CreateCaseTypeEventType(caseType, eventType);
        //List<Event_Type__c> ids = IQ_SurveyRepository.GetEventTypesByCaseTypeId(caseType.Id);
        Event__c event = IQ_SurveyRepository.CreateEvent('Test Event', eventType, 'Test Case Type Name', survey, 
        patient, '10/15/2016', null, 'Left');
        //System.debug(ids);
        List<Survey__c> surveys = IQ_SurveyRepository.GetSurveysByEvent(event.id);
        System.debug(surveys);
        System.assert(surveys.size() > 0); //make sure we have surveys!    
    }

    @isTest
    static void GetSurveysByEventTypesTest2() {
        string caseName = 'Test Case';
        Case__c caseRecord = CreateTestCase(caseName);
        string caseTypeId = IQ_SurveyRepository.GetCaseTypeIdByCaseId(caseRecord.Id);
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType = IQ_SurveyRepository.CreateEventType('Test Event');
        Event_Type_Survey__c eventTypeSurvey = CreateEventTypeSurvey(eventType, survey);
        Case_Type__c caseType = CreateCaseType('TestCaseType');
        Case_Type_Event_Type__c caseTypeEventType = CreateCaseTypeEventType(caseType, eventType);
        List<Event_Type__c> ids = IQ_SurveyRepository.GetEventTypesByCaseTypeId(caseType.Id);
        System.debug(ids);
        List<Survey__c> surveys = IQ_SurveyRepository.GetSurveysByEventTypes(ids, false);
        System.debug(surveys);
        System.assert(surveys.size() > 0); //make sure we have surveys!    
    }

    @isTest
    static void testCreateSurvey() {
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        System.debug(survey);
        System.Assert(!string.IsBlank(survey.Id));    
    }

    @isTest
    static void testCreateEventType() {
        Event_Type__c eventType = CreateEventType();
        System.debug(eventType);
        System.Assert(!string.IsBlank(eventType.Id));
    }
    @isTest
    static void testCreateEventTypeSurvey() {
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType = CreateEventType();
        Event_Type_Survey__c eventTypeSurvey = CreateEventTypeSurvey(eventType, survey);
        System.debug(eventTypeSurvey);
        System.Assert(!string.IsBlank(eventTypeSurvey.Id));
    }

    @isTest
    static void testCreateEventForm() {
        string eventFormName = 'Test Event';
        Event_Form__c ef = CreateTestEventForm(eventFormName);
        System.assertEquals(ef.Name, eventFormName);
    }

    // To do: create asserts
    @isTest
    static void CreateEventTest() {
        Account acct = IQ_SurveyRepository.CreateHospital('BBB Hospital');
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Event Type Name');
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        Event_Type_Survey__c eventTypeSurvey = IQ_SurveyRepository.CreateEventTypeSurvey(eventType, survey);      
        string eventName = 'Test Event Name';
        Event__c event = IQ_SurveyRepository.CreateEvent(eventName, eventType, 'Test Case Type Name', survey, 
        patient, '10/15/2016', null, 'Left');
        System.assertEquals(event.Event_Name__c, eventName);    
    }

    @isTest 
    static void CreatePatientTest() {
        Account acct = IQ_SurveyRepository.CreateHospital('ZZZ Hospital');
        string lastName = 'Smith';
        string firstName = 'Fred';
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, lastName, firstName);
        System.debug(patient);
        System.assertEquals(patient.Last_Name__c, lastName);
    }

    @isTest
    static void getSurveyTest() {
        string surveyName = 'Test Survey';
        Survey__c survey = IQ_SurveyRepository.CreateSurvey(surveyName);
        Survey__c retrievedSurvey = IQ_SurveyRepository.GetSurveybyName(survey.Name__c);
        System.assertEquals(retrievedSurvey.Name__c, surveyName);
        //these examples won't be found in other sandboxes or production
        surveyName = 'XYZ Survey';
        retrievedSurvey = IQ_SurveyRepository.GetSurveyByName(surveyName);
        System.assert(true);
        surveyName = 'PROM101';
        retrievedSurvey = IQ_SurveyRepository.GetSurveyByName(surveyName);
        System.assert(true);          
    }

    @isTest
    static void testGetEventType() {
        string eventTypeName = 'Test Event Type';
        Event_Type__c et = IQ_SurveyRepository.CreateEventType(eventTypeName);
        Event_Type__c retrievedEt = IQ_SurveyRepository.GetEventType(eventTypeName);
        System.AssertEquals(et.Name, retrievedEt.Name);
    }

    @isTest
    static void testCreateSurveyResponse() {
        Survey_Response__c sr = new Survey_Response__c();
        string eventFormName = 'Test Event';
        Event_Form__c eventForm = CreateTestEventForm(eventFormName);
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        string question = 'Pain';
        string response = '42';
        
        Survey_Response__c surveyResponse = IQ_SurveyRepository.CreateSurveyResponse(eventForm, survey, question, response);
        System.assertEquals(surveyResponse.survey_question__c, question);
        System.assertEquals(surveyResponse.Survey_response__c, response);
    }

    @isTest static void GetPatientByIdTest() {
        Account acct = IQ_SurveyRepository.CreateHospital('MFM Hospital');
        
        string firstName = 'Jones';
        string lastName = 'Jerry';
        string hospitalId = acct.Id;
        Patient__c newPatient = IQ_SurveyRepository.CreatePatient(hospitalId, lastName, firstName);
        Patient__c patient = IQ_SurveyRepository.GetPatientById(newPatient.Id);
        System.debug(patient.Id);
        System.debug(patient.Last_Name__c);
        System.AssertEquals(patient.Last_Name__c, lastName);
    }

    //this is a test of existing sandbox data
    @isTest 
    static void GetEventFormTest() {
        string eventFormName = 'Test Event Form';
        Account acct = IQ_SurveyRepository.CreateHospital('III Hospital');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Event Type Name');   
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
          Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        string eventName = 'Test Event Name';
        Event__c event = IQ_SurveyRepository.CreateEvent(eventName, eventType, 'Total Knee Arthroplasty', survey, patient,
                                                        '12/01/2018', null, 'Left');
        System.assertEquals(event.Event_Name__c, eventName);    
        Event_Form__c ef = IQ_SurveyRepository.CreateEventForm(eventFormName, event, survey);
        
        Event_Form__c eventFormRetrieved = IQ_SurveyRepository.GetEventForm(survey, event); 
        
        System.debug(eventFormRetrieved.Name);
        System.assertEquals(eventFormRetrieved.Name, eventFormName);
    }
    
    @isTest public static void GetEventTypeIdByEventIdTest() {
        string test = IQ_SurveyRepository.GetEventTypeIdByEventId('');
        System.Assert(test == ''); //negative case
        
    }

    // rewrite this to create and verify a newly created record
    @isTest static void GetSurveyEventTypesTest() {
        List<Survey__c> prom = [Select Id, Name, Name__c From Survey__c];
        //assumes first record has associated event types 
        //a6M7A0000000AFeUAM
        List<Event_Type_Survey__c> eventTypes = IQ_SurveyRepository.GetSurveyEventTypes('a6M7A0000000AFeUAM');
        //System.Assert(eventTypes.size() > 0);
        System.Assert(true);
    } 
    

    @isTest
    static void GetSurveyByIdTest() {
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Survey__c retrievedSurvey = IQ_SurveyRepository.GetSurveyById(survey.Id);
        System.AssertEquals(retrievedSurvey.Id, survey.Id);
    }

    //@isTest
    //static void GetSurveyByIdTest2() {
    //    Test.startTest();
    //    Survey__c retrievedSurvey = IQ_SurveyRepository.GetSurveyById('0090990909');// fake value
        //System.debug(retrievedSurvey);
    //    System.Assert(true);
    //    Test.stopTest();
    //}

    @isTest static void GetScoreNamesTest1() {
        List<String> surveyScoreNames = IQ_SurveyRepository.GetScoreNames('PROM101');
        System.Assert(surveyScoreNames.size() > 4);
    }
    @isTest 
    static void GetScoreNamesTest2() {
        List<string> questions = IQ_SurveyRepository.GetScoreNames('KOOS');
        System.Assert(questions.size() > 4);
    }

    @isTest 
    static void GetScoreNamesTest3() {
        List<string> questions = IQ_SurveyRepository.GetScoreNames('XYZ Survey');
        System.Assert(questions.size() > 4);
    }

    @isTest 
    static void GetScoreNamesTest4() {
        List<string> questions = IQ_SurveyRepository.GetScoreNames('ZZZzzz');
        System.Assert(questions.size() > 3);
    }

    @isTest
    static void GetSurveysTest() {
        Test.startTest();
        List<Survey__c> surveys = IQ_SurveyRepository.GetSurveys();
        System.debug(surveys);
        System.assert(surveys.size() > 0);
        Test.stopTest();
    }
    @isTest static void ExistsInSurveyListTest() {
        List<Survey__c> surveys = [Select Id, Name, Name__c from Survey__c];
        List<Event_Form__c> evt = [Select Id, Name, Survey__r.Id from Event_Form__c];
        boolean exists = IQ_SurveyRepository.ExistsInSurveyList(surveys, evt[0]); 
    }

    @isTest static void ExistsInSurveyListTest2() {
        List<Survey__c> surveys = [Select Id, Name, Name__c from Survey__c];
        List<Event_Type_Survey__c> evt = [Select Id, Name, Survey__r.Id from Event_Type_Survey__c];
        boolean exists = IQ_SurveyRepository.ExistsInSurveyList(surveys, evt[0]); 
    }

    @isTest static void GetHospitalByNameTest() {
        String acctName = 'Test Account';
        Account newacct = new Account();

        newacct.Name = acctName;
        insert newacct;
        Account acct = IQ_SurveyRepository.GetHospitalByName(acctName);
        System.assertEquals(acctName, acct.Name);
    }

    @isTest
    static void SendEmailTest() {
        Test.startTest();
        String to = 'richard.salit@orthosensor.com';
        String subject = 'Testing email';
        String body = '<h1>Title</h1><p>This is the body </p>';
        // IQ_SurveyRepository.SendEmail(to, subject, body);
        Test.stopTest();
    }
    /********************* classes to create test records *****************/
    private static Case__c CreateTestCase(string caseName) {
        Account acct = IQ_SurveyRepository.CreateHospital('ZYX Hospital');
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Event Type Name');
        Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        Event_Type_Survey__c eventTypeSurvey = IQ_SurveyRepository.CreateEventTypeSurvey(eventType, survey);        
        System.debug('Created EventTypeSurvey record: ' + eventTypeSurvey);
        
        Case__c caseRecord = IQ_SurveyRepository.CreateCase(
            caseName, 'Test Case Type', patient, survey, eventType, '12/01/2017', null, 'Left');
        System.debug(caseRecord);
        return  caseRecord;   
    }

    private static Event_Form__c CreateTestEventForm(string eventFormName) {
        Account acct = IQ_SurveyRepository.CreateHospital('RTS Hospital');
        Event_Type__c eventType  = IQ_SurveyRepository.CreateEventType('Event Type Name');   
        Survey__c survey = IQ_SurveyRepository.CreateSurvey('Test Survey');
          Patient__c patient = IQ_SurveyRepository.CreatePatient(acct.Id, 'Test', 'Test');
        string eventName = 'Test Event Name';
        Event__c event = IQ_SurveyRepository.CreateEvent(eventName, eventType, 'Total Knee Arthroplasty', survey, patient,
                                                        '12/01/2018', null, 'Left');
        System.assertEquals(event.Event_Name__c, eventName);    
        Event_Form__c ef = IQ_SurveyRepository.CreateEventForm(eventFormName, event, survey);
        return ef;
    }

    private static Case_Type__c CreateCaseType(string caseTypeDescription) {
        return IQ_SurveyRepository.CreateCaseType(caseTypeDescription);       
    }

    static Case_Type_Event_Type__c CreateCaseTypeEventType(Case_Type__c caseType, Event_Type__c eventType) {
        //ctet.Case_Type__c = caseType.Id;
        //ctet.Event_Type__c = eventType.Id;
        return IQ_SurveyRepository.CreateCaseTypeEventType(caseType, eventType); 
    }

    static Event_Type__c CreateEventType(){
        return IQ_SurveyRepository.CreateEventType('Test Event');
    }

    static Event_Type_Survey__c CreateEventTypeSurvey(Event_Type__c eventType, Survey__c survey) {
        Event_Type_Survey__c eventTypeSurvey = IQ_SurveyRepository.CreateEventTypeSurvey(eventType, survey);
        return eventTypeSurvey ;
    }
    
}