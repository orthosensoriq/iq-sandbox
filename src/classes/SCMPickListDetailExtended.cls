public with sharing class SCMPickListDetailExtended {

	public integer lineNumber { get; private set; }
	public SCMC__Pick_list_Detail__c line {get; private set;}
	public list<SCMC__Serial_Number__c> serialNumberList {get; private set;}
	public set<String> lotNumberList {get; private set;}
	public boolean displayInboundOutboundSerialNumbers { get; private set; }
	public boolean displayLotNumbers { get; private set; }
	public boolean isVirtualKitItem { get; private set; }


	public SCMPickListDetailExtended(integer lineNumber, SCMC__Pick_list_Detail__c PickListDetail, list<SCMC__Serial_Number__c> serialNumberList, set<string> LotNumbers) {
		this.lineNumber = lineNumber;
		this.line = PickListDetail;

		this.serialNumberList = serialNumberList;
		this.displayInboundOutboundSerialNumbers = false;
		if(serialNumberList.size() > 0){
			this.displayInboundOutboundSerialNumbers = true;
		} 		

		this.lotNumberList = LotNumbers;
		this.displayLotNumbers = false;
		if(lotNumberList.size() > 0){
			this.displayLotNumbers = true;
		} 		

	}

}