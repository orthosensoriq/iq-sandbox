public with sharing class SCMSalesOrderLineItemTriggerHandler {
	
	private integer batchSize = 0;

	public SCMSalesOrderLineItemTriggerHandler(integer size) {
		this.batchSize = size;
	}	
	
	public void OnBeforeInsert(list<SCMC__Sales_Order_Line_Item__c> newLines){
		list<SCMC__Sales_Order_Line_Item__c> getPriceList = new list<SCMC__Sales_Order_Line_Item__c>();
		for(SCMC__Sales_Order_Line_Item__c line : newLines){
			if(line.Based_on_Price_List__c){
				getPriceList.add(line);
			}
		}
		this.setPriceForLines(getPriceList);
	}
	
	private void setPriceForLines(list<SCMC__Sales_Order_Line_Item__c> lines){
		if(lines.size() >0){
			map<Id, SCMC__Item__c> itemIdToDetails = new map<Id, SCMC__Item__c>();
			map<Id, SCMC__Sales_Order__c> solIdToSO = new map<Id, SCMC__Sales_Order__c>();
			
			set<Id> itemId = new set<Id>();
			set<Id> soId = new set<Id>();
			for(SCMC__Sales_Order_Line_Item__c line : lines){
				soId.add(line.SCMC__Sales_Order__c);
				itemId.add(line.SCMC__Item_Master__c);
			}
			
			for(SCMC__Sales_Order__c so : [select Id
												 ,SCMC__Customer_Account__c
												 ,SCMC__Customer_Account__r.SCMC__Price_Type__c
												 ,SCMC__Price_Type__c
			                                 from SCMC__Sales_Order__c
			                                where Id In : soId]){
				solIdToSO.put(so.Id, so);
			}
			
			list<string> ItemNames = new list<string>(); 
			for(SCMC__Item__c item : [select Id
											,Name 
			                            from SCMC__Item__c
			                           where Id In : itemId]){
				itemIdToDetails.put(item.Id, item);
				ItemNames.add(item.Name);				
			}
			
			SCMC.Pricing priceClass = new SCMC.Pricing(ItemNames);
			for(SCMC__Sales_Order_Line_Item__c line : lines){
				SCMC__Sales_Order__c so = solIdToSO.get(line.SCMC__Sales_Order__c);	
				SCMC__Item__c item = itemIdToDetails.get(line.SCMC__Item_Master__c);	
				SCMC.Pricing.CustomerType customerType = new SCMC.Pricing.CustomerType(so.SCMC__Customer_Account__c, so.SCMC__Customer_Account__r.SCMC__Price_Type__c);
				
				SCMC__Price__c priceBreakdown = priceClass.price(item.Name, line.SCMC__Quantity__c, customerType, line.SCMC__Condition_Code__c, so.SCMC__Price_Type__c, null);
				if(priceBreakdown != null){
					line.SCMC__Price__c = priceBreakdown.SCMC__Price__c;
					line.SCMC__Item_Cost__c = priceBreakdown.SCMC__Item_Number__r.SCMC__Cost__c;
				}
			}
			
		}
	}
	
}