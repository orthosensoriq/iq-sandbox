public with sharing class IQ_DashboardDataController {
	public List<AggregateResult> sensors {get; set;}
	public List<SensorDataLine> allSensors {get; set;}
	public List<IQ_MonthlySensorData__c> monthlyData {get; set;}
	public List<IQ_Prom_Delta__c> promDeltas {get; set;}
	public List<IQ_Phase_Count__c> phaseCounts {get; set;}
	
	public IQ_DashboardDataController() {
	}

	public void GetSensorDataLines() {
    	Date eventStartDate = Date.newInstance(2016,11, 1);
    	Date eventEndDate = Date.newInstance(2016,11, 30);

        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();

        sensors = repo.GetSensorDataLines(eventStartDate, eventEndDate);
        System.debug(sensors);
        
    }
	
    public void AggregateMonthKneeBalanceData() {
		Date eventStartDate = Date.newInstance(2016,11, 1);
    	Date eventEndDate = Date.newInstance(2017,11, 30);

        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
		repo.AggregateMonthKneeBalanceData(11, 2016);

    }
	
    public void AggregateMonthKneeBalanceData2() {
		Date eventStartDate = Date.newInstance(2016,11, 1);
    	Date eventEndDate = Date.newInstance(2017,11, 30);

        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
		repo.AggregateMonthKneeBalanceData(11, 2016);
		monthlyData = repo.GetAllMonthlySensorData();
		System.debug(monthlyData);
    }
	
	public void AggregateKneeBalanceData() {
		Date eventStartDate = Date.newInstance(2016,11, 1);
    	Date eventEndDate = Date.newInstance(2016,11, 30);

        IQ_KneeBalanceAggregator repo = new IQ_KneeBalanceAggregator();
		repo.AggregateKneeBalanceData(9, 2016, 3, 2017);
		monthlyData = repo.GetAllMonthlySensorData();
		System.debug(monthlyData);
    }

    public void AggregatePromDeltas() {
		IQ_PromDeltaAggregate iq = new IQ_PromDeltaAggregate();

		iq.currentSurvey = [Select ID, Name from Survey__c where Name__c = 'KOOS' limit 1];
		System.debug(iq.currentSurvey);
		iq.Process();
		promDeltas = iq.GetAllPromDeltas();
    }
	
	public void patientPhaseAggregation() {
        IQ_PhaseCountAggregator agg = new IQ_PhaseCountAggregator();
        agg.PhaseCountAggregatorBySurgeon();
        phaseCounts = [Select Id, Name, Month_Date__c, Practice_Name__c, Surgeon_Name__c, Count__c from IQ_Phase_Count__c order by Month_Date__c, Surgeon_Name__c];

    }
}
