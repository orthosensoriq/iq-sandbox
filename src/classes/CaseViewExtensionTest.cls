@isTest
private class CaseViewExtensionTest {

	static testMethod void testCaseViewExtension() {

		//Create Case to use for Testing
		Case__c testCase = new Case__c();
		testCase.Name__c = 'TestCase';
		testCase.Description__C = 'Test description information';
		insert testCase;

		//Construct controller
		ApexPages.StandardController stdCase = new ApexPages.StandardController(testCase);
          CaseViewExtension conExt = new CaseViewExtension(stdCase);

          //Execute code
          conExt.LogConstructor();
          conExt.getEvents();
          testCase.Description__C = 'Edit to description information';
          conExt.save();
          PageReference pageRef = conExt.toggleEditView();
          PageReference pageRef2 = conExt.cancelEdit();
	}

}