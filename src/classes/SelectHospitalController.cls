public with sharing class SelectHospitalController{

	public List<Account> hospitals {get;set;}
	public String searchPhrase {get;set;}
	public Id AccountID {get;set;}
	public List<Account> recentlyViewedHospitals {get;set;}
	public Boolean renderRecentlyViewed {get;set;}
	
	public SelectHospitalController(){
		
		hospitals = [SELECT Id, Account_Image__c, AccountImageURL__c, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner = TRUE ORDER BY Name ASC LIMIT 10000];
		recentlyViewedHospitals = [Select Id, Account_Image__c, AccountImageURL__c, Name from Account WHERE RecordType.Name='Hospital Account' AND LastViewedDate!=null AND IsPartner = TRUE ORDER BY LastViewedDate DESC LIMIT 20];
		Set<Account> uniqueRVHospitals  = new Set<Account>();
		uniqueRVHospitals.addAll(recentlyViewedHospitals);
		recentlyViewedHospitals.clear();
		recentlyViewedHospitals.addAll(uniqueRVHospitals);
		renderRecentlyViewed = false;
	}
	
	public void Search(){
		
		searchPhrase = '%' + searchPhrase + '%';
		renderRecentlyViewed = false;
		hospitals = [SELECT Id, Account_Image__c, AccountImageURL__c, Name from Account WHERE RecordType.Name='Hospital Account' AND IsPartner = TRUE AND Name LIKE: searchPhrase ORDER BY Name ASC LIMIT 10000];
	}
	
	public PageReference goToProcedureListing(){
		Account updateAccountViewDate = [SELECT Name, Account_Image__c, AccountImageURL__c, ID FROM Account WHERE Id =:AccountID LIMIT 1 FOR VIEW];
		PageReference pageRef = new PageReference('/apex/ProcedureListing?accid=' + AccountID);
		return pageRef;
		
	}	
	public void getRecentlyViewed(){
			renderRecentlyViewed = true;
	}
	
	public void getAllHospitals(){
	
		renderRecentlyViewed = false;
		
	}
	
}