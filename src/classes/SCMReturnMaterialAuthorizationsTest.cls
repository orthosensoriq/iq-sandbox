/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
@isTest()
public with sharing class SCMReturnMaterialAuthorizationsTest {

	// Purely a code coverage method.
	@isTest()
	public static void codeCoverageTest() {

		SCMC__Return_Material_Authorization__c rma = 
			new SCMC__Return_Material_Authorization__c(
				SCMC__Sales_Order_Line_Item__c = SCMSetUpTest.generateMockId(SCMC__Sales_Order_Line_Item__c.SObjectType));
		try {
			insert rma;
		}
		catch (DmlException ex) {
			if (!ex.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY'))
				throw ex;
			System.debug('Expected insertion exception was caught.  The RMA exercises the before-insert trigger code but is NOT really valid.');
		}
	}
}