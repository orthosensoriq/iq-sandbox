public class ImplantComponentTriggerHandler extends BaseTriggerHandler
{
	final String CLASSNAME = '\n\n**** ImplantComponentTriggerHandler.METHODNAME()';
    private integer BatchSize = 0;
    private Profile currentUserProfile;

    //@author : EnablePath
	//@date : 12/18/2013
	//@description : the class constructor method
	//@paramaters : a Boolean value indicating if a trigger is executing or not, an Integer value indicating the number of records in the batch/list
	//@returns : nothing
    public ImplantComponentTriggerHandler(boolean isExecuting, integer pTriggerSize, string sObjectName)
    {
    	String METHODNAME = CLASSNAME.replace('METHODNAME', 'ImplantComponentTriggerHandler') + ' - class constructor';
    	system.debug(LoggingLevel.DEBUG, METHODNAME.replace('**** ', '**** Inside ' + sObjectName) + ' \n\n');

		// trigger is executing
        TriggerIsExecuting = isExecuting;
		
		// set batch size
        BatchSize = pTriggerSize;
		
		// set current User profile
        currentUserProfile = [Select Name From Profile where Id = :UserInfo.getProfileId()];
    }

    public override void OnBeforeInsert(List<SObject> insertObject)
    {
    	updateNotInHospitalStock(insertObject);
    }
    
    public override void OnBeforeUpdate(Map<Id, SObject> beforeObject, Map<Id, SObject> afterObject)
    {
    	updateNotInHospitalStock(afterObject.values());
    }

    public override void OnAfterDelete(Map<Id, SObject> deleteObject)
    {
    	//Not Called for this Trigger
    }

    private void updateNotInHospitalStock(List<sobject> dataObj)
    {
        system.debug('in ImplantComponentTriggerHandler:updateNotInHospitalStock');
        set<Id> ProductIds = new set<Id>();
        set<Id> EventIds = new set<Id>();
        set<Id> HospIds = new set<Id>();
    	//Update Not in Hospital Stock 
    	//Component Product --> Implant Component --> event --> Location --> Hospital Product
    	for(implant_component__c cp : (list<implant_component__c>)dataObj)
        {
       		ProductIds.add(cp.Product_Catalog_Id__c );
            EventIds.add(cp.Event__c);
        }
        map<Id, event__c> Events = new map<Id, event__c>([select Id, Location__c from event__c where Id in :EventIds]);
		For(event__c evt : Events.values())
        {
            HospIds.add(evt.Location__c);
        }
        List<hospital_product__c> hospitals = [select Hospital__c, Product_catalog__c from hospital_product__c where Hospital__c in :HospIds]; //product_catalog__c in :ProductIds];
        system.debug('HospitalProduct size: ' + hospitals.size() + ', ProductIds size: ' + ProductIds.size());
        for(implant_component__c cp : (list<implant_component__c>)dataObj)
        {
            string setVal = 'N/A';
           	for(hospital_product__c hp : hospitals)
            {
                system.debug('Implant_Component__c.Id: ' + cp.Id);
                system.debug('Hospital: ' + events.get(cp.event__c).Location__c + ' | ' + hp.hospital__c);
                system.debug('Product: ' + cp.Product_Catalog_Id__c + ' | ' + hp.Product_Catalog__c);
                if(events.get(cp.event__c).Location__c == hp.Hospital__c &&
                   cp.Product_Catalog_Id__c  == hp.product_catalog__c)
                {
                    setVal = 'Yes';
                    break;
                }
                else if(events.get(cp.event__c).Location__c == hp.hospital__c)
                {
                    setVal = 'No';
                }
            }
            system.debug('Setting Implant Component Is_Hospital_Product__c to: ' + setVal);
            cp.Is_Hospital_Product__c = setVal;
        }
    }
}