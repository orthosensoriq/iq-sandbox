trigger SCMSalesOrderLineItemTrigger on SCMC__Sales_Order_Line_Item__c (after delete, after insert, after update, 
before delete, before insert, before update) {
	
	SCMSalesOrderLineItemTriggerHandler triggerHandler = new SCMSalesOrderLineItemTriggerHandler(trigger.size);
	if(trigger.isInsert && trigger.isBefore){
		triggerHandler.OnBeforeInsert(trigger.new);
	}
	
}