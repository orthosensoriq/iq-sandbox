trigger CaseTrigger on Case__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
	// declare Trigger Handler	
	CaseTriggerHandler handler = new CaseTriggerHandler(Trigger.isExecuting, Trigger.size, 'Case__c');

	if(trigger.isInsert)
	{
	    if(Trigger.isBefore){
			// before insert event
	       	handler.OnBeforeInsert(Trigger.new);
	    }
	    else if(Trigger.isAfter)
        {
			// after insert event
            handler.OnAfterInsert(Trigger.newMap);
	    }
	}else if(trigger.isUpdate){
	
	    if(Trigger.isBefore){
			// before update event
	       	handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
	    }
	    else if(Trigger.isAfter){
			// after update event
            handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
	    }
    }else if(trigger.isDelete){

	    if(Trigger.isBefore){
			// before delete event
            handler.OnBeforeDelete(Trigger.oldMap);
	    }
	    else if(Trigger.isAfter){ 
			// after delete event
            handler.OnAfterDelete(Trigger.oldMap);
	    }
    }
    else if(Trigger.isUnDelete){ 
		// undelete event
        handler.OnUndelete(Trigger.new);
    }

}