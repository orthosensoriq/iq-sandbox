trigger SCMSerialNumberTrigger on SCMC__Serial_Number__c (after delete, after insert, after update, 
before delete, before insert, before update) {

	SCMSerialNumberTriggerHandler snTriggerClass = new SCMSerialNumberTriggerHandler(trigger.new, trigger.size);

	if(trigger.isBefore && trigger.isInsert){
		snTriggerClass.OnBeforeInsert();
	} else if(trigger.isBefore && trigger.isUpdate) {
		snTriggerClass.OnBeforeUpdate(trigger.old);
	} else if(trigger.isAfter && trigger.isInsert) {
		snTriggerClass.onAfterInsert(Trigger.new, Trigger.newMap);
	}
	
}