/**
 * Copyright (c) 2010, Less Software, inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 *  When a requisition is approved, flag all line items that are not disapproved or cancelled as
 *  approved.
**/
trigger RequisitionApprove on SCMC__Requisition__c (before update) {
    
    Map<ID, SCMC__Requisition__c>approvedReq = new Map<ID, SCMC__Requisition__c>();
    
    for (Integer i = 0; i < trigger.New.Size(); i++) {
        if (trigger.New[i].Approval_Status__c != null && 
            trigger.Old[i].Approval_Status__c != null &&
            trigger.New[i].Approval_Status__c.equals('Approved') && 
            !trigger.Old[i].Approval_Status__c.equals('Approved')){
            //Change the Record Type of the Requisition to "Approved Requisition"
            trigger.New[i].recordTypeId = '012U0000000UwPjIAK';
            //Collect all the requisitions that are in this state
            approvedReq.put(trigger.New[i].id, trigger.New[i]);
        }
    }
    
    if (approvedReq.keyset().Size() > 0){
        List<SCMC__Requisition_Line_Item__c>lines = [select id
            ,name
            ,SCMC__Line_Status__c
            from SCMC__Requisition_Line_Item__c
            where SCMC__Requisition__c in :approvedReq.keyset() and
                    SCMC__Line_Status__c != 'Cancelled'];
        if (lines.Size() > 0){
            for (SCMC__Requisition_Line_Item__c line : lines){
                line.SCMC__Line_Status__c = 'Approved';
            }
            update lines;
        }
    }
}