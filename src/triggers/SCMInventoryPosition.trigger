/**
 * Copyright (c) 2014, Financial Force, inc
 * All rights reserved.
 *
 *  Orthosensor trigger on inventory position
 **/ 
 trigger SCMInventoryPosition on SCMC__Inventory_Position__c (before insert, before update) {
 	
 	SCMInventoryPositionTriggerHandler th = new SCMInventoryPositionTriggerHandler();
 	
 	if (Trigger.isInsert){
 		th.onBeforeInsert(Trigger.new);
 	}else {
 		th.onBeforeUpdate(Trigger.old, Trigger.new);
 	}
 	

}