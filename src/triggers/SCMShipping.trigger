/*
    Copyright (c) 2013, 2014 LessSoftware.com, inc.
    All rights reserved.
	Send invoice to FF when shipment is complete 
    
*/

trigger SCMShipping on SCMC__Shipping__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	SCMShipmentTriggerHandler handler = new SCMShipmentTriggerHandler();

	if (trigger.isBefore && trigger.isInsert) {
		handler.OnBeforeInsert(trigger.new);
	}
	
	if (trigger.isAfter && trigger.isUpdate){
		handler.shipmentAfterUpdate(Trigger.old, Trigger.new);
	}

}