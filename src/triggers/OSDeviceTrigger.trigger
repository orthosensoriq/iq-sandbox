trigger OSDeviceTrigger on OS_Device__c (before insert) {
        set<string> m = new set<string>();
        for(OS_Device__c c : trigger.new){
            m.add(c.Manufacturer_Name__c);
        }
        map<String,Id> omc = new map<String,Id>();
        for (Manufacturer__c mc : [select id, name from Manufacturer__c where name in :m]){
            omc.put(mc.name, mc.id);
        }
        for(OS_Device__c o : trigger.new){
            o.OSManufacturer__c = omc.get(o.Manufacturer_Name__c);
        }
}