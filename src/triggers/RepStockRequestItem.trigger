/* Human.txt Jamie Smith - Coastal Cloud - jamie.smith@coastalcloud.us */
trigger RepStockRequestItem on Rep_Stock_Request_Item__c (after insert, before update) {

	RepStockRequestItemTriggerHandler handler = new RepStockRequestItemTriggerHandler();
	
	if(Trigger.isInsert || Trigger.isUpdate){
		if (Trigger.isBefore || Trigger.isAfter) {
			
			for(Rep_Stock_Request_Item__c n : Trigger.New) {
				
				Id masterId = n.Rep_Stock_Request__c;
				Id newItem = n.Item_Master__c;
				Decimal want = n.Quantity__c;
				Id newItemId = n.Id;
				
				//Make the parent status new when items are added to a rep stock request
				handler.makeParentStatusNew(masterId);
				System.debug('makeParentStatusNew ran ');
				
				//Duplicate check
				String dupResponse = handler.checkForDups(masterId, newItem, newItemId);
				if(dupResponse != 'No Error'){
                    n.adderror(dupResponse);
                }
                
                //Inventory check
                //jamiesHelper test = new jamiesHelper();
				String wantVsHaveResponse = handler.isWantLessThanInventory(newItemId, want);
				if(wantVsHaveResponse != 'No Error'){
                    n.adderror(wantVsHaveResponse);
                } 
                //TODO remove else - is only for testing!
                /*else {
                	n.adderror('Hey ' + UserInfo.getFirstName() + ', it didnt error but was stopped to not throw off numbers while in development.');
                }*/
				
            }
		}
	}
}