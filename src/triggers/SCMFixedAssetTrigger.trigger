/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2014 FinancialForce.com, inc. All rights reserved.
*/
trigger SCMFixedAssetTrigger on FAM__Fixed_Asset__c (after insert, after update) {

	SCMFixedAssetTriggerHandler handler = new SCMFixedAssetTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isAfter && Trigger.isInsert) {
		handler.OnAfterInsert(Trigger.new, Trigger.newMap);
	}
	
	if (Trigger.isAfter && Trigger.isUpdate) {
		handler.OnAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	}
}