trigger SurgeonHospitalTrigger on Surgeon_Hospital__c (after insert) {

    if(Trigger.isAfter)
    {
        if(Trigger.IsInsert)
        {
            SurgeonHospitalActions.AddContact(Trigger.new);
        }
    }

}