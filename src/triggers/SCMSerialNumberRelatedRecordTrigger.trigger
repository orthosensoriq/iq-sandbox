trigger SCMSerialNumberRelatedRecordTrigger on SCMC__Serial_Number_Related_Record__c (before insert) {

	if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		SCMSerialNumberTriggerHandler snTriggerClass = new SCMSerialNumberTriggerHandler(trigger.new, trigger.size);
		snTriggerClass.OnBeforeInsertOrUpdateRelatedRecord();
	}

}