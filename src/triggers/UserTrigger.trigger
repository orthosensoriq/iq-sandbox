trigger UserTrigger on User (after update,after insert) 
{
    // declare Trigger Handler  
    //osTriggerHandler handler = new osTriggerHandler(Trigger.isExecuting, Trigger.size, 'User__c');

    if(trigger.isUpdate && Trigger.isAfter)
    {
        // after update event
        if(system.UserInfo.getUserId() == ((User)Trigger.old[0]).id)
            SetupObjectTriggerHandler.logSetupUpdate('Update', JSON.serialize(Trigger.old), JSON.serialize(Trigger.new), 'User');
        
    }
    if(trigger.isInsert && Trigger.isAfter){
        //add logic to grab user.contactid if not null
        //set corresponding contact.user__c = user.id
    }

}