/*
* FinancialForce.com, inc. claims copyright in this software, its screen
* display designs and supporting documentation. FinancialForce and
* FinancialForce.com are trademarks of FinancialForce.com, inc. Any
* unauthorized use, copying or sale of the above may constitute an
* infringement of copyright and may result in criminal or other legal
* proceedings.
*
* Copyright (c) 2015 FinancialForce.com, inc. All rights reserved.
*/
trigger SCMFinancialInsert on SCMC__Inventory_Transaction_Financial_Records__c (before insert) {
    boolean customerReturn = false;
    Map<ID, SCMC__Inventory_Transaction_Perpetual_Record__c>perps = new Map<ID, SCMC__Inventory_Transaction_Perpetual_Record__c>();
    for (SCMC__Inventory_Transaction_Financial_Records__c tran : Trigger.New){
        if (tran.SCMC__ICP_Value_Amount__c == 0){
            perps.put(tran.SCMC__Inventory_Transaction__c, null);
            if (tran.SCMC__GL_Account__c == 'Return'){
                customerReturn = true;
            }
    	}
    }
    if (customerReturn){
        //we only need to do this if there has been a customer return
        perps = new Map<ID, SCMC__Inventory_Transaction_Perpetual_Record__c>([Select id, name
                                                                             ,SCMC__Inventory_Position__r.SCMC__Current_Value__c
                                                                             ,SCMC__Inventory_Position__r.SCMC__Quantity__c
                                                                             ,SCMC__Quantity__c
                                                                             from SCMC__Inventory_Transaction_Perpetual_Record__c
                                                                             where id in :perps.Keyset() and
                                                                             SCMC__Inventory_Transactions__c = 'Receive Customer Returned Material' and
                                                                             SCMC__Inventory_Position__r.SCMC__Current_Value__c != 0]);
        for (SCMC__Inventory_Transaction_Financial_Records__c tran : Trigger.New){
        	SCMC__Inventory_Transaction_Perpetual_Record__c perp = perps.get(tran.SCMC__Inventory_Transaction__c);
            
            if (perp != null){
            	// we have customer return with a value 
            	System.debug('financial transaction ' + tran);
                System.debug('perpetual transaction ' + perp);
                System.debug('inventory ' + perp.SCMC__Inventory_Position__r.SCMC__Current_Value__c + '*' + perp.SCMC__Inventory_Position__r.SCMC__Quantity__c);
                if (tran.SCMC__GL_Account__c == 'Return'){
                    tran.SCMC__ICP_Unit_Cost__c = perp.SCMC__Inventory_Position__r.SCMC__Current_Value__c * -1;
                } else {
                    tran.SCMC__ICP_Unit_Cost__c = perp.SCMC__Inventory_Position__r.SCMC__Current_Value__c;
                } 
                tran.SCMC__Trade_Unit_Cost__c = tran.SCMC__ICP_Unit_Cost__c;
                tran.SCMC__ICP_Value_Amount__c = tran.SCMC__ICP_Unit_Cost__c * perp.SCMC__Inventory_Position__r.SCMC__Quantity__c;
                tran.SCMC__Trade_Value_Amount__c = tran.SCMC__ICP_Value_Amount__c;
            }
        }
    }

}