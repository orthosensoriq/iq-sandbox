trigger SCMSalesOrder on SCMC__Sales_Order__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	SCMSalesOrderTriggerHandler handler = new SCMSalesOrderTriggerHandler(trigger.isExecuting, trigger.size);

	if (trigger.isBefore && trigger.isInsert) {
		handler.OnBeforeInsert(trigger.new);
	} else if(trigger.isBefore && trigger.isUpdate) {
		handler.OnBeforeUpdate(trigger.new, trigger.old);
	}
}