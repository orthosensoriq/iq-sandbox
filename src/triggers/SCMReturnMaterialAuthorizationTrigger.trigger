trigger SCMReturnMaterialAuthorizationTrigger on SCMC__Return_Material_Authorization__c (before insert) {

	SCMReturnMaterialAuthorizationTriggerHa handler = new SCMReturnMaterialAuthorizationTriggerHa(Trigger.isExecuting, Trigger.size);

	if (Trigger.isBefore && Trigger.isInsert)
		handler.OnBeforeInsert(Trigger.new);
}