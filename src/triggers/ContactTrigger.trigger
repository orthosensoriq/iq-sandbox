trigger ContactTrigger on Contact (after insert) {

	List<Id> contactIds = new List<Id>();
	for(Contact contact: Trigger.new)
	{
		contactIds.add(contact.Id);	
	}
	
	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			ContactActions.CreatePortalUser(contactIds);	
		}		
	}
}