trigger SCMReceivingInspection on SCMC__Receiving_Inspection__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

	SCMReceivingInspectionTriggerHandler handler = new SCMReceivingInspectionTriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isAfter && Trigger.isUpdate)
		handler.OnAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	
}