<apex:page title="Patient Population" tabstyle="Analytics__tab" showHeader="true" sidebar="false" controller="PatientPopulationSearchController" docType="html-5.0">

<!--apex:includescript value="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.js"/-->
<script src="{!URLFOR($Resource.JQueryUI, 'jquery-ui-1.10.3/jquery-1.9.1.js')}"></script>
    
    <style type="text/css" title="currentStyle">
        @import "{!URLFOR($Resource.DataTables, 'media/css/demo_page.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/demo_table.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/demo_table_jui.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/ColReorder.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/ColVis.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/TableTools.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/css/ColumnFilterWidgets.css')}";
        @import "{!URLFOR($Resource.DataTables, 'media/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css')}";
        .custPopup{
            background-color: white;
            border-width: 2px;
            border-style: solid;
            z-index: 9999;
            left: 45%;
            padding:10px;
            position: absolute;
            /* These are the 3 css properties you will need to change so the popup 
                        displays in the center of the screen. First set the width. Then set 
                        margin-left to negative half of what the width is. You can add 
                        the height property for a fixed size pop up if you want.*/
                    width: 50px;
                    top:100px;
                }
        .popupBackground{
            background-color:black;
            opacity: 0.20;
            filter: alpha(opacity = 20);
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 9998;
        }	
        button.ColVis_Button { display: none; }
        span.dateInput span.dateFormat{ display:none; }
    </style>

    <script src="{!URLFOR($Resource.DataTables, 'media/js/jquery.dataTables.min.js')}"></script>
    <script src="{!URLFOR($Resource.DataTables, 'media/js/jquery.dataTables.js')}"></script>
    <script src="{!URLFOR($Resource.DataTables, 'media/js/ColVis.js')}"></script>
    <script src="{!URLFOR($Resource.DataTables, 'media/js/ZeroClipboard.js')}"></script>
    <script src="{!URLFOR($Resource.DataTables, 'media/js/TableTools.js')}"></script>
    <script src="{!URLFOR($Resource.DataTables, 'media/js/ColumnFilterWidgets.js')}"></script>
    
    <apex:define name="body">
    <apex:form >
    <apex:pageBlock title="Patient Population" id="theBlock">
        <apex:outputPanel id="messageblock">
            <apex:Messages style="list-style-type:none; color:red; font-weight:strong; text-align:center;"/>
        </apex:outputPanel>
        <apex:actionRegion >
            <div><!-- style="position: relative;">-->
                <apex:outputpanel >
                    <apex:actionstatus id="status">
                        <apex:facet name="start">
                            <div class="popupBackground"/> 
                            <div class="custPopup">
                                <img src="{!$Resource.AjaxSpinner}" width="50" height="50" />
                            </div>
                        </apex:facet>
                    </apex:actionstatus>
               	</apex:outputpanel>
            </div>
        </apex:actionRegion>
        <apex:pageBlockSection title="Procedure Data" columns="1" id="ProcedureSection">
            <apex:outputPanel id="ProcedureParamTable">
				<Table width="100%">
                	<tr>
            			<td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Hospital"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Hospitals}" size="5" multiselect="true">
                                <apex:selectOptions value="{!HospitalItems}"/>
                            </apex:selectList>
	                    </td>
                        <td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Surgeon"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Surgeons}" size="5" multiselect="true">
                                <apex:selectOptions value="{!SurgeonItems}"/>
                            </apex:selectList>
                        </td>
                        <td colspan="2">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Procedure Date"/><br/>
                            <apex:selectList style="width:100px;" value="{!ProcedureDateOperation}" size="1">
                            	<apex:selectOptions value="{!ProcedureDateOperationItems}"/>    
                            	<apex:actionSupport id="ProcedureDateOp" action="{!setUpperDateVisibility}" event="onchange" rerender="datePanel" />
                            </apex:selectList>&nbsp;&nbsp;
                            <apex:outputPanel id="datePanel">
	                            <apex:inputText Style="width:75px;" value="{!searchProcDate}" size="10" id="procDate" onfocus="DatePicker.pickDate(false, this, false);"/>
                                <apex:outputText value=" and " rendered="{!showUpperProcDate}"/>
                            	<apex:inputText Style="width:75px;" value="{!searchUpperProcDate}" size="10" id="upperProcDate" onfocus="DatePicker.pickDate(false, this, false);" rendered="{!showUpperProcDate}"/>
                            </apex:outputPanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Case Type"/><br/>
                            <apex:selectList Style="width:150px;" value="{!CaseTypes}" size="5" multiselect="true">
                                <apex:selectOptions value="{!CaseTypeItems}"/>
                            </apex:selectList>
                        </td>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Laterality"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Laterality}" size="5" multiselect="true">
                                <apex:selectOptions value="{!LateralityItems}"/>
                            </apex:selectList>
                        </td>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Primary Diagnosis"/><br/>
                            <apex:selectList Style="width:150px;" value="{!PrimaryDiagnosis}" size="5" multiselect="true">
                                <apex:selectOptions value="{!PrimaryDiagItems}"/>
                            </apex:selectList>
                        </td>
                        <td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Secondary Diagnosis"/><br/>
                            <apex:selectList Style="width:150px;" value="{!SecondaryDiagnosis}" size="5" multiselect="true">
                                <apex:selectOptions value="{!SecondaryDiagItems}"/>
                            </apex:selectList>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
        </apex:pageBlockSection>

		<apex:pageBlockSection title="Clinical/Demographic Data" columns="1" id="DemographicSection">
            <apex:outputPanel id="DemogParamTable">
				<Table width="100%">
                	<tr>
                        <td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Height (inches)"/><br/>
                            <apex:inputText Style="width:150px;" value="{!searchHeight}" size="10" id="height"/>
                        </td>
                        <td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Weight (pounds)"/><br/>
                            <apex:inputText Style="width:150px;" value="{!searchWeight}" size="10" id="weight"/>
                        </td>
                        <td width="25%">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="BMI"/><br/>
                            <apex:inputText Style="width:150px;" value="{!searchBMI}" size="10" id="BMI"/>
                        </td>
                        <td width="25%">&nbsp;</td>
                    </tr>
            
                    <tr>           
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Age"/><br/>
                            <apex:selectList style="width:100px;" value="{!AgeOperation}" size="1">
                            	<apex:selectOptions value="{!AgeOperationItems}"/>    
                            	<apex:actionSupport id="AgeOp" action="{!setUpperAgeVisibility}" event="onchange" rerender="agePanel" />
                            </apex:selectList>&nbsp;&nbsp;
                            <apex:outputPanel id="agePanel">
                                <apex:inputText Style="width:50px;" value="{!searchAge}" size="10" id="age"/>
                                <apex:outputText value=" and " rendered="{!showUpperAge}"/>
                                <apex:inputText Style="width:50px;" value="{!searchUpperAge}" size="10" id="upperage" rendered="{!showUpperAge}"/>
                            </apex:outputPanel>
                        </td>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Race"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Races}" size="3" multiselect="true">
                                <apex:selectOptions value="{!RaceItems}"/>
                            </apex:selectList>
                        </td>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Gender"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Genders}" size="3" multiselect="true">
                                <apex:selectOptions value="{!GenderItems}"/>
                            </apex:selectList>
                        </td>
                        <td width="25%">&nbsp;</td>
                	</tr>
	            </table>
            </apex:outputPanel>
        </apex:pageBlockSection>
		<apex:pageBlockSection title="Component Data" columns="1" id="ComponentSection" >
            <apex:outputPanel id="CompParamTable">
				<Table width="100%">
                	<tr>
                        <td>
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Manufacturer"/><br/>
                            <apex:selectList Style="width:150px;" value="{!Manufacturers}" size="3" multiselect="true">
                                <apex:selectOptions value="{!ManufacturerItems}"/>
                            </apex:selectList>
                        </td>
                    </tr>
	            </table>
            </apex:outputPanel>
        </apex:pageBlockSection><br/>
		<apex:pageBlockSection title="Results" columns="1">
        <apex:outputPanel id="actionButtons">
            <table width="100%">
            	<tr>
                    <td width="20%">
                        <apex:commandButton action="{!getResults}" status="status" value="Search" reRender="messageblock, actionButtons, patients, jqpanel">
                        </apex:commandButton>
                        <apex:commandButton action="{!exportToCSV}" value="Export to CSV" status="status" rendered="{!showExport}">
                        </apex:commandButton>
                    </td>
                    <td width="5%">&nbsp;</td>
                	<td>
                        <apex:outputPanel rendered="{!showExport}">
                            <apex:outputLabel Style="width:70px;font-weight:bold;" value="Parameters"/><br/>
                            <apex:outputText value="{!searchParams}" escape="false" style="font-size:8pt;"/>
                            <script type="text/javascript">
                                twistSection(document.getElementById('{!$Component.theBlock.ProcedureSection}').getElementsByTagName('img')[0]);
                                twistSection(document.getElementById('{!$Component.theBlock.DemographicSection}').getElementsByTagName('img')[0]);
                                twistSection(document.getElementById('{!$Component.theBlock.ComponentSection}').getElementsByTagName('img')[0]);
                            </script>
                        </apex:outputPanel>
					</td>
				</tr>
			</table>
		</apex:outputPanel>
        <apex:outputPanel id="patients">
            <div id="dataTables_wrapper">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="Populationlist" style="margin-top:20px;">    
            <thead>
                <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Hospital</th>
                    <!--<th>MRN</th>
                    <th>Acct#</th>-->
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Hospital</th>
                    <!--<th>MRN</th>
                    <th>Acct#</th>-->
                </tr>
            </tfoot>
            <tbody>
                <apex:repeat value="{!patientPopLines}" var="p">
                    <tr>
                        <td>
                            <apex:commandLink action="{!goToPatientView}" value="{!p.LastName}">
                                <apex:param name="ID" assignTo="{!PatientID}" value="{!p.PatientId}"/>
                            </apex:commandLink>
                        </td>
                        <td>{!p.FirstName}</td>
                        <td>
                            <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                <apex:param value="{!p.DateOfBirth}" /> 
                            </apex:outputText>
                        </td>
                        <td>{!p.Gender}</td>
                        <td>{!p.HospitalName}</td>
                        <!--<td>{!p.Medical_Record_Number__c}</td>
                        <td>{!p.Account_Number__c}</td>-->
                    </tr>
                 </apex:repeat>
            </tbody>
        </table>
       </div>
        </apex:outputPanel>
	</apex:pageBlockSection>
	</apex:pageBlock>
    </apex:form>
    <apex:outputPanel id="jqpanel">
        <script type="text/javascript" charset="UTF-8">
            var jq$ = jQuery.noConflict();
            jq$(document).ready( function () {
                var oTable = jq$('#Populationlist').dataTable( {
                    "oLanguage": { "sSearch": "Filter Last Name:" },
                    "iDisplayLength": 25,
                    "aLengthMenu": [[25, 50, 100, 150], [25, 50, 100, 150]],
                    "sDom": 'T<"clear">lfrtip',
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumnDefs": [ { "bSearchable": false, "aTargets": [ 1, 2, 3, 4, 5, 6 ] }],
                    "aoColumnDefs": [ { "bVisible": false, "aTargets": [ ] }],
                    "oColumnFilterWidgets": { "aiExclude": [ 0, 1, 2, 3, 5, 6 ] },
					"oTableTools": { "sSwfPath": "{!URLFOR($Resource.DataTables, 'media/swf/copy_csv_xls_pdf.swf')}",
                                     "aButtons": [ "pdf" ] }
                });
                
                jq$("#patientlist_filter input").keyup( function () {
                    /* Filter on the column (the index) of this element */
                    oTable.fnFilter( this.value, jq$("#patientlist_filter input").index(0) );
                } );
                
                if(oTable.fnSettings().fnRecordsDisplay()==0)
            	{
                	jq$('#Populationlist').parents('div.dataTables_wrapper').first().hide();
            	}
            	else
            	{
                	jq$('#Populationlist').parents('div.dataTables_wrapper').first().show();
            	}
            });
        </script>
    </apex:outputPanel>
    </apex:define>
</apex:page>