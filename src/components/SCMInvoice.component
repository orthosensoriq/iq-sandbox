<apex:component access="global" controller="SCMC.InvoiceController" extensions="SCMInvoiceControllerExtension">
<apex:attribute name="invoiceId" type="String" required="false" assignTo="{!invId}" description="The id of the Invoice that is being rendered."/> 
<apex:attribute name="invoiceIdExtend" type="String" required="false" assignTo="{!invIdExtend}" description="The id of the Invoice that is being rendered."/> 
<apex:stylesheet value="{!URLFOR($Resource.SCMC__SCM_Resources, 'styles/formsStyles.css')}" />
<apex:stylesheet value="{!$Resource.SCMC__MainStyleSheet}"/>              
<apex:stylesheet value="{!$Resource.SCMC__PortraitPageStyle}"/>               
<apex:componentBody >

    <div class="section-a" style="margin-top: -25px;">
	    <apex:image value="{!CompanyLogo}" style="margin-left: -10px;" />
		<div style="clear: both;">
        	<div class="company" style="padding-top: 10px;">{!org.SCMC__Name__c}</div>
            <apex:outputText escape="false" value="{!CompanyAddress}" />
		</div>
    </div>

    <div class="section-b details">
        <div class="box">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td colspan="2" class="section-title">Invoice</td>
                </tr>
                <tr>
                    <td width="30%" class="title">Date:</td>
                <td><apex:outputField value="{!inv.SCMC__Invoice_Date__c}"/></td>
                </tr>
                <tr>
                    <td class="title">Invoice #:</td>
                <td>{!if(inv.SCMC__Foreign_Invoice_Number__c != null, inv.SCMC__Foreign_Invoice_Number__c, inv.name)}</td>
                </tr>
                <tr>
                    <td class="title">Customer ID:</td>
                <td><apex:outputField value="{!inv.SCMC__Customer_Account__c}"/></td>
                </tr>
            </table>
        </div>
    </div> 

    <div class="form-header-section">
        <table cellpadding="0" cellspacing="0" border="0">
			 <tr>
                <td width="3%" class="title">To:</td>
                <td width="95%"><apex:outputText escape="false" value="{!ToAddress}" /></td>
	        </tr>   
	    </table>
    </div>
    
    <div class="form-details-section">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <th>Salesperson</th>
                <th>Your Purchase Order Number</th>
                <th>Payment Terms</th>
                <th class="last">Due Date</th>
            </tr>
            <tr>
                <td class="first">{!inv.SCMC__Sales_Representative__c}&nbsp;</td>
                <td>{!inv.SCMC__Customer_Purchase_Order_Number__c}&nbsp;</td>
                <td>{!invExtended.SCMC__Payment_Terms_Name__c}&nbsp;</td>
                <td class="last"><apex:outputField value="{!inv.SCMC__Invoice_Due_Date__c}"/>&nbsp;</td>
            </tr>
        </table>    
    </div>
    
    <div class="form-data-section">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                 <th>Line</th>
                 <th>
                     <div>Item</div>
                     <div>Description</div>
                 </th>
                 <th>
                 	<apex:outputText rendered="{!displayAdditionInfo}">
	                    <div>Lot Number</div>
	                    <div>Serial Number</div>
                 	</apex:outputText>
                 	<apex:outputText rendered="{!if(displayAdditionInfo, false, true)}">
                 		<div>&nbsp;</div>
                 	</apex:outputText>
                 </th>
                 <th>Quantity</th>
                 <th class="right">Unit Price</th>
                 <th class="right last">Line Total</th>
            </tr>
            <apex:repeat value="{!LinesExtended}" var="line">
            <tr>
                    <td class="first"><Apex:OutputText value="{!line.lineNumber}"/></td>
                    <td>
                        <div class="{!if(line.line.SCMC__Item_Number__c != '', '', 'hide')}"><Apex:OutputField value="{!line.line.SCMC__Item_Number__c}"/></div>
                        <div class="{!if(line.line.SCMC__Item_Description__c != '', '', 'hide')}" style="font-size: 10px;"><Apex:OutputField value="{!line.line.SCMC__Item_Description__c}"/></div>
                    </td>
                    <td>
                    	<apex:outputText rendered="{!displayAdditionInfo}">
	                        <apex:outputText rendered="{!if(line.line.SCMC__Lot_Number__c != '', true, false)}">
	                            <div><span style="font-size: 9px; font-style:italic;">Lot:</span> {!line.line.SCMC__Lot_Number__c}</div>
	                        </apex:outputText>
	                        <apex:outputText rendered="{!if(line.line.SCMC__Good_Serial_Number__c != '' && line.displayInboundOutboundSerialNumbers == false, true, false)}">
	                            <div><span style="font-size: 9px; font-style:italic;">SN:</span> {!line.line.SCMC__Good_Serial_Number__c}</div>
	                        </apex:outputText>
	                        <apex:outputText rendered="{!if(line.line.SCMC__Serial_Numbers__c != '', true, false)}">
	                            <div><span style="font-size: 9px; font-style:italic;">SN:</span> {!line.line.SCMC__Serial_Numbers__c}</div>
	                        </apex:outputText>
	                        <apex:repeat value="{!line.serialNumberList}" var="sn" rendered="{!line.displayInboundOutboundSerialNumbers}">
	                            <div style="padding-bottom: 2px; "><span style="font-size: 9px; font-style:italic;">SN:</span> {!sn.SCMC__Serial_Number__c}</div>
	                        </apex:repeat>
	                        <apex:outputText rendered="{!if(line.line.SCMC__Serial_Numbers__c == '' && line.line.SCMC__Good_Serial_Number__c == '' && line.line.SCMC__Lot_Number__c == '' && line.displayInboundOutboundSerialNumbers == false, true, false)}">
	                            <div>&nbsp;</div>
	                        </apex:outputText>
						</apex:outputText>
	                 	<apex:outputText rendered="{!if(displayAdditionInfo, false, true)}">
	                 		<div>&nbsp;</div>
	                 	</apex:outputText>
                    </td>
                    <td><Apex:OutputField value="{!line.line.SCMC__Quantity__c}"/></td>
                    <td class="right"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.line.SCMC__Price_With_Options__c}" /></apex:outputText>&nbsp;</td>
                    <td class="right last"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.line.SCMC__Amount__c}" /></apex:outputText></td>
            </tr>
            <tr class="{!if(line.hasBillOfMaterials, '', 'hide')}">
                <td class="first last" style="padding-left: 50px;" colspan="6">
                    <div class="kit-title">Virtual Kit Lines</div>
                    <div class="kit-data"> 
                        <table class="kit-data" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <th>Item</th>
                                <th>Description</th>
                                <th>Quantity Per Assembly</th>
                                <th>UOM</th>
                            </tr>
	                        <apex:repeat value="{!line.billOfMaterialsInvoiceLine}" var="kit">
	                                <tr>
	                                    <td>{!kit.SCMC__Item__r.Name}</td>
	                                    <td>{!kit.SCMC__Item__r.SCMC__Item_Description__c}</td>
	                                    <td>{!kit.SCMC__Quantity__c}</td>
	                                    <td>{!kit.SCMC__Item__r.SCMC__Stocking_UOM__r.Name}</td>
	                                </tr>
	                        </apex:repeat>
	                    </table>
	                </div>
	            </td>
	        </tr>
	       	<apex:outputText escape="false" rendered="{!line.hasCreditLines}">
	       		<apex:repeat value="{!line.creditLineList}" var="cr">
	        		<tr>
		                 <td class="right first" colspan="5" style="font-style: italic; font-size: 9px; padding:  0 3px 0 0; margin-top: -20px;">Credited Amount: </td>
		                 <td class="right last" style="padding:  0 3px 0 0; font-size: 9px; margin-top: -20px;">-<apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!cr.SCMC__Amount__c}" /></apex:outputText></td>
	        		</tr>
				</apex:repeat>
	       		<tr>
	                 <td class="right first" colspan="5" style="font-style: italic; font-size: 9px; padding:  0 3px 3px 0; margin-top: -8px;">New Line Total After Credit: </td>
	                 <td class="right last" style="padding: 0 3px 3px 0; margin-top: -8px;"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.newLineTotal}" /></apex:outputText></td>
	       		</tr>
	       	</apex:outputText>
	        <tr>
	            <td colspan="6" class="footer"></td>
	        </tr>
        </apex:repeat>
        <apex:repeat value="{!FreightLinesExtended}" var="line">
            <tr>
                <td class="first">&nbsp;</td>
                <td>
                    <div><Apex:OutputText value="{!line.lineItem}"/></div>
                    <div class="{!if(line.line.SCMC__Item_Description__c != '', '', 'hide')}" style="font-size: 10px;"><Apex:OutputField value="{!line.line.SCMC__Item_Description__c}"/></div>
                </td>
                <td>&nbsp;</td>
                <td><Apex:OutputField value="{!line.line.SCMC__Quantity__c}"/></td>
                <td class="right"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.line.SCMC__Price__c}" /></apex:outputText>&nbsp;</td>
                <td class="right last"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.line.SCMC__Extended_Price__c}" /></apex:outputText></td>
            </tr>
	       	<apex:outputText escape="false" rendered="{!line.hasCreditLines}">
	       		<apex:repeat value="{!line.creditLineList}" var="cr">
	        		<tr>
		                 <td class="right first" colspan="5" style="font-style: italic; font-size: 9px; padding:  0 3px 0 0; margin-top: -20px;">Credited Amount: </td>
		                 <td class="right last" style="padding:  0 3px 0 0; font-size: 9px; margin-top: -20px;">-<apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!cr.SCMC__Amount__c}" /></apex:outputText></td>
	        		</tr>
				</apex:repeat>
	       		<tr>
	                 <td class="right first" colspan="5" style="font-style: italic; font-size: 9px; padding:  0 3px 3px 0; margin-top: -8px;">New Line Total After Credit: </td>
	                 <td class="right last" style="padding: 0 3px 3px 0; margin-top: -8px;"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!line.newLineTotal}" /></apex:outputText></td>
	       		</tr>
	       	</apex:outputText>
            <tr>
                <td colspan="6" class="footer"></td>
            </tr>
        </apex:repeat>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2" class="totals-title">Subtotal</td>
            <td class="count"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!SubTotal.SCMC__Amount__c}" /></apex:outputText></td>
        </tr>
       	<apex:outputText escape="false" rendered="{!if(CreditTotal > 0, true, false)}">
	        <tr>
	            <td colspan="3">&nbsp;</td>
	            <td colspan="2" class="totals-title">Credit Amount</td>
	            <td class="count"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!CreditTotal}" /></apex:outputText></td>
	        </tr>
		</apex:outputText>	
        <tr style="{!if(inv.SCMC__Discount__c > 0, '', 'display: none;')}">
            <td colspan="3">&nbsp;</td>
            <td colspan="2" class="totals-title">Discount</td>
            <td class="count"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!inv.SCMC__Discount__c}" /></apex:outputText></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2" class="totals-title">Sales Tax</td>
            <td class="count"><apex:outputText value="{0, number, #,###,##0.00}"><apex:param value="{!TaxTotal.SCMC__Amount__c}" /></apex:outputText></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2" class="totals-title">Total</td>
            <td class="count"><apex:outputField value="{!GrandTotal.SCMC__Amount__c}"/></td>
        </tr>
    </table>
    
    </div>
    
</apex:componentBody>
</apex:component>