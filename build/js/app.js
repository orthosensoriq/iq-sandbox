var orthosensor;
(function (orthosensor) {
    angular.module('app.core', [
        // Angular modules 
        'ngAnimate',
        'ngSanitize',
        'ngTouch',
        'ngMessages',
        'ui.router',
        // Custom modules
        'common',
        'common.services',
        // 3rd Party Modules       
        // 'googlechart',
        'monospaced.qrcode',
        'nvd3',
        'ui.bootstrap',
        'ui.bootstrap.datetimepicker',
        'angular-ladda',
        'angularMoment',
    ]);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=app.core.module.js.map
// import { module } from 'angular';
var orthosensor;
(function (orthosensor) {
    angular.module('app', [
        'app.core',
        'app.services'
    ]);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=app.module.js.map
var orthosensor;
(function (orthosensor) {
    angular.module('app.services', []);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=app.services.module.js.map
var orthosensor;
(function (orthosensor) {
    angular.module('orthosensor', [
        'app',
        'app.core',
        'app.common',
        'angular-confirm',
        'orthosensor.services',
        'orthosensor.data',
        'orthosensor.dashboard',
        'orthosensor.admin',
    ]);
    angular.module('app.common', []);
    angular.module('orthosensor.services', [
        'app.core'
    ]);
    angular.module('orthosensor.dashboard', [
        'app.core',
        'orthosensor.services'
    ]);
    angular.module('orthosensor.data', []);
    angular.module('orthosensor.admin', [
        'app.core',
        'orthosensor.data',
        'orthosensor.services'
    ]);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=orthosensor.module.js.map
(function () {
    'use strict';
    // Define the common module 
    // Contains services:
    //  - common
    //  - logger
    //  - spinner
    var commonModule = angular.module('common', []);
    // Must configure the common service and set its 
    // events via the commonConfigProvider
    commonModule.provider('commonConfig', function () {
        this.config = {};
        this.$get = function () {
            return {
                config: this.config
            };
        };
    });
    commonModule.factory('common', ['$q', '$rootScope', '$timeout', 'commonConfig', 'logger', common]);
    function common($q, $rootScope, $timeout, commonConfig, logger) {
        var throttles = {};
        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            $timeout: $timeout,
            // generic
            activateController: activateController,
            createSearchThrottle: createSearchThrottle,
            debouncedThrottle: debouncedThrottle,
            isNumber: isNumber,
            logger: logger,
            textContains: textContains
        };
        return service;
        function activateController(promises, controllerId) {
            return $q.all(promises).then(function (eventArgs) {
                var data = { controllerId: controllerId };
                $broadcast(commonConfig.config.controllerActivateSuccessEvent, data);
            });
        }
        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }
        function createSearchThrottle(viewmodel, list, filteredList, filter, delay) {
            // After a delay, search a viewmodel's list using 
            // a filter function, and return a filteredList.
            // custom delay or use default
            delay = +delay || 300;
            // if only vm and list parameters were passed, set others by naming convention 
            if (!filteredList) {
                // assuming list is named sessions, filteredList is filteredSessions
                filteredList = 'filtered' + list[0].toUpperCase() + list.substr(1).toLowerCase(); // string
                // filter function is named sessionFilter
                filter = list + 'Filter'; // function in string form
            }
            // create the filtering function we will call from here
            var filterFn = function () {
                // translates to ...
                // vm.filteredSessions 
                //      = vm.sessions.filter(function(item( { returns vm.sessionFilter (item) } );
                viewmodel[filteredList] = viewmodel[list].filter(function (item) {
                    return viewmodel[filter](item);
                });
            };
            return (function () {
                // Wrapped in outer IFFE so we can use closure 
                // over filterInputTimeout which references the timeout
                var filterInputTimeout;
                // return what becomes the 'applyFilter' function in the controller
                return function (searchNow) {
                    if (filterInputTimeout) {
                        $timeout.cancel(filterInputTimeout);
                        filterInputTimeout = null;
                    }
                    if (searchNow || !delay) {
                        filterFn();
                    }
                    else {
                        filterInputTimeout = $timeout(filterFn, delay);
                    }
                };
            })();
        }
        function debouncedThrottle(key, callback, delay, immediate) {
            // Perform some action (callback) after a delay. 
            // Track the callback by key, so if the same callback 
            // is issued again, restart the delay.
            var defaultDelay = 1000;
            delay = delay || defaultDelay;
            if (throttles[key]) {
                $timeout.cancel(throttles[key]);
                throttles[key] = undefined;
            }
            if (immediate) {
                callback();
            }
            else {
                throttles[key] = $timeout(callback, delay);
            }
        }
        function isNumber(val) {
            // negative or positive
            return /^[-]?\d+$/.test(val);
        }
        function textContains(text, searchText) {
            return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
        }
    }
})();

angular.module("common.services", ["ngResource"])
    .constant("appSettings", {
    serverPath: "http://localhost:44023/"
});
//# sourceMappingURL=common.services.module.js.map
var orthosensor;
(function (orthosensor) {
    angular
        .module('orthosensor')
        .component('app', {
        controller: 'ShellCtrl',
        templateUrl: 'app/layout/shell.html'
    });
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=app.component.js.map
// Include in index.html so that app level exceptions are handled.
// Exclude from testRunner.html which should run exactly what it wants to run
(function () {
    'use strict';
    var app = angular.module('app');
    // Configure by setting an optional string value for appErrorPrefix.
    // Accessible via config.appErrorPrefix (via config value).
    app.config(['$provide', function ($provide) {
            $provide.decorator('$exceptionHandler', ['$delegate', 'config', 'logger', extendExceptionHandler]);
        }]);
    // Extend the $exceptionHandler service to also display a toast.
    function extendExceptionHandler($delegate, config, logger) {
        var appErrorPrefix = config.appErrorPrefix;
        var logError = logger.getLogFn('app', 'error');
        return function (exception, cause) {
            $delegate(exception, cause);
            if (appErrorPrefix && exception.message.indexOf(appErrorPrefix) === 0) {
                return;
            }
            var errorData = { exception: exception, cause: cause };
            var msg = appErrorPrefix + exception.message;
            logError(msg, errorData, true);
        };
    }
})();

(function () {
    'use strict';
    var app = angular.module('orthosensor');
    // Configure Toastr
    toastr.options.timeOut = 5000;
    //toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.positionClass = 'toast-top-center';
    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle'
    };
    var config = {
        appErrorPrefix: '[HT Error] ',
        docTitle: 'OrthoLogIQ',
        events: events,
        //remoteServiceName: remoteServiceName,
        version: '0.1.0',
        // 'userType': {
        //     "practice": "Partner Partner Community User",
        //     "hospital": "Hospital Partner Community User",
        //     "surgicalTech": "IQ Surgical Tech",
        //     "admin": "Administrative Account"
        // }
    };
    app.constant('dateFormat', 'shortDate');
    app.value('config', config);
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);
    //#region Configure the common services via commonConfig
    app.config(['commonConfigProvider', function (cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
        cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
    }]);
    app.config(["$locationProvider", function ($locationProvider) {
        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        // });
    }]);
    app.config(["laddaProvider", function (laddaProvider) {
        laddaProvider.setOption({
            style: 'zoom-in',
            spinnerSize: 35,
            spinnerColor: '#ffffff'
        });
    }]);
    app.value('$routerRootComponent', 'orthosensor');

    function appConfig($rootScopeProvider) {
        $rootScopeProvider.digestTtl(25);
    }

    
    app.config(['$rootScopeProvider', appConfig]);
   
})();

(function () {
    'use strict';
    angular
        .module('orthosensor')
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', routeConfigurator]);
    function routeConfigurator($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        //$urlRouterProvider.otherwise({ redirectTo: '/' });
        // Now set up the states
        var dashboardUrl = 'app/layout/dashboard.js';
        $httpProvider.interceptors.push('sessionTimeoutHandler');
        $urlRouterProvider.otherwise('patientList');
        $stateProvider
            .state('home', {
            url: '#',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': { template: '<os-dashboard></os-dashboard>' }
            }
        })
            .state('dashboard', {
            url: '/dashboard',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': { template: '<os-dashboard></os-dashboard>' }
            },
        })
            .state('admin', {
            url: '/admin',
            views: {
                'header': { template: '<os-top-menu title="Admin"></os-top-menu>' },
                'content': {
                    template: '<admin></admin>'
                }
            }
        })
            .state('admin.doctors', {
            url: '/doctors',
            template: '<doctor-list></doctor-list>'
        })
            .state('admin.hospitals', {
            url: '/admin/hospitals',
            template: "<hospital-list></hospital-list>"
        })
            .state('admin.hospitalDetail', {
            url: '/hospitalDetail/:id',
            template: "<hospital-detail></hospital-detail>"
        })
            .state('admin.people', {
            url: '/admin/people',
            template: '<h1>People!</h1>'
        })
            .state('admin.users', {
            url: '/admin/users',
            template: '<user-list></user-list>'
        })
            .state('reports', {
            url: '/reports',
            views: {
                'header': { template: '<os-top-menu title="Reports"></os-top-menu>' },
                'content': {
                    template: '<os-reports></os-reports>'
                }
            }
        })
            .state('reports.promDeltaChart', {
            url: "/reports/promDeltaChart",
            template: '<div class = "row"><os-mean-change-in-prom-score></os-mean-change-in-prom-score><os-patients-koos-score-list></os-patients-koos-score-list></div>',
        })
            .state('reports.phaseCountChart', {
            url: "/reports/phaseCountChart",
            template: '<os-patient-phase-count chartType="bar"></os-patient-phase-count>',
        })
            .state('reports.kneeBalanceChart', {
            url: "/reports/kneeBalanceChart",
            template: '<os-knee-balance-data chartType2="line"></os-knee-balance-data><os-knee-balance-patient-cards></os-knee-balance-patient-cards>',
        })
            .state('reports.caseActivityChart', {
            url: "/reports/caseActivityChart",
            template: '<os-case-activity-chart></os-case-activity-chart>',
        })
            .state('test', {
            url: '/test',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': {
                    template: '<os-dashboard-test></os-dashboard-test>'
                }
            }
        })
            .state('patientTest', {
            url: '/patientTest',
            views: {
                'header': { template: '<os-top-menu title="Dashboard"></os-top-menu>' },
                'content': {
                    template: '<os-patient-test></os-patient-test>'
                }
            }
        })
            .state('patientList', {
            url: '/patient-list',
            views: {
                'header': {
                    templateUrl: 'app/patientList/patientListHeader.html',
                    controller: 'PatientListController',
                    controllerAs: '$ctrl'
                },
                'content': {
                    templateUrl: 'app/patientList/patientListContent.html',
                    // template: '<os-patient-list><os-patient-list>'
                    controller: 'PatientListController',
                    controllerAs: '$ctrl'
                }
            }
        })
            .state('addPatient', {
            url: '/addPatient',
            views: {
                'header': {
                    template: '<os-top-menu title="Add Patient"></os-top-menu>',
                },
                'content': {
                    template: '<os-add-patient></os-add-patient>',
                }
            }
        })
            .state('addPatientCase', {
            url: '/addPatientCase',
            views: {
                'header': {
                    template: '<os-top-menu title="Add Patient Case"></os-top-menu>',
                },
                'content': {
                    template: '<os-add-patient-case></os-add-patient-case>',
                }
            }
        })
            .state('patientDetails', {
            url: '/patient-details',
            views: {
                'header': {
                    templateUrl: 'app/patientDetail/patientDetailHeader.html',
                    controller: 'PatientDetailsController',
                    controllerAs: '$ctrl'
                },
                'content': {
                    templateUrl: 'app/patientDetail/patientDetailContent.html',
                    controller: 'PatientDetailsController',
                    controllerAs: '$ctrl'
                }
            }
        })
            .state('caseDetails', {
            url: '/case-details',
            views: {
                'header': {
                    templateUrl: 'app/caseDetails/caseDetailHeader.html',
                    controller: 'CaseDetailsController'
                },
                'content': {
                    templateUrl: 'app/caseDetails/caseDetailContent.html',
                    controller: 'CaseDetailsController'
                }
            },
            resolve: {
                events: function (CaseDetailsFactory) {
                    //return dashboardService.getHospitals();
                    return CaseDetailsFactory.LoadCaseEvents()
                        .then(function (data) {
                        //var hospitals = data;
                        console.log(data);
                        return data;
                    }, function (error) {
                        console.log(error);
                    });
                },
                sensorRefs: function (CaseDetailsFactory) {
                    return CaseDetailsFactory.loadSensorRefs().then(function (data) {
                        //console.log(data);
                        return data;
                    }, function (error) {
                    });
                }
            }
        })
            .state('patientActivityChart', {
            url: "/patientActivityChart",
            views: {
                'header': { template: '<top-nav title="Patient Activity Chart"> </top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><patient-activity></patient-activity></div></div></div>'
                }
            }
        })
            .state('practiceActivityChart', {
            url: "/practiceActivityChart",
            views: {
                'header': { template: '<top-nav title="Practice Activity Chart"></top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><practice-activity></practice-activity></div></div></div>'
                }
            }
        })
            .state('promActivityChart', {
            url: "/promActivityChart",
            views: {
                'header': { template: '<top-nav title="PROM Activity Chart"> </top-nav>' },
                'content': {
                    template: '<div class="mainbar"><div class="col-sm-10 col-sm-offset-1"><div class="main-content"><prom-activity></prom-activity></div></div></div>'
                }
            }
        })
            .state('notifications', {
            url: "/notifications",
            views: {
                'header': { template: '<os-top-menu title="Notifications"></os-top-menu>' },
                'content': {
                    template: '<os-notifications></os-notifications>'
                }
            }
        });
        $locationProvider.html5Mode(true);
    }
})();
//# sourceMappingURL=config.route.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientTestController = (function () {
            function PatientTestController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state) {
                this.DashboardService = DashboardService;
                this.UserService = UserService;
                this.DashboardDataService = DashboardDataService;
                this.PatientService = PatientService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.$state = $state;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            PatientTestController.prototype.$onInit = function () {
                var practiceId = '0017A00000M803ZQAR';
                practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
            };
            return PatientTestController;
        }());
        PatientTestController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        var PatientTest = (function () {
            function PatientTest() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = PatientTestController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/patientTest.component.html';
            }
            return PatientTest;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientTest', new PatientTest());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientTest.component.js.map
// import { UserService } from './services/UserService';
(function () {
    angular
        .module('orthosensor')
        .run(runBlock);
    runBlock.$inject = ['$state', '$rootScope', '$timeout', 'InitFactory', '$window', 'UserService', 'DashboardService', 'PatientListFactory', 'SFDataService', 'HospitalService'];
    function runBlock($state, $rootScope, $timeout, InitFactory, $window, UserService, DashboardService, PatientListFactory, SFDataService, HospitalService) {
        $rootScope.handleSessionTimeout = function (event) {
            if (!event.status && event.statusCode === 500) {
                $window.location = '/login';
            }
        };
        InitFactory.isUserAdmin()
            .then(function (data) {
            // console.log(data);
            $rootScope.isUserAdmin = data;
            UserService.isAdmin = data;
        }, function (error) {
        });
        InitFactory.getCurrentUser()
            .then(function (data) {
            // console.log(data);
            $rootScope.currentUser = data;
            var sfUser = data;
            var user = UserService.convertSFToObject(sfUser);
            UserService.user = user;
            console.log(user);
            console.log(UserService.user);
            // let startPage = 'dashboard';
            var startPage = 'patientList';
            var hospitalPracticeId = '';
            DashboardService.setSurveys();
            // change if we get user info!
            // $timeout(() => {
            switch (UserService.user.userProfile) {
                case 2 /* Surgeon */:
                    console.log('surgeon Account');
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        HospitalService.loadHospitalByPractice(UserService.user.accountId);
                        console.log(HospitalService.practices);
                    })
                        .then(function () {
                        startPage = 'dashboard';
                        $state.go(startPage);
                    });
                // break;
                case 1 /* HospitalAdmin */:
                    hospitalPracticeId = user.accountId;
                    console.log('hospital Account');
                    HospitalService.setPractices(hospitalPracticeId);
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        startPage = 'dashboard';
                        $state.go(startPage);
                    });
                // break;
                case 3 /* PracticeAdmin */:
                    hospitalPracticeId = user.accountId;
                    // console.log('practice Account');
                    // DashboardService.loadHospitalPractices(hospitalPracticeId);
                    return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                        .then(function () {
                        console.log(HospitalService.surgeons);
                    })
                        .then(function () {
                        HospitalService.loadHospitalByPractice(UserService.user.accountId);
                        console.log(HospitalService.practices);
                    })
                        .then(function () {
                        startPage = 'patientList';
                        $state.go(startPage);
                    });
                // case 'Customer Community User':
                //     hospitalPracticeId = user.accountId;
                //     // console.log('practice Account');
                //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                //         .then(() => {
                //             console.log(HospitalService.surgeons);
                //         })
                //         .then(() => {
                //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                //             console.log(HospitalService.practices);
                //         })
                //         .then(() => {
                //             startPage = 'patientList';
                //             $state.go(startPage);
                //         });
                // case 'Customer Community Plus User':
                //     hospitalPracticeId = user.accountId;
                //     // console.log('practice Account');
                //     // DashboardService.loadHospitalPractices(hospitalPracticeId);
                //     return HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                //         .then(() => {
                //             console.log(HospitalService.surgeons);
                //         })
                //         .then(() => {
                //             HospitalService.loadHospitalByPractice(UserService.user.accountId);
                //             console.log(HospitalService.practices);
                //         })
                //         .then(() => {
                //             startPage = 'patientList';
                //             $state.go(startPage);
                //         });
                // break;
                default:
                    // console.log(DashboardService.hospitals);
                    // DashboardService.loadPractices(hospitalPracticeId);
                    HospitalService.getHospitals()
                        .then(function (data) {
                        // console.log(data);
                        SFDataService.loadHospitalPractices(HospitalService.hospitals[0].id)
                            .then(function (data) {
                            // console.log(data);
                            HospitalService.practices = HospitalService.convertSFPracticeToObject(data);
                            HospitalService.loadAllSurgeons()
                                .then(function () {
                                console.log(HospitalService.surgeons);
                                startPage = 'dashboard';
                                $state.go(startPage);
                            });
                        }, function (error) {
                            console.log(error);
                        });
                    });
            }
        });
        // TODO: move this to a service at some point!
        $rootScope.dateformat = dateformat;
        function dateformat(milliseconds) {
            console.log('- using date format in run js!!! -');
            if (!milliseconds) {
                console.log('Not milliseconds');
                return '--';
            }
            else {
                var dateValue = moment.utc(milliseconds).format('LL');
                console.log(dateValue);
                return dateValue;
            }
        }
    }
})();
//# sourceMappingURL=run.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardTestController = (function () {
            function DashboardTestController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state) {
                this.DashboardService = DashboardService;
                this.UserService = UserService;
                this.DashboardDataService = DashboardDataService;
                this.PatientService = PatientService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.$state = $state;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            DashboardTestController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = '0017A00000M803ZQAR';
                practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                // this.DashboardDataService.getScheduledPatients(practiceId)
                //     .then((data: any) => {
                //         this.patientsScheduled = data.length;
                //         console.log(this.patients);
                //     }, (error: any) => {
                //         console.log(error);
                //         return error;
                //     });
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then(function (data) {
                    _this.patientsUnderBundleCount = data.length;
                    console.log(_this.patientsUnderBundleCount);
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return DashboardTestController;
        }());
        DashboardTestController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state'];
        var DashboardTest = (function () {
            function DashboardTest() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = DashboardTestController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/testPage.html';
            }
            return DashboardTest;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osDashboardTest', new DashboardTest());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=testPage.js.map
var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('admin', {
        templateUrl: "app/admin/admin.component.html",
        controller: 'AdminController',
        controllerAs: '$ctrl'
    });
})(app || (app = {}));
//# sourceMappingURL=admin.component.js.map
(function () {
    'use strict';
    var controllerId = 'AdminController';
    AdminController.$inject = ['$timeout', 'UserService'];
    function AdminController($timeout, UserService) {
        /* jshint validthis:true */
        var $ctrl = this;
        $ctrl.title = 'Admin Test';
        getUser = getUser;
        setUser = setUser;
        $ctrl.addSensor = addSensor;
        activate();
        function activate() {
            // [setUser()];
        }
        //         function login(force) {
        // force.login().then(function () {
        //                 force.query('select id, name from contact limit 50')
        //                 .then(
        //                     function (contacts) {
        //                         vm.contacts = contacts.records;
        //                         console.log(vm.contacts);
        //                     });
        //             });
        //         }
        function getUser() {
            // $ctrl.user = UserService.getUser();
        }
        function setUser() {
            UserService.setUser();
        }
        function addSensor() {
            // var l = Ladda.create(document.querySelector('.sensor-btn'));
            // console.log(l);
            $ctrl.loading = true;
            $timeout(function () {
                $ctrl.loading = false;
            }, 4000);
        }
    }
    angular
        .module('orthosensor.admin')
        .controller(controllerId, AdminController);
})();
//# sourceMappingURL=admin.controller.js.map
(function () {
    'use strict';
    angular.module('app.common').factory('logger', ['$log', logger]);
    function logger($log) {
        var service = {
            getLogFn: getLogFn,
            log: log,
            logError: logError,
            logSuccess: logSuccess,
            logWarning: logWarning
        };
        return service;
        function getLogFn(moduleId, fnName) {
            fnName = fnName || 'log';
            switch (fnName.toLowerCase()) {
                case 'success':
                    fnName = 'logSuccess';
                    break;
                case 'error':
                    fnName = 'logError';
                    break;
                case 'warn':
                    fnName = 'logWarning';
                    break;
                case 'warning':
                    fnName = 'logWarning';
                    break;
            }
            var logFn = service[fnName] || service.log;
            return function (msg, data, showToast) {
                logFn(msg, data, moduleId, (showToast === undefined) ? true : showToast);
            };
        }
        function log(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'info');
        }
        function logWarning(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'warning');
        }
        function logSuccess(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'success');
        }
        function logError(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'error');
        }
        // commented out toast messages        
        function logIt(message, data, source, showToast, toastType) {
            var write = (toastType === 'error') ? $log.error : $log.log;
            source = source ? '[' + source + '] ' : '';
            write(source, message, data);
            if (showToast) {
                if (toastType === 'error') {
                    toastr.error(message);
                }
                else if (toastType === 'warning') {
                    toastr.warning(message);
                }
                else if (toastType === 'success') {
                    toastr.success(message);
                }
            }
        }
    }
})();
//# sourceMappingURL=logger.js.map
(function () {
    'use strict';
    // Must configure the common service and set its 
    // events via the commonConfigProvider
    angular.module('common')
        .factory('spinner', ['common', 'commonConfig', spinner]);
    function spinner(common, commonConfig) {
        var service = {
            spinnerHide: spinnerHide,
            spinnerShow: spinnerShow
        };
        return service;
        function spinnerHide() { spinnerToggle(false); }
        function spinnerShow() { spinnerToggle(true); }
        function spinnerToggle(show) {
            common.$broadcast(commonConfig.config.spinnerToggleEvent, { show: show });
        }
    }
})();
//# sourceMappingURL=spinner.js.map
(function () {
    "use strict";
    angular
        .module("common.services")
        .factory(webContentResource, ["$resource", "appSettings", webContentResource]);
    function webContentResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/webContent/:id");
    }
}());
//# sourceMappingURL=webContentResource.js.map
(function () {
    angular
        .module('orthosensor')
        .controller('CaseDetailsController', CaseDetailsController);
    CaseDetailsController.$inject = ['$window', '$scope', '$rootScope', '$state', '$uibModal', '$confirm', 'CaseDetailsFactory', 'LogFactory', 'SurveyService', 'PatientService', 'SensorService', 'CaseService', 'moment', 'events', 'sensorRefs'];
    function CaseDetailsController($window, $scope, $rootScope, $state, $uibModal, $confirm, CaseDetailsFactory, LogFactory, SurveyService, PatientService, SensorService, CaseService, moment, events, sensorRefs) {
        var ctrl = this; //can we mix $scope with controller as?
        //variables
        //$scope.patient = $rootScope.patient;
        $scope.case = $rootScope.case;
        $scope.surveyGraphable = true;
        $scope.toggleValue = 'chart';
        $scope.sensorEntryType = 'barcode';
        $scope.implantEntryType = 'barcode';
        $scope.showGraph = false;
        //methods        
        $scope.goBack = goBack;
        $scope.switchEvent = switchEvent;
        $scope.surveyChange = surveyChange;
        $scope.canGraph = canGraph;
        $scope.editPatient = editPatient;
        $scope.togglePatientDetailsDrawer = togglePatientDetailsDrawer;
        $scope.toggleOutcomes = toggleOutcomes;
        // $scope.openSurveyLinkQRCode = openSurveyLinkQRCode;
        $scope.addSensor = addSensor;
        $scope.checkAndSaveImplantBarcode = checkAndSaveImplantBarcode;
        $scope.addImplant = addImplant;
        $scope.refreshEventsAndSurveys = refreshEventsAndSurveys;
        $scope.refreshEvents = refreshEvents;
        $scope.deleteSensor = deleteSensor;
        $scope.addNewCaseNote = addNewCaseNote;
        $scope.editNote = editNote;
        $scope.deleteNote = deleteNote;
        //activate should include all start up functions - for now it does not include all
        // and will be refactored over time 
        activate();
        function activate() {
            // retrieved from resolver
            this.events = events;
            $scope.events = events;
            // console.log($scope.events);
            $scope.patient = PatientService.patient;
            CaseService.setCurrentCase($scope.case);
            //console.log($scope.patient);
            $scope.isPatientAnonymous = PatientService.isAnonymous();
            LogFactory.logReadsObject($scope.case.Id, 'Case__c');
            loadCaseEvents();
            //retrieved from resolver
            $scope.sensorRefs = sensorRefs;
            loadProducts();
            //loadSensorRefs();
            // getCompletedSurveys();
        }
        function goBack() {
            $window.history.back();
        }
        function loadCaseEvents() {
            for (var i = 0; i < $scope.events.length; i++) {
                if ($scope.events[i].Event_Type_Name__c === 'Procedure') {
                    $scope.procedure = $scope.events[i];
                    CaseService.currentProcedure = $scope.procedure;
                    $scope.clinicalData = $scope.procedure.Clinical_Data__r[0];
                    CaseService.clinicalData = $scope.procedure.Clinical_Data__r[0];
                    console.log($scope.clinicalData);
                    if ($scope.events[i].Implant_Components__r) {
                        for (var j = 0; j < $scope.events[i].Implant_Components__r.length; j++) {
                            var expDate = $scope.events[i].Implant_Components__r[j].Expiration_Date__c;
                            // console.log(expDate);
                            expDate = moment(expDate).add(1, 'd');
                            // console.log(expDate);
                            $scope.events[i].Implant_Components__r[j].Expiration_Date__c = expDate;
                        }
                    }
                }
                if ($rootScope.event && $rootScope.event.Event_Type_Name__c === $scope.events[i].Event_Type_Name__c) {
                    $scope.event = $scope.events[i];
                    CaseDetailsFactory.setEvent($scope.event);
                }
            }
            //get survey events
            if ($scope.events.length > 0) {
                SurveyService.setEventTypes($scope.events);
            }
            //console.log($rootScope.event);
            if (!$rootScope.event) {
                $scope.event = $scope.procedure;
                // CaseDetailsFactory.setEvent($scope.event);
            }
            else {
                if (!$scope.event) {
                    //should not reach this
                    if ($rootScope.event) {
                        console.log($rootScope.event);
                        $scope.event = $rootScope.event;
                        // CaseDetailsFactory.setEvent($scope.event);
                    }
                    else {
                        // if there is no event passed in
                        if ($scope.events.length > 0) {
                            $scope.event = $scope.events[0];
                        }
                        else {
                            $state.go('patient-list');
                        }
                    }
                }
            }
            ctrl.event = $scope.event;
            CaseDetailsFactory.setEvent($scope.event);
            console.log($scope.event);
            setSurveyList($scope.event);
            LogFactory.logReadsObject($scope.event.Id, 'Event__c');
        }
        function refreshEvents() {
            return CaseDetailsFactory.LoadCaseEvents()
                .then(function (data) {
                console.log(data);
                $scope.events = data;
                loadCaseEvents();
            });
        }
        // $scope.escapeURL = function (urlString) {
        //     return urlString.replace(/&amp;/g, '&');
        // };
        function setSurveyList(event) {
            //console.log($scope.event.Id);
            SurveyService.retrieveSurveysByEventId(event.Id)
                .then(function (data) {
                $scope.surveys = data;
                console.log($scope.surveys);
                if (data.length > 0) {
                    SurveyService.setSurveys(data);
                    console.log(data);
                    if ($scope.surveys.length > 0) {
                        setSurveysAndScores();
                    }
                }
            });
        }
        // called at initialization - gets first survey
        function setSurveysAndScores() {
            //console.log($scope.surveys);
            if ($scope.surveys.length > 0) {
                var currentSurvey = SurveyService.getSurvey();
                if (currentSurvey !== undefined) {
                    var found = false;
                    for (var i = 0; i < $scope.surveys.length; i++) {
                        if ($scope.surveys[0].Name__c === currentSurvey.Name__c) {
                            //set it to the matching survey
                            $scope.currentSurvey = $scope.surveys[i];
                            found = true;
                        }
                    }
                    if (!found) {
                        // survey not found in current event so set it to the first survey
                        $scope.currentSurvey = $scope.surveys[0];
                    }
                }
                else {
                    //set it to the first survey
                    $scope.currentSurvey = $scope.surveys[0];
                }
                SurveyService.setSurvey($scope.currentSurvey);
                $scope.renderedSurvey = $scope.currentSurvey.Name__c;
                //canGraphSurvey = canGraph($scope.currentSurvey);
                loadSurveyScores($scope.case.Id, $scope.currentSurvey.Id);
            }
        }
        function loadSurveyScores(caseId, surveyId) {
            console.log(caseId + ' ' + surveyId);
            var survey = SurveyService.getSurveyById(surveyId)
                .then(function (data) {
                survey = data;
                SurveyService.setSurvey(survey);
                //if ($scope.chartCategories) {
                CaseDetailsFactory.loadSurveyScores(caseId, surveyId)
                    .then(function (scores) {
                    console.log(scores);
                    $scope.showGraph = (scores.length > 0);
                    console.log($scope.showGraph);
                    if ($scope.showGraph) {
                        $scope.chartCategories = SurveyService.getChartCategories();
                        $scope.chartData = SurveyService.getChartData(scores);
                        console.log($scope.chartData);
                        $scope.chartGridLines = c3.generate(SurveyService.setGraphParameters('#kss-chart', $scope.chartData));
                    }
                    //$scope.chartGridLines.select($scope.questions, [$scope.chartCategories.indexOf($scope.event.Event_Type_Name__c)], true);
                }, function (error) {
                });
                //}
            });
        }
        $rootScope.expDateformat = function (milliseconds) {
            if (!milliseconds) {
                return '--';
            }
            else {
                return moment.utc(milliseconds).format('MM/YYYY');
            }
        };
        function toggleOutcomes(viewID) {
            if ($scope.toggleValue === 'data') {
                $scope.toggleValue = 'chart';
            }
            else {
                $scope.toggleValue = 'data';
            }
        }
        ;
        function switchEvent(event) {
            console.log(event);
            $scope.event = event;
            $rootScope.event = event;
            CaseDetailsFactory.setEvent($scope.event);
            setSurveyList(event);
            // if ($scope.surveys.length > 0) {
            //     setSurveysAndScores();
            // }    
            if ($scope.chartGridLines) {
                $scope.chartGridLines.select($scope.questions, [$scope.chartCategories.indexOf($scope.event.Event_Type_Name__c)], true);
            }
            LogFactory.logReadsObject(event.Id, 'Event__c');
        }
        $scope.patientDetailsVisible = false;
        function togglePatientDetailsDrawer() {
            $scope.patientDetailsVisible = !$scope.patientDetailsVisible;
        }
        $scope.open = { expDate: false };
        $scope.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.open[whichDate] = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };
        $scope.expYears = [{ "key": "null", "value": "Exp. Year" }];
        var thisYear2 = Number(new Date().getFullYear());
        var yearRange2 = 11;
        for (var i2 = 1; i2 <= yearRange2; i2++) {
            // console.log('thisYear ' +  thisYear2);
            var element = {};
            element.key = thisYear2;
            element.value = thisYear2;
            $scope.expYears.push(element);
            thisYear2 = thisYear2 + 1;
        }
        $scope.year = 'null';
        $scope.syear = 'null';
        $scope.expMonths = [
            {
                "key": "null",
                "value": "Exp. Month"
            },
            {
                "key": 1,
                "value": "01 - Jan"
            },
            {
                "key": 2,
                "value": "02 - Feb"
            },
            {
                "key": 3,
                "value": "03 - Mar"
            },
            {
                "key": 4,
                "value": "04 - Apr"
            },
            {
                "key": 5,
                "value": "05 - May"
            },
            {
                "key": 6,
                "value": "06 - Jun"
            },
            {
                "key": 7,
                "value": "07 - Jul"
            },
            {
                "key": 8,
                "value": "08 - Aug"
            },
            {
                "key": 9,
                "value": "09 - Sep"
            },
            {
                "key": 10,
                "value": "10 - Oct"
            },
            {
                "key": 11,
                "value": "11 - Nov"
            },
            {
                "key": 12,
                "value": "12 - Dec"
            }
        ];
        $scope.month = 'null';
        $scope.smonth = 'null';
        function editPatient() {
            //PatientService.setPatient(patient);
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }
        function refreshEventsAndSurveys() {
            var l = Ladda.create(document.querySelector('.refresh-btn'));
            l.start();
            CaseDetailsFactory.LoadCaseEvents().then(function (data) {
                //console.log(data);
                $scope.events = data;
                loadCaseEvents();
                loadSurveyScores($scope.case.Id, $scope.surveys[0].Id);
                Ladda.stopAll();
            }, function (error) {
            });
            Ladda.stopAll();
        }
        ;
        //Implants
        function loadProducts() {
            CaseDetailsFactory.LoadProducts().then(function (data) {
                //console.log(data);
                $scope.products = data;
            }, function (error) {
            });
        }
        $scope.onSelectProduct = function ($item, $model, $label) {
            $scope.selectedProduct = $item;
        };
        function checkAndSaveImplantBarcode() {
            console.log($scope.implantEntryType);
            if ($scope.implantEntryType === 'barcode') {
                console.log($scope.firstbar);
                if ((angular.isDefined($scope.firstbar) && $scope.firstbar.indexOf('/') !== -1) || (angular.isDefined($scope.firstbar) && $scope.firstbar.length > 25)) {
                    $scope.secondbar = '';
                    $scope.month = '';
                    $scope.year = '';
                    $scope.addImplant();
                }
                else if (angular.isDefined($scope.firstbar) && $scope.firstbar.length > 0 &&
                    angular.isDefined($scope.secondbar) && $scope.secondbar.length > 0 &&
                    angular.isDefined($scope.month) && $scope.month != null &&
                    angular.isDefined($scope.year) && $scope.year != null) {
                    $scope.addImplant();
                }
                else {
                    Ladda.stopAll();
                }
            }
            else if ($scope.implantEntryType === 'manual') {
                addImplant();
            }
        }
        function addImplant() {
            console.log('called?');
            var l = Ladda.create(document.querySelector('.implant-btn'));
            l.start();
            if ($scope.implantEntryType === 'barcode') {
                CaseDetailsFactory.AddImplant($scope.firstbar, $scope.secondbar, $scope.month, $scope.year, $scope.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    $scope.firstbar = '';
                    $scope.secondbar = '';
                    $scope.month = 'null';
                    $scope.year = 'null';
                    angular.element(document.querySelector('#firstbar'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
            else if ($scope.implantEntryType === 'manual') {
                CaseDetailsFactory.AddImplantManually($scope.selectedProduct.Id, $scope.lotNumber, $scope.month, $scope.year, $scope.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    $scope.selectedProduct = '';
                    $scope.lotNumber = '';
                    $scope.month = 'null';
                    $scope.year = 'null';
                    angular.element(document.querySelector('#ProductNumber'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
        }
        $scope.isEnterKeyPressed = function (event) {
            if (event.keyCode === 13) {
                angular.element(document.querySelector('#secondbar'))[0].focus();
            }
        };
        $scope.deleteImplant = function (implant) {
            $confirm({ text: 'Are you sure you want to delete this implant (' + implant.Product_Catalog_Id__r.Name + ')?', title: 'Delete Implant' })
                .then(function () {
                CaseDetailsFactory.DeleteImplant(implant.Id).then(function (data) {
                    refreshEvents();
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        };
        $scope.isEnterKeyPressedSensor = function (event) {
            if (event.keyCode === 13) {
                $scope.addSensor();
            }
        };
        //Sensors
        function loadSensorRefs() {
            CaseDetailsFactory.loadSensorRefs().then(function (data) {
                //console.log(data);
                $scope.sensorRefs = data;
            }, function (error) {
            });
        }
        $scope.onSelectRef = function ($item, $model, $label) {
            $scope.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        };
        function addSensor() {
            if ($scope.sensorCode !== null) {
                var l = Ladda.create(document.querySelector('.sensor-btn'));
                console.log(l);
                l.start();
                if ($scope.sensorEntryType === 'barcode') {
                    SensorService.AddSensor($scope.sensorCode, $scope.procedure.Id)
                        .then(function (data) {
                        console.log('Data is retrieved');
                        console.log(data);
                        if (data === 'Successful') {
                            refreshEvents();
                            $scope.sensorRefs = sensorRefs;
                            $scope.sensorCode = '';
                            angular.element(document.querySelector('#sensorCode'))[0].focus();
                        }
                        else {
                            alert(data);
                        }
                        Ladda.stopAll();
                    }, function (error) {
                        Ladda.stopAll();
                    });
                }
                else {
                    if ($scope.sensorForm.$valid) {
                        SensorService.AddSensorManually($scope.selectedRef.Name, $scope.selectedRef.Manufacturer__c, $scope.serialNumber, $scope.slotNumber, $scope.smonth, $scope.syear, $scope.procedure.Id)
                            .then(function (data) {
                            console.log(data);
                            refreshEvents();
                            $scope.sensorRefs = sensorRefs;
                            $scope.sensorForm.submitted = false;
                            $scope.selectedRef = '';
                            $scope.serialNumber = '';
                            $scope.slotNumber = '';
                            $scope.smonth = 'null';
                            $scope.syear = 'null';
                            Ladda.stopAll();
                        }, function (error) {
                            Ladda.stopAll();
                        });
                    }
                    else {
                        Ladda.stopAll();
                        $scope.sensorForm.submitted = true;
                    }
                }
            }
        }
        function deleteSensor(device) {
            $confirm({ text: 'Are you sure you want to delete this sensor (' + device.OS_Device__r.Device_ID__c + ')?', title: 'Delete Sensor' })
                .then(function () {
                CaseDetailsFactory.DeleteSensor(device.OS_Device__r.Id, $scope.procedure.Id).then(function (data) {
                    refreshEvents();
                    //console.log(data);
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
        ;
        //case note methods        
        function addNewCaseNote(procedureId) {
            $uibModal.open({
                templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_caseAddNewNote',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory) {
                    $scope.procedureId = procedureId;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.addNote = function () {
                        CaseDetailsFactory.AddNote($scope.caseTitle, $scope.caseNote, $scope.procedureId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        ;
        function deleteNote(note) {
            $confirm({ text: 'Are you sure you want to delete this note?', title: 'Delete Note' })
                .then(function () {
                CaseDetailsFactory.DeleteNote(note.Id).then(function (data) {
                    //console.log(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
        ;
        function editNote(note) {
            $uibModal.open({
                templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_caseUpdateNote',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory) {
                    $scope.caseTitle = note.Title;
                    $scope.caseNote = note.Body;
                    $scope.noteId = note.Id;
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.updateNote = function () {
                        CaseDetailsFactory.UpdateNote($scope.caseTitle, $scope.caseNote, $scope.noteId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        ;
        function surveyChange(surveyName) {
            //canGraphSurvey = canGraph($scope.renderedSurvey);
            var survey = SurveyService.getSurveyByName($scope.renderedSurvey)
                .then(function (data) {
                survey = data;
                SurveyService.setSurvey(survey);
                //$scope.chartCategories = SurveyService.getChartCategories();
                console.log(survey);
                // $scope.surveyGraphable = canGraph(survey);
                if (survey.id !== undefined) {
                    loadSurveyScores($scope.case.Id, survey.Id);
                }
                else {
                    $scope.showGraph = false;
                }
            });
        }
        function canGraph(survey) {
            //console.log(survey.Can_Graph__c);
            var graphable = survey.Can_Graph__c;
            if (!graphable) {
                $scope.toggleValue = 'data';
            }
            return graphable;
        }
    }
})();
//# sourceMappingURL=caseDetail.controller.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var CaseDetailsService = (function () {
            function CaseDetailsService() {
            }
            return CaseDetailsService;
        }());
        CaseDetailsService.inject = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
        services.CaseDetailsService = CaseDetailsService;
        angular
            .module('orthosensor.dashboard')
            .service('CaseDetailsService', CaseDetailsService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseDetails.service.js.map
angular
    .module("app")
    .component("hospitalDetail", {
    template: "app/hospital/hospitalDetail.component.html",
    bindings: {},
    controllerAs: 'vm',
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=hospitalDetail.component.js.map
angular
    .module("app")
    .component("hospitalDetailHeader", {
    templateUrl: "app/components/hospital/hospitalDetailHeader.component.html",
    bindings: {},
    controllerAs: "vm",
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=hospitalDetailHeader.component.js.map
var app;
(function (app) {
    angular
        .module("app")
        .component("hospitalList", {
        template: "\n            <section>\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n            <div class=\"panel-title\">{{vm.title}}</div>\n        </div>\n\n        <div class=\"panel-body\">\n            <table class=\"table table-responsive table-striped\">\n                <thead>\n                    <tr>\n                        <th>Name</th>\n                        <th>ID</th>\n                        \n                    </tr>\n                    <tbody>\n                       <tr ng-repeat=\"hospital in vm.hospitals\" \n                            ui-sref=\"admin.hospitalDetail({hospital})\">\n                            <td>{{hospital.name}}</td>\n                            <td>{{hospital.id}}</td>\n                        \n                        </tr>\n                    </tbody>\n                </thead>\n            </table>\n        </div>\n</section>",
        bindings: {},
        controller: "HospitalsController",
        controllerAs: 'vm'
    });
})(app || (app = {}));
//# sourceMappingURL=hospitalList.component.js.map 
//# sourceMappingURL=hospitalList.component.js.map
angular
    .module("app")
    .component("practiceDetailModal", {
    template: "app/hospital/proacticeDetailModal.html",
    bindings: {},
    controllerAs: "vm",
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=practiceDetailModal.component.js.map
angular
    .module("app")
    .component("practiceSummary", {
    template: "app/components/hospital/practiceSummary.component.html",
    bindings: {},
    controller: "PracticeSummaryController",
    controllerAs: "vm",
});
//# sourceMappingURL=practiceSummary.component.js.map
var orthosensor;
(function (orthosensor) {
    var admin;
    (function (admin) {
        // import { Practice } from '../domains/practice';
        // import Modal = 
        var PracticeSummaryController = (function () {
            function PracticeSummaryController($uibModal) {
                this.$uibModal = $uibModal;
                this.title = 'Practices';
                this.practices = [
                    {
                        id: '989098',
                        name: 'Practice A',
                        parentId: '',
                        parentName: '',
                    },
                    {
                        id: '989097',
                        name: 'Practice B',
                        parentId: '',
                        parentName: '',
                    },
                ];
            }
            PracticeSummaryController.$inject = ["$uibModal"];
            PracticeSummaryController.prototype.open = function (practice) {
                var options = {
                    template: '<practice-detail-modal></practice-detail-modal>',
                };
                var modalInstance = this.$uibModal.open(options);
            };
            return PracticeSummaryController;
        }());
        PracticeSummaryController.$inject = ['$uibModal'];
        admin.PracticeSummaryController = PracticeSummaryController;
        angular
            .module('app')
            .controller('PracticeSummaryController', PracticeSummaryController);
    })(admin = orthosensor.admin || (orthosensor.admin = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=practiceSummary.controller.js.map
angular
    .module("orthosensor")
    .component("surgeonSummary", {
    templateUrl: "app/hospital/surgeonSummary.component.html",
    bindings: {},
});
//# sourceMappingURL=surgeonSummary.component.js.map
var orthosensor;
(function (orthosensor) {
    var ShellCtrl = (function () {
        function ShellCtrl($rootScope, UserService) {
            this.$rootScope = $rootScope;
            this.UserService = UserService;
            this.title = 'Orthosensor IQ Playground';
            this.name = $rootScope.currentUser;
            this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            this.sitePrefix = $rootScope.sitePrefix;
            // console.log($rootScope.sitePrefix);
            this.logoutCall = $rootScope.sitePrefix + '/secur/logout.jsp';
        }
        ShellCtrl.$inject = ["$rootScope", "UserService"];
        ShellCtrl.prototype.$onInit = function () {
            if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                this.systemMode = 'production';
            }
            else {
                this.systemMode = this.$rootScope.systemMode;
            }
            this.messages = 2;
            this.user = this.UserService.user;
            console.log(this.user);
        };
        return ShellCtrl;
    }());
    ShellCtrl.$inject = ['$rootScope', 'UserService'];
    orthosensor.ShellCtrl = ShellCtrl;
    angular
        .module('orthosensor')
        .controller('ShellCtrl', ShellCtrl);
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=shell.js.map
angular
    .module('app')
    .component('sidebar', {
    templateUrl: 'app/layout/sidebar.html'
});
//# sourceMappingURL=sidebar.component.js.map
(function () {
    'use strict';
    var controllerId = 'sidebar';
    function sidebar($route, config, routes) {
        var vm = this;
        var title;
        vm.isCurrent = isCurrent;
        activate();
        function activate() { getNavRoutes(); }
        function getNavRoutes() {
            vm.navRoutes = routes.filter(function (r) {
                return r.config.settings && r.config.settings.nav;
            }).sort(function (r1, r2) {
                return r1.config.settings.nav - r2.config.settings.nav;
            });
        }
        function isCurrent(route) {
            if (!route.config.title || !$route.current || !$route.current.title) {
                return '';
            }
            var menuName = route.config.title;
            return $route.current.title.substr(0, menuName.length) === menuName ? 'current' : '';
        }
        angular.module('app').controller(controllerId, ['$route', 'config', 'routes', sidebar]);
    }
    ;
})();
//# sourceMappingURL=sidebar.js.map
(function () {
    'use strict';
    // Usage: this displays hamburger style menu in the upper left header
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osTopMenu', {
        templateUrl: 'app/layout/topMenu.component.html',
        //templateUrl: 'templateUrl',
        controller: topMenuController,
        bindings: {
            title: '@'
        }
    });
    topMenuController.$inject = ['$rootScope'];
    function topMenuController($rootScope) {
        /*jshint validthis: true */
        var $ctrl = this;
        console.log($rootScope.systemMode);
        if ($rootScope.systemMode === undefined || $rootScope.systemMode === null) {
            $ctrl.systemMode = 'production';
        }
        else {
            $ctrl.systemMode = $rootScope.systemMode;
        }
        ////////////////
        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };
    }
})();
//# sourceMappingURL=topMenu.component.js.map
var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('topNav', {
        templateUrl: "app/layout/topnav.html",
        controller: function () {
            var ctrl = this;
            ctrl.title = 'Dashboard';
        },
        //controllerAs: 'vm',
        bindings: {
            title: '@'
        }
    });
})(app || (app = {}));
//# sourceMappingURL=topnav.component.js.map
//# sourceMappingURL=chartController.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardController = (function () {
            function DashboardController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, $state, $rootScope) {
                this.DashboardService = DashboardService;
                this.DashboardDataService = DashboardDataService;
                this.PatientsUnderBundleService = PatientsUnderBundleService;
                this.PatientService = PatientService;
                this.UserService = UserService;
                this.$state = $state;
                this.$rootScope = $rootScope;
                this.patientsUnderBundleTitle = 'Patients Under Bundle';
                // this.logo = $rootScope.globalStaticResourcePath + '/images/iq_logo.svg';
            }
            DashboardController.prototype.$onInit = function () {
                var _this = this;
                var practiceId = '0017A00000M803ZQAR';
                this.user = this.UserService.user;
                this.isAdmin = this.UserService.isAdmin;
                // console.log(this.isAdmin);
                this.standardComponentSize = 'col-sm-6 col-md-4 col-xs-12 dashboardComponentHeight';
                this.standardDashboardWidgetSize = 'col-sm-4 col-xs-12';
                if (!this.isAdmin) {
                    practiceId = this.UserService.user.accountId;
                    if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                        this.systemMode = 'production';
                    }
                    else {
                        this.systemMode = this.$rootScope.systemMode;
                    }
                    console.log(this.$rootScope);
                    this.DashboardDataService.getScheduledPatients(practiceId, 7)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsScheduled = data.length;
                        }
                        else {
                            _this.patientsScheduled = 0;
                        }
                        // console.log(this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                    this.DashboardDataService.getPatientsUnderBundle(practiceId)
                        .then(function (data) {
                        if (data !== null) {
                            _this.patientsUnderBundleCount = data.length;
                        }
                        else {
                            _this.patientsUnderBundleCount = 0;
                        }
                        // console.log(this.patientsUnderBundleCount);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            return DashboardController;
        }());
        DashboardController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', '$state', '$rootScope'];
        var Dashboard = (function () {
            function Dashboard() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = DashboardController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/dashboard.component.html';
            }
            return Dashboard;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osDashboard', new Dashboard());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.component.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var DashboardService = (function () {
            /* @ngInject */
            function DashboardService(PatientListFactory, SurveyService, $q, HospitalService, SFDataService) {
                this.SurveyService = SurveyService;
                this.$q = $q;
                this.HospitalService = HospitalService;
                this.SFDataService = SFDataService;
                this.PatientService = PatientListFactory;
                // this.hospitals = this.getHospitals();
                this.chartHeight = 280;
                this.surveys = new Array();
                // this.dashboardService = IQ_DashboardRepository;
            }
            DashboardService.$inject = ["PatientListFactory", "SurveyService", "$q", "HospitalService", "SFDataService"];
            DashboardService.$inject = ["PatientListFactory", "SurveyService", "$q", "HospitalService", "SFDataService"];
            Object.defineProperty(DashboardService.prototype, "practices", {
                // private dashboardService: any;
                get: function () {
                    return this._practices;
                },
                set: function (newPractices) {
                    this._practices = newPractices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surgeons", {
                get: function () {
                    return this._surgeons;
                },
                set: function (newSurgeons) {
                    this._surgeons = newSurgeons;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (newHospitals) {
                    this._hospitals = newHospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "hospital", {
                get: function () {
                    return this._hospital;
                },
                set: function (newHospital) {
                    this._hospital = newHospital;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DashboardService.prototype, "surveys", {
                get: function () {
                    return this._surveys;
                },
                set: function (newSurvey) {
                    this._surveys = newSurvey;
                },
                enumerable: true,
                configurable: true
            });
            DashboardService.prototype.getHospitals = function () {
                // console.log(this.hospitals);
                if (this.hospitals !== undefined && this.hospitals.length > 0) {
                    return this.hospitals;
                }
                else {
                    this.hospitals = this.HospitalService.hospitals;
                }
            };
            DashboardService.prototype.loadPractices = function () {
                var _this = this;
                this.SFDataService.loadPractices()
                    .then(function (data) {
                    // console.log(data);
                    _this._practices = _this.HospitalService.convertSFPracticeToObject(data);
                    // console.log(this.practices);
                }, function (error) {
                    console.log(error);
                });
            };
            DashboardService.prototype.loadHospitalPractices = function (hospitalId) {
                this.HospitalService.setPractices(hospitalId);
                this.practices = this.HospitalService.practices;
            };
            DashboardService.prototype.getSurgeons = function (hospitalId) {
                return this.surgeons;
            };
            DashboardService.prototype.setSurgeons = function (hospitalId) {
                var _this = this;
                var surgeonData;
                // console.log(hospitalId);
                if (hospitalId !== null) {
                    this.PatientService.loadSurgeons(hospitalId)
                        .then(function (data) {
                        var surg = data;
                        // console.log(surg);
                        for (var i = 0; i < surg.length; i++) {
                            var surgeonR = surg[i].Surgeon__r;
                            var surgeonD = void 0;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        _this.surgeons = surgeonData;
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            DashboardService.prototype.getCaseActivityByPractice = function (practiceId, startDate, endDate) {
                this.PatientService.getCaseActivityByPractice(practiceId, startDate, endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log(results);
                    return results;
                    // this.surgeons = surgeonData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            DashboardService.prototype.setSurveys = function () {
                var _this = this;
                this.surveys = [];
                this.SurveyService.loadSurveys()
                    .then(function (data) {
                    for (var i = 0; i <= data.length; i++) {
                        if (data[i] !== undefined) {
                            var survey = new orthosensor.domains.Survey();
                            survey.id = data[i].Id;
                            survey.name = data[i].Name__c;
                            // console.log(survey);
                            _this.surveys.push(survey);
                        }
                    }
                }, function (error) {
                    console.log(error);
                    // return error;
                });
            };
            DashboardService.prototype.formatDate = function (data) {
                // console.log(data);
                var dateLabel;
                dateLabel = moment(data).add(1, 'd').toDate();
                var month = moment(dateLabel).month() + 1;
                var smonth;
                if (month < 10) {
                    smonth = '0' + String(month);
                }
                else {
                    smonth = String(month);
                }
                var formattedDate = moment(dateLabel).year() + '-' + smonth + '-02';
                console.log(formattedDate);
                return formattedDate;
            };
            return DashboardService;
        }());
        DashboardService.inject = ['PatientListFactory', 'SurveyService', '$q', 'HospitalService', 'SFDataService'];
        services.DashboardService = DashboardService;
        // DashboardService.$inject = ['PatientListFactory', 'SurveyService', '$q'];
        angular
            .module('orthosensor.dashboard')
            .service('DashboardService', DashboardService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard.service.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardBaseController = (function () {
            function DashboardBaseController(UserService, HospitalService, $rootScope) {
                this.UserService = UserService;
                this.HospitalService = HospitalService;
                this.$rootScope = $rootScope;
                this.dateFormat = 'MM/DD/YYYY';
                this.isDashboard = false;
            }
            DashboardBaseController.prototype.$onInit = function () {
                this.user = this.UserService.user;
                console.log(this.user);
                this.isUserAdmin = this.UserService.isAdmin;
                // console.log(this.$rootScope);
                console.log(this.$rootScope.systemMode);
                if (this.$rootScope.systemMode === undefined || this.$rootScope.systemMode === null) {
                    this.systemMode = 'production';
                }
                else {
                    this.systemMode = this.$rootScope.systemMode;
                }
                console.log(this.systemMode);
                this.showPracticeList = this.isUserAdmin || this.user.userProfile === 1 /* HospitalAdmin */;
                this.showSurgeonList = this.isUserAdmin || this.user.userProfile === 2 /* Surgeon */ || this.user.userProfile === 3 /* PracticeAdmin */;
                console.log(this.showPracticeList);
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        console.log(this.HospitalService);
                        this.hospital = this.HospitalService.hospital;
                        this.practice = new orthosensor.domains.Practice;
                        this.practice.id = this.user.accountId;
                        this.practice.name = this.user.accountName;
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.hospital = this.HospitalService.hospital;
                        this.practices = this.HospitalService.practices;
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('hospital Account');
                        this.hospital = this.HospitalService.hospital;
                        this.practice = new orthosensor.domains.Practice;
                        this.practice.id = this.user.accountId;
                        this.practice.name = this.user.accountName;
                        break;
                    // case 'Customer Community User':
                    //     // console.log('hospital Account');
                    //     this.hospital = this.HospitalService.hospital;
                    //     this.practice = new orthosensor.domains.Practice;
                    //     this.practice.id = this.user.accountId;
                    //     this.practice.name = this.user.accountName;
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('hospital Account');
                    //     this.hospital = this.HospitalService.hospital;
                    //     this.practice = new orthosensor.domains.Practice;
                    //     this.practice.id = this.user.accountId;
                    //     this.practice.name = this.user.accountName;
                    //     break;
                    default:
                        // console.log('Admin Account');
                        if (this.isUserAdmin) {
                            this.hospitals = this.HospitalService.hospitals;
                            this.practices = this.HospitalService.practices;
                            console.log(this.practices);
                        }
                }
                this.surgeons = this.HospitalService.surgeons;
                // console.log(this.surgeons);
            };
            return DashboardBaseController;
        }());
        DashboardBaseController.$inject = ['UserService', 'HospitalService', '$rootScope'];
        dashboard.DashboardBaseController = DashboardBaseController;
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboardBase.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var DashboardChartBaseController = (function (_super) {
            __extends(DashboardChartBaseController, _super);
            function DashboardChartBaseController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.HospitalService = HospitalService;
                _this.$rootScope = $rootScope;
                _this.dateFormat = 'MM/DD/YYYY';
                _this.PatientService = PatientListFactory;
                return _this;
            }
            DashboardChartBaseController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.monthsToChart = '3';
                console.log(this.surgeons);
                if (this.surgeons && this.surgeons.length) {
                    //add logic to get surgeon that is logged in if its a surgeon account
                    this.surgeon = this.surgeons[0];
                }
                this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
                // console.log(this.surgeonSelectClass);
                this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
            };
            DashboardChartBaseController.prototype.setDateParams = function () {
                var today = moment();
                // console.log(today);
                var endDate = moment(today).add((Number(this.monthsToChart) + 1), 'months');
                //  console.log(endDate);
                var sEndDate = moment(endDate).format(this.dateFormat);
                // console.log(sEndDate);
                var startMonth = moment(today).subtract((Number(this.monthsToChart) - 1), 'months');
                var startDate = moment(startMonth).startOf('month');
                this.monthDiff = moment(endDate).diff(startDate, 'months');
                // console.log(this.monthDiff);
                this.startDate = moment(startDate).format(this.dateFormat);
                this.endDate = moment(endDate).format(this.dateFormat);
                // console.log(this.startDate);
            };
            DashboardChartBaseController.prototype.getSurgeonPartnerSurgeonSelectClass = function () {
                if (this.user.userProfile === 2 /* Surgeon */) {
                    return 'col-md-4 col-sm-6 col-xs-10';
                }
                else {
                    return 'col-md-2 col-sm-3 col-xs-6';
                }
            };
            DashboardChartBaseController.prototype.getHospitalPartnerPracticeSelectClass = function () {
                if (this.user.userProfile === 1 /* HospitalAdmin */) {
                    return 'col-md-4 col-sm-6 col-xs-10';
                }
                else {
                    return 'col-md-2 col-sm-3 col-xs-6';
                }
            };
            DashboardChartBaseController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            DashboardChartBaseController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticeData(); // for testing!!
                            }
                        }
                    }
                }
            };
            DashboardChartBaseController.prototype.formatDate = function (data) {
                // console.log(data);
                return this.DashboardService.formatDate(data);
            };
            DashboardChartBaseController.prototype.ascii = function (a) { return String.fromCharCode(a); };
            return DashboardChartBaseController;
        }(dashboard.DashboardBaseController));
        DashboardChartBaseController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        dashboard.DashboardChartBaseController = DashboardChartBaseController;
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboardChartBase.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        'use strict';
        var PromDeltaService = (function () {
            function PromDeltaService(DashboardService, DashboardDataService, UserService, moment) {
                this.DashboardService = DashboardService;
                this.DashboardDataService = DashboardDataService;
                this.isUserAdmin = UserService.isUserAdmin;
                // these values are being hard coded for KOOS and current event types
                this.eventTypes = ['Pre-Op', '4-8 Week Follow-up', '6 Month Follow-up', '12 Month Follow-up', '2 Year Follow-up'];
            }
            PromDeltaService.$inject = ["DashboardService", "DashboardDataService", "UserService", "moment"];
            PromDeltaService.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        _this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            //// stopped here - creating service to manage data collection
            PromDeltaService.prototype.getData = function () {
                //console.log(this.promDeltas);
                //this.data = this.chartData;
                return this.data;
            };
            PromDeltaService.prototype.convertDataToObjects = function (data) {
                var promDeltas = [];
                // console.log(data);
                for (var k = 0; k <= this.eventTypes.length; k++) {
                    for (var i = 0; i <= data.length; i++) {
                        // make sure a record exists!
                        if (data[i] !== undefined) {
                            if (data[i].Stage__c === this.eventTypes[k]) {
                                var delta = this.convertSFToObject(data[i]);
                                promDeltas.push(delta);
                            }
                        }
                    }
                }
                return promDeltas;
            };
            // stopped here 2/3/17 - need to add another method for aggregate objects and one for detail
            // convert to object - dates come in last day of the prior month
            PromDeltaService.prototype.convertSFToObject = function (data) {
                // console.log(data);
                var promDelta = new orthosensor.domains.PromDelta();
                promDelta.id = data.Id;
                promDelta.practiceId = data.PracticeID__c;
                promDelta.surgeonId = data.SurgeonID__c;
                promDelta.practiceName = data.PracticeName__c;
                promDelta.surgeonName = data.SurgeonName__c;
                promDelta.section = data.Section__c;
                promDelta.stage = data.Stage__c;
                promDelta.score = data.Score__c;
                promDelta.patientName = data.PatientName__c;
                promDelta.patientIde = data.PatientID__c;
                promDelta.procedureDate = moment(data.ProcedureDate__c);
                // console.log(promDelta);
                return promDelta;
            };
            return PromDeltaService;
        }());
        PromDeltaService.inject = ['DashboardService', 'DashboardDataService', 'UserService', 'moment'];
        dashboard.PromDeltaService = PromDeltaService;
        angular
            .module('orthosensor.dashboard')
            .service('PromDeltaService', PromDeltaService);
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=promDelta.service.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Account = (function () {
            function Account(id, name, recordTypeName, parentId) {
                this.id = id;
                this.name = name;
                this.recordTypeName = recordTypeName;
                this.parentId = parentId;
            }
            return Account;
        }());
        domains.Account = Account;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Account.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { ICaseType } from './caseType';
        // import { IPhysician } from './physician';
        // import { Hospital } from './hospital';
        var Case = (function () {
            function Case(id, caseType, laterality, physician, procedure_Date, hospital) {
                this.id = id;
                this.caseType = caseType;
                this.laterality = laterality;
                this.physician = physician;
                this.procedure_Date = procedure_Date;
                this.hospital = hospital;
            }
            return Case;
        }());
        domains.Case = Case;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Case.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var CaseType = (function () {
            function CaseType(id, description) {
                this.id = id;
                this.description = description;
            }
            return CaseType;
        }());
        domains.CaseType = CaseType;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=CaseType.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var ChartDataElement = (function () {
            function ChartDataElement() {
            }
            return ChartDataElement;
        }());
        domains.ChartDataElement = ChartDataElement;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=ChartDataElement.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var ChartType = (function () {
            function ChartType() {
            }
            return ChartType;
        }());
        domains.ChartType = ChartType;
        var ChartOptions = (function () {
            function ChartOptions() {
            }
            return ChartOptions;
        }());
        domains.ChartOptions = ChartOptions;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=ChartType.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Contact = (function () {
            function Contact(Id, Name, AccountId) {
                this.Id = Id;
                this.Name = Name;
                this.AccountId = AccountId;
            }
            return Contact;
        }());
        domains.Contact = Contact;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Contact.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Hospital = (function () {
            function Hospital(id, name, recordTypeId) {
                this.id = id;
                this.name = name;
                this.recordTypeId = recordTypeId;
            }
            return Hospital;
        }());
        domains.Hospital = Hospital;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Hospital.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var KneeBalanceAvg = (function () {
            function KneeBalanceAvg() {
            }
            return KneeBalanceAvg;
        }());
        domains.KneeBalanceAvg = KneeBalanceAvg;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=KneeBalanceAvg.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var KneeBalanceLoads = (function () {
            function KneeBalanceLoads() {
            }
            return KneeBalanceLoads;
        }());
        domains.KneeBalanceLoads = KneeBalanceLoads;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=KneeBalanceLoads.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Patient = (function () {
            function Patient() {
            }
            return Patient;
        }());
        domains.Patient = Patient;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Patient.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PhaseCount = (function () {
            function PhaseCount() {
            }
            return PhaseCount;
        }());
        domains.PhaseCount = PhaseCount;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=PhaseCount.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Physician = (function () {
            function Physician(id, name) {
                this.id = id;
                this.name = name;
            }
            return Physician;
        }());
        domains.Physician = Physician;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Physician.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PostOpPatientActivity = (function () {
            function PostOpPatientActivity(id, lastName, firstName, days, climbed, walked, last, rangeOfMotion, postOperativeStatus, ptEconomic) {
                this.id = id;
                this.lastName = lastName;
                this.firstName = firstName;
                this.days = days;
                this.climbed = climbed;
                this.walked = walked;
                this.last = last;
                this.rangeOfMotion = rangeOfMotion;
                this.postOperativeStatus = postOperativeStatus;
                this.ptEconomic = ptEconomic;
            }
            return PostOpPatientActivity;
        }());
        domains.PostOpPatientActivity = PostOpPatientActivity;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=PostOpPatientActivity.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Practice = (function () {
            function Practice() {
            }
            return Practice;
        }());
        domains.Practice = Practice;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Practice.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PromDelta = (function () {
            function PromDelta() {
            }
            return PromDelta;
        }());
        domains.PromDelta = PromDelta;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=PromDelta.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Surgeon = (function () {
            function Surgeon(id, name, parentId, parentName) {
                this.id = id;
                this.name = name;
                this.parentId = parentId;
                this.parentName = parentName;
            }
            return Surgeon;
        }());
        domains.Surgeon = Surgeon;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Surgeon.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var Survey = (function () {
            function Survey() {
            }
            return Survey;
        }());
        domains.Survey = Survey;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=Survey.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { IContact } from './contact';
        var UserProfile;
        (function (UserProfile) {
            UserProfile[UserProfile["SystemAdmin"] = 0] = "SystemAdmin";
            UserProfile[UserProfile["HospitalAdmin"] = 1] = "HospitalAdmin";
            UserProfile[UserProfile["Surgeon"] = 2] = "Surgeon";
            UserProfile[UserProfile["PracticeAdmin"] = 3] = "PracticeAdmin";
        })(UserProfile = domains.UserProfile || (domains.UserProfile = {}));
        var User = (function () {
            function User() {
            }
            return User;
        }());
        domains.User = User;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=User.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var UserType = (function () {
            function UserType() {
            }
            return UserType;
        }());
        domains.UserType = UserType;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=UserType.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PatientKoosScore = (function () {
            function PatientKoosScore() {
            }
            return PatientKoosScore;
        }());
        domains.PatientKoosScore = PatientKoosScore;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientKoosScores.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var PatientsOutOfCompliance = (function () {
            function PatientsOutOfCompliance() {
            }
            return PatientsOutOfCompliance;
        }());
        domains.PatientsOutOfCompliance = PatientsOutOfCompliance;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsOutOfCompliance.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        var sfPatient = (function () {
            function sfPatient() {
            }
            return sfPatient;
        }());
        domains.sfPatient = sfPatient;
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=sfPatient.js.map
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var NotificationListController = (function () {
            function NotificationListController(UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'Notifications';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            NotificationListController.prototype.$onInit = function () {
                this.notifications = [
                    {
                        patientName: 'Jane Wales', id: 29347, message: 'Upcoming Appointment, I have some questions and concerns about my upcoming Total Knee Arthroplasty. Would you please contact me at your earliest convenience.'
                    },
                    {
                        patientName: 'Robert Narnacle', id: 29532, message: 'Upcoming 4-8 Week Post-Op Follow-up, I wanted to follow up with you and see if you need me to complete any additional surveys or bring any additional documentation with me to my appointment. Also, as you requested I will send you photos of my wound to your review.'
                    },
                    {
                        patientName: 'Mildred Ginger', id: 21237, message: 'Upcoming Operation March 22, 2017, Dr. McCoy would you please resend the Pre-Operative Knee Injury and Osteoarthritis Outcome Score (KOOS) survey that you spoke about when we met previously as I have not received it yet.'
                    },
                ];
            };
            ;
            return NotificationListController;
        }());
        NotificationListController.$inject = ['UserService'];
        angular
            .module('orthosensor')
            .component('osNotifications', {
            controller: NotificationListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/notifications/notificationList.component.html',
            bindings: {}
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=notificationList.component.js.map
(function () {
    function PatientDetailsController($q, $rootScope, $state, $uibModal, PatientDetailsFactory, CaseDetailsFactory, LogFactory, SurveyService, PatientService, CaseService) {
        //vars
        var $ctrl = this;
        //$ctrl.patient = $rootScope.patient;
        $ctrl.patient = PatientService.getSFObject();
        $ctrl.surveyGraphable = true;
        $ctrl.toggleValue = 'chart';
        $ctrl.patientDetailsVisible = false;
        //methods
        $ctrl.surveyChange = surveyChange;
        $ctrl.canGraph = canGraph;
        $ctrl.addPatientCase = addPatientCase;
        //ctrl.editPatient = editPatient;
        $ctrl.completeEvent = completeEvent;
        $ctrl.caseDetails = caseDetails;
        $ctrl.caseCount = 0;
        $ctrl.showGraph = false;
        // $ctrl.togglePatientDetailsDrawer = togglePatientDetailsDrawer;
        //////////
        LogFactory.logReadsObject($ctrl.patient.Id, 'Patient__c');
        $q.all([
            PatientDetailsFactory.LoadCasesAndEvents()
                .then(function (data) {
                console.log(data);
                $ctrl.cases = data;
                $ctrl.caseCount = $ctrl.cases.length;
                console.log($ctrl.caseCount);
                for (var i = 0; i < $ctrl.cases.length; i++) {
                    LogFactory.logReadsObject($ctrl.cases[i].Id, 'Case__c');
                }
                if ($ctrl.cases.length > 0) {
                    SurveyService.setEventTypes($ctrl.cases[0].Events__r);
                }
            }, function (error) {
            })
        ])
            .then(function () {
            if ($ctrl.cases.length > 0) {
                SurveyService.loadSurveysFromCaseType($ctrl.cases[0].Id).then(function (data) {
                    $ctrl.surveys = data;
                    console.log(data);
                    if ($ctrl.surveys.length > 0) {
                        $ctrl.renderedSurvey = $ctrl.surveys[0].Name__c;
                        $ctrl.surveyGraphable = canGraph($ctrl.surveys[0]);
                        loadDataBySurvey($ctrl.surveys[0]);
                    }
                }, function (error) {
                });
            }
        });
        function loadSurveyScores(caseId, surveyId) {
            console.log('SurveyId: ' + surveyId);
            console.log('CaseId: ' + caseId);
            SurveyService.setCaseId(caseId);
            var survey = SurveyService.getSurveyById(surveyId)
                .then(function (data) {
                survey = data;
                console.log(data);
                SurveyService.setSurvey(survey);
                CaseDetailsFactory.loadSurveyScores(caseId, surveyId)
                    .then(function (scores) {
                    console.log(scores);
                    $ctrl.showGraph = (scores.length > 0);
                    if ($ctrl.showGraph) {
                        $ctrl.chartCategories = SurveyService.getChartCategories();
                        console.log($ctrl.chartCategories);
                        $ctrl.chartData = SurveyService.getChartData(scores);
                        console.log($ctrl.chartData);
                        //$ctrl.chartCategories = SurveyService.getEventTypes(surveyId);
                        $ctrl.chartGridLines = c3.generate(SurveyService.setGraphParameters('#' + caseId, $ctrl.chartData));
                    }
                }, function (error) {
                    //      });
                });
            });
        }
        /// this was not used - created to show completion dates.
        function completeEvent(event) {
            var found = false;
            var today = new Date();
            if ($ctrl.eventsToComplete) {
                if ($ctrl.eventsToComplete.length > 0) {
                    for (var i = 0; i < $ctrl.eventsToComplete.length; i++) {
                        var complete = $ctrl.eventsToComplete[i];
                        // console.log(complete);
                        if (event.Event_Type_Name__c === $ctrl.eventsToComplete[i].Event__r.Event_Type_Name__c) {
                            console.log('Events match: ' + event.Event_Type_Name__c);
                            console.log(today);
                            // if its before today
                            if (complete.End_Date__c <= today) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                }
            }
            return found;
        }
        function addPatientCase(patient) {
            $state.go('addPatientCase');
            // $uibModal.open({
            //     templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
            //     controller: 'AddPatientCase',
            //     controllerAs: '$ctrl',
            //     size: 'small'
            // });
        }
        function caseDetails(patientCase, event) {
            $rootScope.case = patientCase;
            $rootScope.event = event;
            $state.go('caseDetails');
        }
        $ctrl.toggleOutcomes = function (viewID) {
            if ($ctrl.toggleValue === 'data') {
                $ctrl.toggleValue = 'chart';
            }
            else {
                $ctrl.toggleValue = 'data';
            }
        };
        function surveyChange(surveyName) {
            //console.log(patientCase);
            console.log(surveyName);
            //console.log($ctrl.renderedSurvey);
            var survey = SurveyService.getSurveyByName(surveyName)
                .then(function (data) {
                survey = data;
                SurveyService.setSurvey(survey);
                $ctrl.chartCategories = SurveyService.getChartCategories();
                console.log(survey);
                $ctrl.surveyGraphable = canGraph(survey);
                //LoadSurveyScores(patientCase.Id, survey.Id);
                loadDataBySurvey(survey);
            });
        }
        //determine if survey can be graphed
        function canGraph(survey) {
            var graphable = survey.Can_Graph__c;
            //console.log(survey.Can_Graph__c)
            if (!graphable) {
                $ctrl.toggleValue = 'data';
            }
            return graphable;
        }
        function loadDataBySurvey(survey) {
            console.log('Loading Survey Data');
            if ($ctrl.cases.length > 0) {
                console.log($ctrl.cases.length);
                for (var i = 0; i < $ctrl.cases.length; i++) {
                    loadSurveyScores($ctrl.cases[i].Id, survey.Id);
                }
            }
        }
    }
    PatientDetailsController.$inject = ['$q', '$rootScope', '$state', '$uibModal', 'PatientDetailsFactory', 'CaseDetailsFactory', 'LogFactory', 'SurveyService', 'PatientService', 'CaseService'];
    angular
        .module("orthosensor")
        .controller('PatientDetailsController', PatientDetailsController);
})();
//# sourceMappingURL=patientDetail.controller.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osExistingPatients', {
        templateUrl: 'app/patientList/existingPatients.component.html',
        controller: ExistingPatientsController,
        bindings: {
            Binding: '=',
        },
    });
    ExistingPatientsController.inject = [];
    function ExistingPatientsController() {
        var ctrl = this;
        ctrl.addPatientCase = addPatientCase;
        ctrl.filteredPatients = [];
        ////////////////
        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
        function addPatientCase() {
        }
    }
})();
//# sourceMappingURL=existingPatients.component.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('PatientListController', PatientListController);
    // tslint:disable-next-line:max-line-length
    PatientListController.$inject = ['$rootScope', '$state', '$filter', '$uibModal', 'PatientListFactory', 'PatientService', 'CaseDetailsFactory', 'UserService', 'HospitalService', 'CaseService', 'moment'];
    function PatientListController(
        // $scope: ng.IScope,
        $rootScope, $state, $filter, $uibModal, PatientListFactory, PatientService, CaseDetailsFactory, UserService, HospitalService, CaseService, moment) {
        //variables
        var ctrl = this;
        ctrl.patientSearchValue = '';
        ctrl.title = 'Patients';
        ctrl.activeTab = 'existing-patients';
        ctrl.surgeonFilters = [];
        ctrl.surgeonFilter = {};
        ctrl.sourceFilter = 'Existing';
        ctrl.filterOpen = false;
        ctrl.filterDrillDown = false;
        ctrl.procedureDateFilter = false;
        ctrl.currentHospitalName = '';
        ctrl.currentPracticeName = '';
        ctrl.filterHospitalId = '';
        ctrl.hospitals = [];
        ctrl.practices = [];
        //methods
        ctrl.addNewPatient = addNewPatient;
        ctrl.editPatient = editPatient;
        ctrl.searchPatients = searchPatients;
        ctrl.openHospitalFilter = openHospitalFilter;
        ctrl.currentHospital = currentHospital;
        ctrl.procedureDateFilterValue = null;
        ctrl.canChangeHospitalPracticeView = false;
        ctrl.addPatientCase = addPatientCase;
        ctrl.patientDetails = patientDetails;
        ctrl.caseDetails = caseDetails;
        ctrl.filterClicked = filterClicked;
        ctrl.toggleFilter = toggleFilter;
        ctrl.closeFilter = closeFilter;
        ctrl.drillDownFilter = drillDownFilter;
        ctrl.drillUpFilter = drillUpFilter;
        ctrl.removeSurgeonFilter = removeSurgeonFilter;
        ctrl.removeDateFilter = removeDateFilter;
        ctrl.searchPatientSelected = searchPatientSelected;
        ctrl.resetSearch = resetSearch;
        ctrl.searchHL7PatientSelected = searchHL7PatientSelected;
        activate();
        //add more start up to this as we refactor        
        function activate() {
            console.log($rootScope);
            // console.log($rootScopeProvider);
            getUserRights();
            ctrl.hospitals = HospitalService.hospitals;
            ctrl.practices = HospitalService.practices;
            // getHospitalPracticeSettings();
            console.log('Current filter:' + HospitalService.currentFilterId);
            if (HospitalService.currentFilterId === '' || HospitalService.currentFilterId === null) {
                console.log('no default hospital');
                setUserDefaultHospital();
                // console.log(userDefaultHospital.Id);
                // console.log(UserService.user);
                // console.log('User default hospital: ' + UserService.user.accountId);
                // $rootScope.filterHospitalId = $rootScope.userDefaultHospital.Id;
                // HospitalService.currentFilterId = userDefaultHospital;
            }
            else {
                console.log('remembering last selected or default hospital');
                getHospitalPracticeSettings();
            }
            getHospitalPracticeSettings();
            consoleLogHospitalSettings();
            getPatients();
            getHL7Patients();
            loadSurgeons();
            if (currentHospital() !== 'All Hospitals') {
                getHospitalPractices();
            }
            // console.log('got current');
        }
        function consoleLogHospitalSettings() {
            // console.log(HospitalService.filterHospitalId);
            // console.log(HospitalService.filterPracticeId);
            // console.log(ctrl.currentHospitalName);
            // console.log(ctrl.currentPracticeName);
            // console.log(HospitalService.currentFilterId);
        }
        function getUserRights() {
            ctrl.isUserAdmin = UserService.isAdmin;
            // if user is not an admin, see if there are practices for their hospital
            if (!UserService.isAdmin) {
                ctrl.canChangeHospitalPracticeView = userHasPractices();
            }
            else {
                ctrl.canChangeHospitalPracticeView = true;
            }
        }
        function setUserDefaultHospital() {
            if (UserService.user.userProfile !== 0 /* SystemAdmin */) {
                HospitalService.filterHospitalId = UserService.user.hospitalId;
                HospitalService.filterHospitalName = UserService.user.hospitalName;
                HospitalService.filterPracticeId = UserService.user.accountId;
                HospitalService.filterPracticeName = UserService.user.accountName;
                HospitalService.currentFilterId = UserService.user.accountId;
            }
            else {
                //  no default for admin!!
                // return 'All Hospitals';
            }
        }
        function getPatients() {
            if (HospitalService.currentFilterId === undefined) {
                console.log('undefining');
                HospitalService.currentFilterId = null;
            }
            console.log('getting patients - filter: ' + HospitalService.currentFilterId);
            PatientListFactory.getPatients(HospitalService.currentFilterId)
                .then(function (data) {
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i]);
                        if (data[i].Date_Of_Birth__c !== undefined) {
                            var date = data[i].Date_Of_Birth__c;
                            // console.log(date);
                            var newDate = moment(new Date(date)).add(1, 'days');
                            console.log(newDate);
                            data[i].Date_Of_Birth__c = newDate;
                        }
                        if (data[i].Cases__r !== undefined) {
                            if (data[i].Cases__r.length > 0) {
                                var cases = data[i].Cases__r;
                                for (var j = 0; j < cases.length; j++) {
                                    // let procDate = cases[j].Procedure_Date__c;
                                    // console.log(procDate);
                                    // let newProcDate = new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds());
                                    // console.log(newProcDate);
                                    var newProcDate = PatientService.formatDate(cases[j].Procedure_Date__c);
                                    cases[j].Procedure_Date__c = newProcDate;
                                    console.log(newProcDate);
                                    data[i].Cases__r[j].Procedure_Date__c = newProcDate;
                                }
                            }
                        }
                    }
                }
                ctrl.patients = data;
                ctrl.filteredPatients = data;
                // console.log(ctrl.filteredPatients);
            }, function (error) {
            });
        }
        function getHL7Patients() {
            console.log(HospitalService.currentFilterId);
            if (HospitalService.currentFilterId) {
                PatientListFactory.getHL7Patients(HospitalService.currentFilterId)
                    .then(function (data) {
                    console.log(data);
                    ctrl.hl7patients = data;
                    PatientListFactory.hl7Patients = data;
                    console.log(PatientListFactory.hl7Patients);
                }, function (error) {
                });
            }
        }
        function loadSurgeons() {
            // get surgeons at start up        
            // console.log(HospitalService.currentFilterId);
            // if (PatientService.currentFilterId) {
            console.log(UserService.user);
            HospitalService.loadHospitalPracticeSurgeons(UserService.user.accountId)
                .then(function (data) {
                console.log(HospitalService.surgeons);
                ctrl.surgeons = HospitalService.surgeons;
            }, function (error) {
                console.log(error);
            });
            // }
        }
        function getHospitalPractices() {
            if (PatientService.currentFilterId) {
                PatientListFactory.getHospitalPractices(HospitalService.currentFilterId)
                    .then(function (data) {
                    // console.log(data);
                    ctrl.hospitalPractices = data;
                    // userHasPractices();
                    if (!ctrl.isUserAdmin) {
                        ctrl.canChangeHospitalPracticeView = userHasPractices();
                    }
                    else {
                        ctrl.canChangeHospitalPracticeView = true;
                    }
                }, function (error) {
                    console.log(error);
                });
            }
        }
        function userHasPractices() {
            if (ctrl.hospitalPractices) {
                // console.log('should be true if practices exist' 
                return (ctrl.hospitalPractices.length > 0);
            }
            else {
                return false;
            }
        }
        function openHospitalFilter() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            });
            modalInstance.result
                .then(function () {
                getHospitalPracticeSettings();
                getPatients();
                loadSurgeons();
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        }
        function getHospitalPracticeSettings() {
            ctrl.currentHospitalId = HospitalService.filterHospitalId;
            ctrl.currentPracticeId = HospitalService.filterPracticeId;
            ctrl.currentHospitalName = HospitalService.filterHospitalName;
            ctrl.currentPracticeName = HospitalService.filterPracticeName;
            console.log(HospitalService.currentFilterId);
        }
        function currentHospital() {
            var hospitalName = '';
            // console.log($rootScope.currentUser);
            // console.log(UserService.user);
            // console.log(UserService.user.userType);
            //console.log(
            if (UserService.user.userProfile !== 0 /* SystemAdmin */) {
                setHospitalNameForContact();
            }
            else {
                setHospitalNameForNonContact();
            }
            return ctrl.currentHospitalName;
        }
        function setHospitalNameForContact() {
            var hospitalName = '';
            var contact = $rootScope.currentUser.Contact;
            // console.log(contact);
            if ('ParentId' in contact.Account) {
                if (contact.Account.Parent !== null && contact.Account.Parent.Name !== null) {
                    ctrl.currentHospitalName = contact.Account.Parent.Name;
                    ctrl.currentPracticeName = contact.Account.Name;
                }
                else {
                    ctrl.currentHospitalName = contact.Account.Name;
                }
            }
            else {
                ctrl.currentHospitalName = $rootScope.currentUser.Contact.Account.Name;
            }
            // return ctrl.currentHospitalName;
        }
        function setHospitalNameForNonContact() {
            consoleLogHospitalSettings();
            var hospitalName = '';
            if (PatientService.currentFilterId == null) {
                ctrl.currentHospitalName = 'All Hospitals';
            }
            else {
                if (PatientService.currentFilterId !== '') {
                    ctrl.currentHospitalName = HospitalService.filterHospitalName;
                    ctrl.currentPracticeName = HospitalService.filterPracticeName;
                }
                else {
                    console.log('no filter set!!!');
                    //$rootScope.hospital.Id === getHospitalName($rootScope.filterHospitalId);
                }
            }
            // return hospitalName;
        }
        // ctrl.dateOptions = {
        //     formatYear: 'yy',
        //     startingDay: 1,
        //     showWeeks: false
        // };
        function filterClicked(filterType, filter, evt) {
            // let $element = $(evt.currentTarget);
            if (filterType === 'surgeon') {
                console.log(filter);
                // let index = ctrl.surgeonFilters.indexOf(filter);
                // console.log(index);
                // if (index > -1) {
                //     ctrl.surgeonFilters.splice(index, 1);
                // } else {
                ctrl.surgeonFilter = filter;
                //}
                filterPatients();
            }
            else if (filterType === 'source') {
                if (ctrl.sourceFilter !== filter) {
                    ctrl.sourceFilter = filter;
                    filterPatients();
                }
            }
            else if (filterType === 'date') {
                evt.preventDefault();
                evt.stopPropagation();
                ctrl.procedureDateFilter = true;
            }
        }
        ;
        // ctrl.$watch('procedureDateFilterValue', function (newValue, oldValue) {
        //     filterPatients();
        // });
        function toggleFilter() {
            if (ctrl.filterOpen) {
                ctrl.filterOpen = false;
                ctrl.filterDrillDown = false;
            }
            else {
                ctrl.filterOpen = true;
            }
        }
        ;
        function closeFilter() {
            ctrl.filterOpen = false;
            ctrl.filterDrillDown = false;
        }
        ;
        function drillDownFilter() {
            ctrl.filterDrillDown = true;
        }
        ;
        function drillUpFilter() {
            ctrl.filterDrillDown = false;
        }
        ;
        function removeSurgeonFilter(filter) {
            var index = ctrl.surgeonFilters.indexOf(filter);
            if (index > -1) {
                ctrl.surgeonFilters.splice(index, 1);
            }
            filterPatients();
        }
        ;
        function removeDateFilter() {
            ctrl.procedureDateFilterValue = undefined;
            ctrl.procedureDateFilter = false;
            filterPatients();
        }
        ;
        function filterPatients() {
            //console.log('Calling filter function');
            var source = [];
            if (ctrl.sourceFilter == 'Existing') {
                source = ctrl.patients;
            }
            else if (ctrl.sourceFilter == 'Scheduled') {
                source = ctrl.hl7patients;
            }
            console.log(ctrl.surgeonFilter);
            if (ctrl.surgeonFilter) {
                ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    // for (let i = 0; i < ctrl.surgeonFilters.length; i++) {
                    if (p.Cases__r) {
                        for (var c = 0; c < p.Cases__r.length; c++) {
                            if (p.Cases__r[c].Physician__c === ctrl.surgeonFilter.id) {
                                return true;
                            }
                        }
                    }
                    // }
                    return false;
                });
                source = ctrl.filteredPatients;
            }
            else {
                ctrl.filteredPatients = source;
            }
            if (ctrl.procedureDateFilterValue) {
                ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    if (p.Cases__r) {
                        var filterDateValue = moment(ctrl.procedureDateFilterValue);
                        var filterDate = new Date(filterDateValue);
                        filterDate = new Date(filterDate.getFullYear(), filterDate.getMonth(), filterDate.getDate());
                        var filterTime = filterDate.getTime();
                        for (var c = 0; c < p.Cases__r.length; c++) {
                            var procedureDateValue = p.Cases__r[c].Procedure_Date__c;
                            var procedureDate = new Date(procedureDateValue);
                            procedureDate = moment(new Date(procedureDate.getFullYear(), procedureDate.getMonth(), procedureDate.getDate())).add(1, 'days');
                            //procedureDate.setMinutes(procedureDate.getTimezoneOffset());
                            var procedureTime = procedureDate.getTime();
                            //console.log(procedureTime);
                            if (filterTime === procedureTime) {
                                return true;
                            }
                        }
                    }
                    return false;
                });
                source = ctrl.filteredPatients;
                console.log(source);
            }
            else {
                ctrl.filteredPatients = source;
            }
        }
        function addNewPatient() {
            PatientService.patient = null;
            $state.go('addPatient');
            // $uibModal.open({
            //     templateUrl: 'app/components/addPatient/addPatient.modal.html',
            //     controller: 'AddPatient',
            //     controllerAs: '$ctrl',
            //     size: 'large'
            // });
        }
        function addPatientCase(patient) {
            console.log(patient);
            PatientService.setPatientFromSFObject(patient);
            $state.go('addPatientCase');
            // $uibModal.open({
            //     //template: '<add-patient-case patient=patient></add-patient-case>',
            //     templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
            //     controller: 'AddPatientCase',
            //     controllerAs: '$ctrl',
            //     size: 'small'
            // });
        }
        function editPatient(patient) {
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }
        function patientDetails(patient) {
            console.log(patient);
            PatientService.setPatientFromSFObject(patient);
            if (patient.Cases__r) {
                var physician = { name: patient.Cases__r[0].Physician__r.Name };
                CaseService.setPhysician(physician);
            }
            $state.go('patientDetails');
        }
        function caseDetails(patient, patientCase) {
            PatientService.setPatientFromSFObject(patient);
            if (patient.Cases__r) {
                var physician = { name: patient.Cases__r[0].Physician__r.Name };
                CaseService.setPhysician(physician);
            }
            $rootScope.case = patientCase;
            CaseDetailsFactory.LoadCaseEvents()
                .then(function (data) {
                console.log(data);
                var events = data;
                for (var i = 0; i < events.length; i++) {
                    if (events[i].Event_Type_Name__c === 'Procedure') {
                        CaseDetailsFactory.setEvent(events[i]);
                    }
                }
                $state.go('caseDetails');
            });
        }
        // ctrl.patientSearchValue = '';
        // ctrl.activeTab = 'existing-patients';
        function searchPatients() {
            if (ctrl.patientSearchValue.length > 2) {
                var hospitalId = HospitalService.currentFilterId;
                console.log(hospitalId);
                PatientListFactory.SearchPatientList(hospitalId, ctrl.patientSearchValue).then(function (data) {
                    console.log(data);
                    ctrl.searchPatientResults = data;
                }, function (error) {
                });
                ctrl.searchHL7PatientResults = PatientListFactory.SearchHL7PatientList(hospitalId, ctrl.patientSearchValue);
                console.log(ctrl.searchHL7PatientResults);
                //ctrl.searchHL7PatientResults = data;
            }
        }
        function searchPatientSelected(result) {
            //$rootScope.patient = result;
            console.log(result);
            PatientService.setPatientFromSFObject(result);
            $state.go('patientDetails');
        }
        ;
        function resetSearch() {
            ctrl.patientSearchValue = '';
        }
        ;
        function searchHL7PatientSelected(result) {
            PatientService.patient = initPatientRecord(result);
            console.log(PatientService);
            $state.go('addPatient');
        }
        function initPatientRecord(result) {
            var patient = new orthosensor.domains.Patient();
            patient.hl7 = true;
            // $ctrl.patient.hospital = result.SFDC_Account__c; //For Internal Users only - read-only
            patient.practice = ''; //For Internal Users only - will not render
            //patient.hospital.id = result.SFDC_Account__c;
            patient.hospitalId = result.SFDC_Account__c ? result.SFDC_Account__c : null;
            patient.firstName = result.Patient_First_Name__c ? result.Patient_First_Name__c : null;
            patient.lastName = result.Patient_Last_Name__c ? result.Patient_Last_Name__c : null;
            patient.gender = result.Patient_Gender__c ? result.Patient_Gender__c : null;
            patient.dateOfBirthString = result.Patient_Date_Of_Birth__c ? result.Patient_Date_Of_Birth__c : null;
            var dob = new Date(result.Patient_Date_Of_Birth__c);
            console.log(dob);
            patient.dateOfBirth = dob;
            // patient.dateOfBirthString = new Date(moment(dob).year(), moment(dob).month(), moment(dob).date());
            patient.dateOfBirthString = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
            console.log(patient.dateOfBirthString);
            // $ctrl.patient.DateofBirthString = moment(result.Patient_Date_Of_Birth__c).format('MM/DD/YYYY');
            patient.race = angular.isDefined(result.Patient_Race__c) ? result.Patient_Race__c : '';
            patient.language = angular.isDefined(result.Patient_Language__c) ? result.Patient_Language__c : '';
            patient.patientNumber = '';
            patient.medicalRecordNumber = angular.isDefined(result.Medical_Record_Number__c) ? result.Medical_Record_Number__c : '';
            patient.accountNumber = angular.isDefined(result.Account_Number__c) ? result.Account_Number__c : '';
            patient.socialSecurityNumber = angular.isDefined(result.Patient_SSN__c) ? result.Patient_SSN__c : '';
            patient.email = '';
            return patient;
            // console.log('$ctrl.patient.hospital = ' + $ctrl.patient.hospital);
            // console.log('$ctrl.patient.HospitalId = ' + $ctrl.patient.HospitalId);
            // console.log('$ctrl.patient.FirstName = ' + $ctrl.patient.FirstName);
        }
    }
})();
//# sourceMappingURL=patientList.controller.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osPatientListFilter', {
        // template:'htmlTemplate',
        templateUrl: 'app/patientList/patientListFilter.component.html',
        controller: PatientListFilterController,
        bindings: {
            Binding: '=',
        },
    });
    PatientListFilterController.inject = ['$rootScope'];
    function PatientListFilterController($rootScope) {
        var ctrl = this;
        ctrl.currentHospital = 'Orthosensor Hospital';
        ctrl.logo = $rootScope.globalStaticResourcePath + '/images/OrthoLogiQLogo_darkblue.png';
        ////////////////
        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
    }
})();
//# sourceMappingURL=patientListFilter.component.js.map
var app;
(function (app) {
    angular
        .module('orthosensor')
        .component('patientListHeader', {
        template: "\n            <div class=\"dropdown\">\n                <button id=\"main-navigation\" \n                    class=\"btn btn-os navigation-action\" type=\"button\" data-toggle=\"dropdown\" \n                    aria-haspopup=\"true\" aria-expanded=\"false\">\n                    <i class=\"glyphicon glyphicon-menu-hamburger\"></i>\n                </button>\n                <ul class=\"dropdown-menu\" aria-labelledby=\"dLabel\">\n                    <li><a><i class=\"glyphicon glyphicon-list\"></i> Patients</a></li>\n                    <li><a href=\"#\" ui-sref=\"dashboard\">\n                    <i class=\"glyphicon glyphicon-stats\"></i> Dashboard</a>\n                    </li>\n                </ul>\n            </div>\n\n            <h1>Patients</h1>",
        controller: 'PatientListController'
    });
})(app || (app = {}));
//# sourceMappingURL=patientListHeader.component.js.map 
//# sourceMappingURL=patientListHeader.component.js.map
(function () {
    'use strict';
    function PatientSummaryController($ctrl, $rootScope, $state, $filter, $uibModal, PatientListFactory, PatientService, CaseDetailsFactory, UserService) {
        //variables
        $ctrl.patientSearchValue = '';
        $ctrl.title = 'Patients';
        $ctrl.activeTab = 'existing-patients';
        $ctrl.surgeonFilters = [];
        $ctrl.sourceFilter = 'Existing';
        $ctrl.filterOpen = false;
        $ctrl.filterDrillDown = false;
        $ctrl.procedureDateFilter = false;
        //methods
        $ctrl.addNewPatient = addNewPatient;
        $ctrl.editPatient = editPatient;
        $ctrl.searchPatients = searchPatients;
        $ctrl.openHospitalFilter = openHospitalFilter;
        $ctrl.currentHospital = currentHospital;
        $ctrl.procedureDateFilterValue = null;
        $ctrl.canChangeHospitalPracticeView = false;

        activate();

        //implementations
        if ($rootScope.filterHospitalId === undefined) {
            $rootScope.filterHospitalId = null;
        }

        //add more start up to this as we refactor        
        function activate() {
            console.log('initializing controller!');
            getUserRights();
            getUserDefaultHospital();
            if ($rootScope.userDefaultHospital) {
                console.log($rootScope.userDefaultHospital);
                $rootScope.filterHospitalId = $rootScope.userDefaultHospital.Id;
            }
            loadHospitals();
            loadPractices();
            loadSurgeons();

            getPatients();
            getHL7Patients();
            if (currentHospital() !== 'All Hospitals') {
                getHospitalPractices();
            }
            getHospitalPractices();

        }

        function getUserRights() {
            $ctrl.isUserAdmin = $rootScope.isUserAdmin;
            // if user is not an admin, see if there are practices for their hospital
            if (!$ctrl.isUserAdmin) {
                $ctrl.canChangeHospitalPracticeView = userHasPractices();

            } else {
                $ctrl.canChangeHospitalPracticeView = true;
            }
        }

        function getUserDefaultHospital() {
            if ($rootScope.currentUser.Contact != null) {
                $rootScope.userDefaultHospital = $rootScope.currentUser.Contact.Account;
                console.log($rootScope.userDefaultHospital);
                return $rootScope.userDefaultHospital;
            } else {
                //  no default for admin!!
            }
        }

        function getPatients() {
            console.log($rootScope.filterHospitalId);
            // if ($rootScope.filterHospitalId) {
            PatientListFactory.getPatients($rootScope.filterHospitalId)
                .then(function (data) {
                    $ctrl.patients = data;
                    $ctrl.filteredPatients = data;
                    console.log($ctrl.filteredPatients);
                }, function (error) {
                });
            // }
        }

        function getHL7Patients() {
            if ($rootScope.filterHospitalId) {
                PatientListFactory.getHL7Patients($rootScope.filterHospitalId).then(function (data) {
                    //console.log(data);
                    $ctrl.hl7patients = data;
                }, function (error) {
                });
            }
        }
        function loadSurgeons() {
            // get surgeons at start up        
            console.log($rootScope.filterHospitalId);
            if ($rootScope.filterHospitalId) {
                PatientListFactory.loadSurgeons($rootScope.filterHospitalId)
                    .then(function (data) {
                        //console.log(data);
                        $ctrl.surgeons = data;
                    }, function (error) {
                        console.log(error);
                    });
            }
        }

        function loadHospitals() {
            PatientListFactory.loadHospitals()
                .then(function (data) {
                    console.log(data);
                    $rootScope.hospitals = data;
                }, function (error) {
                    console.log(error);
                });
        }

        function loadPractices() {
            PatientListFactory.loadPractices()
                .then(function (data) {
                    console.log(data);
                    $rootScope.practices = data;
                }, function (error) {
                    console.log(error);
                });
        }

        function getHospitalPractices() {
            if ($rootScope.filterHospitalId) {
                PatientListFactory.getHospitalPractices($rootScope.filterHospitalId)
                    .then(function (data) {
                        console.log(data);
                        $rootScope.hospitalPractices = data;
                        // userHasPractices();
                        if (!$ctrl.isUserAdmin) {
                            $ctrl.canChangeHospitalPracticeView = userHasPractices();

                        } else {
                            $ctrl.canChangeHospitalPracticeView = true;
                        }
                    }, function (error) {
                        console.log(error);
                    });
            }
        }

        function userHasPractices() {
            if ($rootScope.hospitalPractices) {
                console.log('should be true if practices exist' + ($rootScope.hospitalPractices.length > 0));
                return ($rootScope.hospitalPractices.length > 0);
            } else {
                return false;
            }
        }

        function openHospitalFilter() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            });

            modalInstance.result
                .then(function () {
                    getPatients();
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        }

        function currentHospital() {
            var hospitalName = '';
            // console.log($rootScope.currentUser);
            if ($rootScope.currentUser.Contact != null) {
                hospitalName = getHospitalNameForContact();
            } else {
                hospitalName = getHospitalNameForNonContact();
            }
            return hospitalName;
        }

        function getHospitalNameForContact() {
            var hospitalName = '';
            var contact = $rootScope.currentUser.Contact;
            if ('ParentId' in contact.Account) {
                if (contact.Account.Parent !== null && contact.Account.Parent.Name !== null) {
                    hospitalName = contact.Account.Parent.Name + ' - ' + contact.Account.Name;
                } else {
                    hospitalName = contact.Account.Name;
                }
            } else {
                hospitalName = $rootScope.currentUser.Contact.Account.Name;
            }
            return hospitalName;
        }

        function getHospitalNameForNonContact() {
            var hospitalName = '';
            if ($rootScope.filterHospitalId == null) {
                hospitalName = 'All Hospitals';
            } else {
                if ($rootScope.filterPracticeId !== undefined) {
                    for (var i = 0; i < $rootScope.practices.length; i++) {
                        if ($rootScope.practices[i].Id === $rootScope.filterPracticeId) {
                            hospitalName = $rootScope.practices[i].Parent.Name + ' - ' + $rootScope.practices[i].Name;
                        }
                    }
                } else {
                    for (var i = 0; i < $rootScope.hospitals.length; i++) {
                        if ($rootScope.hospitals[i].Id === $rootScope.filterHospitalId) {
                            hospitalName = $rootScope.hospitals[i].Name;
                        }
                    }
                }
            }
            return hospitalName;
        }

        $ctrl.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };

        $ctrl.filterClicked = function (filterType, filter, evt) {
            var $element = $(evt.currentTarget);
            if (filterType === 'surgeon') {
                var index = $ctrl.surgeonFilters.indexOf(filter);
                if (index > -1) {
                    $ctrl.surgeonFilters.splice(index, 1);
                }
                else {
                    $ctrl.surgeonFilters.push(filter);
                }
                filterPatients();
            }
            else if (filterType === 'source') {
                if ($ctrl.sourceFilter !== filter) {
                    $ctrl.sourceFilter = filter;
                    filterPatients();
                }
            }
            else if (filterType === 'date') {
                evt.preventDefault();
                evt.stopPropagation();
                $ctrl.procedureDateFilter = true;
            }
        };

        $ctrl.$watch('procedureDateFilterValue', function (newValue, oldValue) {
            filterPatients();
        });

        $ctrl.toggleFilter = function () {
            if ($ctrl.filterOpen) {
                $ctrl.filterOpen = false;
                $ctrl.filterDrillDown = false;
            }
            else {
                $ctrl.filterOpen = true;
            }
        };
        $ctrl.closeFilter = function () {
            $ctrl.filterOpen = false;
            $ctrl.filterDrillDown = false;
        };
        $ctrl.drillDownFilter = function () {
            $ctrl.filterDrillDown = true;
        };
        $ctrl.drillUpFilter = function () {
            $ctrl.filterDrillDown = false;
        };
        $ctrl.removeSurgeonFilter = function (filter) {
            var index = $ctrl.surgeonFilters.indexOf(filter);
            if (index > -1) {
                $ctrl.surgeonFilters.splice(index, 1);
            }
            filterPatients();
        };
        $ctrl.removeDateFilter = function () {
            $ctrl.procedureDateFilterValue = undefined;
            $ctrl.procedureDateFilter = false;
            filterPatients();
        };
        function filterPatients() {
            //console.log('Calling filter function');
            var source = [];
            if ($ctrl.sourceFilter === 'Existing') {
                source = $ctrl.patients;
            }
            else if ($ctrl.sourceFilter === 'Scheduled') {
                source = $ctrl.hl7patients;
            }
            if ($ctrl.surgeonFilters.length > 0) {
                $ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    for (var i = 0; i < $ctrl.surgeonFilters.length; i++) {
                        if (p.Cases__r) {
                            for (var c = 0; c < p.Cases__r.length; c++) {
                                if (p.Cases__r[c].Physician__c === $ctrl.surgeonFilters[i].Surgeon__r.AccountId) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                });
                source = $ctrl.filteredPatients;
            }
            else {
                $ctrl.filteredPatients = source;
            }
            if ($ctrl.procedureDateFilterValue) {
                $ctrl.filteredPatients = $filter('filter')(source, function (p) {
                    if (p.Cases__r) {
                        var filterDateValue = moment($ctrl.procedureDateFilterValue);
                        var filterDate = new Date(filterDateValue);
                        filterDate = new Date(filterDate.getFullYear(), filterDate.getMonth(), filterDate.getDate());
                        var filterTime = filterDate.getTime();
                        for (var c = 0; c < p.Cases__r.length; c++) {
                            var procedureDateVale = p.Cases__r[c].Procedure_Date__c;
                            var procedureDate = new Date(procedureDateVale);
                            procedureDate = new Date(procedureDate.getFullYear(), procedureDate.getMonth(), procedureDate.getDate());
                            //procedureDate.setMinutes(procedureDate.getTimezoneOffset());
                            var procedureTime = procedureDate.getTime();
                            //console.log(procedureTime);
                            if (filterTime === procedureTime) {
                                return true;
                            }
                        }
                    }
                    return false;
                });
                source = $ctrl.filteredPatients;
                console.log(source);
            }
            else {
                $ctrl.filteredPatients = source;
            }
        }
        function addNewPatient() {
            $uibModal.open({
                templateUrl: 'app/components/addPatient/addPatient.modal.html',
                controller: 'AddPatient',
                size: 'large'
            });
        }
        $ctrl.addPatientCase = function (patient) {
            PatientService.setPatient(patient);
            console.log(patient);
            $uibModal.open({
                //template: '<add-patient-case patient=patient></add-patient-case>',
                templateUrl: 'app/components/addPatientCase/addPatientCase.modal.html',
                controller: 'AddPatientCase',
                size: 'small'
            });
        };
        function editPatient(patient) {
            $uibModal.open({
                templateUrl: 'app/components/editPatient/editPatient.component.html',
                controller: 'editPatientController',
                size: 'large'
            });
        }
        $ctrl.patientDetails = function (patient) {
            PatientService.setPatient(patient);
            $state.go('patientDetails');
        };
        $ctrl.caseDetails = function (patient, patientCase) {
            PatientService.setPatient(patient);
            console.log(patient);
            //$rootScope.patient = patient;
            $rootScope.case = patientCase;
            //$rootScope.event = null;
            CaseDetailsFactory.LoadCaseEvents()
                .then(function (data) {
                    console.log(data);
                    var events = data;
                    for (var i = 0; i < events.length; i++) {
                        if (events[i].Event_Type_Name__c === 'Procedure') {
                            CaseDetailsFactory.setEvent(events[i]);
                        }
                    }
                    $state.go('caseDetails');
                });
        };
        // $ctrl.patientSearchValue = '';
        // $ctrl.activeTab = 'existing-patients';
        function searchPatients() {
            if ($ctrl.patientSearchValue.length > 2) {
                var hospitalId = $rootScope.filterHospitalId;
                PatientListFactory.SearchPatientList(hospitalId, $ctrl.patientSearchValue).then(function (data) {
                    //console.log(data);
                    $ctrl.searchPatientResults = data;
                }, function (error) {
                });
                PatientListFactory.SearchHL7PatientList(hospitalId, $ctrl.patientSearchValue).then(function (data) {
                    //console.log(data);
                    $ctrl.searchHL7PatientResults = data;
                }, function (error) {
                });
            }
        }
        $ctrl.searchPatientSelected = function (result) {
            //$rootScope.patient = result;
            PatientService.setPatient(result);
            $state.go('patientDetails');
        };
        $ctrl.resetSearch = function () {
            $ctrl.patientSearchValue = '';
        };
        $ctrl.searchHL7PatientSelected = function (result) {
            $uibModal.open({
                templateUrl: 'app/addPatient/addPatient.modal.html',
                controller: function ($ctrl, $rootScope, $state, $uibModalInstance, PatientListFactory) {
                    $ctrl.open = { birthDate: false };
                    $ctrl.open = function ($event, whichDate) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $ctrl.open[whichDate] = true;
                    };
                    $ctrl.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 1,
                        showWeeks: false
                    };
                    $ctrl.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $ctrl.patient = {};
                    $ctrl.patient.hl7 = true;
                    $ctrl.patient.hospital = result.SFDC_Account__c; //For Internal Users only - read-only
                    $ctrl.patient.practice = ''; //For Internal Users only - will not render
                    $ctrl.patient.HospitalId = result.SFDC_Account__c;
                    $ctrl.patient.FirstName = result.Patient_First_Name__c;
                    $ctrl.patient.LastName = result.Patient_Last_Name__c;
                    $ctrl.patient.Gender = result.Patient_Gender__c;
                    //$ctrl.patient.DateofBirthString = result.Patient_Date_Of_Birth__c;
                    $ctrl.patient.dateOfBirthString = moment.utc(result.Patient_Date_Of_Birth__c).format('MM/DD/YYYY');
                    $ctrl.patient.Race = angular.isDefined(result.Patient_Race__c) ? result.Patient_Race__c : '';
                    $ctrl.patient.Language = angular.isDefined(result.Patient_Language__c) ? result.Patient_Language__c : '';
                    $ctrl.patient.PatientNumber = '';
                    $ctrl.patient.MedicalRecordNumber = angular.isDefined(result.Medical_Record_Number__c) ? result.Medical_Record_Number__c : '';
                    $ctrl.patient.AccountNumber = angular.isDefined(result.Account_Number__c) ? result.Account_Number__c : '';
                    $ctrl.patient.SocialSecurityNumber = angular.isDefined(result.Patient_SSN__c) ? result.Patient_SSN__c : '';
                    console.log('$ctrl.patient.hospital = ' + $ctrl.patient.hospital);
                    console.log('$ctrl.patient.HospitalId = ' + $ctrl.patient.HospitalId);
                    console.log('$ctrl.patient.FirstName = ' + $ctrl.patient.FirstName);
                    $ctrl.createNewPatient = function () {
                        if ($ctrl.pageForm.$valid) {
                            PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                                //console.log(data);               
                                $uibModalInstance.dismiss();
                                PatientService.setPatient(data);
                                //$rootScope.patient = data;
                                $state.go('patientDetails');
                            }, function (error) {
                                console.log(error);
                            });
                        }
                        else {
                            Ladda.stopAll();
                            $ctrl.pageForm.submitted = true;
                        }
                    };
                    $ctrl.savePatient = function () {
                        if ($ctrl.pageForm.$valid) {
                            PatientListFactory.checkDuplicate($ctrl.patient).then(function (data) {
                                //console.log(data); 
                                if (data != null) {
                                    $ctrl.matchedPatient = data;
                                    Ladda.stopAll();
                                }
                                else {
                                    PatientListFactory.createPatient($ctrl.patient).then(function (data) {
                                        //console.log(data);               
                                        $uibModalInstance.dismiss();
                                        PatientService.setPatient(data);
                                        //$rootScope.patient = data;
                                        $state.go('patientDetails');
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }
                            }, function (error) {
                                console.log(error);
                            });
                        }
                        else {
                            Ladda.stopAll();
                            $ctrl.pageForm.submitted = true;
                        }
                    };
                    $ctrl.createNewCase = function (matchdPatient) {
                        $uibModalInstance.dismiss();
                        $rootScope.addPatientCase(matchdPatient);
                    };
                },
                size: 'large'
            });
        };
    }

    PatientSummaryController.$inject = ['$ctrl', '$rootScope', '$state', '$filter', '$uibModal', 'PatientListFactory', 'PatientService', 'CaseDetailsFactory', 'UserService'];

    angular
        .module('orthosensor')
        .controller('PatientSummaryController', PatientSummaryController);

})();

var orthosensor;
(function (orthosensor) {
    var reports;
    (function (reports) {
        var ReportsSidebarController = (function () {
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function ReportsSidebarController(config, UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'PROM score deltas';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            ReportsSidebarController.$inject = ["config", "UserService"];
            ReportsSidebarController.prototype.$onInit = function () {
            };
            ;
            return ReportsSidebarController;
        }());
        ReportsSidebarController.$inject = ['config', 'UserService'];
        angular
            .module('orthosensor')
            .component('osReportsSidebar', {
            controller: ReportsSidebarController,
            controllerAs: '$ctrl',
            templateUrl: 'app/reports/reports-sidebar.component.html',
            bindings: {}
        });
    })(reports = orthosensor.reports || (orthosensor.reports = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=reports-sidebar.component.js.map
var orthosensor;
(function (orthosensor) {
    var reports;
    (function (reports) {
        var ReportsController = (function () {
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function ReportsController(config, UserService) {
                // this.PatientService = PatientListFactory;
                this.title = 'PROM score deltas';
                this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                this.$onInit();
            }
            ReportsController.$inject = ["config", "UserService"];
            ReportsController.prototype.$onInit = function () {
            };
            ;
            return ReportsController;
        }());
        ReportsController.$inject = ['config', 'UserService'];
        angular
            .module('orthosensor')
            .component('osReports', {
            controller: ReportsController,
            controllerAs: '$ctrl',
            templateUrl: 'app/reports/reports.component.html',
            bindings: {}
        });
    })(reports = orthosensor.reports || (orthosensor.reports = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=reports.component.js.map
var app;
(function (app) {
    angular
        .module("app")
        .component("userList", {
        bindings: {},
        templateUrl: "app/user/userList.html"
    });
})(app || (app = {}));
//# sourceMappingURL=user.component.js.map 
//# sourceMappingURL=user.component.js.map
angular
    .module("orthosensor")
    .component("userDetail", {
    bindings: {
        "user": "<",
    },
    controller: "UserDetailCtrl",
    templateUrl: "/app/user/userDetail.html",
});
//# sourceMappingURL=userDetail.component.js.map
var orthosensor;
(function (orthosensor) {
    var admin;
    (function (admin) {
        // import { UserService } from '../services/user.service';
        // import { IUser } from '../domains/user';
        var UserDetailCtrl = (function () {
            function UserDetailCtrl(userService) {
                this.userService = userService;
                this.title = 'User List';
                var ctrl = this;
                // let newUser = new orthosensor.domain.User("1002", "Johnson", "102938",
                //     new orthosensor.domain.Contact("8298", "Contact Name", "90912"));
                // let newUser2 = new orthosensor.domain.User("1003", "Honest Joe", "103238",
                //     new orthosensor.domain.Contact("8292", "Contact Name2", "903222"));
                // this.users = [newUser];
                // this.users.push(newUser2);
            }
            UserDetailCtrl.$inject = ["userService"];
            return UserDetailCtrl;
        }());
        admin.UserDetailCtrl = UserDetailCtrl;
        angular.module("app")
            .controller("UserDetailCtrl", UserDetailCtrl);
    })(admin = orthosensor.admin || (orthosensor.admin = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=userDetail.controller.js.map
angular
    .module('orthosensor')
    .component("userList", {
    bindings: {},
    controller: "UserListCtrl",
    controllerAs: "vm",
    templateUrl: "/app/user/userList.html",
});
//# sourceMappingURL=userList.component.js.map
var orthosensor;
(function (orthosensor) {
    var domains;
    (function (domains) {
        // import { IUser } from '../domains/user';
        var UserListCtrl = (function () {
            function UserListCtrl() {
                // let newUser = new orthosensor.domain.User("1002", "Johnson", "102938",
                //     new orthosensor.domain.Contact("8298", "Contact Name", "90912"));
                // let newUser2 = new orthosensor.domain.User("1003", "Honest Joe", "103238",
                //     new orthosensor.domain.Contact("8292", "Contact Name2", "903222"));
                this.title = "UserList";
                this.people = [
                    {
                        "id": '9087',
                        "lastName": "Schwartz",
                        "firstName": "Moses",
                    },
                    {
                        "id": '9088',
                        "lastName": "Mertz",
                        "firstName": "Fred",
                    },
                    {
                        "id": '9089',
                        "lastName": "Smith",
                        "firstName": "Missy",
                    },
                ];
                // this.users = [newUser];
                // this.users.push(newUser2);
            }
            UserListCtrl.prototype.editRecord = function (user) {
                //populate the detail
                //console.log(user.l.lastName);
            };
            return UserListCtrl;
        }());
        domains.UserListCtrl = UserListCtrl;
        angular
            .module("app")
            .controller("UserListCtrl", UserListCtrl);
    })(domains = orthosensor.domains || (orthosensor.domains = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=userList.controller.js.map
angular
    .module("app")
    .component("users", {
    bindings: {},
    templateUrl: "app/user/users.html",
});
//# sourceMappingURL=users.component.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        // intended for calls to salesforce data - need to migrate some 
        // other 
        var SFDataService = (function () {
            function SFDataService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
            }
            SFDataService.$inject = ["$q", "$rootScope", "$http"];
            SFDataService.prototype.loadHospitals = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitals(function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(() => {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadPractices = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadPractices(function (result, event) {
                    // console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadHospitalPractices = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                    console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadHospitalPracticeSurgeons = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                    console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.loadAllSurgeons = function () {
                var deferred = this.$q.defer();
                // if (hospitalId !== null) {
                IQ_PatientActions.LoadAllSurgeons(function (result, event) {
                    // console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SFDataService.prototype.getHospitalByPractice = function (practiceId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                IQ_PatientActions.LoadHospitalByPractice(practiceId, function (result, event) {
                    console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            return SFDataService;
        }());
        SFDataService.inject = ['$q', '$rootScope', '$http'];
        services.SFDataService = SFDataService;
        angular
            .module('orthosensor.services')
            .service('SFDataService', SFDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=SFDataService.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var CaseService = (function () {
            function CaseService($q, $rootScope) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this._currentCase = {};
                this.caseEvent = {};
                this.physician = {};
            }
            CaseService.$inject = ["$q", "$rootScope"];
            Object.defineProperty(CaseService.prototype, "currentProcedure", {
                get: function () {
                    return this._currentProcedure;
                },
                set: function (_currentProcedure) {
                    this._currentProcedure = _currentProcedure;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CaseService.prototype, "currentCase", {
                get: function () {
                    return this._currentCase;
                },
                set: function (_currentCase) {
                    this._currentCase = _currentCase;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CaseService.prototype, "clinicalData", {
                get: function () {
                    return this._clinicalData;
                },
                set: function (_clinicalData) {
                    this._clinicalData = _clinicalData;
                },
                enumerable: true,
                configurable: true
            });
            CaseService.prototype.getCurrentCaseEvent = function () {
                return this.caseEvent;
            };
            CaseService.prototype.setCurrentCaseEvent = function (event) {
                this.caseEvent = event;
            };
            CaseService.prototype.getCurrentCase = function () {
                return this.currentCase;
            };
            CaseService.prototype.setCurrentCase = function (_currentCase) {
                this._currentCase = _currentCase;
            };
            CaseService.prototype.getProcedure = function () {
                return this.currentProcedure;
            };
            CaseService.prototype.setProcedure = function (_procedure) {
                this.currentProcedure = _procedure;
                this.clinicalData = _procedure.Clinical_Data__r[0];
                console.log(this._clinicalData);
            };
            CaseService.prototype.getSurveyCompletionDates = function (caseId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_CaseActions.GetSurveyCompletionDates(caseId, function (result, event) {
                    //console.log(result);
                    // $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            CaseService.prototype.getPhysician = function () {
                return this.physician;
            };
            CaseService.prototype.setPhysician = function (_physician) {
                this.physician = _physician;
            };
            return CaseService;
        }());
        CaseService.inject = ['$q', '$rootScope'];
        services.CaseService = CaseService;
        angular
            .module('orthosensor.services')
            .service('CaseService', CaseService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=case.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('CaseDetailsFactory', CaseDetailsFactory);
    CaseDetailsFactory.$inject = ['$q', '$rootScope', '$http'];
    function CaseDetailsFactory($q, $rootScope, $http) {
        var event = {};
        var service = {
            LoadCaseEvents: LoadCaseEvents,
            LoadSurveys: LoadSurveys,
            loadSurveyScores: loadSurveyScores,
            CompleteEvent: CompleteEvent,
            UpdateProcedure: UpdateProcedure,
            AddSensor: AddSensor,
            DeleteSensor: DeleteSensor,
            LoadProducts: LoadProducts,
            loadSensorRefs: loadSensorRefs,
            AddImplantManually: AddImplantManually,
            AddImplant: AddImplant,
            DeleteImplant: DeleteImplant,
            AddNote: AddNote,
            UpdateNote: UpdateNote,
            DeleteNote: DeleteNote,
            DeleteSensorLine: DeleteSensorLine,
            LoadLinkStationsIDs: LoadLinkStationsIDs,
            CheckSoftwareVersion: CheckSoftwareVersion,
            GetImageList: GetImageList,
            SaveImageToRecord: SaveImageToRecord,
            MatchSensorDataAndAttachment: MatchSensorDataAndAttachment,
            loadSensorDataLines: loadSensorDataLines,
            getEvent: getEvent,
            setEvent: setEvent
        };
        return service;
        ////////////////
        function LoadCaseEvents() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadCaseEvents($rootScope.case.Id, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadSurveys() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSurveys(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSurveyScores(caseId, surveyId) {
            var deferred = $q.defer();
            //var questions = ['KOOS Symptoms', 'KOOS Pain', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS QOL'];
            var questions = 'Symptoms, Pain, ADL, Sport/Rec, QOL';
            if (!surveyId) {
                surveyId = 'a6M7A0000000AFeUAM';
            }
            IQ_PatientActions.LoadSurveyScores(caseId, surveyId, function (result, event) {
                //console.log('Case ID: ' + caseId);
                //console.log('Survey ID: ' + surveyId);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function CompleteEvent(eventId) {
            var deferred = $q.defer();
            IQ_PatientActions.CompleteEvent(eventId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function UpdateProcedure(procedureData) {
            var deferred = $q.defer();
            var procedureDateString = moment.utc(procedureData.procedureDateString).format('MM/DD/YYYY');
            var patientConsentObtained = procedureData.patientConsentObtained;
            if (procedureData.patientConsentObtained === 1) {
                patientConsentObtained = true;
            }
            IQ_PatientActions.UpdateProcedure(procedureData.Id, procedureData.caseId, procedureData.physician, procedureData.laterality, procedureDateString, patientConsentObtained, procedureData.anesthesiaType, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddSensor(barcode, procedureId) {
            var deferred = $q.defer();
            IQ_CaseActions.AddSensor(barcode, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteSensor(deviceId, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteSensor(deviceId, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadProducts() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadProducts(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSensorRefs() {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSensorRefs(function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddImplantManually(productId, lotNumber, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddImplantManually(productId, lotNumber, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddImplant(firstbarcode, secondbarcode, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddImplant(firstbarcode, secondbarcode, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteImplant(implantId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteImplant(implantId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddNote(title, text, procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.AddNote(title, text, procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function UpdateNote(title, text, noteId) {
            var deferred = $q.defer();
            IQ_PatientActions.UpdateNote(title, text, noteId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteNote(noteId) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteNote(noteId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function DeleteSensorLine(line, procedureID) {
            var deferred = $q.defer();
            IQ_PatientActions.DeleteSensorLine(line.SD_Type_MedialLoadID, line.SD_Type_LateralLoadID, line.SD_Type_RotationID, line.SD_Type_ActualFlexID, line.SD_Type_APID, line.SD_Type_HKAID, line.SD_Type_TibiaRotID, line.SD_Type_TibiaTiltID, procedureID, function (result, event) {
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadLinkStationsIDs(hospitalId) {
            ////
            var deferred = $q.defer();
            IQ_PatientActions.LoadLinkStationsIDs(hospitalId, function (result, event) {
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function CheckSoftwareVersion(procedureId, linkStationId, laterality) {
            var deferred = $q.defer();
            IQ_PatientActions.CheckSoftwareVersion(procedureId, linkStationId, laterality, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function GetImageList(procedureId, osDeviceId, linkStationId) {
            var deferred = $q.defer();
            IQ_PatientActions.GetImageList(procedureId, osDeviceId, linkStationId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function SaveImageToRecord(recordId, imageURL) {
            var deferred = $q.defer();
            IQ_PatientActions.SaveImageToRecord(recordId, imageURL, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function MatchSensorDataAndAttachment(recordId) {
            var deferred = $q.defer();
            IQ_PatientActions.MatchSensorDataAndAttachment(recordId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function loadSensorDataLines(procedureId) {
            var deferred = $q.defer();
            IQ_PatientActions.LoadSensorDataLines(procedureId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function getEvent() {
            return event;
        }
        function setEvent(_event) {
            event = _event;
            //console.log(event);
        }
        // factory.LoadSurveysFromCaseType = function (caseId) {
        //     var deferred = $q.defer();
        //     IQ_SurveyRepository.LoadSurveysFromCaseType(
        //         caseId,
        //         function (result, event) {
        //             //console.log(result);
        //             $rootScope.$apply(function () {
        //                 if (event.status) {
        //                     deferred.resolve(result);
        //                 } else {
        //                     $rootScope.handleSessionTimeout(event);
        //                     deferred.reject(event);
        //                 }
        //             })
        //         }, { buffer: false, escape: true, timeout: 120000 });
        //     return deferred.promise;
        // }
    }
})();
//# sourceMappingURL=caseDetails.service.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var ChartService = (function () {
            function ChartService() {
                this.chart = new orthosensor.domains.ChartType();
                this.chartTitle = 'Test';
                //this.chart.options = this.getColumnChartOptions();
                this.chart.type = 'ColumnChart';
                this.chartHeight = 240;
            }
            ChartService.prototype.getChart = function () {
                return this.chart;
            };
            ChartService.prototype.getMultiBarChartOptions = function () {
                return {
                    chart: {
                        type: 'multiBarChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showValues: true,
                        valueFormat: function (d) {
                            return d3.format(',.2f')(d);
                        },
                        showLegend: true,
                        showControls: false,
                        reduceXTicks: false,
                        rotateLabels: 0,
                        transitionDuration: 400,
                        stacked: false,
                        tooltips: true,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            ticks: d3.time.months,
                            tickFormat: function (d) {
                                // console.log(d);
                                var format = d3.time.format('%b-%y');
                                // console.log(format);
                                var dateConvert = format(new Date(d));
                                // console.log(dateConvert);
                                return dateConvert;
                            },
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -20,
                            tickFormat: (d3.format(',.1f')),
                            margin: { top: 10, left: 20 },
                            showMaxMin: false,
                        },
                        showXAxis: true,
                        showYAxis: true,
                        legend: {
                            margin: {
                                top: 5,
                                right: 0,
                                bottom: 20,
                                left: 0
                            },
                            radioButtonMode: true,
                            rightAlign: true,
                        },
                    },
                    title: {
                        enable: false,
                        text: this.chartTitle,
                    },
                };
            };
            ChartService.prototype.getMultiBarHorizontalChartOptions = function () {
                return {
                    chart: {
                        type: 'multiBarHorizontalChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showValues: true,
                        valueFormat: function (d) {
                            return d3.format(',.2f')(d);
                        },
                        showLegend: false,
                        showControls: false,
                        reduceXTicks: false,
                        rotateLabels: 0,
                        transitionDuration: 400,
                        stacked: false,
                        tooltips: true,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            ticks: d3.time.months,
                            tickFormat: function (d) {
                                // console.log(d);
                                var format = d3.time.format("%b-%y");
                                // console.log(new Date(d));
                                var dateConvert = format(new Date(d));
                                // console.log(dateConvert);
                                return dateConvert;
                            },
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -20,
                            tickFormat: (d3.format(',.1f')),
                            margin: { top: 10, left: 20 },
                            showMaxMin: false,
                        },
                        showXAxis: true,
                        showYAxis: true,
                        legend: {
                            margin: {
                                top: 5,
                                right: 0,
                                bottom: 20,
                                left: 0
                            },
                            radioButtonMode: true,
                            rightAlign: true,
                        },
                    },
                    title: {
                        enable: false,
                        text: this.chartTitle,
                    },
                };
            };
            ChartService.prototype.getLineChartOptions = function () {
                var events = ["Pre-Op", "4 - 8 week", "6 month", "1 year", "2 year"];
                return {
                    chart: {
                        type: 'lineChart',
                        height: this.chartHeight,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 40,
                            left: 55
                        },
                        useInteractiveGuideline: false,
                        interactive: false,
                        xAxis: {
                            axisLabel: this.xAxisLabel,
                            tickFormat: function (d) {
                                return events[d];
                            }
                        },
                        yAxis: {
                            axisLabel: this.yAxisLabel,
                            axisLabelDistance: -10
                        },
                        yTickFormat: function (d) {
                            return d3.format('.02f')(d);
                        },
                    },
                    title: {
                        enable: false,
                        text: 'Title for Line Chart'
                    },
                    subtitle: {
                        enable: false,
                        text: '',
                        css: {
                            'text-align': 'center',
                            'margin': '10px 13px 0px 7px'
                        }
                    },
                    caption: {
                        enable: false,
                        css: {
                            'text-align': 'justify',
                            'margin': '10px 13px 0px 7px'
                        }
                    },
                    styles: {
                        css: {
                            'background-color': 'White'
                        }
                    }
                };
            };
            ChartService.prototype.getColumnChartOptions = function () {
                // var chart = {};
                var options;
                options = new orthosensor.domains.ChartOptions();
                options.title = 'Tewt';
                options.legend = 'none';
                options.displayExactValues = true;
                options.fill = 20;
                options.animation = {
                    'duration': 1000,
                    'easing': 'out',
                };
                options.tooltip = { 'isHtml': true };
                options.is3D = true;
                options.chartArea = { 'left': 180, 'top': 20, 'bottom': 40, 'height': '100%' };
                options.hAxis = { title: 'Patients', 'gridlines': { 'count': 6 } };
                console.log(options);
                return options;
            };
            ChartService.prototype.getChartOptions = function () {
                return {
                    'title': this.chartTitle,
                    'legend': 'none',
                    'displayExactValues': true,
                    'fill': 20,
                    'animation': {
                        'duration': 1000,
                        'easing': 'out',
                    },
                    'tooltip': { 'isHtml': true },
                    'is3D': true,
                    'chartArea': { 'left': 180, 'top': 20, 'bottom': 40, 'height': '100%' },
                    'hAxis': { 'title': 'Patients', 'gridlines': { 'count': 6 } },
                };
            };
            return ChartService;
        }());
        services.ChartService = ChartService;
        // ChartService.$inject = [];
        angular
            .module('orthosensor.services')
            .service('ChartService', ChartService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=chart.service.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var DashboardDataService = (function () {
            function DashboardDataService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
            }
            DashboardDataService.$inject = ["$q", "$rootScope", "$http"];
            DashboardDataService.prototype.getAggregatePromDeltasBySurgeon = function (surgeonId, startMonth, startYear, endMonth, endYear) {
                console.log(surgeonId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetAggregatePromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getAggregatePromDeltasByPractice = function (practiceId, startMonth, startYear, endMonth, endYear) {
                console.log(practiceId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetAggregatePromDeltasByPractice(practiceId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPromDeltasBySurgeon = function (surgeonId, startMonth, startYear, endMonth, endYear) {
                console.log(surgeonId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPromDeltasBySurgeon(surgeonId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPromDeltasByPractice = function (practiceId, startMonth, startYear, endMonth, endYear) {
                console.log(practiceId, startMonth, startYear, endMonth, endYear);
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPromDeltasByPractice(practiceId, 1, 2016, 12, 2017, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.GetPracticeSensorData = function (practiceId, startMonth, startYear, endMonth, endYear) {
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPracticeSensorData(practiceId, startMonth, startYear, endMonth, endYear, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            DashboardDataService.prototype.getPatientsUnderBundle = function (practiceId) {
                var _this = this;
                console.log(practiceId);
                var deferred = this.$q.defer();
                if (practiceId !== null) {
                    IQ_DashboardRepository.GetPatientsUnderBundle(practiceId, function (result, event) {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            DashboardDataService.prototype.getScheduledPatients = function (practiceId, daysInFuture) {
                var _this = this;
                console.log(practiceId);
                var deferred = this.$q.defer();
                if (practiceId !== null) {
                    IQ_DashboardRepository.GetScheduledPatients(practiceId, daysInFuture, function (result, event) {
                        console.log(result);
                        // this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            return DashboardDataService;
        }());
        DashboardDataService.inject = ['$q', '$rootScope', '$http'];
        services.DashboardDataService = DashboardDataService;
        angular
            .module('orthosensor.services')
            .service('DashboardDataService', DashboardDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=dashboard-data.service.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var HospitalService = (function () {
            function HospitalService($q, $rootScope, $http, SFDataService) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
                this.SFDataService = SFDataService;
            }
            HospitalService.$inject = ["$q", "$rootScope", "$http", "SFDataService"];
            Object.defineProperty(HospitalService.prototype, "hospitals", {
                get: function () {
                    return this._hospitals;
                },
                set: function (hospitals) {
                    this._hospitals = hospitals;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "hospital", {
                get: function () {
                    return this._hospital;
                },
                set: function (_hospital) {
                    this._hospital = _hospital;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "practices", {
                get: function () {
                    return this._practices;
                },
                set: function (practices) {
                    this._practices = practices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalId", {
                get: function () {
                    return this._filterHospitalId;
                },
                set: function (filterHospitalId) {
                    this._filterHospitalId = filterHospitalId;
                    // this._filterHospitalName = this.getHospitalName(filterHospitalId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeId", {
                get: function () {
                    return this._filterPracticeId;
                },
                set: function (filterPracticeId) {
                    this._filterPracticeId = filterPracticeId;
                    // this._filterPracticeName = this.getPracticeName(filterPracticeId);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "currentFilterId", {
                get: function () {
                    return this._currentFilterId;
                },
                set: function (currentFilterId) {
                    this._currentFilterId = currentFilterId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterHospitalName", {
                get: function () {
                    return this._filterHospitalName;
                },
                set: function (name) {
                    this._filterHospitalName = name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "filterPracticeName", {
                get: function () {
                    return this._filterPracticeName;
                },
                set: function (name) {
                    this._filterPracticeName = name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HospitalService.prototype, "surgeons", {
                get: function () {
                    console.log(this._surgeons);
                    return this._surgeons;
                },
                set: function (_surgeons) {
                    console.log(this._surgeons);
                    this._surgeons = _surgeons;
                    console.log(this._surgeons);
                },
                enumerable: true,
                configurable: true
            });
            HospitalService.prototype.getHospitals = function () {
                var _this = this;
                return this.SFDataService.loadHospitals()
                    .then(function (data) {
                    _this._hospitals = _this.convertSFHospitalToObject(data);
                    console.log(_this._hospitals);
                    return _this._hospitals;
                }, function (error) {
                });
            };
            HospitalService.prototype.setHospitals = function (data) {
                this._hospitals = this.convertSFHospitalToObject(data);
                console.log(this._hospitals);
            };
            HospitalService.prototype.setPractices = function (hospitalId) {
                var _this = this;
                return this.SFDataService.loadHospitalPractices(hospitalId)
                    .then(function (data) {
                    _this._practices = _this.convertSFPracticeToObject(data);
                    console.log(_this._practices);
                    //return this._practices;
                }, function (error) {
                });
                // return [];
            };
            HospitalService.prototype.getPracticeName = function (id) {
                console.log('Searching for practice: ' + id);
                if (id !== undefined) {
                    for (var i = 0; i < this._practices.length; i++) {
                        // console.log(this._practices[i].Id);
                        if (this._practices[i].Id === id) {
                            console.log(this._practices[i].Id + ', ' + id + ' found!');
                            return this._practices[i].Name;
                        }
                    }
                }
                return '';
            };
            HospitalService.prototype.getHospitalName = function (id) {
                console.log('Searching for hospital: ' + id);
                if (id !== undefined) {
                    for (var i = 0; i < this._hospitals.length; i++) {
                        if (this._hospitals[i].Id === id) {
                            console.log(this._hospitals[i].Id + ', ' + id + ' found!');
                            return this._hospitals[i].Name;
                        }
                    }
                }
                return '';
            };
            HospitalService.prototype.loadHospitalPracticeSurgeons = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                // console.log(hospitalPracticeId);
                // if (hospitalPracticeId !== null) {
                return this.SFDataService.loadHospitalPracticeSurgeons(hospitalPracticeId)
                    .then(function (data) {
                    _this._surgeons = _this.convertSFSurgeonToObject(data);
                    console.log(_this._surgeons);
                }, function (error) {
                    console.log(error);
                });
                // return surgeonData;
                // }
            };
            ;
            HospitalService.prototype.loadHospitalByPractice = function (hospitalPracticeId) {
                var _this = this;
                // let surgeonData: orthosensor.domains.Surgeon[];
                console.log(hospitalPracticeId);
                if (hospitalPracticeId !== null) {
                    return this.SFDataService.getHospitalByPractice(hospitalPracticeId)
                        .then(function (data) {
                        console.log(data);
                        _this._hospital = new orthosensor.domains.Hospital(data.ParentId, data.Parent.Name, 'HospitalAccount');
                        console.log(_this._hospital);
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            HospitalService.prototype.loadAllSurgeons = function () {
                var _this = this;
                return this.SFDataService.loadAllSurgeons()
                    .then(function (data) {
                    // console.log(data);
                    _this.surgeons = _this.convertSFSurgeonToObject(data);
                }, function (error) {
                    console.log(error);
                });
                // return surgeonData;
            };
            HospitalService.prototype.loadSurgeons = function (hospitalId) {
                var _this = this;
                var surgeonData;
                // console.log(hospitalId);
                if (hospitalId !== null) {
                    this.PatientService.loadSurgeons(hospitalId)
                        .then(function (data) {
                        var surg = data;
                        // console.log(surg);
                        for (var i = 0; i < surg.length; i++) {
                            var surgeonR = surg[i].Surgeon__r;
                            var surgeonD = void 0;
                            // surgeonD = surgeonModule.Surgeon;
                            // console.log(surgeonR);
                            // surgeonD.id = surg[i].Surgeon__r.Id;
                            // surgeonD.name = surgeonR.Name;
                            // surgeonData.push(surgeonD);
                        }
                        _this.surgeons = surgeonData;
                    }, function (error) {
                        console.log(error);
                    });
                    // return surgeonData;
                }
            };
            HospitalService.prototype.loadHospitalPractices = function (hospitalId) {
                this.setPractices(hospitalId);
            };
            HospitalService.prototype.convertSFHospitalToObject = function (data) {
                var hospitals = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var hospital = {
                        id: data[i].Id,
                        name: data[i].Name,
                    };
                    // console.log(practice);
                    hospitals.push(hospital);
                }
                // console.log(hospitals);
                return hospitals;
            };
            HospitalService.prototype.convertSFPracticeToObject = function (data) {
                var practices;
                practices = [];
                // console.log(data.length);
                for (var i = 0; i < data.length; i++) {
                    var practice = {
                        id: data[i].Id,
                        name: data[i].Name,
                        parentId: data[i].ParentId,
                        parentName: '' //data[i].Parent.Name
                    };
                    // console.log(practice);
                    practices.push(practice);
                }
                // console.log(practices);
                return practices;
            };
            HospitalService.prototype.convertSFSurgeonToObject = function (data) {
                console.log(data);
                var surgeons;
                surgeons = [];
                for (var i = 0; i < data.length; i++) {
                    var surgeon = {
                        id: data[i].Surgeon__r.AccountId,
                        name: data[i].Surgeon__r.Name,
                        parentId: data[i].Hospital__c,
                        parentName: '' //data[i].Parent.Name //do we need this?
                    };
                    surgeons.push(surgeon);
                }
                console.log(surgeons);
                return surgeons;
            };
            return HospitalService;
        }());
        HospitalService.inject = ['$q', '$rootScope', '$http', 'SFDataService'];
        services.HospitalService = HospitalService;
        angular
            .module('orthosensor.services')
            .service('HospitalService', HospitalService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=hospital.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('InitFactory', InitFactory);
    InitFactory.$inject = ['$q', '$rootScope', '$http'];
    function InitFactory($q, $rootScope, $http) {
        var service = {
            getCurrentUser: getCurrentUser,
            isUserAdmin: isUserAdmin
        };
        return service;
        ////////////////
        function getCurrentUser() {
            var deferred = $q.defer();
            IQ_PatientActions.GetCurrentUser(function (result, event) {
                //console.log(result);
                //$rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function isUserAdmin() {
            var deferred = $q.defer();
            IQ_PatientActions.IsUserAdmin(function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=init.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('LogFactory', LogFactory);
    LogFactory.$inject = ['$q', '$rootScope', '$http'];
    function LogFactory($q, $rootScope, $http) {
        var service = {
            logReadsObject: logReadsObject
        };
        return service;
        ////////////////
        function logReadsObject(objectId, objectType) {
            var deferred = $q.defer();
            IQ_PatientActions.logReadsObject(objectId, objectType, function (result, event) {
                //console.log(result);
                //No need to confirm
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();

var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientDataService = (function () {
            function PatientDataService($rootScope, $q, moment) {
                this.$rootScope = $rootScope;
                this.$q = $q;
                this.moment = moment;
            }
            PatientDataService.$inject = ["$rootScope", "$q", "moment"];
            PatientDataService.prototype.checkDuplicate = function (patient) {
                var _this = this;
                var deferred = this.$q.defer();
                var accountId = null;
                if (patient.anonymous) {
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    var dateOfBirthString = this.moment.utc(patient.dateOfBirthString).format('MM/DD/YYYY');
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    /// TO DO: add email to Apex and call
                    IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateOfBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.createPatient = function (patient) {
                var _this = this;
                var deferred = this.$q.defer();
                var dateOfBirthString = this.moment.utc(patient.dateOfBirthString).format('MM/DD/YYYY');
                var accountId = null;
                if (patient.hospitalId != null) {
                    accountId = patient.hospitalId;
                }
                else if (patient.practice !== undefined && patient.practice.length > 0) {
                    accountId = patient.practice;
                }
                else if (patient.hospital.length > 0) {
                    accountId = patient.hospital;
                }
                if (patient.anonymous) {
                    // TO DO: add email 
                    IQ_PatientActions.CreateNewAnonymousPatient(patient.hospitalId, patient.label, patient.medicalRecordNumber, patient.birthYear, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    // console.log(patient.hospitalId+  ', ' + patient.lastName+  ', ' + patient.firstName+  ', ' + patient.medicalRecordNumber+  ', ' + dateOfBirthString+  ', ' + patient.gender+  ', ' + patient.socialSecurityNumber+  ', ' + patient.patientNumber+  ', ' + patient.accountNumber+  ', ' + patient.race+  ', ' + patient.language+  ', ' + patient.email);
                    IQ_PatientActions.CreateNewPatient(patient.hospitalId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateOfBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        _this.$rootScope.$apply(function () {
                            if (event.status) {
                                deferred.resolve(result);
                            }
                            else {
                                _this.$rootScope.handleSessionTimeout(event);
                                deferred.reject(event);
                            }
                        });
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.updatePatient = function (patient) {
                var _this = this;
                var deferred = this.$q.defer();
                var dateOfBirthString = this.moment.utc(patient.dateOfBirthString).format('MM/DD/YYYY');
                if (patient.anonymous) {
                    IQ_PatientActions.UpdateAnonymousPatient(patient.Id, patient.Label, patient.BirthYear, patient.MedicalRecordNumber, patient.Gender, patient.SocialSecurityNumber, patient.PatientNumber, patient.AccountNumber, patient.Race, patient.Language, function (result, event) {
                        //console.log(result);
                        _this.$rootScope.$apply(function () {
                            if (event.status) {
                                deferred.resolve(result);
                            }
                            else {
                                _this.$rootScope.handleSessionTimeout(event);
                                deferred.reject(event);
                            }
                        });
                    }, { buffer: false, escape: true, timeout: 120000 });
                }
                else {
                    // var dateOfBirthString = moment.utc(patient.dateOfBirthString).format('MM/DD/YYYY');
                    // console.log(patient.id + ', ' + patient.lastName + ',' + patient.firstName + ', ' + patient.medicalRecordNumber + ', ' + dateOfBirthString + ', ' + patient.gender + ', ' + patient.socialSecurityNumber + ', ' + patient.patientNumber + ', ' + patient.accountNumber + ', ' + patient.race + ', ' + patient.language + ', ' + patient.email);
                    IQ_PatientActions.UpdatePatient(patient.id, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateOfBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, patient.email, function (result, event) {
                        //console.log(result);
                        // $rootScope.$apply(() => {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                    // }, { buffer: false, escape: true, timeout: 120000 });
                }
                return deferred.promise;
            };
            PatientDataService.prototype.getPatient = function (patientId) {
                var deferred = this.$q.defer();
                // console.log(patientId);
                IQ_PatientActions.GetPatient(patientId, function (result, event) {
                    //console.log(result);
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientDataService.prototype.checkAnonymous = function (account) {
                var deferred = this.$q.defer();
                IQ_PatientActions.checkAnonymous(account, function (result, event) {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientDataService.prototype.getLanguages = function () {
                var languages;
                languages = [
                    'English', 'Spanish', 'French', 'Chinese', 'Creole', 'Unknown', 'Russian', 'Portuguese'
                ];
                return languages;
            };
            PatientDataService.prototype.getRaces = function () {
                var races;
                races = [
                    'White',
                    'Black or African American',
                    'American Indian',
                    'Asian',
                    'Native Hawaiian',
                    'Hispanic',
                    'Other'
                ];
                return races;
            };
            return PatientDataService;
        }());
        PatientDataService.inject = ['$rootScope', '$q'];
        services.PatientDataService = PatientDataService;
        angular
            .module('orthosensor.services')
            .service('PatientDataService', PatientDataService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patient.data.service.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientService = (function () {
            function PatientService($rootScope, $q, PatientDataService, moment) {
                this.$rootScope = $rootScope;
                this.$q = $q;
                this.PatientDataService = PatientDataService;
                this.moment = moment;
                // this._patient = new orthosensor.domains.Patient();
                this._currentFilterId = '';
            }
            PatientService.$inject = ["$rootScope", "$q", "PatientDataService", "moment"];
            Object.defineProperty(PatientService.prototype, "patient", {
                get: function () {
                    return this._patient;
                },
                set: function (newPatient) {
                    this._patient = newPatient;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PatientService.prototype, "currentFilterId", {
                get: function () {
                    return this._currentFilterId;
                },
                //public $q: any;
                set: function (id) {
                    this._currentFilterId = id;
                },
                enumerable: true,
                configurable: true
            });
            // this should be deprecated and use direct call using object initializer
            PatientService.prototype.getPatient = function () {
                // console.log(this._patient);
                return this._patient;
            };
            PatientService.prototype.setPatient = function (_patient) {
                // console.log(_patient);
                this._patient = new orthosensor.domains.Patient();
                this._patient = _patient;
            };
            // this is to handle when no object has been created.
            PatientService.prototype.setPatientId = function (patientId) {
                // this.patient = new orthosensor.domains.Patient();
                var _this = this;
                // this.patient.id = patientId;
                this.PatientDataService.getPatient(patientId)
                    .then(function (data) {
                    // console.log(data);
                    var sfPatient = data;
                    _this.setPatientFromSFObject(sfPatient);
                }, function (error) {
                    console.log(error);
                });
            };
            PatientService.prototype.isAnonymous = function () {
                //console.log(patient);
                //console.log(patient.Anonymous__c);
                if (this.patient.anonymous) {
                    return this.patient.anonymous;
                }
                else {
                    return false;
                }
            };
            PatientService.prototype.checkDuplicate = function (patient) {
                var deferred = this.$q.defer();
                var accountId = null;
                if (patient.anonymous) {
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    IQ_PatientActions.CheckDuplicateAnonymous(accountId, patient.label, patient.birthYear, function (result, event) {
                        //console.log(result);
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                else {
                    var dateofBirthString = moment.utc(patient.dateOfBirthString).format('MM/DD/YYYY');
                    if (patient.hospitalId !== null) {
                        accountId = patient.hospitalId;
                    }
                    else if (patient.practice !== undefined && patient.practice.length > 0) {
                        accountId = patient.practice;
                    }
                    else if (patient.hospital.length > 0) {
                        accountId = patient.hospital;
                    }
                    /// TO DO: add email to Apex and call
                    IQ_PatientActions.CheckDuplicate(accountId, patient.lastName, patient.firstName, patient.medicalRecordNumber, dateofBirthString, patient.gender, patient.socialSecurityNumber, patient.patientNumber, patient.accountNumber, patient.race, patient.language, function (result, event) {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }
                return deferred.promise;
            };
            PatientService.prototype.checkAnonymous = function (account) {
                var deferred = this.$q.defer();
                IQ_PatientActions.checkAnonymous(account, function (result, event) {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            PatientService.prototype.getSFObject = function () {
                console.log(this._patient);
                var sfPatient = new orthosensor.domains.sfPatient();
                sfPatient.Id = this._patient.id;
                sfPatient.Anonymous__c = this._patient.anonymous;
                sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
                sfPatient.Hl7__c = this._patient.hl7;
                sfPatient.HospitalId__c = this._patient.hospitalId;
                sfPatient.hospital = this._patient.hospital;
                sfPatient.Last_Name__c = this._patient.lastName;
                sfPatient.First_Name__c = this._patient.firstName;
                sfPatient.practice = this._patient.practice;
                // sfPatient.Anonymous_Label__c = this._patient.label;
                sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
                sfPatient.Email__c = this._patient.email;
                // let date = this._patient.dateofBirth;
                // let newDate = moment(new Date(date)).add(1, 'days');
                sfPatient.Date_Of_Birth__c = this._patient.dateOfBirth;
                sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
                sfPatient.Gender__c = this._patient.gender;
                sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
                sfPatient.Source_Record_Id__c = this._patient.patientNumber;
                sfPatient.Account_Number__c = this._patient.accountNumber;
                sfPatient.Race__c = this._patient.race;
                sfPatient.Language__c = this._patient.language;
                return sfPatient;
            };
            PatientService.prototype.setPatientFromSFObject = function (patient) {
                console.log(patient);
                if (!this._patient) {
                    this._patient = new orthosensor.domains.Patient();
                }
                this._patient.id = patient.Id ? patient.Id : null;
                this._patient.anonymous = patient.Anonymous__c ? patient.Anonymous__c : null;
                this._patient.hl7 = patient.Hl7__c ? patient.Hl7__c : null;
                this._patient.hospitalId = patient.Hospital__r.ParentId ? patient.Hospital__r.ParentId : patient.Hospital__c;
                this._patient.hospital = patient.hospital ? patient.hospital : null;
                this._patient.lastName = patient.Last_Name__c ? patient.Last_Name__c : null;
                this._patient.firstName = patient.First_Name__c ? patient.First_Name__c : null;
                this._patient.practiceId = patient.Hospital__c ? patient.Hospital__c : null;
                this._patient.anonymousLabel = patient.Anonymous_Label__c ? patient.Anonymous_Label__c : null;
                this._patient.medicalRecordNumber = patient.Medical_Record_Number__c ? patient.Medical_Record_Number__c : null;
                this._patient.email = patient.Email__c ? patient.Email__c : null;
                this._patient.dateOfBirth = patient.Date_Of_Birth__c ? patient.Date_Of_Birth__c : null;
                this._patient.dateOfBirthString = patient.Date_Of_Birth__c ? moment(new Date(patient.Date_Of_Birth__c)).format('MM/DD/YYYY') : null;
                this._patient.birthYear = patient.Anonymous_Year_Of_Birth__c ? patient.Anonymous_Year_Of_Birth__c : null;
                this._patient.gender = patient.Gender__c ? patient.Gender__c : null;
                this._patient.socialSecurityNumber = patient.Social_Security_Number__c ? patient.Social_Security_Number__c : null;
                this._patient.patientNumber = patient.Source_Record_Id__c ? patient.Source_Record_Id__c : null;
                this._patient.accountNumber = patient.Account_Number__c ? patient.Account_Number__c : null;
                this._patient.race = patient.Race__c ? patient.Race__c : null;
                this._patient.language = patient.Language__c ? patient.Language__c : null;
            };
            PatientService.prototype.formatDate = function (date) {
                var outputDate = this.moment.utc(date).format('MM/DD/YYYY');
                console.log(outputDate);
                return outputDate;
            };
            return PatientService;
        }());
        PatientService.inject = ['$rootScope', '$q', 'PatientDataService', 'moment'];
        services.PatientService = PatientService;
        angular
            .module('orthosensor.services')
            .service('PatientService', PatientService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patient.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('PatientDetailsFactory', PatientDetailsFactory);
    PatientDetailsFactory.$inject = ['$q', '$rootScope', '$http', 'PatientService'];
    function PatientDetailsFactory($q, $rootScope, $http, PatientService) {
        var service = {
            LoadCasesAndEvents: LoadCasesAndEvents,
            LoadSurveysFromCaseType: LoadSurveysFromCaseType,
        };
        return service;
        ////////////////
        function LoadCasesAndEvents() {
            var deferred = $q.defer();
            var patient = PatientService.patient;
            // console.log(patient);
            IQ_PatientActions.LoadCasesAndEvents(patient.id, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function LoadSurveysFromCaseType(caseId) {
            var deferred = $q.defer();
            IQ_SurveyRepository.LoadSurveysFromCaseType(caseId, function (result, event) {
                //console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=patientDetails.service.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var PatientListFactory = (function () {
            function PatientListFactory($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.$http = $http;
                this.patient = {};
            }
            PatientListFactory.$inject = ["$q", "$rootScope", "$http"];
            Object.defineProperty(PatientListFactory.prototype, "hl7Patients", {
                get: function () {
                    return this._hl7Patients;
                },
                set: function (hl7Patients) {
                    this._hl7Patients = hl7Patients;
                },
                enumerable: true,
                configurable: true
            });
            // hospitalId can be a hospital or practice        
            PatientListFactory.prototype.getPatients = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                if (!hospitalId) {
                    // console.log('hospitalId is not defined at all!');
                    hospitalId = null;
                }
                if (hospitalId === undefined) {
                    // console.log('hospitalId is not defined!');
                    hospitalId = null;
                }
                IQ_PatientActions.LoadPatientList(hospitalId, function (result, event) {
                    //console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getHL7Patients = function (hospitalId) {
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHL7PatientList(hospitalId, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.SearchPatientList = function (hospitalId, keyword) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.SearchPatientList(hospitalId, keyword, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.SearchHL7PatientList = function (hospitalId, keyword) {
                console.log(hospitalId + ', ' + keyword);
                console.log(this.hl7Patients);
                var matching = [];
                if (this.hl7Patients.length > 0) {
                    for (var i = 0; i < this.hl7Patients.length; i++) {
                        if (this.hl7Patients[i].Patient_Last_Name__c.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) {
                            matching.push(this.hl7Patients[i]);
                        }
                    }
                }
                return matching;
            };
            // function getFilterHospitalId() {
            //     return _filterHospitalId;
            // }
            // function setFilterHospitalId(filterHospitalId: string) {
            //     _filterHospitalId = filterHospitalId;
            // }
            // function getFilterPracticeId() {
            //     return _filterPracticeId;
            // }
            // function setFilterPracticeId(filterPracticeId: string) {
            //     _filterPracticeId = filterPracticeId;
            // }
            PatientListFactory.prototype.getCurrentPatient = function () {
                return this.patient;
            };
            PatientListFactory.prototype.setCurrentPatient = function (_patient) {
                this.patient = _patient;
            };
            PatientListFactory.prototype.createCase = function (patient, patientCase) {
                console.log(patient);
                console.log(patientCase);
                var deferred = this.$q.defer();
                var procedureDateString = moment.utc(patientCase.ProcedureDateString).format('MM/DD/YYYY');
                // console.log(procedureDateString);
                IQ_PatientActions.CreateCase(patient.id, patientCase.CaseTypeId, procedureDateString, patientCase.SurgeonID, patientCase.laterality, function (result, event) {
                    //console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadCaseTypes = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log(hospitalId);
                IQ_PatientActions.GetCaseTypes(hospitalId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadSurgeons = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_PatientActions.GetSurgeons(hospitalId, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                // }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadHospitalPracticeSurgeons = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                // if (hospitalId !== null) {
                IQ_DashboardRepository.LoadHospitalPracticeSurgeons(hospitalId, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            //            else {
            //                return null;
            //            }
            //        }
            PatientListFactory.prototype.loadHospitals = function () {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadHospitals(function (result, event) {
                    //console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.loadPractices = function (hospitalId) {
                var _this = this;
                var deferred = this.$q.defer();
                IQ_PatientActions.LoadPractices(function (result, event) {
                    // console.log(result);
                    _this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            _this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getHospitalPractices = function (hospitalId) {
                var deferred = this.$q.defer();
                // console.log(hospitalId);
                IQ_PatientActions.LoadHospitalPractices(hospitalId, function (result, event) {
                    //console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getCaseActivityByPractice = function (practiceId, startDate, endDate) {
                // let IQ_DashboardRepository: any
                var deferred = this.$q.defer();
                // console.log(startDate);
                // console.log(endDate);
                IQ_DashboardRepository.GetProceduresPerMonthByPractice(practiceId, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getCaseActivityBySurgeon = function (surgeonId, startDate, endDate) {
                // let IQ_DashboardRepository: any
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetProceduresPerMonthBySurgeon(surgeonId, startDate, endDate, function (result, event) {
                    //console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getSurgeonPhaseCount = function (id, startDate, endDate) {
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetSurgeonPhaseCount(id, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getPracticePhaseCount = function (id, startDate, endDate) {
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPracticePhaseCount(id, startDate, endDate, function (result, event) {
                    // console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getMonthlyPracticeSensorAverages = function (id, startMonth, startYear, endMonth, endYear) {
                console.log(id, startMonth, startYear, endMonth, endYear);
                /// change this before deployment - set for test data
                // startMonth = 4;
                // startYear = 2016;
                // endMonth = 8;
                // endYear = 2016;
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetPracticeSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            PatientListFactory.prototype.getMonthlySurgeonSensorAverages = function (id, startMonth, startYear, endMonth, endYear) {
                console.log(startMonth, startYear, endMonth, endYear);
                /// change this before deployment - set for test data
                // startMonth = 4;
                // startYear = 2016;
                // endMonth = 8;
                // endYear = 2016;
                var deferred = this.$q.defer();
                IQ_DashboardRepository.GetSurgeonSensorAverages(id, startMonth, startYear, endMonth, endYear, function (result, event) {
                    console.log(result);
                    this.$rootScope.$apply(function () {
                        if (event.status) {
                            deferred.resolve(result);
                        }
                        else {
                            this.$rootScope.handleSessionTimeout(event);
                            deferred.reject(event);
                        }
                    });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            return PatientListFactory;
        }());
        PatientListFactory.inject = ['$q', '$rootScope', '$http'];
        services.PatientListFactory = PatientListFactory;
        angular
            .module('orthosensor.services')
            .service('PatientListFactory', PatientListFactory);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientList.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('SensorService', SensorService);
    SensorService.$inject = ['$q', '$rootScope', '$http'];
    function SensorService($q, $rootScope, $http) {
        var service = {
            AddSensor: AddSensor,
            AddSensorManually: AddSensorManually
        };
        return service;
        ////////////////
        function AddSensor(barcode, procedureId) {
            var deferred = $q.defer();
            console.log(barcode);
            IQ_CaseActions.AddSensor(barcode, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply( () => {
                if (event.status) {
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    deferred.reject(event);
                }
            });
            // }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function AddSensorManually(ref, man, sn, lot, expMonths, expYears, procedureId) {
            var deferred = $q.defer();
            //console.log('adding sensor manually');
            IQ_CaseActions.AddSensorManually(ref, man, sn, lot, expMonths, expYears, procedureId, function (result, event) {
                //console.log(result);
                // $rootScope.$apply(function () {
                if (event.status) {
                    //console.log('succeeding in adding sensor');
                    deferred.resolve(result);
                }
                else {
                    $rootScope.handleSessionTimeout(event);
                    //console.log('failed to add sensor');
                    deferred.reject(event);
                }
            });
            //}, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
    }
})();
//# sourceMappingURL=sensor.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('sessionTimeoutHandler', sessionTimeoutHandler);
    sessionTimeoutHandler.$inject = ['$q', '$injector', '$window'];
    function sessionTimeoutHandler($q, $injector, $window) {
        var sessionHandler = {
            responseError: function (response) {
                // Session has expired
                if (response.status === 500 || response.status === 404 || response.status === 401) {
                    $state.go('dashboard');
                }
                return $q.reject(response);
            }
        };
        return sessionHandler;
    }
    //     var service = {
    //         sessionHandler: sessionHandler
    //     };
    //     function sessionHandler(response) {
    //         // Session has expired
    //         if (response.status == 500 || response.status == 404 || response.status == 401){
    //             $window.location = '/login';
    //         }
    //         return $q.reject(response);
    //     }
    // };
})();
//# sourceMappingURL=sessionTimeout.factory.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var SurveyService = (function () {
            // public $q: ng.IQService;
            // public $rootScope: ng.IRootScopeService;
            function SurveyService($q, $rootScope, $http) {
                this.$q = $q;
                this.$rootScope = $rootScope;
                this.eventTypes = [];
                this.$q = $q;
                this.$rootScope = $rootScope;
            }
            SurveyService.$inject = ["$q", "$rootScope", "$http"];
            // return service;
            ////////////////
            SurveyService.prototype.loadSurveysFromCaseType = function (caseId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.LoadSurveysFromCaseType(caseId, function (result, event) {
                    // console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.loadSurveysFromEvent = function (eventId) {
                this.retrieveSurveysByEventId(eventId)
                    .then(function (data) {
                    var surveys = data;
                    //console.log(eventTypeId);
                    // if (eventTypeId !== '') {
                    //     retrieveSurveysFromEventType(eventTypeId)
                    //         .then(function (data) {
                    //             survey = data;
                    //             console.log('Retrieving data');
                    //             console.log(data);
                    //             //return surveys;
                    //         });
                    // }
                });
                ///return survey;
            };
            SurveyService.prototype.retrieveSurveysFromEventType = function (eventTypeId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveysByEventType(eventTypeId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.retrieveSurveysByEventId = function (eventId) {
                //console.log(eventId);
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveysByEvent(eventId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getEventTypeIdByEventId = function (eventId) {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetEventTypeIdByEventId(eventId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveyByName = function (surveyName) {
                //console.log("getSurveyByName:" + surveyName)
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveyByName(surveyName, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveyById = function (id) {
                var _this = this;
                console.log("getSurveyByName:" + id);
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveyById(id, function (result, event) {
                    console.log(result);
                    //this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.loadSurveys = function () {
                var deferred = this.$q.defer();
                IQ_SurveyRepository.GetSurveys(function (result, event) {
                    // console.log(result);
                    // $evalAsync(function () {
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.getSurveys = function () {
                return this.surveys;
            };
            SurveyService.prototype.setSurveys = function (_surveys) {
                this.surveys = _surveys;
            };
            SurveyService.prototype.getSurvey = function () {
                return this.survey;
            };
            // sets up survey parameters based on survey
            SurveyService.prototype.setSurvey = function (_survey) {
                if (this.survey !== _survey) {
                    this.survey = _survey;
                    console.log(this.survey);
                    console.log(this.survey.Id);
                    if (this.survey.Id !== undefined) {
                        this.getEventTypes(this.survey.Id);
                        if ((this.survey.Name__c !== undefined) || (this.survey.Name__c !== '')) {
                            console.log('Retrieving Survey Score Names');
                            this.setSurveyScoreNames(this.survey.Name__c);
                        }
                    }
                }
            };
            SurveyService.prototype.setSurveyScoreNames = function (surveyName) {
                var _this = this;
                this.retrieveSurveyScoreNames(surveyName)
                    .then(function (data) {
                    _this.surveyScoreNames = data;
                    console.log(_this.surveyScoreNames);
                });
            };
            //retrieve from db
            SurveyService.prototype.retrieveSurveyScoreNames = function (surveyName) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log('Survey Name: ' + surveyName);
                IQ_SurveyRepository.GetScoreNames(surveyName, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, {
                    buffer: false, escape: true, timeout: 120000
                });
                return deferred.promise;
            };
            SurveyService.prototype.setCaseId = function (id) {
                this.caseId = id;
            };
            SurveyService.prototype.getCaseId = function () {
                return this.caseId;
            };
            SurveyService.prototype.retrieveCaseTypeId = function () {
                var _this = this;
                var deferred = this.$q.defer();
                var caseId = this.getCaseId();
                var caseTypeId = IQ_PatientActions.GetCaseTypeIdByCaseId(caseId, function (result, event) {
                    //console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                    // });
                }, { buffer: false, escape: true, timeout: 120000 });
                return deferred.promise;
            };
            SurveyService.prototype.retrieveSurveyEventTypes = function (surveyId) {
                var _this = this;
                var deferred = this.$q.defer();
                console.log(surveyId);
                IQ_SurveyRepository.GetSurveyEventTypes(surveyId, function (result, event) {
                    console.log(result);
                    // this.$rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        _this.$rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                return deferred.promise;
            };
            SurveyService.prototype.setEventTypes = function (events) {
                var j = 0;
                for (var i = 0; i < events.length; i++) {
                    //if (events[i].Event_Type_Name__c != 'Procedure') {
                    this.eventTypes[j] = events[i].Event_Type_Name__c;
                    j++;
                }
                //console.log(eventTypes);
                //["Pre-Op", "4-8 Week Follow-up", "6 Month Follow-up", "12 Month Follow-up", "2 Year Follow-up", "4 Year Follow-up"];
            };
            SurveyService.prototype.getEventTypes = function (surveyId) {
                var _this = this;
                console.log(surveyId);
                return this.retrieveSurveyEventTypes(surveyId)
                    .then(function (data) {
                    //just get array of periods
                    //var periods = data;
                    var categories = data;
                    // console.log(categories);
                    _this.chartCategories = []; //clear array
                    for (var i = 0; i < categories.length; i++) {
                        _this.chartCategories[i] = categories[i].Event_Type__r.Name;
                    }
                });
            };
            SurveyService.prototype.getChartCategories = function () {
                return this.chartCategories;
            };
            // to change how survey score names are set, get them in this method
            // as the data is received and then iterate to get the list of score names        
            SurveyService.prototype.getChartData = function (surveyScores) {
                // console.log(surveyScores);
                var chartData = this.setChartQuestions();
                console.log(chartData);
                if (this.surveyScoreNames) {
                    chartData = this.clearChart(chartData);
                    //cycle though questions
                    for (var q = 0; q < this.surveyScoreNames.length; q++) {
                        //cycle through chartCategories (Time periods)
                        // console.log(this.chartCategories);
                        if (this.chartCategories !== undefined) {
                            for (var i = 0; i < this.chartCategories.length; i++) {
                                //cycle through the scores
                                for (var j = 0; j < surveyScores.length; j++) {
                                    if ((surveyScores[j].Event_Form__r.Event__r.Event_Type_Name__c === this.chartCategories[i]) && (surveyScores[j].Survey_Question__c === this.surveyScoreNames[q])) {
                                        if (isNaN(surveyScores[j].Survey_Response__c)) {
                                            chartData[q][i + 1] = surveyScores[j].Survey_Response__c;
                                        }
                                        else {
                                            chartData[q][i + 1] = parseFloat(surveyScores[j].Survey_Response__c).toFixed(2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return chartData;
            };
            SurveyService.prototype.clearChart = function (chartData) {
                //console.log(survey.Name__c); //where are we when it fails!!!
                //console.log(surveyScoreNames);
                //console.log(chartData);
                if (this.surveyScoreNames) {
                    //Initialize chartData with Null values - required to draw the chart if there is missing scores.
                    for (var k = 0; k < this.surveyScoreNames.length; k++) {
                        if (this.chartCategories !== undefined) {
                            for (var i = 0; i < this.chartCategories.length; i++) {
                                chartData[k][i + 1] = null;
                            }
                        }
                    }
                }
                return chartData;
            };
            //sync chart questions with data
            SurveyService.prototype.setChartQuestions = function () {
                console.log(this.surveyScoreNames);
                if (this.surveyScoreNames) {
                    var chartData = new Array(this.surveyScoreNames.length);
                    if (this.chartCategories !== undefined) {
                        if (this.chartCategories.length !== undefined) {
                            for (var i = 0; i < this.surveyScoreNames.length; i++) {
                                chartData[i] = new Array(this.chartCategories.length);
                                chartData[i][0] = this.surveyScoreNames[i];
                            }
                        }
                    }
                    return chartData;
                }
                else {
                    return [];
                }
            };
            SurveyService.prototype.setGraphParameters = function (anchorTag, chartData) {
                return {
                    bindto: anchorTag,
                    data: {
                        columns: chartData,
                        selection: {
                            enabled: true
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: this.chartCategories,
                            tick: {
                                centered: true
                            }
                        },
                        y: {
                            count: 5
                        },
                        y2: {
                            show: true
                        }
                    },
                    grid: {
                        y: {
                            show: true
                        }
                    },
                    legend: {
                        position: 'left'
                    },
                    tooltip: {
                        show: true
                    }
                };
            };
            return SurveyService;
        }());
        SurveyService.$inject = ['$q', '$rootScope', '$http'];
        services.SurveyService = SurveyService;
        angular
            .module('orthosensor.services')
            .service('SurveyService', SurveyService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=survey.service.js.map
//# sourceMappingURL=test.factory.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        // import orthosensor.domains.UserProfile from orthosensor.domains.orthosensor.domains.UserProfile;
        // import User from orthosensor.domains.User;
        var UserService = (function () {
            function UserService() {
            }
            Object.defineProperty(UserService.prototype, "hasPractices", {
                get: function () {
                    return this._hasPractices;
                },
                set: function (hasPractices) {
                    this._hasPractices = hasPractices;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(UserService.prototype, "isAdmin", {
                get: function () {
                    return this._isAdmin;
                },
                set: function (isAdmin) {
                    this._isAdmin = isAdmin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(UserService.prototype, "user", {
                get: function () {
                    return this._user;
                },
                // get current user from Force.com. this is done in the run.js file.
                // Change if we authenticate differently
                set: function (_user) {
                    this._user = _user;
                },
                enumerable: true,
                configurable: true
            });
            UserService.prototype.isSurgeon = function () {
                // console.log(this.user.Contact.Account.RecordType.Name);
                // return this.user.Contact.Account.RecordType.Name === 'Surgeon';
                return true;
            };
            UserService.prototype.convertSFToObject = function (data) {
                console.log(data);
                var user = new orthosensor.domains.User();
                user.id = data.Id;
                user.name = data.Name;
                user.userType = data.Profile.Name;
                user.userProfile = this.getUserProfile(user.userType);
                if (user.userType !== 'System Administrator') {
                    user.contactId = data.ContactId;
                    user.accountName = data.Contact.Account.Name;
                    user.accountId = data.Contact.Account.Id;
                    user.recordType = data.Contact.Account.RecordType.Name;
                    user.anonymousPatients = data.Contact.Account.Anonymous_Patients__c;
                    if (data.Contact.Account.Parent) {
                        user.hospitalId = data.Contact.Account.ParentId;
                        user.hospitalName = data.Contact.Account.Parent.Name;
                    }
                }
                console.log(user);
                return user;
            };
            UserService.prototype.getUserProfile = function (userType) {
                switch (userType) {
                    case 'System Administrator': return 0 /* SystemAdmin */;
                    case 'Hospital Partner Community User': return 1 /* HospitalAdmin */;
                    case 'Hospital Admin Community Login User': return 1 /* HospitalAdmin */;
                    case 'Hospital Admin Community User': return 1 /* HospitalAdmin */;
                    case 'Surgeon Partner Community User': return 2 /* Surgeon */;
                    case 'Surgeon Community Login User': return 2 /* Surgeon */;
                    case 'Surgeon Community User': return 2 /* Surgeon */;
                    case 'Partner Community User': return 3 /* PracticeAdmin */;
                    case 'Practice Community Login User': return 3 /* PracticeAdmin */;
                    case 'Practice Community User': return 3 /* PracticeAdmin */;
                    case 'Customer Community User': return 3 /* PracticeAdmin */;
                    case 'Customer Community Plus User': return 3 /* PracticeAdmin */;
                    default: return 3 /* PracticeAdmin */;
                }
            };
            return UserService;
        }());
        services.UserService = UserService;
        UserService.$inject = [];
        angular
            .module('orthosensor.services')
            .service('UserService', UserService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=user.service.js.map
angular
    .module("app")
    .component("doctorList", {
    templateUrl: "app/components/doctorList/doctorList.component.html",
});
//# sourceMappingURL=doctorList.component.js.map
(function () {
    'use strict';
    var bootstrapModule = angular.module('common.bootstrap', ['ui.bootstrap']);
    bootstrapModule.factory('bootstrap.dialog', ['$modal', '$templateCache', modalDialog]);
    function modalDialog($modal, $templateCache) {
        var service = {
            deleteDialog: deleteDialog,
            confirmationDialog: confirmationDialog
        };
        $templateCache.put('modalDialog.tpl.html', '<div>' +
            '    <div class="modal-header">' +
            '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-ng-click="cancel()">&times;</button>' +
            '        <h3>{{title}}</h3>' +
            '    </div>' +
            '    <div class="modal-body">' +
            '        <p>{{message}}</p>' +
            '    </div>' +
            '    <div class="modal-footer">' +
            '        <button class="btn btn-primary" data-ng-click="ok()">{{okText}}</button>' +
            '        <button class="btn btn-info" data-ng-click="cancel()">{{cancelText}}</button>' +
            '    </div>' +
            '</div>');
        return service;
        function deleteDialog(itemName) {
            var title = 'Confirm Delete';
            itemName = itemName || 'item';
            var msg = 'Delete ' + itemName + '?';
            return confirmationDialog(title, msg);
        }
        function confirmationDialog(title, msg, okText, cancelText) {
            var modalOptions = {
                templateUrl: 'modalDialog.tpl.html',
                controller: ModalInstance,
                keyboard: true,
                resolve: {
                    options: function () {
                        return {
                            title: title,
                            message: msg,
                            okText: okText,
                            cancelText: cancelText
                        };
                    }
                }
            };
            return $modal.open(modalOptions).result;
        }
    }
    var ModalInstance = ['$scope', '$modalInstance', 'options',
        function ($scope, $modalInstance, options) {
            $scope.title = options.title || 'Title';
            $scope.message = options.message || '';
            $scope.okText = options.okText || 'OK';
            $scope.cancelText = options.cancelText || 'Cancel';
            $scope.ok = function () { $modalInstance.close('ok'); };
            $scope.cancel = function () { $modalInstance.dismiss('cancel'); };
        }];
})();

angular
    .module("app")
    .component("hospitalDetail", {
    template: "app/hospital/hospitalDetail.component.html",
    bindings: {},
    controllerAs: 'vm',
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=hospitalDetail.component.js.map
angular
    .module("app")
    .component("hospitalDetailHeader", {
    templateUrl: "app/components/hospital/hospitalDetailHeader.component.html",
    bindings: {},
    controllerAs: "vm",
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=hospitalDetailHeader.component.js.map
var app;
(function (app) {
    angular
        .module("app")
        .component("hospitalList", {
        template: "\n            <section>\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n            <div class=\"panel-title\">{{vm.title}}</div>\n        </div>\n\n        <div class=\"panel-body\">\n            <table class=\"table table-responsive table-striped\">\n                <thead>\n                    <tr>\n                        <th>Name</th>\n                        <th>ID</th>\n                        \n                    </tr>\n                    <tbody>\n                       <tr ng-repeat=\"hospital in vm.hospitals\" \n                            ui-sref=\"admin.hospitalDetail({hospital})\">\n                            <td>{{hospital.name}}</td>\n                            <td>{{hospital.id}}</td>\n                        \n                        </tr>\n                    </tbody>\n                </thead>\n            </table>\n        </div>\n</section>",
        bindings: {},
        controller: "HospitalsController",
        controllerAs: 'vm'
    });
})(app || (app = {}));
//# sourceMappingURL=hospitalList.component.js.map 
//# sourceMappingURL=hospitalList.component.js.map
(function () {
    'use strict';
    function HospitalsController(HospitalService) {
        var vm = this;
        // vm.hospitalService = hospitalService;
        vm.title = "Hospitals";
        vm.hospitals = [];
        activate();
        function activate() {
            vm.hospitals = getHospitals();
            console.log(vm.hospitals);
        }
        function getHospitals() {
            var hospitals = [];
            HospitalService.getHospitals()
                .then(function (data) {
                console.log(data);
                hospitals = data;
            }, function (error) {
                console.log(error);
            });
            return hospitals;
        }
    }
    HospitalsController.$inject = ['HospitalService'];
    angular
        .module("orthosensor")
        .controller("HospitalsController", HospitalsController);
})();
//# sourceMappingURL=hospitals.controller.js.map
angular
    .module("app")
    .component("practiceDetailModal", {
    template: "app/hospital/proacticeDetailModal.html",
    bindings: {},
    controllerAs: "vm",
    controller: function () {
        var vm = this;
        vm.name = "Fulton Orthopedic";
        vm.id = "8920349";
    }
});
//# sourceMappingURL=practiceDetailModal.component.js.map
angular
    .module("app")
    .component("practiceSummary", {
    template: "app/components/hospital/practiceSummary.component.html",
    bindings: {},
    controller: "PracticeSummaryController",
    controllerAs: "vm",
});
//# sourceMappingURL=practiceSummary.component.js.map
var orthosensor;
(function (orthosensor) {
    var admin;
    (function (admin) {
        // import { Practice } from '../domains/practice';
        // import Modal = 
        var PracticeSummaryController = (function () {
            function PracticeSummaryController($uibModal) {
                this.$uibModal = $uibModal;
                this.title = 'Practices';
                this.practices = [
                    {
                        id: '989098',
                        name: 'Practice A',
                        parentId: '',
                        parentName: '',
                    },
                    {
                        id: '989097',
                        name: 'Practice B',
                        parentId: '',
                        parentName: '',
                    },
                ];
            }
            PracticeSummaryController.$inject = ["$uibModal"];
            PracticeSummaryController.prototype.open = function (practice) {
                var options = {
                    template: '<practice-detail-modal></practice-detail-modal>',
                };
                var modalInstance = this.$uibModal.open(options);
            };
            return PracticeSummaryController;
        }());
        PracticeSummaryController.$inject = ['$uibModal'];
        admin.PracticeSummaryController = PracticeSummaryController;
        angular
            .module('app')
            .controller('PracticeSummaryController', PracticeSummaryController);
    })(admin = orthosensor.admin || (orthosensor.admin = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=practiceSummary.controller.js.map
angular
    .module("orthosensor")
    .component("surgeonSummary", {
    templateUrl: "app/hospital/surgeonSummary.component.html",
    bindings: {},
});
//# sourceMappingURL=surgeonSummary.component.js.map
angular.module('app')
    .factory('contactService', ["$http", "$q", function ($http, $q) {
    return {
        getContactDetails: function (quoteId) {
            var soql = 'SELECT Name FROM Contact Limit 1';
            var url = 'http://cs18.salesforce.com/services/data/v30.0/query?q=' + encodeURIComponent(soql);
            var sessionId = '00D1100000BzLId!ASAAQPOKAZvD05Q1Jy71YcLdkK0Y_tGwK1bqTUcFfYLS8THKJ5DJtC2VXmrswtBWvjjp';
            var deferred = $q.defer();
            $http({ url: url, method: 'GET', headers: { 'Authorization': 'OAuth ' + sessionId } }).success(function (data, status) {
                deferred.resolve(data);
            }).error(function (err, status) {
                console.log(err);
                deferred.reject(err);
            });
            return deferred.promise;
        }
    };
}]);
//# sourceMappingURL=contact.service.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor.services')
        .factory('DataAccessService', DataAccessService);
    //Service.$inject = ['dependency1'];
    function DataAccessService() {
        service = {
            getHospitals: getHospitals,
            getPatientActivity: getPatientActivity
        };
        return service;
        function getHospitals() {
            var hospitals = [
                { "id": "9090", "name": "Albert Einstein Hospital" },
                { "id": "9091", "name": "University Hospital" },
                { "id": "9092", "name": "Holy Cross Hospital" },
                { "id": "9093", "name": "NYU Hospital" },
                { "id": "9094", "name": "Biggets one Hospital" },
                { "id": "9095", "name": "Mayo Clinic" },
            ];
            return hospitals;
        }
        ;
    }
})();
//# sourceMappingURL=dataAccessService.js.map
(function () {
    'use strict';
    // Factory name is handy for logging
    var serviceId = 'datacontext';
    // Define the factory on the module.
    // Inject the dependencies. 
    // Point to the factory definition function.
    angular
        .module('orthosensor.services')
        .factory(serviceId, datacontext);
    function datacontext() {
        // Define the functions and properties to reveal.
        var service = {
            getPatientList: getPatientList,
            getPracticeList: getPracticeList,
            getSurgeonList: getSurgeonList,
            //getPatientActivity: getPatientActivity,
            getPatientActivity1: getPatientActivity1,
            getPatientActivity2: getPatientActivity2,
            getCaseActivityChart: getCaseActivityChart,
            getAllPatientActivity: getAllPatientActivity
        };
        return service;
        function getPatientList() {
            var list = [
                {
                    "id": "109092",
                    "lastName": "Mertz",
                    "firstName": "Fred",
                    "name": "Mertz, Fred"
                },
                {
                    "id": "109093",
                    "lastName": "Ricardo",
                    "firstName": "Lucy",
                    "name": "Ricardo, Lucy"
                },
                {
                    "id": "109094",
                    "lastName": "Ricardo",
                    "firstName": "Ricky",
                    "name": "Ricardo, Ricky"
                },
                {
                    "id": "109095",
                    "lastName": "Masterson",
                    "firstName": "Bat",
                    "name": "Masterson, Bat"
                }
            ];
            return list;
        }
        function getPracticeList() {
            var list = [
                {
                    "id": "0",
                    "practiceName": "All"
                },
                {
                    "id": "109092",
                    "practiceName": "Mertz Orthopedic"
                },
                {
                    "id": "109067",
                    "practiceName": "van Dyke Orthopedic"
                },
                {
                    "id": "109093",
                    "practiceName": "Crosby & Nash Orthopedic"
                },
                {
                    "id": "109094",
                    "practiceName": "Sullivan Systems"
                },
                {
                    "id": "109095",
                    "practiceName": "Podsky Surgical"
                },
                {
                    "id": "109096",
                    "practiceName": "Helium Bormsky"
                },
                {
                    "id": "109098",
                    "practiceName": "Smith Hone and Brown"
                }
            ];
            return list;
        }
        function getSurgeonList() {
            var list = [
                {
                    "id": "0",
                    "surgeonName": "All"
                },
                {
                    "id": "109092",
                    "surgeonName": "Patrick Meere"
                },
                {
                    "id": "109067",
                    "surgeonName": "Martin Roche"
                },
            ];
            return list;
        }
        function getPatientActivity2(practiceId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                //data.push(["Pre-op", 123]);
                data.push(["Consultations", 185]);
                data.push(["Scheduled", 125]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                //data.push(["In long term follow up", 265]);
                data.push(["Inactive", 102]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                //data.push(["Pre-op", 10]);
                data.push(["Consultations", 24]);
                data.push(["Scheduled", 30]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                //data.push(["In long term follow up", 54]);
                data.push(["Inactive", 9]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                //data.push(["Pre-op", 45]);
                data.push(["Consultations", 80]);
                data.push(["cheduled", 85]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                //data.push(["In long term follow up", 76]);
                data.push(["Inactive", 14]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                //data.push(["Pre-op", 10]);
                data.push(["Consultations", 30]);
                data.push(["Scheduled", 45]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                //data.push(["In long term follow up", 54]);
                data.push(["Inactive", 9]);
            }
            else {
                //data.push(["Pre-op", 4]);
                data.push(["Consultations", 23]);
                data.push(["Scheduled", 185]);
                //data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                //data.push(["In long term follow up", 5]);
                data.push(["Inactive", 3]);
            }
            return data;
        }
        function getPatientActivity1(practiceId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                data.push(["Pre-op", 123]);
                //data.push(["Booked / Scheduled", 185]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                data.push(["In long term follow up", 265]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                data.push(["Pre-op", 45]);
                //data.push(["Booked / Scheduled", 20]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                data.push(["In long term follow up", 76]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else {
                data.push(["Pre-op", 4]);
                //data.push(["Booked / Scheduled", 3]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                data.push(["In long term follow up", 5]);
            }
            return data;
        }
        function getCaseActivityChart(practiceId, surgeonId, dateRange) {
            var data = [];
            var practiceName = getPracticeName(practiceId);
            if (practiceName === "All") {
                data.push(["Pre-op", 123]);
                //data.push(["Booked / Scheduled", 185]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
                data.push(["In long term follow up", 265]);
            }
            else if (practiceName === "Mertz Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else if (practiceName === "van Dyke Orthopedic") {
                data.push(["Pre-op", 45]);
                //data.push(["Booked / Scheduled", 20]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 43]);
                data.push(["In long term follow up", 76]);
            }
            else if (practiceName === "Crosby, Still & Nash Orthopedic") {
                data.push(["Pre-op", 10]);
                //data.push(["Booked / Scheduled", 30]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 13]);
                data.push(["In long term follow up", 54]);
            }
            else {
                data.push(["Pre-op", 4]);
                //data.push(["Booked / Scheduled", 3]);
                data.push(["In post-acute follow-up / bundle (1wk – 90) ", 6]);
                data.push(["In long term follow up", 5]);
            }
            return data;
        }
        function getPracticeName(practiceId) {
            return "All";
        }
        function getCaseActivityData() {
            return [];
        }
        // function getPatientActivity1(practiceName, dateRange) {
        //     let data = []
        //     //alert(practiceName);
        //     if (practiceName === "All") {
        //         data = [
        //             { "id": "1", "practiceName": "All", "category": "Pre-op", "count": 123, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "Booked / Scheduled", "count": 185, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 320, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "In long term follow up", "count": 265, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "All", "category": "Inactive", "count": 18, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "Mertz Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Pre-op", "count": 12, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Booked / Scheduled", "count": 18, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 20, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "In long term follow up", "count": 65, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Mertz Orthopedic", "category": "Inactive", "count": 15, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "van Dyke Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Pre-op", "count": 3, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Booked / Scheduled", "count": 8, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 10, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "In long term follow up", "count": 5, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "van Dyke Orthopedic", "category": "Inactive", "count": 1, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else if (practiceName === "Crosby, Still & Nash Orthopedic") {
        //         data = [
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Pre-op", "count": 15, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Booked / Scheduled", "count": 12, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 10, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "In long term follow up", "count": 6, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Crosby, Still & Nash Orthopedic", "category": "Inactive", "count": 5, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     else {
        //         data = [
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Pre-op", "count": 1, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Booked / Scheduled", "count": 1, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "In post-acute follow-up / bundle (1wk – 90)", "count": 2, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "In long term follow up", "count": 15, "procedureDate": "6/7/2016" },
        //             { "id": "1", "practiceName": "Joe Shmoe Podiatry", "category": "Inactive", "count": 1, "procedureDate": "6/7/2016" },
        //         ];
        //     }
        //     return data;
        // }
        // function getPatientActivity2(practiceId, dateRange) {
        //     let data = [];
        //     let allData = getAllPatientActivity();
        //     let practiceData = filterByPractice(practiceId, allData);
        //     //let filteredDateData = filterByDate(dateRange, practiceData);
        //     let aggregatedData = aggregateData(practiceData);
        //     return practiceData;
        // }
        function aggregateData(filteredData) {
            // data.push(["Pre-op", 123]);
            //     data.push(["Booked / Scheduled", 185]);
            //     data.push(["In post-acute follow-up / bundle (1wk – 90) ", 56]);
            //     data.push(["In long term follow up", 265]);
            //     data.push(["Inactive", 165]);
            var preop = 0;
            var booked = 0;
            var postAcuteFollowUp = 0;
            var longTermFollowUp = 0;
            var inactive = 0; //how do we determine???
            for (var i = 0; i < filteredData.length; i++) {
                // need to make sure the procedure date is a date!
                if (filteredData[i].procedureDate < getDate()) {
                    preop++;
                }
                if (filteredData[i].procedureDate < getDate()) {
                    booked++;
                }
            }
        }
        function filterByDate(dateRange, practiceData) {
            var data = [];
            for (var i = 0; i < practiceData.length; i++) {
                if (dateRange === "1 week") {
                    data.push(practiceData[i]);
                }
                else {
                    data.push(practiceData[i]);
                }
            }
            return data;
        }
        function filterByPractice(practiceId, allData) {
            if (practiceId === "0") {
                return allData;
            }
            else {
                var practiceData = [];
                for (var i = 0; i < allData.length; i++) {
                    if (allData[i].practiceId === practiceId) {
                        practiceData.push(allData[i]);
                    }
                }
                return practiceData;
            }
        }
        function getAllPatientActivity() {
            var data = [
                {
                    "id": "1",
                    "practiceId": "109092",
                    "patientLastName": "Smith",
                    "patientFirst": "James",
                    "procedureDate": "6/7/2016"
                },
                {
                    "id": "2",
                    "practiceId": "109067",
                    "patientLastName": "Smith",
                    "patientFirst": "James",
                    "procedureDate": "4/7/2016"
                },
                {
                    "id": "3",
                    "practiceId": "109092",
                    "patientLastName": "Jones",
                    "patientFirst": "James",
                    "procedureDate": "11/7/2016"
                },
            ];
            return data;
        }
    }
})();
//# sourceMappingURL=datacontext.js.map
var mockResource = angular
    .module("patientResourceMock", ["ngMockE2E"]);
mockResource.run(mockRun);
mockRun.$inject = ["$httpBackend"];
function mockRun($httpBackend) {
    var patients = [];
    var patient;
    patient = new orthosensor.domains.Patient("1", "Johnny", "Q", "Rotten", new Date(2009, 2, 19), "Male", "English", "RottenJohnny01", "0909", "OS-9099", "", "OrthoTest");
    patients.push(patient);
    var patientUrl = "/api/patientlist";
    $httpBackend.whenGET(patientUrl).respond(patients);
    var editingRegex = new RegExp(patientUrl + "/[0-9][0-9]*", '');
    $httpBackend.whenGET(editingRegex).respond(function (method, url, data) {
        var patient = { "id": 0 };
        var parameters = url.split('/');
        var length = parameters.length;
        var id = parameters[length - 1];
        // if (id > 0) {
        //     for (var i = 0; i < products.length; i++) {
        //         if (products[i].productId == id) {
        //             product = products[i];
        //             break;
        //         }
        //     }
        // }
        return [200, patient, {}];
    });
    // Catch all for testing purposes
    $httpBackend.whenGET(/api/).respond(function (method, url, data) {
        return [200, patients, {}];
    });
    // Pass through any requests for application files
    $httpBackend.whenGET(/app/).passThrough();
}
//# sourceMappingURL=patientResourceMock.js.map
var orthosensor;
(function (orthosensor) {
    var AddPatientCaseController = (function () {
        function AddPatientCaseController($rootScope, $state, UserService, PatientListFactory, PatientService, CaseService, HospitalService, $timeout) {
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.UserService = UserService;
            this.PatientListFactory = PatientListFactory;
            this.PatientService = PatientService;
            this.CaseService = CaseService;
            this.HospitalService = HospitalService;
            this.$timeout = $timeout;
            this.title = 'New Patient Case';
            //this.patient = patient;
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false
            };
            this.multipleCaseTypes = false;
        }
        AddPatientCaseController.prototype.$onInit = function () {
            this.UserService = this.UserService;
            this.user = this.UserService.user;
            this.patient = this.PatientService.patient;
            console.log(this.patient);
            var today = new Date();
            var today_utc = new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate(), today.getUTCHours(), today.getUTCMinutes(), today.getUTCSeconds());
            console.log(today_utc);
            //this.patient.dateofBirthString = dob_utc;
            this.patientCase = {
                CaseTypeId: '',
                ProcedureDateString: today_utc,
                laterality: 'Left',
                SurgeonID: ''
            };
            console.log(this.patientCase.ProcedureDateString);
            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }
        };
        AddPatientCaseController.prototype.$onChanges = function (patient) {
            console.log(patient);
            console.log(this.patient);
            if (this.patient) {
                this.loadCaseTypes();
                this.loadSurgeons();
                this.loadingCase = false; //test to see if this helps!
            }
        };
        AddPatientCaseController.prototype.loadCaseTypes = function () {
            var _this = this;
            console.log(this.user.accountId);
            this.PatientListFactory.loadCaseTypes(this.user.accountId)
                .then(function (data) {
                console.log(data);
                _this.caseTypes = data;
                _this.multipleCaseTypes = _this.caseTypes.length > 1;
                if (_this.caseTypes.length === 1) {
                    _this.caseTypeDescription = _this.caseTypes[0].Case_Type__r.Description__c;
                    _this.patientCase.CaseTypeId = _this.caseTypes[0].Case_Type__c;
                }
            }, function (error) {
                console.log(error);
            });
        };
        AddPatientCaseController.prototype.loadSurgeons = function () {
            this.surgeons = this.HospitalService.surgeons;
        };
        // this.open = { procDate: false };
        AddPatientCaseController.prototype.open = function ($event, whichDate) {
            //$event.preventDefault();
            //$event.stopPropagation();
            this.open[whichDate] = true;
        };
        AddPatientCaseController.prototype.cancel = function () {
            // this.$uibModalInstance.dismiss('cancel');
        };
        AddPatientCaseController.prototype.createCase = function (pageForm) {
            var _this = this;
            console.log(this.patient);
            console.log(this.patientCase);
            if (pageForm.$valid) {
                this.loadingCase = true;
                this.$timeout(function () {
                    if (_this.patientCase.laterality === 'Both') {
                        _this.patientCase.laterality = 'Left';
                        _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                            .then(function (data) {
                            _this.patientCase.laterality = 'Right';
                            _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                                .then(function (data) {
                                _this.$rootScope.case = data;
                                _this.CaseService.setCurrentCase(data);
                                _this.$state.go('caseDetails');
                            }, function (error) {
                                console.log(error);
                            });
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    else {
                        _this.PatientListFactory.createCase(_this.patient, _this.patientCase)
                            .then(function (data) {
                            // console.log(data);              
                            // this.$uibModalInstance.close('OK');
                            _this.$rootScope.case = data;
                            _this.$state.go('caseDetails');
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    // this.loadingCase = false;
                }, 1000);
            }
            else {
                pageForm.submitted = true;
            }
            // this.loadingCase = false;
            // Ladda.stopAll();
        };
        return AddPatientCaseController;
    }());
    AddPatientCaseController.$inject = ['$rootScope', '$state', 'UserService', 'PatientListFactory', 'PatientService', 'CaseService', 'HospitalService', '$timeout'];
    var AddPatientCase = (function () {
        function AddPatientCase() {
            this.bindings = {
                patient: '<',
            };
            this.controller = AddPatientCaseController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatientCase/addPatientCase.component.html';
        }
        return AddPatientCase;
    }());
    angular
        .module('orthosensor')
        .component('osAddPatientCase', new AddPatientCase());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=addPatientCase.component.js.map
(function () {
    'use strict';
    // this is not used yet. We need to consoloidate this with the addPatient controller
    // Ultimately, have one controller and pass in the patient information as needed.
    angular
        .module('orthosensor')
        .controller('AddHL7Patient', AddHL7Patient);
    AddHL7Patient.$inject = ['$rootScope', '$state', '$timeout', '$uibModalInstance', 'PatientListFactory', 'PatientService', 'DashboardService'];
    function AddHL7Patient($rootScope, $state, $timeout, $uibModalInstance, PatientListFactory, PatientService, DashboardService) {
        var $ctrl = this;
    }
})();
//# sourceMappingURL=addHl7Patient.controller.js.map
var orthosensor;
(function (orthosensor) {
    var AddPatientController = (function () {
        function AddPatientController($rootScope, $state, $timeout, UserService, PatientListFactory, PatientService, PatientDataService, HospitalService, PatientDetailsFactory, SFDataService, moment) {
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.$timeout = $timeout;
            this.UserService = UserService;
            this.PatientService = PatientService;
            this.PatientDataService = PatientDataService;
            this.HospitalService = HospitalService;
            this.PatientDetailsFactory = PatientDetailsFactory;
            this.SFDataService = SFDataService;
            this.moment = moment;
            this.title = 'New Patient';
            this.user = UserService.user;
            this.PatientListFactory = PatientListFactory;
        }
        AddPatientController.prototype.$onInit = function () {
            this.user = this.UserService.user;
            console.log(this.user);
            this.isUserAdmin = this.UserService.isAdmin;
            this.hospitals = this.HospitalService.hospitals;
            this.practices = this.HospitalService.practices;
            console.log(this.practices);
            this.languages = this.PatientDataService.getLanguages();
            this.races = this.PatientDataService.getRaces();
            console.log(this.hospitals);
            console.log(this.practices);
            console.log(this.PatientService.patient);
            if (this.PatientService.patient === null) {
                this.createPatient();
                this.mode = 'Create';
            }
            else {
                this.patient = this.PatientService.patient;
                this.mode = this.patient.hl7 === true ? 'Create' : 'Edit';
                console.log(this.mode);
                var dob = new Date(this.patient.dateOfBirth);
                var dob_utc = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
                console.log(dob_utc);
                this.patient.dateOfBirthString = dob_utc;
                var maxDate = new Date();
                this.maxBirthDate = new Date(maxDate.getUTCFullYear(), maxDate.getUTCMonth(), maxDate.getUTCDate(), maxDate.getUTCHours(), maxDate.getUTCMinutes(), maxDate.getUTCSeconds());
                console.log(dob_utc);
                //this.patient.dateOfBirthString = new Date(this.patient.dateOfBirth);
                //this.patient.dateofBirthString = new Date();;
                console.log(this.patient);
                this.title = 'Edit Patient';
                // this.mode = 'Edit';
            }
            var today = new Date();
            var startYear = this.moment().subtract(65, 'year').year();
            var month = this.moment(today).month();
            var day = this.moment(today).day();
            console.log(startYear + ',' + month + ',' + day);
            this.dateOptions = {
                formatYear: 'yy',
                startingDay: 1,
                showWeeks: false,
                initDate: new Date(startYear, month, day),
                maxDate: new Date(),
                minDate: new Date(1920, 1, 1)
            };
            // this.patient.dateofBirthString = new Date(1945, 8, 1);
            this.birthYears = [];
            for (var i = new Date().getFullYear(); i > 1920; i--) {
                this.birthYears.push(i);
            }
        };
        //this.open = { birthDate: false };
        AddPatientController.prototype.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            this.open[whichDate] = true;
        };
        AddPatientController.prototype.cancel = function () {
            this.$state.go('patientList');
        };
        AddPatientController.prototype.resetPractice = function () {
            var _this = this;
            this.SFDataService.loadHospitalPractices(this.patient.hospital.id)
                .then(function (data) {
                console.log(data);
                _this.practices = _this.HospitalService.convertSFPracticeToObject(data);
            }, function (error) {
                console.log(error);
            });
            this.patient.practice = undefined;
        };
        AddPatientController.prototype.checkAnonymous = function () {
            var _this = this;
            var accountId = this.patient.hospital;
            console.log(this.patient.hospital);
            console.log(this.patient.hl7);
            console.log(this.patient);
            if (this.patient.practice !== undefined && this.patient.practice.length > 0) {
                accountId = this.patient.practice;
            }
            this.PatientService.checkAnonymous(accountId)
                .then(function (data) {
                //console.log(data);
                _this.patient.anonymous = data;
            }, function (error) {
                console.log(error);
            });
        };
        AddPatientController.prototype.createPatient = function () {
            this.patient = new orthosensor.domains.Patient();
            this.patient.anonymous = this.UserService.isAdmin ? this.user.anonymousPatients : false;
            this.patient.hl7 = false;
            this.patient.hospitalId = this.UserService.isAdmin ? null : this.user.accountId;
            this.patient.hospital = ''; //For Internal Users only - required
            this.patient.practice = ''; //For Internal Users only - optional
            this.patient.practiceId = this.UserService.isAdmin ? null : this.user.accountId;
            this.patient.lastName = '';
            this.patient.firstName = '';
            this.patient.label = '';
            this.patient.medicalRecordNumber = '';
            this.patient.email = '';
            this.patient.dateOfBirthString = '';
            this.patient.birthYear = null;
            this.patient.gender = '';
            this.patient.socialSecurityNumber = '';
            this.patient.patientNumber = '';
            this.patient.accountNumber = '';
            this.patient.race = '';
            this.patient.language = '';
        };
        AddPatientController.prototype.createNewPatient = function (pageForm) {
            if (pageForm.$valid) {
                this.loadingPatient = true;
                this.$timeout(function () {
                    var _this = this;
                    this.PatientDataService.createPatient(this.patient)
                        .then(function (data) {
                        console.log(data);
                        _this.PatientService.patient = data;
                        _this.$state.go('patientDetails');
                        _this.loading = false;
                    }, function (error) {
                        console.log(error);
                    });
                }, 1000);
            }
            else {
                pageForm.submitted = true;
            }
            //this.loading = false;
        };
        AddPatientController.prototype.savePatientOnly = function (pageForm) {
            this.savePatient(pageForm, 'patientList');
            //            this.$state.go('patientList');
        };
        AddPatientController.prototype.savePatientAndAddCase = function (pageForm) {
            this.savePatient(pageForm, 'addPatientCase');
            this.$state.go('addPatientCase');
        };
        AddPatientController.prototype.savePatient = function (pageForm, nextForm) {
            var _this = this;
            console.log(this.mode);
            if (pageForm.$valid) {
                this.loadingPatient = true;
                if (!this.isUserAdmin) {
                    this.patient.hospitalId = this.user.accountId;
                }
                else {
                    console.log(this.patient.practice);
                    this.patient.hospitalId = this.patient.practice;
                }
                console.log(this.patient);
                this.PatientService.patient = this.patient;
                if (this.mode === 'Create') {
                    this.PatientDataService.checkDuplicate(this.patient)
                        .then(function (data) {
                        //console.log(data); 
                        if (data != null) {
                            _this.matchedPatient = data;
                        }
                        else {
                            _this.PatientDataService.createPatient(_this.patient)
                                .then(function (newPatient) {
                                _this.PatientService.patient.id = newPatient.Id;
                                _this.$state.go(nextForm);
                            }, function (error) {
                                console.log(error);
                            });
                        }
                    }, function (error) {
                        console.log(error);
                    });
                }
                else {
                    // let sfPatient = this.PatientService.getSFObject();
                    console.log(this.patient);
                    this.PatientDataService.updatePatient(this.patient)
                        .then(function (data) {
                        console.log(data);
                        // this.PatientService.patient = data;
                        // $rootScope.patient = data;
                        console.log(data);
                        //PatientService.setPatient(data);
                        _this.$state.go(nextForm);
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            else {
                pageForm.submitted = true;
            }
        };
        return AddPatientController;
    }());
    AddPatientController.$inject = ['$rootScope', '$state', '$timeout', 'UserService', 'PatientListFactory', 'PatientService', 'PatientDataService', 'HospitalService', 'PatientDetailsFactory', 'SFDataService', 'moment'];
    var AddPatient = (function () {
        function AddPatient() {
            this.bindings = {};
            this.controller = AddPatientController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/addPatient/addPatient.component.html';
        }
        return AddPatient;
    }());
    angular
        .module('orthosensor')
        .component('osAddPatient', new AddPatient());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=addPatient.component.js.map
var app;
(function (app) {
    angular
        .module("app")
        .component("adminSidebar", {
        templateUrl: "app/components/adminSidebar/adminSidebar.component.html",
        bindings: {}
    });
})(app || (app = {}));
//# sourceMappingURL=adminSidebar.component.js.map 
//# sourceMappingURL=adminSidebar.component.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseImplantInfo', {
        templateUrl: 'app/components/caseImplantInfo/caseImplantInfo.component.html',
        controller: CaseImplantInfoController,
        bindings: {
            event: '<'
        }
    });
    CaseImplantInfoController.$inject = ['CaseDetailsFactory', '$confirm'];
    function CaseImplantInfoController(CaseDetailsFactory, $confirm) {
        var ctrl = this;
        ctrl.implantEntryType = 'barcode';
        ctrl.selectedProduct = '';
        ctrl.onSelectProduct = onSelectProduct;
        ctrl.loadProducts = loadProducts;
        ctrl.checkAndSaveImplantBarcode = checkAndSaveImplantBarcode;
        ctrl.addImplant = addImplant;
        ctrl.isEnterKeyPressed = isEnterKeyPressed;
        ctrl.deleteImplant = deleteImplant;
        ////////////////
        ctrl.$onInit = function () {
            loadProducts();
        };
        ctrl.$onChanges = function (changesObj) { };
        ctrl.$onDestory = function () { };
        //Implants
        function loadProducts() {
            CaseDetailsFactory.LoadProducts().then(function (data) {
                //console.log(data);
                ctrl.products = data;
            }, function (error) {
            });
        }
        function onSelectProduct($item, $model, $label) {
            ctrl.selectedProduct = $item;
        }
        function checkAndSaveImplantBarcode() {
            console.log(ctrl.implantEntryType);
            if (ctrl.implantEntryType === 'barcode') {
                console.log(ctrl.firstbar);
                if ((angular.isDefined(ctrl.firstbar) && ctrl.firstbar.indexOf('/') !== -1) || (angular.isDefined(ctrl.firstbar) && ctrl.firstbar.length > 25)) {
                    ctrl.secondbar = '';
                    ctrl.month = '';
                    ctrl.year = '';
                    ctrl.addImplant();
                }
                else if (angular.isDefined(ctrl.firstbar) && ctrl.firstbar.length > 0 &&
                    angular.isDefined(ctrl.secondbar) && ctrl.secondbar.length > 0 &&
                    angular.isDefined(ctrl.month) && ctrl.month != null &&
                    angular.isDefined(ctrl.year) && ctrl.year != null) {
                    ctrl.addImplant();
                }
                else {
                    Ladda.stopAll();
                }
            }
            else if (ctrl.implantEntryType === 'manual') {
                addImplant();
            }
        }
        function addImplant() {
            console.log('called?');
            var l = Ladda.create(document.querySelector('.implant-btn'));
            l.start();
            if (ctrl.implantEntryType === 'barcode') {
                CaseDetailsFactory.AddImplant(ctrl.firstbar, ctrl.secondbar, ctrl.month, ctrl.year, ctrl.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    ctrl.firstbar = '';
                    ctrl.secondbar = '';
                    ctrl.month = 'null';
                    ctrl.year = 'null';
                    angular.element(document.querySelector('#firstbar'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
            else if (ctrl.implantEntryType === 'manual') {
                CaseDetailsFactory.AddImplantManually(ctrl.selectedProduct.Id, ctrl.lotNumber, ctrl.month, ctrl.year, ctrl.procedure.Id).then(function (data) {
                    //console.log(data);
                    refreshEvents();
                    ctrl.selectedProduct = '';
                    ctrl.lotNumber = '';
                    ctrl.month = 'null';
                    ctrl.year = 'null';
                    angular.element(document.querySelector('#ProductNumber'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    console.log(error);
                    Ladda.stopAll();
                });
            }
        }
        function isEnterKeyPressed(event) {
            if (event.keyCode === 13) {
                angular.element(document.querySelector('#secondbar'))[0].focus();
            }
        }
        function deleteImplant(implant) {
            $confirm({ text: 'Are you sure you want to delete this implant (' + implant.Product_Catalog_Id__r.Name + ')?', title: 'Delete Implant' })
                .then(function () {
                CaseDetailsFactory.DeleteImplant(implant.Id).then(function (data) {
                    refreshEvents();
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
        ;
    }
})();
//# sourceMappingURL=caseImplantInfo.component.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor')
        .component('osAddCaseNote', {
        templateUrl: 'app/components/caseNotes/addCaseNote.component.html',
        controller: AddCaseNoteController,
        bindings: {
            procedure: '<'
        },
    });
    AddCaseNoteController.inject = ['$state', '$uibModalInstance', 'CaseDetailsFactory', 'CaseService'];
    function AddCaseNoteController($state, $uibModalInstance, CaseDetailsFactory, CaseService) {
        var $ctrl = this;
        $ctrl.addNote = addNote;
        $ctrl.procedure = CaseService.getProcedure();
        console.log($ctrl.procedure);
        $ctrl.procedureId = $ctrl.procedure.Id;
        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function addNote() {
            console.log($ctrl.caseTitle);
            console.log($ctrl.caseNote);
            console.log($ctrl.procedureId);
            CaseDetailsFactory.AddNote($ctrl.caseTitle, $ctrl.caseNote, $ctrl.procedureId)
                .then(function (data) {
                //console.log(data);
                $uibModalInstance.close('OK');
                // $state.go($state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        }
        activate();
        ////////////////
        function activate() { }
    }
})();
//# sourceMappingURL=addCaseNote.component.js.map
(function () {
    'use strict';
    // Usage: individual notes
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseNote', {
        templateUrl: 'app/components/caseNotes/caseNote.component.html',
        controller: CaseNoteController,
        bindings: {
            note: '=',
        },
    });
    CaseNoteController.inject = ['$uibModal', 'CaseDetailsFactory', '$confirm'];
    function CaseNoteController($uibModal, CaseDetailsFactory, $confirm) {
        var ctrl = this;
        ctrl.editCaseNote = editCaseNote;
        ctrl.deleteCaseNote = deleteCaseNote;
        ////////////////
        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
        function editCaseNote(note) {
            $uibModal.open({
                templateUrl: 'app/components/caseNotes/editCaseNote.component.html',
                controller: function ($state, $uibModalInstance, CaseDetailsFactory) {
                    var $ctrl = this;
                    $ctrl.caseTitle = note.Title;
                    $ctrl.caseNote = note.Body;
                    $ctrl.noteId = note.Id;
                    $ctrl.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                    $ctrl.updateNote = function () {
                        CaseDetailsFactory.UpdateNote($ctrl.caseTitle, $ctrl.caseNote, $ctrl.noteId).then(function (data) {
                            //console.log(data);
                            $uibModalInstance.dismiss();
                            $state.go($state.current, {}, { reload: true });
                        }, function (error) {
                            console.log(error);
                        });
                    };
                },
                size: 'large'
            });
        }
        function deleteCaseNote(note) {
            $confirm({ text: 'Are you sure you want to delete this note?', title: 'Delete Note' })
                .then(function () {
                CaseDetailsFactory.DeleteNote(note.Id).then(function (data) {
                    //console.log(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        }
    }
})();
//# sourceMappingURL=caseNote.component.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osCaseNotes', {
        templateUrl: 'app/components/caseNotes/caseNotes.component.html',
        controller: CaseNotesController,
        bindings: {
            procedure: '<',
            notes: '<'
        }
    });
    CaseNotesController.$inject = ['$state', '$uibModal', '$confirm', 'CaseDetailsFactory', 'CaseService', 'LogFactory'];
    function CaseNotesController($state, $uibModal, $confirm, CaseDetailsFactory, CaseService, LogFactory) {
        var ctrl = this;
        ctrl.addNewCaseNote = addNewCaseNote;
        // ctrl.editCaseNote = editCaseNote;
        var procedureId = '';
        ////////////////
        ctrl.$onInit = function () {
            console.log(ctrl.procedure);
            console.log(ctrl.notes);
            ctrl.title = this.procedure.Id;
            procedureId = this.procedure.Id;
            CaseService.setProcedure(ctrl.procedure);
        };
        ctrl.$onChanges = function (changesObj) { };
        ctrl.$onDestory = function () { };
        //case note methods        
        function addNewCaseNote() {
            $uibModal.open({
                template: '<os-add-case-note></os-add-casenote>',
                //app/components/caseNotes/ addCaseNote.component.html',
                // controller: 'AddCaseNoteController',
                //  controllerAs: '$ctrl',
                size: 'medium'
            });
        }
        function editCaseNote(note) {
            $uibModal.open({
                templateUrl: 'app/components/caseNotes/editCaseNote.component.html',
                controller: 'EditCaseNoteController',
                controllerAs: '$ctrl',
                size: 'medium'
            });
        }
    }
})();
//# sourceMappingURL=caseNotes.component.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('EditCaseNoteController', EditCaseNoteController);
    EditCaseNoteController.inject = ['$state', '$uibModalInstance', 'CaseDetailsFactory'];
    function EditCaseNoteController($state, $uibModalInstance, CaseDetailsFactory) {
        var ctrl = this;
        ctrl.caseTitle = note.Title;
        ctrl.caseNote = note.Body;
        ctrl.noteId = note.Id;
        ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        ctrl.updateNote = function () {
            CaseDetailsFactory.UpdateNote(ctrl.caseTitle, ctrl.caseNote, ctrl.noteId).then(function (data) {
                //console.log(data);
                $uibModalInstance.dismiss();
                $state.go($state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        };
        activate();
        ////////////////
        function activate() { }
    }
    EditCaseNoteController.$inject = ["$state", "$uibModalInstance", "CaseDetailsFactory"];
})();
//# sourceMappingURL=editCaseNotes.controller.js.map
var orthosensor;
(function (orthosensor) {
    // 
    var CaseSensorInfoController = (function () {
        function CaseSensorInfoController(CaseDetailsFactory, SensorService) {
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.SensorService = SensorService;
            this.sensorEntryType = 'barcode';
            this.sensorEntryType = 'barcode';
        }
        ////////////////
        CaseSensorInfoController.prototype.$onInit = function () {
            console.log(this.event);
            this.sensorEntryType = 'barcode';
            this.loadSensorRefs();
        };
        // this.$onChanges = function (changesObj) { };
        // this.$onDestory = function () { };
        //Sensors
        CaseSensorInfoController.prototype.loadSensorRefs = function () {
            var _this = this;
            this.CaseDetailsFactory.loadSensorRefs()
                .then(function (data) {
                //console.log(data);
                _this.sensorRefs = data;
            }, function (error) {
            });
        };
        CaseSensorInfoController.prototype.onSelectRef = function ($item, $model, $label) {
            this.selectedRef = $item;
            setTimeout(function () {
                angular.element(document.querySelector('#serialNumber'))[0].focus();
            }, 200);
        };
        CaseSensorInfoController.prototype.addSensor = function () {
            var l = Ladda.create(document.querySelector('.sensor-btn'));
            l.start();
            if (this.sensorEntryType === 'barcode') {
                this.SensorService.AddSensor(this.sensorCode, this.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    //loadCaseEvents();
                    this.sensorCode = '';
                    angular.element(document.querySelector('#sensorCode'))[0].focus();
                    Ladda.stopAll();
                }, function (error) {
                    Ladda.stopAll();
                });
            }
        };
        CaseSensorInfoController.prototype.isEnterKeyPressedSensor = function (event) {
            if (event.keyCode === 13) {
                this.addSensor();
            }
        };
        return CaseSensorInfoController;
    }());
    CaseSensorInfoController.$inject = ['CaseDetailsFactory', 'SensorService'];
    var CaseSensorInfo = (function () {
        function CaseSensorInfo() {
            this.bindings = {};
            this.controller = CaseSensorInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/caseSensorInfo/caseSensorInfo.component.html';
        }
        return CaseSensorInfo;
    }());
    angular
        .module('orthosensor')
        .component('osCaseSensorInfo', new CaseSensorInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseSensorInfo.component.js.map
var orthosensor;
(function (orthosensor) {
    var CaseSensorItemsController = (function () {
        function CaseSensorItemsController(CaseDetailsFactory, $confirm) {
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.$confirm = $confirm;
            this.onChanges = function (changesObj) { };
            this.onDestroy = function () { };
        }
        CaseSensorItemsController.prototype.$onInit = function () {
            this.procedure = this.event;
            console.log(this.event);
        };
        ;
        CaseSensorItemsController.prototype.deleteSensor = function (device) {
            var _this = this;
            this.$confirm({ text: 'Are you sure you want to delete this sensor (' + device.OS_Device__r.Device_ID__c + ')?', title: 'Delete Sensor' })
                .then(function () {
                _this.CaseDetailsFactory.DeleteSensor(device.OS_Device__r.Id, _this.procedure.Id)
                    .then(function (data) {
                    //console.log(data);
                    // $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            });
        };
        ;
        return CaseSensorItemsController;
    }());
    CaseSensorItemsController.$inject = ['CaseDetailsFactory', '$confirm'];
    var CaseSensorItems = (function () {
        function CaseSensorItems() {
            this.bindings = {
                // clinicalData: '<',
                event: '<',
            };
            this.controller = CaseSensorItemsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/caseSensorInfo/caseSensorItems.component.html';
        }
        return CaseSensorItems;
    }());
    angular
        .module('orthosensor')
        .component('osCaseSensorItems', new CaseSensorItems());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseSensorItems.component.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor')
        .controller('editPatientController', editPatientController);
    editPatientController.$inject = ['$scope', '$rootScope', '$state', '$uibModalInstance', 'PatientDetailsFactory', 'PatientService'];
    function editPatientController($scope, $rootScope, $state, $uibModalInstance, PatientDetailsFactory, PatientService) {
        var patient = PatientService.getSFObject();
        console.log(patient);
        var dob = new Date(patient.Date_Of_Birth__c);
        var dob_utc = new Date(dob.getUTCFullYear(), dob.getUTCMonth(), dob.getUTCDate(), dob.getUTCHours(), dob.getUTCMinutes(), dob.getUTCSeconds());
        $scope.patient = [];
        $scope.patient.Id = patient.Id;
        $scope.patient.anonymous = patient.Anonymous__c;
        $scope.patient.Label = patient.Anonymous_Label__c ? patient.Anonymous_Label__c : '';
        $scope.patient.BirthYear = patient.Anonymous_Year_of_Birth__c ? patient.Anonymous_Year_of_Birth__c : '';
        $scope.patient.LastName = patient.Last_Name__c ? patient.Last_Name__c : '';
        $scope.patient.FirstName = patient.First_Name__c ? patient.First_Name__c : '';
        $scope.patient.MedicalRecordNumber = patient.Medical_Record_Number__c ? patient.Medical_Record_Number__c : '';
        $scope.patient.dateOfBirthString = dob_utc;
        $scope.patient.Gender = patient.Gender__c ? patient.Gender__c : '';
        $scope.patient.SocialSecurityNumber = patient.SSN__c ? patient.SSN__c : '';
        $scope.patient.PatientNumber = patient.Source_Record_Id__c ? patient.Source_Record_Id__c : '';
        $scope.patient.AccountNumber = patient.Account_Number__c ? patient.Account_Number__c : '';
        $scope.patient.Race = patient.Race__c ? patient.Race__c : '';
        $scope.patient.Language = patient.Language__c ? patient.Language__c : '';
        $scope.open = { birthDate: false };
        $scope.open = function ($event, whichDate) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.open[whichDate] = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };
        $scope.birthYears = [];
        for (var i = new Date().getFullYear(); i > 1900; i--) {
            $scope.birthYears.push(i);
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.updatePatient = function () {
            if ($scope.pageForm.$valid) {
                PatientDetailsFactory.UpdatePatient($scope.patient).then(function (data) {
                    console.log(data);
                    $uibModalInstance.dismiss();
                    PatientService.patient = data;
                    //$rootScope.patient = data;
                    console.log(data);
                    //PatientService.setPatient(data);
                    $state.go($state.current, {}, { reload: true });
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                Ladda.stopAll();
                $scope.pageForm.submitted = true;
            }
        };
    }
})();
//# sourceMappingURL=editPatient.controller.js.map
var orthosensor;
(function (orthosensor) {
    var EditProcedureController = (function () {
        function EditProcedureController(PatientService, CaseDetailsFactory, DashboardService, CaseService, $timeout, $state, HospitalService) {
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.DashboardService = DashboardService;
            this.CaseService = CaseService;
            this.$timeout = $timeout;
            this.$state = $state;
            this.HospitalService = HospitalService;
            this.popup = {
                opened: false
            };
            this.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(2014, 1, 1),
                startingDay: 1,
                showWeeks: false
            };
        }
        EditProcedureController.prototype.$onInit = function () {
            this.patient = this.PatientService.getPatient();
            this.surgeons = this.HospitalService.surgeons;
            var procedure = this.CaseService.getProcedure();
            var patientCase = this.CaseService.currentCase;
            console.log(procedure);
            var clinicalData = this.CaseService.clinicalData;
            this.procedureData = {
                Id: '',
                caseId: '',
                physician: '',
                laterality: '',
                procedureDateString: '',
                patientConsentObtained: false,
                anesthesiaType: ''
            };
            this.procedureData.Id = procedure.Id;
            this.procedureData.caseId = patientCase.Id;
            this.procedureData.physician = procedure.Physician__c;
            this.procedureData.laterality = patientCase.Laterality__c;
            var procDate = new Date(procedure.Appointment_Start__c);
            console.log(procDate);
            console.log(new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds()));
            this.procedureData.procedureDateString = new Date(procDate.getUTCFullYear(), procDate.getUTCMonth(), procDate.getUTCDate(), procDate.getUTCHours(), procDate.getUTCMinutes(), procDate.getUTCSeconds());
            this.procedureData.patientConsentObtained = clinicalData.Patient_Consent_Obtained__c;
            this.procedureData.anesthesiaType = clinicalData.Anesthesia_Type__c;
            console.log(this.procedureData.procedureDateString);
        };
        EditProcedureController.prototype.completeEvent = function (eventId) {
            this.CaseDetailsFactory.CompleteEvent(eventId)
                .then(function (data) {
                //console.log(data);
                this.$state.go('patientDetails');
            }, function (error) {
            });
        };
        ;
        EditProcedureController.prototype.cancel = function () {
            this.modalInstance.dismiss('cancel');
        };
        ;
        // this.open = { procDate: false };
        EditProcedureController.prototype.open = function () {
            this.popup.opened = true;
        };
        ;
        EditProcedureController.prototype.updateProcedure = function () {
            var _this = this;
            // this.case.Laterality__c = this.procedureData.laterality;
            this.CaseDetailsFactory.UpdateProcedure(this.procedureData)
                .then(function (data) {
                //console.log(data);
                _this.modalInstance.dismiss();
                _this.$state.go(_this.$state.current, {}, { reload: true });
            }, function (error) {
                console.log(error);
            });
        };
        ;
        return EditProcedureController;
    }());
    EditProcedureController.$inject = ['PatientService', 'CaseDetailsFactory', 'DashboardService', 'CaseService', '$timeout', '$state', 'HospitalService'];
    var EditProcedure = (function () {
        function EditProcedure() {
            this.bindings = {
                modalInstance: '<',
            };
            this.controller = EditProcedureController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/editProcedure/editProcedure.component.html';
        }
        return EditProcedure;
    }());
    angular
        .module('orthosensor')
        .component('osEditProcedure', new EditProcedure());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=editProcedure.component.js.map
(function () {
    'use strict';
    function HospitalSelector($rootScope, $uibModalInstance, UserService, PatientListFactory, PatientService, HospitalService) {
        var $ctrl = this;
        $ctrl.practice = null;
        $ctrl.hospital = null;
        $ctrl.setHospitalFilter = setHospitalFilter;
        $ctrl.cancel = cancel;
        activate();
        function activate() {
            $ctrl.hospitals = HospitalService.hospitals;
            console.log(HospitalService.hospitals);
            $ctrl.isUserAdmin = UserService.isAdmin;
            if (!$ctrl.isUserAdmin) {
                $ctrl.hospital = UserService.accountId;
            }
            console.log($ctrl.isUserAdmin);
            console.log($ctrl.hospital);
            $ctrl.practices = HospitalService.practices;
            setModalTitle();
        }
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
        function setModalTitle() {
            if ($ctrl.isUserAdmin) {
                $ctrl.title = 'Select Hospital / Practice';
            }
            else {
                $ctrl.title = 'Select Practice';
            }
        }
        function setHospitalFilter() {
            console.log($ctrl.hospital);
            console.log($ctrl.practice);
            if ($ctrl.hospital) {
                // $rootScope.filterHospitalId = $ctrl.hospital.Id;
                HospitalService.filterHospitalId = $ctrl.hospital.id;
                HospitalService.currentFilterId = $ctrl.hospital.id;
                // $rootScope.filterPracticeId = undefined;
                HospitalService.filterPracticeId = undefined;
            }
            if ($ctrl.practice) {
                console.log($ctrl.practice);
                console.log('Getting practice info');
                // $rootScope.filterHospitalId = $ctrl.practice;
                HospitalService.filterHospitalId = $ctrl.hospital.id;
                HospitalService.filterPracticeId = $ctrl.practice;
                HospitalService.currentFilterId = $ctrl.practice;
            }
            $uibModalInstance.close('Ok');
            // $state.go($state.current, {}, { reload: true });
        }
    }
    HospitalSelector.$inject = ['$rootScope', '$uibModalInstance', 'UserService', 'PatientListFactory', 'PatientService', 'HospitalService'];
    angular
        .module('orthosensor')
        .controller('HospitalSelector', HospitalSelector);
})();
//# sourceMappingURL=hospitalSelector.controller.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('osHospitalSurgeonSelector', {
        templateUrl: 'app/components/hospitalSurgeonSelector.component.html',
        controller: hospitalSurgeonSelectorController,
        bindings: {
            hospital: '=',
            surgeon: '='
        }
    });
    hospitalSurgeonSelectorController.$inject = ['$rootScope'];
    function hospitalSurgeonSelectorController($rootScope) {
        var $ctrl = this;
        ////////////////
        $ctrl.$onInit = function () { };
        $ctrl.$onChanges = function (changesObj) { };
        $ctrl.$onDestory = function () { };
    }
})();
//# sourceMappingURL=hospitalSurgeonSelector.component.js.map
var orthosensor;
(function (orthosensor) {
    var PatientDetailInfoController = (function () {
        function PatientDetailInfoController(PatientService, $state, $uibModal) {
            this.PatientService = PatientService;
            this.$state = $state;
            this.$uibModal = $uibModal;
            this.$onInit = function () {
                this.patient = this.PatientService.patient;
                console.log(this.patient);
                //let newDate = moment(new Date(x.dateOfBirth)).add(1, 'days');
                this.patientSF = this.PatientService.getSFObject();
                console.log(this.patientSF);
            };
            this.$onChanges = function (changesObj) { };
            this.$onDestroy = function () { };
            this.patientDetailsVisible = false;
        }
        PatientDetailInfoController.prototype.togglePatientDetailsDrawer = function () {
            this.patientDetailsVisible = !this.patientDetailsVisible;
        };
        PatientDetailInfoController.prototype.editPatient = function (patient) {
            // this.PatientService.setPatientFromSFObject(patient);
            this.$state.go('addPatient');
            // this.$uibModal.open({
            //     templateUrl: 'app/components/editPatient/editPatient.component.html',
            //     controller: 'editPatientController',
            //     size: 'large'
            // });
        };
        return PatientDetailInfoController;
    }());
    PatientDetailInfoController.$inject = ['PatientService', '$state', '$uibModal'];
    var PatientDetailInfo = (function () {
        function PatientDetailInfo() {
            this.bindings = {};
            this.controller = PatientDetailInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/components/patientDetailInfo/patientDetailInfo.component.html';
        }
        return PatientDetailInfo;
    }());
    angular
        .module('orthosensor')
        .component('osPatientDetailInfo', new PatientDetailInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientDetailInfo.component.js.map
(function () {
    'use strict';
    // Usage:
    // 
    // Creates:
    // 
    angular
        .module('orthosensor')
        .component('0sSurveyOutcomeData', {
        templateUrl: 'app/component.surveyOutcomeData/surveyOutcomeData.component.html',
        controller: SurveyOutcomeDataController,
        bindings: {
            chartCategories: '<',
            chartData: '<'
        },
    });
    SurveyOutcomeDataController.inject = ['dependency1'];
    function SurveyOutcomeDataController(dependency1) {
        var ctrl = this;
        ctrl.toggleValue = 'chart';
        ctrl.toggleOutcomes = toggleOutcomes;
        ////////////////
        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
        function toggleOutcomes(viewID) {
            if (ctrl.toggleValue === 'data') {
                ctrl.toggleValue = 'chart';
            }
            else {
                ctrl.toggleValue = 'data';
            }
        }
    }
})();
//# sourceMappingURL=surveyOutcomeData.component.js.map
(function () {
    'use strict';
    angular
        .module('orthosensor')
        .component('osSurveyList', {
        templateUrl: 'app/components/takeSurvey/takeSurvey.component.html',
        controller: SurveyListController,
        bindings: {
            event: '<'
        },
    });
    SurveyListController.$inject = ['$q', '$rootScope', 'CaseService', 'SurveyService', 'CaseDetailsFactory', 'PatientService', '$uibModal', 'logger'];
    function SurveyListController($q, $rootScope, CaseService, SurveyService, CaseDetailsFactory, PatientService, $uibModal, logger) {
        var $ctrl = this;
        $ctrl.surveys = [];
        $ctrl.completedSurveys = [];
        $ctrl.event = {};
        $ctrl.patient = {};
        $ctrl.openQRCode = openSurveyLinkQRCode;
        $ctrl.isSurveyComplete = isSurveyComplete;
        $ctrl.escapeURL = escapeURL;
        $ctrl.sendQRCode = sendQRCode;
        ////////////////
        $ctrl.$onInit = function () {
            $ctrl.case = CaseService.getCurrentCase();
            $ctrl.event = CaseDetailsFactory.getEvent();
            $ctrl.surveys = SurveyService.getSurveys();
            $ctrl.patient = PatientService.patient;
            // console.log($ctrl.patient);
            // console.log($ctrl.surveys);
            processSurveys();
            getCompletedSurveys();
        };
        $ctrl.$onChanges = function (changesObj) {
            if (changesObj.event) {
                $ctrl.case = CaseService.getCurrentCase();
                $ctrl.event = CaseDetailsFactory.getEvent();
                getCompletedSurveys();
                $ctrl.surveys = setSurveyList(changesObj.event.currentValue);
            }
        };
        $ctrl.$onDestroy = function () { };
        function getCompletedSurveys() {
            CaseService.getSurveyCompletionDates($ctrl.case.Id)
                .then(function (data) {
                // console.log(data);
                $ctrl.completedSurveys = data;
            });
        }
        function setSurveyAsComplete() {
            console.log($ctrl.event);
            if ($ctrl.completedSurveys) {
                if ($ctrl.completedSurveys.length > 0) {
                    for (var i = 0; i < $ctrl.surveys.length; i++) {
                        // let complete = $ctrl.eventsToComplete[i];
                        console.log($ctrl.completedSurveys);
                        for (var j = 0; j < $ctrl.completedSurveys; j++) {
                            if ($ctrl.surveys[i].Survey_Name__c === $ctrl.completedSurveys[j].Survey__r.Name__c && event.Event_Type_Name__c === $ctrl.completedSurveys[j].Event__r.Event_Type_Name__c) {
                                console.log('Events match: ' + event.Event_Type_Name__c);
                                //console.log(today)
                                // if its before today
                                //if (complete.End_Date__c <= today) {
                                return true;
                                //} else {
                                //    return false;
                                //}
                            }
                        }
                    }
                }
            }
            return false;
        }
        function setSurveyList(event) {
            // make sure we have the right event
            // $ctrl.event = CaseDetailsFactory.getEvent();
            console.log(event.Id);
            SurveyService.retrieveSurveysByEventId(event.Id)
                .then(function (data) {
                $ctrl.surveys = data;
                console.log($ctrl.surveys);
                SurveyService.setSurveys(data);
                processSurveys();
            });
        }
        function escapeURL(urlString) {
            return urlString.replace(/&amp;/g, '&');
        }
        function sendQRCode(target) {
            console.log(target);
            var physician = CaseService.getPhysician();
            console.log(physician);
            // console.log($ctrl.patient.firstName + ' ' + $ctrl.patient.lastName);
            // TO DO: get survey name
            // TO DO: get surgeon address and Practice
            var body = '<div><p>From: Dr. ' + physician.name + '</p><p>Subject: KOOS Knee Survey</p><p>Dear ' + $ctrl.patient.firstName + ' ' + $ctrl.patient.lastName + ', </p><p>Dr.' + physician.name + ' is sending you this KOOS Survey to determine how your knee is affecting your quality of life. </p><p>This information will help us keep track of how you feel about your knee and how well you are able to perform your usual activities.</p><p>Answer every question by selecting the appropriate answer, only one answer for each question. If you are unsure about how to answer a question, please give the best answer you can. </p><p>To start the survey, please click this link: <a href =' + target + '>KOOS Survey</a></p><p>Best Regards,</p><p style="font-weight:600;">Dr. ' + physician.name + '</p><p>Orthosensor Practice</p><address>123 Main Street<br>Dania Beach, FL 33004<br>954-999-9090</address></div>';
            var subject = 'Knee Survey';
            var to = 'OIQDemoPatient@orthosensor.com';
            // let to = 'richard.salit@orthosensor.com';
            var from = 'Dr. Matt Foley';
            var deferred = $q.defer();
            IQ_SurveyRepository.SendEmail(from, to, subject, body, function (result, event) {
                console.log(result);
                $rootScope.$apply(function () {
                    if (event.status) {
                        deferred.resolve(result);
                    }
                    else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                });
                logger.logSuccess('Email sent!', '', '', true);
            }, { buffer: false, escape: true, timeout: 120000 });
            return deferred.promise;
        }
        function openSurveyLinkQRCode(target) {
            console.log(target);
            var modalInstance = $uibModal.open({
                //templateUrl: '/resource/' + Date.now() + '/IQ_html_modal_surveyLinkQRCode',
                templateUrl: 'app/components/surveyLinkQRCode/surveyLinkQRCode.modal.html',
                controller: function ($scope, $rootScope, $state, $uibModalInstance, PatientListFactory) {
                    var vm = this;
                    console.log(target);
                    vm.surveyURL = target;
                    vm.close = close;
                    function close() {
                        // console.log('return from modal');
                        // getCompletedSurveys();
                        // $ctrl.surveys = setSurveyList($ctrl.event);
                        $uibModalInstance.close('OK');
                    }
                },
                controllerAs: 'vm',
                size: 'sm'
            });
            modalInstance.result.then(function (selectedItem) {
                console.log('return from modal');
                // getCompletedSurveys();
                // $ctrl.surveys = setSurveyList($ctrl.event);
            }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
            });
        }
        function processSurveys() {
            if ($ctrl.surveys) {
                if ($ctrl.surveys.length) {
                    for (var i = 0; i < $ctrl.surveys.length; i++) {
                        isSurveyComplete($ctrl.surveys[i]);
                    }
                }
            }
            console.log($ctrl.surveys);
        }
        function isSurveyComplete(survey) {
            var completed = false;
            if ($ctrl.completedSurveys) {
                if ($ctrl.completedSurveys.length > 0) {
                    // console.log('Found completed surveys');
                    // console.log($ctrl.completedSurveys);
                    for (var j = 0; j < $ctrl.completedSurveys.length; j++) {
                        // console.log(survey.Name__c);
                        if ($ctrl.completedSurveys[j].Survey__r) {
                            // console.log($ctrl.completedSurveys[j].Survey__r.Name__c);
                        }
                        // console.log($ctrl.event.Event_Type_Name__c);
                        // console.log($ctrl.completedSurveys[j].Event__r.Event_Type_Name__c);
                        if ($ctrl.completedSurveys[j].Survey__r) {
                            if (survey.Name__c === $ctrl.completedSurveys[j].Survey__r.Name__c && $ctrl.event.Event_Type_Name__c === $ctrl.completedSurveys[j].Event__r.Event_Type_Name__c) {
                                // console.log($ctrl.completedSurveys[j]);
                                // console.log('Events match: ' + $ctrl.event.Event_Type_Name__c)
                                if ($ctrl.completedSurveys[j].End_Date__c) {
                                    survey.EndDate = $ctrl.completedSurveys[j].End_Date__c;
                                    survey.Completed = true;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            survey.Completed = false;
            return completed;
        }
    }
})();
//# sourceMappingURL=SurveyList.component.js.map
var orthosensor;
(function (orthosensor) {
    var CaseInteroperativeDataController = (function () {
        function CaseInteroperativeDataController($window, CaseDetailsFactory, LogFactory, PatientService, SensorService, CaseService, $confirm) {
            this.$window = $window;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.LogFactory = LogFactory;
            this.PatientService = PatientService;
            this.SensorService = SensorService;
            this.CaseService = CaseService;
            this.$confirm = $confirm;
        }
        ////////////////
        CaseInteroperativeDataController.prototype.$onInit = function () {
            this.caseEvent = {};
            this.showImages = false;
            this.sensorDataLines = [];
            this.loading = false;
            this.patient = this.PatientService.getPatient();
            console.log(this.patient);
            //console.log(caseEvent);
            // this.event = this.CaseDetailsFactory.getEvent();
            //console.log(this.caseEvent);
            console.log(this.event);
            if (this.event.Id !== null) {
                this.clinicalData = this.CaseService.clinicalData;
                //if (this.event.Clinical_Data__r) {
                //    this.clinicalData = this.event.Clinical_Data__r[0];
                //}    
                if (this.event.OS_Device_Events__r) {
                    this.showImages = this.event.OS_Device_Events__r.length > 0;
                }
                else {
                    this.showImages = false;
                }
                this.loadSensorDataLines(this.event.Id);
                // console.log(this.sensorDataLines);
            }
            else {
                console.log('Event not yet defined');
            }
        };
        ;
        CaseInteroperativeDataController.prototype.$onChanges = function (changesObj) { };
        ;
        CaseInteroperativeDataController.prototype.$onDestory = function () { };
        ;
        // get list of link stations for the hospital and then call the routines to retrive the data       
        CaseInteroperativeDataController.prototype.getLinkStationData = function () {
            var _this = this;
            var hospitalId = this.patient.hospitalId;
            console.log(hospitalId);
            if (hospitalId !== null) {
                // var l = Ladda.create(document.querySelector('.retrieve-btn'));
                // l.start();
                this.loading = true;
                this.CaseDetailsFactory.LoadLinkStationsIDs(hospitalId)
                    .then(function (data) {
                    console.log(data);
                    _this.linkStationsIDs = data;
                    _this.loadData();
                    _this.loading = false;
                }, function (error) {
                    console.log(error);
                });
            }
        };
        // start retrieving linked images from sensors. CheckSoftwareVersion acutally retrieves data once
        // it determines which version of Verasense is being used. It then retrieves the images
        CaseInteroperativeDataController.prototype.loadData = function () {
            var _this = this;
            if (this.linkStationsIDs.length > 0) {
                for (var i = 0; i < this.linkStationsIDs.length; i++) {
                    var procedureId = this.event.Id;
                    console.log(this.event.Id);
                    var serialNo = this.linkStationsIDs[i].Serial_Number__c;
                    console.log(serialNo);
                    var laterality = this.clinicalData.Laterality__c;
                    console.log(laterality);
                    if (serialNo !== null) {
                        this.CaseDetailsFactory.CheckSoftwareVersion(procedureId, serialNo, laterality)
                            .then(function (data) {
                            console.log(data);
                            _this.getImages(data);
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            else {
                this.$window.alert('No images available');
            }
        };
        // images are retrieved and then added as attachments in Force.com
        CaseInteroperativeDataController.prototype.getImages = function (linkStationsID) {
            var _this = this;
            //console.log(linkStationsID);
            var deviceEvents = this.event.OS_Device_Events__r;
            console.log(deviceEvents);
            if (deviceEvents !== null) {
                if (deviceEvents.length > 0) {
                    for (var i = 0; i < deviceEvents.length; i++) {
                        //console.log(deviceEvents[i]);
                        this.CaseDetailsFactory.GetImageList(this.event.Id, deviceEvents[i].OS_Device__r.Id, linkStationsID)
                            .then(function (data) {
                            //console.log(data);
                            var result = JSON.parse(data.replace(/&quot;/g, '"'));
                            console.log(result);
                            if ((typeof result.data !== "undefined") && (result.status !== 'error')) {
                                var imageURLArray = result.data.images;
                                var imageURLArrayCaptures = [];
                                var recId = _this.event.Id;
                                var ii = 0;
                                for (var k = 0; k < imageURLArray.length; k++) {
                                    //console.log('imageURLArray[i].indexOf(45) = ' + imageURLArray[k].indexOf('45.'));
                                    //console.log('imageURLArray.length = ' + imageURLArray.length);
                                    if (imageURLArray[k].indexOf('2.') > -1 || imageURLArray[k].indexOf('10.') > -1 || imageURLArray[k].indexOf('45.') > -1 || imageURLArray[k].indexOf('90.') > -1) {
                                        imageURLArrayCaptures.push(imageURLArray[k]);
                                    }
                                }
                                for (var j = 0; j < imageURLArrayCaptures.length; j++) {
                                    console.log(imageURLArrayCaptures[j]);
                                    _this.createImageAttachment(recId, imageURLArrayCaptures[j]);
                                    ii++;
                                }
                                console.log(imageURLArrayCaptures.length);
                                console.log(ii);
                                if (ii === imageURLArrayCaptures.length) {
                                    //console.log('about to match');
                                    _this.matchSDwithAttachment(recId);
                                }
                            }
                            else {
                                console.log(event);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
                else {
                    console.log('Nothing to do hide the spinner');
                }
            }
            else {
                console.log('Nothing to do hide the spinner');
            }
            // Ladda.stopAll();
        };
        CaseInteroperativeDataController.prototype.createImageAttachment = function (recordId, imageURL) {
            this.CaseDetailsFactory.SaveImageToRecord(recordId, imageURL)
                .then(function (data) {
                console.log(data);
                //console.log('Image was saved');
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.matchSDwithAttachment = function (recordId) {
            var _this = this;
            console.log(recordId);
            this.CaseDetailsFactory.MatchSensorDataAndAttachment(recordId)
                .then(function (data) {
                console.log(data);
                //console.log('Sensor Data Match Complete');
                _this.loadSensorDataLines(recordId);
            }, function (error) {
                //console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.loadSensorDataLines = function (procedureId) {
            var _this = this;
            console.log('Procedure Id: ' + procedureId);
            this.CaseDetailsFactory.loadSensorDataLines(procedureId)
                .then(function (data) {
                console.log(data);
                _this.sensorDataLines = data;
                if (_this.sensorDataLines.length > 0) {
                    if (_this.sensorDataLines[0].AttachmentID === "") {
                        //if attachment ids are not found
                        //console.log("Getting attachments again!");
                        console.log(_this.sensorDataLines);
                    }
                }
                //console.log(this.sensorDataLines);
                // Ladda.stopAll();
            }, function (error) {
                console.log('No Images Captured.');
                console.log(error);
            });
        };
        CaseInteroperativeDataController.prototype.deleteSensorImage = function (line) {
            this.$confirm({ text: 'Are you sure you want to delete this image?', title: 'Delete Sensor Image' })
                .then(function () {
                var _this = this;
                this.CaseDetailsFactory.DeleteSensorLine(line, this.event.Id)
                    .then(function (data) {
                    console.log(data);
                    _this.loadSensorDataLines(_this.event.Id);
                }, function (error) {
                    console.log(error);
                });
            });
        };
        return CaseInteroperativeDataController;
    }());
    CaseInteroperativeDataController.$inject = ['$window', 'CaseDetailsFactory', 'LogFactory', 'PatientService', 'SensorService', 'CaseService', '$confirm'];
    var CaseInteroperativeData = (function () {
        function CaseInteroperativeData() {
            this.bindings = {
                event: '<',
            };
            this.controller = CaseInteroperativeDataController;
            this.templateUrl = 'app/caseDetails/caseInteroperativeData/caseInteroperativeData.component.html';
        }
        return CaseInteroperativeData;
    }());
    angular
        .module('orthosensor')
        .component('osCaseInteroperativeData', new CaseInteroperativeData());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseInteroperativeData.component.js.map
var orthosensor;
(function (orthosensor) {
    var ProcedureInfoController = (function () {
        function ProcedureInfoController(PatientService, CaseDetailsFactory, CaseService, $timeout, $state, $uibModal) {
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.CaseService = CaseService;
            this.$timeout = $timeout;
            this.$state = $state;
            this.$uibModal = $uibModal;
        }
        ProcedureInfoController.prototype.$onInit = function () {
            //console.log(this.clinicalData);
            this.clinicalData = this.CaseService.clinicalData;
            console.log(this.clinicalData);
            //thisevent: '<',
            this.patientCase = this.CaseService.currentCase;
            this.procedure = this.CaseService.currentProcedure;
        };
        ProcedureInfoController.prototype.completeEvent = function (eventId) {
            var _this = this;
            this.CaseDetailsFactory.CompleteEvent(eventId)
                .then(function (data) {
                //console.log(data);
                _this.$state.go('patientDetails');
            }, function (error) { });
        };
        ;
        ProcedureInfoController.prototype.editProcedure = function (procedure, patientCase) {
            console.log(procedure);
            console.log(patientCase);
            // console.log(clinicalData);`
            this.$uibModal.open({
                component: 'osEditProcedure',
                // controller: function ($scope, $rootScope, $state, $uibModalInstance, CaseDetailsFactory, PatientListFactory, PatientService) {
                //     var patient = PatientService.getPatient();
                //     PatientListFactory.loadSurgeons(patient.practiceId).then(function (data) {
                //         //console.log(data);
                //         $scope.surgeons = data;
                //     }, function (error) {
                //         console.log(error);
                //         });
                //     let clinical = procedure.Clinical_Data__r[0];
                //     $scope.procedureData = {};
                //     $scope.procedureData.Id = procedure.Id;
                //     $scope.procedureData.caseId = patientCase.Id;
                //     $scope.procedureData.physician = procedure.Physician__c;
                //     $scope.procedureData.laterality = patientCase.Laterality__c;
                //     $scope.procedureData.procedureDateString = new Date(procedure.Appointment_Start__c);
                //     $scope.procedureData.patientConsentObtained = clinical.Patient_Consent_Obtained__c;
                //     $scope.procedureData.anesthesiaType = clinical.Anesthesia_Type__c;
                //     console.log($scope.procedureData.procedureDateString);
                //     $scope.cancel = function () {
                //         $uibModalInstance.dismiss('cancel');
                //     };
                //     $scope.open = { procDate: false };
                //     $scope.open = function ($event, whichDate) {
                //         $event.preventDefault();
                //         $event.stopPropagation();
                //         $scope.open[whichDate] = true;
                //     };
                //     $scope.checkinput = function () {
                //         console.log(months);
                //     };
                //     $scope.dateOptions = {
                //         formatYear: 'yy',
                //         startingDay: 1,
                //         showWeeks: false
                //     };
                //     $scope.updateProcedure = function () {
                //         $scope.case.Laterality__c = $scope.procedureData.laterality;
                //         CaseDetailsFactory.UpdateProcedure($scope.procedureData).then(function (data) {
                //             //console.log(data);
                //             $uibModalInstance.dismiss();
                //             $state.go($state.current, {}, { reload: true });
                //         }, function (error) {
                //             console.log(error);
                //         });
                //     };
                // },
                size: 'sm'
            });
        };
        ;
        return ProcedureInfoController;
    }());
    ProcedureInfoController.$inject = ['PatientService', 'CaseDetailsFactory', 'CaseService', '$timeout', '$state', '$uibModal'];
    var ProcedureInfo = (function () {
        function ProcedureInfo() {
            this.bindings = {};
            this.controller = ProcedureInfoController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/caseDetails/procedureInfo/procedureInfo.component.html';
        }
        return ProcedureInfo;
    }());
    angular
        .module('orthosensor')
        .component('osProcedureInfo', new ProcedureInfo());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=procedureInfo.component.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        'use strict';
        var CaseActivityService = (function () {
            function CaseActivityService(DashboardDataService, UserService, PatientListFactory, DashboardService) {
                this.DashboardDataService = DashboardDataService;
                this.UserService = UserService;
                this.PatientListFactory = PatientListFactory;
                this.DashboardService = DashboardService;
                // this.PatientService = PatientListFactory;
            }
            CaseActivityService.$inject = ["DashboardDataService", "UserService", "PatientListFactory", "DashboardService"];
            CaseActivityService.prototype.getActivityByPractice = function (practiceId, startDate, endDate) {
                var _this = this;
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                console.log(practiceId, startDate, endDate);
                this.PatientListFactory.getCaseActivityByPractice(practiceId, startDate, endDate)
                    .then(function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.DashboardService.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                    }
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            return CaseActivityService;
        }());
        CaseActivityService.inject = ['DashboardDataService', 'UserService', 'PatientService', 'DashboardService'];
        services.CaseActivityService = CaseActivityService;
        angular
            .module('orthosensor.services')
            .service('CaseActivityService', CaseActivityService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseActivity.service.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var CaseActivityChartController = (function (_super) {
            __extends(CaseActivityChartController, _super);
            // public isHospital(): boolean;
            // public $onInit(): void;
            ///// /* @ngInject */
            function CaseActivityChartController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, CaseActivityService, moment, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                _this.HospitalService = HospitalService;
                _this.CaseActivityService = CaseActivityService;
                _this.$rootScope = $rootScope;
                _this.PatientService = PatientListFactory;
                _this.title = 'Case Activity Chart';
                _this.hospitalPracticeLabel = 'Practices';
                // this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.monthsToChart = '3';
                _this.chartHeight = DashboardService.chartHeight;
                return _this;
            }
            CaseActivityChartController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "HospitalService", "CaseActivityService", "moment", "config", "$rootScope"];
            CaseActivityChartController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                console.log(this.user);
                // handle surgeon or practice staff
                console.log(this.surgeons);
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            CaseActivityChartController.prototype.initChartParams = function () {
                console.log('Initializing chart');
                // this.initChartLookups();
                this.caseActivityChart = new orthosensor.domains.ChartType();
                // let data: orthosensor.domains.ChartDataElement[];
                var service = this.ChartService;
                this.caseActivityChart = service.chart;
                // this.caseActivityChart.options.title = 'Case Activity Chart';
                this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Case Activity Chart';
                this.ChartService.xAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Procedure Count';
                if (!this.isDashboard) {
                    // this.ChartService.chartHeight = 480;
                }
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            CaseActivityChartController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('hospital Account');
                        this.setSurgeonPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            CaseActivityChartController.prototype.setSurgeonPartner = function () {
                console.log(this.surgeons);
                console.log(this.practice.id);
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getData();
                            break;
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getData();
                    }
                }
            };
            CaseActivityChartController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            CaseActivityChartController.prototype.getPracticeData = function () {
                console.log(this.practice);
                console.log(this.surgeons);
                if (this.practice) {
                    this.surgeon = this.surgeons[0];
                }
                return [];
            };
            CaseActivityChartController.prototype.getData = function () {
                var data = [];
                var averageData = this.getAverageData();
                console.log(averageData);
                var averageChartData;
                averageChartData = {
                    "key": ["Average"],
                    "color": "#b1b9bd",
                    "values": averageData
                };
                data.push(averageChartData);
                if (this.user.userProfile !== 1 /* HospitalAdmin */ && this.user.userProfile !== 3 /* PracticeAdmin */) {
                    var surgeonChartData = void 0;
                    var surgeonData = this.getSurgeonData();
                    console.log(surgeonData);
                    surgeonChartData = {
                        "key": [this.surgeon.name],
                        "color": "#109bd6",
                        "values": surgeonData
                    };
                    data.push(surgeonChartData);
                }
                console.log(data);
                this.data = data;
                return data;
            };
            CaseActivityChartController.prototype.getAverages = function () {
                // console.log(this.practice);
                return this.getPracticeData();
            };
            CaseActivityChartController.prototype.getChart = function () {
                this.setDateParams();
                this.getData();
            };
            CaseActivityChartController.prototype.getSurgeonData = function () {
                var _this = this;
                if (this.surgeon === null) {
                    if (this.surgeons.length > 0) {
                        this.surgeon = this.surgeons[0];
                    }
                }
                console.log(this.surgeon);
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                this.PatientService.getCaseActivityBySurgeon(this.surgeon.id, this.startDate, this.endDate)
                    .then(function (data) {
                    // console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var dateLabel = _this.formatDate(data[i]);
                        item = { label: dateLabel, value: data[i].Total };
                        items.push(item);
                        chartData.push([dateLabel, data[i].Total]);
                    }
                    console.log(items);
                    // this.caseActivityChart.data = chartData;
                    // console.log(this.caseActivityChart);
                    return items;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return items;
            };
            CaseActivityChartController.prototype.getAverageData = function () {
                console.log(this.practice);
                var items = [];
                if (this.UserService.user.userProfile === 1 /* HospitalAdmin */) {
                    items = null;
                }
                else {
                    items = this.CaseActivityService.getActivityByPractice(this.practice.id, this.startDate, this.endDate);
                }
                return items;
            };
            return CaseActivityChartController;
        }(dashboard.DashboardChartBaseController));
        CaseActivityChartController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'CaseActivityService', 'moment', 'config', '$rootScope'];
        var CaseActivityChart = (function () {
            function CaseActivityChart() {
                this.bindings = {};
                this.controller = CaseActivityChartController;
                this.templateUrl = 'app/dashboard/caseActivityChart/caseActivityChart.component.html';
            }
            return CaseActivityChart;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osCaseActivityChart', new CaseActivityChart());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=caseActivityChart.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var MeanChangeInPromScoreController = (function (_super) {
            __extends(MeanChangeInPromScoreController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function MeanChangeInPromScoreController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, DashboardDataService, PromDeltaService, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                _this.DashboardDataService = DashboardDataService;
                _this.$rootScope = $rootScope;
                // this.PatientService = PatientListFactory;
                _this.title = 'PROM score deltas';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                // this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                _this.promDeltaService = PromDeltaService;
                return _this;
                // this.$onInit();
            }
            MeanChangeInPromScoreController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "HospitalService", "moment", "config", "DashboardDataService", "PromDeltaService", "$rootScope"];
            MeanChangeInPromScoreController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.surveys = this.DashboardService.surveys;
                this.promDeltas = [];
                this.setDateParams();
                this.initChartParams();
                // console.log(this.surveys);
                // for now - only KOOS!
                for (var i = 0; i <= this.surveys.length; i++) {
                    // console.log(i);
                    if (this.surveys[i]) {
                        if (this.surveys[i].name === 'KOOS') {
                            this.prom = this.surveys[i];
                            break;
                        }
                    }
                }
                this.initData();
            };
            MeanChangeInPromScoreController.prototype.getProms = function () {
                return [];
            };
            MeanChangeInPromScoreController.prototype.initChartParams = function () {
                this.initChartLookups();
                this.promDeltaChart = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.promDeltaChart = service.chart;
                // this.options = this.ChartService.getLineChartOptions();
                this.ChartService.chartTitle = 'Prom Delta';
                this.ChartService.yAxisLabel = 'Score';
                this.ChartService.xAxisLabel = 'Events';
                this.ChartService.xAxisLabel = 'PROM Scores Events';
                this.options = this.ChartService.getLineChartOptions();
                this.setSelectSizes();
                // these values are being hard coded for KOOS and current event types
                this.eventTypes = this.promDeltaService.eventTypes;
                this.responseTypes = ['KOOS Symptoms', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS Pain', 'KOOS QOL'];
                this.lineColors = ['#ff7f0e', 'green', 'blue', '#787878', 'red'];
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and returning?)
            MeanChangeInPromScoreController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            MeanChangeInPromScoreController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i <= this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            MeanChangeInPromScoreController.prototype.setSelectSizes = function () {
                this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
                // console.log(this.surgeonSelectClass);
                this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.hospitalSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.promSelectClass = 'col-md-2 col-sm-3 col-xs-4';
                this.dateRangeSelectClass = 'col-md-1 col-sm-2 col-xs-3';
            };
            MeanChangeInPromScoreController.prototype.getSurgeonChart = function () {
                this.getSurgeonData();
            };
            MeanChangeInPromScoreController.prototype.createChartObject = function (key, color) {
                var getChartValuesForType = this.getChartValues(key);
                var chartObj = {
                    'key': [key],
                    'color': color,
                    'values': this.getChartValues(key)
                };
                // console.log(chartObj);
                return chartObj;
            };
            MeanChangeInPromScoreController.prototype.getChartValues = function (key) {
                var values = [];
                // console.log(this.promDeltas);
                if (this.promDeltas.length > 0) {
                    for (var i = 0; i <= this.promDeltas.length; i++) {
                        // console.log(this.promDeltas[i]);
                        for (var j = 0; j <= this.eventTypes.length; j++) {
                            if (this.promDeltas[i] !== undefined) {
                                if (this.promDeltas[i].stage === this.eventTypes[j]) {
                                    if (this.promDeltas[i].section === key) {
                                        var point = { x: j, y: this.promDeltas[i].score };
                                        // console.log(point);
                                        values.push(point);
                                        // console.log(values);
                                    }
                                }
                            }
                            else {
                                // console.log(this.promDeltas[i]);
                            }
                        }
                    }
                    return values;
                }
                // console.log(values);
                return values;
            };
            MeanChangeInPromScoreController.prototype.getChartData = function () {
                // console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                for (var i = 0; i <= this.responseTypes.length; i++) {
                    // console.log('Response Type' + i);
                    this.chartData.push(this.createChartObject(this.responseTypes[i], this.lineColors[i]));
                }
                console.log(this.chartData);
                return this.chartData;
            };
            MeanChangeInPromScoreController.prototype.getPracticeData = function () {
                var _this = this;
                var practice = this.practice.id;
                // loop through months
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.DashboardDataService.getAggregatePromDeltasByPractice(this.practice.id, startMonth, 0, 0, 0)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        _this.getChartData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            MeanChangeInPromScoreController.prototype.getSurgeonData = function () {
                var _this = this;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.surgeon);
                if (this.surgeon !== null) {
                    // let surgeon = this.surgeon.id;
                    this.DashboardDataService.getAggregatePromDeltasBySurgeon(this.surgeon.id, 0, 0, 0, 0)
                        .then(function (data) {
                        var result = data;
                        // console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                            // console.log(this.promDeltas);
                            _this.getChartData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            MeanChangeInPromScoreController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            MeanChangeInPromScoreController.prototype.formatDate = function (data) {
                // console.log(data);
                var dateLabel;
                dateLabel = moment(data).add(1, 'd').toDate();
                var month = moment(dateLabel).month() + 1;
                var formattedDate = moment(dateLabel).year() + '-' + month + '-02';
                // console.log(formattedDate);
                return formattedDate;
            };
            return MeanChangeInPromScoreController;
        }(dashboard.DashboardChartBaseController));
        MeanChangeInPromScoreController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService', '$rootScope'];
        angular
            .module('orthosensor.dashboard')
            .component('osMeanChangeInPromScore', {
            controller: MeanChangeInPromScoreController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/meanChangeInProm/meanChangeInPromScore.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=meanChangeInPromScore.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var MeanChangeInPromScoreMiniController = (function (_super) {
            __extends(MeanChangeInPromScoreMiniController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function MeanChangeInPromScoreMiniController(DashboardService, ChartService, PatientListFactory, UserService, moment, config, DashboardDataService, PromDeltaService) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, moment, config) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                // this.PatientService = PatientListFactory;
                _this.title = 'PROM score deltas';
                _this.DashboardDataService = DashboardDataService;
                _this.hospitals = [];
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                _this.user = UserService.user;
                _this.promDeltaService = PromDeltaService;
                return _this;
                // this.$onInit();
            }
            MeanChangeInPromScoreMiniController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "moment", "config", "DashboardDataService", "PromDeltaService"];
            MeanChangeInPromScoreMiniController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // console.log(this.surgeons);
                this.surveys = this.DashboardService.surveys;
                this.promDeltas = [];
                this.setDateParams();
                this.initChartParams();
                // console.log(this.surveys);
                // for now - only KOOS!
                for (var i = 0; i <= this.surveys.length; i++) {
                    // console.log(i);
                    if (this.surveys[i]) {
                        if (this.surveys[i].name === 'KOOS') {
                            this.prom = this.surveys[i];
                            break;
                        }
                    }
                }
                this.initData();
            };
            MeanChangeInPromScoreMiniController.prototype.getProms = function () {
                return [];
            };
            MeanChangeInPromScoreMiniController.prototype.initChartParams = function () {
                this.initChartLookups();
                this.promDeltaChart = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.promDeltaChart = service.chart;
                this.options = this.ChartService.getLineChartOptions();
                this.options.showLegend = false;
                this.ChartService.chartTitle = 'Prom Delta';
                this.ChartService.yAxisLabel = 'Score';
                this.ChartService.xAxisLabel = 'Events';
                this.setSelectSizes();
                // these values are being hard coded for KOOS and current event types
                this.eventTypes = this.promDeltaService.eventTypes;
                this.responseTypes = ['KOOS Symptoms', 'KOOS ADL', 'KOOS Sport/Rec', 'KOOS Pain', 'KOOS QOL'];
                this.lineColors = ['#ff7f0e', 'green', 'blue', '#787878', 'red'];
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and returning?)
            MeanChangeInPromScoreMiniController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            MeanChangeInPromScoreMiniController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i <= this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            MeanChangeInPromScoreMiniController.prototype.setSelectSizes = function () {
                this.surgeonSelectClass = this.getSurgeonPartnerSurgeonSelectClass();
                // console.log(this.surgeonSelectClass);
                this.practiceSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.hospitalSelectClass = this.getHospitalPartnerPracticeSelectClass();
                this.promSelectClass = 'col-md-2 col-sm-3 col-xs-4';
                this.dateRangeSelectClass = 'col-md-1 col-sm-2 col-xs-3';
            };
            MeanChangeInPromScoreMiniController.prototype.getSurgeonChart = function () {
                this.getSurgeonData();
            };
            MeanChangeInPromScoreMiniController.prototype.createChartObject = function (key, color) {
                var getChartValuesForType = this.getChartValues(key);
                var chartObj = {
                    'key': [key],
                    'color': color,
                    'values': this.getChartValues(key)
                };
                // console.log(chartObj);
                return chartObj;
            };
            MeanChangeInPromScoreMiniController.prototype.getChartValues = function (key) {
                var values = [];
                // console.log(this.promDeltas);
                if (this.promDeltas.length > 0) {
                    for (var i = 0; i <= this.promDeltas.length; i++) {
                        // console.log(this.promDeltas[i]);
                        for (var j = 0; j <= this.eventTypes.length; j++) {
                            if (this.promDeltas[i] !== undefined) {
                                if (this.promDeltas[i].stage === this.eventTypes[j]) {
                                    if (this.promDeltas[i].section === key) {
                                        var point = { x: j, y: this.promDeltas[i].score };
                                        // console.log(point);
                                        values.push(point);
                                        // console.log(values);
                                    }
                                }
                            }
                            else {
                                // console.log(this.promDeltas[i]);
                            }
                        }
                    }
                    return values;
                }
                // console.log(values);
                return values;
            };
            MeanChangeInPromScoreMiniController.prototype.getChartData = function () {
                // console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                for (var i = 0; i <= this.responseTypes.length; i++) {
                    // console.log('Response Type' + i);
                    this.chartData.push(this.createChartObject(this.responseTypes[i], this.lineColors[i]));
                }
                console.log(this.chartData);
                return this.chartData;
            };
            MeanChangeInPromScoreMiniController.prototype.getPracticeData = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.DashboardDataService.getAggregatePromDeltasByPractice(this.practice.id, startMonth, 0, 0, 0)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                        // console.log(this.promDeltas);
                        _this.getChartData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            MeanChangeInPromScoreMiniController.prototype.getSurgeonData = function () {
                var _this = this;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.surgeon);
                if (this.surgeon !== null) {
                    // let surgeon = this.surgeon.id;
                    this.DashboardDataService.getAggregatePromDeltasBySurgeon(this.surgeon.id, 0, 0, 0, 0)
                        .then(function (data) {
                        var result = data;
                        // console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.promDeltaService.convertDataToObjects(result);
                            // console.log(this.promDeltas);
                            _this.getChartData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            MeanChangeInPromScoreMiniController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            MeanChangeInPromScoreMiniController.prototype.validate = function () {
                return true;
            };
            return MeanChangeInPromScoreMiniController;
        }(dashboard.DashboardChartBaseController));
        MeanChangeInPromScoreMiniController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService'];
        angular
            .module('orthosensor.dashboard')
            .component('osMeanChangeInPromScoreMini', {
            controller: MeanChangeInPromScoreMiniController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/meanChangeInProm/meanChangeInPromScoreMini.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=meanChangeInPromScoreMini.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientPhaseCountController = (function (_super) {
            __extends(PatientPhaseCountController, _super);
            ///// /* @ngInject */
            function PatientPhaseCountController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, $filter, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.$filter = $filter;
                _this.$rootScope = $rootScope;
                _this.PatientService = PatientListFactory;
                return _this;
            }
            PatientPhaseCountController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "HospitalService", "moment", "$filter", "config", "$rootScope"];
            PatientPhaseCountController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.title = 'Patient Phase Count';
                this.phases = this.getPhases();
                this.hospitalPracticeLabel = 'Practices';
                this.surgeonLabel = 'Surgeon';
                this.monthsToChart = "6";
                this.chartHeight = this.DashboardService.chartHeight;
                console.log(this.surgeons);
                this.setDateParams();
                console.log(this.chartType);
                this.initChartParams();
            };
            ;
            PatientPhaseCountController.prototype.getPhases = function () {
                return [
                    new Phase('PreOp', 'Pre Op', '"#b1b9bd"'),
                    new Phase('PostOp', 'Post Op', '#109bd6'),
                    new Phase('LongTerm', 'Long Term', '#cf0a2c'),
                ];
            };
            PatientPhaseCountController.prototype.setDateParams = function () {
                var today = moment();
                var monthsInPast;
                var monthsInFuture;
                if ((Number(this.monthsToChart) % 2) !== 0) {
                    monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                    monthsInFuture = Math.ceil((Number(this.monthsToChart) / 2));
                }
                else {
                    monthsInPast = Math.floor((Number(this.monthsToChart) / 2));
                    monthsInFuture = Math.floor((Number(this.monthsToChart) / 2));
                }
                // console.log(today);
                var endDate = moment(today).add(monthsInFuture, 'months');
                endDate = moment(endDate).endOf('month');
                // console.log(endDate);
                var sEndDate = moment(endDate).format(this.dateFormat);
                // console.log(sEndDate);
                var startMonth = moment(today).subtract((monthsInPast - 1), 'months');
                var startDate = moment(startMonth).startOf('month');
                this.startDate = moment(startDate).format(this.dateFormat);
                this.endDate = moment(endDate).format(this.dateFormat);
                // console.log(this.startDate);
            };
            PatientPhaseCountController.prototype.isHospital = function () {
                return true;
            };
            PatientPhaseCountController.prototype.initChartParams = function () {
                this.patientPhaseCount = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.patientPhaseCount = service.chart;
                // if (this.chartType === 'line') {
                //     this.options = this.ChartService.getLineChartOptions();
                // } else {
                this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Patient Phase Chart';
                this.ChartService.xAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Procedures';
                // console.log(this.options);
                this.initData();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientPhaseCountController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('practice Account');
                        this.setSurgeonPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('practice Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('practice Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientPhaseCountController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            PatientPhaseCountController.prototype.setHospitalPartner = function () {
                console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            PatientPhaseCountController.prototype.setAdmin = function () {
                console.log(this.hospitals);
                // console.log(this.practices);
                // console.log(this.surgeons);
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getSurgeonData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            // called from page
            PatientPhaseCountController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PatientPhaseCountController.prototype.getSurgeonData = function () {
                this.setDateParams();
                return this.getSurgeonPhaseCount(this.surgeon.id, this.startDate, this.endDate);
            };
            PatientPhaseCountController.prototype.getPracticeData = function () {
                this.setDateParams();
                return this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
            };
            PatientPhaseCountController.prototype.setChartData = function (allData) {
                var data = [];
                this.allData = allData;
                // console.log(this.allData);
                var preOpData = this.getPreOpData();
                // console.log(preOpData);
                var preOpChartData = {
                    "key": ["Pre Op"],
                    "color": "#b1b9bd",
                    "values": preOpData
                };
                data.push(preOpChartData);
                var postOpData = this.getPostOpData();
                // console.log(postOpData);
                var postOpChartData = {
                    "key": ["Post Op"],
                    "color": "#109bd6",
                    "values": postOpData
                };
                data.push(postOpChartData);
                var longTermData = this.getLongTermData();
                // console.log(longTermData);
                var longTermChartData = {
                    "key": ['Long Term'],
                    "color": "#cf0a2c",
                    "values": longTermData
                };
                data.push(longTermChartData);
                // console.log(data);
                this.data = data;
                return data;
            };
            PatientPhaseCountController.prototype.validate = function () {
                return true;
            };
            PatientPhaseCountController.prototype.getPreOpData = function () {
                var data = this.getPhaseData('PreOp');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getPostOpData = function () {
                var data = this.getPhaseData('PostOp');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getLongTermData = function () {
                var data = this.getPhaseData('Long Term');
                console.log(data);
                return this.convertPhaseData(data);
            };
            PatientPhaseCountController.prototype.getPhaseData = function (phaseName) {
                var phases = [];
                console.log(this.allData);
                for (var i = 0; i < this.allData.length; i++) {
                    if (this.allData[i].phase === phaseName) {
                        // console.log(this.allData[i]);
                        var phase = this.allData[i];
                        phases.push(phase);
                    }
                }
                console.log(phases);
                return phases;
            };
            PatientPhaseCountController.prototype.convertPhaseData = function (filteredData) {
                this.setDateParams();
                var items = [];
                var item;
                item = new orthosensor.domains.ChartDataElement();
                // items.push(item);
                var chartData = [];
                console.log(filteredData);
                if (filteredData) {
                    for (var i = 0; i < filteredData.length; i++) {
                        var dateLabel = this.formatDate(filteredData[i].startDate);
                        item = { label: dateLabel, value: filteredData[i].count };
                        items.push(item);
                        chartData.push([dateLabel, filteredData[i].count]);
                    }
                }
                // console.log(items);
                return items;
            };
            PatientPhaseCountController.prototype.getSurgeonPhaseCount = function (surgeonId, startDate, endDate) {
                var _this = this;
                var surgeon = this.surgeon.id;
                this.PatientService.getSurgeonPhaseCount(surgeon, this.startDate, this.endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log(results);
                    var phaseCount;
                    phaseCount = _this.convertSFPhaseCountsToObject(results);
                    console.log(phaseCount);
                    if (phaseCount.length > 0) {
                        phaseCount = _this.createBlankMonths(phaseCount);
                        //phaseCount = this.$filter('orderBy')(phaseCount, 'startDate');
                        var sortedPhases = void 0;
                        sortedPhases = phaseCount.slice(0);
                        sortedPhases = phaseCount.sort(function (n1, n2) {
                            if (n1.startDate > n2.startDate) {
                                return 1;
                            }
                            if (n1.startDate < n2.startDate) {
                                return -1;
                            }
                            return 0;
                        });
                        // console.log(phaseCount);
                        var chartData = _this.setChartData(phaseCount);
                        return chartData;
                    }
                    else {
                        return [];
                    }
                    // return results;
                    //this.surgeons = surgeonData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            PatientPhaseCountController.prototype.aggregatePracticeData = function (data) {
                var temp = [];
                // data = this.getPracticePhaseCount(this.practice.id, this.startDate, this.endDate);
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var found = false;
                    for (var j = 0; j < temp.length; j++) {
                        if (data[i].startDate === temp[j].startDate) {
                            found = true;
                            temp[j].count = data[i].count + temp[i].count;
                        }
                    }
                    if (!found) {
                        temp.push(data[i]);
                    }
                }
                return temp;
            };
            PatientPhaseCountController.prototype.getPracticePhaseCount = function (practiceId, startDate, endDate) {
                var _this = this;
                var practice = this.practice.id;
                this.PatientService.getPracticePhaseCount(practice, this.startDate, this.endDate)
                    .then(function (data) {
                    var results = data;
                    // console.log('Practice Phase count');
                    // console.log(results);
                    var phaseCount;
                    phaseCount = _this.convertSFPhaseCountsToObject(results);
                    // console.log(phaseCount);
                    phaseCount = _this.aggregatePracticeData(phaseCount);
                    // console.log(phaseCount);
                    var chartData = _this.setChartData(phaseCount);
                    return chartData;
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return [];
            };
            // convert to object - dates come in last day of the prior month
            PatientPhaseCountController.prototype.convertSFPhaseCountsToObject = function (data) {
                var phaseCounts = [];
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var phaseCount = new orthosensor.domains.PhaseCount();
                    phaseCount.id = data[i].Id;
                    phaseCount.phase = data[i].Name;
                    phaseCount.startDate = moment(data[i].Month_Date__c).add(1, 'd');
                    phaseCount.startDateString = moment(phaseCount.startDate).format(this.dateFormat);
                    phaseCount.surgeonId = data[i].Surgeon_ID__c;
                    phaseCount.practiceId = data[i].Parent_ID__c;
                    phaseCount.count = data[i].Count__c;
                    phaseCounts.push(phaseCount);
                }
                // }    
                // console.log(phaseCounts);
                return phaseCounts;
            };
            // assumes we are doing one surgeon at a time        
            PatientPhaseCountController.prototype.createBlankMonths = function (phaseCounts) {
                var phaseCounts2 = phaseCounts;
                for (var i = 0; i < Number(this.monthsToChart); i++) {
                    var found = false;
                    var dateToCompare = moment(this.startDate).add(i, 'months');
                    // console.log(dateToCompare);
                    console.log(this.phases);
                    for (var j = 0; j < this.phases.length; j++) {
                        for (var k = 0; k < phaseCounts.length; k++) {
                            // console.log(phaseCounts[i]);
                            if (moment(dateToCompare).format(this.dateFormat) === phaseCounts[k].startDateString && this.phases[j].name === phaseCounts[k].phase) {
                                found = true;
                                // console.log(found = true);
                            }
                        }
                        if (!found) {
                            var phaseCount = this.createPhaseCountRecord(phaseCounts[0].id, this.phases[j].name, moment(dateToCompare).toDate(), moment(dateToCompare).format(this.dateFormat), phaseCounts[0].surgeonId, phaseCounts[0].practiceId, 0);
                            // console.log(phaseCount);
                            phaseCounts2.push(phaseCount);
                            // console.log(phaseCounts2);
                        }
                    }
                }
                return phaseCounts2;
            };
            PatientPhaseCountController.prototype.createPhaseCountRecord = function (id, name, toDate, sDate, surgeonId, practiceId, count) {
                var phaseCount = new orthosensor.domains.PhaseCount();
                phaseCount.id = id;
                phaseCount.phase = name;
                phaseCount.startDate = toDate;
                phaseCount.startDateString = sDate;
                phaseCount.surgeonId = surgeonId;
                phaseCount.practiceId = practiceId;
                phaseCount.count = count;
                // console.log(phaseCount);
                return phaseCount;
            };
            return PatientPhaseCountController;
        }(dashboard.DashboardChartBaseController));
        PatientPhaseCountController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', '$filter', 'config', '$rootScope'];
        var PatientPhaseCountChart = (function () {
            function PatientPhaseCountChart() {
                this.bindings = {
                    chartType: '@'
                };
                this.controller = PatientPhaseCountController;
                this.templateUrl = 'app/dashboard/patientPhaseCount/patientPhaseCount.component.html';
            }
            return PatientPhaseCountChart;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientPhaseCount', new PatientPhaseCountChart());
        var Phase = (function () {
            function Phase(name, displayName, color) {
                this.name = name;
                this.displayName = displayName;
                this.color = color;
            }
            return Phase;
        }());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientPhaseCount.component.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        var KneeBalanceService = (function () {
            function KneeBalanceService(DashboardDataService) {
                // this._patient = new orthosensor.domains.Patient();
                this.DashboardDataService = DashboardDataService;
            }
            KneeBalanceService.$inject = ["DashboardDataService"];
            Object.defineProperty(KneeBalanceService.prototype, "practiceId", {
                set: function (_practiceId) {
                    this._practiceId = _practiceId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "kneePatients", {
                get: function () {
                    console.log(this._kneePatients);
                    return this._kneePatients;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "startMonth", {
                set: function (_startMonth) {
                    this._startMonth = _startMonth;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "startYear", {
                set: function (_startYear) {
                    this._startYear = _startYear;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "endMonth", {
                set: function (_endMonth) {
                    this._endMonth = _endMonth;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(KneeBalanceService.prototype, "endYear", {
                set: function (_endYear) {
                    this._endYear = _endYear;
                },
                enumerable: true,
                configurable: true
            });
            KneeBalanceService.prototype.setKneeBalanceByPatient = function () {
                var _this = this;
                return this.DashboardDataService.GetPracticeSensorData(this._practiceId, this._startMonth, this._startYear, this._endMonth, this._endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    _this.setToPatientKneeObjects(result);
                }, function (error) {
                    console.log(error);
                });
            };
            KneeBalanceService.prototype.getSFObject = function () {
                var sfPatient = new orthosensor.domains.sfPatient();
                // sfPatient.Id = this._patient.id;
                // sfPatient.Anonymous__c = this._patient.anonymous;
                // sfPatient.Anonymous_Label__c = this._patient.anonymousLabel;
                // sfPatient.Hl7__c = this._patient.hl7;
                // sfPatient.HospitalId__c = this._patient.hospitalId;
                // sfPatient.hospital = this._patient.hospital;
                // sfPatient.Last_Name__c = this._patient.lastName;
                // sfPatient.First_Name__c = this._patient.firstName;
                // sfPatient.practice = this._patient.practice;
                // // sfPatient.Anonymous_Label__c = this._patient.label;
                // sfPatient.Medical_Record_Number__c = this.patient.medicalRecordNumber;
                // sfPatient.Email__c = this._patient.email;
                // sfPatient.Date_Of_Birth__c = this._patient.dateOfBirth;
                // sfPatient.Anonymous_Year_Of_Birth__c = this._patient.birthYear;
                // sfPatient.Gender__c = this._patient.gender;
                // sfPatient.Social_Security_Number__c = this._patient.socialSecurityNumber;
                // sfPatient.Source_Record_Id__c = this._patient.patientNumber;
                // sfPatient.Account_Number__c = this._patient.accountNumber;
                // sfPatient.Race__c = this._patient.race;
                // sfPatient.Language__c = this._patient.language;
                return sfPatient;
            };
            KneeBalanceService.prototype.setToPatientKneeObjects = function (result) {
                this._kneePatients = [];
                var patientId = '';
                console.log(result);
                for (var i = 0; i < result.length; i++) {
                    patientId = result[i].PatientID__c;
                    var found = false;
                    for (var j = 0; j < this._kneePatients.length; j++) {
                        if (patientId === this._kneePatients[j].patientId) {
                            found = true;
                            this.mapFields(j, result[i]);
                        }
                    }
                    if (!found) {
                        console.log('new patient');
                        this.mapFields(-1, result[i]);
                    }
                }
                console.log(this._kneePatients);
                return this._kneePatients;
            };
            KneeBalanceService.prototype.mapFields = function (i, result) {
                var record;
                if (i === -1) {
                    record = new orthosensor.domains.KneeBalanceLoads();
                }
                else {
                    record = this._kneePatients[i];
                }
                console.log(record);
                if (i === -1) {
                    record.id = result.Id;
                    record.patientId = result.PatientID__c;
                    record.patientName = result.PatientName__c;
                    record.practiceId = result.Practice_ID__c;
                    record.surgeonId = result.Surgeon_ID__c;
                    record.surgeonName = result.Surgeon_Name__c;
                    record.procedureDate = result.ProcedureDate__c;
                }
                switch (result.ActualFlex__c) {
                    case 10:
                        record.medial10 = result.MedialLoad__c;
                        record.lateral10 = result.LateralLoad__c;
                        break;
                    case 45:
                        record.medial45 = result.MedialLoad__c;
                        record.lateral45 = result.LateralLoad__c;
                        break;
                    case 90:
                        record.medial90 = result.MedialLoad__c;
                        record.lateral90 = result.LateralLoad__c;
                        break;
                }
                if (i === -1) {
                    this._kneePatients.push(record);
                }
                console.log(record);
                console.log(this._kneePatients);
            };
            return KneeBalanceService;
        }());
        KneeBalanceService.inject = ['DashboardDataService'];
        services.KneeBalanceService = KneeBalanceService;
        angular
            .module('orthosensor.services')
            .service('KneeBalanceService', KneeBalanceService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=kneeBalance.service.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var KneeBalanceDataController = (function (_super) {
            __extends(KneeBalanceDataController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function KneeBalanceDataController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'Medial & Lateral Load Values (10' + _this.ascii(176) + ', 45' + _this.ascii(176) + ', 90' + _this.ascii(176) + ')';
                _this.hospitals = [];
                _this.hospitalPracticeLabel = 'Practices';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                return _this;
            }
            KneeBalanceDataController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "HospitalService", "moment", "config", "$rootScope"];
            KneeBalanceDataController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                this.kneeBalanceData = [];
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            KneeBalanceDataController.prototype.initChartParams = function () {
                this.initChartLookups();
                this.kneeBalanceDataChart = new orthosensor.domains.ChartType();
                var service = this.ChartService;
                this.kneeBalanceDataChart = service.chart;
                // this.options = this.ChartService.getMultiBarChartOptions();
                this.ChartService.chartTitle = 'Knee Balance Data';
                this.ChartService.yAxisLabel = 'Months';
                this.ChartService.xAxisLabel = 'Medial & Lateral Load Values';
                this.options = this.ChartService.getMultiBarChartOptions();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            KneeBalanceDataController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            KneeBalanceDataController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonKneeBalanceAvg();
                            break;
                        }
                    }
                }
            };
            KneeBalanceDataController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeKneeBalanceAvg();
                    }
                }
            };
            KneeBalanceDataController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getSurgeonKneeBalanceAvg(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            KneeBalanceDataController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        // console.log('trying to change the chart');    
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            KneeBalanceDataController.prototype.getSurgeonData = function () {
                return this.getSurgeonKneeBalanceAvg();
            };
            KneeBalanceDataController.prototype.getPracticeData = function () {
                return this.getPracticeKneeBalanceAvg();
            };
            KneeBalanceDataController.prototype.createChartObject = function (key, color) {
                var getChartValuesForType = this.getChartValues(key);
                var chartObj = {
                    'key': [key],
                    'color': color,
                    'values': getChartValuesForType
                };
                // console.log(chartObj);
                return chartObj;
            };
            KneeBalanceDataController.prototype.getChartValues = function (key) {
                var values = [];
                // console.log(this.kneeBalanceData);
                if (this.kneeBalanceData.length > 0) {
                    for (var i = 0; i <= this.kneeBalanceData.length; i++) {
                        // console.log(this.kneeBalanceData[i]);
                        if (this.kneeBalanceData[i]) {
                            switch (key) {
                                case "10-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral10
                                    });
                                    break;
                                case "10-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial10
                                    });
                                    break;
                                case "45-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral45
                                    });
                                    break;
                                case "45-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial45
                                    });
                                    break;
                                case "90-L":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].lateral90
                                    });
                                    break;
                                case "90-M":
                                    values.push({
                                        "label": this.getMonthLabel(this.kneeBalanceData[i].startDate),
                                        "value": this.kneeBalanceData[i].medial90
                                    });
                                    break;
                            }
                        }
                    }
                }
                // console.log(values);
                return values;
            };
            KneeBalanceDataController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            KneeBalanceDataController.prototype.getChartData = function () {
                // console.log(this.kneeBalanceData);
                this.chartData = [];
                // create chart objects for each laterality _ ange to arrays in fure 
                var lateral10 = this.createChartObject("10-L", "#787878");
                this.chartData.push(lateral10);
                var medial10 = this.createChartObject("10-M", "#b1b9bd");
                this.chartData.push(medial10);
                var lateral45 = this.createChartObject("45-L", "#0000FF");
                this.chartData.push(lateral45);
                var medial45 = this.createChartObject("45-M", "#109bd6");
                this.chartData.push(medial45);
                var lateral90 = this.createChartObject("90-L", "#5F9EA0");
                this.chartData.push(lateral90);
                var medial90 = this.createChartObject("90-M", "#82e49e");
                this.chartData.push(medial90);
                this.data = this.chartData;
                return this.chartData;
            };
            KneeBalanceDataController.prototype.getPracticeKneeBalanceAvg = function () {
                var _this = this;
                var practice = this.practice.id;
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.practice);
                this.PatientService.getMonthlyPracticeSensorAverages(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    // console.log(result);
                    if (result !== null) {
                        _this.kneeBalanceData = _this.convertDataToObjects(result);
                        // console.log(this.kneeBalanceData);
                        _this.getChartData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.kneeBalanceData;
            };
            KneeBalanceDataController.prototype.getSurgeonKneeBalanceAvg = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                // console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                // console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    console.log(this.surgeon);
                    var surgeon = this.surgeon.id;
                    this.PatientService.getMonthlySurgeonSensorAverages(surgeon, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.kneeBalanceData = _this.convertDataToObjects(result);
                            // console.log(this.kneeBalanceData);
                            _this.getChartData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.kneeBalanceData;
            };
            KneeBalanceDataController.prototype.initChartLookups = function () {
                // get hospitals, surgeon 
                // get hospitals
            };
            KneeBalanceDataController.prototype.validate = function () {
                return true;
            };
            KneeBalanceDataController.prototype.convertDataToObjects = function (data) {
                var kneeBalance = [];
                // console.log(data);
                for (var i = 0; i <= data.length; i++) {
                    // make sure a record exists!
                    if (data[i] !== undefined && data[i].Month) {
                        var knee = this.convertSFKneeBalanceToObject(data[i]);
                        kneeBalance.push(knee);
                    }
                }
                return kneeBalance;
            };
            // convert to object - dates come in last day of the prior month
            KneeBalanceDataController.prototype.convertSFKneeBalanceToObject = function (data) {
                var kneeBalance = new orthosensor.domains.KneeBalanceAvg();
                // console.log(data);
                kneeBalance.startDate = moment(data.Year + '-' + data.Month + '-' + '02').toDate();
                // kneeBalance.startDateString = moment(kneeBalance.startDate).format(this.dateFormat);
                if (data.SurgeonId) {
                    kneeBalance.surgeonId = data.SurgeonId;
                }
                if (data.PracticeId) {
                    kneeBalance.practiceId = data.PracticeId;
                }
                kneeBalance.medial10 = data.Medial10;
                kneeBalance.medial45 = data.Medial45;
                kneeBalance.medial90 = data.Medial90;
                kneeBalance.lateral10 = data.Lateral10;
                kneeBalance.lateral45 = data.Lateral45;
                kneeBalance.lateral90 = data.Lateral90;
                // console.log(kneeBalance);
                return kneeBalance;
            };
            return KneeBalanceDataController;
        }(dashboard.DashboardChartBaseController));
        KneeBalanceDataController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        angular
            .module('orthosensor.dashboard')
            .component('osKneeBalanceData', {
            controller: KneeBalanceDataController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/kneeBalanceData/kneeBalanceData.component.html',
            bindings: {
                chartType: '@',
                chartType2: '@'
            }
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
var KneeBalanceData = (function () {
    function KneeBalanceData(name, displayName, color) {
        this.name = name;
        this.displayName = displayName;
        this.color = color;
    }
    return KneeBalanceData;
}());
//# sourceMappingURL=kneeBalanceData.component.js.map
var orthosensor;
(function (orthosensor) {
    var KneeBalancePatientCardController = (function () {
        function KneeBalancePatientCardController(PatientService, $timeout, $state) {
            this.PatientService = PatientService;
            this.$timeout = $timeout;
            this.$state = $state;
        }
        KneeBalancePatientCardController.prototype.$onInit = function () {
            this.tenDegree = '10' + this.ascii(176);
            this.fortyFiveDegree = '45' + this.ascii(176);
            this.ninetyDegree = '90' + this.ascii(176);
        };
        KneeBalancePatientCardController.prototype.ascii = function (a) { return String.fromCharCode(a); };
        KneeBalancePatientCardController.prototype.patientDetails = function (patient) {
            var _this = this;
            console.log(patient);
            this.PatientService.setPatientId(patient.patientId);
            this.$timeout(function () {
                _this.$state.go('patientDetails');
            }, 1000);
        };
        return KneeBalancePatientCardController;
    }());
    KneeBalancePatientCardController.$inject = ['PatientService', '$timeout', '$state'];
    var KneeBalancePatientCard = (function () {
        function KneeBalancePatientCard() {
            this.bindings = {
                patient: '<'
            };
            this.controller = KneeBalancePatientCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/kneeBalanceData/kneeBalancePatientCard.component.html';
        }
        return KneeBalancePatientCard;
    }());
    angular
        .module('orthosensor')
        .component('osKneeBalancePatientCard', new KneeBalancePatientCard());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=kneeBalancePatientCard.component.js.map
var orthosensor;
(function (orthosensor) {
    var KneeBalancePatientCardsController = (function () {
        function KneeBalancePatientCardsController(DashboardDataService, KneeBalanceService, UserService) {
            this.DashboardDataService = DashboardDataService;
            this.KneeBalanceService = KneeBalanceService;
            this.UserService = UserService;
        }
        KneeBalancePatientCardsController.prototype.$onInit = function () {
            var _this = this;
            this.patients = [];
            var practiceId = this.UserService.user.accountId;
            this.KneeBalanceService.practiceId = practiceId;
            this.KneeBalanceService.startMonth = 1;
            this.KneeBalanceService.startYear = 2017;
            this.KneeBalanceService.endMonth = 4;
            this.KneeBalanceService.endYear = 2017;
            this.KneeBalanceService.setKneeBalanceByPatient()
                .then(function () {
                _this.patients = _this.KneeBalanceService.kneePatients;
                console.log(_this.patients);
            });
            // this.patient = {
            //     name: 'Mildred Ginger',
            //     id: '123456',
            //     procedureDate: new Date(2017, 2, 24),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient.name = 'Mark Tweedy';
            // this.patient.kneeBalanceData.medial10 = 18;
            // this.patient.kneeBalanceData.medial45 = 16;
            // this.patient.kneeBalanceData.medial90 = 26;
            // this.patient.kneeBalanceData.lateral10 = 12;
            // this.patient.kneeBalanceData.lateral45 = 14;
            // this.patient.kneeBalanceData.lateral90 = 19;
            // this.patients.push(this.patient);
            // this.patient1 = {
            //     name: 'Jane Hart',
            //     id: '12345236',
            //     procedureDate: new Date(2017, 2, 4),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient1.kneeBalanceData.medial10 = 20;
            // this.patient1.kneeBalanceData.medial45 = 16;
            // this.patient1.kneeBalanceData.medial90 = 26;
            // this.patient1.kneeBalanceData.lateral10 = 12;
            // this.patient1.kneeBalanceData.lateral45 = 14;
            // this.patient1.kneeBalanceData.lateral90 = 19;
            // this.patients.push(this.patient1);
            // this.patient3 = {
            //     name: 'Martin Half',
            //     id: '12345343',
            //     procedureDate: new Date(2017, 3, 4),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient3.kneeBalanceData.medial10 = 18;
            // this.patient3.kneeBalanceData.medial45 = 18;
            // this.patient3.kneeBalanceData.medial90 = 22;
            // this.patient3.kneeBalanceData.lateral10 = 12;
            // this.patient3.kneeBalanceData.lateral45 = 16;
            // this.patient3.kneeBalanceData.lateral90 = 16;
            // this.patients.push(this.patient3);
            // this.patient4 = {
            //     name: 'Robert Richard',
            //     id: '123987',
            //     procedureDate: new Date(2017, 2, 13),
            //     kneeBalanceData: new orthosensor.domains.KneeBalanceLoads()
            // }; 
            // this.patient4.kneeBalanceData.medial10 = 4;
            // this.patient4.kneeBalanceData.medial45 = 12;
            // this.patient4.kneeBalanceData.medial90 = 20;
            // this.patient4.kneeBalanceData.lateral10 = 15;
            // this.patient4.kneeBalanceData.lateral45 = 16;
            // this.patient4.kneeBalanceData.lateral90 = 10;
            // this.patients.push(this.patient4);
        };
        return KneeBalancePatientCardsController;
    }());
    KneeBalancePatientCardsController.$inject = ['DashboardDataService', 'KneeBalanceService', 'UserService'];
    var KneeBalancePatientCards = (function () {
        function KneeBalancePatientCards() {
            this.bindings = {};
            this.controller = KneeBalancePatientCardsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/kneeBalanceData/kneeBalancePatientCards.component.html';
        }
        return KneeBalancePatientCards;
    }());
    angular
        .module('orthosensor')
        .component('osKneeBalancePatientCards', new KneeBalancePatientCards());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=kneeBalancePatientCards.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsUnderBundleController = (function (_super) {
            __extends(PatientsUnderBundleController, _super);
            function PatientsUnderBundleController(DashboardService, DashboardDataService, PatientsUnderBundleService, PatientService, UserService, HospitalService, $state, $timeout, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.PatientService = PatientService;
                _this.UserService = UserService;
                _this.$timeout = $timeout;
                _this.DashboardDataService = DashboardDataService;
                _this.PatientsUnderBundleService = PatientsUnderBundleService;
                _this.$state = $state;
                _this.title = 'Patients Under Bundle';
                return _this;
            }
            PatientsUnderBundleController.prototype.$onInit = function () {
                var _this = this;
                _super.prototype.$onInit.call(this);
                var practiceId = this.UserService.user.accountId;
                // console.log(practiceId);
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                if (!this.isUserAdmin) {
                    this.DashboardDataService.getPatientsUnderBundle(practiceId)
                        .then(function (data) {
                        _this.patients = data;
                        // console.log(this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            PatientsUnderBundleController.prototype.patientDetails = function (patient) {
                var _this = this;
                //console.log(patient);
                this.PatientService.setPatientId(patient.patientId);
                this.$timeout(function () {
                    _this.$state.go('patientDetails');
                }, 1000);
            };
            return PatientsUnderBundleController;
        }(dashboard.DashboardBaseController));
        PatientsUnderBundleController.$inject = ['DashboardService', 'DashboardDataService', 'PatientsUnderBundleService', 'PatientService', 'UserService', 'HospitalService', '$state', '$timeout', '$rootScope'];
        var PatientsUnderBundle = (function () {
            function PatientsUnderBundle() {
                this.bindings = {
                    practiceId: '@',
                };
                this.controller = PatientsUnderBundleController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/patientsUnderBundle/patientsUnderBundle.component.html';
            }
            return PatientsUnderBundle;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientsUnderBundle', new PatientsUnderBundle());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsUnderBundle.component.js.map
var orthosensor;
(function (orthosensor) {
    var services;
    (function (services) {
        'use strict';
        var PatientsUnderBundleService = (function () {
            function PatientsUnderBundleService(DashboardDataService, UserService) {
                this.DashboardDataService = DashboardDataService;
            }
            PatientsUnderBundleService.$inject = ["DashboardDataService", "UserService"];
            PatientsUnderBundleService.prototype.getPatientsUnderBundle = function (practiceId) {
                this.DashboardDataService.getPatientsUnderBundle(practiceId)
                    .then(function (data) {
                    // console.log(data);
                    return data;
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return PatientsUnderBundleService;
        }());
        PatientsUnderBundleService.inject = ['DashboardDataService', 'UserService'];
        services.PatientsUnderBundleService = PatientsUnderBundleService;
        angular
            .module('orthosensor.services')
            .service('PatientsUnderBundleService', PatientsUnderBundleService);
    })(services = orthosensor.services || (orthosensor.services = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsUnderBundle.service.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsOutOfComplianceController = (function (_super) {
            __extends(PatientsOutOfComplianceController, _super);
            ///// /* @ngInject */
            function PatientsOutOfComplianceController(DashboardService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.HospitalService = HospitalService;
                _this.PatientService = PatientListFactory;
                _this.title = 'Patients out of compliance';
                _this.hospitalPracticeLabel = 'Practices';
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                return _this;
            }
            PatientsOutOfComplianceController.$inject = ["DashboardService", "PatientListFactory", "UserService", "HospitalService", "moment", "config", "$rootScope"];
            PatientsOutOfComplianceController.prototype.$onInit = function () {
                // super.$onInit();
                this.surgeons = this.DashboardService.surgeons;
                // console.log(this.surgeons);
                this.patientsOutOfCompliance = [];
                var patient1 = new orthosensor.domains.PatientsOutOfCompliance();
                patient1.patientName = 'Fredericka Winfield';
                patient1.phaseName = 'Procedure';
                patient1.issue = 'Incomplete';
                patient1.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient1);
                var patient2 = new orthosensor.domains.PatientsOutOfCompliance();
                patient2.patientName = 'Jane Hart';
                patient2.phaseName = '4 - 8 week';
                patient2.issue = 'Survey Missing';
                patient2.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient2);
                var patient3 = new orthosensor.domains.PatientsOutOfCompliance();
                patient3.patientName = 'James Johnson';
                patient3.phaseName = 'Pre-Op';
                patient3.issue = 'Survey Missing';
                patient3.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient3);
                var patient4 = new orthosensor.domains.PatientsOutOfCompliance();
                patient4.patientName = 'Midred Ginger';
                patient4.phaseName = 'Pre-Op';
                patient4.issue = 'Survey Missing';
                patient4.lastContactDate = new Date();
                this.patientsOutOfCompliance.push(patient4);
                // console.log(this.patientsOutOfCompliance);
            };
            ;
            PatientsOutOfComplianceController.prototype.isHospital = function () {
                return true;
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientsOutOfComplianceController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    case 3 /* PracticeAdmin */:
                        // console.log('practice Account');
                        this.setHospitalPartner();
                        break;
                    // case 'Customer Community User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    // case 'Customer Community Plus User':
                    //     // console.log('surgeon Account');
                    //     this.setSurgeonPartner();
                    //     break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientsOutOfComplianceController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonData();
                            break;
                        }
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticeData();
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.setAdmin = function () {
                // console.log(this.hospitals);
                // console.log(this.practices);
                // console.log(this.surgeons);
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                if (this.surgeons !== undefined) {
                                    for (var i = 0; i < this.surgeons.length; i++) {
                                        if (this.surgeons[i].parentId === this.practice.id) {
                                            this.surgeon = this.surgeons[i];
                                            this.data = this.getSurgeonData();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            PatientsOutOfComplianceController.prototype.getSurgeonData = function () {
                return this.getSurgeonCompliance(this.surgeon.id);
            };
            PatientsOutOfComplianceController.prototype.getPracticeData = function () {
                return this.getPracticeCompliance(this.practice.id);
            };
            PatientsOutOfComplianceController.prototype.validate = function () {
                return true;
            };
            PatientsOutOfComplianceController.prototype.getSurgeonCompliance = function (surgeonId) {
                var surgeon = this.surgeon.id;
                return [];
            };
            PatientsOutOfComplianceController.prototype.getPracticeCompliance = function (practiceId) {
                var practice = this.practice.id;
                return [];
            };
            // convert to object - dates come in last day of the prior month
            PatientsOutOfComplianceController.prototype.convertSFComplianceToObject = function (data) {
                return [];
            };
            PatientsOutOfComplianceController.prototype.createPatientComplianceRecord = function (id, name, toDate, sDate, surgeonId, practiceId, count) {
                var patient = new orthosensor.domains.PatientsOutOfCompliance();
                // phaseCount.id = id;
                // phaseCount.phase = name;
                // phaseCount.startDate = toDate;
                // phaseCount.startDateString = sDate;
                // phaseCount.surgeonId = surgeonId;
                // phaseCount.practiceId = practiceId;
                // phaseCount.count = count;
                // console.log(phaseCount);
                return patient;
            };
            return PatientsOutOfComplianceController;
        }(dashboard.DashboardBaseController));
        PatientsOutOfComplianceController.$inject = ['DashboardService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', '$rootScope'];
        var PatientsOutOfCompliance = (function () {
            function PatientsOutOfCompliance() {
                this.bindings = {
                    chartType: '@'
                };
                this.controller = PatientsOutOfComplianceController;
                this.templateUrl = 'app/dashboard/patientsOutOfCompliance/patientsOutOfCompliance.component.html';
            }
            return PatientsOutOfCompliance;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osPatientsOutOfCompliance', new PatientsOutOfCompliance());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsOutOfCompliance.component.js.map
var orthosensor;
(function (orthosensor) {
    var PatientKoosCardController = (function () {
        function PatientKoosCardController() {
        }
        PatientKoosCardController.prototype.$onInit = function () { };
        return PatientKoosCardController;
    }());
    var PatientKoosCard = (function () {
        function PatientKoosCard() {
            this.bindings = {};
            this.controller = PatientKoosCardController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/patientKoosCard.component.html';
        }
        return PatientKoosCard;
    }());
    angular
        .module('orthosensor')
        .component('osPatientKoosCard', new PatientKoosCard());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientKoosCard.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PatientsKoosScoreListController = (function (_super) {
            __extends(PatientsKoosScoreListController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function PatientsKoosScoreListController(DashboardService, PatientListFactory, UserService, moment, config, DashboardDataService, PromDeltaService, HospitalService, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.UserService = UserService;
                _this.DashboardDataService = DashboardDataService;
                _this.PromDeltaService = PromDeltaService;
                _this.HospitalService = HospitalService;
                _this.$rootScope = $rootScope;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'Patient KOOS scores';
                _this.hospitals = [];
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                // this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                // this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                _this.$onInit();
                return _this;
            }
            PatientsKoosScoreListController.$inject = ["DashboardService", "PatientListFactory", "UserService", "moment", "config", "DashboardDataService", "PromDeltaService", "HospitalService", "$rootScope"];
            PatientsKoosScoreListController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // this.surgeons = this.DashboardService.surgeons;
                // console.log(this.surgeons);
                // this.proms = this.getProms();
                // this.chartData = [];
                this.surveys = this.DashboardService.surveys;
                this.patientsScores = [
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Richard Robert',
                        patientId: '7348',
                        phase: 'Pre-op',
                        procedureDate: new Date(2016, 12, 2),
                        symptom: 15,
                        pain: 14,
                        aDL: 10,
                        sportRec: 16,
                        qOL: 15
                    },
                    {
                        id: '8901',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: 'Pre-op',
                        procedureDate: new Date(2017, 1, 15),
                        symptom: 12,
                        pain: 13,
                        aDL: 11,
                        sportRec: 17,
                        qOL: 18
                    },
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: '4-8 week Follow-up',
                        procedureDate: new Date(2017, 3, 4),
                        symptom: 18,
                        pain: 16,
                        aDL: 14,
                        sportRec: 20,
                        qOL: 20
                    },
                    {
                        id: '8900',
                        surgeonId: '4562',
                        practiceId: '9877',
                        surgeonName: 'Samantha Jones',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Mildred Ginger',
                        patientId: '9871',
                        phase: '6 month Follow-up',
                        procedureDate: new Date(2017, 6, 4),
                        symptom: 25,
                        pain: 22,
                        aDL: 24,
                        sportRec: 22,
                        qOL: 21
                    },
                    {
                        id: '8903',
                        surgeonId: '4563',
                        practiceId: '9878',
                        surgeonName: 'Marcus Welby',
                        practiceName: 'Orthosensor Orthopedic',
                        patientName: 'Marc Abt',
                        patientId: '9876',
                        phase: 'Pre-Op',
                        procedureDate: new Date(2017, 3, 1),
                        symptom: 11,
                        pain: 12,
                        aDL: 10,
                        sportRec: 8,
                        qOL: 10
                    },
                ];
                //this.setDateParams();
                this.initData();
            };
            ;
            PatientsKoosScoreListController.prototype.initChartParams = function () {
                // this.initChartLookups();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PatientsKoosScoreListController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PatientsKoosScoreListController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonPromDeltas();
                            break;
                        }
                    }
                }
            };
            PatientsKoosScoreListController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticePromDeltas();
                    }
                }
            };
            PatientsKoosScoreListController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticePromDeltas(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            PatientsKoosScoreListController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        console.log('trying to change the chart');
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PatientsKoosScoreListController.prototype.getSurgeonData = function () {
                return this.getSurgeonPromDeltas();
            };
            PatientsKoosScoreListController.prototype.getPracticeData = function () {
                return this.getPracticePromDeltas();
            };
            PatientsKoosScoreListController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            PatientsKoosScoreListController.prototype.getData = function () {
                console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                this.data = this.chartData;
                return this.chartData;
            };
            PatientsKoosScoreListController.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                // console.log('Getting knee balance data');
                // loop through months
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                        console.log(_this.promDeltas);
                        //this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            PatientsKoosScoreListController.prototype.getSurgeonPromDeltas = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    var surgeon = this.surgeon.id;
                    this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                            console.log(_this.promDeltas);
                            _this.getData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            return PatientsKoosScoreListController;
        }(dashboard.DashboardBaseController));
        PatientsKoosScoreListController.$inject = ['DashboardService', 'PatientListFactory', 'UserService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService', 'HospitalService', '$rootScope'];
        var PatientsKoosScoreList = (function () {
            function PatientsKoosScoreList() {
                this.bindings = {};
                this.controller = PatientsKoosScoreListController;
                this.controllerAs = '$ctrl';
                this.templateUrl = 'app/dashboard/promDeltaList/patientsKoosScoresList.component.html';
            }
            return PatientsKoosScoreList;
        }());
        angular
            .module('orthosensor')
            .component('osPatientsKoosScoreList', new PatientsKoosScoreList());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=patientsKoosScoresList.component.js.map
var orthosensor;
(function (orthosensor) {
    var PromByPatientCardsController = (function () {
        function PromByPatientCardsController() {
        }
        PromByPatientCardsController.prototype.$onInit = function () { };
        return PromByPatientCardsController;
    }());
    var PromByPatientCards = (function () {
        function PromByPatientCards() {
            this.bindings = {};
            this.controller = PromByPatientCardsController;
            this.controllerAs = '$ctrl';
            this.templateUrl = 'app/dashboard/promDeltaList/promByPatientCards.component.html';
        }
        return PromByPatientCards;
    }());
    angular
        .module('orthosensor')
        .component('osPromByPatientCards', new PromByPatientCards());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=promByPatientCards.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var PromDeltaListController = (function (_super) {
            __extends(PromDeltaListController, _super);
            ///// /* @ngInject */
            // tslint:disable-next-line:max-line-length
            function PromDeltaListController(DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, DashboardDataService, PromDeltaService, $rootScope) {
                var _this = _super.call(this, DashboardService, ChartService, PatientListFactory, UserService, HospitalService, moment, config, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.ChartService = ChartService;
                _this.UserService = UserService;
                // private PatientService: any;
                _this.dateFormat = 'MM/DD/YYYY';
                // this.PatientService = PatientListFactory;
                _this.title = 'PROM score deltas';
                _this.DashboardDataService = DashboardDataService;
                _this.PromDeltaService = PromDeltaService;
                _this.practices = [];
                _this.practice = new orthosensor.domains.Practice();
                _this.surgeonLabel = 'Surgeon';
                _this.surgeons = [];
                _this.chartHeight = DashboardService.chartHeight;
                // this.user = UserService.user;
                // console.log(this.user);
                // console.log(config);
                _this.$onInit();
                return _this;
            }
            PromDeltaListController.$inject = ["DashboardService", "ChartService", "PatientListFactory", "UserService", "HospitalService", "moment", "config", "DashboardDataService", "PromDeltaService", "$rootScope"];
            PromDeltaListController.prototype.$onInit = function () {
                _super.prototype.$onInit.call(this);
                // this.surgeons = this.DashboardService.surgeons;
                console.log(this.surgeons);
                // this.proms = this.getProms();
                // this.chartData = [];
                this.surveys = this.DashboardService.surveys;
                this.promDeltas = [];
                this.setDateParams();
                this.initChartParams();
                this.initData();
            };
            ;
            PromDeltaListController.prototype.initChartParams = function () {
                // this.initChartLookups();
            };
            // initializes data to first practice and surgeon (do we want to save after leaving and retuning?)
            PromDeltaListController.prototype.initData = function () {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        // console.log('surgeon Account');
                        this.setSurgeonPartner();
                        break;
                    case 1 /* HospitalAdmin */:
                        // console.log('hospital Account');
                        this.setHospitalPartner();
                        break;
                    default:
                        // console.log('Admin Account');
                        this.setAdmin();
                }
            };
            PromDeltaListController.prototype.setSurgeonPartner = function () {
                if (this.surgeons !== undefined) {
                    for (var i = 0; i < this.surgeons.length; i++) {
                        if (this.surgeons[i].parentId === this.practice.id) {
                            this.surgeon = this.surgeons[i];
                            this.data = this.getSurgeonPromDeltas();
                            break;
                        }
                    }
                }
            };
            PromDeltaListController.prototype.setHospitalPartner = function () {
                // console.log(this.practices);
                if (this.practices !== undefined) {
                    if (this.practices.length > 0) {
                        this.practice = this.practices[0];
                        this.data = this.getPracticePromDeltas();
                    }
                }
            };
            PromDeltaListController.prototype.setAdmin = function () {
                if (this.hospitals !== undefined) {
                    if (this.hospitals.length > 0) {
                        this.hospital = this.hospitals[0];
                        if (this.practices !== undefined) {
                            if (this.practices.length > 0) {
                                this.practice = this.practices[0];
                                // this.data = this.getPracticeKneeBalanceAvg();
                                this.getPracticePromDeltas(); // for testing!!
                            }
                        }
                    }
                }
            };
            // called from page
            PromDeltaListController.prototype.getChart = function (surgeon) {
                switch (this.user.userProfile) {
                    case 2 /* Surgeon */:
                        return this.getSurgeonData();
                    case 1 /* HospitalAdmin */:
                        console.log('trying to change the chart');
                        return this.getPracticeData();
                    default:
                        return this.getSurgeonData();
                }
            };
            PromDeltaListController.prototype.getSurgeonData = function () {
                return this.getSurgeonPromDeltas();
            };
            PromDeltaListController.prototype.getPracticeData = function () {
                return this.getPracticePromDeltas();
            };
            PromDeltaListController.prototype.getMonthLabel = function (date) {
                var dateLabel = this.formatDate(date);
                return dateLabel;
            };
            PromDeltaListController.prototype.getData = function () {
                console.log(this.promDeltas);
                this.chartData = [];
                // let chartData: any[] = [];
                this.data = this.chartData;
                return this.chartData;
            };
            PromDeltaListController.prototype.getPracticePromDeltas = function () {
                var _this = this;
                var practice = this.practice.id;
                // console.log('Getting knee balance data');
                // loop through months
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.practice);
                this.DashboardDataService.getPromDeltasByPractice(this.practice.id, startMonth, startYear, endMonth, endYear)
                    .then(function (data) {
                    var result = data;
                    console.log(result);
                    if (result !== null) {
                        _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                        console.log(_this.promDeltas);
                        //this.getData();
                    }
                }, function (error) {
                    console.log(error);
                    return error;
                });
                return this.promDeltas;
            };
            PromDeltaListController.prototype.getSurgeonPromDeltas = function () {
                var _this = this;
                // let surgeon: string = this.surgeon.id;
                console.log(this.monthDiff);
                var startMonth = moment(this.startDate).month() + 1;
                var startYear = moment(this.startDate).year();
                //let endDate: Date = moment(this.startDate).add(this.monthDiff, 'months');
                var endMonth = moment(this.endDate).month() + 1;
                var endYear = moment(this.endDate).year();
                console.log(this.surgeon);
                if (this.surgeon !== undefined) {
                    var surgeon = this.surgeon.id;
                    this.DashboardDataService.getPromDeltasBySurgeon(this.surgeon.id, startMonth, startYear, endMonth, endYear)
                        .then(function (data) {
                        var result = data;
                        console.log(result);
                        if (result !== null) {
                            _this.promDeltas = _this.PromDeltaService.convertDataToObjects(result);
                            console.log(_this.promDeltas);
                            _this.getData();
                        }
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
                return this.promDeltas;
            };
            return PromDeltaListController;
        }(dashboard.DashboardChartBaseController));
        PromDeltaListController.$inject = ['DashboardService', 'ChartService', 'PatientListFactory', 'UserService', 'HospitalService', 'moment', 'config', 'DashboardDataService', 'PromDeltaService', 'rootScope'];
        angular
            .module('orthosensor.dashboard')
            .component('osPromDeltaList', {
            controller: PromDeltaListController,
            controllerAs: '$ctrl',
            templateUrl: 'app/dashboard/promDeltaList/promDeltaList.component.html',
            bindings: {}
        });
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
var PromDeltaList = (function () {
    function PromDeltaList(name, displayName, color) {
        this.name = name;
        this.displayName = displayName;
        this.color = color;
    }
    return PromDeltaList;
}());
//# sourceMappingURL=promDeltaList.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var orthosensor;
(function (orthosensor) {
    var dashboard;
    (function (dashboard) {
        var ScheduledPatientsController = (function (_super) {
            __extends(ScheduledPatientsController, _super);
            function ScheduledPatientsController(DashboardService, DashboardDataService, PatientListFactory, UserService, PatientService, $state, $timeout, HospitalService, $rootScope) {
                var _this = _super.call(this, UserService, HospitalService, $rootScope) || this;
                _this.DashboardService = DashboardService;
                _this.DashboardDataService = DashboardDataService;
                _this.PatientListFactory = PatientListFactory;
                _this.UserService = UserService;
                _this.PatientService = PatientService;
                _this.$state = $state;
                _this.$timeout = $timeout;
                _this.HospitalService = HospitalService;
                _this.title = 'Scheduled Procedures';
                return _this;
            }
            ScheduledPatientsController.prototype.$onInit = function () {
                var _this = this;
                _super.prototype.$onInit.call(this);
                var practiceId = this.UserService.user.accountId;
                //this.patients = this.PatientsUnderBundleService.getPatientsUnderBundle(practiceId);
                // let daysInFuture: number = 60;
                this.period = '30';
                if (!this.isUserAdmin) {
                    this.DashboardDataService.getScheduledPatients(practiceId, Number(this.period))
                        .then(function (data) {
                        _this.patients = data;
                        // console.log(this.patients);
                    }, function (error) {
                        console.log(error);
                        return error;
                    });
                }
            };
            ScheduledPatientsController.prototype.patientDetails = function (patient) {
                var _this = this;
                // console.log(patient);
                this.PatientService.setPatientId(patient.patientId);
                this.$timeout(function () {
                    _this.$state.go('patientDetails');
                }, 1000);
            };
            ScheduledPatientsController.prototype.changePeriod = function () {
                var _this = this;
                var practiceId = this.UserService.user.accountId;
                // console.log(this.period);
                var daysInFuture = Number(this.period);
                this.DashboardDataService.getScheduledPatients(practiceId, daysInFuture)
                    .then(function (data) {
                    _this.patients = data;
                    // console.log(this.patients);
                }, function (error) {
                    console.log(error);
                    return error;
                });
            };
            return ScheduledPatientsController;
        }(dashboard.DashboardBaseController));
        ScheduledPatientsController.$inject = ['DashboardService', 'DashboardDataService', 'PatientListFactory', 'UserService', 'PatientService', '$state', '$timeout', 'HospitalService', '$rootScope'];
        var ScheduledPatients = (function () {
            function ScheduledPatients() {
                this.bindings = {};
                this.controller = ScheduledPatientsController;
                this.templateUrl = 'app/dashboard/scheduledPatients/scheduledPatients.component.html';
            }
            return ScheduledPatients;
        }());
        angular
            .module('orthosensor.dashboard')
            .component('osScheduledPatients', new ScheduledPatients());
    })(dashboard = orthosensor.dashboard || (orthosensor.dashboard = {}));
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=scheduledPatients.component.js.map
var orthosensor;
(function (orthosensor) {
    var HospitalPracticeController = (function () {
        function HospitalPracticeController(PatientListFactory, PatientService, CaseDetailsFactory, UserService, HospitalService, $uibModal) {
            this.PatientListFactory = PatientListFactory;
            this.PatientService = PatientService;
            this.CaseDetailsFactory = CaseDetailsFactory;
            this.UserService = UserService;
            this.HospitalService = HospitalService;
            this.$uibModal = $uibModal;
        }
        HospitalPracticeController.prototype.openHospitalFilter = function () {
            var _this = this;
            var options = {
                templateUrl: 'app/components/hospitalSelector/hospitalSelector.component.html',
                controller: 'HospitalSelector',
                controllerAs: '$ctrl',
                size: 'small'
            };
            this.$uibModal.open(options).result
                .then(function () {
                // getPatients();
                // loadSurgeons();
                _this.currentHospitalId = _this.PatientListFactory.getFilterHospitalId();
                _this.currentPracticeId = _this.PatientListFactory.getFilterPracticeId();
                _this.currentHospitalName = _this.HospitalService.getHospitalName(_this.currentHospitalId);
                _this.currentPracticeName = _this.HospitalService.getPracticeName(_this.currentPracticeId);
                console.log(_this.PatientService.currentFilterId);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
        HospitalPracticeController.prototype.getPracticeName = function (id) {
            console.log('Searching for practice: ' + id);
            for (var i = 0; i < this.HospitalService.practices.length; i++) {
                console.log(this.HospitalService.practices[i].Id);
                if (this.HospitalService.practices[i].Id === id) {
                    console.log(this.HospitalService.practices[i].Id + ', ' + id);
                    return this.HospitalService.practices[i].Name;
                }
            }
            return '';
        };
        HospitalPracticeController.prototype.getHospitalName = function (id) {
            console.log('Searching for hospital: ' + id);
            for (var i = 0; i < this.HospitalService.hospitals.length; i++) {
                if (this.HospitalService.hospitals[i].Id === id) {
                    return this.HospitalService.hospitals[i].Name;
                }
            }
            return '';
        };
        return HospitalPracticeController;
    }());
    HospitalPracticeController.$inject = ['PatientListFactory', 'PatientService',
        'UserService',
        'HospitalService', '$uibModal'];
    var HospitalPractice = (function () {
        function HospitalPractice() {
            this.bindings = {
                hospital: '=',
                practice: '='
            };
            this.controller = HospitalPracticeController;
            this.templateUrl = 'app/patientList/hospitalPractice/hospitalPractice.component.html';
        }
        return HospitalPractice;
    }());
    angular
        .module('orthosensor')
        .component('osHospitalPractice', new HospitalPractice());
})(orthosensor || (orthosensor = {}));
//# sourceMappingURL=hospitalPractice.component.js.map
angular.module('orthosensor').run(['$templateCache', function($templateCache) {$templateCache.put('app/patientTest.component.html','<div class="container-fluid content test"><div class=row><div class=col-sm-2><h4>Patient Info</h4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Info</div></div><div class=panel-body><div class="form-group form-group-sm"><label for>DOB</label>{{$ctrl.dateOfBirth}}</div><div class="form-group form-group-sm"><label for>Medical Record #</label></div><div class="form-group form-group-sm"><label for>Sex</label></div><div class="form-group form-group-sm"><label for>Patient #</label></div><div class="form-group form-group-sm"><label for>Email</label></div><div class="form-group form-group-sm"><label for>Language</label></div><div class="form-group form-group-sm"><label for>Account #</label></div></div></div><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Risk Factors</div></div><div class=panel-body><div>Weight:</div><div>BMI:</div></div></div></div><div class=col-sm-6><h4>Case Info</h4><div class=col-sm-12><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Procedure Info</div></div><div class=panel-body><os-procedure-info></os-procedure-info></div></div></div><div class=col-sm-6><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Sensor Info</div></div><div class=panel-body></div></div></div><div class=col-sm-6><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Implants</div></div><div class=panel-body></div></div></div><div class=col-sm-12><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Load Balances</div></div><div class=panel-body></div></div></div></div><div class=col-sm-4><h4>Time Line</h4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Prom / Follow Up</div></div><div class=panel-body></div></div></div></div></div>');
$templateCache.put('app/testPage.html','<div class="container-fluid content test"><div class=row style=padding-left:0px;><div class=col-sm-12><button class="btn btn-default" ui-sref=patientTest>Patient Test\u0131</button></div><div class=col-sm-4><div class="panel panel-warning"><div class="panel-body text-center"><span style=font-size:1.1em>Appointments next week <span class=badge>{{$ctrl.patientsScheduled}}</span></span></div></div></div><div class=col-sm-4><div class="panel panel-danger"><div class="panel-body text-center"><span style=font-size:1.1em>{{$ctrl.patientsUnderBundleTitle}}<span class=badge>{{$ctrl.patientsUnderBundleCount}}</span></span></div></div></div><div class=col-sm-4><div class="panel panel-info"><div class="panel-body text-center"><span style=font-size:1.1em>Patients out of compliance <span class=badge>6</span></span></div></div></div></div><div class=row><div class=col-sm-4><div class=col-sm-12><os-case-activity-chart></os-case-activity-chart></div><div class=col-sm-12><os-patient-phase-count charttype=bar></os-patient-phase-count></div><div class=col-sm-12><os-knee-balance-data charttype2=line></os-knee-balance-data></div><div class=col-sm-12><os-mean-change-in-prom-score></os-mean-change-in-prom-score></div></div><div class=col-sm-4><os-scheduled-patients></os-scheduled-patients></div><div class=col-sm-4><os-patients-under-bundle></os-patients-under-bundle><div class=col-sm-12 ng-if=!$ctrl.isAdmin><os-patients-out-of-compliance></os-patients-out-of-compliance></div></div></div></div>');
$templateCache.put('app/admin/admin.component.html','<div class=container><section class=mainbar style=margin-top:30px;><div class="col-sm-12 col-sm-offset-0"><div class="row main-content"><div class="col-sm-2 col-xs-12"><admin-sidebar></admin-sidebar></div><div class="col-sm-10 col-xs-12"><div ui-view class=slide></div></div></div></div></section></div>');
$templateCache.put('app/caseDetails/caseDetailContent.html','<div id=patient-details-content class=content><div class><div class=row><div class=col-xs-12><h2>{{case.Case_Type__r.Description__c}} - {{case.Laterality__c}}</h2><div class=wizard-steps><div ng-repeat="event in events" class=step ng-class="{current: event.Status__c == \'Pending Completion\', completed: event.Status__c == \'Complete\', warning: event.Date_Status__c == \'Past\' && event.Status__c != \'Complete\' }"><div class=box ng-click=switchEvent(event)><span class=title ng-show=event.Event_Type__r.Display_Name__c>{{event.Event_Type__r.Display_Name__c}}</span> <span class=title ng-hide=event.Event_Type__r.Display_Name__c>{{event.Event_Type_Name__c}}</span><i class="icon icon-c-checkmark" ng-show="event.Status__c == \'Complete\'"></i></div><div class=date>{{event.Appointment_Start__c | date:\'shortDate\'}}</div></div></div></div></div><div id=procedure ng-show="event.Event_Type_Name__c == \'Procedure\'"><os-procedure-info></os-procedure-info><div class="row row-separator"><form name=sensorForm><div class=row-title><h4>Sensor</h4><div class=input-group style=width:500px ng-show="sensorEntryType==\'barcode\'"><span class=input-group-btn><select ng-model=sensorEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text class=form-control placeholder=Barcode style=height:38px;z-index:0 id=sensorCode ng-model=sensorCode ng-keypress=isEnterKeyPressedSensor($event)> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button sensor-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=addSensor();><span>Add Sensor</span> <i class="icon icon-c-add"></i></button></span></div><div ng-messages=sensorForm.serialNumber.$error ng-if=sensorForm.submitted class=input-group style=width:180px><span class=help-block style=color:#a94442 ng-message=required><i class="fa fa-exclamation-triangle"></i> SN is required.</span> <span class=help-block style=color:#a94442 ng-message=pattern><i class="fa fa-exclamation-triangle"></i> SN should be 9 characters.</span></div><div class=input-group style=width:900px ng-show="sensorEntryType==\'manual\'"><span class=input-group-btn><select ng-model=sensorEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text id=REF ng-model=selectedRef.Name placeholder=REF style=height:38px;z-index:0 uib-typeahead="sensorRef as sensorRef.Name for sensorRef in sensorRefs | filter:{Name:$viewValue}" class=form-control typeahead-editable=false typeahead-on-select="onSelectRef($item, $model, $label)" typeahead-show-hint=true typeahead-min-length=1> <span class=input-group-btn style=width:0px;></span><div class=form-group ng-class="{ \'has-error\' : sensorForm.serialNumber.$invalid && sensorForm.submitted}"><input type=text class=form-control placeholder=SN style=height:38px;margin-left:-1px;z-index:0 id=serialNumber name=serialNumber ng-model=serialNumber required pattern=.{9,9}></div><span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="LOT #" style=height:38px;margin-left:-1px;z-index:0 id=slotNumber ng-model=slotNumber> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=smonth class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in expMonths"></select></span> <span class=input-group-btn><select ng-model=syear class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button sensor-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=addSensor();><span>Add Sensor</span> <i class="icon icon-c-add"></i></button></span></div></div></form><div class=col-xs-12><table class="table table-os"><thead><tr><th>Device ID</th><th>Manufacturer</th><th>Lot #</th><th>Model</th><th>Expiration</th><th>Year Assembled</th><th class=action>Delete</th></tr></thead><tbody><tr ng-repeat="sensor in event.OS_Device_Events__r"><td>{{sensor.OS_Device__r.Device_ID__c}}</td><td>{{sensor.OS_Device__r.Manufacturer_Name__c}}</td><td>{{sensor.OS_Device__r.Lot_Number__c}}</td><td>{{sensor.OS_Device__r.Style__c}}</td><td>{{sensor.OS_Device__r.Expiration_Date__c | date: \'MM/yyyy\'}}</td><td>{{sensor.OS_Device__r.Year_Assembled__c}}</td><td class=action><button type=button class="btn btn-os" ng-click=deleteSensor(sensor)><i class="fa fa-remove"></i></button></td></tr></tbody></table></div></div><div class="row row-separator"><div class=row-title><h4>Implant</h4><div class=input-group style=width:800px; ng-show="implantEntryType==\'manual\'"><span class=input-group-btn><select ng-model=implantEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text id=ProductNumber ng-model=selectedProduct.Name placeholder="Product Number" style=height:38px;z-index:0 uib-typeahead="product as product.Product_Number_and_Name__c for product in products | filter:{Name:$viewValue}" class=form-control typeahead-editable=false typeahead-on-select="onSelectProduct($item, $model, $label)" typeahead-show-hint=true typeahead-min-length=1> <span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="Lot #" style=height:38px;margin-left:-1px;z-index:0 id=lotNumber ng-model=lotNumber> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=month class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in expMonths"></select></span> <span class=input-group-btn><select ng-model=year class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button implant-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=checkAndSaveImplantBarcode();><span>Add Implant</span> <i class="icon icon-c-add"></i></button></span><script>Ladda.bind(\'.input-group-btn button\');</script></div><div class=input-group style=width:800px; ng-show="implantEntryType==\'barcode\'"><span class=input-group-btn><select ng-model=implantEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text class=form-control placeholder="First Barcode" style=height:38px;z-index:0 ng-model=firstbar id=firstbar ng-keypress=isEnterKeyPressed($event)> <span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="Second Barcode" style=height:38px;margin-left:-1px;z-index:0 id=secondbar ng-model=secondbar ng-focus=checkAndSaveImplantBarcode()> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=month class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in expMonths"></select></span> <span class=input-group-btn><select ng-model=year class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button implant-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=checkAndSaveImplantBarcode();><span>Add Implant</span> <i class="icon icon-c-add"></i></button></span><script>Ladda.bind(\'.input-group-btn button\');</script></div></div><div class=col-xs-12><table class="table table-os"><thead><tr><th>Manufacturer</th><th>Product #</th><th>Product Name</th><th>Lot #</th><th>Expiration</th><th>Status</th><th class=action>Delete</th></tr></thead><tbody><tr ng-repeat="implant in event.Implant_Components__r"><td>{{implant.Manufacturer__c}}</td><td>{{implant.Product_Catalog_Id__r.Name}}</td><td>{{implant.Product_Catalog_Id__r.Product_Name__c}}</td><td>{{implant.Lot_Number__c}}</td><td>{{implant.Expiration_Date__c + 1 | date: \'MM/yyyy\'}}</td><td><div class=select><select><option>{{implant.Status__c}}</option></select></div></td><td class=action><button type=button class="btn btn-os" ng-click=deleteImplant(implant)><i class="fa fa-remove"></i></button></td></tr></tbody></table></div></div><div class="row row-separator"><div class=row-title><h4>Notes</h4><button type=button class="btn btn-os btn-os-primary" ng-click=addNewCaseNote(procedure.Id)><span>Add Note</span> <i class="icon icon-c-add"></i></button></div><div class=col-xs-12><div class="row row-separator" ng-repeat="note in event.Notes | orderBy: \'-CreatedDate\'"><div class="col-xs-12 flex"><div class=left style=width:100%><span class=date-stamp>{{note.CreatedDate | date: \'shortDate\'}}: {{note.Title}}</span><p>{{note.Body}}</p></div><div class=right><button type=button class="btn btn-os" ng-click=editNote(note)><i class="fa fa-pencil"></i></button> <button type=button class="btn btn-os" ng-click=deleteNote(note)><i class="fa fa-remove"></i></button></div></div></div></div></div><os-case-interoperative-data event=event></os-case-interoperative-data></div><div id=non-procedure ng-show="event.Event_Type_Name__c != \'Procedure\'"><div class=row><div class=row-title><h3>{{event.Event_Type_Name__c}}</h3><div class=form-horizontal><div class=form-group><label class=control-label>Date of Visit</label><div class=value>{{event.Appointment_Start__c | date: \'shortDate\'}}</div></div></div><div class=actions><button type=button class="btn btn-os btn-os-primary ladda-button" style=z-index:0 data-style=zoom-in data-spinner-color=DodgerBlue ng-click=completeEvent(event.Id) ng-show="event.Status__c != \'Complete\'"><span>Complete Visit</span> <i class="icon icon-checkmark"></i></button> <button type=button class="btn btn-os btn-os-primary ladda-button refresh-btn" style=z-index:0 data-style=zoom-in data-spinner-color=DodgerBlue ng-click=refreshEventsAndSurveys()><span>Refresh</span></button></div><script>Ladda.bind(\'.actions button\');</script></div><div class=col-xs-12></div></div></div><os-survey-list event=event></os-survey-list><div id=non-procedure ng-show="event.Event_Type_Name__c != \'Procedure\'"><div class="row row-separator"><div class=row-title><h3>Outcomes</h3><div class=toggle-buttons><select ng-model=renderedSurvey class=form-control ng-change=surveyChange(renderedSurvey) style="background-color:white; height:38px;margin-left:-2px;" ng-options="survey.Name__c as survey.Name__c for survey in surveys"></select></div></div><div class="row row-separator" ng-show="(toggleValue === \'chart\') || (toggleValue === \'data\' && surveyGraphable)"><div class=row-title><h3></h3><div class=toggle-buttons><button type=button ng-show="toggleValue === \'chart\'" class="btn btn-os btn-os-primary" ng-click=toggleOutcomes(data)><span>View Data</span> <i class="fa fa-th-list"></i></button> <button type=button ng-show="toggleValue === \'data\' && surveyGraphable" class="btn btn-os btn-os-primary" ng-click=toggleOutcomes(chart)><span>View Chart</span> <i class="fa fa-signal"></i></button></div></div></div><div class="col-xs-12 col-sm-10 col-sm-offset-1"><div class=toggle-view-container><div id=outcomes-chart class="toggle-view chart" ng-show="showGraph && toggleValue===\'chart\'"><div class="row chart"><div id=kss-chart class=chart-svg></div></div></div><div id=outcomes-data class="toggle-view chart" ng-show="toggleValue===\'data\'"><div class=row><div class=col-xs-12><br><br><table class="table table-os"><thead><tr><th style=font-size:14px>Question</th><th style=font-size:14px ng-repeat="category in chartCategories track by $index">{{category}}</th></tr></thead><tbody><tr ng-repeat="items in chartData"><td ng-repeat="item in items track by $index">{{item}}</td></tr></tbody></table></div></div></div></div></div></div></div></div></div>');
$templateCache.put('app/caseDetails/caseDetailHeader.html','<button class="btn btn-os navigation-action" ui-sref=patientList><i class="fa fa-chevron-left"></i></button><os-patient-detail-info></os-patient-detail-info>');
$templateCache.put('app/layout/shell.html','<div id=header class=container-fluid><div id=logo><img ng-src={{$ctrl.logo}} width=42 height=42 style=margin-top:15px></div><div id=view-header class="ng-animate slide" ui-view=header></div><div style=padding-top:15px;></div><div id=user-menu><div class="dropdown clear pull-right"><button id=user-name type=button data-toggle=dropdown aria-haspopup=true aria-expanded=false><span>{{$ctrl.name}}</span> <span class=caret></span></button><ul aria-labelledby=dLabel class="dropdown-menu pull-right"><li><a href=#><i class="fa fa-gear"></i> Preferences</a></li><li ng-show="$ctrl.systemMode==\'development\'"><a href=# ui-sref=admin><i class="fa fa-suitecase"></i>Admin</a></li><li ng-show="$ctrl.systemMode==\'development\'"><a href=# ui-sref=test><i class="fa fa-check"></i>Test</a></li><li><a href={{$ctrl.logoutCall}} target=_self><i class="fa fa-sign-out"></i> Log out</a></li></ul></div></div></div><div id=view-content class="ng-animate slide" style=margin-top:30px;padding-top:30px ui-view=content></div><div class=modal></div>');
$templateCache.put('app/layout/sidebar.html','<div class=sidebar><ul class=list-group><li class=list-group-item><a ng-link="[\'Dashboard\']">Home</a></li></ul></div>');
$templateCache.put('app/layout/topMenu.component.html','<div style=width:600px;><div class=row><div class=col-sm-1><div class="dropdown pull-left"><button id=main-navigation class="btn btn-os navigation-action" type=button data-toggle=dropdown aria-haspopup=true aria-expanded=false><i class="fa fa-bars" aria-hidden=true></i></button><ul class=dropdown-menu aria-labelledby=dLabel><li><a ui-sref=patientList><i class="fa fa-child"></i> Patients</a></li><li><a href=# ui-sref=dashboard><i class="fa fa-dashboard"></i> Dashboard</a></li><li ng-show="$ctrl.systemMode==\'development\'"><a href=# ui-sref=notifications><i class="fa fa-envelope"></i>Notifications</a></li><li ng-show="$ctrl.systemMode==\'development\'"><a href=# ui-sref=reports><i class="fa fa-bar-chart"></i>Reports</a></li></ul></div></div><div class=col-sm-5><h1 style=padding-left:1px;>{{$ctrl.title}}</h1></div></div></div>');
$templateCache.put('app/layout/topnav.html','<os-top-menu></os-top-menu><span style=font-size:1.4em;font-weight:600;>{{$ctrl.title}}</span>');
$templateCache.put('app/layout/widgetheader.html','<div class="page-title pull-left">{{title}}</div><small class=page-title-subtle data-ng-show=subtitle>({{subtitle}})</small><div class="widget-icons pull-right" data-ng-if=allowCollapse><a data-cc-widget-minimize></a></div><small class="pull-right page-title-subtle" data-ng-show=rightText>{{rightText}}</small><div class=clearfix></div>');
$templateCache.put('app/dashboard/dashboard.component.html','<div class="container-fluid content test"><div class=row style=padding-left:0px; ng-if="!$ctrl.isAdmin || !$ctrl.user.userProfile == orthosensor.domains.UserProfile.HospitalAdmin"><div ng-class=$ctrl.standardDashboardWidgetSize><div class="panel panel-warning"><div class="panel-body text-center"><span style=font-size:1.1em>Appointments next week <span class=badge>{{$ctrl.patientsScheduled}}</span></span></div></div></div><div ng-class=$ctrl.standardDashboardWidgetSize><div class="panel panel-danger"><div class="panel-body text-center"><span style=font-size:1.1em>{{$ctrl.patientsUnderBundleTitle}} <span class=badge>{{$ctrl.patientsUnderBundleCount}}</span></span></div></div></div><div ng-class=$ctrl.standardDashboardWidgetSize ng-show="$ctrl.systemMode == \'development\'"><div class="panel panel-info"><div class="panel-body text-center"><span style=font-size:1.1em>Patients out of compliance <span class=badge>2</span></span></div></div></div></div><div class="row text-left"><div ng-class=$ctrl.standardComponentSize><os-case-activity-chart></os-case-activity-chart></div><div ng-class=$ctrl.standardComponentSize><os-patient-phase-count charttype=bar></os-patient-phase-count></div><div ng-class=$ctrl.standardComponentSize ng-if="!$ctrl.isAdmin || $ctrl.user.userProfile == orthosensor.domains.UserProfile.HospitalAdmin"><os-knee-balance-data charttype2=line></os-knee-balance-data></div><div ng-class=$ctrl.standardComponentSize ng-if="!$ctrl.isAdmin || $ctrl.user.userProfile == orthosensor.domains.UserProfile.HospitalAdmin"><os-mean-change-in-prom-score></os-mean-change-in-prom-score></div><div ng-class=$ctrl.standardComponentSize ng-if="!$ctrl.isAdmin || $ctrl.user.userProfile == orthosensor.domains.UserProfile.HospitalAdmin"><os-patients-under-bundle></os-patients-under-bundle></div><div ng-class=$ctrl.standardComponentSize ng-if="!$ctrl.isAdmin || $ctrl.user.userProfile == orthosensor.domains.UserProfile.HospitalAdmin"><os-scheduled-patients></os-scheduled-patients></div></div><div class="row text-left" ng-show="$ctrl.systemMode == \'development\'"><div ng-class=$ctrl.standardComponentSize ng-if=!$ctrl.isAdmin><os-patients-out-of-compliance></os-patients-out-of-compliance></div><div ng-class=$ctrl.standardComponentSize ng-if=!$ctrl.isAdmin></div><div ng-class=$ctrl.standardComponentSize ng-if=!$ctrl.isAdmin></div></div><footer class=footer ng-show="$ctrl.systemMode==\'development\'"><form class=form-inline><div class=col-sm-4><div class=form-group><label class=control-label>User Name</label><p class=form-control-static>{{$ctrl.user.name}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>User Type</label><p class=form-control-static>{{$ctrl.user.userType}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>User Profile</label><p class=form-control-static>{{$ctrl.user.userProfile}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>Record Type</label><p class=form-control-static>{{$ctrl.user.recordType}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>Account Name</label><p class=form-control-static>{{$ctrl.user.accountName}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>Hospital Name</label><p class=form-control-static>{{$ctrl.user.hospitalName}}</p></div></div><div class=col-sm-4><div class=form-group><label class=control-label>System Mode</label><p class=form-control-static>{{$ctrl.systemMode}}</p></div></div></form></footer></div>');
$templateCache.put('app/notifications/notificationList.component.html','<section class="container-fluid content test" style=padding-top:30px;><div class=row><div class="col-sm-10 col-sm-offset-1 col-xss-12"><h4>{{$ctrl.title}}</h4><div ng-repeat="notification in $ctrl.notifications"><div class="panel panel-default"><div class=panel-heading><div class=panel-title>{{notification.patientName}}: {{notification.id}}</div></div><div class=panel-body><p>{{notification.message}}</p></div><div class=panel-footer><div class=btn-group><div type=button class="btn btn-primary">Mark as read</div></div></div></div></div></div></div></section>');
$templateCache.put('app/patientDetail/patientDetailContent.html','<div id=patient-details-content class=content><div class><div class=row><div class=row-title ng-show="$ctrl.caseCount>0"><h3>Cases</h3><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.addPatientCase(patient)><span>Add a Case</span> <i class="icon icon-c-add"></i></button></div><div class="col-xs-12 col-sm-6 col-sm-offset-3" ng-if="$ctrl.caseCount==0"><div class="panel panel-default"><div class=panel-body><div class=text-center><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.addPatientCase(patient)><span>Add a Case</span> <i class="icon icon-c-add"></i></button></div></div></div></div><div class=col-xs-12><div class="row row-separator" ng-repeat="case in $ctrl.cases"><div class=col-xs-12><h4>{{case.Case_Type__r.Description__c}} - {{case.Laterality__c}}</h4><div class=wizard-steps><div ng-repeat="event in case.Events__r" ng-click=$ctrl.caseDetails(case,event) class=step ng-class="{current: event.Status__c == \'Pending Completion\', completed: event.Status__c == \'Complete\', warning: event.Date_Status__c == \'Past\' && event.Status__c != \'Complete\' }"><div class=box><span class=title ng-show=event.Event_Type__r.Display_Name__c>{{event.Event_Type__r.Display_Name__c}}</span> <span class=title ng-hide=event.Event_Type__r.Display_Name__c>{{event.Event_Type_Name__c}}</span><i class="icon icon-c-checkmark" ng-show="event.Status__c == \'Complete\'"></i></div><div class=date>{{event.Appointment_Start__c | date: \'shortDate\'}}</div></div></div></div></div></div></div></div><div class ng-show="$ctrl.cases.length > 0"><div class="row row-separator"><div class=row-title><h3>Outcomes</h3><div class=toggle-buttons><select ng-model=$ctrl.renderedSurvey class=form-control ng-change=$ctrl.surveyChange($ctrl.renderedSurvey) ng-options="survey.Name__c as survey.Name__c for survey in $ctrl.surveys"></select></div></div><div class="row row-separator" ng-show="($ctrl.toggleValue === \'chart\') || ($ctrl.toggleValue === \'data\' && $ctrl.surveyGraphable)"><div class=row-title><h3></h3><div class=toggle-buttons><button type=button ng-show="$ctrl.toggleValue === \'chart\'" class="btn btn-os btn-os-primary" ng-click=$ctrl.toggleOutcomes(data)><span>View Data</span> <i class="fa fa-th-list"></i></button> <button type=button ng-show="$ctrl.toggleValue === \'data\' && $ctrl.surveyGraphable" class="btn btn-os btn-os-primary" ng-click=$ctrl.toggleOutcomes(chart)><span>View Chart</span> <i class="fa fa-signal"></i></button></div></div></div><div class="col-xs-12 col-sm-10 col-sm-offset-1"><div class=toggle-view-container><div class="row row-separator chart"><div ng-repeat="case in $ctrl.cases track by $index" style=margin-bottom:50px ng-model=$ctrl.currentCase><h4>{{case.Case_Type__r.Description__c}} - {{case.Laterality__c}}</h4><div id=outcomes-chart class="toggle-view chart" ng-show="$ctrl.showGraph && $ctrl.toggleValue === \'chart\'"><div id={{case.Id}} class=chart-svg></div></div><div id=outcomes-data ng-show="$ctrl.toggleValue === \'data\'" class=toggle-view><div class="row row-separator"><h4>{{$ctrl.renderedSurvey}}</h4><div class=col-xs-12><table class="table table-os"><thead><tr><th style=font-size:14px>Question</th><th style=font-size:14px ng-repeat="category in $ctrl.chartCategories track by $index">{{category}}</th></tr></thead><tbody><tr ng-repeat="items in $ctrl.chartData"><td ng-repeat="item in items track by $index">{{item}}</td></tr></tbody></table></div></div></div></div></div></div></div></div></div></div>');
$templateCache.put('app/patientDetail/patientDetailHeader.html','<button class="btn btn-os navigation-action" ui-sref=patientList><i class="glyphicon glyphicon-chevron-left"></i></button><style>\n\t#header #view-header .drawer.open {\n\t\theight: 60px;\n\t}\n</style><os-patient-detail-info></os-patient-detail-info>');
$templateCache.put('app/patientList/existingPatients.component.html','<div style="padding-left:12px; margin-right:12px;margin-top:12px;"><table class="table table-os" ng-if="sourceFilter==\'Existing\'"><thead><tr><th>Name</th><th>DOB</th><th>Sex</th><th>Surgeon</th><th>Case Type</th><th>Procedure Date</th><th class=action>Add a Case</th></tr></thead><tbody ng-repeat="patient in $ctrl.filteredPatients"><tr><td class=action><button type=button class="btn btn-os" ng-click=$ctrl.addPatientCase(patient)><i class="icon icon-add-folder"></i></button></td><td rowspan=99><a ng-click=patientDetails(patient) ng-show=!patient.Anonymous__c>{{patient.Last_Name__c}}, {{patient.First_Name__c}}</a> <a ng-click=patientDetails(patient) ng-show=patient.Anonymous__c>{{patient.Anonymous_Label__c}} (Anonymous)</a></td><td rowspan=99><span ng-show=!patient.Anonymous__c>{{patient.Date_Of_Birth__c | date: \'shortDate\'}}</span> <span ng-show=patient.Anonymous__c>{{patient.Anonymous_Year_of_Birth__c}}</span></td><td rowspan=99>{{patient.Gender__c}}</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr class=sub-row ng-click=caseDetails(patient,case) ng-repeat="case in patient.Cases__r"><td>{{case.Physician__r.Name}}</td><td><strong>{{case.Case_Type__r.Description__c}} - {{case.Laterality__c}}</strong></td><td>{{case.Procedure_Date__c | date:\'longDate\'}}</td><td>&nbsp;</td></tr></tbody></table></div>');
$templateCache.put('app/patientList/patientListContent.component.html','<div id=patient-list-content class=content><div class="flex container-fluid"><div class=left><h2>Patient List</h2><div style=font-weight:bold;font-size:14px;><span>{{$ctrl.currentHospital()}}</span> <a href=# ng-if="currentUser.ContactId==null" ng-click=$ctrl.openHospitalFilter();>(Change)</a></div></div><div class="right large-search" style=z-index:1><input type=text placeholder="Patient Search" ng-model=$ctrl.patientSearchValue ng-class="{\'s-active\': patientSearchValue !== \'\'}" ng-keyup=$ctrl.searchPatients()> <button type=button class="btn btn-os" ng-click=$ctrl.resetSearch()><i class="icon icon-close" ng-show="$ctrl.patientSearchValue !== \'\'"></i> <i class="icon icon-search" ng-show="$ctrl.patientSearchValue === \'\'"></i></button><div class=suggestions ng-show="$ctrl.patientSearchValue.length > 2" ng-class="{\'multi-sources\': $ctrl.patientSearchValue !== \'\'}"><div id=existing-patients class="title active" ng-click="activeTab = \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}">Existing Patients ({{$ctrl.searchPatientResults.length}})</div><div id=hl7-patients class=title ng-click="activeTab = \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}">HL7 Patients ({{$ctrl.searchHL7PatientResults.length}})</div><ul ng-show="patientSearchValue !== \'\' && activeTab === \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}"><li ng-repeat="result in searchPatientResults"><span ng-show=!result.Anonymous__c><span class="patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{$ctrl.result.Last_Name__c}}, {{$ctrl.result.First_Name__c}} {{$ctrl.result.Middle_Initial__c}}</span> <span class=dob>{{$ctrl.result.Gender__c}} - {{$ctrl.result.Date_Of_Birth__c | date: \'shortDate\'}}</span><span class=patient-number>{{$ctrl.result.Medical_Record_Number__c}}</span></span> <span ng-show=$ctrl.result.Anonymous__c><span class="patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{$ctrl.result.Anonymous_Label__c}} (Anonymous)</span> <span class=dob>{{$ctrl.result.Gender__c}} - {{$ctrl.result.Anonymous_Year_of_Birth__c}}</span><span class=patient-number>{{$ctrl.result.Medical_Record_Number__c}}</span></span></li></ul><ul ng-show="$ctrl.patientSearchValue !== \'\' && activeTab === \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}"><li ng-repeat="result in $ctrl.searchHL7PatientResults"><i class="fa fa-rss HL7-icon"></i> <span class="patient-name matching" ng-click=$ctrl.searchHL7PatientSelected(result)>{{$ctrl.result.Patient_Last_Name__c}}, {{$ctrl.result.Patient_First_Name__c}} {{$ctrl.result.Patient_Middle_Initial__c}}</span> <span class=dob>{{$ctrl.result.Patient_Gender__c}} - {{$ctrl.result.Patient_Date_Of_Birth__c | date: \'shortDate\'}}</span> <span class=patient-number>{{$ctrl.result.Medical_Record_Number__c}}</span></li></ul></div></div></div><div class style="padding-left:12px; margin-right:12px;"><div class=row><div class=col-xs-12><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.addNewPatient()><span>Add a Patient</span> <i class="icon icon-add-person"></i></button><div class=filter><div id=patients-list-filter class=custom-drop-down><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.toggleFilter()><span>Filter By</span> <i class="icon icon-download-bars"></i></button><div class=backdrop ng-class="{\'open\': filterOpen}" ng-click=$ctrl.closeFilter()></div><ul class=level1 ng-class="{\'open\': filterOpen}"><li><div><a ng-hide="$ctrl.filterDrillDown == true" ng-click=$ctrl.drillDownFilter()><i class="fa fa-chevron-right"></i> Surgeon</a><ul ng-show=$ctrl.filterDrillDown><li class=back ng-click=$ctrl.drillUpFilter()><a><i class="fa fa-chevron-left pull-left"></i>Back</a></li><li ng-repeat="surgeon in $ctrl.surgeons"><a ng-click="$ctrl.filterClicked(\'surgeon\', surgeon, $event)" ng-class="{\'active\':$ctrl.surgeonFilters.indexOf(surgeon)!=-1}"><i class="fa fa-check"></i>{{$ctrl.surgeon.Surgeon__r.Name}}</a></li></ul></div></li><li><div ng-hide="$ctrl.filterDrillDown == true"><a ng-click="$ctrl.filterClicked(\'source\', \'Existing\', $event)"><i class="fa fa-check"></i>Existing Patients</a></div></li><li><div ng-hide="$ctrl.filterDrillDown == true"><a ng-click="$ctrl.filterClicked(\'source\', \'Scheduled\', $event)"><i class="fa fa-check"></i>Scheduled Patients</a></div></li><li><div ng-hide="$ctrl.filterDrillDown == true"><a ng-click="$ctrl.filterClicked(\'date\', \'\', $event)"><i class="fa fa-check"></i>Procedure Date</a><br><br><input type=hidden datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.procedureDateFilterValue is-open=$ctrl.procedureDateFilter datepicker-options=$ctrl.dateOptions></div></li></ul></div><div class=filter-labels><div class="filter-label date" style=margin-right:5px; ng-show="$ctrl.sourceFilter==\'Existing\'"><span>Existing Patients</span></div><div class="filter-label date" style=margin-left:10px;margin-right:5px; ng-show="$ctrl.sourceFilter==\'Scheduled\'"><span>Scheduled Patients</span></div><div class="filter-label surgeon" style=margin-right:5px; ng-repeat="filter in $ctrl.surgeonFilters"><span>{{$ctrl.filter.Surgeon__r.Name}}</span> <button type=button class="button clear" ng-click=$ctrl.removeSurgeonFilter(filter)><i class="fa fa-close"></i></button></div><div class="filter-label stage" style=margin-right:5px; ng-show=$ctrl.procedureDateFilterValue><span>{{$ctrl.procedureDateFilterValue | date:\'longDate\'}}</span> <button type=button class="button clear" ng-click=$ctrl.removeDateFilter()><i class="fa fa-close"></i></button></div></div></div></div></div></div><div style="padding-left:12px; margin-right:12px;margin-top:12px;"><table class="table table-os" ng-if="$ctrl.sourceFilter==\'Existing\'"><thead><tr><th>Name</th><th>DOB</th><th>Sex</th><th>Surgeon</th><th>Case Type</th><th>Procedure Date</th><th class=action>Add a Case</th></tr></thead><tbody ng-repeat="patient in $ctrl.filteredPatients"><tr><td rowspan=99><a ng-click=$ctrl.patientDetails(patient) ng-show=!$ctrl.patient.Anonymous__c>{{$ctrl.patient.Last_Name__c}}, {{patient.First_Name__c}}</a> <a ng-click=$ctrl.patientDetails(patient) ng-show=$ctrl.patient.Anonymous__c>{{$ctrl.patient.Anonymous_Label__c}} (Anonymous)</a></td><td rowspan=99><span ng-show=!$ctrl.patient.Anonymous__c>{{$ctrl.patient.Date_Of_Birth__c | date: \'shortDate\'}}</span> <span ng-show=$ctrl.patient.Anonymous__c>{{$ctrl.patient.Anonymous_Year_of_Birth__c}}</span></td><td rowspan=99>{{$ctrl.patient.Gender__c}}</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td class=action><button type=button class="btn btn-os" ng-click=$ctrl.addPatientCase(patient)><i class="icon icon-add-folder"></i></button></td></tr><tr class=sub-row ng-click=$ctrl.caseDetails(patient,case) ng-repeat="case in $ctrl.patient.Cases__r"><td>{{$ctrl.case.Physician__r.Name}}</td><td><strong>{{$ctrl.case.Case_Type__r.Description__c}} - {{$ctrl.case.Laterality__c}}</strong></td><td>{{$ctrl.case.Procedure_Date__c | date:\'longDate\'}}</td><td>&nbsp;</td></tr></tbody></table></div><os-scheduled-patients></os-scheduled-patients></div>');
$templateCache.put('app/patientList/patientListContent.html','<style>\n\tinput[type="text"]::-ms-clear {\n\t\tdisplay: none;\n\t}\n</style><div id=patient-list-content class=content><div class="flex container-fluid"><div class=left><h3>Patient List</h3><div style=font-weight:bold;font-size:1.1em;><label>Hospital</label> <span class="form-control-static strong" style=padding-right:8px;>{{$ctrl.currentHospitalName}}</span> <label ng-show="$ctrl.currentPracticeName != \'\'">Practice</label> <span class=form-control-static>{{$ctrl.currentPracticeName}}</span> <a href=# class="btn btn-link btn-sm" ng-if=$ctrl.canChangeHospitalPracticeView ng-click=$ctrl.openHospitalFilter();>(Change)</a></div></div></div><div class style="padding-left:12px; margin-right:12px;"><nav class="navbar navbar-default"><form class="navbar-form navbar-left col-sm-3"><button type=button class="btn btn-default" ng-click=$ctrl.addNewPatient()><span>Add a Patient</span> <i class="icon icon-add-person"></i></button></form><form class="navbar-form navbar-left"><div class="form-group form-group-sm"><label class="control-label col-sm-3" style=padding-top:6px;>Surgeon</label><div class=col-sm-3><select class="form-control input-sm" ng-options="surgeon.name for surgeon in $ctrl.surgeons | filter: {parentId: $ctrl.practice.id}" name=surgeon ng-model=$ctrl.surgeon value=surgeon ng-disabled="practice == \'All\'" placeholder=Surgeon ng-change="$ctrl.filterClicked(\'surgeon\', $ctrl.surgeon, $event)"></select></div></div></form><form class="navbar-form navbar-right" style=margin-right:24px;><div class=large-search><input type=text placeholder="Patient Search" ng-model=$ctrl.patientSearchValue class=form-control ng-class="{\'s-active\': $ctrl.patientSearchValue !== \'\'}" ng-keyup=$ctrl.searchPatients()> <button type=button class="btn btn-os" ng-click=$ctrl.resetSearch()><i class="icon icon-close" ng-show="$ctrl.patientSearchValue !== \'\'"></i> <i class="icon icon-search" ng-show="$ctrl.patientSearchValue === \'\'"></i></button><div class=suggestions ng-show="$ctrl.patientSearchValue.length > 2" ng-class="{\'multi-sources\': $ctrl.patientSearchValue !== \'\'}"><div id=existing-patients class="title active" ng-click="activeTab = \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}">Existing Patients ({{$ctrl.searchPatientResults.length}})</div><div id=hl7-patients class=title ng-click="activeTab = \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}">HL7 Patients ({{$ctrl.searchHL7PatientResults.length}})</div><ul ng-show="$ctrl.patientSearchValue !== \'\' && activeTab === \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}"><li ng-repeat="result in $ctrl.searchPatientResults"><span ng-show=!result.Anonymous__c><a class="btn btn-link patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{result.Last_Name__c}}, {{result.First_Name__c}} {{result.Middle_Initial__c}}</a> <span class="dob tex-center">{{result.Gender__c}} - {{result.Date_Of_Birth__c | date: \'shortDate\'}}</span> <span class="patient-number text-center">{{result.Medical_Record_Number__c}}</span></span> <span ng-show=result.Anonymous__c><span class="patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{result.Anonymous_Label__c}} (Anonymous)</span> <span class=dob>{{result.Gender__c}} - {{result.Anonymous_Year_of_Birth__c}}</span><span class=patient-number>{{result.Medical_Record_Number__c}}</span></span></li></ul><ul ng-show="$ctrl.patientSearchValue !== \'\' && activeTab === \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}"><li ng-repeat="result in $ctrl.searchHL7PatientResults"><i class="fa fa-rss HL7-icon"></i> <a class="btn btn-link patient-name matching" ng-click=$ctrl.searchHL7PatientSelected(result)>{{result.Patient_Last_Name__c}}, {{result.Patient_First_Name__c}} {{result.Patient_Middle_Initial__c}}</a> <span class=dob>{{result.Patient_Gender__c}} - {{result.Patient_Date_Of_Birth__c | date: \'shortDate\'}}</span> <span class=patient-number>{{result.Medical_Record_Number__c}}</span></li></ul></div></div></form></nav></div><div style="padding-left:12px; margin-right:12px;margin-top:12px;" ng-if="$ctrl.sourceFilter==\'Existing\'"><table class="table table-os" ng-if="$ctrl.sourceFilter==\'Existing\'"><thead><tr><th>Name</th><th>DOB</th><th>Sex</th><th>Surgeon</th><th>Case Type</th><th>Procedure Date</th><th class=action>Add a Case</th></tr></thead><tbody ng-repeat="patient in $ctrl.filteredPatients"><tr><td rowspan=99><a ng-click=$ctrl.patientDetails(patient) ng-show=!patient.Anonymous__c>{{patient.Last_Name__c}}, {{patient.First_Name__c}}</a> <a ng-click=$ctrl.patientDetails(patient) ng-show=patient.Anonymous__c>{{patient.Anonymous_Label__c}} (Anonymous)</a></td><td rowspan=99><span ng-show=!patient.Anonymous__c>{{patient.Date_Of_Birth__c + 60 | date: \'shortDate\'}}</span> <span ng-show=patient.Anonymous__c>{{patient.Anonymous_Year_of_Birth__c}}</span></td><td rowspan=99>{{patient.Gender__c}}</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td class=action><button type=button class="btn btn-os" ng-click=$ctrl.addPatientCase(patient)><i class="icon icon-add-folder"></i></button></td></tr><tr class=sub-row ng-click=$ctrl.caseDetails(patient,case) ng-repeat="case in patient.Cases__r"><td>{{case.Physician__r.Name}}</td><td><a ng-click=$ctrl.caseDetails(patient,case)>{{case.Case_Type__r.Description__c}} - {{case.Laterality__c}}</a></td><td>{{case.Procedure_Date__c | date:\'shortDate\'}}</td><td>&nbsp;</td></tr></tbody></table></div><div style="padding-left:12px; margin-right:12px;margin-top:12px;" ng-if="$ctrl.sourceFilter==\'Scheduled\'"><table class="table table-os" ng-if="$ctrl.sourceFilter==\'Scheduled\'"><thead><tr><th>Name</th><th>DOB</th><th>Sex</th><th>Appointment Date</th><th>Physician</th></tr></thead><tbody ng-repeat="patient in $ctrl.filteredPatients"><tr><td><a ng-click=$ctrl.searchHL7PatientSelected(patient)>{{patient.Patient_Last_Name__c}}, {{patient.Patient_First_Name__c}}</a> <i class="fa fa-rss HL7-icon"></i></td><td>{{patient.Patient_Date_Of_Birth__c | date: \'shortDate\'}}</td><td>{{patient.Patient_Gender__c}}</td><td>{{patient.Appointment_Date__c | date: \'shortDate\'}}</td><td>{{patient.Physician_Name__c}}</td></tr></tbody></table></div></div>');
$templateCache.put('app/patientList/patientListFilter.component.html','<nav class="navbar navbar-default"><div class=container-fluid><div class=navbar-header><button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target=#bs-example-navbar-collapse-1 aria-expanded=false><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button> <a class=navbar-brand href=#><img alt=Brand ng-src={{$ctrl.logo}}></a></div><div class="collapse navbar-collapse" id=bs-example-navbar-collapse-1><ul class="nav navbar-nav"><li class=dropdown><a href=# class=dropdown-toggle data-toggle=dropdown role=button aria-haspopup=true aria-expanded=false>{{$ctrl.currentHospital}} <span class=caret></span></a><ul class=dropdown-menu><li><a href=#>Hospital A</a></li><li><a href=#>Hospital B</a></li></ul></li></ul><form class="navbar-form navbar-left"><div class="form-group form-group-sm"><label>Surgeon</label><select class=form-control><option selected value=All>All</option><option value="Last 3 months">Marcus Welby</option><option value="Last 3 months">Samantha Jones</option><option value="Last 3 months">Leonard McCoy</option></select></div></form><form class="navbar-form navbar-left"><div class="form-group form-group-sm"><label>Procedure Start:</label><select class=form-control><option selected value="Last Week">Last Week</option><option selected value="Last Month">Last month</option><option value="Last 3 months">Last 3 months</option><option value="Last 3 months">Next week</option><option value="Last 3 months">Next month</option></select></div></form><form class="navbar-form navbar-left"><div class="form-group form-group-sm"><input type=text class=form-control placeholder=Search></div></form><ul class="nav navbar-nav navbar-right"><li class=dropdown><a href=# class=dropdown-toggle data-toggle=dropdown role=button aria-haspopup=true aria-expanded=false>Joseph Shmoe<span class=caret></span></a><ul class=dropdown-menu><li><a href=#>Action</a></li><li><a href=#>Another action</a></li><li><a href=#>Something else here</a></li><li role=separator class=divider></li><li><a href=#>Separated link</a></li></ul></li></ul></div></div></nav>');
$templateCache.put('app/patientList/patientListHeader.html','<os-top-menu title=Patients></os-top-menu>');
$templateCache.put('app/patientList/patientListSearchFilter.component.html','<div class=suggestions ng-show="$ctrl.patientSearchValue.length > 2" ng-class="{\'multi-sources\': $ctrl.patientSearchValue !== \'\'}"><div id=existing-patients class="title active" ng-click="activeTab = \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}">Existing Patients ({{$ctrl.searchPatientResults.length}})</div><div id=hl7-patients class=title ng-click="activeTab = \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}">HL7 Patients ({{$ctrl.searchHL7PatientResults.length}})</div><ul ng-show="$ctrl.patientSearchValue !== \'\' && activeTab === \'existing-patients\'" ng-class="{\'active\': activeTab === \'existing-patients\'}"><li ng-repeat="result in $ctrl.searchPatientResults"><span ng-show=!result.Anonymous__c><span class="patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{result.Last_Name__c}}, {{result.First_Name__c}} {{result.Middle_Initial__c}}</span> <span class=dob>{{result.Gender__c}} - {{result.Date_Of_Birth__c | date: \'shortDate\'}}</span><span class=patient-number>{{result.Medical_Record_Number__c}}</span></span> <span ng-show=result.Anonymous__c><span class="patient-name matching" ng-click=$ctrl.searchPatientSelected(result)>{{result.Anonymous_Label__c}} (Anonymous)</span> <span class=dob>{{result.Gender__c}} - {{result.Anonymous_Year_of_Birth__c}}</span><span class=patient-number>{{result.Medical_Record_Number__c}}</span></span></li></ul><ul ng-show="$ctrl.patientSearchValue !== \'\' && activeTab === \'hl7-patients\'" ng-class="{\'active\': activeTab === \'hl7-patients\'}"><li ng-repeat="result in $ctrl.searchHL7PatientResults"><i class="fa fa-rss HL7-icon"></i> <span class="patient-name matching" ng-click=$ctrl.searchHL7PatientSelected(result)>{{result.Patient_Last_Name__c}}, {{result.Patient_First_Name__c}} {{result.Patient_Middle_Initial__c}}</span> <span class=dob>{{result.Patient_Gender__c}} - {{result.Patient_Date_Of_Birth__c | date: \'shortDate\'}}</span><span class=patient-number>{{result.Medical_Record_Number__c}}</span></li></ul></div>');
$templateCache.put('app/reports/reports-sidebar.component.html','<aside id=left-panel class=sidebar><nav><ul class=list-group><li class=list-group-item><a ui-sref=.caseActivityChart><i class="fa fa-briefcase"></i> Case Activity</a></li><li class=list-group-item><a ui-sref=.phaseCountChart><i class="fa fa-hospital-o"></i> Phase Count</a></li><li class=list-group-item><a ui-sref=.promDeltaChart><i class="fa fa-bar-chart"></i> Prom Delta</a></li><li class=list-group-item><a ui-sref=.kneeBalanceChart><i class="fa fa-medkit"></i> Knee Balance</a></li><ul></ul></ul></nav></aside>');
$templateCache.put('app/reports/reports.component.html','<div class="container-fluid content test" style="padding:0 0 0 0;"><section class=mainbar><div class="col-sm-12 col-sm-offset-0"><div class="row main-content"><div class="col-sm-2 col-md-2 col-lg-2 col-xs-12" style=padding-left:0;><os-reports-sidebar></os-reports-sidebar></div><div class="col-sm-10 col-md-10 col-lg-10 col-xs-12" style=margin-top:30px;><div ui-view class=slide></div></div></div></div></section></div>');
$templateCache.put('app/user/userDetail.html','<section><div class=panel-panel-default><div class=panel-body><form action><fieldset><div class=form-group><label for>ID</label> <input type=text disabled ng-model=vm.user.id class=form-control></div><div class=form-group><label for>Last Name</label> <input type=text ng-model=$ctrl.user.lastName class=form-control></div><div class=form-group><label for>First Name</label> <input type=text ng-model=user.firstName class=form-control></div><div class=form-group><label for>Phone</label> <input type=text class=form-control></div><div class=form-group><label for>labe1l</label> <input type=text class=form-control></div></fieldset></form></div><div class=panel-footer><div class=btn-group><button class="btn btn-default">Save</button> <button class="btn btn-default">Cancel</button></div></div></div></section>');
$templateCache.put('app/user/userList.html','<section class=row><div class=col-sm-6><section><div class="panel panel-default"><div class=panel-heading><panel-title>Users</panel-title></div><div class=panel-body><table class="table table-responsive table-condensed table-striped"><thead><tr><th>ID</th><th>Last Name</th><th>FirstName</th><th>Last log in</th></tr></thead><tbody><tr ng-repeat="user in vm.people" ng-click=vm.editRecord(user)><td>{{user.id}}</td><td>{{user.lastName}}</td><td>{{user.firstName}}</td><td></td></tr></tbody></table></div></div></section></div><div class=col-sm-6><div class=col-sm-12><div class="col-sm-10 col-sm-offset-1"></div></div></div></section>');
$templateCache.put('app/user/users.html','<section class=row><div class=col-sm-6><user-list></user-list></div><div class=col-sm-6><user-detail user=ctrl.user></user-detail></div></section>');
$templateCache.put('app/admin/doctorList/doctorList.component.html','<section><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Doctors</div></div><div class=panel-body><table class="table table-striped table-responsive table-condensed"><thead><tr><th>Last Name</th><th>First Name</th><th>ID</th></tr></thead><tbody><tr><td>Jones</td><td>Jerry</td><td>100921</td></tr><tr><td>John</td><td>Farker</td><td>100921</td></tr><tr><td>Garcia</td><td>Jerry</td><td>1000801</td></tr></tbody></table></div></div></section>');
$templateCache.put('app/admin/hospital/hospitalDetail.component.html','<section class="col-sm-10 col-sm-offset-1 col-xs-12"><div class=form><hospital-detail-header></hospital-detail-header><div class=col-sm-6><practice-summary></practice-summary></div><div class=col-sm-6><surgeon-summary></surgeon-summary></div></div></section>');
$templateCache.put('app/admin/hospital/hospitalDetail.html','<section><div class=form><fieldset><legend>Hospital Detail</legend><div class=col-sm-6><div class=form-group><label for>Hospital</label> <input type=text class=form-control ng-model=vm.name></div></div><div class=col-sm-6><div class=form-group><label for>Type</label> <input type=text class=form-control id=hospitalId ng-model=vm.id></div></div></fieldset><div class=col-sm-6><practice-summary></practice-summary></div><div class=col-sm-6><surgeon-summary></surgeon-summary></div></div></section>');
$templateCache.put('app/admin/hospital/hospitalDetailHeader.component.html','<fieldset><legend>Hospital Detail</legend><div class=col-sm-6><div class=form-group><label for>Hospital</label> <input type=text class=form-control ng-model=vm.name></div></div><div class=col-sm-6><div class=form-group><label for>Type</label> <input type=text class=form-control id=hospitalId ng-model=vm.id></div></div></fieldset>');
$templateCache.put('app/admin/hospital/hospitalList.html','<section><div class="panel panel-default"><div class=panel-heading><div class=panel-title>{{vm.title}}</div></div><div class=panel-body><table class="table table-responsive table-striped"><thead><tr><th>Name</th><th>ID</th></tr><tbody><tr ng-repeat="hospital in vm.hospitals" ui-sref=admin.hospitalDetail><td>{{hospital.Name}}</td><td>{{hospital.Id}}</td></tr></tbody></thead></table></div></div></section>');
$templateCache.put('app/admin/hospital/practiceDetailModal.html','<div class=modal-header><h3 class=modal-title id=modal-title>Practice Detail</h3></div><div class=modal-body id=modal-body><form action><div class=form-group><label for>Name</label> <input type=text class=form-control></div><div class=form-group><label for>Address</label> <input type=text class=form-control></div><div class=form-group><label for>City</label> <input type=text class=form-control></div></form></div><div class=modal-footer><div class=btn-group><button class="btn btn-default">Save</button> <button class="btn btn-default">Cancel</button></div></div>');
$templateCache.put('app/admin/hospital/practiceSummary.component.html','<fieldset><legend>Practices</legend><table class="table table-striped table-responsive table-bordered"><thead><tr><th>Name</th><th>Id</th></tr></thead><tbody><tr ng-repeat="practice in vm.practices" ng-click=vm.open(practice)><td>{{practice.name}}</td><td>{{practice.id}}</td></tr></tbody></table></fieldset><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Practices</div></div><table class="table table-striped table-responsive table-bordered"><thead><tr><th>Name</th><th>Id</th></tr></thead><tbody><tr ng-repeat="practice in vm.practices" ng-click=vm.open(practice)><td>{{practice.name}}</td><td>{{practice.id}}</td></tr></tbody></table></div>');
$templateCache.put('app/admin/hospital/surgeonSummary.component.html','<div class="panel panel-default"><div class=panel-heading><div class=panel-title>Surgeons</div></div><table class="table table-striped table-responsive table-bordered"><thead><tr><th>Last Name</th><th>First Name</th><th></th></tr></thead><tbody><tr><td>Satriani</td><td>Joseph</td><td></td></tr><tr><td>Carlos</td><td>John</td><td></td></tr></tbody></table></div>');
$templateCache.put('app/components/addPatientCase/addPatientCase.component.html','<div class="content test"><div class=row><div class="col-xs-12 col-md-10 col-md-offset-1"><form class=form-horizontal name=pageForm><div class="panel panel-default"><div class=panel-heading><div class=panel-title>{{$ctrl.title}}</div></div><div class=panel-body><fieldset><h4 ng-if=!$ctrl.patient.anonymous>{{$ctrl.patient.lastName}}, {{$ctrl.patient.firstName}}</h4><h4 ng-if=$ctrl.patient.anonymous>Anonymous Patient: {{$ctrl.patient.label}}</h4><div class=form-group ng-if=$ctrl.multipleCaseTypes ng-class="{ \'has-error\' : pageForm.CaseType.$invalid && pageForm.CaseType.$touched}"><label for=case-type class="col-sm-3 control-label required">Case Type</label><div class=col-sm-8><div class=select><select id=case-type ng-model=$ctrl.patientCase.CaseTypeId required name=CaseType><option ng-repeat="caseType in $ctrl.caseTypes" value={{caseType.Case_Type__r.Id}}>{{caseType.Case_Type__r.Description__c}}</option></select></div><div ng-messages=pageForm.CaseType.$error ng-if="pageForm.CaseType.$invalid && pageForm.CaseType.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Case Type is required.</p></div></div></div><div class=form-group ng-if=!$ctrl.multipleCaseTypes><label for=case-type class="col-sm-3 control-label">Case Type:</label><div class=form-control-static>{{$ctrl.caseTypes[0].Case_Type__r.Description__c}}</div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.laterality.$invalid && pageForm.laterality.$touched}"><label class="col-sm-3 control-label required">Laterality</label><div class=col-sm-8><input type=radio name=laterality required id=laterality-left value=Left ng-model=$ctrl.patientCase.laterality><label for=laterality-left>Left</label> <input type=radio name=laterality required id=laterality-right value=Right ng-model=$ctrl.patientCase.laterality><label for=laterality-right>Right</label> <input type=radio name=laterality required id=laterality-both value=Both ng-model=$ctrl.patientCase.laterality><label for=laterality-both>Both</label><div ng-messages=pageForm.laterality.$error ng-if="pageForm.laterality.$invalid && pageForm.laterality.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Laterality is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.surgeon.$invalid && pageForm.surgeon.$touched}"><label for=case-surgeon class="col-sm-3 control-label required">Surgeon</label><div class=col-sm-8><div class=select><select id=case-surgeon ng-model=$ctrl.patientCase.SurgeonID required name=surgeon><option ng-repeat="surgeon in $ctrl.surgeons" value={{surgeon.id}}>{{surgeon.name}}</option></select></div><div ng-messages=pageForm.surgeon.$error ng-if="pageForm.surgeon.$invalid && pageForm.surgeon.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Surgeon is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.ProcedureDateString.$invalid && pageForm.ProcedureDateString.$touched}"><label for=new-case-procedure-date class="col-sm-4 control-label required">Procedure Date</label><div class=col-sm-8><input type=text name=ProcedureDateString required class=form-control placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.patientCase.ProcedureDateString is-open=$ctrl.open.procDate datepicker-options=dateOptions ng-required=true close-text=Close ng-click="$ctrl.open($event,\'procDate\')"> <button type=button class="btn btn-os" ng-click="$ctrl.open($event,\'procDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.ProcedureDateString.$error ng-if="pageForm.ProcedureDateString.$invalid && pageForm.ProcedureDateString.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Procedure Date is required.</p></div></div></div><div class=row><div class="col-sm-10 col-sm-offset-1"><div class=btn-group><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-primary" ng-disabled=pageForm.$invalid ladda=$ctrl.loadingCase ng-click=$ctrl.createCase(pageForm)><span>Create Case</span></button></div></div></div></fieldset></div></div></form></div></div></div>');
$templateCache.put('app/components/addPatientCase/addPatientCase.modal.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=$ctrl.cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>{{$ctrl.title}}</h4></div><div class=modal-body><h3>{{$ctrl.patient.Last_Name__c}}, {{$ctrl.patient.First_Name__c}}</h3><form class=form-horizontal name=pageForm><div class=form-group ng-class="{ \'has-error\' : pageForm.CaseType.$invalid && pageForm.submitted}"><label for=case-type class="col-sm-3 control-label required">Case Type</label><div class=col-sm-8><div class=select><select id=case-type ng-model=$ctrl.patientCase.CaseTypeId required name=CaseType><option ng-repeat="caseType in $ctrl.caseTypes" value={{caseType.Case_Type__r.Id}}>{{caseType.Case_Type__r.Description__c}}</option></select></div><div ng-messages=pageForm.CaseType.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Case Type is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.laterality.$invalid && pageForm.submitted}"><label class="col-sm-3 control-label required">Laterality</label><div class=col-sm-8><input type=radio name=laterality required id=laterality-left value=Left ng-model=$ctrl.patientCase.Laterality><label for=laterality-left>Left</label> <input type=radio name=laterality required id=laterality-right value=Right ng-model=$ctrl.patientCase.Laterality><label for=laterality-right>Right</label> <input type=radio name=laterality required id=laterality-both value=Both ng-model=$ctrl.patientCase.Laterality><label for=laterality-both>Both</label><div ng-messages=pageForm.laterality.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Laterality is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.surgeon.$invalid && pageForm.submitted}"><label for=case-surgeon class="col-sm-3 control-label required">Surgeon</label><div class=col-sm-8><div class=select><select id=case-surgeon ng-model=$ctrl.patientCase.SurgeonID required name=surgeon><option ng-repeat="surgeon in $ctrl.surgeons" value={{surgeon.Surgeon__r.AccountId}}>{{surgeon.Surgeon__r.Name}}</option></select></div><div ng-messages=pageForm.surgeon.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Surgeon is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.ProcedureDateString.$invalid && pageForm.submitted}"><label for=new-case-procedure-date class="col-sm-4 control-label required">Procedure Date</label><div class=col-sm-8><input type=text name=ProcedureDateString required class=form-control placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.patientCase.ProcedureDateString is-open=$ctrl.open.procDate datepicker-options=dateOptions ng-required=true close-text=Close ng-click="$ctrl.open($event,\'procDate\')"> <button type=button class="btn btn-os" ng-click="$ctrl.open($event,\'procDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.ProcedureDateString.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Procedure Date is required.</p></div></div></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-primary" ladda=$ctrl.loadingCase ng-click=$ctrl.createCase(pageForm)><span>Create Case</span></button></div>');
$templateCache.put('app/components/addPatientCase/patientAddNewCase.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>New Case</h4></div><div class=modal-body><h3>{{patient.Last_Name__c}}, {{patient.First_Name__c}}</h3><form class=form-horizontal name=pageForm><div class=form-group ng-class="{ \'has-error\' : pageForm.CaseType.$invalid && pageForm.submitted}"><label for=case-type class="col-sm-3 control-label required">Case Type</label><div class=col-sm-8><div class=select><select id=case-type ng-model=patientCase.CaseTypeId required name=CaseType><option ng-repeat="caseType in caseTypes" value={{caseType.Id}}>{{caseType.Description__c}}</option></select></div><div ng-messages=pageForm.CaseType.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Case Type is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.laterality.$invalid && pageForm.submitted}"><label class="col-sm-3 control-label required">Laterality</label><div class=col-sm-8><input type=radio name=laterality required id=laterality-left value=Left ng-model=patientCase.Laterality><label for=laterality-left>Left</label> <input type=radio name=laterality required id=laterality-right value=Right ng-model=patientCase.Laterality><label for=laterality-right>Right</label> <input type=radio name=laterality required id=laterality-both value=Both ng-model=patientCase.Laterality><label for=laterality-both>Both</label><div ng-messages=pageForm.laterality.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Laterality is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.surgeon.$invalid && pageForm.submitted}"><label for=case-surgeon class="col-sm-3 control-label required">Surgeon</label><div class=col-sm-8><div class=select><select id=case-surgeon ng-model=patientCase.SurgeonID required name=surgeon><option ng-repeat="surgeon in surgeons" value={{surgeon.Surgeon__r.AccountId}}>{{surgeon.Surgeon__r.Name}}</option></select></div><div ng-messages=pageForm.surgeon.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Surgeon is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.ProcedureDateString.$invalid && pageForm.submitted}"><label for=new-case-procedure-date class="col-sm-4 control-label required">Procedure Date</label><div class=col-sm-8><input type=text name=ProcedureDateString required class=form-control placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=patientCase.ProcedureDateString is-open=open.procDate datepicker-options=dateOptions ng-required=true close-text=Close ng-click="open($event,\'procDate\')"> <button type=button class="btn btn-os" ng-click="open($event,\'procDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.ProcedureDateString.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Procedure Date is required.</p></div></div></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=White ng-click=createCase()><span>Create Case</span></button></div><script>Ladda.bind(\'.modal-footer button\');</script>');
$templateCache.put('app/components/addPatient/addPatient.component.html','<div class="content test"><div class=row><div class="col-xs-12 col-md-10 col-md-offset-1"><form class=form-horizontal name=pageForm novalidate><div class="panel panel-default"><div class=panel-heading><div class=panel-title>{{$ctrl.title}}</div></div><div class=panel-body><div ng-include="\'app/components/addPatient/editPatientFields.component.html\'"></div></div></div></form></div></div></div>');
$templateCache.put('app/components/addPatient/addPatient.modal.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>{{$ctrl.title}}</h4></div><div class=modal-body><form class=form-horizontal name=pageForm><div class=row><div class="col-xs-12 col-md-5"><fieldset><div class=form-group ng-class="{ \'has-error\' : pageForm.hospital.$invalid && pageForm.submitted}" ng-if=!currentUser.Contact><label for=patient-hospital class="col-sm-4 control-label required">Hospital</label><div class=col-sm-8><div class=select><select id=patient-hospital ng-model=$ctrl.patient.hospital required name=hospital ng-disabled=$ctrl.patient.hl7 ng-options="hospital.name for hospital in $ctrl.hospitals track by hospital.id" ng-change=$ctrl.resetPractice();$ctrl.checkAnonymous()></select></div><div ng-messages=pageForm.hospital.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Hospital selection is required.</p></div></div></div><div class=form-group ng-show="$ctrl.patient.hospital && !$ctrl.patient.hl7"><label for=patient-practice class="col-sm-4 control-label">Practice</label><div class=col-sm-8><div class=select><select id=patient-practice ng-model=$ctrl.patient.practice name=surgeon ng-change=$ctrl.checkAnonymous()><option value>Not Selected</option><option ng-repeat="practice in $ctrl.practices | filter: {parentId:patient.hospital.id}" value={{practice.id}}>{{practice.name}}</option></select></div></div></div><div class=form-group ng-show=!$ctrl.patient.hl7><label for=new-patient-anonymous class="col-sm-4 control-label">Is Anonymous</label><div class=col-sm-8><input type=checkbox id=new-patient-anonymous class=show style=height:20px; name=Anonymous ng-model=$ctrl.patient.anonymous></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.Label.$invalid && pageForm.submitted}" ng-show=$ctrl.patient.anonymous><label for=new-patient-label class="col-sm-4 control-label required">Patient Label</label><div class=col-sm-8><input type=text class=form-control id=new-patient-label name=Label ng-model=$ctrl.patient.Label ng-required=$ctrl.patient.anonymous><div ng-messages=pageForm.Label.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Label is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.FirstName.$invalid && pageForm.submitted}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-first-name class="col-sm-4 control-label required">First Name</label><div class=col-sm-8><input type=text class=form-control id=new-patient-first-name name=FirstName ng-model=$ctrl.patient.FirstName ng-required=!$ctrl.patient.anonymous><div ng-messages=pageForm.FirstName.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient First Name is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.LastName.$invalid && pageForm.submitted}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-last-name class="col-sm-4 control-label required">Last Name</label><div class=col-sm-8><input type=text class=form-control id=new-patient-last-name name=LastName ng-model=$ctrl.patient.LastName ng-required=!$ctrl.patient.anonymous><div ng-messages=pageForm.LastName.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Last Name is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.newPatientSex.$invalid && pageForm.submitted}"><label for=new-sex-name class="col-sm-4 control-label required">Sex</label><div class=col-sm-8><input type=radio name=newPatientSex id=new-patient-sex-male value=M ng-model=$ctrl.patient.Gender required><label for=new-patient-sex-male>Male</label> <input type=radio name=newPatientSex id=new-patient-sex-female value=F ng-model=$ctrl.patient.Gender required><label for=new-patient-sex-female>Female</label><div ng-messages=pageForm.newPatientSex.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Sex is required.</p></div></div></div></fieldset><fieldset><div class=form-group ng-class="{ \'has-error\' : pageForm.DateofBirthString.$invalid && pageForm.submitted}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birthdate</label><div class=col-sm-8><input type=text class=form-control name=DateofBirthString placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.patient.DateofBirthString is-open=$ctrl.open.birthDate datepicker-options=$ctrl.dateOptions ng-required=!$ctrl.patient.anonymous close-text=Close ng-click="$ctrl.open($event,\'birthDate\')"> <button type=button class="btn btn-os" ng-click="$ctrl.open($event,\'birthDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.DateofBirthString.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birthdate is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.BirthYear.$invalid && pageForm.submitted}" ng-show=$ctrl.patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birth Year</label><div class=col-sm-2><div class=select><select id=patient-birth-year ng-model=$ctrl.patient.BirthYear ng-required=$ctrl.patient.anonymous name=BirthYear style=padding-left:8px;><option ng-repeat="year in $ctrl.birthYears" value={{year}}>{{year}}</option></select></div><div ng-messages=pageForm.BirthYear.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birth Year is required.</p></div></div></div></fieldset></div><div class="col-xs-12 col-md-7"><fieldset><div class=form-group ng-class="{ \'has-error\' : pageForm.MedicalRecordNumber.$invalid && pageForm.submitted}"><label for=new-patient-medical-record-number class="col-sm-4 control-label" ng-class="!$ctrl.patient.anonymous?\'required\':\'\'">Medical Record #</label><div class=col-sm-8><input type=text name=MedicalRecordNumber class=form-control id=new-patient-medical-record-number ng-model=$ctrl.patient.MedicalRecordNumber ng-required=!$ctrl.patient.anonymous><div ng-messages=pageForm.MedicalRecordNumber.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Medical Record # is required.</p></div></div></div><div class=form-group><label for=new-patient-number class="col-sm-4 control-label">Patient #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-number ng-model=$ctrl.patient.PatientNumber></div></div><div class=form-group><label for=new-patient-account-number class="col-sm-4 control-label">Account #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-account-number ng-model=$ctrl.patient.AccountNumber></div></div><div class=form-group><label for=new-patient-ss-number class="col-sm-4 control-label">Social Security #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-ss-number ng-model=$ctrl.patient.SocialSecurityNumber></div></div><fieldset><div class=form-group><label for=new-patient-race class="col-sm-4 control-label">Race</label><div class=col-sm-8><div class=select><select id=new-patient-race ng-model=$ctrl.patient.Race><option>White</option><option>Black or African American</option><option>American Indian</option><option>Asian</option><option>Native Hawaiian</option><option>Hispanic</option><option>Other</option></select></div></div></div><div class=form-group><label for=new-patient-language class="col-sm-4 control-label">Language</label><div class=col-sm-8><div class=select><select id=new-patient-language ng-model=$ctrl.patient.Language><option>English</option><option>Spanish</option><option>French</option><option>Chinese</option><option>Creole</option><option>Unknown</option><option>Russian</option><option>Portuguese</option></select></div></div></div></fieldset></fieldset><fieldset></fieldset></div></div></form></div><div class=modal-footer ng-if=!$ctrl.matchedPatient><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-primary" ladda=$ctrl.loadingPatient ng-click=$ctrl.savePatient(pageForm)><span>Save</span></button></div><div class=modal-footer ng-if=$ctrl.matchedPatient><div class=col-sm-12 style=text-align:left; ng-show=!$ctrl.matchedPatient.Anonymous__c><h1>Possible Match Found:</h1><p>Would you like to create a new Patient or proceed with the existing Patient?</p><br><b>First Name:</b>{{$ctrl.matchedPatient.First_Name__c}}<br><b>Last Name:</b>{{$ctrl.matchedPatient.Last_Name__c}}<br><b>SSN:</b>{{$ctrl.matchedPatient.SSN__c}}<br><b>Date of Birth:</b>{{$ctrl.matchedPatient.Date_Of_Birth__c | date: \'shortDate\'}}<br><b>Race:</b>{{$ctrl.matchedPatient.Race__c}}<br><b>Medical Record Number:</b>{{$ctrl.matchedPatient.Medical_Record_Number__c}}<br></div><div class=col-sm-12 style=text-align:left; ng-show=$ctrl.matchedPatient.Anonymous__c><h1>Possible Match Found:</h1><p>Would you like to create a new Patient or proceed with the existing Patient?</p><br><b>Patient Label:</b>{{$ctrl.matchedPatient.Anonymous_Label__c}}<br><b>Year of Birth:</b>{{$ctrl.matchedPatient.Anonymous_Year_of_Birth__c}}<br></div><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button ladda=$ctrl.loadingPatient class="btn btn-os btn-os-primary" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.createNewPatient(pageForm)><span>Create New Patient</span></button> <button type=button class="btn btn-os btn-os-default" data-style=zoom-in data-spinner-color=White ng-click=$ctrl.createNewCase(matchedPatient)><span>Proceed With the Existing Patient</span></button></div>');
$templateCache.put('app/components/addPatient/editPatientFields.component.html','<fieldset><div class=row><div class="col-xs-12 col-md-5"><div class=form-group ng-class="{ \'has-error\' : pageForm.hospital.$invalid && pageForm.submitted}" ng-if=$ctrl.isUserAdmin><label for=patient-hospital class="col-sm-4 control-label required">Hospital</label><div class=col-sm-8><div class=select><select id=patient-hospital ng-model=$ctrl.patient.hospital required name=hospital ng-disabled=$ctrl.patient.hl7 ng-options="hospital.name for hospital in $ctrl.hospitals track by hospital.id" ng-change=$ctrl.resetPractice();$ctrl.checkAnonymous()></select></div><div ng-messages=pageForm.hospital.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Hospital selection is required.</p></div></div></div><div class=form-group ng-show="$ctrl.patient.hospital && !$ctrl.patient.hl7"><label for=patient-practice class="col-sm-4 control-label">Practice</label><div class=col-sm-8><div class=select><select id=patient-practice ng-model=$ctrl.patient.practice name=surgeon ng-change=$ctrl.checkAnonymous()><option value>Not Selected</option><option ng-repeat="practice in $ctrl.practices | filter: {parentId:patient.hospital.id}" value={{practice.id}}>{{practice.name}}</option></select></div></div></div><div class=form-group ng-show=!$ctrl.patient.hl7><label for=new-patient-anonymous class="col-sm-4 control-label">Is Anonymous</label><div class=col-sm-8><input type=checkbox id=new-patient-anonymous class=show style=height:20px; name=anonymous ng-model=$ctrl.patient.anonymous></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.label.$invalid && pageForm.label.$touched}" ng-show=$ctrl.patient.anonymous><label for=new-patient-label class="col-sm-4 control-label required">Patient Label</label><div class=col-sm-8><input type=text class=form-control id=new-patient-label name=label ng-model=$ctrl.patient.label ng-required=$ctrl.patient.anonymous><div ng-messages=pageForm.label.$error ng-if="pageForm.label.$invalid && pageForm.label.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Label is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.firstName.$invalid && pageForm.firstName.$touched}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-first-name class="col-sm-4 control-label required">First Name</label><div class=col-sm-8><input type=text class=form-control id=new-patient-first-name name=firstName ng-model=$ctrl.patient.firstName ng-minlength=2 ng-required=!$ctrl.patient.anonymous><div ng-messages=pageForm.firstName.$error ng-if="pageForm.firstName.$invalid && pageForm.firstName.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient First Name is required.</p><p class=help-block ng-message=minlength><i class="fa fa-exclamation-triangle"></i> Patient First Name must be at least 2 characters.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.lastName.$invalid && pageForm.lastName.$touched}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-last-name class="col-sm-4 control-label required">Last Name</label><div class=col-sm-8><input class=form-control id=new-patient-last-name name=lastName ng-model=$ctrl.patient.lastName ng-required=!$ctrl.patient.anonymous ng-minlength=2><div ng-messages=pageForm.lastName.$error ng-if="pageForm.lastName.$invalid && pageForm.lastName.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Last Name is required.</p><p class=help-block ng-message=minlength><i class="fa fa-exclamation-triangle"></i> Patient Last Name must be at least 2 characters.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.newPatientSex.$invalid && pageForm.newPatientSex.$touched}"><label for=new-sex-name class="col-sm-4 control-label required">Sex</label><div class=col-sm-8><input type=radio name=newPatientSex id=new-patient-sex-male value=M ng-model=$ctrl.patient.gender required><label for=new-patient-sex-male>Male</label> <input type=radio name=newPatientSex id=new-patient-sex-female value=F ng-model=$ctrl.patient.gender required><label for=new-patient-sex-female>Female</label><div ng-messages=pageForm.newPatientSex.$error ng-if="pageForm.newPatientSex.$invalid && pageForm.newPatientSex.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Sex is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.dateOfBirthString.$invalid && pageForm.dateofBirsthString.$touched}" ng-show=!$ctrl.patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birthdate</label><div class=col-sm-8><input type=text class=form-control name=dateOfBirthString placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.patient.dateOfBirthString is-open=$ctrl.open.birthDate datepicker-options=$ctrl.dateOptions ng-required=!$ctrl.patient.anonymous ng-max=$ctrl.maxBirthDate min=01/01/1920 close-text=Close ng-click="$ctrl.open($event,\'birthDate\')"> <button type=button class="btn btn-os" ng-click="$ctrl.open($event,\'birthDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.dateOfBirthString.$error ng-if="pageForm.dateOfBirthString.$invalid && pageForm.dateofBirsthString.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birthdate is required.</p><p class=help-block ng-message=min><i class="fa fa-exclamation-triangle"></i> Patient Birthdate must be after 1920!</p><p class=help-block ng-message=ng-max><i class="fa fa-exclamation-triangle"></i> Patient Birthdate must be before today!</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.BirthYear.$invalid && pageForm.BirthYear.$touched}" ng-show=$ctrl.patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birth Year</label><div class=col-sm-2><div class=select><select id=patient-birth-year ng-model=$ctrl.patient.birthYear ng-required=$ctrl.patient.anonymous name=BirthYear style=padding-left:8px;><option ng-repeat="year in $ctrl.birthYears" value={{year}}>{{year}}</option></select></div><div ng-messages=pageForm.BirthYear.$error ng-if="pageForm.BirthYear.$invalid && pageForm.BirthYear.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birth Year is required.</p></div></div></div></div><div class="col-xs-12 col-md-7"><div class=form-group ng-class="{ \'has-error\' : pageForm.medicalRecordNumber.$invalid && pageForm.medicalRecordNumber.$touched}"><label for=new-patient-medical-record-number class="col-sm-4 control-label" ng-class="!$ctrl.patient.anonymous?\'required\':\'\'">Medical Record #</label><div class=col-sm-8><input type=text name=medicalRecordNumber class=form-control id=new-patient-medical-record-number ng-model=$ctrl.patient.medicalRecordNumber ng-required=!$ctrl.patient.anonymous><div ng-messages=pageForm.medicalRecordNumber.$error ng-if="pageForm.medicalRecordNumber.$invalid && pageForm.medicalRecordNumber.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Medical Record # is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.email.$invalid && pageForm.email.$touched}"><label for=new-patient-email class="col-sm-4 control-label">Email</label><div class=col-sm-8><input type=email name=email class=form-control id=new-patient-email ng-model=$ctrl.patient.email><div ng-messages=pageForm.email.$error ng-if="pageForm.email.$invalid && pageForm.email.$touched"><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Email is required.</p><p class=help-block ng-message=email><i class="fa fa-exclamation-triangle"></i> Email is not valid.</p></div></div></div><div class=form-group><label for=new-patient-number class="col-sm-4 control-label">Patient #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-number ng-model=$ctrl.patient.patientNumber></div></div><div class=form-group><label for=new-patient-account-number class="col-sm-4 control-label">Account #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-account-number ng-model=$ctrl.patient.accountNumber></div></div><div class=form-group><label for=new-patient-ss-number class="col-sm-4 control-label">Social Security #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-ss-number ng-model=$ctrl.patient.socialSecurityNumber></div></div><div class=form-group><label for=new-patient-race class="col-sm-4 control-label">Race</label><div class=col-sm-8><div class=select><select id=new-patient-race ng-model=$ctrl.patient.race><option>White</option><option>Black or African American</option><option>American Indian</option><option>Asian</option><option>Native Hawaiian</option><option>Hispanic</option><option>Other</option></select></div></div></div><div class=form-group><label for=new-patient-language class="col-sm-4 control-label">Language</label><div class=col-sm-8><div class=select><select id=new-patient-language ng-model=$ctrl.patient.language><option>English</option><option>Spanish</option><option>French</option><option>Chinese</option><option>Creole</option><option>Unknown</option><option>Russian</option><option>Portuguese</option></select></div></div></div></div></div><div class=row><div class="col-sm-10 col-sm-offset-1"><div class=btn-group><div class ng-if=!$ctrl.matchedPatient><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-primary" ladda=$ctrl.loadingPatient ng-disabled=pageForm.$invalid ng-click=$ctrl.savePatientOnly(pageForm)><span>Save</span></button> <button type=button class="btn btn-os btn-os-primary" ladda=$ctrl.loadingPatient ng-disabled=pageForm.$invalid ng-click=$ctrl.savePatientAndAddCase(pageForm)><span>Save and add Case</span></button></div><div class ng-if=$ctrl.matchedPatient><div class=col-sm-12 style=text-align:left; ng-show=!$ctrl.matchedPatient.Anonymous__c><h1>Possible Match Found:</h1><p>Would you like to create a new Patient or proceed with the existing Patient?</p><br><b>First Name:</b>{{$ctrl.matchedPatient.First_Name__c}}<br><b>Last Name:</b>{{$ctrl.matchedPatient.Last_Name__c}}<br><b>SSN:</b>{{$ctrl.matchedPatient.SSN__c}}<br><b>Date of Birth:</b>{{$ctrl.matchedPatient.Date_Of_Birth__c | date: \'shortDate\'}}<br><b>Race:</b>{{$ctrl.matchedPatient.Race__c}}<br><b>Medical Record Number:</b>{{$ctrl.matchedPatient.Medical_Record_Number__c}}<br></div><div class=col-sm-12 style=text-align:left; ng-show=$ctrl.matchedPatient.Anonymous__c><h1>Possible Match Found:</h1><p>Would you like to create a new Patient or proceed with the existing Patient?</p><br><b>Patient Label:</b>{{$ctrl.matchedPatient.Anonymous_Label__c}}<br><b>Year of Birth:</b>{{$ctrl.matchedPatient.Anonymous_Year_of_Birth__c}}<br></div><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button ladda=$ctrl.loadingPatient class="btn btn-os btn-os-primary" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.createNewPatient(pageForm)><span>Create New Patient</span></button> <button type=button class="btn btn-os btn-os-default" data-style=zoom-in data-spinner-color=White ng-click=$ctrl.createNewCase(matchedPatient)><span>Proceed With the Existing Patient</span></button></div></div></div></div></fieldset>');
$templateCache.put('app/components/adminSidebar/adminSidebar.component.html','<aside id=left-panel class=sidebar><nav><ul class=list-group><li class=list-group-item><a ui-sref=.users><i class="fa fa-user"></i> Users</a></li><li class=list-group-item><a ui-sref=.doctors><i class="fa fa-medkit"></i> Doctors</a></li><li class=list-group-item><a ui-sref=.hospitals><i class="fa fa-hospital-o"></i> Hospitals</a></li><ul></ul></ul></nav></aside>');
$templateCache.put('app/components/caseImplantInfo/caseImplantInfo.component.html','<div class=row-title><h4>Implant</h4><div class=input-group ng-show="implantEntryType==\'$ctrl.manual\'"><span class=input-group-btn><select ng-model=$ctrl.implantEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text id=ProductNumber ng-model=$ctrl.selectedProduct.Name placeholder="Product Number" style=height:38px;z-index:0 uib-typeahead="product as product.Product_Number_and_Name__c for product in $ctrl.products | filter:{Name:$viewValue}" class=form-control typeahead-editable=false typeahead-on-select="$ctrl.onSelectProduct($item, $model, $label)" typeahead-show-hint=true typeahead-min-length=1> <span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="Lot #" style=height:38px;margin-left:-1px;z-index:0 id=lotNumber ng-model=lotNumber> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=month class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in $ctrl.expMonths"></select></span> <span class=input-group-btn><select ng-model=year class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in $ctrl.expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button implant-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.checkAndSaveImplantBarcode();><span>Add Implant</span> <i class="icon icon-c-add"></i></button></span><script>Ladda.bind(\'.input-group-btn button\');</script></div><div class=input-group style=width:800px; ng-show="$ctrl.implantEntryType==\'barcode\'"><span class=input-group-btn><select ng-model=implantEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text class=form-control placeholder="First Barcode" style=height:38px;z-index:0 ng-model=firstbar id=firstbar ng-keypress=$ctrl.isEnterKeyPressed($event)> <span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="Second Barcode" style=height:38px;margin-left:-1px;z-index:0 id=secondbar ng-model=secondbar ng-focus=$ctrl.checkAndSaveImplantBarcode()> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=month class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in expMonths"></select></span> <span class=input-group-btn><select ng-model=year class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in $ctrl.expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button implant-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.checkAndSaveImplantBarcode();><span>Add Implant</span> <i class="icon icon-c-add"></i></button></span><script>Ladda.bind(\'.input-group-btn button\');</script></div></div><div class=col-xs-12><table class="table table-os"><thead><tr><th>Manufacturer</th><th>Product #</th><th>Product Name</th><th>Lot #</th><th>Expiration</th><th>Status</th><th class=action>Delete</th></tr></thead><tbody><tr ng-repeat="implant in $ctrl.event.Implant_Components__r"><td>{{implant.Manufacturer__c}}</td><td>{{implant.Product_Catalog_Id__r.Name}}</td><td>{{implant.Product_Catalog_Id__r.Product_Name__c}}</td><td>{{implant.Lot_Number__c}}</td><td>{{implant.Expiration_Date__c | date: \'MM/yyyy\'}}</td><td><div class=select><select><option>{{implant.Status__c}}</option></select></div></td><td class=action><button type=button class="btn btn-os" ng-click=$ctrl.deleteImplant(implant)><i class="fa fa-remove"></i></button></td></tr></tbody></table></div>');
$templateCache.put('app/components/caseNotes/addCaseNote.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=$ctrl.cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>New Note</h4></div><div class=modal-body style=margin:25px;><form class=form-horizontal><div class=form-group><div class=input-group><span class=input-group-addon>Title</span> <input type=text class=form-control ng-model=$ctrl.caseTitle></div></div><div class=form-group><div class=input-group><span class=input-group-addon>Note</span> <textarea class=form-control rows=5 ng-model=$ctrl.caseNote></textarea></div></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=white ng-click=$ctrl.addNote()><span>Save</span></button></div><script>Ladda.bind(\'.modal-footer button\');</script>');
$templateCache.put('app/components/caseNotes/caseNote.component.html','<div class="panel panel-default"><div class=panel-body><span class=date-stamp>{{$ctrl.note.CreatedDate| date: \'shortDate\'}}: {{$ctrl.note.Title}}</span><p>{{$ctrl.note.Body}}</p><div class=pull-right><button type=button class="btn btn-os" ng-click=$ctrl.editNote($ctrl.note)><i class="fa fa-pencil"></i></button> <button type=button class="btn btn-os" ng-click=$ctrl.deleteNote($ctrl.note)><i class="fa fa-remove"></i></button></div></div></div>');
$templateCache.put('app/components/caseNotes/caseNotes.component.html','<div class=row-title><h4>Notes</h4><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.addNewCaseNote()><span>Add Note</span> <i class="icon icon-c-add"></i></button></div><div class=col-xs-12><div class=row><div class="col-sm-10 col-sm-offset-1 col-xs-12" ng-repeat="note in $ctrl.notes | orderBy: \'-CreatedDate\'"><os-case-note note=note></os-case-note></div></div></div>');
$templateCache.put('app/components/caseNotes/editCaseNote.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>Update Note</h4></div><div class=modal-body style=margin:25px;><form class=form-horizontal><div class=form-group><div class=input-group><span class=input-group-addon>Title</span> <input type=text class=form-control ng-model=caseTitle></div></div><div class=form-group><div class=input-group><span class=input-group-addon>Note</span> <textarea class=form-control rows=5 ng-model=caseNote></textarea></div></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=white ng-click=updateNote()><span>Save</span></button></div><script>Ladda.bind(\'.modal-footer button\');</script>');
$templateCache.put('app/components/caseSensorInfo/caseSensorInfo.component.html','<form name=sensorForm><div class=row-title><h4>Sensor</h4><div class=input-group style=width:500px ng-show="$ctrl.sensorEntryType==\'barcode\'"><span class=input-group-btn><select ng-model=$ctrl.sensorEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text class=form-control placeholder=Barcode style=height:38px;z-index:0 id=sensorCode ng-model=$ctrl.sensorCode ng-keypress=$ctrl.isEnterKeyPressedSensor($event)> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button sensor-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.addSensor();><span>Add Sensor</span> <i class="icon icon-c-add"></i></button></span></div><div ng-messages=sensorForm.serialNumber.$error ng-if=$ctrl.sensorForm.submitted class=input-group style=width:180px><span class=help-block style=color:#a94442 ng-message=required><i class="fa fa-exclamation-triangle"></i> SN is required.</span> <span class=help-block style=color:#a94442 ng-message=pattern><i class="fa fa-exclamation-triangle"></i> SN should be 9 characters.</span></div><div class=input-group ng-show="$ctrl.sensorEntryType==\'manual\'"><div ng-include="\'app/components/caseSensorInfo/caseSensorInfoKeyboard.component.html\'"></div></div></div></form><os-case-sensor-items event=$ctrl.event></os-case-sensor-items>');
$templateCache.put('app/components/caseSensorInfo/caseSensorInfoKeyboard.component.html','<div style=width:800px;><span class=input-group-btn><select ng-model=$ctrl.sensorEntryType class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0;margin-right:20px><option value=barcode>Barcode</option><option value=manual>Keyboard</option></select></span> <input type=text id=REF ng-model=selectedRef.Name placeholder=REF style=height:38px;z-index:0 uib-typeahead="sensorRef as sensorRef.Name for sensorRef in $ctrl.sensorRefs | filter:{Name:$viewValue}" class=form-control typeahead-editable=false typeahead-on-select="$ctrl.onSelectRef($item, $model, $label)" typeahead-show-hint=true typeahead-min-length=1> <span class=input-group-btn style=width:0px;></span><div class=form-group ng-class="{ \'has-error\' : sensorForm.serialNumber.$invalid && sensorForm.submitted}"><input type=text class=form-control placeholder=SN style=height:38px;margin-left:-1px;z-index:0 id=serialNumber name=serialNumber ng-model=$ctrl.serialNumber required pattern=.{9,9}></div><span class=input-group-btn style=width:0px;></span> <input type=text class=form-control placeholder="LOT #" style=height:38px;margin-left:-1px;z-index:0 id=slotNumber ng-model=$ctrl.slotNumber> <span class=input-group-btn style=width:0px;></span> <span class=input-group-btn><select ng-model=$ctrl.smonth class=form-control style="background-color:white;height:38px;margin-left:-2px; width:110px;z-index:0" ng-options="currMonth.key as currMonth.value for currMonth in $ctrl.expMonths"></select></span> <span class=input-group-btn><select ng-model=syear class=form-control style=background-color:white;height:38px;margin-left:-2px;width:110px;z-index:0 ng-options="currYear.key as currYear.value for currYear in $ctrl.expYears"></select></span> <span class=input-group-btn><button type=button class="btn btn-os btn-os-primary ladda-button sensor-btn" data-style=zoom-in data-spinner-color=DeepSkyBlue style=margin-top:0px;z-index:0 ng-click=$ctrl.addSensor();><span>Add Sensor</span> <i class="icon icon-c-add"></i></button></span></div>');
$templateCache.put('app/components/caseSensorInfo/caseSensorItems.component.html','<div class=col-xs-12><table class="table table-os"><thead><tr><th>Device ID</th><th>Manufacturer</th><th>Lot #</th><th>Model</th><th>Expiration</th><th>Year Assembled</th><th class=action>Delete</th></tr></thead><tbody><tr ng-repeat="sensor in $ctrl.event.OS_Device_Events__r"><td>{{sensor.OS_Device__r.Device_ID__c}}</td><td>{{sensor.OS_Device__r.Manufacturer_Name__c}}</td><td>{{sensor.OS_Device__r.Lot_Number__c}}</td><td>{{sensor.OS_Device__r.Style__c}}</td><td>{{sensor.OS_Device__r.Expiration_Date__c | date: \'MM/yyyy\'}}</td><td>{{sensor.OS_Device__r.Year_Assembled__c}}</td><td class=action><button type=button class="btn btn-os" ng-click=$ctrl.deleteSensor(sensor)><i class="fa fa-remove"></i></button></td></tr></tbody></table></div>');
$templateCache.put('app/components/editPatient/editPatient.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>Edit Patient</h4></div><div class=modal-body><form class=form-horizontal name=pageForm><div class=row><div class="col-xs-12 col-md-5"><fieldset><div class=form-group><label for=new-patient-anonymous class="col-sm-4 control-label">Is Anonymous</label><div class=col-sm-8><input type=checkbox id=new-patient-anonymous class=show style=height:20px; name=Anonymous ng-model=patient.anonymous></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.Label.$invalid && pageForm.submitted}" ng-show=patient.anonymous><label for=new-patient-label class="col-sm-4 control-label required">Patient Label</label><div class=col-sm-8><input type=text class=form-control id=new-patient-label name=Label ng-model=patient.Label ng-required=patient.anonymous><div ng-messages=pageForm.Label.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Label is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.FirstName.$invalid && pageForm.submitted}" ng-show=!patient.anonymous><label for=new-patient-first-name class="col-sm-4 control-label required">First Name</label><div class=col-sm-8><input type=text class=form-control id=new-patient-first-name name=FirstName ng-model=patient.FirstName ng-required=!patient.anonymous><div ng-messages=pageForm.FirstName.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient First Name is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.LastName.$invalid && pageForm.submitted}" ng-show=!patient.anonymous><label for=new-patient-last-name class="col-sm-4 control-label required">Last Name</label><div class=col-sm-8><input type=text class=form-control id=new-patient-last-name name=LastName ng-model=patient.LastName ng-required=!patient.anonymous><div ng-messages=pageForm.LastName.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Last Name is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.newPatientSex.$invalid && pageForm.submitted}"><label for=new-sex-name class="col-sm-4 control-label required">Sex</label><div class=col-sm-8><input type=radio name=newPatientSex id=new-patient-sex-male value=M ng-model=patient.Gender required><label for=new-patient-sex-male>Male</label> <input type=radio name=newPatientSex id=new-patient-sex-female value=F ng-model=patient.Gender required><label for=new-patient-sex-female>Female</label><div ng-messages=pageForm.newPatientSex.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Sex is required.</p></div></div></div></fieldset><fieldset><div class=form-group ng-class="{ \'has-error\' : pageForm.dateOfBirthString.$invalid && pageForm.submitted}" ng-if=!patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birthdate</label><div class=col-sm-8><input type=text class=form-control name=dateOfBirthString placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=patient.dateOfBirthString is-open=open.birthDate datepicker-options=dateOptions ng-required=!patient.anonymous close-text=Close ng-click="open($event,\'birthDate\')"> <button type=button class="btn btn-os" ng-click="open($event,\'birthDate\')"><i class="fa fa-calendar"></i></button><div ng-messages=pageForm.dateOfBirthString.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birthdate is required.</p></div></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.BirthYear.$invalid && pageForm.submitted}" ng-show=patient.anonymous><label for=new-patient-dob class="col-sm-4 control-label required">Birth Year</label><div class=col-sm-2><div class=select><select id=patient-birth-year ng-model=patient.BirthYear ng-required=patient.anonymous name=BirthYear style=padding-left:8px;><option ng-repeat="year in birthYears" value={{year}}>{{year}}</option></select></div><div ng-messages=pageForm.BirthYear.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Birth Year is required.</p></div></div></div></fieldset><fieldset><div class=form-group><label for=new-patient-race class="col-sm-4 control-label">Race</label><div class=col-sm-8><div class=select><select id=new-patient-race ng-model=patient.Race><option>White</option><option>Black or African American</option><option>American Indian</option><option>Asian</option><option>Native Hawaiian</option><option>Hispanic</option><option>Other</option></select></div></div></div><div class=form-group><label for=new-patient-language class="col-sm-4 control-label">Language</label><div class=col-sm-8><div class=select><select id=new-patient-language ng-model=patient.Language><option>English</option><option>Spanish</option><option>French</option><option>Chinese</option><option>Creole</option><option>Unknown</option><option>Russian</option><option>Portuguese</option></select></div></div></div></fieldset></div><div class="col-xs-12 col-md-7"><fieldset><div class=form-group><label for=new-patient-number class="col-sm-4 control-label">Patient #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-number ng-model=patient.PatientNumber></div></div><div class=form-group ng-class="{ \'has-error\' : pageForm.MedicalRecordNumber.$invalid && pageForm.submitted}"><label for=new-patient-medical-record-number class="col-sm-4 control-label" ng-class="!patient.anonymous?\'required\':\'\'">Medical Record #</label><div class=col-sm-8><input type=text ng-required=!patient.anonymous name=MedicalRecordNumber class=form-control id=new-patient-medical-record-number ng-model=patient.MedicalRecordNumber><div ng-messages=pageForm.MedicalRecordNumber.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Patient Medical Record # is required.</p></div></div></div><div class=form-group><label for=new-patient-account-number class="col-sm-4 control-label">Account #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-account-number ng-model=patient.AccountNumber></div></div><div class=form-group><label for=new-patient-ss-number class="col-sm-4 control-label">Social Security #</label><div class=col-sm-8><input type=text class=form-control id=new-patient-ss-number ng-model=patient.SocialSecurityNumber></div></div></fieldset><fieldset></fieldset></div></div></form></div><div class=modal-footer ng-if=!matchedPatient><button type=button class="btn btn-os btn-os-primary" ng-click=cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=White ng-click=updatePatient()><span>Save</span></button><script>Ladda.bind(\'.modal-footer button\');</script></div>');
$templateCache.put('app/components/editProcedure/editProcedure.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=$ctrl.cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>Edit Procedure</h4></div><div class=modal-body><form class=form-horizontal><div class=form-group><label class="col-sm-3 control-label">Laterality</label><div class=col-sm-8><input type=radio name=laterality id=laterality-left value=Left ng-model=$ctrl.procedureData.laterality><label for=laterality-left>Left</label> <input type=radio name=laterality id=laterality-right value=Right ng-model=$ctrl.procedureData.laterality><label for=laterality-right>Right</label></div></div><div class=form-group><label for=case-surgeon class="col-sm-3 control-label">Surgeon</label><div class=col-sm-8><div class=select><select id=case-surgeon ng-model=$ctrl.procedureData.physician><option ng-repeat="surgeon in $ctrl.surgeons" value={{surgeon.id}}>{{surgeon.name}}</option></select></div></div></div><div class=form-group><label for=new-case-procedure-date class="col-sm-4 control-label">Procedure Date</label><div class=col-sm-8><input type=text class=form-control placeholder=MM/DD/YYYY style="background-color:white; width:150px;height:38px;margin-left:-2px;" datetime-picker=MM/dd/yyyy enable-time=false ng-model=$ctrl.procedureData.procedureDateString is-open=$ctrl.popup.opened datepicker-options=$ctrl.dateOptions ng-required=true close-text=Close ng-click=$ctrl.open()> <button type=button class="btn btn-os" ng-click=$ctrl.open()><i class="fa fa-calendar"></i></button></div></div><div class=form-group><label for=case-type class="col-sm-3 control-label">Anesthesia Type</label><div class=col-sm-8><div class=select><select id=anesthesia-type ng-model=$ctrl.procedureData.anesthesiaType><option value=General>General</option><option value=Epidural>Epidural</option><option value=Other>Other</option></select></div></div></div><div class=form-group><label class="col-sm-3 control-label">Consent</label><div class=col-sm-8><input type=radio name=consent id=consent-yes value=1 ng-model=$ctrl.procedureData.patientConsentObtained><label for=consent-yes>Yes</label> <input type=radio name=consent id=consent-no value=0 ng-model=$ctrl.procedureData.patientConsentObtained><label for=consent-no>No</label></div></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=White ng-click=$ctrl.updateProcedure()><span>Update Procedure</span></button></div><script>Ladda.bind(\'.modal-footer button\');</script>');
$templateCache.put('app/components/hospitalSelector/hospitalSelector.component.html','<div class=modal-header><button type=button class="btn btn-os close" aria-label=Close ng-click=$ctrl.cancel()><i class="icon icon-close"></i></button><h4 class=modal-title>{{$ctrl.title}}</h4></div><div class=modal-body><form class=form-horizontal name=pageForm><div class=row><fieldset><div class=form-group ng-class="{ \'has-error\' : pageForm.hospital.$invalid && pageForm.submitted}" ng-if=$ctrl.isUserAdmin><label for=hospital class="col-sm-4 control-label required">Hospital</label><div class=col-sm-6><div class=select><select id=hospital ng-model=$ctrl.hospital required name=hospital ng-options="hospital.name for hospital in $ctrl.hospitals track by hospital.id"></select></div><div ng-messages=pageForm.hospital.$error ng-if=pageForm.submitted><p class=help-block ng-message=required><i class="fa fa-exclamation-triangle"></i> Hospital selection is required.</p></div></div></div><div class=form-group ng-if=!$ctrl.isUserAdmin><div class=text-center><h4 ng-model=$ctrl.hospital>{{$ctrl.hospital.Name}}</h4></div></div><div class=form-group ng-show="$ctrl.hospital && $ctrl.practices.length>0"><label for=practice class="col-sm-4 control-label">Practice</label><div class=col-sm-6><div class=select><select id=practice ng-model=$ctrl.practice required name=practice><option value>Not Selected</option><option ng-repeat="practice in $ctrl.practices | filter: {ParentId:$ctrl.hospital.id}" value={{practice.id}}>{{practice.name}}</option></select></div></div></div></fieldset></div></form></div><div class=modal-footer><button type=button class="btn btn-os btn-os-primary" ng-click=$ctrl.cancel()><span>Cancel</span></button> <button type=button class="btn btn-os btn-os-default ladda-button" data-style=zoom-in data-spinner-color=White ng-click=$ctrl.setHospitalFilter()><span>Set</span></button><script>Ladda.bind(\'.modal-footer button\');</script></div>');
$templateCache.put('app/components/hospitalSurgeonSelector/hospitalSurgeonSelector.component.html','<span class=pull-right ng-if=$ctrl.isHospital><select ng-options="surgeon.name for surgeon in $ctrl.surgeons" name=surgeon ng-model=surgeon value=surgeon ng-disabled="practice == \'All\'" class="form-control input-sm" placeholder=Surgeon ng-change=$ctrl.getSurgeonPracticeActivity(surgeon.id)></select></span> <span class=pull-right ng-if=$ctrl.isHospital><select ng-options="practice.Name for practice in $ctrl.practices" name=practice ng-model=practice value=practice class="form-control input-sm" placeholder=Practice ng-change=$ctrl.getPracticeActivity(practice.id)></select></span>');
$templateCache.put('app/components/patientDetailInfo/patientDetailBasicInfo.component.html','<h1 ng-show=!patient.Anonymous__c>{{$ctrl.patient.First_Name__c}} {{$ctrl.patient.Last_Name__c}}</h1><h1 ng-show=patient.Anonymous__c>{{$ctrl.patient.Anonymous_Label__c}} (Anonymous)</h1><div class=form-group><label class=control-label>Sex</label> <span class=value>{{$ctrl.patient.Gender__c}}</span> <label class=control-label>DOB</label> <span class=value ng-show=!patient.Anonymous__c>{{$ctrl.patient.Date_Of_Birth__c | date: \'shortDate\'}}</span> <span class=value ng-show=patient.Anonymous__c>{{$ctrl.patient.Anonymous_Year_of_Birth__c}}</span> <label class=control-label>Patient #</label> <span class=value>{{$ctrl.patient.Source_Record_Id__c}}</span> <button type=button class="btn btn-os" ng-click=$ctrl.togglePatientDetailsDrawer()><i class="icon icon-c-arrow-down"></i></button></div>');
$templateCache.put('app/components/patientDetailInfo/patientDetailInfo.component.html','<div class=patient-details><div hidden>{{$ctrl.patient.Id}}</div><h1 ng-show=!$ctrl.patient.Anonymous__c>{{$ctrl.patient.firstName}} {{$ctrl.patient.lastName}}</h1><h1 ng-show=$ctrl.patient.Anonymous__c>{{$ctrl.patient.anonymousLabel}} (Anonymous)</h1><div class=form-group><label class=control-label>Sex</label> <span class=value>{{$ctrl.patient.gender}}</span> <span style=padding-right:10px;padding-left:14px;><label class=control-label>DOB</label> <span class=value ng-show=!patient.Anonymous__c>{{$ctrl.patient.dateOfBirthString | date: \'shortDate\'}}</span></span> <span class=value ng-show=patient.Anonymous__c>{{$ctrl.patient.birthYear}}</span> <span style=padding-right:104px;><label class=control-label>Patient #</label> <span class=value>{{$ctrl.patient.patientNumber}}</span></span> <span style=padding-right:14px;><label class=control-label>Email</label> <span class=value>{{$ctrl.patient.email}}</span></span> <button type=button class="btn btn-os" ng-click=$ctrl.togglePatientDetailsDrawer()><i class="icon icon-c-arrow-down"></i></button></div></div><div class=drawer ng-class="{\'open\': $ctrl.patientDetailsVisible}"><div class=row><div class="col-sm-11 col-sm-offset-1"><div class=row><div class=col-sm-12><div class="form-horizontal tight-vertically"><div class=col-sm-3><div class=form-group><label class="col-sm-6 col-md-5 control-label">Language</label><div class="col-sm-6 col-md-7 value">{{$ctrl.patient.language}}</div></div></div><div class=col-sm-3><div class=form-group><label class="col-sm-7 col-md-6 control-label" for=MedicalRecordNumber>Medical Record #</label><div id=medicalRecordNumber class="col-sm-5 col-md-6 value">{{$ctrl.patient.medicalRecordNumber}}</div></div></div><div class=col-sm-3><div class=form-group><label class="col-sm-6 col-md-5 control-label">Account #</label><div class="col-sm-6 col-md-7 value">{{$ctrl.patient.accountNumber}}</div></div></div><div class=col-sm-3><a class=pull-right style=margin-right:15px; ng-click=$ctrl.editPatient()><span>Edit</span> <i class="icon icon-edit"></i></a></div></div></div></div></div></div></div>');
$templateCache.put('app/components/procedureInfo/procedureInfo.component.html','<div class="row row-separator form-horizontal tight-vertically"><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Date of Surgery</label><div class="col-sm-8 value">{{$ctrl.procedure.Appointment_Start__c | date:\'shortDate\'}}</div></div><div class=form-group><label class="col-sm-4 control-label">Surgeon</label><div class="col-sm-8 value">{{$ctrl.procedure.Physician__r.Name}}</div></div></div><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Procedure</label><div class="col-sm-8 value">{{$ctrl.procedure.Case__r.Case_Type__r.Description__c}}</div></div><div class=form-group><label class="col-sm-4 control-label">Laterality</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Laterality__c}}</span></div></div></div><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Patient Consent</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Patient_Consent_Obtained__c?\'Yes\':\'No\'}}</span></div></div><div class=form-group><label class="col-sm-4 control-label">Anesthesia</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Anesthesia_Type__c}}</span></div></div></div></div>');
$templateCache.put('app/components/surveyLinkQRCode/surveyLinkQRCode.modal.html','<div class=modal-header><h4 class=modal-title>Survey Link</h4></div><div class=modal-body><div class=text-center><qrcode version=10 data={{vm.surveyURL}} error-correction-level=L size=360></qrcode></div><div class="col-sm-12 text-center"><div style=padding-top:12px;><a href={{vm.surveyURL}} target=_blank style=font-size:1.4em;>Link to Survey</a></div></div></div><div class=modal-footer><button class="btn btn-os btn-os-primary" ng-click=vm.close()>Close</button></div>');
$templateCache.put('app/components/surveyOutcomeData/surveyChartData.component.html','<div class=toggle-view-container><div id=outcomes-chart class="toggle-view chart" ng-show="$ctrl.toggleValue===\'chart\'"><div class="row chart"><div id=kss-chart class=chart-svg></div></div></div><div id=outcomes-data class="toggle-view chart" ng-show="$ctrl.toggleValue===\'data\'"><div class=row><div class=col-xs-12><br><br><table class="table table-os"><thead><tr><th style=font-size:14px>Question</th><th style=font-size:14px ng-repeat="category in $ctrl.chartCategories track by $index">{{category}}</th></tr></thead><tbody><tr ng-repeat="items in $ctrl.chartData"><td ng-repeat="item in items track by $index">{{item}}</td></tr></tbody></table></div></div></div></div>');
$templateCache.put('app/components/surveysToTake/surveysToTake.component.html','<div class="row row-separator"><div class=row-title><h4>Survey</h4></div><div class=col-xs-12><div class=row><div ng-repeat="survey in surveys"><div class="panel paneldefault"><div class=panel-body><i class="fa fa-file"></i> <span>{{survey.Name__c}}</span> <button type=button class="btn btn-os btn-os-primary" ng-click=openSurveyLinkQRCode(escapeURL(survey.Survey_URL__c))><span>Take Survey</span> <i class="icon icon-c-add"></i></button> <button type=button class="btn btn-os btn-os-primary" ng-click=sendSurveyLinkQRCode(escapeURL(survey.Survey_URL__c))><span>Send Survey</span> <i class="icon icon-c-add"></i></button></div></div></div></div></div></div>');
$templateCache.put('app/components/takeSurvey/takeSurvey.component.html','<div class="row row-separator"><div class=row-title><h4>Survey</h4></div><div class=col-xs-12><div class=row><div class="col-xs-12 col-sm-2 col-sm-offset-0" ng-repeat="survey in $ctrl.surveys"><div class="panel panel-default"><div class="panel-body text-center"><div style="vertical-align: middle;"><h5>{{survey.Name__c}}</h5></div><div class><div style="vertical-align: middle;"><span class="text-center completedSurvey" ng-show="survey.Completed==true">Completed: {{survey.EndDate | date:\'shortDate\'}}</span></div></div><div class><button type=button class="btn btn-os btn-info btn-block" style=margin-bottom:6px; ng-click=$ctrl.sendQRCode($ctrl.escapeURL(survey.Survey_URL__c))><span>Send Survey</span> <i class="fa fa-send"></i></button></div><div class><button type=button class="btn btn-os btn-os-primary btn-block" style=margin-bottom:6px; ng-click=$ctrl.openQRCode($ctrl.escapeURL(survey.Survey_URL__c))><span>{{survey.Completed==true ? \'Retake Survey\' : \'Take Survey\'}}</span> <i class="fa fa-tasks"></i></button></div></div></div></div></div></div></div>');
$templateCache.put('app/caseDetails/caseInteroperativeData/caseImages.component.html','<div class="row row-separator dark-theme" ng-show=$ctrl.showImages><div class="row-title getData"><h4>Intraoperative Data</h4><button type=button class="btn btn-os btn-os-primary ladda-button retrieve-btn" style=z-index:0 ng-click=$ctrl.getData() data-style=zoom-in data-spinner-color=DeepSkyBlue><span>Retrieve Data</span> <i class="icon icon-download-cloud"></i></button></div><script>Ladda.bind(\'.getData button\');</script><div class=col-xs-12><div class="row row-separator"><div class="col-sm-6 image-card" style=width:340px ng-repeat="line in $ctrl.sensorDataLines | orderBy : \'SD_MeasurementDate\'"><div class=header><span class=timne-stamp>{{line.SD_MeasurementDate}}</span> <button type=button class="btn btn-os" ng-click=$ctrl.deleteSensorImage(line)><span>Delete Capture</span> <i class="fa fa-remove"></i></button></div><a href="/servlet/servlet.FileDownload?file={{line.AttachmentID}}" download=ImageName title=ImageName><img class=img-responsive src="/servlet/servlet.FileDownload?file={{line.AttachmentID}}" height=160 width=340></a><div class=details><div class="block degrees"><span class=measure-value>{{line.SD_Type_ActualFlex}}</span></div><div class="block lsb-med"><span class=measure-value>{{line.SD_Type_MedialLoad}}</span><span class=measure>lbs Med</span></div><div class="block lbs-lat"><span class=measure-value>{{line.SD_Type_LateralLoad}}</span><span class=measure>lbs Lat</span></div><div class="block rotation"><span class=measure-value>{{line.SD_Type_Rotation}}</span></div></div></div></div></div></div>');
$templateCache.put('app/caseDetails/caseInteroperativeData/caseInteroperativeData.component.html','<div class="row row-separator dark-theme" ng-show="$ctrl.event.OS_Device_Events__r.length > 0"><div class="row-title getData"><h4>Intraoperative Data</h4><button type=button class="btn btn-os btn-os-primary" ladda=$ctrl.loading ng-click=$ctrl.getLinkStationData()><span>Retrieve Data</span> <i class="icon icon-download-cloud"></i></button></div><div class=col-xs-12><div class="row row-separator"><div class="col-sm-6 image-card" style=width:340px ng-repeat="line in $ctrl.sensorDataLines | orderBy : \'SD_MeasurementDate\'"><div class=header><span class=time-stamp>{{$ctrl.line.SD_MeasurementDate}}</span> <button type=button class="btn btn-os" ng-click=$ctrl.deleteSensorImage(line)><span>Delete Capture</span> <i class="fa fa-remove"></i></button></div><a href="/servlet/servlet.FileDownload?file={{line.AttachmentID}}" download=ImageName title=ImageName><img class=img-responsive src="/servlet/servlet.FileDownload?file={{line.AttachmentID}}" height=160 width=340></a><div class=details><div class="block degrees"><span class=measure-value>{{line.SD_Type_ActualFlex}}</span></div><div class="block lsb-med"><span class=measure-value>{{line.SD_Type_MedialLoad}}</span><span class=measure>lbs Med</span></div><div class="block lbs-lat"><span class=measure-value>{{line.SD_Type_LateralLoad}}</span><span class=measure>lbs Lat</span></div><div class="block rotation"><span class=measure-value>{{line.SD_Type_Rotation}}</span></div></div></div></div></div></div>');
$templateCache.put('app/caseDetails/procedureInfo/procedureInfo.component.html','<div class=row><div class=row-title><h3>Procedure</h3><div class=actions><button type=button class="btn btn-os btn-os-primary ladda-button" style=z-index:0 data-style=zoom-in data-spinner-color=DodgerBlue ng-click=$ctrl.completeEvent($ctrl.event.Id) ng-show="$ctrl.event.Status__c != \'Complete\'"><span>Complete Procedure</span> <i class="icon icon-checkmark"></i></button> <button type=button class="btn btn-os btn-os-primary" style=z-index:0 ng-show="$ctrl.event.Status__c != \'Complete\'" ng-click=$ctrl.editProcedure($ctrl.event,$ctrl.case,$ctrl.clinicalData)><span>Edit</span> <i class="fa fa-pencil"></i></button></div><script>\n            Ladda.bind(\'.actions button\');\n        </script></div></div><div class="row row-separator form-horizontal tight-vertically"><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Date of Surgery</label><div class="col-sm-8 value">{{$ctrl.procedure.Appointment_Start__c | date:\'shortDate\'}}</div></div><div class=form-group><label class="col-sm-4 control-label">Surgeon</label><div class="col-sm-8 value">{{$ctrl.procedure.Physician__r.Name}}</div></div></div><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Procedure</label><div class="col-sm-8 value">{{$ctrl.procedure.Case__r.Case_Type__r.Description__c}}</div></div><div class=form-group><label class="col-sm-4 control-label">Laterality</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Laterality__c}}</span></div></div></div><div class=col-sm-4><div class=form-group><label class="col-sm-4 control-label">Patient Consent</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Patient_Consent_Obtained__c?\'Yes\':\'No\'}}</span></div></div><div class=form-group><label class="col-sm-4 control-label">Anesthesia</label><div class=col-sm-8><span class=value>{{$ctrl.clinicalData.Anesthesia_Type__c}}</span></div></div></div></div>');
$templateCache.put('app/dashboard/caseActivityChart/caseActivityChart.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title><span ng-show=$ctrl.isUserAdmin>{{$ctrl.title}}</span> <span ng-show=!$ctrl.isUserAdmin>{{$ctrl.title}} - {{$ctrl.practice.name}}</span></div></div><div class=panel-body><nav class="navbar navbar-default" ng-show="$ctrl.showSurgeonList || $ctrl.showPracticeList || $ctrl.isUserAdmin"><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listHospitals>Hospital</label>&nbsp;<select id=listHospitals ng-options="hospital.name for hospital in $ctrl.hospitals" name=hospital ng-model=$ctrl.hospital value=hospitals class="form-control input-sm" placeholder=Hospital ng-change=$ctrl.setHospital(hospital.id)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showPracticeList><div class="form-group form-group-sm"><label for=listPractices>Practice</label><select id=listPractices ng-options="practice.name for practice in $ctrl.practices | filter: {parentId: $ctrl.hospital.id}" name=practice ng-model=$ctrl.practice value=practices class="form-control input-sm" placeholder=Practice ng-change=$ctrl.getPracticeData()></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showSurgeonList><div class=form-group><label for=listSurgeons class=col-sm-3 style=padding-top:6px;>Surgeons</label><div class=col-sm-8><select id=listSurgeons ng-options="surgeon.name for surgeon in $ctrl.surgeons | filter: {parentId: $ctrl.practice.id}" name=surgeon ng-model=$ctrl.surgeon value=surgeon ng-disabled="practice == \'All\'" class="form-control input-sm" placeholder=Surgeon ng-change=$ctrl.getData()></select></div></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listMonths>Months</label><select id=listMonths name=month ng-model=$ctrl.monthsToChart value=months class="form-control input-sm" placeholder=months ng-change=$ctrl.getChart()><option value=1>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option><option value=7>7</option><option value=8>8</option><option value=9>9</option><option value=10>10</option><option value=11>11</option><option value=12>12</option></select></div></form></nav><div class=row><nvd3 options=$ctrl.options data=$ctrl.data api=api interactive=true ui-sref=reports.caseActivityChart></nvd3></div></div></div>');
$templateCache.put('app/dashboard/meanChangeInProm/meanChangeInPromScore.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title><span ng-show=$ctrl.isUserAdmin>{{$ctrl.title}}</span> <span ng-show=!$ctrl.isUserAdmin>{{$ctrl.title}}</span></div></div><div class=panel-body><nav class="navbar navbar-default" ng-show="$ctrl.showSurgeonList || $ctrl.showPracticeList || $ctrl.isUserAdmin"><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listHospitals>Hospital</label><select id=listHospitals ng-options="hospital.name for hospital in $ctrl.hospitals" name=hospital ng-model=$ctrl.hospital value=hospitals class="form-control input-sm" placeholder=Hospital ng-change=$ctrl.setHospital(hospital.id)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showPracticeList><div class="form-group form-group-sm"><label for=listPractices>Practice</label><select id=listPractices ng-options="practice.name for practice in $ctrl.practices | filter: {parentId: $ctrl.hospital.id}" name=practice ng-model=$ctrl.practice value=practices class="form-control input-sm" placeholder=Practice ng-change=$ctrl.getPracticeData()></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showSurgeonList><div class="form-group form-group-sm"><label for=listProm>PROM</label><select id=listProm ng-options="prom.name for prom in $ctrl.surveys" name=prom ng-model=$ctrl.prom value=proms class="form-control input-sm" placeholder=Prom ng-change=$ctrl.setProm()></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showSurgeonList><div class="form-group form-group-sm"><label for=listSurgeons>Surgeons</label><select id=listSurgeons ng-options="surgeon.name for surgeon in $ctrl.surgeons | filter: {parentId: $ctrl.practice.id}" name=surgeon ng-model=$ctrl.surgeon value=surgeon ng-disabled="practice == \'All\'" class="form-control input-sm" placeholder=Surgeon ng-change=$ctrl.getSurgeonData()></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listMonths>Months</label><select id=listMonths name=month ng-model=$ctrl.monthsToChart value=months class="form-control input-sm" placeholder=months ng-change=$ctrl.getChart(surgeon)><option>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option><option value=7>7</option><option value=8>8</option><option value=9>9</option><option value=10>10</option><option value=11>11</option><option value=12>12</option></select></div></form></nav><div class=row><nvd3 options=$ctrl.options data=$ctrl.chartData api=api interactive=true ui-sref=reports.promDeltaChart></nvd3></div></div></div>');
$templateCache.put('app/dashboard/meanChangeInProm/meanChangeInPromScoreMini.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title></div></div><div class=panel-body><div class=row><nvd3 options=$ctrl.options data=$ctrl.chartData api=api interactive=false></nvd3></div></div></div>');
$templateCache.put('app/dashboard/patientPhaseCount/patientPhaseCount.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title><span ng-show=$ctrl.isUserAdmin>{{$ctrl.title}}</span> <span ng-show=!$ctrl.isUserAdmin>{{$ctrl.title}} - {{$ctrl.practice.name}}</span></div></div><div class=panel-body><nav class="navbar navbar-default" ng-show="$ctrl.showSurgeonList || $ctrl.showPracticeList || $ctrl.isUserAdmin"><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listHospitals>Hospital</label><select id=listHospitals ng-options="hospital.name for hospital in $ctrl.hospitals" name=hospital ng-model=$ctrl.hospital value=hospitals class="form-control input-sm" placeholder=Hospital ng-change=$ctrl.setHospital(hospital.id)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showPracticeList><div class="form-group form-group-sm"><label for=listPractices>Practice</label><select id=listPractices ng-options="practice.name for practice in $ctrl.practices | filter: {parentId: $ctrl.hospital.id}" name=practice ng-model=$ctrl.practice value=practices class="form-control input-sm" placeholder=Practice ng-change=$ctrl.getChart(practice.id)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showSurgeonList><div class="form-group form-group-sm"><label for=listSurgeons>Surgeons</label><select id=listSurgeons ng-options="surgeon.name for surgeon in $ctrl.surgeons | filter: {parentId: $ctrl.practice.id}" name=surgeon ng-model=$ctrl.surgeon value=surgeon ng-disabled="practice == \'All\'" class="form-control input-sm" placeholder=Surgeon ng-change=$ctrl.getChart(surgeon)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listMonths>Months</label><select id=listMonths name=month ng-model=$ctrl.monthsToChart value=months class="form-control input-sm" placeholder=months ng-change=$ctrl.getChart(surgeon)><option value=1>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option><option value=7>7</option><option value=8>8</option><option value=9>9</option><option value=10>10</option><option value=11>11</option><option value=12>12</option></select></div></form></nav><div class=row><div class=col-sm-12><nvd3 options=$ctrl.options data=$ctrl.data api=api interactive=true ui-sref=reports.phaseCountChart></nvd3></div></div></div></div>');
$templateCache.put('app/dashboard/kneeBalanceData/kneeBalanceData.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title><span ng-show=$ctrl.isUserAdmin>{{$ctrl.title}}</span> <span ng-show=!$ctrl.isUserAdmin>{{$ctrl.title}}</span></div></div><div class=panel-body><nav class="navbar navbar-default" ng-show="$ctrl.showSurgeonList || $ctrl.showPracticeList || $ctrl.isUserAdmin"><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><label for=listHospitals>Hospital</label><div class="form-group form-group-sm"><select id=listHospitals ng-options="hospital.name for hospital in $ctrl.hospitals" name=hospital ng-model=$ctrl.hospital value=hospitals class="form-control input-sm" placeholder=Hospital ng-change=$ctrl.setHospital(hospital.id)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showPracticeList><div class="form-group form-group-sm"><label for=listPractices>Practice</label><select id=listPractices ng-options="practice.name for practice in $ctrl.practices | filter: {parentId: $ctrl.hospital.id}" name=practice ng-model=$ctrl.practice value=practices class="form-control input-sm" placeholder=Practice ng-change=$ctrl.getChart(practice)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.showSurgeonList><div class="form-group form-group-sm"><label for=listSurgeons>Surgeons</label><select id=listSurgeons ng-options="surgeon.name for surgeon in $ctrl.surgeons | filter: {parentId: $ctrl.practice.id}" name=surgeon ng-model=$ctrl.surgeon value=surgeon ng-disabled="practice == \'All\'" class="form-control input-sm" placeholder=Surgeon ng-change=$ctrl.getChart(surgeon)></select></div></form><form class="navbar-form navbar-left" ng-show=$ctrl.isUserAdmin><div class="form-group form-group-sm"><label for=listMonths>Months</label><select id=listMonths name=month ng-model=$ctrl.monthsToChart value=months class="form-control input-sm" placeholder=months ng-change=$ctrl.getChart(surgeon)><option>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option><option value=7>7</option><option value=8>8</option><option value=9>9</option><option value=10>10</option><option value=11>11</option><option value=12>12</option></select></div></form></nav><div class=row><nvd3 options=$ctrl.options data=$ctrl.data api=api interactive=true ui-sref=reports.kneeBalanceChart></nvd3></div></div></div>');
$templateCache.put('app/dashboard/kneeBalanceData/kneeBalanceDataByPatient.component.html','');
$templateCache.put('app/dashboard/kneeBalanceData/kneeBalancePatientCard.component.html','<div class="panel panel-default"><div class=panel-heading><div class=panel-title><a ng-show=!patient.isAnonymous ng-click=$ctrl.patientDetails($ctrl.patient)>{{$ctrl.patient.patientName}}</a> <a ng-show=patient.isAnonymous ng-click=$ctrl.patientDetails($ctrl.patient)>{{$ctrl.patient.anonymousLabel}}</a></div><div class=panel-body><div class=panel-right><label>Procedure Date</label><strong>{{$ctrl.patient.procedureDate|date:\'shortDate\'}}</strong></div><br><table class="table table responsive table-condensed"><thead><tr><th>Flex</th><th>Medial</th><th>Lateral</th></tr></thead><tbody><tr><td>{{$ctrl.tenDegree}}</td><td class=text-center>{{$ctrl.patient.medial10}}</td><td class=text-center>{{$ctrl.patient.lateral10}}</td></tr><tr><td>{{$ctrl.fortyFiveDegree}}</td><td class=text-center>{{$ctrl.patient.medial45}}</td><td class=text-center>{{$ctrl.patient.lateral45}}</td></tr><tr><td>{{$ctrl.ninetyDegree}}</td><td class=text-center>{{$ctrl.patient.medial90}}</td><td class=text-center>{{$ctrl.patient.lateral90}}</td></tr></tbody></table></div></div></div>');
$templateCache.put('app/dashboard/kneeBalanceData/kneeBalancePatientCards.component.html','<div class=row><div ng-repeat="patient in $ctrl.patients" class="col-sm-3 col-xs-12"><os-knee-balance-patient-card patient=patient></os-knee-balance-patient-card></div></div>');
$templateCache.put('app/dashboard/patientsUnderBundle/patientsUnderBundle.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title>{{$ctrl.title}}</div></div><div class=panel-body style="height: 300px; overflow: auto;"><table class="table table-condensed"><thead><tr><th>Name</th><th>DOB</th><th>Procedure Date</th><th>Physician</th><th>Laterality</th></tr></thead><tbody ng-repeat="patient in $ctrl.patients | orderBy: \'procedureDate\'"><tr><td><a ng-show=!patient.isAnonymous ng-click=$ctrl.patientDetails(patient)>{{patient.lastName}}, {{patient.firstName}}</a> <a ng-show=patient.isAnonymous ng-click=$ctrl.patientDetails(patient)>{{patient.anonymousLabel}}</a></td><td ng-show=!patient.isAnonymous>{{patient.dateOfBirth | date: \'shortDate\'}}</td><td ng-show=patient.isAnonymous>{{patient.birthYear}}</td><td>{{patient.procedureDate | date: \'shortDate\'}}</td><td>{{patient.surgeonName}}</td><td>{{patient.laterality}}</td></tr></tbody></table></div></div>');
$templateCache.put('app/dashboard/patientsOutOfCompliance/patientsOutOfCompliance.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=panel-title><span>{{$ctrl.title}}</span></div></div><div class=panel-body><table class="table table-responsive table-condensed"><thead><tr><th>Patient Name</th><th>Phase</th><th>Issue</th><th>Last Notification</th><th>Action</th></tr></thead><tbody ng-repeat="patient in $ctrl.patientsOutOfCompliance"><tr><td>{{patient.patientName}}</td><td>{{patient.phaseName}}</td><td>{{patient.issue}}</td><td>{{patient.lastContactDate | date: \'shortDate\'}}</td><td><a ng-show="patient.issue==\'Survey Missing\'" class="btn btn-primary btn-sm">Send</a> <a ng-show="patient.issue==\'Incomplete\'" class="btn btn-warning btn-sm">Complete</a></td></tr></tbody></table></div></div>');
$templateCache.put('app/dashboard/promDeltaList/patientKoosCard.component.html','<div class="panel panel-default"><div class=panel-heading><div class=panel-title>{{$ctrl.patientName}}</div><div class=panel-body>Procedure Date: {{$ctrl.procedureDate | date: \'shortDate\'}}<table class="table table-condensed"><thead><tr><th>Category</th><th>Symptom</th><th></th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table><os-mean-change-in-prom-score-mini></os-mean-change-in-prom-score-mini></div></div></div>');
$templateCache.put('app/dashboard/promDeltaList/patientsKoosScoresList.component.html','<section class="panel panel-default"><div class=panel-body><table class="table table-responsive"><thead><tr><th>Patient</th><th>Phase</th><th>Symptom</th><th>Pain</th><th>ADL</th><th>Sport/Rec</th><th>QOL</th></tr></thead><tbody><tr ng-repeat="prom in $ctrl.patientsScores"><td>{{prom.patientName}}</td><td>{{prom.phase}}</td><td>{{prom.symptom}}</td><td>{{prom.pain}}</td><td>{{prom.aDL}}</td><td>{{prom.sportRec}}</td><td>{{prom.qOL}}</td></tr></tbody></table></div></section>');
$templateCache.put('app/dashboard/promDeltaList/promByPatientCards.component.html','<div class=row><div class=col-sm-12><div class=row><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table><os-mean-change-in-prom-score-mini></os-mean-change-in-prom-score-mini></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table><os-mean-change-in-prom-score-mini></os-mean-change-in-prom-score-mini></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table><os-mean-change-in-prom-score></os-mean-change-in-prom-score></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body>Procedure Date: 1/30/2017 Pre-op<table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body><table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table></div></div></div></div><div class=col-sm-4><div class="panel panel-default"><div class=panel-heading><div class=panel-title>Patient Name</div><div class=panel-body><table class="table table-condensed"><thead><tr><th>Category</th><th>Pre-Op</th><th>4-8 Week</th></tr></thead><tbody><tr><td>KOOS Sympton</td><td>56</td><td>65</td></tr><tr><td>KOOSPain</td><td>42</td><td>54</td></tr></tbody><tr><td>KOOS Sport/Red</td><td>77</td></tr></table></div></div></div></div></div></div></div>');
$templateCache.put('app/dashboard/promDeltaList/promDeltaList.component.html','<section class="panel panel-default"><div class=panel-body><table class="table table-responsive"><thead><tr><th>Patien</th><th>Phase</th><th>Symptom</th><th>Pain</th><th>ADL</th><th>Sport/Rec</th><th>QOL</th></tr></thead><tbody><tr ng-repeat="prom in $ctrl.promDeltas"><td>{{prom.patientName}}</td><td>{{prom.phase}}</td><td>{{prom.symptom}}</td><td>{{prom.aDL}}</td><td>{{prom.sportRec}}</td><td>{{prom.qOl}}</td></tr></tbody></table></div></section><os-prom-by-patient-cards patientskoosscores=$ctrl.patientsKoosScores></os-prom-by-patient-cards>');
$templateCache.put('app/dashboard/scheduledPatients/scheduledPatients.component.html','<div class="panel panel-default dashboard-card"><div class=panel-heading><div class=col-xs-7><h3 class=panel-title>{{$ctrl.title}}</h3></div><div class=col-xs-0></div><div class=col-xs-5><div class="form-group form-group-sm"><select id=daysInFuture class="form-control input-sm" ng-model=$ctrl.period ng-change=$ctrl.changePeriod()><option selected value=7>one week</option><option value=14>two weeks</option><option value=30>1 month</option><option value=60>60 days</option><option value=90>90 days</option></select></div></div><div class=clearfix></div></div><div class=panel-body style="height: 300px; overflow: auto;"><table class="table table-condensed"><thead><tr><th>Name</th><th>DOB</th><th>Procedure Date</th><th>Physician</th><th>Laterality</th></tr></thead><tbody ng-repeat="patient in $ctrl.patients | orderBy: \'procedureDate\'"><tr><td><a ng-show=!patient.isAnonymous ng-click=$ctrl.patientDetails(patient)>{{patient.lastName}}, {{patient.firstName}}</a> <a ng-show=patient.isAnonymous ng-click=$ctrl.patientDetails(patient)>{{patient.anonymousLabel}}</a></td><td ng-show=!patient.isAnonymous>{{patient.dateOfBirth | date: \'shortDate\'}}</td><td ng-show=patient.isAnonymous>{{patient.birthYear}}</td><td>{{patient.procedureDate | date: \'shortDate\'}}</td><td>{{patient.surgeonName}}</td><td>{{patient.laterality}}</td></tr></tbody></table></div></div>');
$templateCache.put('app/patientList/hospitalPractice/hospitalPractice.component.html','<div style=font-weight:bold;font-size:1.1em;><label>Hospital</label> <span class="form-control-static strong" style=padding-right:8px;>{{$ctrl.hospital}}</span> <label ng-show="practice!= \'\'">Practice</label> <span class=form-control-static>{{$ctrl.practice}}</span> <a href=# class="btn btn-link btn-sm" ng-click=$ctrl.openHospitalFilter();>(Change)</a></div>');}]);