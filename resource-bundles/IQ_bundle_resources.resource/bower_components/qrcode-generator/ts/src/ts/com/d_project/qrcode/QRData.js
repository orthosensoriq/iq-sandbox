'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * QRData
             * @author Kazuhiko Arase
             */
            var QRData = (function () {
                function QRData(mode, data) {
                    this.mode = mode;
                    this.data = data;
                }
                QRData.prototype.getMode = function () {
                    return this.mode;
                };
                QRData.prototype.getData = function () {
                    return this.data;
                };
                QRData.prototype.getLengthInBits = function (typeNumber) {
                    if (1 <= typeNumber && typeNumber < 10) {
                        // 1 - 9
                        switch (this.mode) {
                            case qrcode.Mode.MODE_NUMBER: return 10;
                            case qrcode.Mode.MODE_ALPHA_NUM: return 9;
                            case qrcode.Mode.MODE_8BIT_BYTE: return 8;
                            case qrcode.Mode.MODE_KANJI: return 8;
                            default:
                                throw 'mode:' + this.mode;
                        }
                    }
                    else if (typeNumber < 27) {
                        // 10 - 26
                        switch (this.mode) {
                            case qrcode.Mode.MODE_NUMBER: return 12;
                            case qrcode.Mode.MODE_ALPHA_NUM: return 11;
                            case qrcode.Mode.MODE_8BIT_BYTE: return 16;
                            case qrcode.Mode.MODE_KANJI: return 10;
                            default:
                                throw 'mode:' + this.mode;
                        }
                    }
                    else if (typeNumber < 41) {
                        // 27 - 40
                        switch (this.mode) {
                            case qrcode.Mode.MODE_NUMBER: return 14;
                            case qrcode.Mode.MODE_ALPHA_NUM: return 13;
                            case qrcode.Mode.MODE_8BIT_BYTE: return 16;
                            case qrcode.Mode.MODE_KANJI: return 12;
                            default:
                                throw 'mode:' + this.mode;
                        }
                    }
                    else {
                        throw 'typeNumber:' + typeNumber;
                    }
                };
                return QRData;
            }());
            qrcode.QRData = QRData;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=QRData.js.map