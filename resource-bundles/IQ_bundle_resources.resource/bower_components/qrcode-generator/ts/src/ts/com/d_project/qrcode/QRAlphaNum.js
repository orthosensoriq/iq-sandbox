/// <reference path="QRData.ts" />
'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * QRAlphaNum
             * @author Kazuhiko Arase
             */
            var QRAlphaNum = (function (_super) {
                __extends(QRAlphaNum, _super);
                function QRAlphaNum(data) {
                    return _super.call(this, qrcode.Mode.MODE_ALPHA_NUM, data) || this;
                }
                QRAlphaNum.prototype.write = function (buffer) {
                    var s = this.getData();
                    var i = 0;
                    while (i + 1 < s.length) {
                        buffer.put(QRAlphaNum.getCode(s.charAt(i)) * 45 +
                            QRAlphaNum.getCode(s.charAt(i + 1)), 11);
                        i += 2;
                    }
                    if (i < s.length) {
                        buffer.put(QRAlphaNum.getCode(s.charAt(i)), 6);
                    }
                };
                QRAlphaNum.prototype.getLength = function () {
                    return this.getData().length;
                };
                QRAlphaNum.getCode = function (c) {
                    if ('0' <= c && c <= '9') {
                        return c.charCodeAt(0) - '0'.charCodeAt(0);
                    }
                    else if ('A' <= c && c <= 'Z') {
                        return c.charCodeAt(0) - 'A'.charCodeAt(0) + 10;
                    }
                    else {
                        switch (c) {
                            case ' ': return 36;
                            case '$': return 37;
                            case '%': return 38;
                            case '*': return 39;
                            case '+': return 40;
                            case '-': return 41;
                            case '.': return 42;
                            case '/': return 43;
                            case ':': return 44;
                            default:
                                throw 'illegal char :' + c;
                        }
                    }
                };
                return QRAlphaNum;
            }(qrcode.QRData));
            qrcode.QRAlphaNum = QRAlphaNum;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=QRAlphaNum.js.map