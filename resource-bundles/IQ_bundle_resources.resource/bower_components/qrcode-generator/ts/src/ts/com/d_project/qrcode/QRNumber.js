/// <reference path="QRData.ts" />
'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * QRNumber
             * @author Kazuhiko Arase
             */
            var QRNumber = (function (_super) {
                __extends(QRNumber, _super);
                function QRNumber(data) {
                    return _super.call(this, qrcode.Mode.MODE_NUMBER, data) || this;
                }
                QRNumber.prototype.write = function (buffer) {
                    var data = this.getData();
                    var i = 0;
                    while (i + 2 < data.length) {
                        buffer.put(QRNumber.strToNum(data.substring(i, i + 3)), 10);
                        i += 3;
                    }
                    if (i < data.length) {
                        if (data.length - i == 1) {
                            buffer.put(QRNumber.strToNum(data.substring(i, i + 1)), 4);
                        }
                        else if (data.length - i == 2) {
                            buffer.put(QRNumber.strToNum(data.substring(i, i + 2)), 7);
                        }
                    }
                };
                QRNumber.prototype.getLength = function () {
                    return this.getData().length;
                };
                QRNumber.strToNum = function (s) {
                    var num = 0;
                    for (var i = 0; i < s.length; i += 1) {
                        num = num * 10 + QRNumber.chatToNum(s.charAt(i));
                    }
                    return num;
                };
                QRNumber.chatToNum = function (c) {
                    if ('0' <= c && c <= '9') {
                        return c.charCodeAt(0) - '0'.charCodeAt(0);
                    }
                    throw 'illegal char :' + c;
                };
                return QRNumber;
            }(qrcode.QRData));
            qrcode.QRNumber = QRNumber;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=QRNumber.js.map