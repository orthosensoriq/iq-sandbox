'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * ErrorCorrectLevel
             * @author Kazuhiko Arase
             */
            var ErrorCorrectLevel;
            (function (ErrorCorrectLevel) {
                /**
                 * 7%
                 */
                ErrorCorrectLevel[ErrorCorrectLevel["L"] = 1] = "L";
                /**
                 * 15%
                 */
                ErrorCorrectLevel[ErrorCorrectLevel["M"] = 0] = "M";
                /**
                 * 25%
                 */
                ErrorCorrectLevel[ErrorCorrectLevel["Q"] = 3] = "Q";
                /**
                 * 30%
                 */
                ErrorCorrectLevel[ErrorCorrectLevel["H"] = 2] = "H";
            })(ErrorCorrectLevel = qrcode.ErrorCorrectLevel || (qrcode.ErrorCorrectLevel = {}));
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=ErrorCorrectLevel.js.map