angular
    .module('orthosensor')
    .factory('sessionTimeoutHandler', ['$q', '$injector', '$window', function($q, $injector, $window) {
        var sessionHandler = {
            responseError: function(response) {
                // Session has expired
                if (response.status == 500 || response.status == 404 || response.status == 401) {
                    $window.location = '/login';
                }
                return $q.reject(response);
            }
        };
        return sessionHandler;
    }])

.factory('InitFactory', function($q, $rootScope, $http) {

    var factory = {};

    factory.getCurrentUser = function() {

        var deferred = $q.defer();

        IQ_PatientActions.GetCurrentUser(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    return factory;
})

.factory('PatientListFactory', function($q, $rootScope, $http) {

    var factory = {};

    factory.getPatients = function(hospitalId) {
        var deferred = $q.defer();

        IQ_PatientActions.LoadPatientList(
            hospitalId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.getHL7Patients = function(hospitalId) {
        var deferred = $q.defer();

        IQ_PatientActions.LoadHL7PatientList(
            hospitalId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.SearchPatientList = function(hospitalId, keyword) {
        var deferred = $q.defer();

        IQ_PatientActions.SearchPatientList(
            hospitalId,
            keyword,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.SearchHL7PatientList = function(hospitalId, keyword) {
        var deferred = $q.defer();

        IQ_PatientActions.SearchHL7PatientList(
            hospitalId,
            keyword,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.checkDuplicate = function(patient) {

        var deferred = $q.defer();

        var dateofBirthString = moment.utc(patient.DateofBirthString).format('MM/DD/YYYY');

        var accountId = null;
        if (patient.HospitalId != null) {
            accountId = patient.HospitalId;
        } else if (patient.practice.length > 0) {
            accountId = patient.practice;
        } else if (patient.hospital.length > 0) {
            accountId = patient.hospital;
        }

        IQ_PatientActions.CheckDuplicate(
            accountId,
            patient.LastName,
            patient.FirstName,
            patient.MedicalRecordNumber,
            dateofBirthString,
            patient.Gender,
            patient.SocialSecurityNumber,
            patient.PatientNumber,
            patient.AccountNumber,
            patient.Race,
            patient.Language,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.createPatient = function(patient) {

        var deferred = $q.defer();

        var dateofBirthString = moment.utc(patient.DateofBirthString).format('MM/DD/YYYY');

        var accountId = null;
        if (patient.HospitalId != null) {
            accountId = patient.HospitalId;
        } else if (patient.practice.length > 0) {
            accountId = patient.practice;
        } else if (patient.hospital.length > 0) {
            accountId = patient.hospital;
        }

        IQ_PatientActions.CreateNewPatient(
            accountId,
            patient.LastName,
            patient.FirstName,
            patient.MedicalRecordNumber,
            dateofBirthString,
            patient.Gender,
            patient.SocialSecurityNumber,
            patient.PatientNumber,
            patient.AccountNumber,
            patient.Race,
            patient.Language,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };


    factory.createCase = function(patient, patientCase) {

        var deferred = $q.defer();

        var procedureDateString = moment.utc(patientCase.ProcedureDateString).format('MM/DD/YYYY');

        IQ_PatientActions.CreateCase(
            patient.Id,
            patientCase.CaseTypeId,
            procedureDateString,
            patientCase.SurgeonID,
            patientCase.Laterality,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.loadCaseTypes = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadCaseTypes(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.loadSurgeons = function(hospitalId) {

        var deferred = $q.defer();

        IQ_PatientActions.LoadSurgeons(
            hospitalId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.loadHospitals = function() {
        var deferred = $q.defer();

        IQ_PatientActions.LoadHospitals(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.loadPractices = function() {
        var deferred = $q.defer();

        IQ_PatientActions.LoadPractices(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    return factory;
})

.factory('PatientDetailsFactory', function($q, $rootScope, $http) {

    var factory = {};

    factory.LoadCasesAndEvents = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadCasesAndEvents($rootScope.patient.Id,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.UpdatePatient = function(patient) {

        var deferred = $q.defer();

        var dateofBirthString = moment.utc(patient.DateofBirthString).format('MM/DD/YYYY');

        IQ_PatientActions.UpdatePatient(
            patient.Id,
            patient.LastName,
            patient.FirstName,
            patient.MedicalRecordNumber,
            dateofBirthString,
            patient.Gender,
            patient.SocialSecurityNumber,
            patient.PatientNumber,
            patient.AccountNumber,
            patient.Race,
            patient.Language,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    return factory;
})

.factory('CaseDetailsFactory', function($q, $rootScope, $http) {

    var factory = {};

    factory.LoadCaseEvents = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadCaseEvents(
            $rootScope.case.Id,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.LoadSurveys = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadSurveys(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.LoadSurveyScores = function(caseId, surveyId) {

        var deferred = $q.defer();

        IQ_PatientActions.LoadSurveyScores(
            caseId, surveyId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.CompleteEvent = function(eventId) {
        var deferred = $q.defer();

        IQ_PatientActions.CompleteEvent(
            eventId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.UpdateProcedure = function(procedureData) {

        var deferred = $q.defer();

        var procedureDateString = moment.utc(procedureData.procedureDateString).format('MM/DD/YYYY');
        var patientConsentObtained = procedureData.patientConsentObtained;
        if (procedureData.patientConsentObtained == 1) {
            patientConsentObtained = true;
        }

        IQ_PatientActions.UpdateProcedure(
            procedureData.Id,
            procedureData.caseId,
            procedureData.physician,
            procedureData.laterality,
            procedureDateString,
            patientConsentObtained,
            procedureData.anesthesiaType,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    factory.AddSensor = function(barcode, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.AddSensor(
            barcode, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.AddSensorManually = function(ref, man, sn, lot, expMonths, expYears, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.AddSensorManually(
            ref, man, sn, lot, expMonths, expYears, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.DeleteSensor = function(deviceId, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.DeleteSensor(
            deviceId, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.LoadProducts = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadProducts(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.loadSensorRefs = function() {

        var deferred = $q.defer();

        IQ_PatientActions.LoadSensorRefs(
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        handleSessionTimeout(event);
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.AddImplantManually = function(productId, lotNumber, expMonths, expYears, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.AddImplantManually(
            productId, lotNumber, expMonths, expYears, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.AddImplant = function(firstbarcode, secondbarcode, expMonths, expYears, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.AddImplant(
            firstbarcode, secondbarcode, expMonths, expYears, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.DeleteImplant = function(implantId) {

        var deferred = $q.defer();

        IQ_PatientActions.DeleteImplant(
            implantId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.AddNote = function(title, text, procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.AddNote(
            title, text, procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.UpdateNote = function(title, text, noteId) {

        var deferred = $q.defer();

        IQ_PatientActions.UpdateNote(
            title, text, noteId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.DeleteNote = function(noteId) {

        var deferred = $q.defer();

        IQ_PatientActions.DeleteNote(
            noteId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.DeleteSensorLine = function(line, procedureID) {
        var deferred = $q.defer();

        IQ_PatientActions.DeleteSensorLine(
            line.SD_Type_MedialLoadID,
            line.SD_Type_LateralLoadID,
            line.SD_Type_RotationID,
            line.SD_Type_ActualFlexID,
            line.SD_Type_APID,
            line.SD_Type_HKAID,
            line.SD_Type_TibiaRotID,
            line.SD_Type_TibiaTiltID,
            procedureID,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.LoadLinkStationsIDs = function(hospitalId) {

        var deferred = $q.defer();

        IQ_PatientActions.LoadLinkStationsIDs(
            hospitalId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }


    factory.CheckSoftwareVersion = function(procedureId, linkStationId, laterality) {

        var deferred = $q.defer();

        IQ_PatientActions.CheckSoftwareVersion(
            procedureId, linkStationId, laterality,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.GetImageList = function(procedureId, osDeviceId, linkStationId) {

        var deferred = $q.defer();

        IQ_PatientActions.GetImageList(
            procedureId, osDeviceId, linkStationId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.SaveImageToRecord = function(recordId, imageURL) {

        var deferred = $q.defer();

        IQ_PatientActions.SaveImageToRecord(
            recordId, imageURL,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.MatchSensorDataAndAttachment = function(recordId) {

        var deferred = $q.defer();

        IQ_PatientActions.MatchSensorDataAndAttachment(
            recordId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    factory.LoadSensorDataLines = function(procedureId) {

        var deferred = $q.defer();

        IQ_PatientActions.LoadSensorDataLines(
            procedureId,
            function(result, event) {
                //console.log(result);
                $rootScope.$apply(function() {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        $rootScope.handleSessionTimeout(event);
                        deferred.reject(event);
                    }
                })
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    }

    return factory;
})

.factory('LogFactory', function($q, $rootScope, $http) {

    var factory = {};

    factory.logReadsObject = function(objectId, objectType) {

        var deferred = $q.defer();

        IQ_PatientActions.logReadsObject(
            objectId,
            objectType,
            function(result, event) {
                //console.log(result);
                //No need to confirm
            }, { buffer: false, escape: true, timeout: 120000 });

        return deferred.promise;
    };

    return factory;
});
