'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var io;
        (function (io) {
            /**
             * Base64
             * @author Kazuhiko Arase
             */
            var Base64 = (function () {
                function Base64() {
                    throw 'error';
                }
                Base64.encode = function (data) {
                    var bout = new io.ByteArrayOutputStream();
                    try {
                        var ostream = new io.Base64EncodeOutputStream(bout);
                        try {
                            ostream.writeBytes(data);
                        }
                        finally {
                            ostream.close();
                        }
                    }
                    finally {
                        bout.close();
                    }
                    return bout.toByteArray();
                };
                Base64.decode = function (data) {
                    var bout = new io.ByteArrayOutputStream();
                    try {
                        var istream = new io.Base64DecodeInputStream(new io.ByteArrayInputStream(data));
                        try {
                            var b;
                            while ((b = istream.readByte()) != -1) {
                                bout.writeByte(b);
                            }
                        }
                        finally {
                            istream.close();
                        }
                    }
                    finally {
                        bout.close();
                    }
                    return bout.toByteArray();
                };
                return Base64;
            }());
            io.Base64 = Base64;
        })(io = d_project.io || (d_project.io = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=Base64.js.map