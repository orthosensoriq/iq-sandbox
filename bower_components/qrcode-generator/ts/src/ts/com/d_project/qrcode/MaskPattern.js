'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * MaskPattern
             * @author Kazuhiko Arase
             */
            (function (MaskPattern) {
                /**
                 * mask pattern 000
                 */
                MaskPattern[MaskPattern["PATTERN000"] = 0] = "PATTERN000";
                /**
                 * mask pattern 001
                 */
                MaskPattern[MaskPattern["PATTERN001"] = 1] = "PATTERN001";
                /**
                 * mask pattern 010
                 */
                MaskPattern[MaskPattern["PATTERN010"] = 2] = "PATTERN010";
                /**
                 * mask pattern 011
                 */
                MaskPattern[MaskPattern["PATTERN011"] = 3] = "PATTERN011";
                /**
                 * mask pattern 100
                 */
                MaskPattern[MaskPattern["PATTERN100"] = 4] = "PATTERN100";
                /**
                 * mask pattern 101
                 */
                MaskPattern[MaskPattern["PATTERN101"] = 5] = "PATTERN101";
                /**
                 * mask pattern 110
                 */
                MaskPattern[MaskPattern["PATTERN110"] = 6] = "PATTERN110";
                /**
                 * mask pattern 111
                 */
                MaskPattern[MaskPattern["PATTERN111"] = 7] = "PATTERN111";
            })(qrcode.MaskPattern || (qrcode.MaskPattern = {}));
            var MaskPattern = qrcode.MaskPattern;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=MaskPattern.js.map