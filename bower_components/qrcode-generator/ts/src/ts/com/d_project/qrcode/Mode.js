'use strict';
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * Mode
             * @author Kazuhiko Arase
             */
            (function (Mode) {
                /**
                 * number
                 */
                Mode[Mode["MODE_NUMBER"] = 1] = "MODE_NUMBER";
                /**
                 * alphabet and number
                 */
                Mode[Mode["MODE_ALPHA_NUM"] = 2] = "MODE_ALPHA_NUM";
                /**
                 * 8bit byte
                 */
                Mode[Mode["MODE_8BIT_BYTE"] = 4] = "MODE_8BIT_BYTE";
                /**
                 * KANJI
                 */
                Mode[Mode["MODE_KANJI"] = 8] = "MODE_KANJI";
            })(qrcode.Mode || (qrcode.Mode = {}));
            var Mode = qrcode.Mode;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=Mode.js.map