/// <reference path="QRData.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * QRKanji(SJIS only)
             * @author Kazuhiko Arase
             */
            var QRKanji = (function (_super) {
                __extends(QRKanji, _super);
                function QRKanji(data) {
                    _super.call(this, qrcode.Mode.MODE_KANJI, data);
                }
                QRKanji.prototype.write = function (buffer) {
                    var data = qrcode.QRCode.stringToBytes(this.getData());
                    var i = 0;
                    while (i + 1 < data.length) {
                        var c = ((0xff & data[i]) << 8) | (0xff & data[i + 1]);
                        if (0x8140 <= c && c <= 0x9FFC) {
                            c -= 0x8140;
                        }
                        else if (0xE040 <= c && c <= 0xEBBF) {
                            c -= 0xC140;
                        }
                        else {
                            throw 'illegal char at ' + (i + 1) + '/' + c;
                        }
                        c = ((c >>> 8) & 0xff) * 0xC0 + (c & 0xff);
                        buffer.put(c, 13);
                        i += 2;
                    }
                    if (i < data.length) {
                        throw 'illegal char at ' + (i + 1);
                    }
                };
                QRKanji.prototype.getLength = function () {
                    return qrcode.QRCode.stringToBytes(this.getData()).length / 2;
                };
                return QRKanji;
            }(qrcode.QRData));
            qrcode.QRKanji = QRKanji;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=QRKanji.js.map