/// <reference path="QRData.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var com;
(function (com) {
    var d_project;
    (function (d_project) {
        var qrcode;
        (function (qrcode) {
            /**
             * QR8BitByte
             * @author Kazuhiko Arase
             */
            var QR8BitByte = (function (_super) {
                __extends(QR8BitByte, _super);
                function QR8BitByte(data) {
                    _super.call(this, qrcode.Mode.MODE_8BIT_BYTE, data);
                }
                QR8BitByte.prototype.write = function (buffer) {
                    var data = qrcode.QRCode.stringToBytes(this.getData());
                    for (var i = 0; i < data.length; i += 1) {
                        buffer.put(data[i], 8);
                    }
                };
                QR8BitByte.prototype.getLength = function () {
                    return qrcode.QRCode.stringToBytes(this.getData()).length;
                };
                return QR8BitByte;
            }(qrcode.QRData));
            qrcode.QR8BitByte = QR8BitByte;
        })(qrcode = d_project.qrcode || (d_project.qrcode = {}));
    })(d_project = com.d_project || (com.d_project = {}));
})(com || (com = {}));
//# sourceMappingURL=QR8BitByte.js.map